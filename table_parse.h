
#ifndef TABLE_PARSE_h_
#define TABLE_PARSE_h_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include "cmod.ext.h"
#include "parts/variant_cmod_option.h"

enum table_parse {
    TABLE_PARSE_OK = 0, /* default */
    TABLE_PARSE_OOM = 1,        /* out of memory */
    TABLE_PARSE_BADROW = 2,     /* incorrect row size */
    TABLE_PARSE_NDEFAULT = 3,   /* bad number of non-default columns */
};
#define enum_table_parse_len 3

int (table_lambda_parse) (vec_vec_string * lambda_table[static 1], size_t ncol_o[static 1],
                          vec_vec_string * rows_o[static 1]);

int (table_firstrow_colnames) (vec_vec_string * svv_rows[static 1],
                               vec_string * colnarg_o[static 1], size_t nnondef_o[static 1]);

enum table_fmt {
    TABLE_FMT_TSV,
    TABLE_FMT_JSON,
    TABLE_FMT_LAMBDA
};

union table_body {
    const srt_string *verbatim;
    vec_vec_string **lambda;
};

int (table_build) (union table_body tbody, vec_namarg * sv_colnarg, enum table_fmt format,
                   struct table tab[static 1], size_t ierr[static 1]);

#endif
