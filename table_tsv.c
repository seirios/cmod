#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include "table_tsv.h"
#include "parser.h"

#define YY_HEADER_EXPORT_START_CONDITIONS
#include "scanner.h"

int table_tsv_parse(const char *tsv, size_t tsvlen, size_t ncol_o[static 1],
                    vec_vec_string *rows_o[static 1])
{
    int rc = TSV_PARSE_OK;
    size_t tab_ncol = 0;
    bool firstrow = true;
    vec_vec_string *svv_rows = NULL;
    srt_string *cell = NULL;
    vec_string *trow = NULL;

    yyscan_t tsv_scanner = (yyscan_t) 0;
    YY_BUFFER_STATE buf = (YY_BUFFER_STATE) 0;
    struct param tsv_pp = {.init = TSV };
    (tsv_pp.line) = ss_alloc(128);
    if (ss_void == (tsv_pp.line)) {
        rc = TSV_PARSE_OOM;
        goto cleanup;
    }

    errno = 0;
    if (yylex_init_extra(&tsv_pp, &tsv_scanner)) {
        rc = TSV_PARSE_INIT;
        goto cleanup;
    }
    buf = yy_scan_bytes(tsv, tsvlen, tsv_scanner);

    (svv_rows) = sv_alloc_t(SV_PTR, 32);
    if (sv_void == (svv_rows)) {
        rc = TSV_PARSE_OOM;
        goto cleanup;
    }

    int tok;
    YYSTYPE yylval = { 0 };
    YYLTYPE yylloc = { 1, 1, 1, 1 };

    (cell) = ss_alloc(8);
    if (ss_void == (cell)) {
        rc = TSV_PARSE_OOM;
        goto cleanup;
    }
    ;   // init cell
    while ((tok = yylex(&yylval, &yylloc, tsv_scanner)) != YYEOF) {
        switch (tok) {
        case TSV_VALUE:
#if DEBUG
            debug("TSV_VALUE \"%s\"", yyget_text(tsv_scanner));
#endif
            ss_cat_cn(&cell, yyget_text(tsv_scanner), yyget_leng(tsv_scanner));
            break;
        case '\t':     // field separator
        case '\n':     // record separator
#if DEBUG
            debug("%s", ('\t' == tok) ? "TAB\n" : "NL\n");
#endif
            {   // append cell value
                char *cellval = NULL;
                {
                    const char *ctmp = ss_to_c(cell);
                    cellval = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                    if (NULL != (ctmp) && NULL == (cellval)) {
                        rc = TSV_PARSE_OOM;
                        goto cleanup;
                    }

                }

                ss_clear(cell); // reset instead of free

                if (NULL == trow) {     // allocate row
                    size_t ncol = firstrow ? 16 : tab_ncol;
                    (trow) = sv_alloc_t(SV_PTR, ncol);
                    if (sv_void == (trow)) {
                        rc = TSV_PARSE_OOM;
                        goto cleanup;
                    }

                }
                /* append cell to row */
                if (!sv_push(&(trow), &(cellval))) {
                    rc = TSV_PARSE_OOM;
                    goto cleanup;
                }

            }

            if ('\n' == tok) {  // row end
                if (firstrow) { // get number of columns
                    tab_ncol = sv_len(trow);
                    sv_shrink(&trow);   // save space on first row
                    firstrow = false;
                }
                else if (sv_len(trow) != tab_ncol) {    // bad row size
                    rc = TSV_PARSE_BADROW;
                    goto cleanup;
                }
                /* add row to table */
                if (!sv_push(&(svv_rows), &(trow))) {
                    rc = TSV_PARSE_OOM;
                    goto cleanup;
                }

                trow = NULL;    // hand-over
            }
            break;
        default:       // illegal token
            assert(0 && "C% internal error" "");
        }
    }

    sv_shrink(&svv_rows);       // save space

    *ncol_o = tab_ncol;
    *rows_o = svv_rows;
    svv_rows = NULL;    // hand-over
 cleanup:
    ss_free(&cell);
    sv_free(&trow);
    sv_free(&svv_rows);
    ss_free(&tsv_pp.line);
    yy_delete_buffer(buf, tsv_scanner);
    yylex_destroy(tsv_scanner);

    return rc;
}
