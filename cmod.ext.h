
#ifndef CMOD_EXT_h_
#define CMOD_EXT_h_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <stdio.h>

#ifndef info0
#define info0(M) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif

#if DEBUG
#define HERE debug0("HERE");
#define HEREx(x) debug("HERE %d",(x));
#endif

#include "retval.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#if HAVE_LIBSECCOMP
#include <seccomp.h>
#include <sys/ptrace.h>

#ifndef SYS_SECCOMP
#define SYS_SECCOMP 1
#endif                          // SYS_SECCOMP
#endif                          // HAVE_LIBSECCOMP

#include "array_type.h"
#include <signal.h>
#include <stdbool.h>

#ifndef CMOD_RALLOC_H
#define CMOD_RALLOC_H
struct cmod_ralloc_params {
    volatile sig_atomic_t giveup;
    bool verbose;
    int ntimes;
    int msleep;
};

extern struct cmod_ralloc_params ralloc_glob;
__attribute__((malloc, warn_unused_result))
void *(rmalloc) (size_t size);
__attribute__((malloc, warn_unused_result))
void *(rcalloc) (size_t nmemb, size_t size);
__attribute__((warn_unused_result))
void *(rrealloc) (void *ptr, size_t size);
__attribute__((malloc, nonnull((1))))
char *(rstrdup) (const char *s);
extern const struct ret_f0 rmalloc_ret_f;
extern const struct ret_f0 rcalloc_ret_f;
extern const struct ret_f0 rrealloc_ret_f;
extern const struct ret_f0 rstrdup_ret_f;
#endif

#include "keyword_opts.h"

/* error reporting stuff */
#define ERRMSG_SEP "     "
#define ERRMSG_SEP_LEN "5"
void print_wiggly_underline(size_t first, size_t last);

/* indices stuff */
typedef int cmp_func_t(const void *, const void *);
cmp_func_t cmp_namix_by_name;
cmp_func_t cmp_namix_as_float;
cmp_func_t cmp_namix_as_int;

/* global params stuff */
#include "parts/struct_param.h"
extern struct param *glob_pp;   // GLOBAL state
extern const struct ret_f7 pp_alloc_ret_f;
ret_t pp_alloc(struct param *[static 1], bool);
void pp_free(struct param *, bool);
void clear_global_state(void);

#ifndef CMOD_AUTOTYPES_H
#define CMOD_AUTOTYPES_H
/* tagged unions */
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
/* C parsing */
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"

#include "parts/autoarr_arr_cdi.h"

#include "parts/autoarr_arr_cdd.h"

srt_string *(cini_to_str) (const struct c_initializer *ini);
int (arr_cdi_to_str) (const arr_cdi *x, const vec_string *name, srt_string *buf[static 1],
                      bool is_comparison);
int (arr_cdd_to_decl) (arr_cdd *x, struct decl *decl);
srt_string *(decl_to_str) (const struct decl *decl, const char *fname, const char *fsuffix,
                           bool is_statement, bool const_named);
/* C% switch */
#include "parts/variant_mod_case.h"
#endif

/* file I/O stuff */
extern const struct ret_f1 file_open_write_ret_f;
__attribute__((warn_unused_result))
    ret_t(file_open_write) (const char *pathname, bool overwrite, int fd_out[static 1]);

/* map stuff */
int (map_setup_order) (const struct table *tab, size_t *order_o[static 1], size_t nrows_o[static 1],
                       const char *err[static 1], const struct map_cmod_opts *const opts);

int (map_table_lambda) (const struct table *tab, const struct snippet *snip,
                        const vec_namarg *selcols, size_t nrows, size_t order[static nrows],
                        const bool has_rowno, const struct map_cmod_opts *const opts,
                        vec_string *buffers[static 1], srt_vector *buflens[static 1]);

int (map_defined_snippet) (const struct table *tab, const struct snippet *snip,
                           const vec_namarg *argvals, size_t nrows, size_t order[static nrows],
                           const bool has_rowno, const struct map_cmod_opts *const opts,
                           vec_string *buffers[static 1], srt_vector *buflens[static 1]);

/* table stuff */
int (tab_icol_from_opt) (const struct table *tab, const struct cmod_option *opt,
                         size_t out[static 1]);

int (tab_find_icol_irow) (const struct table tab[static 1], const char *search_key, uint64_t icol,
                          uint64_t irow_o[static 1], const struct table_get_cmod_opts *const opts,
                          int ierr[static 1], char *err[static 1]);

inline static const char *table_getcell(const struct table tab[static 1], size_t irow, size_t icol)
{
    return (icol < tab->nnondef) ? sv_at_ptr(sv_at_ptr(tab->svv_rows, irow), icol)
        : CMOD_ARG_VERBATIM_get(*(const struct namarg *)sv_at(tab->sv_colnarg, icol));
}

/* string representation stuff */

srt_string *(hstr_str_repr) (const char *hstr, srt_string * *sout);
srt_string *(namarg_str_repr) (const struct namarg * narg, const vec_namarg * vnarg,
                               srt_string * *sout);
srt_string *(vec_namarg_str_repr) (const vec_namarg * vnarg, setix first, setix last,
                                   srt_string * *sout);
srt_string *(dllarg_str_repr) (const struct dllarg * arg, srt_string * *sout);
srt_string *(snippet_body_str_repr) (const struct snippet * snip, const vec_dllarg * sv_argl,
                                     srt_string * *sout);

/* iftrie stuff */
#include "parts/struct_iftrie_params.h"
#define ss_cat_iftrie_ascii(fp,ifp) ss_cat_iftrie_ascii_aux(fp,0,0,NULL,ifp)
int (ss_cat_iftrie_ascii_aux) (srt_string * *ss, size_t iter, size_t ichar, const vec_u64 * class,
                               const struct iftrie_params * ifp);

#endif
