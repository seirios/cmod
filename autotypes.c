#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>

#include "cmod.ext.h"
#ifndef CMOD_AUTOTYPES_H
#define CMOD_AUTOTYPES_H
/* tagged unions */
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
/* C parsing */
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"

#include "parts/autoarr_arr_cdi.h"

#include "parts/autoarr_arr_cdd.h"

srt_string *(cini_to_str) (const struct c_initializer * ini);
int (arr_cdi_to_str) (const arr_cdi * x, const vec_string * name, srt_string * buf[static 1],
                      bool is_comparison);
int (arr_cdd_to_decl) (arr_cdd * x, struct decl * decl);
srt_string *(decl_to_str) (const struct decl * decl, const char *fname, const char *fsuffix,
                           bool is_statement, bool const_named);
/* C% switch */
#include "parts/variant_mod_case.h"
#endif
#include "util_string.h"

/* auto-growing array of any integers */
void xint_free(struct xint *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_XINT(SIGNED):
        x->value.Signed = 0;
        break;
    case ENUM_XINT(UNSIGNED):
        x->value.Unsigned = 0;
        break;
    }

    *x = (struct xint) { 0 };
}

int xint_dup(const struct xint *x, struct xint *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_XINT(SIGNED):
        y->value.Signed = x->value.Signed;
        break;
    case ENUM_XINT(UNSIGNED):
        y->value.Unsigned = x->value.Unsigned;
        break;
    }

    return 0;
}

/* C% keyword option */
void cmod_option_free(struct cmod_option *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_CMOD_OPTION(XINT):
        xint_free(&(x->value.xint));
        break;
    case ENUM_CMOD_OPTION(STRING):
        ss_free(&(x->value.string));
        break;
    }

    free(x->name);
    x->name = NULL;
    x->is_special = false;
    x->is_verbatim = false;
    x->assign = 0;

    *x = (struct cmod_option) { 0 };
}

int cmod_option_dup(const struct cmod_option *x, struct cmod_option *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_CMOD_OPTION(XINT):
        if (xint_dup(&(x->value.xint), &(y->value.xint))) {
            return 2;
        }
        break;
    case ENUM_CMOD_OPTION(STRING):
        y->value.string = ss_dup(x->value.string);
        if (ss_void == y->value.string && NULL != x->value.string) {
            return 2;
        }

        break;
    }

    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    y->is_special = x->is_special;
    y->is_verbatim = x->is_verbatim;
    y->assign = x->assign;

    return 0;
}

/* switch case stuff */
void mod_case_free(struct mod_case *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_MOD_CASE(EXPRL):
        {
            for (size_t i = 0; i < sv_len(x->value.exprl); ++i) {
                const void *_item = sv_at(x->value.exprl, i);
                {
                    char **item = (char **)(_item);
                    free(*item);
                    *item = NULL;
                }
            }
            sv_free(&(x->value.exprl));
        }

        break;
    case ENUM_MOD_CASE(VACDI):
        {
            for (size_t i = 0; i < sv_len(x->value.vacdi); ++i) {
                const void *_item = sv_at(x->value.vacdi, i);
                arr_cdi_free((arr_cdi *) (_item));
            }
            sv_free(&(x->value.vacdi));
        }

        break;
    }

    free(x->body);
    x->body = NULL;

    *x = (struct mod_case) { 0 };
}

int mod_case_dup(const struct mod_case *x, struct mod_case *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_MOD_CASE(EXPRL):
        {
            y->value.exprl = sv_dup(x->value.exprl);    // duplicate buffer
            if (sv_void == y->value.exprl && NULL != x->value.exprl) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.exprl); ++i) {
                char **_item = sv_elem_addr(y->value.exprl, i);
                const char *_item_old = *_item;
                *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                if (NULL != (_item_old) && NULL == (*_item)) {
                    return 2;
                }

            }
        }

        break;
    case ENUM_MOD_CASE(VACDI):
        {
            y->value.vacdi = sv_dup(x->value.vacdi);    // duplicate buffer
            if (sv_void == y->value.vacdi && NULL != x->value.vacdi) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.vacdi); ++i) {
                arr_cdi *_item = sv_elem_addr(y->value.vacdi, i);
                const arr_cdi _item_old = *_item;
                if (arr_cdi_dup(&(_item_old), &(*_item))) {
                    return 2;
                }
            }
        }

        break;
    }

    y->body = (NULL != (x->body)) ? rstrdup(x->body) : NULL;
    if (NULL != (x->body) && NULL == (y->body)) {
        return 2;
    }

    return 0;
}
