#include "cmod.ext.h"
#include "parser.h"
#include "scanner.h"
#include "filestack.h"
#include <limits.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

struct ret_f2 {
    char *name;
    const struct ret_f2 *p[1 + 2];
};

const struct ret_f2 pushfile_ret_f = {
    .name = "pushfile",
    .p[1] = (const struct ret_f2 *)&rcalloc_ret_f,
    .p[2] = (const struct ret_f2 *)&rstrdup_ret_f,
};

ret_t pushfile(yyscan_t yyscanner, struct param pp[static 1], FILE fp[static 1],
               const char fname[static 1], const char *fpath,
               bool quiet, bool bs_noblank, bool bs_quiet)
{
    ret_t ret = 0;

    /* NOTE: yyget_lloc returns the address of a variable local to yyparse! */
    YYLTYPE *yylloc = pp->in_parse ? yyget_lloc(yyscanner) : NULL;

    if (NULL != pp->curbs) {    // stack not empty
        if (!quiet && pp->verbose)
            yyverbose(NULL, yyscanner, NULL, "switching to buffer \"%s\"", fname);
        if (NULL != yylloc)
            pp->curbs->lloc = *yylloc;  // store location before switch
    }

    /* allocate new buffer stack entry */
    struct bufstack *bs = rcalloc(1, sizeof(struct bufstack));
    if (NULL == bs) {   // fatal
        ret = (unsigned char)RET_ERR_ERRNO;
        ret = ret << 8;
        ret += 1 + 0;

        goto end;
    }
    bs->prev = pp->curbs;       // save last buffer
    bs->fp = fp;        // set file pointer
    bs->bs = yy_create_buffer(bs->fp, YY_BUF_SIZE, yyscanner);  // allocate new buffer
    bs->fname = rstrdup(fname); // non-fatal
    bs->fpath = NULL;
    if (NULL != fpath) {
        char rpath[PATH_MAX] = { 0 };
        errno = 0;
        // FIXME: CWE-120/CWE-785 check input argument no longer than PATH_MAX
        if (rpath == realpath(fpath, rpath))
            bs->fpath = rstrdup(rpath); // non-fatal
    }
    bs->noblank = bs_noblank;   // suppress empty lines
    bs->quiet = bs_quiet;       // suppress all output
    bs->lloc = (YYLTYPE) {
    1, 1, 1, 1};        // initial location

    pp->curbs = bs;     // update top of stack
    yy_switch_to_buffer(pp->curbs->bs, yyscanner);      // switch buffer
    if (NULL != yylloc)
        *yylloc = pp->curbs->lloc;      // update location
    if (NULL != pp->namein)
        free(pp->namein);
    pp->namein = rstrdup(pp->curbs->fname);     // update input filename
    if (NULL == pp->namein)
        pp->namein = "<null>";

 end:
    return ret;
}

static const struct ret_f0 fclose_ret_f = {
    .name = "fclose"
};

struct ret_f1 {
    char *name;
    const struct ret_f1 *p[1 + 1];
};

const struct ret_f1 popfile_ret_f = {
    .name = "popfile",
    .p[1] = (const struct ret_f1 *)&fclose_ret_f,
};

ret_t popfile(yyscan_t yyscanner, struct param pp[static 1])
{
    ret_t ret = 0;

    /* NOTE: yyget_lloc returns the address of a variable local to yyparse! */
    YYLTYPE *yylloc = pp->in_parse ? yyget_lloc(yyscanner) : NULL;

    if (NULL == pp->curbs)      // empty stack, no-op
        goto end;

    struct bufstack *bs = pp->curbs;    // current entry

    if (NULL == bs->prev) {     // last entry
        pp->curbs = NULL;
    }
    else {      // more entries
        pp->curbs = bs->prev;   // update top of stack
        yy_switch_to_buffer(pp->curbs->bs, yyscanner);  // switch buffer
        if (NULL != yylloc)
            *yylloc = pp->curbs->lloc;  // update location
        if (NULL != pp->namein)
            free(pp->namein);
        pp->namein = rstrdup(pp->curbs->fname); // update input filename
        if (NULL == pp->namein)
            pp->namein = "<null>";
        if (pp->verbose)
            yyverbose(NULL, yyscanner, NULL, "switching to buffer \"%s\"", pp->curbs->fname);
    }

    /* free buffer stack entry */
    errno = 0;
    if (NULL != bs->fp && fclose(bs->fp) == EOF) {
        ret = (unsigned char)RET_ERR_ERRNO;
        ret = ret << 8;
        ret += 1 + 0;

    }
    yy_delete_buffer(bs->bs, yyscanner);
    {
        free(bs->fname);
        (bs->fname) = NULL;
    }
    {
        free(bs->fpath);
        (bs->fpath) = NULL;
    }
    {
        free(bs);
        (bs) = NULL;
    }

 end:
    return ret;
}
