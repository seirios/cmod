#ifndef CMOD_YYLVAL_H
#define CMOD_YYLVAL_H
#include "parts/struct_range.h"
#include "parts/struct_table_like.h"
#include "parts/struct_recall_like.h"
#include "parts/variant_c_typespec.h"
#include "parts/variant_c_specifier.h"
#endif

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "cmod.ext.h"

int cmod_include_action_0(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *, bool, srt_string **);
int cmod_once_action_0(void *,
                       struct param *,
                       vec_cmopt **, const YYLTYPE *,
                       char **, const YYLTYPE *, bool, srt_string **);
int cmod_snippet_action_0(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *,
                          vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_snippet_action_1(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *,
                          char *, const YYLTYPE *,
                          struct snippet *, const YYLTYPE *, bool, srt_string **);
int cmod_snippet_action_2(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *, char **, const YYLTYPE *, bool, srt_string **);
int cmod_recall_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         struct recall_like *, const YYLTYPE *,
                         char *, const YYLTYPE *, bool, srt_string **);
int cmod_table_action_0(void *,
                        struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        char **, const YYLTYPE *,
                        vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_table_action_1(void *,
                        struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        char **, const YYLTYPE *,
                        char *, const YYLTYPE *,
                        struct table *, const YYLTYPE *, bool, srt_string **);
int cmod_table_action_2(void *,
                        struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        char **, const YYLTYPE *, char **, const YYLTYPE *, bool, srt_string **);
int cmod_table_stack_action_0(void *,
                              struct param *,
                              vec_cmopt **, const YYLTYPE *,
                              char **, const YYLTYPE *,
                              vec_string **, const YYLTYPE *, bool, srt_string **);
int cmod_map_action_0(void *,
                      const struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      struct table_like *, const YYLTYPE *,
                      const char **, const YYLTYPE *,
                      struct recall_like *, const YYLTYPE *, bool, srt_string **);
int cmod_map_action_1(void *,
                      const struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      struct table_like *, const YYLTYPE *,
                      const char **, const YYLTYPE *,
                      srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_pipe_action_0(void *,
                       const struct param *,
                       vec_cmopt **, const YYLTYPE *,
                       char **, const YYLTYPE *,
                       srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_pipe_action_1(void *,
                       const struct param *,
                       vec_cmopt **, const YYLTYPE *,
                       char **, const YYLTYPE *,
                       struct recall_like *, const YYLTYPE *, bool, srt_string **);
int cmod_unittest_action_0(void *,
                           const struct param *,
                           vec_cmopt **, const YYLTYPE *,
                           char **, const YYLTYPE *,
                           char **, const YYLTYPE *,
                           srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_dsl_def_action_0(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *,
                          char *, const YYLTYPE *,
                          struct table_like *, const YYLTYPE *, bool, srt_string **);
int cmod_dsl_action_0(void *,
                      const struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_intop_action_0(void *,
                        const struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        vec_xint **, const YYLTYPE *, bool, srt_string **);
int cmod_delay_action_0(void *,
                        const struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        char **, const YYLTYPE *,
                        struct xint *, const YYLTYPE *, bool, srt_string **);
int cmod_defined_action_0(void *,
                          const struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          vec_string **, const YYLTYPE *, bool, srt_string **);
int cmod_defined_action_1(void *,
                          const struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          vec_string **, const YYLTYPE *,
                          vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_strcmp_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_strstr_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_strlen_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_strsub_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_table_size_action_0(void *,
                             const struct param *,
                             vec_cmopt **, const YYLTYPE *,
                             vec_string **, const YYLTYPE *, bool, srt_string **);
int cmod_table_length_action_0(void *,
                               const struct param *,
                               vec_cmopt **, const YYLTYPE *,
                               vec_string **, const YYLTYPE *,
                               vec_string **, const YYLTYPE *, bool, srt_string **);
int cmod_table_get_action_0(void *,
                            const struct param *,
                            vec_cmopt **, const YYLTYPE *,
                            vec_string **, const YYLTYPE *,
                            vec_namarg **, const YYLTYPE *, bool, srt_string **);
int cmod_typedef_action_0(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          vec_string **, const YYLTYPE *,
                          const char **, const YYLTYPE *, bool, srt_string **);
int cmod_typedef_action_1(void *,
                          struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          struct decl *, const YYLTYPE *, bool, srt_string **);
int cmod_proto_action_0(void *,
                        struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        struct decl *, const YYLTYPE *, bool, srt_string **);
int cmod_def_action_0(void *,
                      struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      const char **, const YYLTYPE *, bool, srt_string **);
int cmod_def_action_1(void *,
                      struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      const char **, const YYLTYPE *, bool, srt_string **);
int cmod_def_action_2(void *,
                      struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      const char **, const YYLTYPE *, bool, srt_string **);
int cmod_def_action_3(void *,
                      struct param *,
                      vec_cmopt **, const YYLTYPE *,
                      char **, const YYLTYPE *,
                      const char **, const YYLTYPE *, bool, srt_string **);
int cmod_unused_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_string **, const YYLTYPE *,
                         const char **, const YYLTYPE *, bool, srt_string **);
int cmod_prefix_action_0(void *,
                         struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         char **, const YYLTYPE *,
                         const char **, const YYLTYPE *, bool, srt_string **);
int cmod_enum_action_0(void *,
                       struct param *,
                       vec_cmopt **, const YYLTYPE *,
                       char **, const YYLTYPE *,
                       const char **, const YYLTYPE *,
                       struct table_like *, const YYLTYPE *, bool, srt_string **);
int cmod_strin_action_0(void *,
                        const struct param *,
                        vec_cmopt **, const YYLTYPE *,
                        char **, const YYLTYPE *,
                        struct table_like *, const YYLTYPE *,
                        srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_foreach_action_0(void *,
                          const struct param *,
                          vec_cmopt **, const YYLTYPE *,
                          char **, const YYLTYPE *,
                          vec_string **, const YYLTYPE *,
                          srt_string **, const YYLTYPE *, bool, srt_string **);
int cmod_switch_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         vec_string **, const YYLTYPE *,
                         vec_mcase **, const YYLTYPE *, bool, srt_string **);
int cmod_free_action_0(void *,
                       const struct param *,
                       vec_cmopt **, const YYLTYPE *,
                       vec_string **, const YYLTYPE *,
                       const char **, const YYLTYPE *, bool, srt_string **);
int cmod_arrlen_action_0(void *,
                         const struct param *,
                         vec_cmopt **, const YYLTYPE *,
                         char **, const YYLTYPE *, bool, srt_string **);
