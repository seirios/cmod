" Vim syntax file
" Language: C%
" Maintainer: Sirio Bolaños Puchet <seirios@ik.me>
" URL: https://gitlab.com/seirios/cmod
" Last Change: 2024-03-30
" Version: 0.4
"
" Changelog:
"   0.4 - Improve and update to latest syntax
"   0.3 - Hand-made rich syntax!
"   0.2 - Added all C% keywords so far
"   0.1 - initial upload, based on cpp.vim

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" Read the C syntax to start with
if version < 600
  so <sfile>:p:h/c.vim
else
  runtime! syntax/c.vim
  unlet b:current_syntax
endif

" C% keywords

" General elements
syn match cmodFrom "<-"
syn match cmodDnl "%dnl"
syn match cmodHerestringOpen "%<<\([a-zA-Z_]\([a-zA-Z_]\|[0-9]\)*\)\?"
syn match cmodHerestringClose "\([a-zA-Z_]\([a-zA-Z_]\|[0-9]\)*\)\?>>%"
syn region cmodVerbatim oneline start="`" skip="\\\\\|\\`" end="`"
" General elements (invalid in their context)
syn match cmodBadPipe "|" contained
syn match cmodBadPeriod "\." contained
syn match cmodBadNul "%nul" contained
syn match cmodBadRowno "%NR" contained
syn match cmodBadWildcard "*" contained
syn match cmodBadInteger "\(0[xX][0-9A-Fa-f]\+\)\|\([-+]\?[0-9]\+\)" contained
syn match cmodBadIdentifierSpecial "#\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodBadIdentifierExt "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodBadIdentifier "[a-zA-Z_][a-zA-Z_0-9]*" contained
syn region cmodBadVerbatim oneline start="`" skip="\\\\\|\\`" end="`" contained
" General elements (contained)
syn match cmodLambdaBraces "[{}]" contained
syn match cmodLambdaParens "[()]" contained
syn match cmodPipe "|" contained
syn match cmodPeriod "\." contained
syn match cmodComma "," contained
syn match cmodAssign "=\|?=\|:=\|+=\||=" contained
syn match cmodNul "%nul" contained
syn match cmodNulValue "%nul" contained
syn match cmodRowno "%NR" contained
syn match cmodWildcard "*" contained
syn match cmodLineCont "\\\n" contained
syn match cmodRecallClose "|%" contained
syn match cmodPath "\"[^"]*\"" contained
syn match cmodIdentifier "[a-zA-Z_][a-zA-Z_0-9]*" contained
syn match cmodIdentifierName "[a-zA-Z_][a-zA-Z_0-9]*" contained
syn match cmodIdentifierValue "[a-zA-Z_][a-zA-Z_0-9]*" contained
syn match cmodIdentifierExt "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodIdentifierExtName "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodIdentifierExtValue "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodIdentifierSpecial "#\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?" contained
syn match cmodInteger "\(0[xX][0-9A-Fa-f]\+\)\|\([-+]\?[0-9]\+\)" contained
syn match cmodHerestringOpen "%<<\([a-zA-Z_]\([a-zA-Z_]\|[0-9]\)*\)\?" contained
syn match cmodHerestringClose "\([a-zA-Z_]\([a-zA-Z_]\|[0-9]\)*\)\?>>%" contained
syn region cmodVerbatim oneline start="`" skip="\\\\\|\\`" end="`" contained
syn region cmodVerbatimName oneline start="`" skip="\\\\\|\\`" end="`" contained
" General elements (invalid in their context)
syn match cmodBadAssign "[0-9]\+\s*=" contained
syn match cmodBadAssign "#\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained
syn match cmodBadAssign "`\([^`]\|\\`\)*`\s*=" contained
syn match cmodBadAssign "%nul\s*=" contained
syn match cmodBadEquals "\s*=\s*[,)]"me=e-1 contained

" Identifier capture
syn match cmodIdentifierCapture "\(\s*[a-zA-Z_][a-zA-Z_0-9]*\)\(\s*,\s*[a-zA-Z_][a-zA-Z_0-9]*\)*\s*<-\s*%|"me=e-2 contains=cmodFrom,cmodComma

" Herestring
syn region cmodHerestring matchgroup=cmodDelimiter start="%<<\([A-Z]\+\)\?" end="\([A-Z]\+\)\?>>%" contained
syn region cmodHerestringNested start="%<<" end=">>%" contained containedin=cmodHerestring
syn match cmodHerestringNestedBegin "%<<[A-Z]\+" contained containedin=cmodHerestringNested
syn match cmodHerestringNestedEnd "[A-Z]\+>>%" contained containedin=cmodHerestringNested

" Comments
syn region cmodComment matchgroup=cmodKeyword start='%comment \|%//' end='\n' contains=@cCommentGroup
syn region cmodComment matchgroup=cmodKeyword start='%#' end='%#'

" Lambda table
syn region cmodLambdaTable matchgroup=cmodLambdaDelimiter start="{" skip="`[^`]*`\|[{}(]" end=")" contained contains=cmodVerbatim,cmodNulValue,cmodComma,cmodLambdaBraces keepend
syn match cmodLambdaRange "[-+]\?[0-9]\+\.\.[-+]\?[0-9]\+\(\.\.[-+]\?[0-9]\+\)\?" contained containedin=cmodLambdaTable
syn match cmodLambdaEscape "%%" contained containedin=cmodLambdaTable
syn match cmodLambdaColumnList "\(}\s*\)\@<=(\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\(\s*,\s*\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\)*)" contained containedin=cmodLambdaTable contains=cmodComma,cmodIdentifierExtName,cmodLambdaParens

" Option lists
syn region cmodOptionList matchgroup=cmodOptionDelimiter start="\[" skip="`[^`]*`" end="\]" contained contains=cmodOptionNamed,cmodIdentifierName,cmodBadInteger,cmodBadVerbatim,cmodBadNul,cmodBadIdentifierSpecial,cmodBadAssign,cmodBadRowno,cmodBadWildcard,cmodBadPipe,cmodBadPeriod keepend
syn match cmodOptionValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodOptionList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierSpecial,cmodIdentifierExtValue
syn match cmodOptionNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained contains=cmodAssign,cmodIdentifierName,cmodBadIdentifierExt,cmodBadEquals

" Formal argument list
syn region cmodFormalArgumentList matchgroup=cmodDelimiter start="(" skip="`[^`]*`" end=")" contained contains=cmodComma,cmodIdentifierExtName,cmodBadVerbatim,cmodBadNul,cmodBadInteger,cmodBadIdentifierSpecial,cmodBadAssign,cmodBadRowno,cmodBadWildcard,cmodBadPeriod,cmodBadPipe keepend
syn match cmodFormalArgumentValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodFormalArgumentList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierSpecial,cmodIdentifierExtValue,cmodWildcard,cmodPeriod,cmodHerestring
syn match cmodFormalArgumentNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained containedin=cmodFormalArgumentList contains=cmodAssign,cmodIdentifierExtName,cmodBadEquals

" Table column list
syn region cmodTableColumnList matchgroup=cmodDelimiter start="(" skip="`[^`]*`" end=")" contained contains=cmodComma,cmodIdentifierExtName,cmodBadVerbatim,cmodBadNul,cmodBadInteger,cmodBadIdentifierSpecial,cmodBadAssign,cmodBadRowno,cmodBadWildcard,cmodBadPeriod,cmodBadPipe keepend
syn match cmodTableColumnValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodTableColumnList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierExtValue,cmodBadIdentifierSpecial,cmodBadPeriod
syn match cmodTableColumnNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained containedin=cmodTableColumnList contains=cmodAssign,cmodIdentifierExtName,cmodBadEquals

" Actual argument list
syn region cmodActualArgumentList matchgroup=cmodDelimiter start="(" skip="`[^`]*`" end=")" contained contains=cmodComma,cmodInteger,cmodVerbatim,cmodHerestringOpen,cmodHerestringClose,cmodNulValue,cmodIdentifierValue,cmodIdentifierExtValue,cmodBadIdentifierSpecial,cmodBadAssign,cmodBadRowno,cmodBadWildcard keepend
syn match cmodActualArgumentValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodActualArgumentList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierExtValue,cmodBadIdentifierSpecial
syn match cmodActualArgumentNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained containedin=cmodActualArgumentList contains=cmodAssign,cmodIdentifierExtName,cmodBadEquals

" Function-like arguments
syn region cmodFunctionArgumentList matchgroup=cmodDelimiter start="(" skip="`[^`]*`" end=")" contained contains=cmodComma,cmodVerbatim,cmodNulValue,cmodInteger,cmodIdentifierValue,cmodIdentifierExtValue,cmodIdentifierSpecial,cmodBadAssign,cmodBadRowno,cmodBadWildcard keepend
syn match cmodFunctionArgumentValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodFunctionArgumentList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierSpecial,cmodIdentifierExtValue
syn match cmodFunctionArgumentNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained containedin=cmodFunctionArgumentList contains=cmodAssign,cmodIdentifierExtName,cmodBadEquals

" Map argument list
syn region cmodMapArgumentList matchgroup=cmodDelimiter start="(" skip="`[^`]*`" end=")" contained contains=cmodComma,cmodIdentifierValue,cmodIdentifierExtValue,cmodVerbatim,cmodNulValue,cmodInteger,cmodIdentifierSpecial,cmodBadAssign,cmodRowno,cmodBadWildcard,cmodPeriod keepend
syn match cmodMapArgumentValue "\(=\s*\)\@<=[^,]*" contained containedin=cmodMapArgumentList contains=cmodIdentifierValue,cmodInteger,cmodVerbatim,cmodNulValue,cmodIdentifierSpecial,cmodIdentifierExtValue,cmodRowno,cmodPeriod
syn match cmodMapArgumentNamed "\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?\s*=" contained containedin=cmodMapArgumentList contains=cmodAssign,cmodIdentifierName,cmodIdentifierExtName,cmodBadEquals

" Identifier lists
syn region cmodIdentifierExtList matchgroup=cmodDelimiter start="(" end=")" contained contains=cmodComma,cmodIdentifierExt,cmodBadVerbatim,cmodBadNul,cmodBadIdentifierSpecial,cmodBadRowno
syn region cmodIdentifierExtOrVerbatimNameList matchgroup=cmodDelimiter start="(" end=")" contained contains=cmodComma,cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodBadIdentifierSpecial,cmodBadRowno
syn region cmodIdentifierSpecialList matchgroup=cmodDelimiter start="(" end=")" contained contains=cmodComma,cmodBadIdentifierExt,cmodBadVerbatim,cmodBadNul,cmodIdentifierSpecial,cmodBadRowno

" Integer list
syn region cmodIntegerList matchgroup=cmodDelimiter start="(" end=")" contained contains=cmodComma,cmodInteger,cmodBadIdentifierExt,cmodBadVerbatim,cmodBadNul,cmodBadIdentifierSpecial,cmodBadRowno

" C expression list
syn region cmodCExpressionList matchgroup=cmodDelimiter start="(" end=")" contained

" JSON
syn match cmodJsonBadChar "\S" contained
syn match cmodJsonComma "," contained contains=cmodComma
syn match cmodJsonDelimiter "{\|}\|\[\|\]" contained
syn match cmodJsonKeyword "false\|true\|null" contained
syn match cmodJsonNumber "\([0-9]\+\|[1-9][0-9]\+\|-[0-9]\+\|-[1-9][0-9]\+\)\(\.[0-9]\+\([Ee]\([+-]\)\?[0-9]\+\)\?\)\?" contained
syn match cmodJsonBadNumber "\(0[0-9]\+\|-0[0-9]\+\)\(\.[0-9]\+\([Ee]\([+-]\)\?[0-9]\+\)\?\)\?" contained
syn match cmodJsonString "\"\(\\[\"\\/bfnrt]\|\\u\x\{4}\)*\"" contained
syn match cmodJsonString "\"\([^\"]\|\\\n\)*\"" contained
syn match cmodJsonKey "\"[^\"]*\"\s*:" contained
syn match cmodJsonEscape "\\[\"\\/bfnrt]" contained containedin=cmodJsonString
syn match cmodJsonEscape "\\u\x\{4}" contained containedin=cmodJsonString
syn match cmodJsonLineCont "\\\n" contained containedin=cmodJsonString
syn match cmodJsonBadCont "\\\\\n" contained
syn cluster cmodJson contains=cmodJsonDelimiter,cmodJsonKeyword,cmodJsonString,cmodJsonKey,cmodJsonNumber,cmodJsonBadNumber,cmodJsonBadCont,cmodJsonBadChar,cmodJsonComma

" TSV
syn match cmodTsvChar "." contained
syn match cmodTsvSpace " " contained
syn match cmodTsvNul "%nul" contained contains=cmodNul
syn match cmodTsvLineCont "\\\n" contained contains=cmodLineCont
syn cluster cmodTsv contains=cmodTsvSpace,cmodTsvChar,cmodTsvLineCont,cmodTsvNul

" Table
syn cluster cmodTable contains=cmodTableTsv,cmodTableJson
syn region cmodTableJson matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=@cmodJson,cmodNul keepend
syn region cmodTableJson matchgroup=cmodDelimiter start="%\(json\|JSON\){" skip="%\{2,}}" end="%}" contained contains=@cmodJson,cmodNul keepend
syn region cmodTableTsv matchgroup=cmodDelimiter start="%{\(\s\|\n\)*\(^#.*\n\)*\t"rs=s+2 skip="%\{2,}}" end="%}" contained contains=@cmodTsv,cmodNul keepend
syn region cmodTableTsv matchgroup=cmodDelimiter start="%\(tsv\|TSV\){\(\s\|\n\)*\(^#.*\n\)*\t"rs=s+5 skip="%\{2,}}" end="%}" contained contains=@cmodTsv,cmodNul keepend
syn match cmodTableEscape "%%" contained containedin=cmodTableTsv,cmodTableJson
syn match cmodTableComment "^#.*$" contained containedin=cmodTableTsv,cmodTableJson

" Snippet block
if !exists("cmod_no_highlight_snippet")
    syn region cmodSnippet matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=TOP,@cmodKeywordRegion keepend
else
    syn region cmodSnippet matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=Regular keepend
endif
syn match cmodBadDllarg "$\+[^0-9]\+" contained containedin=cmodSnippet
syn match cmodDllarg "$\+[0-9]\+" contained containedin=cmodSnippet
syn match cmodDllarg "$\+[a-zA-Z_]*{[0-9]\+}" contained containedin=cmodSnippet
syn match cmodDllarg "$\+[a-zA-Z_]*{\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?}" contained containedin=cmodSnippet
syn match cmodDllargSpecial "$\+[a-zA-Z_]*{#[0-9]\+}" contained containedin=cmodSnippet
syn match cmodDllargSpecial "$\+[a-zA-Z_]*{#\([0-9]\+\)\?\([a-zA-Z_]\|[!@&]\)\([a-zA-Z_]\|[0-9]\|[-+.?!@&:]\)*\(/[0-9]\+\)\?}" contained containedin=cmodSnippet
syn match cmodSnippetEscape "\$\{2,}" contained containedin=cmodSnippet
syn match cmodSnippetEscape "%\{2,}" contained containedin=cmodSnippet

" Switch block
syn region cmodSwitchBlock matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=cmodHerestring,cmodVerbatim,Regular keepend

" Verbatim block
syn region cmodVerbatimBlock matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained keepend

" Unittest block
syn region cmodUnittestBlock matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=cmodDllarg,cmodDllargSpecial,cmodBadDllarg,cmodSnippetEscape keepend


" DSL block
syn region cmodDslBlock matchgroup=cmodDelimiter start="%{" skip="%\{2,}}" end="%}" contained contains=Regular,cmodVerbatim keepend
syn match cmodDslKeyword "[a-zA-Z_][a-zA-Z_0-9]*\(/[0-9]\+\)\?" contained containedin=cmodDslBlock
syn match cmodDslOperator "\$" contained containedin=cmodDslBlock
syn match cmodDslOperator "\.[a-zA-Z_][a-zA-Z_0-9]*" contained containedin=cmodDslBlock
syn match cmodDslOperator "[-+*/%=<\[:\]>^&|?!~]\+" contained containedin=cmodDslBlock
syn match cmodDslDelimiter "[()]" contained containedin=cmodDslBlock

" Keyword names
if !exists("cmod_no_highlight_snippet_keywords")
    syn match cmodKeyword "%|" contained containedin=cmodKeyword_recall_short,cmodOptions_recall_short,cmodSnippet
    syn match cmodKeyword "%\*" contained containedin=cmodKeyword_snippet,cmodOptions_snippet,cmodOptions_snippet_short,cmodSnippet
    syn match cmodKeyword "%@" contained containedin=cmodKeyword_delay,cmodOptions_delay,cmodOptions_delay_short,cmodSnippet
    syn match cmodKeyword "%!" contained containedin=cmodKeyword_pipe,cmodOptions_pipe,cmodOptions_pipe_short,cmodSnippet
    syn match cmodKeyword "%?" contained containedin=cmodKeyword_strcmp,cmodOptions_strcmp,cmodOptions_strcmp_short,cmodSnippet
    syn match cmodKeyword "%tablen" contained containedin=cmodKeyword_table_length,cmodOptions_table_length,cmodOptions_table_length_short,cmodSnippet
    syn match cmodKeyword "%tabsize" contained containedin=cmodKeyword_table_size,cmodOptions_table_size,cmodOptions_table_size_short,cmodSnippet
    syn match cmodKeyword "%tabget" contained containedin=cmodKeyword_table_get,cmodOptions_table_get,cmodOptions_table_get_short,cmodSnippet
    syn match cmodKeyword "%tabstack" contained containedin=cmodKeyword_table_stack,cmodOptions_table_stack,cmodOptions_table_stack_short,cmodSnippet
    syn match cmodKeyword "%typedef" contained containedin=cmodKeyword_typedef,cmodOptions_typedef,cmodSnippet
    syn match cmodKeyword "%proto" contained containedin=cmodKeyword_proto,cmodOptions_proto,cmodSnippet
    syn match cmodKeyword "%def" contained containedin=cmodKeyword_def,cmodOptions_def,cmodSnippet
    syn match cmodKeyword "%unused" contained containedin=cmodKeyword_unused,cmodOptions_unused,cmodSnippet
    syn match cmodKeyword "%prefix" contained containedin=cmodKeyword_prefix,cmodOptions_prefix,cmodSnippet
    syn match cmodKeyword "%enum" contained containedin=cmodKeyword_enum,cmodOptions_enum,cmodSnippet
    syn match cmodKeyword "%strin" contained containedin=cmodKeyword_strin,cmodOptions_strin,cmodSnippet
    syn match cmodKeyword "%foreach" contained containedin=cmodKeyword_foreach,cmodOptions_foreach,cmodSnippet
    syn match cmodKeyword "%switch" contained containedin=cmodKeyword_switch,cmodOptions_switch,cmodSnippet
    syn match cmodKeyword "%free" contained containedin=cmodKeyword_free,cmodOptions_free,cmodSnippet
    syn match cmodKeyword "%arrlen" contained containedin=cmodKeyword_arrlen,cmodOptions_arrlen,cmodSnippet
    syn match cmodKeyword "%once" contained containedin=cmodKeyword_once,cmodOptions_once,cmodSnippet
    syn match cmodKeyword "%snippet" contained containedin=cmodKeyword_snippet,cmodOptions_snippet,cmodSnippet
    syn match cmodKeyword "%recall" contained containedin=cmodKeyword_recall,cmodOptions_recall,cmodSnippet
    syn match cmodKeyword "%table" contained containedin=cmodKeyword_table,cmodOptions_table,cmodSnippet
    syn match cmodKeyword "%table-stack" contained containedin=cmodKeyword_table_stack,cmodOptions_table_stack,cmodSnippet
    syn match cmodKeyword "%map" contained containedin=cmodKeyword_map,cmodOptions_map,cmodSnippet
    syn match cmodKeyword "%pipe" contained containedin=cmodKeyword_pipe,cmodOptions_pipe,cmodSnippet
    syn match cmodKeyword "%unittest" contained containedin=cmodKeyword_unittest,cmodOptions_unittest,cmodSnippet
    syn match cmodKeyword "%dsl-def" contained containedin=cmodKeyword_dsl_def,cmodOptions_dsl_def,cmodSnippet
    syn match cmodKeyword "%dsl" contained containedin=cmodKeyword_dsl,cmodOptions_dsl,cmodSnippet
    syn match cmodKeyword "%include" contained containedin=cmodKeyword_include,cmodOptions_include,cmodSnippet
    syn match cmodKeyword "%intop" contained containedin=cmodKeyword_intop,cmodOptions_intop,cmodSnippet
    syn match cmodKeyword "%delay" contained containedin=cmodKeyword_delay,cmodOptions_delay,cmodSnippet
    syn match cmodKeyword "%defined" contained containedin=cmodKeyword_defined,cmodOptions_defined,cmodSnippet
    syn match cmodKeyword "%strcmp" contained containedin=cmodKeyword_strcmp,cmodOptions_strcmp,cmodSnippet
    syn match cmodKeyword "%strstr" contained containedin=cmodKeyword_strstr,cmodOptions_strstr,cmodSnippet
    syn match cmodKeyword "%strlen" contained containedin=cmodKeyword_strlen,cmodOptions_strlen,cmodSnippet
    syn match cmodKeyword "%strsub" contained containedin=cmodKeyword_strsub,cmodOptions_strsub,cmodSnippet
    syn match cmodKeyword "%table-size" contained containedin=cmodKeyword_table_size,cmodOptions_table_size,cmodSnippet
    syn match cmodKeyword "%table-length" contained containedin=cmodKeyword_table_length,cmodOptions_table_length,cmodSnippet
    syn match cmodKeyword "%table-get" contained containedin=cmodKeyword_table_get,cmodOptions_table_get,cmodSnippet
else
    syn match cmodKeyword "%|" contained containedin=cmodKeyword_recall_short,cmodOptions_recall_short
    syn match cmodKeyword "%\*" contained containedin=cmodKeyword_snippet,cmodOptions_snippet,cmodOptions_snippet_short
    syn match cmodKeyword "%@" contained containedin=cmodKeyword_delay,cmodOptions_delay,cmodOptions_delay_short
    syn match cmodKeyword "%!" contained containedin=cmodKeyword_pipe,cmodOptions_pipe,cmodOptions_pipe_short
    syn match cmodKeyword "%?" contained containedin=cmodKeyword_strcmp,cmodOptions_strcmp,cmodOptions_strcmp_short
    syn match cmodKeyword "%tablen" contained containedin=cmodKeyword_table_length,cmodOptions_table_length,cmodOptions_table_length_short
    syn match cmodKeyword "%tabsize" contained containedin=cmodKeyword_table_size,cmodOptions_table_size,cmodOptions_table_size_short
    syn match cmodKeyword "%tabget" contained containedin=cmodKeyword_table_get,cmodOptions_table_get,cmodOptions_table_get_short
    syn match cmodKeyword "%tabstack" contained containedin=cmodKeyword_table_stack,cmodOptions_table_stack,cmodOptions_table_stack_short
    syn match cmodKeyword "%typedef" contained containedin=cmodKeyword_typedef,cmodOptions_typedef
    syn match cmodKeyword "%proto" contained containedin=cmodKeyword_proto,cmodOptions_proto
    syn match cmodKeyword "%def" contained containedin=cmodKeyword_def,cmodOptions_def
    syn match cmodKeyword "%unused" contained containedin=cmodKeyword_unused,cmodOptions_unused
    syn match cmodKeyword "%prefix" contained containedin=cmodKeyword_prefix,cmodOptions_prefix
    syn match cmodKeyword "%enum" contained containedin=cmodKeyword_enum,cmodOptions_enum
    syn match cmodKeyword "%strin" contained containedin=cmodKeyword_strin,cmodOptions_strin
    syn match cmodKeyword "%foreach" contained containedin=cmodKeyword_foreach,cmodOptions_foreach
    syn match cmodKeyword "%switch" contained containedin=cmodKeyword_switch,cmodOptions_switch
    syn match cmodKeyword "%free" contained containedin=cmodKeyword_free,cmodOptions_free
    syn match cmodKeyword "%arrlen" contained containedin=cmodKeyword_arrlen,cmodOptions_arrlen
    syn match cmodKeyword "%once" contained containedin=cmodKeyword_once,cmodOptions_once
    syn match cmodKeyword "%snippet" contained containedin=cmodKeyword_snippet,cmodOptions_snippet
    syn match cmodKeyword "%recall" contained containedin=cmodKeyword_recall,cmodOptions_recall
    syn match cmodKeyword "%table" contained containedin=cmodKeyword_table,cmodOptions_table
    syn match cmodKeyword "%table-stack" contained containedin=cmodKeyword_table_stack,cmodOptions_table_stack
    syn match cmodKeyword "%map" contained containedin=cmodKeyword_map,cmodOptions_map
    syn match cmodKeyword "%pipe" contained containedin=cmodKeyword_pipe,cmodOptions_pipe
    syn match cmodKeyword "%unittest" contained containedin=cmodKeyword_unittest,cmodOptions_unittest
    syn match cmodKeyword "%dsl-def" contained containedin=cmodKeyword_dsl_def,cmodOptions_dsl_def
    syn match cmodKeyword "%dsl" contained containedin=cmodKeyword_dsl,cmodOptions_dsl
    syn match cmodKeyword "%include" contained containedin=cmodKeyword_include,cmodOptions_include
    syn match cmodKeyword "%intop" contained containedin=cmodKeyword_intop,cmodOptions_intop
    syn match cmodKeyword "%delay" contained containedin=cmodKeyword_delay,cmodOptions_delay
    syn match cmodKeyword "%defined" contained containedin=cmodKeyword_defined,cmodOptions_defined
    syn match cmodKeyword "%strcmp" contained containedin=cmodKeyword_strcmp,cmodOptions_strcmp
    syn match cmodKeyword "%strstr" contained containedin=cmodKeyword_strstr,cmodOptions_strstr
    syn match cmodKeyword "%strlen" contained containedin=cmodKeyword_strlen,cmodOptions_strlen
    syn match cmodKeyword "%strsub" contained containedin=cmodKeyword_strsub,cmodOptions_strsub
    syn match cmodKeyword "%table-size" contained containedin=cmodKeyword_table_size,cmodOptions_table_size
    syn match cmodKeyword "%table-length" contained containedin=cmodKeyword_table_length,cmodOptions_table_length
    syn match cmodKeyword "%table-get" contained containedin=cmodKeyword_table_get,cmodOptions_table_get

endif

" Keyword options
syn region cmodOptions_recall_short start="%|\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_recall_short contains=cmodOptionList keepend
syn region cmodOptions_snippet_short start="%\*\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_snippet contains=cmodOptionList keepend
syn region cmodOptions_delay_short start="%@\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_delay contains=cmodOptionList keepend
syn region cmodOptions_pipe_short start="%!\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_pipe contains=cmodOptionList keepend
syn region cmodOptions_strcmp_short start="%?\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strcmp contains=cmodOptionList keepend
syn region cmodOptions_table_length_short start="%tablen\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_length contains=cmodOptionList keepend
syn region cmodOptions_table_size_short start="%tabsize\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_size contains=cmodOptionList keepend
syn region cmodOptions_table_get_short start="%tabget\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_get contains=cmodOptionList keepend
syn region cmodOptions_table_stack_short start="%tabstack\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_stack contains=cmodOptionList keepend
syn region cmodOptions_typedef start="%typedef\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_typedef contains=cmodOptionList keepend
syn region cmodOptions_proto start="%proto\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_proto contains=cmodOptionList keepend
syn region cmodOptions_def start="%def\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_def contains=cmodOptionList keepend
syn region cmodOptions_unused start="%unused\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_unused contains=cmodOptionList keepend
syn region cmodOptions_prefix start="%prefix\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_prefix contains=cmodOptionList keepend
syn region cmodOptions_enum start="%enum\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_enum contains=cmodOptionList keepend
syn region cmodOptions_strin start="%strin\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strin contains=cmodOptionList keepend
syn region cmodOptions_foreach start="%foreach\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_foreach contains=cmodOptionList keepend
syn region cmodOptions_switch start="%switch\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_switch contains=cmodOptionList keepend
syn region cmodOptions_free start="%free\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_free contains=cmodOptionList keepend
syn region cmodOptions_arrlen start="%arrlen\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_arrlen contains=cmodOptionList keepend
syn region cmodOptions_once start="%once\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_once contains=cmodOptionList keepend
syn region cmodOptions_snippet start="%snippet\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_snippet contains=cmodOptionList keepend
syn region cmodOptions_recall start="%recall\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_recall contains=cmodOptionList keepend
syn region cmodOptions_table start="%table\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table contains=cmodOptionList keepend
syn region cmodOptions_table_stack start="%table-stack\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_stack contains=cmodOptionList keepend
syn region cmodOptions_map start="%map\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_map contains=cmodOptionList keepend
syn region cmodOptions_pipe start="%pipe\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_pipe contains=cmodOptionList keepend
syn region cmodOptions_unittest start="%unittest\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_unittest contains=cmodOptionList keepend
syn region cmodOptions_dsl_def start="%dsl-def\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_dsl_def contains=cmodOptionList keepend
syn region cmodOptions_dsl start="%dsl\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_dsl contains=cmodOptionList keepend
syn region cmodOptions_include start="%include\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_include contains=cmodOptionList keepend
syn region cmodOptions_intop start="%intop\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_intop contains=cmodOptionList keepend
syn region cmodOptions_delay start="%delay\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_delay contains=cmodOptionList keepend
syn region cmodOptions_defined start="%defined\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_defined contains=cmodOptionList keepend
syn region cmodOptions_strcmp start="%strcmp\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strcmp contains=cmodOptionList keepend
syn region cmodOptions_strstr start="%strstr\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strstr contains=cmodOptionList keepend
syn region cmodOptions_strlen start="%strlen\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strlen contains=cmodOptionList keepend
syn region cmodOptions_strsub start="%strsub\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_strsub contains=cmodOptionList keepend
syn region cmodOptions_table_size start="%table-size\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_size contains=cmodOptionList keepend
syn region cmodOptions_table_length start="%table-length\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_length contains=cmodOptionList keepend
syn region cmodOptions_table_get start="%table-get\(\s\|\n\)*\[" end="\]" contained containedin=cmodKeyword_table_get contains=cmodOptionList keepend


" Keyword regions
" FIXME: does not handle mid-keyword newlines for newline-terminated keywords
syn region cmodKeyword_recall_short		start="%|"			end="|%" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodActualArgumentList,cmodFrom,cmodRecallClose,cmodBadPipe,cmodBadPeriod keepend

syn region cmodKeyword_include			start="^%include\( \|\n\)"	end="\n" contains=cmodPath,cmodBadPipe,cmodBadPeriod keepend

syn region cmodKeyword_once 			start="%once\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_snippet			start="%snippet\( \|\n\)\|%\*\( \|\n\)"				end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodFormalArgumentList,cmodSnippet,cmodFrom,cmodAssign,cmodPath,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_recall			start="%recall\( \|\n\)"	end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodActualArgumentList,cmodFrom,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_table			start="%table\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodTableColumnList,@cmodTable,cmodPath,cmodAssign,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_table_stack		start="%table-stack\( \|\n\)\|%tabstack\( \|\n\)"	end="\n" contains=cmodIdentifierExtOrVerbatimNameList,cmodVerbatimName,cmodNul,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_map				start="%map\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodMapArgumentList,cmodSnippet,cmodLambdaTable,cmodPipe,cmodFrom,cmodComma,cmodBadPeriod
syn region cmodKeyword_pipe				start="%pipe\( \|\n\)\|%!\( \|\n\)"					end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodActualArgumentList,cmodVerbatimBlock,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_unittest			start="%unittest\( \|\n\)"	end="\n" contains=cmodIdentifierExt,cmodVerbatim,cmodUnittestBlock,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_dsl_def 			start="%dsl-def\( \|\n\)"	end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodFunctionArgumentList,@cmodTable,cmodAssign,cmodBadPipe,cmodBadPeriod
syn region cmodKeyword_dsl				start="%dsl\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodDslBlock,cmodBadPipe,cmodBadPeriod

syn region cmodKeyword_intop			start="%intop"		end=")" contains=cmodIntegerList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_delay			start="%delay\|%@"		end=")" contains=cmodIntegerList,cmodVerbatim,cmodIdentifierValue,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_defined			start="%defined"	end=")" contains=cmodIdentifier,cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_strcmp			start="%strcmp\|%?"		end=")" contains=cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_strstr			start="%strstr"		end=")" contains=cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_strlen			start="%strlen"		end=")" contains=cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_strsub			start="%strsub"		end=")" contains=cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_table_size		start="%table-size\|%tabsize"		end=")" contains=cmodIdentifierExtOrVerbatimNameList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_table_length		start="%table-length\( \|\n\)\|%tablen\( \|\n\)"	end=")" contains=cmodVerbatimName,cmodNul,cmodIdentifierSpecialList,cmodBadPipe,cmodBadPeriod keepend
syn region cmodKeyword_table_get		start="%table-get\( \|\n\)\|%tabget\( \|\n\)"		end=")" contains=cmodVerbatimName,cmodNul,cmodFunctionArgumentList,cmodBadPipe,cmodBadPeriod keepend

if !exists("cmod_no_highlight_c_declaration")
    syn region cmodKeyword_typedef		start="%typedef\( \|\n\)"	end=";" contains=TOP,@cmodKeywordRegion keepend
    syn region cmodKeyword_proto		start="%proto\( \|\n\)"		end=";" contains=TOP,@cmodKeywordRegion keepend
else
    syn region cmodKeyword_typedef		start="%typedef\( \|\n\)"	end=";" contains=Regular keepend
    syn region cmodKeyword_proto		start="%proto\( \|\n\)"		end=";" contains=Regular keepend
endif
syn region cmodKeyword_def				start="%def\( \|\n\)"		end="{\|}"me=e-1 contains=cmodIdentifier
syn region cmodKeyword_unused			start="%unused\( \|\n\)"	end=";" contains=Regular keepend
syn region cmodKeyword_prefix 			start="%prefix\( \|\n\)"	end=";" contains=cmodIdentifier
" enum: Column references are correct but table column names are not
syn region cmodKeyword_enum 			start="%enum\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,@cmodTable,cmodLambdaTable,cmodFunctionArgumentList,cmodAssign
syn region cmodKeyword_strin			start="%strin\( \|\n\)"		end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodLambdaTable,cmodSnippet,cmodFunctionArgumentList
syn region cmodKeyword_foreach			start="%foreach\( \|\n\)"	end="\n" contains=cmodIdentifierExt,cmodVerbatimName,cmodNul,cmodCExpressionList,cmodSnippet
syn region cmodKeyword_switch			start="%switch\( \|\n\)"	end="\n" contains=cmodCExpressionList,cmodSwitchBlock

syn region cmodKeyword_arrlen			start="%arrlen"		end=")" contains=cmodCExpressionList keepend
syn region cmodKeyword_free				start="%free"		end=")" contains=cmodCExpressionList keepend

syn cluster cmodKeywordRegion contains=cmodKeyword_recall_short,cmodKeyword_typedef,cmodKeyword_proto,cmodKeyword_def,cmodKeyword_unused,cmodKeyword_prefix,cmodKeyword_enum,cmodKeyword_strin,cmodKeyword_foreach,cmodKeyword_switch,cmodKeyword_free,cmodKeyword_arrlen,cmodKeyword_once,cmodKeyword_snippet,cmodKeyword_recall,cmodKeyword_table,cmodKeyword_table_stack,cmodKeyword_map,cmodKeyword_pipe,cmodKeyword_unittest,cmodKeyword_dsl_def,cmodKeyword_dsl,cmodKeyword_intop,cmodKeyword_delay,cmodKeyword_defined,cmodKeyword_strcmp,cmodKeyword_strstr,cmodKeyword_strlen,cmodKeyword_strsub,cmodKeyword_table_size,cmodKeyword_table_length,cmodKeyword_table_get
" Default highlighting
if version >= 508 || !exists("did_cmod_syntax_inits")
  if version < 508
    let did_cmod_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink cmodKeyword				Keyword
  HiLink cmodRecallClose			Keyword
  HiLink cmodDnl					Keyword

  HiLink cmodNul					Special
  HiLink cmodRowno					Special
  HiLink cmodWildcard				Special
  HiLink cmodComma					Operator
  HiLink cmodPeriod					Operator
  HiLink cmodAssign					Operator
  HiLink cmodPipe					Operator
  HiLink cmodFrom					Operator
  HiLink cmodInteger				Number
  HiLink cmodPath					String
  HiLink cmodDelimiter				Delimiter
  HiLink cmodLineCont				Continuation
  HiLink cmodVerbatim				String
  HiLink cmodVerbatimBlock			String
  HiLink cmodNulValue				String
  HiLink cmodComment				Comment
  HiLink cmodIdentifierName			Type
  HiLink cmodIdentifierExtName		Type
  HiLink cmodIdentifierValue		String
  HiLink cmodIdentifierExtValue		String

  HiLink cmodBadNul					Error
  HiLink cmodBadPeriod				Error
  HiLink cmodBadPipe				Error
  HiLink cmodBadDllarg				Error
  HiLink cmodBadRowno				Error
  HiLink cmodBadWildcard			Error
  HiLink cmodBadEquals				Error
  HiLink cmodBadAssign				Error
  HiLink cmodBadVerbatim			Error
  HiLink cmodBadInteger				Error
  HiLink cmodBadIdentifier			Error
  HiLink cmodBadIdentifierExt		Error
  HiLink cmodBadIdentifierSpecial	Error

  HiLink cmodOptionDelimiter		Operator

  HiLink cmodIdentifier				Identifier
  HiLink cmodIdentifierExt			Identifier
  HiLink cmodIdentifierSpecial		Identifier
  HiLink cmodVerbatimName			Identifier

  HiLink cmodLambdaTable			String
  HiLink cmodLambdaDelimiter		Operator
  HiLink cmodLambdaRange			Special
  HiLink cmodLambdaEscape			Special
  HiLink cmodLambdaParens			Operator
  HiLink cmodLambdaBraces			Operator

  HiLink cmodDllarg					PreProc
  HiLink cmodDllargSpecial			PreProc
  HiLink cmodSnippetEscape			Special
  HiLink cmodTableEscape			Special
  HiLink cmodTableComment			Comment

  HiLink cmodHerestring				String
  HiLink cmodHerestringNested		String
  HiLink cmodHerestringNestedBegin	String
  HiLink cmodHerestringNestedEnd	String
  HiLink cmodHerestringOpen			Delimiter
  HiLink cmodHerestringClose		Delimiter

  HiLink cmodIdentifierCapture		Identifier
  HiLink cmodCExpressionList		Identifier

  HiLink cmodDslKeyword				Identifier
  HiLink cmodDslOperator			Operator
  HiLink cmodDslDelimiter			Delimiter

  HiLink cmodJsonDelimiter			Delimiter
  HiLink cmodJsonKeyword			Type
  HiLink cmodJsonString				String
  HiLink cmodJsonLineCont			StringCont
  HiLink cmodJsonBadCont			Error
  HiLink cmodJsonKey				Identifier
  HiLink cmodJsonNumber				Number
  HiLink cmodJsonEscape				Special
  HiLink cmodJsonBadNumber			Error
  HiLink cmodJsonBadChar			Error

  HiLink cmodTsvChar				Regular
  HiLink cmodTsvSpace				Underlined
  delcommand HiLink
endif

let b:current_syntax = "cmod"
