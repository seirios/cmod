
#ifndef CMOD_UTIL_SNIPPET_H
#define CMOD_UTIL_SNIPPET_H

#include "array_type.h"

int (snip_def_check) (struct snippet * snip, bool is_lambda, size_t ierr[static 1],
                      const char *err[static 1]);
int (snippet_subst) (const struct snippet * snip, const vec_namarg * arglist, char *buf[static 1],
                     size_t buflen[static 1]);

int (snip_subst_args) (const struct snippet * snip, const vec_namarg * inpargs,
                       const vec_namarg * outargs, char *buf[static 1], size_t ierr[static 1],
                       const char *err[static 1], size_t nempty_o[static 1]);
char *(snip_get_buf_rowno) (const struct snippet * snip, size_t nval, const char * *values,
                            size_t out_len[static 1], const char *rowno);
#define snip_get_buf(x0,x1,x2,x3) snip_get_buf_rowno(x0,x1,x2,x3,NULL)

srt_string *(dllarg_str_repr) (const struct dllarg * dll, srt_string * *sout);

#endif
