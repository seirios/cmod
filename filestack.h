#ifndef CMOD_FILESTACK_H_
#define CMOD_FILESTACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <stdbool.h>
#include "parts/struct_bufstack.h"

extern const struct ret_f2 pushfile_ret_f;
__attribute__((warn_unused_result))
ret_t pushfile(void *, struct param[static 1], FILE[static 1], const char[static 1], const char *,
               bool, bool, bool);

extern const struct ret_f1 popfile_ret_f;
__attribute__((warn_unused_result))
ret_t popfile(void *, struct param[static 1]);

#endif
