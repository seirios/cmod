#ifndef CMOD_ARRAY_TYPE_H
#define CMOD_ARRAY_TYPE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "parts/struct_array_type.h"

__attribute__((unused))
void dllarg_clear(struct dllarg *x);

__attribute__((unused))
    __attribute__((warn_unused_result))
int dllarg_dup(const struct dllarg *x, struct dllarg *y);

int cmp_dllarg_by_name(const void *, const void *);
__attribute__((unused))
void snippet_clear(struct snippet *x);

__attribute__((unused))
    __attribute__((warn_unused_result))
int snippet_dup(const struct snippet *x, struct snippet *y);

int cmp_snippet_by_name(const void *, const void *);
__attribute__((unused))
void table_clear(struct table *x);

__attribute__((unused))
    __attribute__((warn_unused_result))
int table_dup(const struct table *x, struct table *y);

int cmp_table_by_name(const void *, const void *);
__attribute__((unused))
void decl_clear(struct decl *x);

__attribute__((unused))
    __attribute__((warn_unused_result))
int decl_dup(const struct decl *x, struct decl *y);

int cmp_decl_by_name(const void *, const void *);
__attribute__((unused))
void fdef_clear(struct fdef *x);

__attribute__((unused))
    __attribute__((warn_unused_result))
int fdef_dup(const struct fdef *x, struct fdef *y);

int cmp_fdef_by_name(const void *, const void *);
#endif
