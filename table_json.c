
#include <assert.h>

#define JSMN_STRICT
#define JSMN_PARENT_LINKS
#include "jsmn.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include "ralloc.h"

#ifndef TABLE_JSON_H
#define TABLE_JSON_H
#include "libsrt/svector.h"
#include "cmod.ext.h"
enum json_parse {
    JSON_PARSE_OK = 0,  /* default */
    JSON_PARSE_OOM = 1, /* out of memory */
    JSON_PARSE_BADROW = 2,      /* incorrect row size */
    JSON_PARSE_NDEFAULT = 3,    /* bad number of non-default columns */
    JSON_PARSE_FORMAT = 4,      /* bad format */
    JSON_PARSE_INVALID = 5,     /* invalid JSON token */
    JSON_PARSE_PARTIAL = 6,     /* partial JSON input */
    JSON_PARSE_UNKNOWN = 7,     /* unknown JSON error */
    JSON_PARSE_DUPLICATE = 8,   /* duplicate column name */
    JSON_PARSE_BADCOLUMN = 9,   /* unknown or mismatched column name */
    JSON_PARSE_MULTI = 10,      /* bad multi-table format */
};
#define enum_json_parse_len 10

/* parse a single JSON table */
int (table_json_parse) (const char *jstr, size_t jstrlen, size_t ncol_o[static 1],
                        vec_vec_string * rows_o[static 1], vec_namarg * colnarg_o[static 1],
                        size_t ierr[static 1]);
/* parse multiple JSON tables */
int (multitable_json_parse) (const char *jstr, size_t jstrlen, vec_table * tables_o[static 1],
                             size_t ierr[static 1]);
/* ensure implicit columns match preset column names */
int (table_json_colnam_preset) (vec_vec_string * rows, const vec_namarg * json_colnam,
                                size_t nnondef, const vec_namarg * preset_colnam,
                                size_t ierr[static 1]);
/* ensure implicit columns match variable column names */
int (table_json_colnam_variable) (const vec_namarg * json_colnam, size_t nnondef,
                                  const vec_namarg * preset_colnam, setix * reqcols[static 1],
                                  size_t ierr[static 1]);
#endif
#include "table_parse.h"

#define FAIL_OOM_CLEANUP { rc = JSON_PARSE_OOM; goto cleanup; }

/* JSON stuff */
int json_parse(size_t jstrlen, const char jstr[static jstrlen], jsmntok_t *tokens_o[static 1],
               size_t ntok_o[static 1], size_t *errpos)
{
    int ret;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    jsmn_parser parser;

    /* query number of tokens */
    jsmn_init(&parser);
    ret = jsmn_parse(&parser, jstr, jstrlen, NULL, 0);
    if (ret < 0)
        goto fail;
    ntok = ret; // number of tokens needed

    if (ntok > 0) {     // parse
        jsmn_init(&parser);
        tokens = calloc(ntok, sizeof(*tokens));
        if (NULL == tokens)
            return JSON_PARSE_OOM;
        ret = jsmn_parse(&parser, jstr, jstrlen, tokens, ntok);
        if (ret < 0)
            goto fail;
        ntok = ret;     // number of tokens used
    }

    *tokens_o = tokens;
    *ntok_o = ntok;

    return JSON_PARSE_OK;

 fail:
    if (NULL != errpos)
        *errpos = parser.pos;   // blame
    free(tokens);
    switch (ret) {
    case JSMN_ERROR_NOMEM:
        return JSON_PARSE_OOM;
    case JSMN_ERROR_INVAL:
        return JSON_PARSE_INVALID;
    case JSMN_ERROR_PART:
        return JSON_PARSE_PARTIAL;
    default:
        return JSON_PARSE_UNKNOWN;
    }
}

bool valid_json_format_arrarr(size_t ntok, const jsmntok_t tokens[static ntok])
{
    if (ntok == 0)
        return false;

    if (tokens[0].type != JSMN_ARRAY)
        return false;   // root must be array

    for (size_t i = 1, irow = 0; i < ntok; i++) {
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_ARRAY:
            if (tok->parent != 0)
                return false;   // must be child of root
            irow = i;   // current row index
            break;
        case JSMN_PRIMITIVE:   /* FALLTHROUGH */
        case JSMN_STRING:
            if (irow == 0 || tok->parent != (int)irow)
                return false;   // must be child of current row
            break;
        default:
            return false;       // other tokens not allowed
        }
    }

    return true;
}

bool valid_json_format_arrobj(size_t ntok, const jsmntok_t tokens[static ntok])
{
    if (ntok == 0)
        return false;

    if (tokens[0].type != JSMN_ARRAY)
        return false;   // root must be array

    for (size_t i = 1, irow = 0, ikey = 0; i < ntok; i++) {
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_OBJECT:
            if (tok->parent != 0)
                return false;   // must be child of root
            irow = i;   // current row index
            break;
        case JSMN_PRIMITIVE:   /* FALLTHROUGH */
        case JSMN_STRING:
            if (tok->size == 0) {       // value
                if (ikey == 0 || tok->parent != (int)ikey)
                    return false;       // must be child of current key
            }
            else if (tok->size == 1) {  // key
                if (irow == 0 || tok->parent != (int)irow)
                    return false;       // must be child of current row
                if (tok->start == tok->end)
                    return false;       // must be non-empty
                ikey = i;       // current key index
            }
            else
                return false;   // only one value per key
            break;
        default:
            return false;       // other tokens not allowed
        }
    }

    return true;
}

char *parse_json_strval(const char *jstr, const jsmntok_t *tok)
{
    char *val = NULL;
    srt_string *(buf) = ss_alloc(0);
    if (ss_void == (buf)) {
        goto cleanup;
    }
    ;

    if (!(tok->type == JSMN_PRIMITIVE && 'n' == jstr[tok->start])) {    // not null
        for (size_t i = tok->start; i < tok->end; ++i) {
            const char c = jstr[i];
            if ('\\' != c) {    // regular char
                ss_cat_cn(&buf, &c, 1);
                continue;
            }
            if (++i == tok->end)
                goto cleanup;   // ERROR: unterminated escape
            const char e = jstr[i];     // escaped char
            char u;     // unescaped char
            switch (e) {
            case '\"': /* FALLTHROUGH */
            case '\\': /* FALLTHROUGH */
            case '/':
                u = e;
                break;
            case 'b':
                u = '\b';
                break;
            case 'f':
                u = '\f';
                break;
            case 'n':
                u = '\n';
                break;
            case 'r':
                u = '\r';
                break;
            case 't':
                u = '\t';
                break;
            case 'u':  // escaped unicode
                {
                    char unibuf[5] = { 0 };
                    for (size_t j = 0; j < 4; ++j) {    // get four hex digits
                        if (++i == tok->end)
                            goto cleanup;       // ERROR: unterminated escape
                        if (!isxdigit((unsigned char)(unibuf[j] = jstr[i])))
                            goto cleanup;       // ERROR: not hex digit
                    }
                    uint64_t uchar = strtoull(unibuf, NULL, 16);
                    if (EOF == ss_putchar(&buf, uchar)) // add UTF-8 encoded character
                        goto cleanup;   // ERROR: overflow
                    continue;
                }
                break;
            }
            ss_cat_cn(&buf, &u, 1);     // append unescaped
        }
    }

#if DEBUG
    const char *cbuf = ss_get_buffer(buf);
    for (size_t i = 0; i < ss_len(buf); ++i) {
        int c = (unsigned char)cbuf[i];
        fprintf(stderr, "%" "#02x (%c) ", c, c);
    }
    fputs("\n", stderr);
#endif

    {
        const char *ctmp = ss_to_c(buf);
        val = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
        if (NULL != (ctmp) && NULL == (val)) {
        }

    }
    ss_free(&(buf));
    if (NULL == val) {
        goto cleanup;
    }

 cleanup:
    ss_free(&buf);

    return val; // maybe NULL
}

/* format must've already been validated by valid_json_format_arrarr */
int parse_json_format_arrarr(const char *jstr, size_t ntok, const jsmntok_t tokens[static ntok],
                             size_t ncol_o[static 1], vec_vec_string *rows_o[static 1])
{
    int rc = JSON_PARSE_OK;
    size_t tab_ncol = 0;
    vec_vec_string *svv_rows = NULL;
    char *cell = NULL;
    vec_string *trow = NULL;

    if (tokens[0].size == 0)
        goto pass;      // empty is OK

    (svv_rows) = sv_alloc_t(SV_PTR, tokens[0].size);
    if (sv_void == (svv_rows)) {
    FAIL_OOM_CLEANUP}
    ;
    for (size_t i = 1; i < ntok; i++) { // skip first token => top-level array
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_ARRAY:
            if (i == 1) {
                tab_ncol = tok->size;
            }   // get number of columns
            else if (sv_len(trow) != tab_ncol) {
                rc = JSON_PARSE_BADROW;
                goto cleanup;
            }   // bad row size
            else {      // from second row
                if (!sv_push(&(svv_rows), &(trow))) {
                FAIL_OOM_CLEANUP}
                // add row to table
                trow = NULL;    // hand-over
            }
            (trow) = sv_alloc_t(SV_PTR, tab_ncol);
            if (sv_void == (trow)) {
            FAIL_OOM_CLEANUP}

            break;
        case JSMN_PRIMITIVE:   /* FALLTHROUGH */
        case JSMN_STRING:
            if (NULL == (cell = parse_json_strval(jstr, tok)))
                FAIL_OOM_CLEANUP;
            if (!sv_push(&(trow), &(cell))) {
            FAIL_OOM_CLEANUP}

            cell = NULL;        // hand-over
            break;
        default:       // token must not be present (checked)
            assert(0 && "C% internal error" "");
            break;
        }
    }
    if (!sv_push(&(svv_rows), &(trow))) {
    FAIL_OOM_CLEANUP}
    // add last row
    trow = NULL;        // hand-over

 pass:
    *ncol_o = tab_ncol;
    *rows_o = svv_rows;
    svv_rows = NULL;    // hand-over
 cleanup:
    free(cell);
    sv_free(&trow);
    sv_free(&svv_rows);

    return rc;
}

/* format must've already been validated by valid_json_format_arrobj */
int parse_json_format_arrobj(const char *jstr, size_t ntok, const jsmntok_t tokens[static ntok],
                             vec_namarg *colnarg_o[static 1], vec_vec_string *rows_o[static 1],
                             size_t ierr[static 1])
{
    int rc = JSON_PARSE_OK;
    size_t tab_ncol = 0;
    vec_vec_string *svv_rows = NULL;
    char *cell = NULL;
    vec_string *trow = NULL;
    vec_namarg *sv_colnarg = NULL;      // column names
    hmap_su *shm_colnam = NULL; // hash map with column name indices
    bool *usedcol = NULL;       // duplicate detection

    if (tokens[0].size == 0)
        goto pass;      // empty

    (svv_rows) = sv_alloc_t(SV_PTR, tokens[0].size);
    if (sv_void == (svv_rows)) {
    FAIL_OOM_CLEANUP}

    for (size_t i = 1, irow = 0, ix = 0; i < ntok; i++) {       // skip first token => top-level array
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_OBJECT:
            if (i == 1) {
                tab_ncol = tok->size;
                if (NULL == (usedcol = malloc(tab_ncol * sizeof(*usedcol))))
                    FAIL_OOM_CLEANUP;
                (sv_colnarg) = sv_alloc(sizeof(struct namarg), tab_ncol, NULL);
                if (sv_void == (sv_colnarg)) {
                FAIL_OOM_CLEANUP}

                (shm_colnam) = shm_alloc(SHM_SU, tab_ncol);
                if (NULL == (shm_colnam)) {
                FAIL_OOM_CLEANUP}

            }
            else if ((size_t)tok->size != tab_ncol) {
                rc = JSON_PARSE_BADROW;
                goto cleanup;
            }   // bad row size
            else {      // from second row
                if (!sv_push(&(svv_rows), &(trow))) {
                FAIL_OOM_CLEANUP}
                // add row to table
                trow = NULL;    // hand-over
                irow++;
            }
            (trow) = sv_alloc_t(SV_PTR, tab_ncol);
            if (sv_void == (trow)) {
            FAIL_OOM_CLEANUP}

            assert(NULL != usedcol);
            memset(usedcol, 0x0, tab_ncol * sizeof(*usedcol));  // reset used columns
            break;
        case JSMN_PRIMITIVE:   /* FALLTHROUGH */
        case JSMN_STRING:
            assert(usedcol != NULL);
            if (tok->size == 0) {       // value
                if (usedcol[ix]) {      // duplicate column name
                    *ierr = ix;
                    rc = JSON_PARSE_DUPLICATE;
                    goto cleanup;
                }
                else {
                    usedcol[ix] = true;
                }       // used column flag

                if (NULL == (cell = parse_json_strval(jstr, tok)))
                    FAIL_OOM_CLEANUP;
#if DEBUG
                fprintf(stderr, "%zu %s\n", ix, cell);
#endif
                if (!sv_set(&trow, ix, &cell))
                    FAIL_OOM_CLEANUP;
                cell = NULL;    // hand-over
            }
            else {      // key
                uint64_t ix64;
                if (irow == 0) {        // collect column names from first row
                    char *key = NULL;
                    if (NULL == (key = parse_json_strval(jstr, tok)))
                        FAIL_OOM_CLEANUP;
                    const srt_string *ss = ss_crefs(key);
                    if (shm_atp_su(shm_colnam, ss, &ix64)) {    // duplicate column name
                        *ierr = ix64;
                        rc = JSON_PARSE_DUPLICATE;
                        goto cleanup;
                    }
                    ix = sv_len(sv_colnarg);
#if DEBUG
                    fprintf(stderr, "Found column %zu: %s\n", ix, key);
#endif
                    struct namarg colnarg = CMOD_ARG_NOVALUE_xset(.name = key);
                    if (!sv_push(&(sv_colnarg), &(colnarg))) {
                    FAIL_OOM_CLEANUP}

                    {
                        const srt_string *_ss = ss_crefs(key);
                        if (!shm_insert_su(&(shm_colnam), _ss, ix)) {
                        FAIL_OOM_CLEANUP}
                    }

                }
                else {  // already have columns
                    const srt_string *ss = ss_refs_buf(&jstr[tok->start], tok->end - tok->start);
                    if (!shm_atp_su(shm_colnam, ss, &ix64)) {
                        rc = JSON_PARSE_BADCOLUMN;
                        goto cleanup;
                    }   // unknown column name
                    ix = ix64;
                }
            }
            break;
        default:
            assert(0 && "C% internal error" "");
            // token must not be present (checked)
        }
    }
    if (!sv_push(&(svv_rows), &(trow))) {
    FAIL_OOM_CLEANUP}
    // add last row
    trow = NULL;        // hand-over

 pass:
    *colnarg_o = sv_colnarg;
    sv_colnarg = NULL;  // hand-over
    *rows_o = svv_rows;
    svv_rows = NULL;    // hand-over
 cleanup:
    free(cell);
    sv_free(&trow);
    sv_free(&svv_rows);
    {
        for (size_t i = 0; i < sv_len(sv_colnarg); ++i) {
            const void *_item = sv_at(sv_colnarg, i);
            namarg_free((struct namarg *)_item);
        }
        sv_free(&(sv_colnarg));
    }
    shm_free(&shm_colnam);
    free(usedcol);

    return rc;
}

int table_json_parse(const char *jstr, size_t jstrlen, size_t ncol_o[static 1],
                     vec_vec_string *rows_o[static 1], vec_namarg *colnarg_o[static 1],
                     size_t ierr[static 1])
{
    int rc = JSON_PARSE_OK;
    size_t tab_ncol = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    jsmntok_t *tokens = NULL;

    size_t ntok = 0;
    size_t errpos = 0;
    if (JSON_PARSE_OK != (rc = json_parse(jstrlen, jstr, &tokens, &ntok, &errpos))) {
        *ierr = errpos; // blame
        goto cleanup;
    }
    if (ntok == 0)
        goto pass;      // empty is OK

#if DEBUG
    for (size_t i = 0; i < ntok; i++) {
        const jsmntok_t *tok = &tokens[i];
        fprintf(stderr, "Token %d [%d - %d] (%d): %.*s\n",
                tok->type, tok->start, tok->end, tok->size,
                tok->end - tok->start, &jstr[tok->start]);
    }
#endif

    if (valid_json_format_arrarr(ntok, tokens)) {       // array of arrays
        if ((rc = parse_json_format_arrarr(jstr, ntok, tokens, &tab_ncol, &svv_rows)))
            goto cleanup;
    }
    else if (valid_json_format_arrobj(ntok, tokens)) {  // array of objects
        if ((rc = parse_json_format_arrobj(jstr, ntok, tokens, &sv_colnarg, &svv_rows, ierr)))
            goto cleanup;
        tab_ncol = sv_len(sv_colnarg);
    }
    else {
        rc = JSON_PARSE_FORMAT;
        goto cleanup;
    }   // invalid format

 pass:
    *ncol_o = tab_ncol;
    *rows_o = svv_rows;
    svv_rows = NULL;    // hand-over
    *colnarg_o = sv_colnarg;
    sv_colnarg = NULL;  // hand-over
 cleanup:
    sv_free(&svv_rows);
    {
        for (size_t i = 0; i < sv_len(sv_colnarg); ++i) {
            const void *_item = sv_at(sv_colnarg, i);
            namarg_free((struct namarg *)_item);
        }
        sv_free(&(sv_colnarg));
    }
    free(tokens);

    return rc;
}

bool valid_json_multitable_format(size_t ntok, jsmntok_t tokens[static ntok])
{
    if (ntok == 0)
        return false;

    if (tokens[0].type != JSMN_OBJECT)
        return false;   // root must be object

    for (size_t i = 1, ikey = 0; i < ntok; i++) {
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_STRING:
            if (tok->size != 1)
                return false;   // must be key
            ikey = i;   // current key index
            break;
        case JSMN_ARRAY:
            {
                if (tok->parent != (int)ikey)
                    return false;       // must be child of key
                if (tok->size == 0)
                    return false;       // must have child rows
                size_t ntok_arr = 0;
                for (size_t j = i; j < ntok; j++)       // count tokens in array
                    if (tokens[j].end <= tok->end)
                        ntok_arr++;
                i += ntok_arr - 1;      // skip to after array
            }
            break;
        default:
            return false;       // other tokens not allowed
        }
    }

    return true;
}

int multitable_json_parse(const char *jstr, size_t jstrlen, vec_table *tables_o[static 1],
                          size_t ierr[static 1])
{
    static const struct table tabinit = {.redef = false };      // emphasis on not redefinable

    int rc = JSON_PARSE_OK;
    struct table tab = tabinit; // workspace
    jsmntok_t *tokens = NULL;

    size_t ntok = 0;
    size_t errpos = 0;
    if ((rc = json_parse(jstrlen, jstr, &tokens, &ntok, &errpos))) {
        *ierr = errpos; // blame
        goto cleanup;   // JSMN error codes
    }
    if (ntok == 0)
        goto pass;      // empty is OK

#if DEBUG
    for (size_t i = 0; i < ntok; i++) {
        const jsmntok_t *tok = &tokens[i];
        fprintf(stderr, "Token %d [%d - %d] (%d): %.*s\n",
                tok->type, tok->start, tok->end, tok->size,
                tok->end - tok->start, &jstr[tok->start]);
    }
#endif

    if (!valid_json_multitable_format(ntok, tokens)) {
        rc = JSON_PARSE_MULTI;
        goto cleanup;
    }
    assert(NULL != tokens);
    if (tokens[0].size == 0)
        goto pass;      // no tables is OK

    for (size_t i = 1; i < ntok; i++) { // skip first token => containing object
        const jsmntok_t *tok = &tokens[i];
        switch (tok->type) {
        case JSMN_STRING:      // get table name
            tab.name = parse_json_strval(jstr, tok);
            if (NULL == tab.name)
                FAIL_OOM_CLEANUP;
            break;
        case JSMN_ARRAY:       // parse table
            {
                size_t ntok_tab = 0;
                for (size_t j = i; j < ntok; j++) {     // count tokens in array
                    if (tokens[j].end <= tok->end) {
                        tokens[j].parent -= i;  // shift links for format validation
                        ntok_tab++;
                    }
                    else
                        break;
                }
                jsmntok_t *tokens_tab = &tokens[i];

                if (valid_json_format_arrarr(ntok_tab, tokens_tab)) {   // array of arrays format
                    size_t tab_ncol = 0;
                    if ((rc = parse_json_format_arrarr(jstr, ntok_tab, tokens_tab,
                                                       &tab_ncol, &tab.svv_rows)))
                        goto cleanup;
                    if ((rc =
                         table_firstrow_colnames(&tab.svv_rows, &tab.sv_colnarg, &tab.nnondef))) {
                        switch (rc) {   // FIXME: map return codes
                        case 1:
                            rc = JSON_PARSE_OOM;
                            break;
                        case 4:
                            rc = JSON_PARSE_BADCOLUMN;
                            break;
                        }
                        goto cleanup;
                    }
                    sv_push(tables_o, &tab);    // add table
                    tab = tabinit;      // hand-over and re-init
                    i += ntok_tab - 1;  // skip to after table end
                }
                else if (valid_json_format_arrobj(ntok_tab, tokens_tab)) {      // array of objects format
                    if ((rc = parse_json_format_arrobj(jstr, ntok_tab, tokens_tab,
                                                       &tab.sv_colnarg, &tab.svv_rows, ierr)))
                        goto cleanup;
                    sv_push(tables_o, &tab);    // add_table
                    tab = tabinit;      // hand-over and re-init
                    i += ntok_tab - 1;  // skip to after table end
                }
                else {
                    rc = JSON_PARSE_FORMAT;
                    goto cleanup;
                }       // invalid table format
            }
            break;
        default:
            assert(0 && "C% internal error" "");
            // token must not be present (checked)
        }
    }

 pass:
 cleanup:
    table_clear(&tab);
    free(tokens);

    return rc;
}

int table_json_colnam_preset(vec_vec_string *rows, const vec_namarg *json_colnam, size_t nnondef,
                             const vec_namarg *preset_colnam, size_t ierr[static 1])
{
    size_t njcolnam = sv_len(json_colnam);
    if (njcolnam != nnondef) {
        *ierr = njcolnam;
        return JSON_PARSE_NDEFAULT;
    }
    if (nnondef == 0)
        return JSON_PARSE_OK;   // no-op
    /* permutation to reorder values */
    uint64_t P[nnondef];        // VLA
    {   // check same names
        /* setup hash map for quick find of preset colnames */
        hmap_su *shm_tmp = NULL;
        (shm_tmp) = shm_alloc(SHM_SU, nnondef);
        if (NULL == (shm_tmp)) {
            return JSON_PARSE_OOM;
        }

        for (size_t i = 0; i < nnondef; ++i) {
            const char *col = VNARG_GETNAME(preset_colnam, i);
            {
                const srt_string *_ss = ss_crefs(col);
                if (!shm_insert_su(&(shm_tmp), _ss, i)) {
                    return JSON_PARSE_OOM;
                }
            }

        }
        for (size_t i = 0; i < njcolnam; ++i) {
            const char *jcol = VNARG_GETNAME(json_colnam, i);
            setix found = { 0 };
            {
                const srt_string *_ss = ss_crefs(jcol);
                if (NULL == shm_tmp) {
                }
                found.set = shm_atp_su(shm_tmp, _ss, &(found).ix);
            }

            if (!found.set) {
                *ierr = i;
                return JSON_PARSE_BADCOLUMN;
            }   // ERROR: column not found
            P[found.ix] = i;    // store permutation
        }
        shm_free(&shm_tmp);
    }
    /* reorder non-default values in each row */
    for (size_t i = 0; i < sv_len(rows); ++i) {
        vec_string *row = sv_at_ptr(rows, i);
        char *reorder[nnondef];
        for (size_t j = 0; j < nnondef; ++j)
            reorder[j] = sv_at_ptr(row, P[j]);
        char **buf = (char **)sv_get_buffer(row);
        memcpy(buf, reorder, nnondef * sizeof(*reorder));
    }

    return JSON_PARSE_OK;
}

int table_json_colnam_variable(const vec_namarg *json_colnam, size_t nnondef,
                               const vec_namarg *preset_colnam, setix *reqcols[static 1],
                               size_t ierr[static 1])
{
    size_t njcolnam = sv_len(json_colnam);
    /* setup hash map for quick find of preset colnames */
    hmap_su *shm_tmp = NULL;
    (shm_tmp) = shm_alloc(SHM_SU, nnondef);
    if (NULL == (shm_tmp)) {
        return JSON_PARSE_OOM;
    }

    for (size_t i = 0; i < nnondef; ++i) {
        const char *col = VNARG_GETNAME(preset_colnam, i);
        {
            const srt_string *_ss = ss_crefs(col);
            if (!shm_insert_su(&(shm_tmp), _ss, i)) {
                return JSON_PARSE_OOM;
            }
        }

    }
    for (size_t i = 0; i < njcolnam; ++i) {
        const char *jcol = VNARG_GETNAME(json_colnam, i);
        setix found = { 0 };
        {
            const srt_string *_ss = ss_crefs(jcol);
            if (NULL == shm_tmp) {
            }
            found.set = shm_atp_su(shm_tmp, _ss, &(found).ix);
        }

        if (!found.set) {
            *ierr = i;
            return JSON_PARSE_BADCOLUMN;
        }
        *reqcols[found.ix] = SETIX_set(i);
    }
    shm_free(&shm_tmp);

    return JSON_PARSE_OK;
}
