#ifndef CMOD_DOCS_H
#define CMOD_DOCS_H

#include <stdbool.h>

/* version macros */
#define CPP_TOSTR(x) #x
#define CPPSTR(x) CPP_TOSTR(x)

#define VERSION_MAJOR 6
#define VERSION_MINOR 9
#define VERSION_REVISION 3
#define VERSION_COMMIT 962

#define VERSION_STRING CPPSTR(VERSION_MAJOR) "." \
                       CPPSTR(VERSION_MINOR) "." \
                       CPPSTR(VERSION_REVISION) " " \
                       "(" CPPSTR(VERSION_COMMIT) ")"

void print_usage(void);

typedef void (print_docs_f) (bool, bool);

print_docs_f print_docs;

print_docs_f print_docs__include;
print_docs_f print_docs__once;
print_docs_f print_docs__snippet;
print_docs_f print_docs__recall;
print_docs_f print_docs__table;
print_docs_f print_docs__table_stack;
print_docs_f print_docs__map;
print_docs_f print_docs__pipe;
print_docs_f print_docs__unittest;
print_docs_f print_docs__dsl_def;
print_docs_f print_docs__dsl;
print_docs_f print_docs__intop;
print_docs_f print_docs__delay;
print_docs_f print_docs__defined;
print_docs_f print_docs__strcmp;
print_docs_f print_docs__strstr;
print_docs_f print_docs__strlen;
print_docs_f print_docs__strsub;
print_docs_f print_docs__table_size;
print_docs_f print_docs__table_length;
print_docs_f print_docs__table_get;
print_docs_f print_docs__typedef;
print_docs_f print_docs__proto;
print_docs_f print_docs__def;
print_docs_f print_docs__unused;
print_docs_f print_docs__prefix;
print_docs_f print_docs__enum;
print_docs_f print_docs__strin;
print_docs_f print_docs__foreach;
print_docs_f print_docs__switch;
print_docs_f print_docs__free;
print_docs_f print_docs__arrlen;
print_docs_f print_docs__comments;
print_docs_f print_docs__idcap;
print_docs_f print_docs__shorthands;

void print_docs_stdlib(void);

#endif
