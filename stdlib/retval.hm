%comment Convenience handling of function return codes

%once std:retval

%comment Error codes
%table std:retval_error = (value,code,description) %tsv{
	 0		INVALID		Unknown error
	-1		FAIL		Non-specific failure
	-2		ARGIN		Invalid input argument
	-3		ARGOUT		Invalid output argument
	-4		NOMEM		Memory allocation error
%}

%typedef ret_t;

%snippet std:retval_check = (func,ret,action = ``) %{
{
    check_ret_t(${ret},(const struct ret_f*)&${func}_ret_f);
    %? [not] ($b{action},``,%<< if(${ret}) { ${action} } >>%)
}
%}

%snippet std:retval_blame = (ret) <- (name,fun) %{
    ${ret} = ${ret} << 8;
    ${ret} += 1 + %tabget [strict] ${name}_ret_f (#name,$b{fun});
%}

%snippet std:retval_blame:errno = (ret) <- (name,fun) %{
    ${ret} = (unsigned char)RET_ERR_ERRNO;
    ${ret} = ${ret} << 8;
    ${ret} += 1 + %tabget [strict] ${name}_ret_f (#name,$b{fun});
%}

%snippet std:retval_init_ret_f0 = (name,static = ``) %{
${static} const struct ret_f0 ${name}_ret_f = {
    .name = $S{name}
};
%}

%snippet std:retval_init_ret_f1+ = (name,num,static = ``) %{
%defined [once,not] !std:retval:ret_f${num} (%<<
struct ret_f${num} {
    char *name;
    const struct ret_f${num} *p[1 + ${num}];
};
>>%)
%once [noskip] !std:retval:ret_f${num}

${static} const struct ret_f%tabsize [opt] (${name}_ret_f) ${name}_ret_f = {
    .name = $S{name},
    %map [opt,add1] ${name}_ret_f %%{
    .p[$${#NR}] = (const struct ret_f%tabsize [opt] (${name}_ret_f)*)&$${name}_ret_f,
    %%}
};
%}

%snippet std:retval_init = (name,static = ``) %{
%@(1)strcmp (`%tabsize [opt] (${name}_ret_f)`,`0`,%<<
%@(1)recall std:retval_init_ret_f0 ($b{name},$b{static})
>>%,%<<
%@(1)recall std:retval_init_ret_f1+ ($b{name},`%tabsize [opt] (${name}_ret_f)`,$b{static})
>>%)
%}

%snippet std:retval_export = (name) %{
extern const struct ret_f%tabsize [opt] (${name}_ret_f) ${name}_ret_f;
%}

%snippet std:retval_funcs = (level) %{
void check_ret_t(ret_t rc, const struct ret_f *f) {
    uint8_t n = 0;
    int saverrno = errno;
    while(rc != 0 && n < sizeof(ret_t)) { // unwind error stack
        int8_t tmp = rc & 0xffUL; // mask last byte
        if(tmp > 0) { // blame
            ${level}("%*s%s: error in called function `%s`",2 * (int)n,"",f->name,f->p[tmp]->name);
            rc >>= 8; // shift last byte
            f = f->p[tmp]; // switch to struct of called function
            ++n;
        } else { // actual error
            ${level}("%*s%s: %s (%d)",2 * (int)n,"",f->name,
                    (tmp == RET_ERR_ERRNO) ? strerror(saverrno) : ret_t_err_strenum(tmp),
                    (tmp == RET_ERR_ERRNO) ? saverrno : tmp);
            rc = 0; // finish processing
        }
    }
    if(n == sizeof(ret_t))
        ${level}("%*s%s: error lost in stack too deep",2 * (int)n,"",f->name);
}
%}

%snippet std:retval_header = %{
#ifndef CMOD_STDLIB_RETVAL_H_
#define CMOD_STDLIB_RETVAL_H_

%include "cmod/common.hm"

%include [c,sys] "string.h"
%include [c,sys] "stdint.h"

typedef uint64_t ret_t; // stack depth = 8 (bytes)

#define RET_ERR_ERRNO (int8_t)INT8_MIN
%enum [helper,upper,prefix = RET_ERR_] ret_t_err = std:retval_error (#code,#description,#value)

/* maximal ret_f type */
struct ret_f {
    char *name;
    const struct ret_f *p[1 + INT8_MAX];
};

/* minimal ret_f type */
struct ret_f0 {
    char *name;
    const struct ret_f *p;
};

void check_ret_t(ret_t, const struct ret_f*);

#endif
%}
