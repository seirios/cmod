%once std:random

%include [c,sys] "time.h"

%comment Random seed

%snippet [redef] std:random_seed64 ?= (seed64) <- () %{ %| std:random_seed64:time ($b{seed64}) <- () |% %}
%snippet [redef] std:random_seed64:const ?= (seed64) <- () %{ { $P{seed64} = 0xBB5; } %}
%snippet std:random_seed64:time = (seed64) <- () %{ $P{seed64} = time(NULL); %}

%snippet std:random_seed64:rdtsc = (seed64) <- () %{
{
    unsigned int _lo,_hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (_lo), "=d" (_hi));
    $P{seed64} = ((unsigned long long)_hi << 32) | _lo;
}
%}


%comment LCG random number generator

%#
 # Seed must be between 1 and m - 1, inclusive
 # seed = 1 + seed % (m - 1) accomplishes this
%#
%snippet std:random_lcg64:seed = (seed64) <- () %{
%recall std:random_seed64 ($b{seed64}) <- ()
$P{seed64} = 1 + $P{seed64} % (%| std:random_lcg64:max |%);
%}

%snippet std:random_lcg64:range = (u64) <- (lo,hi) %{
{ // random integer in [${lo}, ${hi}], unbiased
    do { %| std:random_lcg64 ($b{u64}) |% }
    while ($P{u64} >= (uint64_t)($P{hi} - $P{lo} + 1)
                    * (%| std:random_lcg64:modulus |% / (uint64_t)($P{hi} - $P{lo} + 1)));
    $P{u64} /= (%| std:random_lcg64:modulus |% / (uint64_t)($P{hi} - $P{lo} + 1));
    $P{u64} += (uint64_t)$P{lo};
}
%}

%snippet std:random_lcg64:range_biased = (u64) <- (lo,hi) %{
{ // random integer in [${lo}, ${hi}], biased
    %| std:random_lcg64 ($b{u64}) <- () |%
    $P{u64} %= (uint64_t)($P{hi} - $P{lo} + 1);
    $P{u64} += (uint64_t)$P{lo};
}
%}

%snippet std:random_lcg64 = (u64) <- () %{
%recall std:random_lcg64:tc2 ($b{u64}) <- ()
%}

%snippet std:random_lcg64:modulus = %{ 9223372036854771239ULL %}

%comment Maximum equals modulus - 1
%snippet std:random_lcg64:max = %{ 9223372036854771238ULL %}

%#
 # Tang & Chang, Comp. Phys. Comm. 182:11 (2011), LCG2
 # Passes tests SmallCrush, Crush, BigCrush and PseudoDIEHARD
 # m = 2^63 - 4569          SG prime number
 # a = 5428252657583070383  primitive root modulo m
 # X[n + 1] = (a * X[n]) % m
%#
%snippet std:random_lcg64:tc2 = (u64) <- () %{
$P{u64} = (5428252657583070383ULL * $P{u64}) % 9223372036854771239ULL;
%}

%#
 # Tang & Chang, Comp. Phys. Comm. 182:11 (2011), LCG3
 # Passes tests SmallCrush, Crush, BigCrush and PseudoDIEHARD
 # m = 2^63 - 4569          SG prime number
 # a = 4696371964775821841  primitive root modulo m
 # X[n + 1] = (a * X[n]) % m
%#
%snippet std:random_lcg64:tc3 = (u64) <- () %{
$P{u64} = (4696371964775821841ULL * $P{u64}) % 9223372036854771239ULL;
%}
