#ifndef CMOD_UTIL_SNIPPET_H
#define CMOD_UTIL_SNIPPET_H

#include "array_type.h"
int (snip_def_check) (struct snippet * snip, bool is_lambda, size_t ierr[static 1],
                      const char *err[static 1]);
int (snippet_subst) (const struct snippet * snip, const vec_namarg * arglist, char *buf[static 1],
                     size_t buflen[static 1]);
int (snip_subst_args) (const struct snippet * snip, const vec_namarg * inpargs,
                       const vec_namarg * outargs, char *buf[static 1], size_t ierr[static 1],
                       const char *err[static 1], size_t nempty_o[static 1]);
char *(snip_get_buf_rowno) (const struct snippet * snip, size_t nval, const char * *values,
                            size_t out_len[static 1], const char *rowno);
#define snip_get_buf(x0,x1,x2,x3) snip_get_buf_rowno(x0,x1,x2,x3,NULL)
srt_string *(dllarg_str_repr) (const struct dllarg * dll, srt_string * *sout);
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include <assert.h>
#include "ralloc.h"
#include "util_string.h"

#define ARGLINK_MAX_FOLLOW 2

/* check validity of argument placeholders */
static int snip_check_dllargs_valid(const struct snippet *snip, bool is_lambda,
                                    size_t ierr[static 1], const char *err[static 1])
{
    size_t nforl = sv_len(snip->sv_forl);
    {
        const size_t _len = sv_len(snip->sv_argl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _0 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            *err = NULL;
            switch (((const struct dllarg *)sv_at(snip->sv_argl, _0))->type) {
            case DLLTYPE_NORMAL:
                if (NULL == ((const struct dllarg *)sv_at(snip->sv_argl, _0))->name) {
                    if (nforl == 0
                        &&
                        (((((const struct dllarg *)sv_at(snip->sv_argl, _0))->mod.
                           lead) & (LEAD_NAME)) == (LEAD_NAME))) {
                        *ierr = ((const struct dllarg *)sv_at(snip->sv_argl, _0))->num; // blame position
                        return 2;       // ERROR: formal argument has no name
                    }
                    else
                        continue;       // skip normal positional reference
                }
                if (nforl == 0) {       // referenced a formal argument when there are none
                    *err = ((const struct dllarg *)sv_at(snip->sv_argl, _0))->name;     // blame (const borrow)
                    return 4;   // ERROR: undefined formal argument
                }
                break;
            case DLLTYPE_ARGNAME:
                if (nforl == 0) {       // referenced a formal argument name when there are none
                    *ierr = ((const struct dllarg *)sv_at(snip->sv_argl, _0))->num;     // blame position
                    return 2;   // ERROR: formal argument has no name
                }
                break;
            case DLLTYPE_NR:
                if (!is_lambda) {
                    *err = "#NR (non-lambda)";  // blame (const borrow)
                    return 3;   // ERROR: invalid special argument placeholder
                }
                break;
            case DLLTYPE_ARGV_OUT:
                if (NULL == *err)
                    *err = "#ARGV_OUT"; // blame (const borrow)
                /* FALLTHROUGH */
            case DLLTYPE_ARGV_IN:
                if (NULL == *err)
                    *err = "#ARGV_IN";  // blame (const borrow)
                /* FALLTHROUGH */
            case DLLTYPE_RARGV_OUT:
                if (NULL == *err)
                    *err = "#RARGV_OUT";        // blame (const borrow)
                /* FALLTHROUGH */
            case DLLTYPE_RARGV_IN:
                if (NULL == *err)
                    *err = "#RARGV_IN"; // blame (const borrow)
                /* FALLTHROUGH */
            case DLLTYPE_ARGC_OUT:
                if (NULL == *err)
                    *err = "#ARGC_OUT"; // blame (const borrow)
                /* FALLTHROUGH */
            case DLLTYPE_ARGC_IN:
                if (NULL == *err)
                    *err = "#ARGC_IN";  // blame (const borrow)
                /* FALLTHROUGH */
                if (nforl == 0) // needs formal arguments for input/output distinction
                    return 3;   // ERROR: invalid special argument placeholder
                break;
            default:
                continue;
            }
        }
    }
    return 0;
}

/* check named argument placeholders */
static int snip_check_dllargs_named(const struct snippet *snip, size_t nforl,
                                    bool used[static nforl], const char *err[static 1])
{
    {
        const size_t _len = sv_len(snip->sv_argl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _1 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (NULL == ((struct dllarg *)sv_at(snip->sv_argl, _1))->name)
                continue;       // skip without name
            *err = ((struct dllarg *)sv_at(snip->sv_argl, _1))->name;   // blame (const borrow)

            setix found = { 0 };
            {
                const srt_string *_ss = ss_crefs(((struct dllarg *)sv_at(snip->sv_argl, _1))->name);
                if (NULL == snip->shm_forl) {
                }
                found.set = shm_atp_su(snip->shm_forl, _ss, &(found).ix);
            }

            if (!found.set)
                return 1;       // ERROR: formal argument not found

            ((struct dllarg *)sv_at(snip->sv_argl, _1))->num = found.ix;        // 0-based index of formal argument to be substituted
            used[found.ix] = true;
        }
    }
    return 0;
}

static int snip_count_args(struct snippet *snip, size_t ierr[static 1])
{
    if (SNIPPET_HAS_FORMAL_ARGS(snip)) {        // has formal arguments
        snip->nargs = sv_len(snip->sv_forl);
        /* check that argument references (incl. positional) are in-bounds */
        {
            const size_t _len = sv_len(snip->sv_argl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _2 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (((const struct dllarg *)sv_at(snip->sv_argl, _2))->type == DLLTYPE_NORMAL
                    && ((const struct dllarg *)sv_at(snip->sv_argl, _2))->num >= snip->nargs) {
                    *ierr = ((const struct dllarg *)sv_at(snip->sv_argl, _2))->num;     // blame
                    return 2;   // ERROR: reference out of bounds
                }
            }
        }
    }
    else {      // has positional references only
        srt_set *(sms_ix) = sms_alloc(SMS_U, sv_len(snip->sv_argl));
        if (NULL == (sms_ix)) {
            return 1;
        }

        {
            const size_t _len = sv_len(snip->sv_argl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _3 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (((const struct dllarg *)sv_at(snip->sv_argl, _3))->type != DLLTYPE_NORMAL)
                    continue;
                if (!sms_insert_u
                    (&(sms_ix), ((const struct dllarg *)sv_at(snip->sv_argl, _3))->num)) {
                    return 1;
                };

            }
        }
        snip->nargs = sms_len(sms_ix);  // number of unique positional references
        snip->sms_dllarg = sms_ix;      // set of unique positional references
    }
    return 0;
}

static int follow_arglink(const size_t lvl, const size_t ix, const struct namarg *farg,
                          const vec_namarg *sv_forl, const srt_hmap *shm_forl)
{
    int ret;
    if (farg->type == ENUM_NAMARG(SPECIAL) || farg->type == ENUM_NAMARG(ARGLINK)) {     // arglink
        size_t ixref;
        if (farg->type == ENUM_NAMARG(SPECIAL)) {       // arglink to-be
            setix found = { 0 };
            {
                const srt_string *_ss = ss_crefs(CMOD_ARG_SPECIAL_get(*farg));
                if (NULL == shm_forl) {
                }
                found.set = shm_atp_su(shm_forl, _ss, &(found).ix);
            }

            if (!found.set)
                return 6;       // ERROR: invalid arglink to undefined argument
            ixref = found.ix;
        }
        else
            ixref = CMOD_ARG_ARGLINK_get(*farg);        // inherited arglink
        if (ixref == ix)
            return 7;   // ERROR: invalid arglink to self
        if (lvl + 1 > ARGLINK_MAX_FOLLOW)
            return 8;   // ERROR: too many arglink levels
        if ((ret = follow_arglink(lvl + 1, ix, sv_at(sv_forl, ixref), sv_forl, shm_forl)))      // recurse
            return ret;
    }
    else if (farg->type == ENUM_NAMARG(EXPR)) { // expression
        for (size_t i = 0; i < sv_len(CMOD_ARG_EXPR_get(*farg)); ++i) {
            const struct namarg *earg = sv_at(CMOD_ARG_EXPR_get(*farg), i);
            if (earg->type == ENUM_NAMARG(SPECIAL) || earg->type == ENUM_NAMARG(ARGLINK)) {     // arglink
                size_t ixref;
                if (earg->type == ENUM_NAMARG(SPECIAL)) {       // arglink to-be
                    setix found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs(CMOD_ARG_SPECIAL_get(*earg));
                        if (NULL == shm_forl) {
                        }
                        found.set = shm_atp_su(shm_forl, _ss, &(found).ix);
                    }

                    if (!found.set)
                        return 6;       // ERROR: invalid arglink to undefined argument
                    ixref = found.ix;
                }
                else
                    ixref = CMOD_ARG_ARGLINK_get(*earg);        // inherited arglink
                if (ixref == ix)
                    return 7;   // ERROR: invalid arglink to self
                if (lvl + 1 > ARGLINK_MAX_FOLLOW)
                    return 8;   // ERROR: too many arglink levels
                if ((ret = follow_arglink(lvl + 1, ix, sv_at(sv_forl, ixref), sv_forl, shm_forl)))      // recurse
                    return ret;
            }
        }
    }
    return 0;
}

static void resolve_arglink(struct namarg *farg, const srt_hmap *shm_forl)
{
    assert(farg->type == ENUM_NAMARG(SPECIAL));
    setix found = { 0 };
    {
        const srt_string *_ss = ss_crefs(CMOD_ARG_SPECIAL_get(*farg));
        if (NULL == shm_forl) {
        }
        found.set = shm_atp_su(shm_forl, _ss, &(found).ix);
    }

    assert(found.set);  // must exist (already checked)
    free(CMOD_ARG_SPECIAL_get(*farg));  // clear
    *farg = CMOD_ARG_ARGLINK_xset(found.ix,.name = farg->name); // XXX: change type to ARGLINK
}

int snip_def_check(struct snippet *snip, bool is_lambda, size_t ierr[static 1],
                   const char *err[static 1])
{
    {   // check argument placeholders upfront
        int ret;
        if ((ret = snip_check_dllargs_valid(snip, is_lambda, ierr, err)))
            return ret; // return codes 2, 3 or 4
    }

    /* check formal arguments and build hash map */
    size_t nforl = sv_len(snip->sv_forl);
    (snip->shm_forl) = shm_alloc(SHM_SU, nforl);
    if (NULL == (snip->shm_forl)) {
        return 1;
    }
    {
        const size_t _len = sv_len(snip->sv_forl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _4 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (NULL == ((const struct namarg *)sv_at(snip->sv_forl, _4))->name) {
                *ierr = _4;     // blame (const borrow)
                return 2;       // ERROR: no name
            }
            /* count arguments with default value */
            snip->ndefout +=
                (((const struct namarg *)sv_at(snip->sv_forl, _4))->type != ENUM_NAMARG(NOVALUE)
                 && _4 < snip->nout);
            snip->ndefin +=
                (((const struct namarg *)sv_at(snip->sv_forl, _4))->type != ENUM_NAMARG(NOVALUE)
                 && _4 >= snip->nout);
            // add index to hash map
            {
                const srt_string *_ss =
                    ss_crefs(((const struct namarg *)sv_at(snip->sv_forl, _4))->name);
                if (!shm_insert_su(&(snip->shm_forl), _ss, _4)) {
                    return 1;
                }
            }
        }
    }

    size_t unused = 0;
    if (nforl > 0) {    // check argument placeholders against formal arguments
        bool *(used) = rcalloc((nforl), sizeof(*used));
        if (NULL == (used)) {
            return 1;
        }

        /* check that argument placeholders refer to existing formal arguments */
        if (snip_check_dllargs_named(snip, nforl, used, err)) {
            {
                free(used);
                (used) = NULL;
            }
            return 4;   // ERROR: invalid argument placeholder
        }
        /* count unused formal arguments and check arglinks to-be */
        {
            const size_t _len = sv_len(snip->sv_forl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _5 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                unused += (used[_5] == false);
                *err = ((const struct namarg *)sv_at(snip->sv_forl, _5))->name; // blame (const borrow)
                int ret;
                if (((const struct namarg *)sv_at(snip->sv_forl, _5))->type == ENUM_NAMARG(SPECIAL)) {  // arglink to-be
                    if ((ret =
                         follow_arglink(0, _5, ((const struct namarg *)sv_at(snip->sv_forl, _5)),
                                        snip->sv_forl, snip->shm_forl)))
                        return ret;
                }
                else if (((const struct namarg *)sv_at(snip->sv_forl, _5))->type == ENUM_NAMARG(EXPR)) {        // expression
                    for (size_t i = 0;
                         i <
                         sv_len(CMOD_ARG_EXPR_get
                                (*((const struct namarg *)sv_at(snip->sv_forl, _5)))); ++i) {
                        const struct namarg *earg =
                            sv_at(CMOD_ARG_EXPR_get
                                  (*((const struct namarg *)sv_at(snip->sv_forl, _5))), i);
                        if (earg->type != ENUM_NAMARG(SPECIAL))
                            continue;   // not arglink to-be
                        if ((ret = follow_arglink(0, _5, earg, snip->sv_forl, snip->shm_forl)))
                            return ret;
                    }
                }
            }
        }
        {
            free(used);
            (used) = NULL;
        }

        /* store resolved arglinks and expressions */
        {
            const size_t _len = sv_len(snip->sv_forl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _6 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (((struct namarg *)sv_at(snip->sv_forl, _6))->type == ENUM_NAMARG(SPECIAL)) {        // arglink to-be
                    resolve_arglink(((struct namarg *)sv_at(snip->sv_forl, _6)), snip->shm_forl);
                }
                else if (((struct namarg *)sv_at(snip->sv_forl, _6))->type == ENUM_NAMARG(EXPR)) {      // expression
                    for (size_t i = 0;
                         i <
                         sv_len(CMOD_ARG_EXPR_get(*((struct namarg *)sv_at(snip->sv_forl, _6))));
                         ++i) {
                        struct namarg *earg =
                            (struct namarg *)
                            sv_at(CMOD_ARG_EXPR_get(*((struct namarg *)sv_at(snip->sv_forl, _6))),
                                  i);
                        if (earg->type != ENUM_NAMARG(SPECIAL))
                            continue;   // not arglink to-be
                        resolve_arglink(earg, snip->shm_forl);
                    }
                }
            }
        }
    }

    switch (snip_count_args(snip, ierr)) {      // count arguments and check positional references
    case 1:
        return 1;
        break;  // ERROR: out of memory
    case 2:
        *err = "positional reference out of bounds";    // explain
        return 5;       // ERROR: positional reference out of bounds
        break;
    }

    *ierr = unused;

    return 0;
}

static const char *fillbuf_rep(srt_string *buf[static 1], int c, size_t n)
{
    ss_clear(*buf);     // clear buffer
    for (size_t i = 0; i < n; ++i)
        ss_cat_cn1(buf, c);     // add character
    return ss_get_buffer_r(*buf);
}

static const char *fillbuf_utf8(srt_string *buf[static 1], int c)
{
    ss_cpy_char(buf, c);        // add character
    return ss_get_buffer_r(*buf);
}

char *snip_get_buf_rowno(const struct snippet *snip, size_t nval, const char * *values,
                         size_t out_len[static 1], const char *rowno)
{
    char *res = NULL;   // result string
    char ubuf[STRUMAX_LEN];

    srt_string *buf = NULL;     // buffer for snippet body
    srt_string *srep = NULL;    // buffer for repeated characters
    (buf) = ss_alloc(1024);
    if (ss_void == (buf)) {
        goto cleanup;
    }
    ;
    (srep) = ss_alloc(8);
    if (ss_void == (srep)) {
        goto cleanup;
    }

    const size_t nfarg = sv_len(snip->sv_forl);

    size_t offset = 0;
    for (size_t i = 0; i < sv_len(snip->sv_argl); ++i) {
#define rep (arg->rep)
#define num (arg->num)
        const struct dllarg *const arg = sv_at(snip->sv_argl, i);
        ss_cat_substr(&buf, snip->body, offset, arg->pos - offset);     // append chunk of snippet body
        offset = arg->pos + 1;  // advance over '$' from dllarg

        struct str_mod argmod = arg->mod;
        const char *argval = NULL;
        const char *argname = NULL;

        /* transformed value */
        switch (arg->type) {    // choose argument value
        case DLLTYPE_NORMAL:
            argval = (num < nval) ? values[num] : "";
            argname = (num < nfarg) ? VNARG_GETNAME(snip->sv_forl, num) : "";
            if (num < nval && num < nfarg && argval == argname)
                continue;       // SENTINEL for optional argument
            break;
        case DLLTYPE_ARGNAME:
            argval = (num < nfarg) ? VNARG_GETNAME(snip->sv_forl, num) : "";
            argname = (num < nfarg) ? argval : NULL;
            break;
        case DLLTYPE_NAME:
            argval = (NULL != snip->name) ? snip->name : "";
            break;
        case DLLTYPE_NR:
            argval = (NULL != rowno) ? rowno : "";
            break;
        case DLLTYPE_ARGC:
            argval = uint_tostr(nval, ubuf);
            break;
        case DLLTYPE_ARGC_OUT:
            argval = uint_tostr(snip->nout, ubuf);
            break;
        case DLLTYPE_ARGC_IN:
            argval = uint_tostr(SNIPPET_NARGIN(snip), ubuf);
            break;
        default:       // type is handled below
            goto notransform;
        }
        ss_cat_mod(&buf, ss_crefs(argval), argname, argmod);
        continue;

 notransform:
        {       // untransformed value
            size_t arglen = 0;
            switch (arg->type) {        // choose argument value
            case DLLTYPE_M:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, '%', arglen);
                break;
            case DLLTYPE_B:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, '\\', arglen);
                break;
            case DLLTYPE_D:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, '$', arglen);
                break;
            case DLLTYPE_N:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, '\n', arglen);
                break;
            case DLLTYPE_S:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, ' ', arglen);
                break;
            case DLLTYPE_T:
                arglen = (0 == rep) ? 1 : rep;
                argval = fillbuf_rep(&srep, '\t', arglen);
                break;
            case DLLTYPE_U:
                argval = fillbuf_utf8(&srep, rep);
                arglen = ss_len(srep);
                break;
            case DLLTYPE_X:
                argval = (rep < UCHAR_MAX) ? fillbuf_rep(&srep, (unsigned char)rep, 1) : "";
                arglen = 1;
                break;
            case DLLTYPE_COMMA:
                argval = (rep < nval && NULL != values[rep] && '\0' != values[rep][0]) ? "," : "";
                arglen = (',' == argval[0]) ? 1 : 0;
                break;
            default:   // type is handled below
                goto transform_multi;
            }
            /* append untransformed argument value */
            ss_cat_cn(&buf, argval, arglen);
            continue;
        }

 transform_multi:
        {       // multiple transformed values
            size_t lo, hi;      // list bounds
            bool rev = false;   // reverse order?
            const char **const vals = values;
            switch (arg->type) {        // choose argument value
            case DLLTYPE_ARGV:
                lo = rep;
                hi = nval;
                break;
            case DLLTYPE_ARGV_OUT:
                lo = rep;
                hi = snip->nout;
                break;
            case DLLTYPE_ARGV_IN:
                lo = snip->nout + rep;
                hi = snip->nargs;
                break;
            case DLLTYPE_RARGV:
                lo = rep;
                hi = nval;
                rev = true;
                break;
            case DLLTYPE_RARGV_OUT:
                lo = rep;
                hi = snip->nout;
                rev = true;
                break;
            case DLLTYPE_RARGV_IN:
                lo = snip->nout + rep;
                hi = snip->nargs;
                rev = true;
                break;
            default:
                continue;       // type was handled above
            }
            /* append transformed argument values */
            for (size_t j = lo; j < hi; ++j) {
                size_t ix = rev ? hi - (j + 1) : j;
                argname = (ix < nfarg) ? VNARG_GETNAME(snip->sv_forl, ix) : NULL;
                const char *argval = (vals[ix] == argname) ? "" : vals[ix];     // SENTINEL for optional argument
                if (j == hi - 1)
                    argmod.trail = TRAIL_NONE;  // argument lists do not trail
                ss_cat_mod(&buf, ss_crefs(argval), argname, argmod);
            }
        }
#undef rep
#undef num
    }
    ss_cat_substr(&buf, snip->body, offset, ss_len(snip->body) - offset);       // append rest of snippet body
    /* already NUL-terminated from parser non-terminal snippet_body */

    const size_t nbuf = ss_len(buf);    // NOTE: counting NUL
    (res) = rmalloc((nbuf) * sizeof(*res));
    if (NULL == (res)) {
        goto cleanup;
    }

    memcpy(res, ss_get_buffer_r(buf), nbuf);
    *out_len = nbuf;

 cleanup:
    ss_free(&srep);
    ss_free(&buf);

    return res;
}

static int retrieve_arglink_value(const struct namarg *farg, const vec_namarg *sv_forl,
                                  const char **values, srt_string **sexpr)
{
    assert(farg->type == ENUM_NAMARG(ARGLINK));
    size_t ixref = CMOD_ARG_ARGLINK_get(*farg);
    const struct namarg *farg_linked = sv_at(sv_forl, ixref);
    if (farg_linked->type == ENUM_NAMARG(ARGLINK) && values[ixref] == farg_linked->name) {      // SENTINEL for unresolved arglink
        retrieve_arglink_value(farg_linked, sv_forl, values, sexpr);    // recurse
    }
    else if (farg_linked->type == ENUM_NAMARG(EXPR) && values[ixref] == farg_linked->name) {    // SENTINEL for unresolved expression
        for (size_t i = 0; i < sv_len(CMOD_ARG_EXPR_get(*farg_linked)); ++i) {
            const struct namarg *earg = sv_at(CMOD_ARG_EXPR_get(*farg_linked), i);
            assert(earg->type != ENUM_NAMARG(ROWNO));   // already checked in parser
            if (earg->type == ENUM_NAMARG(ARGLINK)) {
                retrieve_arglink_value(earg, sv_forl, values, sexpr);   // recurse
            }
            else
                ss_cat_c(sexpr, CMOD_ARG_VERBATIM_get(*earg));
        }
    }
    else
        ss_cat_c(sexpr, values[ixref]);
    return 0;
}

int snippet_subst(const struct snippet *snip, const vec_namarg *arglist, char *buf[static 1],
                  size_t buflen[static 1])
{
    int rc = 0;

    const char **values = NULL;
    srt_string **sexpr = NULL;

    const size_t nval = SNIPPET_HAS_FORMAL_ARGS(snip)
        ? snip->nargs : sv_len(arglist);
    (values) = rcalloc((nval), sizeof(*values));
    if (NULL == (values)) {
        rc = 1;
        goto cleanup;
    }
    ;
    (sexpr) = rcalloc((nval), sizeof(*sexpr));
    if (NULL == (sexpr)) {
        rc = 1;
        goto cleanup;
    }
    ;

    /* setup default values */
    {
        const size_t _len = sv_len(snip->sv_forl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _7 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            switch (((const struct namarg *)sv_at(snip->sv_forl, _7))->type) {
            case ENUM_NAMARG(NOVALUE):
                values[_7] = NULL;
                break;
            case ENUM_NAMARG(VERBATIM):
                values[_7] =
                    CMOD_ARG_VERBATIM_get(*((const struct namarg *)sv_at(snip->sv_forl, _7)));
                break;
            case ENUM_NAMARG(INTSTR):
                values[_7] =
                    CMOD_ARG_INTSTR_get(*((const struct namarg *)sv_at(snip->sv_forl, _7)));
                break;
            case ENUM_NAMARG(OPTIONAL):
                values[_7] = ((const struct namarg *)sv_at(snip->sv_forl, _7))->name;
                break;
            case ENUM_NAMARG(EXPR):
                values[_7] = ((const struct namarg *)sv_at(snip->sv_forl, _7))->name;
                break;
            case ENUM_NAMARG(ARGLINK):
                values[_7] = ((const struct namarg *)sv_at(snip->sv_forl, _7))->name;   // SENTINEL for unresolved arglink
                break;
            default:   // already checked in parser
                assert(0 && "C% internal error" "");
                break;
            }
        }
    }

    /* set actual arguments */
    {
        const size_t _len = sv_len(arglist);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _8 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (((const struct namarg *)sv_at(arglist, _8))->type == ENUM_NAMARG(NOVALUE))
                continue;
            assert(((const struct namarg *)sv_at(arglist, _8))->type == ENUM_NAMARG(VERBATIM)); // already converted
            values[_8] = CMOD_ARG_VERBATIM_get(*((const struct namarg *)sv_at(arglist, _8)));   // const borrow
        }
    }

    /* compute dynamic values */
    {
        const size_t _len = sv_len(snip->sv_forl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _9 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (((const struct namarg *)sv_at(snip->sv_forl, _9))->type != ENUM_NAMARG(ARGLINK) // arglink OK
                && ((const struct namarg *)sv_at(snip->sv_forl, _9))->type != ENUM_NAMARG(EXPR))        // expression OK
                continue;
            if (values[_9] != ((const struct namarg *)sv_at(snip->sv_forl, _9))->name)
                continue;       // not SENTINEL => already has value
            srt_string *(strval) = ss_alloc(8);
            if (ss_void == (strval)) {
                return 1;
            }

            if (retrieve_arglink_value(&CMOD_ARG_ARGLINK_set(_9), snip->sv_forl, values, &strval)) {
                rc = 1;
                goto cleanup;   // ERROR: out of memory
            }
            ss_cat_cn1(&strval, '\0');  // NUL-terminate
            values[_9] = ss_get_buffer_r(strval);       // const borrow
            sexpr[_9] = strval; // keep reference for freeing
        }
    }

    /* snippet body with substitutions */
    *buf = snip_get_buf(snip, nval, values, buflen);

 cleanup:
    {
        free(values);
        (values) = NULL;
    }   // unborrow
    for (size_t i = 0; i < nval; ++i)
        ss_free(&sexpr[i]);
    {
        free(sexpr);
        (sexpr) = NULL;
    }

    return rc;
}
