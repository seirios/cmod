#ifndef TABLE_TSV_h_
#define TABLE_TSV_h_

#include "cmod.ext.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

enum tsv_parse {
    TSV_PARSE_OK = 0,   /* default */
    TSV_PARSE_OOM = 1,  /* out of memory */
    TSV_PARSE_BADROW = 2,       /* incorrect row size */
    TSV_PARSE_NDEFAULT = 3,     /* bad number of non-default columns */
    TSV_PARSE_INIT = 4, /* failed initializing scanner */
};
#define enum_tsv_parse_len 4

int table_tsv_parse(const char *tsv, size_t tsvlen, size_t ncol_o[static 1],
                    vec_vec_string * rows_o[static 1]);

#endif
