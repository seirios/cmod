void ( * ( bsd_signal ) ( int y  , void ( * fun ) ( int x  )  ) ) ( int  ) ;
float * ( * ( * ( ptr ) ) ( int i  ) ) ( double * * x  , char c  ) ;
unsigned * * ( * ( * ( ptr2 ) ) [ 5 ] ) ( char const * s  , int * d  ) ;
void ( * ( ptr3 ) ( enum op { OP_ADD , OP_SUB } xop  , void ( * fun ) ( int x  )  ) ) ( struct mine { float a ; float b ; } s  ) ;

struct myfun__args {
int _;
enum op { OP_ADD , OP_SUB } xop ;
void ( * fun ) ( int x  ) ;
};
void ( * ( myfun__ ) ( struct myfun__args argv ) ) ( struct mine { float a ; float b ; } s  ) ;
#define myfun(...) myfun__((struct myfun__args){ ._=0, __VA_ARGS__ })
void ( * myfun__ ( struct myfun__args argv ) ) ( struct mine { float a ; float b ; } s  ) {
}
struct myfun2__args {
int _;
enum op { OP_ADD , OP_SUB } xop ;
void ( * fun ) ( int x  ) ;
};
int ( * ( myfun2__ ) ) ( struct myfun2__args argv ) ;
#define myfun2(...) myfun2__((struct myfun2__args){ ._=0, __VA_ARGS__ })
