typedef int my_int ;
my_int ( test ) ( int x  ) ;


typedef int ( my_type ) ( int  ) ;
my_type my_func;

int ( my_func_2 ) ( const char * str  , long siz  ) ;

struct my_named_type__args {
int _;
int x ;
};
typedef int ( my_named_type ) ( struct my_named_type__args argv ) ;
my_named_type my_named_func;
#define my_named_func(...) my_named_func__((struct my_named_type__args){ ._=0, __VA_ARGS__ })

struct my_named_func_2__args {
int _;
const char * str ;
long siz ;
};
int ( my_named_func_2__ ) ( struct my_named_func_2__args argv ) ;
#define my_named_func_2(...) my_named_func_2__((struct my_named_func_2__args){ ._=0, __VA_ARGS__ })
