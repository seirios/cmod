






#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct sqmatrix {
    size_t dim;
    size_t nelem;
    double *dat;
};

int main(void) {
    struct sqmatrix A, B, C;

A = (struct sqmatrix){
    .dim = 2,
    .nelem = 2 * 2,
    .dat = malloc((2 * 2) * sizeof(double)),
};

B = (struct sqmatrix){
    .dim = 2,
    .nelem = 2 * 2,
    .dat = malloc((2 * 2) * sizeof(double)),
};

C = (struct sqmatrix){
    .dim = 2,
    .nelem = 2 * 2,
    .dat = malloc((2 * 2) * sizeof(double)),
};

memset(A.dat,0x0,A.dim * A.dim * sizeof(A.dat));

for(size_t _i = 0; _i  < B.nelem; ++_i)
    B.dat[_i] = 1.2345;

memset(C.dat,0x0,C.dim * C.dim * sizeof(C.dat));

assert(A.nelem == C.nelem);
for(size_t _i = 0; _i  < A.nelem; ++_i)
    C.dat[_i] += A.dat[_i];

assert(B.nelem == C.nelem);
for(size_t _i = 0; _i  < B.nelem; ++_i)
    C.dat[_i] += B.dat[_i];

for(size_t _i = 0; _i  < C.dim; ++_i) {
    for(size_t _j = 0; _j  < C.dim; ++_j)
        printf("%g ",C.dat[_j + _i * C.dim]);
    putchar('\n');
}

}
