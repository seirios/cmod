#ifndef VALIDATE_UTF8_H_
#define VALIDATE_UTF8_H_

#include <stddef.h>
#include <stdbool.h>

bool is_valid_utf8(const unsigned char *buf, int len);

#endif
