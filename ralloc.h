
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#ifndef CMOD_RALLOC_H
#define CMOD_RALLOC_H

struct cmod_ralloc_params {
    volatile sig_atomic_t giveup;
    bool verbose;
    int ntimes;
    int msleep;
};

extern struct cmod_ralloc_params ralloc_glob;
__attribute__((malloc, warn_unused_result))
void *(rmalloc) (size_t size);
__attribute__((malloc, warn_unused_result))
void *(rcalloc) (size_t nmemb, size_t size);
__attribute__((warn_unused_result))
void *(rrealloc) (void *ptr, size_t size);
__attribute__((malloc, nonnull((1))))
char *(rstrdup) (const char *s);

extern const struct ret_f0 rmalloc_ret_f;
extern const struct ret_f0 rcalloc_ret_f;
extern const struct ret_f0 rrealloc_ret_f;
extern const struct ret_f0 rstrdup_ret_f;

#endif
