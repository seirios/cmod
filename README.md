![cmod](img/cmod_banner.png)
# TL;DR

C% (pronounced "c mod") is an experimental meta-programming language. The main goal of C% is to bring meta-programming to the C programming language, while keeping true to its spirit: *trust the programmer* and *don't prevent the programmer from doing what needs to be done*.

Keywords in C% begin with a **%** sign and are usually embedded into C code, where they are evaluated in-place without interfering with the surrounding code. There are currently 21 keywords that can be used for generic code generation and 11 keywords with semantics specific to C, as well as comment keywords. There is also a C% standard library with constructs intended for inclusion into C programs.

C% has two main types of objects: parameterized code snippets (**%snippet**) and data tables (**%table**, in TSV or JSON format). Snippets can be expanded in-place with arguments (**%recall**) and tables can have parameterized code mapped to them (**%map**). These two objects allow for reusability of code and data within and across compilation units. All generated code is present in the output, to allow further inspection by the programmer and the compiler.

Other notable keywords allow defining and evaluating compile-time domain-specific languages (**%dsl-def** and **%dsl**) or performing I/O with arbitrary shell commands (**%pipe**), enabling dynamic multi-language meta-programming. C% can also be used for more general automated code generation, e.g. Python code, etc.

Currently, the main use of C% is in developing its own intepreter `cmod` and other projects of mine, but I hope that you may find it useful for your own projects too!

# C%: cmod

C%: cmod (from "C with mods") is an experimental C meta-programming language and pre-processor (C% is the language and `cmod` is the pre-processor).

The aim of C% is to extend the C language via constructs that allow for faster coding, hopefully without becoming too cryptic. Currently the pre-processor is implemented as a flex/bison parser that recognizes C% keywords and generates C code. While parsing, anything that is not a C% keyword is passed through, and the final output contains no C% keywords. This means C code is not parsed (except inside C% keywords specific to C), and all generated C code is present in the output, which makes it open for inspection and is subject to validation by the C compiler.

Conventional file extensions are `.cm` for code and `.hm` for headers. The usual workflow is for `.c` or `.h` files to be generated from the corresponding `.cm` or `.hm` files with `cmod` (as in the included makefile), and then run through the C compiler. C%-only `.hm` headers containing static data tables and code snippets are also common, as are static data tables stored in TSV or JSON formats.

Beyond C meta-programming, C% generic keywords can be used with other languages such as Python, Flex / Bison specification languages, etc. Given the byte-perfect nature of the parser, all spacing is preserved and C% keywords do not introduce padding or otherwise spurious characters.

For real-world examples look under `src` for the source code of `cmod` itself (that's right, `cmod` is self-hosted!). After every source code update, `cmod` has to re-build itself with strict warning flags under memcheck, to ensure everything still works! The C% unit tests under `tests` may also serve as examples.

The C% standard library contains convenience constructs for inclusion in C programs.

Additionally, a VIM syntax highlighting file is provided to ease coding in C%.

## Rationale

I love programming in C![^1] It's a small and elegant language that lets you do whatever you want with the computer's resources and the information stored within, down to the very last bit.[^2]

Having so much freedom and power, however, comes at the cost of having to build all the structure yourself from the ground up. This tends to be a bit verbose and tiresome, so my goal with C% is to reduce this tedium and make coding in C more efficient and fun!

Some of the things that C% aims to make achievable are:

+ **Reusability**: Avoid source code duplication.
+ **Consistency**: Use the same data across different locations.
+ **Efficiency**: Perform common tasks quick and easy.
+ **Concision**: Write and work with concise code.
+ **Expressivity**: Better express the intent of code.
+ **Transparency**: Hide nothing from the programmer.
+ **Abstraction**: Handle similar things in a uniform manner.
+ **Extensibility**: Easily add new functionality.
+ **Simplicity**: Keep the language simple but powerful.

All while keeping true to the spirit of C, *trust the programmer* and *don't prevent the programmer from doing what needs to be done*!

Of course, the drawback is to have an additional processing layer in yet another language, which may over-complicate things and become an additional source of bugs... or maybe simplify things and reduce the potential for errors!

In any case, I find C% useful and enjoyable, and I hope you do too!

[^1]: C99 to be precise (although C23 looks promising!)

[^2]: As long as the compiler actually does what you expect, and the kernel can actually manage those resources and provides a way to do so, but well...

## Compilation

Requirements are lean and ubiquitous:

+ POSIX-compatible OS: GNU/Linux, macOS, Cygwin, BSD (untested)
+ C99 compiler: `gcc` or `clang`
+ GNU Make

Optional requirements:

+ [libseccomp](https://github.com/seccomp/libseccomp) (encouraged for safety)
+ [Criterion](https://github.com/Snaipe/Criterion) (unit tests only)
+ [Flex](https://github.com/westes/flex) v2.6.4 (maintainer only)
+ [Bison](https://www.gnu.org/software/bison/) v3.7+ (maintainer only)

To compile, Just Run Make™!

If things don't go well, please check that your system meets the requirements above. Please also check that the command paths inside `Makefile` and the configurable options inside `config.mk` (auto-detected) are appropriate for your system.

If you need further assistance, please [create an issue](https://gitlab.com/seirios/cmod/-/issues).

## Examples

Some initial motivating examples. Many more in the documentation below.

### Define multiple functions with the same signature (function type)

When writing a GUI, a callback must be written for each object (button, menu, etc.). These callbacks all have the same signature, and it becomes tedious to write (or even look at) them. Of course, this can be alleviated by using some sort of IDE, but that misses the point, which is to express the fact that all these functions are of the same type.

This can be achieved in C% is by using the `%typedef` and `%def` keywords:

    %typedef OBJECT;
    %typedef void CALLBACK_F(OBJECT *obj, long data);
    
    %def CALLBACK_F btn_callback {
        /* use obj and data (we already know these are the parameters!) */
    }
    
    %def CALLBACK_F menu_callback {
        /* use obj and data, again */
    }

    /* etc. */

### Define a function with named parameters

C does not support named parameters, but it's a well-known trick to wrap them in a struct and use designated initializers to name them. Of course, this has its pitfalls, but if you want to do it, you *should* be able to do it, easily.

In C% we can do the following (full example):

    #include <stdio.h>
    #include <stdlib.h>
    
    /* type of a qsort-compatible comparison function */
    %typedef int cmp_f(const void *keyval, const void *datum);
    /* in case you are like me and always forget the order of these arguments... */
    %proto [named] void qsort_n(void *base, size_t nmemb, size_t size, cmp_f *compar);
    
    %def qsort_n {
        /* argv contains the function argument values */
        return qsort(argv.base, argv.nmemb, argv.size, argv.compar);
    }
    
    %def cmp_f cmp_int {
        /* we already know the parameter names from the typedef */
        int a = *(int*)keyval;
        int b = *(int*)datum;
        return a < b ? -1 : (a > b ? 1 : 0);
    }
    
    int main(void) {
        int array[] = { 7, 3, 5, 1 };
        /* we provide the named arguments in any order */
        qsort_n(.compar = cmp_int,
                .size = sizeof(*array),
                .nmemb = %arrlen(array),
                .base = array);
        %foreach elem (array) %{
            printf("%d\n",${elem});
        %}
    }

## Known issues and limitations

+ Nested expressions are not supported, requiring explicit delayed evaluation of outer keywords.
+ Types not built-in must be manually registered  with `%typedef` for C declaration parsing to work.
+ No proper multi-byte support, but scanner is transparent to valid UTF-8 and verbatim input is checked.
+ No support of `%pipe` under Cygwin (some forking issue) and sporadic failures under `make -j`.
+ C% keywords are recognized inside C string literals, on purpose. String splitting may be required to avoid unwanted behaviors, e.g., `"%""#"`.
+ Only UNIX-style newlines (line feed) are supported.

## Some future features

+ Use PCRE2 for better Unicode support in regex.
+ Support nested keyword expansion inside argument lists.
+ Expand C% standard library (e.g. key-value stores, etc.).

## Acknowledgements

+ libsrt: safe real-time library for C (original by [faragon](https://github.com/faragon/libsrt))
+ JSMN: minimalistic JSON parser in C (original by [zserge](https://github.com/zserge/jsmn), strict-parsing version by [dominickpastore](https://github.com/dominickpastore/jsmn/tree/strict-parsing))
+ [utf8](https://github.com/cyb70289/utf8): fast UTF-8 validation with range algorithm

## Social

+ Check us out at FOSDEM'22! [C meta-programming for the masses with C%: cmod](https://fosdem.org/2022/schedule/event/lt_cmod)

## Donations

If you like this project and would like to further its development, you may donate crypto:

+ BTC: `3LGnjqEdpsaUJ7GtewDzadW9tbVgm6m99W`
+ ETH: `0xe5F008C2aD245BEBBb9820ed76DD00fe096D53Ec`
+ BCH: `bitcoincash:qz5uem9f64vp0wh5drvgypdhkucx2r40uv8xc52m7l`
+ BNB: `0xe5F008C2aD245BEBBb9820ed76DD00fe096D53Ec`

## Unfrequently asked questions

### Is C% useful at all?

It's useful enough to write its own pre-processor and make its own development easier. It's also useful for some of my other projects. I'd love that it became useful for other people's projects in the near future :)

### Won't this make my code too long or obscure?

Not necessarily. Extensive use of `%recall` / `%map` or the C% standard library may result in lots of generated code. Unless that's a concern (e.g. embedded systems), the benefits to the programmer outweight the increase in generated code length and executable size. Regarding code clarity, it depends on the complexity of the meta-program's logic, and finding the right amount of automatization is up to you; using multiple steps or pre-generating some parts may help in keeping the code clear.

### Why not use CPP macros or C functions instead of C% snippets?

Unlike snippets, pre-processor macros are ugly (all those continuation slashes), do not allow argument transformation (e.g. wrap, case fold, etc.) and cannot define other macros. Snippets would be used at least whenever a macro is preferable to a function, that is, to avoid the cost of a function call, to operate on variables disregarding type, to mix in code into a function's scope, etc.

### Can I use C% with languages other than C?

Sure you can! By passing the `-g/--generic` flag to `cmod`, C-specific keywords are disabled and you can meta-program whatever language you like where C% keywords don't have a special meaning. I've used C% successfully with Flex/Bison definition languages and with Python (where whitespace preservation comes in handy).

### Isn't `%pipe` too unsafe to be good?

Currently, `%pipe` can execute arbitrary commands without restrictions only if passed the `[unsafe]` option, and is a potential security liability if left unchecked (as much as running any shell script of unknown origin). *Always check commands before running them on your machine!* However, to make `%pipe` safer, syscall filtering with `libseccomp` has been implemented, as well as an execution timeout to avoid hangups. Notwithstanding these issues, being able to write a multi-language script that generates code is a pretty cool feature!

### What's up with the versioning scheme?

This is an experimental project, so the latest version is always the best version :p Having said that, I try to use semantic versioning but do so sloppily. If there start being other users, I will become more careful with breaking changes and such, even perhaps introduce required versions in the code.

### Shouldn't C% or `cmod` do these \*\*\* things differently?

By all means, please explain and/or propose a better way by [creating an issue](https://gitlab.com/seirios/cmod/-/issues)!

## Documentation

Below is the full documentation (automatically generated):

+ `cmod` usage (`cmod --help`)
+ C% keyword summary (`cmod --docs`)
+ C% language reference with examples (`cmod --docs-full`)
+ C% standard library reference (`cmod --docs-stdlib`)

## cmod usage
      ____  _  __                                  _ 
     / ___|(_)/ /  _    ___  _ __ ___    ___    __| |
    | |      / /  (_)  / __|| '_ ` _ \  / _ \  / _` |
    | |___  / /_   _  | (__ | | | | | || (_) || (_| |
     \____|/_/(_) (_)  \___||_| |_| |_| \___/  \__,_|
    =================================================
      EXPERIMENTAL C META-PROGRAMMING PRE-PROCESSOR  
    =================================================
    v6.9.3 (962) built on 2024-12-29 under Linux x86_64
    
    Usage: cmod [options] [-o <output>] [<inputs>]
    
    Available parameters: (* = required) (+ = multiple) [default]
          -D, --debug                    +  : parser debugging level [0]
          -d, --disable             <arg>+  : disable syntax element: <keyword>, all, none, comments, shorthands or idcap
          -e, --enable              <arg>+  : enable syntax element: <keyword>, all, none, comments, shorthands or idcap
          -l, --eval-limit          <arg>   : parser re-evaluation limit [100]
          -g, --generic                     : disable C-specific keywords [false]
          -I, --include             <arg>+  : add directory to include search path [/usr/include]
          -L, --iter-limit          <arg>   : iteration limit [1000]
          -k, --keep                        : keep temporary files [false]
          -n, --no-clobber                  : do not overwrite existing output file [false]
          -o, --output              <arg>   : output filename
          -q, --quiet                    +  : parser logging suppression level [0]
          -s, --step                <arg>   : perform up to this number of parsing steps [-1]
          -t, --tests-output        <arg>   : output filename for unit tests
          -T, --tmpdir              <arg>   : directory for temporary files [/tmp]
              --color                       : pretty-print logging output
              --disable-seccomp             : disable seccomp syscall filtering
              --docs                        : print C% keyword summary
              --docs-full                   : print full C% documentation
              --docs-kw             <arg>   : print syntax summary for a single C% keyword
              --docs-kw-full        <arg>   : print full documentation for a single C% keyword
              --docs-stdlib                 : print standard library documentation
              --exit-on-first-error         : exit on first error
              --help                        : print usage information
              --no-blanks                   : suppress blank lines in output
              --no-builtin-c                : no built-in standard C types
              --no-builtin-posix            : no built-in standard POSIX types
              --no-color                    : do not pretty-print logging output
              --report-dsls                 : report parsed domain-specific languages
              --report-enums                : report parsed enum constants
              --report-fdefs                : report parsed function definitions
              --report-onces                : report parsed include/repeat guards
              --report-protos               : report parsed function prototypes
              --report-snippets             : report parsed snippets
              --report-tables               : report parsed tables
              --report-types                : report parsed type definitions
              --scanner-disable             : disable keywords in scanner
          -q, --silent                      : alias for --quiet
              --usage                       : alias for --help
              --verbose                     : print diagnostic messages
              --version                     : print version



## C% keyword summary #

### Comment keywords ##

------------------------------------------------------------------------------
+ `%comment`         : comment until the end of a line
------------------------------------------------------------------------------
+ `%//`              : comment until the end of a line
------------------------------------------------------------------------------
+ `%#`               : block-comment until next `%#`

### Generic keywords (statement-like, at beginning of line) ##

------------------------------------------------------------------------------
+ `%include`         : include a source file


### Generic keywords (statement-like) ##

------------------------------------------------------------------------------
+ `%once`            : set an include/repeat guard
------------------------------------------------------------------------------
+ `%snippet`         : define a parameterized code snippet
------------------------------------------------------------------------------
+ `%recall`          : insert a snippet in-place with argument substitution
------------------------------------------------------------------------------
+ `%table`           : define a data table (TSV or JSON format)
------------------------------------------------------------------------------
+ `%table-stack`     : stack data tables row- or column-wise
------------------------------------------------------------------------------
+ `%map`             : map a snippet or lambda to a table
------------------------------------------------------------------------------
+ `%pipe`            : pipe text to external command and capture output
------------------------------------------------------------------------------
+ `%unittest`        : define a unit test
------------------------------------------------------------------------------
+ `%dsl-def`         : define a domain-specific language
------------------------------------------------------------------------------
+ `%dsl`             : evaluate a domain-specific language


### Generic keywords (function-like) ##

------------------------------------------------------------------------------
+ `%intop`           : perform arithmetic with integers (bounds-checked)
------------------------------------------------------------------------------
+ `%delay`           : print text after a delay (number of parse passes)
------------------------------------------------------------------------------
+ `%defined`         : print text conditional on resource being defined
------------------------------------------------------------------------------
+ `%strcmp`          : print text conditional on string comparison result
------------------------------------------------------------------------------
+ `%strstr`          : count substring occurrences
------------------------------------------------------------------------------
+ `%strlen`          : get static string length
------------------------------------------------------------------------------
+ `%strsub`          : perform string substitution
------------------------------------------------------------------------------
+ `%table-size`      : get number of rows/columns in table(s)
------------------------------------------------------------------------------
+ `%table-length`    : compute min/max string length across values in table(s)
------------------------------------------------------------------------------
+ `%table-get`       : retrieve values (or their indices) from table(s)


### C-specific keywords (statement-like) ##

------------------------------------------------------------------------------
+ `%typedef`         : declare type
------------------------------------------------------------------------------
+ `%proto`           : declare function prototype
------------------------------------------------------------------------------
+ `%def`             : define function
------------------------------------------------------------------------------
+ `%unused`          : silence unused variable warning
------------------------------------------------------------------------------
+ `%prefix`          : set prefix for functions and enums
------------------------------------------------------------------------------
+ `%enum`            : define enum with optional helper functions
------------------------------------------------------------------------------
+ `%strin`           : check if string belongs to word list
------------------------------------------------------------------------------
+ `%foreach`         : iterate over array of known size
------------------------------------------------------------------------------
+ `%switch`          : select cases over (structured) variables


### C-specific keywords (function-like) ##

------------------------------------------------------------------------------
+ `%free`            : free and annul a list of pointers
------------------------------------------------------------------------------
+ `%arrlen`          : get length of static array





## General C% syntax #

C% keywords take as arguments identifiers or verbatim text.

Identifiers are of three types:

 + Regular C identifiers. For example: `_Id3nt1f1er`.
 + Extended identifiers. Allow additional characters `!@&` in the first position, `-+.?!@&:` in subsequent positions, and optionally end with a slash and a number. For example: `@is_defined?`, `fun/1`, `fun:mine/3`.
 + String identifiers. Enclosed in backticks, without newlines. For example `` `a nice and long name` ``.

Verbatim values may appear:

 + Enclosed in backticks, without newlines. The backtick is escaped with a double backtick.
 + As a herestring, enclosed in `%<<` and `>>%`.
 + As a tagged herestring, enclosed in `%<<ID` and `ID>>%`, with the same identifier at open and close (can be nested).

Mod-braces `%{` and `%}` enclose snippet code or verbatim text, depending on the keyword. They eat a space after opening and before closing, allowing for prettier code, e.g., `%{ spaces %}` instead of `%{nospaces%}`. Inside snippet code special syntax applies, please refer to the documentation for the `%snippet` keyword.

Special tagged mod-braces `%tsv{` and `%json{` enclose table definitions in either TSV or JSON format. Inside these special syntax applies, please refer to the documentation for the `%table` keyword.

## C% keyword syntax #

### Comment keywords ##

------------------------------------------------------------------------------
+ `%comment`         : comment until the end of a line
------------------------------------------------------------------------------
+ `%//`              : comment until the end of a line
------------------------------------------------------------------------------
+ `%#`               : block-comment until next `%#`

Note: C% comments at the beginning of a line do not produce a newline on the output.


### Generic keywords (statement-like, at beginning of line) ##

------------------------------------------------------------------------------
+ `%include`         : include a source file

  Options:

         blank    <bool> : keep blank lines
             c    <bool> : print C include directive only
         debug += <uint> : verbose logging of keyword invocation
           opt    <bool> : skip inclusion if file not available
         quiet    <bool> : completely suppress output
           sys    <bool> : search only in system include search path
      

  + **Rule 0**: include file from current directory or system include search path
        
        %include [options] "<path>" 
        

    *Example:*
      
        %include "keywords/include.cm"
      
    *Evaluates to:*
        
         
        


### Generic keywords (statement-like) ##

------------------------------------------------------------------------------
+ `%once`            : set an include/repeat guard

  Options:

         debug += <uint> : verbose logging of keyword invocation
        noskip    <bool> : do not skip rest of file
      

  + **Rule 0**: set include/repeat guard
        
        %once [options] <guard> 
        

    *Example:*
      
        %once include_guard
      
    *Evaluates to:*
        
         
        

------------------------------------------------------------------------------
+ `%snippet`         : define a parameterized code snippet

  Options:

        append    <bool> : append to existing snippet
         debug += <uint> : verbose logging of keyword invocation
          here    <bool> : immediate recall of snippet at place of definition
         nl2sp    <bool> : convert newlines to spaces in snippet body
           now    <bool> : immediate evaluation of snippet body
           opt    <bool> : skip definition if snippet already exists
         redef    <bool> : set snippet as redefinable
      

  + **Rule 0**: create empty snippet
        
        %snippet [options] <name> [ [ (<output arguments>) <- ] (<input arguments>) ] 
        %*       [options] <name> [ [ (<output arguments>) <- ] (<input arguments>) ] 
        

    *Example:*
      
        %snippet blank
      
    *Evaluates to:*
        
         
        

  + **Rule 1**: assign explicit snippet
        
        %snippet [options] <name> = or ?= or := or += <snippet-like>
        %*       [options] <name> = or ?= or := or += <snippet-like>
        

    *Example:*
      
        %snippet splitargs = (out) <- (in1,in2) %{
            ${out} = ${in1} + ${in2}
        %}
        %recall splitargs (a) <- (2,3)
        
        %snippet my_insert = (array) <- (val, index) %{
            ${array}[${index}] = ${val};
        %}
        %snippet my_insert_zero = my_insert (index = 0)
        %recall my_insert_zero (A) <- (5)
        
      
    *Evaluates to:*
        
            a = 2 + 3
        
            A[0] = 5;
        
        

  + **Rule 2**: load snippet from file
        
        %snippet [options] <snippet name> "<path>" 
        %*       [options] <snippet name> "<path>" 
        

    *Example:*
      
        %snippet my_load_file "tests/0189_snippet_file.snippet"
      
    *Evaluates to:*
        
         
        

  Notes:
      
        + A <snippet-like> has one of these forms:
        
            1. Explicit body
               [[( <output parameter list> ) <-] ( <input parameter list> )] %{ <code> %}
        
            2. Specialize already-defined snippet
               <parent snippet> [[( <output parameter list> ) <-] ( <input parameter list> )]
        
        + Arguments in parameter lists are named and have one of these forms:
        
            1. Without default value
               <name>
        
            2. With default value
               <name> = #<linked name>
               <name> = <integer> or <identifier>
               <name> = `<verbatim string>`
        
        + The assignment operator work as follows:
        
            =   normal assignment
            ?=  assign if not already defined, equivalent to specifying [opt]
            :=  assign redefinable, equivalent to specifying [redef]
        
        + In a snippet body, the following escape sequences apply:
        
              %% => %, %%% => %%, etc. (linear scaling)
              $$ => $, $$$ => $$, etc. (linear scaling)
        
        + Redefinable snippets defined using the shorthand %* do not emit a warning on redefinition,
          so the typical syntax for temporary snippets is:
        
              %* my_tmp_snippet := %{ ... %}
      
        + Snippet code consists of verbatim text and argument references like:
        
              $[0-9]+                         : argument by position
              $<modifiers>{[0-9]+}            : argument by position
              $<modifiers>{<name>}            : argument by name
              $<modifiers>{#[0-9]+}           : argument name by position
              ${#[0-9]*M}                     : percent sign (repeated)
              ${#[0-9]*B}                     : backslash (repeated)
              ${#[0-9]*D}                     : dollar sign (repeated)
              ${#[0-9]*N}                     : newline (repeated)
              ${#[0-9]*S}                     : space (repeated)
              ${#[0-9]*T}                     : horizontal tab (repeated)
              ${#[0-9]*U}                     : UTF-8 Unicode character (passed as repeat)
              ${#[0-9]*X}                     : single byte (passed as repeat)
              ${#[0-9]*COMMA}                 : comma if Nth argument is not empty (passed as repeat)
              $<modifiers>{#NAME}             : snippet name or table name in lambdas
              $<modifiers>{#NR}               : row number in lambdas
              $<modifiers>{#ARGC}             : total number of arguments
              $<modifiers>{#ARGC_OUT}         : number of output arguments
              $<modifiers>{#ARGC_IN}          : number of input arguments
              $<modifiers>{#[0-9]*ARGV}       : full argument list (starting at repeat)
              $<modifiers>{#[0-9]*ARGV_OUT}   : output argument list (starting at repeat)
              $<modifiers>{#[0-9]*ARGV_IN}    : input argument list (starting at repeat)
              $<modifiers>{#[0-9]*RARGV}      : full argument list in reverse order (starting at repeat)
              $<modifiers>{#[0-9]*RARGV_OUT}  : output argument list in reverse order (starting at repeat)
              $<modifiers>{#[0-9]*RARGV_IN}   : input argument list in reverse order (starting at repeat)
        
        + Where <modifiers> is a set of characters from the following groups (applied in order):
        
              (stacking effects)
              T : Trim whitespace (both ends)
              _ : Trim last character
              n : Trim newlines
              
              (non-stacking effects, last one wins)
              E : Do not apply modifications if empty
              C : Transform into valid C identifier (with underscores and '*' as 'p')
              e : Escape for snippet (percent and dollar sign)
              g : Escape as C string (standard escapes)
              j : Escape as JSON string
              x : Convert to hexadecimal
              X : Convert to base64
              l : Convert to hexadecimal LZ (high-compression)
              
              (non-stacking effects, last one wins)
              L : Fold to lowercase: example
              U : Fold to uppercase: EXAMPLE
              
              (non-stacking effects, last one wins)
              h : Prefix with hashtag: #example
              q : Prefix with backslash: \example
              d : Prefix with dollar sign: $example
              t : Prefix with period: .example
              m : Prefix with percent sign: %example
              a : Prefix with asterisk: *example
              r : Prefix with ampersand: &example
              
              (stacking effects)
              w : Wrap in spaces:  example 
              s : Wrap in single quotes: 'example'
              S : Wrap in double quotes: "example"
              P : Wrap in parentheses: (example)
              K : Wrap in square brackets: [example]
              A : Wrap in angled brackets: <example>
              B : Wrap in curly braces: {example}
              b : Wrap in backticks: `example`
              H : Wrap as herestring: %<< example >>%
              
              (stacking effects)
              p : Insert leading asterisk: *example
              R : Insert leading ampersand: &example
              D : Insert leading dollar sign: $example
              z : Insert leading argument name: <name>=example
              c : Insert leading comma: ,example
              
              (stacking effects)
              Z : Insert trailing comma: example,
              W : Insert trailing space: example 
              N : Insert trailing newline: example\n
              
------------------------------------------------------------------------------
+ `%recall`          : insert a snippet in-place with argument substitution

  Options:

         debug += <uint> : verbose logging of keyword invocation
         empty    <bool> : do not ignore empty snippet
          lazy    <bool> : delay evaluation until snippet is defined
           opt    <bool> : ignore undefined snippet
           str  = <str>  : apply string modifiers to keyword output
      

  + **Rule 0**: no arguments or input (and output) argument lists
        
        %recall [options] <snippet name> [[( <output argument list> ) ... <-] ( <input argument list> ) ...] 
        %|      [options] <snippet name> [[( <output argument list> ) ... <-] ( <input argument list> ) ...] |%
        

    *Example:*
      
        %snippet intdef = (var) <- (init) %{
            int ${var} = ${init};
        %}
        %recall intdef (x)(y) <- (12345)(67890)
      
    *Evaluates to:*
        
            int x = 12345;
            int y = 67890;
        

  Notes:
      
        + Each argument may be of the form:
        
              <identifier>
              <integer>
              `<verbatim value>`
              <name> = <identifier>
              <name> = <integer>
              <name> = `<verbatim value>`
------------------------------------------------------------------------------
+ `%table`           : define a data table (TSV or JSON format)

  Options:

        append  = <uint> : append to existing table (0 = rows, 1 = columns)
         debug += <uint> : verbose logging of keyword invocation
          hash  = <int> or #<str> : column name or index for hash map key
          json    <bool> : assume JSON file format
           opt    <bool> : skip definition if table already exists
         redef    <bool> : set table as redefinable
           tsv    <bool> : assume TSV file format
      

  + **Rule 0**: create empty table
        
        %table [options] <table name> (<column list>) 
        

    *Example:*
      
        %table emptytab (x,y)
      
    *Evaluates to:*
        
         
        

  + **Rule 1**: assign explicit table
        
        %table [options] <table name> = or ?= or := or += or |= [( <column list> )] %TSV{ or %JSON{ <table body> %} 
        

    *Example:*
      
        %table test_tsv = (name,index,description) %tsv{
            intro     12     start of file
            middle    165    header end
            endd      234    data end
        %}
        
        %table test_json = (name,index) %json{
        [
           [ "first", 1 ],
           [ "second", 2 ],
           [ "third", 3 ]
        ]
        %}
      
    *Evaluates to:*
        
        
        

  + **Rule 2**: load table(s) from file
        
        %table [options] [ <table name or prefix> ] "<path>" 
        

    *Example:*
      
        %table test_file_tsv "tests/0079_table_tsv.tsv"
        %table [json] "tests/0059_table_json.json"
      
    *Evaluates to:*
        
         
        

  Notes:
      
        + Each table column may be of the form:
        
              <column name>
              <column name> = <integer> (default value)
              <column name> = `<verbatim value>` (default value)
        
        + Comments start with # at the beginning of a line and until the end of line.
        
        + TSV-formatted tables allow multiple tabs as column separators,
          and ignore tabs at the beginning of a line, allowing for pretty formatting on file.
        
          The special sequence %nul represents a NUL character (empty value),
          and the escape sequence %% expands to %.
        
          When using implicit columns, the first row must contain valid column names (extended identifiers).
        
          When loading a table from a file, column names are read from the first row
          and values must separated by single tab characters.
        
        + JSON-formatted tables can be in one of two formats:
        
              array-of-arrays, where each array represents a row with values in column definition order
              array-of-objects, where each object represents a row,
                                and each key/value pair a cell for the given named column (unordered)
        
          When using implicit columns, either the first row must contain valid column names (array-of-arrays),
          or the column names are taken from the keys of the key/value pairs in each object,
          which must be the same across the whole table (array-of-objects).
        
          When loading tables from a file, it must consist of a single object
          having one table per key/value pair; the key becomes the table name,
          and the value is the table in either the array-of-arrays (with column names in first row)
          or the array-of-objects format.
------------------------------------------------------------------------------
+ `%table-stack`     : stack data tables row- or column-wise

  Options:

          cols    <bool> : stack columns
        common    <bool> : restrict to common columns or rows
         debug += <uint> : verbose logging of keyword invocation
             h    <bool> : alias for [cols]
          hash  = <int> or #<str> : column name or index for hash map key
          lazy    <bool> : delay evaluation until tables are defined
           opt    <bool> : ignore undefined tables
         redef    <bool> : set redefinable
        rename    <bool> : rename duplicate columns
          rows    <bool> : stack rows (default)
             v    <bool> : alias for [rows]
      

  + **Rule 0**: define a new table by stacking a list of tables
        
        %table-stack [options] <table name> ( <table name list> ) 
        %tabstack    [options] <table name> ( <table name list> ) 
        

    *Example:*
      
        %table two_by_two = (a,b) %tsv{
            a    1
            b    2
        %}
        %table two_by_one = (c) %tsv{
            foo
            bar
        %}
        %table-stack [h] two_by_three (two_by_two,two_by_one)
        %map two_by_three %{
        ${a}${c} = ${b}
        %}
      
    *Evaluates to:*
        
        afoo = 1
        bbar = 2
        

------------------------------------------------------------------------------
+ `%map`             : map a snippet or lambda to a table

  Options:

          add1 += <uint> : shift row numbers by this much
           brk  = <str>  : separator between output groups of rows
         debug += <uint> : verbose logging of keyword invocation
          head += <uint> : limit to this many rows from first
           inv    <bool> : invert row selection
          lazy    <bool> : delay evaluation until snippets and tables are defined
          nbrk  = <uint> : number of rows per output group
         nl2sp    <bool> : convert newlines to spaces in lambda snippet
        nohead += <uint> : skip this many rows from first
        notail += <uint> : skip this many rows from last
       notnull  = <int> or #<str> : column name or index for filtering non-empty rows
           num  = <uint> : sort by numeric value (in given base, 0 = floating-point)
           opt    <bool> : ignore undefined snippets and tables
           ord    <bool> : use ordered row numbers
           rev    <bool> : reverse sort direction
           sep  = <str>  : separator between output rows
          sort  = <int> or #<str> : column name or index for sorting by string value
           str  = <str>  : apply string modifiers to keyword output
          tail += <uint> : limit to this many rows from last
          uniq  = <int> or #<str> : column name or index for filtering unique rows
             x    <bool> : map cartesian product of columns (2 or 3)
      

  + **Rule 0**: map a table-like to a defined snippet
        
        %map [options] <table-like> | <snippet name> [[( <output arguments> ) <-] ( <input arguments> )] 
        

    *Example:*
      
        %table numbers_4 = (a,b,c) %tsv{
            1.1    0.2    5.2
            3.5    4.0    3.1
            6.2    4.3    7.5
        %}
        %table numbers_5 = (a,b,c) %tsv{
            0.7    5.2    3.4
        %}
        %snippet split_add_1 = (var) <- (x,y) %{
            x$C{var} = ${x} + ${y};
        %}
        %map numbers_4, numbers_5 | split_add_1 (var = #c) <- (x = #b,y = #a)
      
    *Evaluates to:*
        
            x__2 = 0.2 + 1.1;
            x__1 = 4.0 + 3.5;
            x__5 = 4.3 + 6.2;
            x__4 = 5.2 + 0.7;
        

  + **Rule 1**: map a table-like to a lambda snippet
        
        %map [options] <table-like> [ | ] %{ <code> %}
        

    *Example:*
      
        %table pets_1 = (animal,does,what) %tsv{
            dog     eats      kibbles
            cat     drinks    milk
            bird    sings     songs
        %}
        %table pets_2 = (animal,does,what) %tsv{
            lizard    absorbs    heat
        %}
        char *strings[] = {
        %map pets_1, pets_2 %{
            "My ${animal} ${does} ${what}",
        %}
        };
      
    *Evaluates to:*
        
        char *strings[] = {
            "My dog eats kibbles",
            "My cat drinks milk",
            "My bird sings songs",
            "My lizard absorbs heat",
        };
        

  Notes:
      
        + A single table name may be provided without parentheses.
        
        + Each argument may be of the form:
        
              <column name>
              <formal argument> = <column name>
              <formal argument> = `<verbatim value>`
        
        + A lambda table consists of one or more columns (braces),
          each a comma-separated list of values, which can be:
        
              literal verbatim
              `quoted verbatim`
              <start>..<end>[..<step>] (integer range, excluding <end>)
        
        + In lambda tables the following escape sequences apply:
        
              %nul => NUL character (empty value)
              %% => %
------------------------------------------------------------------------------
+ `%pipe`            : pipe text to external command and capture output

  Options:

         allow += <str>  : allow given syscall
         debug += <uint> : verbose logging of keyword invocation
           env += <str>  : set environment variable
       keepenv    <bool> : do not clear child process environment
           str  = <str>  : apply string modifiers to keyword output
        strict    <bool> : forbid most syscalls
         trace    <bool> : trace child to find forbidden syscalls
        unsafe    <bool> : allow all syscalls
          wait  = <uint> : timeout for child termination (in seconds)
      

  + **Rule 0**: verbatim text
        
        %pipe [options] `<command>` [ %{ <input text> %} ]
        %!    [options] `<command>` [ %{ <input text> %} ]
        

    *Example:*
      
        %pipe [env = `SHELL=/bin/sh`] `sh` %{
        echo $SHELL
        %}
      
    *Evaluates to:*
        
        /bin/sh
        

  + **Rule 1**: recalled text
        
        %pipe [options] `<command>` <snippet name> [[( <output argument list> ) ... <-] ( <input argument list> ) ...] 
        %!    [options] `<command>` <snippet name> [[( <output argument list> ) ... <-] ( <input argument list> ) ...] 
        

    *Example:*
      
        %pipe `echo foo`
      
    *Evaluates to:*
        
        foo
        

------------------------------------------------------------------------------
+ `%unittest`        : define a unit test

  Options:

         debug += <uint> : verbose logging of keyword invocation
        deinit  = <str>  : name of teardown fixture
      disabled    <bool> : disable test
          init  = <str>  : name of setup fixture
        signal  = <str>  : expected signal raised by test
        status  = <int>  : expected exit status returned by test
         suite  = <str>  : test suite name
       timeout += <uint> : timeout for test execution (seconds)
      verbatim    <bool> : verbatim code
      

  + **Rule 0**: test case
        
        %unittest [options] [ <test name> ] [ `<test description>` ] %{ <test code> %}
        

    *Example:*
      
        %unittest strcunesc %{
            srt_string *s = NULL;
            int ret = ${#NAME}("line\\nbreak",&s);
            cr_assert(all(
                eq(int,0,ret),
                eq(str,(char*)ss_to_c(s),"line\nbreak")
            ));
        %}
      
    *Evaluates to:*
        
         
        

------------------------------------------------------------------------------
+ `%dsl-def`         : define a domain-specific language

  Options:

        altsep    <bool> : use alternate statement separator
         debug += <uint> : verbose logging of keyword invocation
        extend    <bool> : extend already-defined language
       maxeval  = <uint> : evaluation depth limit
        syntax  = <str>  : language syntax
      

  + **Rule 0**: table-like with columns: keyword, snippet [, action]
        
        %dsl-def [options] <DSL name> = or += <table-like> 
        

    *Example:*
      
        %snippet mylang1:set = (x,y) %{ ${x} = ${y}; %}
        %snippet mylang1:add = (x,y) %{ ${x} += ${y}; %}
        %snippet mylang1:sub = (x,y) %{ ${x} -= ${y}; %}
        %snippet mylang1:mul = (x,y) %{ ${x} *= ${y}; %}
        %snippet mylang1:div = (x,y) %{ ${x} /= ${y}; %}
        %dsl-def [syntax = asm] mylang1 = (keyword,snippet) %tsv{
            set        mylang1:set
            add        mylang1:add
            sub        mylang1:sub
            mul        mylang1:mul
            div        mylang1:div
        %}
      
    *Evaluates to:*
        
         
        

------------------------------------------------------------------------------
+ `%dsl`             : evaluate a domain-specific language

  Options:

         debug += <uint> : verbose logging of keyword invocation
           sep  = <str>  : evaluated statement separator
           str  = <str>  : apply string modifiers to keyword output
      

  + **Rule 0**: DSL code
        
        %dsl [options] <DSL name> %{ <DSL code> %}
        

    *Example:*
      
        %// See %dsl-def example for definition of mylang1
        %dsl mylang1 %{
            set n, 1
            mul n, 3
            add n, 1
            div n, 2
            div n, 2
        %}
      
    *Evaluates to:*
        
        n = 1;
        n *= 3;
        n += 1;
        n /= 2;
        n /= 2;
        

  Notes:
      
        + DSL code allows verbatim values enclosed in backticks, without newlines. The backtick is escaped with a double backtick.

### Generic keywords (function-like) ##

------------------------------------------------------------------------------
+ `%intop`           : perform arithmetic with integers (bounds-checked)

  Options:

           add    <bool> : addition of two arguments
         debug += <uint> : verbose logging of keyword invocation
           div    <bool> : division of two arguments
           max    <bool> : maximum of all arguments
           min    <bool> : minimum of all arguments
           mod    <bool> : modulo of two arguments
           mul    <bool> : multiplication of two arguments
           pow    <bool> : power of two arguments
          prod    <bool> : product of all arguments
          sign    <bool> : always print sign
           str  = <str>  : apply string modifiers to keyword output
           sub    <bool> : subtraction of two arguments
           sum    <bool> : sum of all arguments
      unsigned    <bool> : treat all arguments as unsigned
      

  + **Rule 0**: arithmetic operation
        
        %intop [options] ( <operand list> )
        

    *Example:*
      
        %intop [sum] (5,-1,4,-8,1)
        %intop [prod] (5,-1,4,-8,1)
        %intop [min] (5,-1,4,-8,1)
        %intop [max] (5,-1,4,-8,1)
      
    *Evaluates to:*
        
        1
        160
        -8
        5
        

------------------------------------------------------------------------------
+ `%delay`           : print text after a delay (number of parse passes)

  Options:

         debug += <uint> : verbose logging of keyword invocation
           str  = <str>  : apply string modifiers to keyword output
      

  + **Rule 0**: optional verbatim text and number of parsing steps
        
        %delay [options] [ `<verbatim text>` ] ( <delay steps> )
        %@     [options] [ `<verbatim text>` ] ( <delay steps> )
        

    *Example:*
      
        %snippet test_name = %{ foobz %}
        %@(1)snippet %| test_name |% = %{ "wow!" %}
        printf("%s\n",%@(1)| foobz |%@(1));
      
    *Evaluates to:*
        
        printf("%s\n","wow!");
        

------------------------------------------------------------------------------
+ `%defined`         : print text conditional on resource being defined

  Options:

           all    <bool> : boolean AND comparison (default)
           any    <bool> : boolean OR comparison
          attr  = <str>  : further check if resource has attribute
         debug += <uint> : verbose logging of keyword invocation
           dsl    <bool> : check if domain-specific language is defined
          enum    <bool> : check if enum constant is defined
          fdef    <bool> : check if function definition is defined
           not    <bool> : negate comparison
          once    <bool> : check if include/repeat guard is defined
         proto    <bool> : check if function prototype is defined
       snippet    <bool> : check if snippet is defined
           str  = <str>  : apply string modifiers to keyword output
         table    <bool> : check if table is defined
          type    <bool> : check if type definition is defined
      

  + **Rule 0**: print 1 if defined, else 0
        
        %defined [options] ( <resource names> )
        

    *Example:*
      
        %snippet test_defined = %{ %}
        %defined [snippet] (test_defined)
        %defined [snippet,all] (test_defined, test_undefined)
      
    *Evaluates to:*
        
        1
        0
        

  + **Rule 1**: print string if defined, else other string
        
        %defined [options] <resource names> ( [ true = <verbatim if-true> ] [, false = <verbatim if-false> ] )
        

    *Example:*
      
        include/repeat guard `test_defined` is %defined [once] test_defined (`defined`,`not defined`)
      
    *Evaluates to:*
        
        include/repeat guard `test_defined` is not defined
        

------------------------------------------------------------------------------
+ `%strcmp`          : print text conditional on string comparison result

  Options:

         debug += <uint> : verbose logging of keyword invocation
            eq    <bool> : equal comparison
           ere    <bool> : use extended regular expression syntax
            ge    <bool> : greater than or equal comparison
            gt    <bool> : greater than comparison
         icase    <bool> : case-insensitive comparison
            le    <bool> : less than or equal comparison
            lt    <bool> : less than comparison
         nchar += <uint> : compare only this many characters
            ne    <bool> : not equal comparison
       newline    <bool> : match-any-character operators don't match a newline
           not    <bool> : negate comparison result
         regex    <bool> : match regular expression
           str  = <str>  : apply string modifiers to keyword output
      unsigned    <bool> : compare numbers as unsigned integers
         whole    <bool> : match whole string (anchor both ends)
      

  + **Rule 0**: multi-branch if-else comparison
        
        %strcmp [options] ( input = <verbatim input> [, match = <verbatim pattern> , then = <verbatim if-match>]+ [, else = <verbatim else> ] )
        %?      [options] ( input = <verbatim input> [, match = <verbatim pattern> , then = <verbatim if-match>]+ [, else = <verbatim else> ] )
        

    *Example:*
      
        %strcmp [le] (`-1`,`5`)
        %strcmp [not] (name,neim,`not equal`)
        %snippet strcmp_test = (x) %{ %strcmp (`${x}_argument`,no_argument,no,an) %}
        printf("has %| strcmp_test (yes) |% argument\n");
      
    *Evaluates to:*
        
        1
        not equal
        printf("has an argument\n");
        

------------------------------------------------------------------------------
+ `%strstr`          : count substring occurrences

  Options:

         debug += <uint> : verbose logging of keyword invocation
           ere    <bool> : use extended regular expression syntax
         icase    <bool> : case-insensitive comparison
         match  = <uint> : print match at given index
       newline    <bool> : match-any-character operators don't match a newline
         regex    <bool> : match regular expression
           str  = <str>  : apply string modifiers to keyword output
      submatch  = <uint> : print submatch at given index
      

  + **Rule 0**: print number of matches or matching string
        
        %strstr [options] ( input = <verbatim input> , match = <verbatim match> )
        

    *Example:*
      
        %strstr (strstr,str)
        %strstr [regex,submatch=1,ere] (a123,`a([0-9]+)`)
      
    *Evaluates to:*
        
        2
        123
        

------------------------------------------------------------------------------
+ `%strlen`          : get static string length

  Options:

          add1 += <uint> : increase computed length by this much
         debug += <uint> : verbose logging of keyword invocation
       escaped    <bool> : interpret C escape sequences
           str  = <str>  : apply string modifiers to keyword output
          utf8    <bool> : count UTF-8 encoded Unicode characters
      

  + **Rule 0**: print string length
        
        %strlen [options] ( <verbatim string> [, <verbatim string> ] )
        

    *Example:*
      
        char buf[%strlen [add1] (`my long string`)] = "my long string";
      
    *Evaluates to:*
        
        char buf[15] = "my long string";
        

------------------------------------------------------------------------------
+ `%strsub`          : perform string substitution

  Options:

        byline    <bool> : process input line-by-line (regex)
           cat    <bool> : append if not present
         debug += <uint> : verbose logging of keyword invocation
           ere    <bool> : use extended regular expression syntax
       escaped    <bool> : interpret C escape sequences
             g    <bool> : alias for [global]
        global    <bool> : replace all occurrences
         icase    <bool> : case-insensitive comparison
       newline    <bool> : match-any-character operators don't match a newline (regex)
         regex    <bool> : match regular expression
           sep  = <str>  : separator for [cat]
           str  = <str>  : apply string modifiers to keyword output
         whole    <bool> : match whole string (anchor both ends)
      

  + **Rule 0**: multiple pattern replacement in input
        
        %strsub [options] ( input = <verbatim input> [, match = <verbatim pattern> , sub = <verbatim replacement>]+ )
        

    *Example:*
      
        %strsub [g] (%<<
            int ugly_var;
            ugly_var = 5;
            ugly_var++;
        >>%,
        ugly_var,niceVar,
        int,`unsigned int`)
        %strsub [cat,sep=`...`] (qwert,y,z)
      
    *Evaluates to:*
        
            unsigned int niceVar;
            niceVar = 5;
            niceVar++;
        
        qwert...z
        

------------------------------------------------------------------------------
+ `%table-size`      : get number of rows/columns in table(s)

  Options:

          add1 += <uint> : increase computed size by this much
          cols    <bool> : get number of columns
         debug += <uint> : verbose logging of keyword invocation
          lazy    <bool> : delay evaluation until tables are defined
           opt    <bool> : ignore undefined tables
          rows    <bool> : get number of rows (default)
           str  = <str>  : apply string modifiers to keyword output
      

  + **Rule 0**: sum over multiple tables
        
        %table-size [options] ( <table names> )
        %tabsize    [options] ( <table names> )
        

    *Example:*
      
        %table test_table_size = (idx) %tsv{
            2
            165
            98
            9878
        %}
        %table-size(test_table_size)
      
    *Evaluates to:*
        
        4
        

------------------------------------------------------------------------------
+ `%table-length`    : compute min/max string length across values in table(s)

  Options:

          add1 += <uint> : increase computed length by this much
         debug += <uint> : verbose logging of keyword invocation
          lazy    <bool> : delay evaluation until columns and tables are defined
           max    <bool> : compute maximum length (default)
           min    <bool> : compute minimum length
           opt    <bool> : ignore undefined columns and tables
           str  = <str>  : apply string modifiers to keyword output
      

  + **Rule 0**: reduce over multiple tables and columns
        
        %table-length [options] <table names> ( <column names> )
        %tablen       [options] <table names> ( <column names> )
        

    *Example:*
      
        %table test_table_length = (description) %tsv{
            a short description
            a longer description
            the longest description
        %}
        %table-length test_table_length (#description)
      
    *Evaluates to:*
        
        23
        

------------------------------------------------------------------------------
+ `%table-get`       : retrieve values (or their indices) from table(s)

  Options:

         debug += <uint> : verbose logging of keyword invocation
           ere    <bool> : use extended regular expression syntax
         icase    <bool> : case-insensitive comparison
          lazy    <bool> : delay evaluation until columns and tables are defined
           opt    <bool> : ignore undefined columns and tables
         regex    <bool> : match regular expression
           str  = <str>  : apply string modifiers to keyword output
        strict    <bool> : fail if not found, instead of printing default
      

  + **Rule 0**: get row index (or column value) of match in key column (or given column)
        
        %table-get [options] <table names> ( [ <search column> ,] `<search key>` [, <value column> ] )
        %tabget    [options] <table names> ( [ <search column> ,] `<search key>` [, <value column> ] )
        

    *Example:*
      
        %table [hash=1] test_table_get = (name,surname) %tsv{
            foo        bar
            foo        baz
            foo        baçar
        %}
        %table-get test_table_get (`baçar`,#name)
      
    *Evaluates to:*
        
        foo
        


### C-specific keywords (statement-like) ##

------------------------------------------------------------------------------
+ `%typedef`         : declare type

  Options:

          argv  = <str>  : name of argument-wrapping struct [argv]
         begin += <str>  : pre-condition snippets
         debug += <uint> : verbose logging of keyword invocation
           end += <str>  : post-condition snippets
         named    <bool> : allow named arguments (struct-wrapped)
           opt    <bool> : skip definition if type already exists
         quiet    <bool> : do not emit C typedef
        suffix  = <str>  : suffix for defined function shadowed by macro [__]
      

  + **Rule 0**: register one or more type names
        
        %typedef [options] <type name list> ;
        

    *Example:*
      
        %typedef my_c_type;
      
    *Evaluates to (formatted):*
        
         
        

  + **Rule 1**: define a type
        
        %typedef [options] <type declaration>
        

    *Example:*
      
        %typedef int (*two_int_type)(int,int);
      
    *Evaluates to (formatted):*
        
        typedef int (*(two_int_type)) (int, int);
        

------------------------------------------------------------------------------
+ `%proto`           : declare function prototype

  Options:

          argv  = <str>  : name of argument-wrapping struct [argv]
         begin += <str>  : pre-condition snippets
         debug += <uint> : verbose logging of keyword invocation
           end += <str>  : post-condition snippets
         named    <bool> : allow named arguments (struct-wrapped)
           opt    <bool> : skip definition if prototype already exists
         quiet    <bool> : do not emit C prototype
        suffix  = <str>  : suffix for defined function shadowed by macro [__]
      

  + **Rule 0**: define prototype with declarator
        
        %proto [options] <function declaration>
        

    *Example:*
      
        %proto int (*my_fun(int (*f)(int)))(int);
      
    *Evaluates to (formatted):*
        
        int (*(my_fun) (int (*f) (int))) (int);
        

------------------------------------------------------------------------------
+ `%def`             : define function

  Options:

         const    <bool> : pass constant (named) arguments
         debug += <uint> : verbose logging of keyword invocation
       nocheck    <bool> : do not emit pre-/post-condition checks
           now    <bool> : disable lazy evaluation
        return  = <str>  : return expression
      

  + **Rule 0**: function declared with %proto
        
        %def [options] <function_name> {
        

    *Example:*
      
        %proto [named] int my_sum(int x, int y);
        
        %def my_sum {
            return argv.x + argv.y;
        }
      
    *Evaluates to (formatted):*
        
        struct my_sum__args
        {
          int _;
          int x;
          int y;
        };
        int (my_sum__) (struct my_sum__args argv);
        #define my_sum(...) my_sum__((struct my_sum__args){ ._=0, __VA_ARGS__ })
        
        int
        my_sum__ (struct my_sum__args argv)
        {
          return argv.x + argv.y;
        }
        

  + **Rule 1**: function with type defined by %typedef
        
        %def [options] <function_type> <function_name> {
        

    *Example:*
      
        %typedef int callback_f(struct object *x, int data);
        
        %def callback_f my_callback {
            fprintf(stderr,"Callback for object %s with data %d\n",x->name,data);
            return 0;
        }
      
    *Evaluates to (formatted):*
        
        typedef int (callback_f) (struct object * x, int data);
        
        int
        my_callback (struct object *x, int data)
        {
          fprintf (stderr, "Callback for object %s with data %d\n", x->name, data);
          return 0;
        }
        

  + **Rule 2**: function with type not yet defined (lazy evaluation)
        
        %def [options] <function_type> <function_name> {
        

    *Example:*
      
        %@(2)typedef int delay_func(int x);
        
        %def delay_func my_delay_func {
            return 0;
        }
      
    *Evaluates to (formatted):*
        
        typedef int (delay_func) (int x);
        
        int
        my_delay_func (int x)
        {
          return 0;
        }
        

  + **Rule 3**: end function definition
        
        %def [options] [ <function_name> ] }
        

    *Example:*
      
        %snippet begin = (x) %{ if(${x} < 0) abort(); %}
        %snippet end = (ret) <- (x) %{ if(${ret} > 10) abort(); %}
        %proto [begin=begin,end=end] int myfun(int x);
        %def [return=y] myfun {
            int y = ++x;
        %def myfun }
      
    *Evaluates to (formatted):*
        
        int (myfun) (int x);
        int
        myfun (int x)
        {
          if (x < 0)
            abort ();
          int y = ++x;
          goto end;
        end:;
        
          if (y > 10)
            abort ();
          return y;
        }
        

------------------------------------------------------------------------------
+ `%unused`          : silence unused variable warning

  Options:

         debug += <uint> : verbose logging of keyword invocation
      

  + **Rule 0**: silence warning for unused variables
        
        %unused [options] <variable list> ;
        

    *Example:*
      
        %unused x,y,z;
      
    *Evaluates to (formatted):*
        
        (void) x;
        (void) y;
        (void) z;
        

------------------------------------------------------------------------------
+ `%prefix`          : set prefix for functions and enums

  Options:

         debug += <uint> : verbose logging of keyword invocation
         unset    <bool> : unset prefix
      

  + **Rule 0**: set prefix
        
        %prefix [options] <prefix> ;
        

    *Example:*
      
        %prefix my_lib;
        %proto void* malloc(size_t size);
      
    *Evaluates to (formatted):*
        
        
        void *(my_lib_malloc) (size_t size);
        

------------------------------------------------------------------------------
+ `%enum`            : define enum with optional helper functions

  Options:

       bitflag    <bool> : values as bit flags
         debug += <uint> : verbose logging of keyword invocation
       default  = <str>  : name of default/unset enum value
        helper  = <uint> : define helper functions (0 = all, 1 | 2 | 4)
        prefix  = <str>  : prefix for enum constants
           rev    <bool> : decreasing enum values
         shift  = <int>  : shift enum values by this amount
         upper    <bool> : uppercase enum constants
      

  + **Rule 0**: table-like with columns: name [, description] [, value]
        
        %enum [options] <enum name> = <table-like> 
        

    *Example:*
      
        %enum [helper] color = (name,description,value) %tsv{
            red      red color      0
            green    green color    1
            blue     blue color     2
        %}
      
    *Evaluates to (formatted):*
        
        enum color
        {
          COLOR_red = 0,          /* red color */
          COLOR_green = 1,        /* green color */
          COLOR_blue = 2,         /* blue color */
        };
        #define enum_color_len 3
        
        __attribute__((const)) __attribute__((unused))
             static inline const char *color_strenum (const enum color x)
        {
          switch (x)
            {
            case COLOR_red:
              return "red color";
              break;
            case COLOR_green:
              return "green color";
              break;
            case COLOR_blue:
              return "blue color";
              break;
            default:
              return "";
              break;
            }
        }
        
        __attribute__((const)) __attribute__((unused))
             static inline const char *color_enumtostr (const enum color x)
        {
          switch (x)
            {
            case COLOR_red:
              return "COLOR_red";
              break;
            case COLOR_green:
              return "COLOR_green";
              break;
            case COLOR_blue:
              return "COLOR_blue";
              break;
            default:
              return "";
              break;
            }
        }
        
        
        __attribute__((const)) __attribute__((unused))
             static inline bool color_strtoenum (const char *str, int val[static 1])
        {
          if ('C' == *((str) + 0) && strncmp ((str) + 1, "OLOR_", 5) == 0)
            {
              if ('b' == *((str) + 6) && strncmp ((str) + 7, "lue", 4) == 0)
            {
              *val = COLOR_blue;
              return true;
            }
              else if ('g' == *((str) + 6) && strncmp ((str) + 7, "reen", 5) == 0)
            {
              *val = COLOR_green;
              return true;
            }
              else if ('r' == *((str) + 6) && 'e' == *((str) + 7)
                   && 'd' == *((str) + 8) && '\0' == *((str) + 9))
            {
              *val = COLOR_red;
              return true;
            }
              else
            {
              goto iftrie_end;
            }
            }
          else
            {
              goto iftrie_end;
            iftrie_end:;
              return false;
            }
        }
        

  Notes:
      
        + <table-like> is one of:
        
            table           = [( <column names> )] %TSV|JSON{ <table body> %}
            table selection = <table name> ( <selected columns> )
            lambda table    = { <lambda table> } ( <column names> )
------------------------------------------------------------------------------
+ `%strin`           : check if string belongs to word list

  Options:

        bysize    <bool> : sort larger classes first
         debug += <uint> : verbose logging of keyword invocation
        expand  = <uint> : maximum string length for character-by-character comparisons
         fuzzy    <bool> : succeed on unique partial match
       nomatch  = <str>  : snippet to recall when there is no match
          none  = <str>  : string value outside of word list
      

  + **Rule 0**: table-like with columns: word [, action], and optional lambda snippet
        
        %strin [options] `<variable>` <table-like> %{ <action> %}
        

    *Example:*
      
        %table strin_words = (word) %json{
        [ ["trie"], ["trei"], ["trai"], ["tree"] ]
        %}
        %strin [none=``] str strin_words (#word) %{ return $S{word}; %}
      
    *Evaluates to (formatted):*
        
        if ('t' == *((str) + 0) && 'r' == *((str) + 1))
          {
            if ('a' == *((str) + 2) && 'i' == *((str) + 3) && '\0' == *((str) + 4))
              {
            return "trai";
              }
            else if ('e' == *((str) + 2))
              {
            if ('e' == *((str) + 3) && '\0' == *((str) + 4))
              {
                return "tree";
              }
            else if ('i' == *((str) + 3) && '\0' == *((str) + 4))
              {
                return "trei";
              }
            else
              {
                goto cmod_strin_else_0;
              }
              }
            else if ('i' == *((str) + 2) && 'e' == *((str) + 3)
                 && '\0' == *((str) + 4))
              {
            return "trie";
              }
            else
              {
            goto cmod_strin_else_0;
              }
          }
        else
          {
            goto cmod_strin_else_0;
          cmod_strin_else_0:;
            return "";
          }
        

------------------------------------------------------------------------------
+ `%foreach`         : iterate over array of known size

  Options:

       autoarr    <bool> : treat array as an std:autoarr type
         const    <bool> : preclude modifying the array
         debug += <uint> : verbose logging of keyword invocation
         deref += <uint> : dereference item this many times
           par    <bool> : parallelize loop (OpenMP)
           ptr  = <str>  : pointer access type
           rev    <bool> : reverse iteration order
          skip += <uint> : skip this number of elements at start
          svec  = <str>  : treat array as srt_vector of item type
      

  + **Rule 0**: iterate over array elements
        
        %foreach [options] <element name> ( <array> [, <length> ] ) %{ <code> %}
        

    *Example:*
      
        double array[len];
        
        %foreach elem (array, len) %{
            ${elem} = 0.0;
        %}
      
    *Evaluates to (formatted):*
        
        double array[len];
        
        {
          const size_t _len = len;
          for (size_t _idx = 0; _idx < _len; ++_idx)
            {
              const size_t _0 = _idx;
              const size_t _idx = 0;
              (void) _idx;
              (((array))[_0]) = 0.0;
            }
        }
        

------------------------------------------------------------------------------
+ `%switch`          : select cases over (structured) variables

  Options:

         array    <bool> : array variable
       bitflag    <bool> : bit flag variable
         const    <bool> : integer constant (default)
         debug += <uint> : verbose logging of keyword invocation
          each    <bool> : non-exclusive cases
           int    <bool> : integer variable
        string    <bool> : string variable
        struct    <bool> : struct variable
        substr    <bool> : check substrings with string variable
      

  + **Rule 0**: switch over explicit cases
        
        %switch [options] ( <variable> [, <length> ] ) %{ <case list> %}
        

    *Example:*
      
        const char my_str[] = "foobar";
        %switch [string] (str) %{
            "mystring" %<<
                return MY_STRING;
            >>%
            "my" "split" "string" %<<
                return MY_SPLIT_STRING;
            >>%
            my_str %<<
                return MY_STR;
            >>%
            %<<
                return DEFAULT;
            >>%
        %}
      
    *Evaluates to (formatted):*
        
        const char my_str[] = "foobar";
        if ((strncmp (str, "mystring", sizeof ("mystring")) == 0))
          {
            return MY_STRING;
        
          }
        else
          if ((strncmp (str, "my" "split" "string", sizeof ("my" "split" "string")) ==
               0))
          {
            return MY_SPLIT_STRING;
        
          }
        else if ((strcmp (my_str, str) == 0))
          {
            return MY_STR;
        
          }
        else
          {
            return DEFAULT;
        
          }
        


### C-specific keywords (function-like) ##

------------------------------------------------------------------------------
+ `%free`            : free and annul a list of pointers

  Options:

         const    <bool> : do not free, only annul
         debug += <uint> : verbose logging of keyword invocation
      

  + **Rule 0**: free and NULL list of pointers
        
        %free [options] ( <pointer list> ) ;
        

    *Example:*
      
        %free (x,y,z->ptr);
        %free [const] (str);
      
    *Evaluates to (formatted):*
        
        {
          free (x);
          (x) = NULL;
        }
        
        {
          free (y);
          (y) = NULL;
        }
        
        {
          free (z->ptr);
          (z->ptr) = NULL;
        }
        
        {
          (str) = NULL;
        }
        

------------------------------------------------------------------------------
+ `%arrlen`          : get length of static array

  Options:

         debug += <uint> : verbose logging of keyword invocation
      

  + **Rule 0**: get length of static array
        
        %arrlen [options] ( <array> )
        

    *Example:*
      
        array[%arrlen (array) - 1] = 0;
        size_t siz = %arrlen (array);
      
    *Evaluates to (formatted):*
        
        array[(sizeof (array) / sizeof (*(array))) - 1] = 0;
        size_t siz = (sizeof (array) / sizeof (*(array)));
        




## C% standard library #

The C% standard library is released under the MIT license, so that it
and the code generated from it may be used in any software, including proprietary.
Forcing standard library-generated code to be GPL'd would be too restrictive
for a project like this that aims to define a (meta-)programming language.
Using the C% standard library may result in meta-meta-programming, use wisely.

Available modules:

--------------------------------------------------------
### `autoarr` : Definition of auto-growing array types ##
-------------------------------------------
### `bitwise` : Operations on single bits ##
------------------------------------
### `common` : Common simple tasks ##
----------------------------------------------------
### `getopt` : Automated definition of CLI options ##
--------------------------------
### `logging` : Logging macros ##
-----------------------------------------------------
### `ralloc` : Retrying memory-allocation functions ##
---------------------------------------------------
### `random` : Random number generation functions ##
-------------------------------------------------------
### `retval` : Standardized propagating return values ##
--------------------------------------------------------------
### `safe` : Safe coding constructs based on SEI CERT C 2016 ##
-----------------------------------------------------
### `sort` : Sorting algorithms for arbitrary types ##
--------------------------------------------------------
### `strnum` : Conversions between strings and numbers ##
--------------------------------------------------------
### `variant` : Definition of tagged unions (variants) ##


----
# Footnotes

