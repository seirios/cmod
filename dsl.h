#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#ifndef CMOD_DSL_H
#define CMOD_DSL_H

#include "parts/struct_dsl.h"

int cmp_dsl_by_name(const void *, const void *);

__attribute__((unused))
static inline void dsl_clear(struct dsl *x)
{
    free(x->name);
    {
        for (size_t i = 0; i < sv_len(x->kw_name); ++i) {
            const void *_item = sv_at(x->kw_name, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->kw_name));
    }
    sv_free(&x->kw_snip);
    sv_free(&x->kw_prec);
    shm_free(&x->kw_map);
    *x = (struct dsl) { 0 };
}

#endif
