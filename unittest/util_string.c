#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include "parts/typedef_srt_alias.h"
#include "parts/struct_str_mod.h"
#include <stdbool.h>
#include <regex.h>
#include "parts/struct_str_mod.h"
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "ralloc.h"
#include <assert.h>
#include "util_string.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };

Test(base, t2__ascidchar,.disabled = false)
{
    cr_assert(all(eq(int, ascidchar('5', true), '_'),
                  eq(int, ascidchar('5', false), '5'),
                  eq(int, ascidchar('a', true), 'a'),
                  eq(int, ascidchar('a', false), 'a'),
                  eq(int, ascidchar('_', true), '_'),
                  eq(int, ascidchar('_', false), '_'),
                  eq(int, ascidchar('@', true), '_'), eq(int, ascidchar('@', false), '_')
              ));

}

Test(base, t3__strisspace,.disabled = false)
{
    cr_assert(all(eq(int, strisspace(NULL), true),
                  eq(int, strisspace(""), true),
                  eq(int, strisspace("   "), true),
                  eq(int, strisspace(" \f\t\v\n "), true),
                  eq(int, strisspace("a"), false), eq(int, strisspace(" # "), false)
              ));

}

Test(base, t4__striscid,.disabled = false)
{
    cr_assert(all(eq(int, striscid(NULL), false),
                  eq(int, striscid(""), false),
                  eq(int, striscid("_cid"), true),
                  eq(int, striscid("_5a_"), true),
                  eq(int, striscid("var5"), true),
                  eq(int, striscid("5a"), false), eq(int, striscid("@var"), false)
              ));

}

Test(base, t5__strisidext,.disabled = false)
{
    cr_assert(all(eq(int, strisidext(NULL), false),
                  eq(int, strisidext("_cid"), true),
                  eq(int, strisidext("_5a_"), true),
                  eq(int, strisidext("var5"), true),
                  eq(int, strisidext("5a"), false),
                  eq(int, strisidext("@var"), true),
                  eq(int, strisidext("_:?!@&+-.what?"), true),
                  eq(int, strisidext("_:?!@&+-.what?/123"), true),
                  eq(int, strisidext("_:?!@&+-.what?/a"), false),
                  eq(int, strisidext("_:?!@&+-.what?/"), false),
                  eq(int, strisidext("_:?!@/+-.what?/"), false)
              ));

}

Test(base, t6__striscidslash,.disabled = false)
{
    cr_assert(all(eq(int, striscidslash(NULL), false),
                  eq(int, striscidslash(""), false),
                  eq(int, striscidslash("_cid"), true),
                  eq(int, striscidslash("_5a_"), true),
                  eq(int, striscidslash("var/5"), true),
                  eq(int, striscidslash("a/a"), false), eq(int, striscidslash("@var"), false)
              ));

}

Test(base, t7__strcunesc,.disabled = false)
{
    srt_string *s[11] = { 0 };
    cr_assert(all(eq(int, 0, strcunesc(strlen("noescapes"), "noescapes", &s[0]))
                  , eq(int, 0, strcunesc(strlen("line\\nbreak"), "line\\nbreak", &s[1]))
                  , eq(int, 0,
                       strcunesc(strlen("double\\\\backslash"), "double\\\\backslash", &s[2]))
                  , eq(int, 0, strcunesc(strlen("nul\\0byte"), "nul\\0byte", &s[3]))
                  , eq(int, 0, strcunesc(strlen("letter\\x41Z"), "letter\\x41Z", &s[4]))
                  , eq(int, 0, strcunesc(strlen("letter\\102Z"), "letter\\102Z", &s[5]))
                  , eq(int, 0, strcunesc(strlen("\\x1BisEscape"), "\\x1BisEscape", &s[6]))
                  , eq(int, 0,
                       strcunesc(strlen("inRange\\x000000000000005a"), "inRange\\x000000000000005a",
                                 &s[7]))
                  , eq(int, 0,
                       strcunesc(strlen("max16hexChar\\x0000000000000073e"),
                                 "max16hexChar\\x0000000000000073e", &s[8]))
                  , eq(int, 0, strcunesc(strlen("uchar\\u0394"), "uchar\\u0394", &s[9]))
                  , eq(int, 0, strcunesc(strlen("uchar\\U00000394"), "uchar\\U00000394", &s[10]))
              ));
    cr_assert(all(eq(sz, 3, strlen(ss_to_c(s[3]))),
                  eq(sz, 8, strlen(ss_to_c(s[5]))), eq(str, (char *)ss_to_c(s[0]), "noescapes")
                  , eq(str, (char *)ss_to_c(s[1]), "line\nbreak")
                  , eq(str, (char *)ss_to_c(s[2]), "double\\backslash")
                  , eq(str, (char *)ss_to_c(s[3]), "nul")
                  , eq(str, (char *)ss_to_c(s[4]), "letterAZ")
                  , eq(str, (char *)ss_to_c(s[5]), "letterBZ")
                  , eq(str, (char *)ss_to_c(s[6]), "\x1BisEscape")
                  , eq(str, (char *)ss_to_c(s[7]), "inRangeZ")
                  , eq(str, (char *)ss_to_c(s[8]), "max16hexCharse")
                  , eq(str, (char *)ss_to_c(s[9]), "uchar\u0394")
                  , eq(str, (char *)ss_to_c(s[10]), "uchar\U00000394")
              ));
    cr_assert(all(eq(int, 2, strcunesc(strlen("stray\x1B"), "stray\x1B", &s[0]))
                  , eq(int, 3, strcunesc(strlen("invalid\\e"), "invalid\\e", &s[0]))
                  , eq(int, 4, strcunesc(strlen("incomplete\\x"), "incomplete\\x", &s[0]))
                  , eq(int, 4, strcunesc(strlen("incomplete\\u000"), "incomplete\\u000", &s[0]))
                  , eq(int, 4,
                       strcunesc(strlen("incomplete\\U000000"), "incomplete\\U000000", &s[0]))
                  , eq(int, 5, strcunesc(strlen("outOfRange\\567"), "outOfRange\\567", &s[0]))
                  , eq(int, 5, strcunesc(strlen("outOfRange\\xFFF"), "outOfRange\\xFFF", &s[0]))
                  , eq(int, 6, strcunesc(strlen("outOfRange\\U001a"), "outOfRange\\U001a", &s[0]))
                  , eq(int, 6,
                       strcunesc(strlen("outOfRange\\u0000001a"), "outOfRange\\u0000001a", &s[0]))
                  , eq(int, 6,
                       strcunesc(strlen("outOfRange\\U0011ffff"), "outOfRange\\U0011ffff", &s[0]))
              ));

}

Test(base, t8__strcesc,.disabled = false)
{
    const char str[] = "\x1b" "ade" "\xff";
    srt_string *ss = NULL;
    int ret = strcesc(strlen(str), str, &ss);
    cr_assert(all(eq(int, ret, 0), eq(str, (char *)ss_to_c(ss), "\\x1bade\\xff")
              ));

}

Test(base, t9__strcesc,.disabled = false)
{
    const char str[] = "\\\"\?";
    srt_string *ss = NULL;
    int ret = strcesc(strlen(str), str, &ss);
    cr_assert(all(eq(int, ret, 0), eq(str, (char *)ss_to_c(ss), "\\\\\\\"\\\?")
              ));

}

static int iscommasep(int c)
{
    return c == ',';
}

Test(base, t11__strsplit,.disabled = false)
{
    char buf[] = "foo,bar,baz";
    vec_string *spl = strsplit(buf, iscommasep);
    cr_assert(all(eq(sz, sv_len(spl), 3),
                  eq(str, sv_at_ptr(spl, 0), "foo"),
                  eq(str, sv_at_ptr(spl, 1), "bar"), eq(str, sv_at_ptr(spl, 2), "baz")
              ));

}

Test(base, t12__strdist,.disabled = false)
{
    cr_assert(all(eq(sz, strdist("same", "same"), 0),
                  eq(sz, strdist("samf", "same"), 1),
                  eq(sz, strdist("smea", "same"), 2),
                  eq(sz, strdist("eams", "same"), 2), eq(sz, strdist("kitten", "sitting"), 3)
              ));

}

Test(base, t13__ss_cat_byline,.disabled = false)
{

}

Test(base, t14__strregsub_parse,.disabled = false)
{

}

struct test_strregsub__args {
    int _;
    const char *inp;
    const char *pat;
    const char *sub;
    bool global;
    bool icase;
    bool ere;
    bool newline;
    srt_string **out;
};
static int (test_strregsub__) (struct test_strregsub__args argv);
#define test_strregsub(...) test_strregsub__((struct test_strregsub__args){ ._=0, __VA_ARGS__ })
static int test_strregsub__(struct test_strregsub__args argv)
{
    int rc = 0;

    srt_string *newbuf = NULL;

    srt_string *buf = ss_alloc(0);
    if (NULL == buf) {
        rc = 1;
        goto fail;
    }
    ss_cat_c(&buf, argv.inp);   // copy input to buffer
    ss_cat_cn1(&buf, '\0');     // NUL-terminate

    regex_t preg;
    int cflags = 0L | (argv.icase ? REG_ICASE : 0L)
        | (argv.ere ? REG_EXTENDED : 0L)
        | (argv.newline ? REG_NEWLINE : 0L);
    if (regcomp(&preg, argv.pat, cflags)) {
        rc = -1;
        goto fail;
    }

    const char *rinp = ss_get_buffer_r(buf);
    bool notfound = false;

    if ((rc = strregsub(rinp, preg, argv.sub, argv.global, &newbuf, &rinp, &notfound)))
         goto fail;
    ss_cat_c(&newbuf, rinp);

    *argv.out = newbuf;
    newbuf = NULL;      // hand-over

 fail:
    ss_free(&buf);
    ss_free(&newbuf);

    return rc;
}

Test(base, t16__strregsub,.disabled = false,.description = "Replacement without options")
{
    srt_string *out = NULL;
    cr_assert(all(eq(int, test_strregsub("bANANa", "[AN]", "_",.out = &out), 0),
                  eq(str, (char *)ss_to_c(out), "b_NANa")
              ));

}

Test(base, t17__strregsub,.disabled = false,.description = "Global replacement")
{
    srt_string *out = NULL;
    cr_assert(all(eq(int, test_strregsub("bANANa", "[AN]", "_",.global = true,.out = &out), 0),
                  eq(str, (char *)ss_to_c(out), "b____a")
              ));

}

Test(base, t18__strregsub,.disabled = false,.description =
     "Global and case-insensitive replacement")
{
    srt_string *out = NULL;
    cr_assert(all
              (eq
               (int, test_strregsub("bANANa", "[AN]", "_",.icase = true,.global = true,.out = &out),
                0), eq(str, (char *)ss_to_c(out), "b_____")
              ));

}

srt_string *s = NULL;

void init_ss_cat_mod(void)
{
    ss_free(&s);
    s = ss_dup_c("test");
}

Test(base, t20__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, NULL, NULL, (struct str_mod) { 0, 0, 0, 0, 0, 0, 0 });
    cr_assert(all(eq(ptr, res, ss_void),
                  eq(sz, ss_len(s), 4), eq(str, (char *)ss_to_c(s), "test");));

}

Test(base, t21__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 12), eq(str, (char *)ss_to_c(s), "test str@ing")
              ));

}

Test(base, t22__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, TRAN_CID, 0, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 12), eq(str, (char *)ss_to_c(s), "test_str_ing")
              ));

}

Test(base, t23__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, FOLD_UPPER, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 12), eq(str, (char *)ss_to_c(s), "test STR@ING")
              ));

}

Test(base, t24__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, 0, WRAP_HSTR, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 20), eq(str, (char *)ss_to_c(s), "test%<<  str@ing >>%")
              ));

}

Test(base, t25__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, 0, WRAP_HSTR | WRAP_SQUOT, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 22), eq(str, (char *)ss_to_c(s), "test%<< ' str@ing' >>%")
              ));

}

Test(base, t26__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, PRE_HTAG, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 13), eq(str, (char *)ss_to_c(s), "test# str@ing")
              ));

}

Test(base, t27__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, 0, 0, LEAD_COMMA, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 13), eq(str, (char *)ss_to_c(s), "test, str@ing")
              ));

}

Test(base, t28__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), NULL,
                                 (struct str_mod) { 0, 0, 0, 0, 0, LEAD_NAME, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 12), eq(str, (char *)ss_to_c(s), "test str@ing")
              ));

}

Test(base, t29__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), "arg",
                                 (struct str_mod) { 0, 0, 0, 0, 0, LEAD_NAME, TRAIL_COMMA });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 17), eq(str, (char *)ss_to_c(s), "testarg= str@ing,")
              ));

}

Test(base, t30__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing"), "arg",
                                 (struct str_mod) { 0, 0, 0, 0, 0, LEAD_NAME | LEAD_COMMA, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 17), eq(str, (char *)ss_to_c(s), "test,arg= str@ing")
              ));

}

Test(base, t31__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing  "), NULL,
                                 (struct str_mod) { TRIM_SPACE, 0, 0, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 11), eq(str, (char *)ss_to_c(s), "teststr@ing")
              ));

}

Test(base, t32__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str@ing  "), NULL,
                                 (struct str_mod) { TRIM_SPACE | TRIM_LAST, 0, 0, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 10), eq(str, (char *)ss_to_c(s), "teststr@in")
              ));

}

Test(base, t33__ss_cat_mod,.disabled = false,.init = init_ss_cat_mod)
{
    srt_string *res = ss_cat_mod(&s, ss_crefs(" str%ing"), NULL,
                                 (struct str_mod) { 0, TRAN_SNIPPET, 0, 0, 0, 0, 0 });
    cr_assert(all(ne(ptr, res, ss_void),
                  eq(sz, ss_len(s), 13), eq(str, (char *)ss_to_c(s), "test str%%ing")
              ));

}

Test(base, t34__ss_dec_esc_c,.disabled = false,.description = "Escaped newline")
{
    srt_string *s = ss_dup_c("a\\nb");
    srt_string *sunesc = ss_dec_esc_c(&s);
    cr_assert(all(ne(ptr, sunesc, ss_void), eq(str, (char *)ss_to_c(s), "a\nb")
              ));

}

Test(base, t35__ss_dec_esc_c,.disabled = false,.description = "Escaped byte")
{
    srt_string *s = ss_dup_c("a\\x1bi");
    srt_string *sunesc = ss_dec_esc_c(&s);
    cr_assert(all(ne(ptr, sunesc, ss_void), eq(str, (char *)ss_to_c(s), "a" "\x1b" "i")
              ));

}

Test(base, t36__ss_enc_esc_c,.disabled = false,.description = "Escaped byte")
{
    srt_string *s = ss_dup_c("a\x1bi");
    srt_string *sesc = ss_enc_esc_c(&s);
    cr_assert(all(ne(ptr, sesc, ss_void), eq(str, (char *)ss_to_c(s), "a\\x1bi")
              ));

}

Test(base, t37__str_fuzzy_match,.disabled = false)
{
    static const char *pool[] = { "mine", "fine", "mein", "fein" };
    const char *match[5] = { 0 };

    bool found0 =
        str_fuzzy_match("", pool, (sizeof(pool) / sizeof(*(pool))), strget_array, 0.5, &match[0]);
    bool found1 =
        str_fuzzy_match("sein", pool, (sizeof(pool) / sizeof(*(pool))), strget_array, 0.5,
                        &match[1]);
    bool found2 =
        str_fuzzy_match("fin", pool, (sizeof(pool) / sizeof(*(pool))), strget_array, 0.5,
                        &match[2]);
    bool found3 =
        str_fuzzy_match("waytoolong", pool, (sizeof(pool) / sizeof(*(pool))), strget_array, 0.5,
                        &match[3]);
    bool found4 =
        str_fuzzy_match("waytoolong", pool, (sizeof(pool) / sizeof(*(pool))), strget_array, 5,
                        &match[4]);

    cr_assert(all(eq(int, found0, 0),
                  eq(ptr, (void *)match[0], NULL),
                  eq(int, found1, 1),
                  eq(str, (char *)match[1], "mein"),
                  eq(int, found2, 1),
                  eq(str, (char *)match[2], "fine"),
                  eq(int, found3, 0), eq(int, found4, 1), eq(str, (char *)match[4], "mine")
              ));

}

Test(base, t38__vstr_join,.disabled = false)
{
    static const char *strings[] = { "my", "join", "test" };
    vec_string *sv_strings = NULL;
    (sv_strings) = sv_alloc_t(SV_PTR, (sizeof(strings) / sizeof(*(strings))));
    if (sv_void == (sv_strings)) {
    }
    if (NULL != sv_strings)
        for (size_t i = 0; i < (sizeof(strings) / sizeof(*(strings))); ++i) {   // const borrow

            const char *item = strings[i];
            if (!sv_push(&(sv_strings), &(item))) {
            }
        }

    cr_expect(eq(sz, sv_len(sv_strings), 3));
    srt_string *ss = vstr_join(sv_strings, '_');
    cr_assert(all(ne(ptr, ss, ss_void), eq(str, (char *)ss_to_c(ss), "my_join_test")
              ));

}

Test(base, t39__vstr_addstr,.disabled = false)
{
    vec_string *x = NULL;
    (x) = sv_alloc_t(SV_PTR, 0);
    if (sv_void == (x)) {
    }

    cr_expect(ne(ptr, x, NULL));

    bool ret = vstr_addstr(&x, "my", "addstr", "test");

    cr_assert(all(eq(int, ret, 1),
                  eq(sz, sv_len(x), 3),
                  eq(str, sv_at_ptr(x, 0), "my"),
                  eq(str, sv_at_ptr(x, 1), "addstr"), eq(str, sv_at_ptr(x, 2), "test")
              ));

}

Test(base, t40__uint_tostr,.disabled = false)
{
    char buf[STRUMAX_LEN];
    cr_assert(all(eq(str, (char *)uint_tostr(0, buf), "0"),
                  eq(str, (char *)uint_tostr(123, buf), "123"),
                  eq(str, (char *)uint_tostr(168984654, buf), "168984654")
              ));

}
