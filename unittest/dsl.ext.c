#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include "ralloc.h"
#include "dsl.ext.h"

struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include "parts/struct_dsl_param.h"
#include "parts/variant_dsl_ast.h"
#include <assert.h>
#include "dsl_parser.h"
#include "dsl_scanner.h"
#include "ralloc.h"
#include "util_snippet.h"
Test(base, t2__dsl_ast_free,.disabled = false)
{
    char *str = strdup("my_verbatim_argument");
    cr_expect(ne(ptr, str, NULL));
    struct dsl_ast tree = DSL_AST_NODE_CONST_set(str);
    dsl_ast_free(&tree);

}
