#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#define JSMN_HEADER
#define JSMN_STRICT
#define JSMN_PARENT_LINKS

#include "table_json.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include <assert.h>
#include "jsmn.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include "ralloc.h"
#include "table_parse.h"
struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };

int json_parse(size_t, const char[static 1], jsmntok_t *[static 1], size_t[static 1], size_t *);
bool valid_json_format_arrarr(size_t, const jsmntok_t[static 1]);
bool valid_json_format_arrobj(size_t, const jsmntok_t[static 1]);
bool valid_json_multitable_format(size_t, jsmntok_t[static 1]);

char *parse_json_strval(const char *, const jsmntok_t *);
int parse_json_format_arrarr(const char *, size_t, const jsmntok_t[static 1], size_t[static 1],
                             vec_vec_string *[static 1]);
int parse_json_format_arrobj(const char *, size_t, const jsmntok_t[static 1],
                             vec_namarg *[static 1], vec_vec_string *[static 1],
                             size_t ierr[static 1]);

static const char jstr_valid[] =
    "  {"
    "  \"array\": [ 0, 1, 2 ],"
    "  \"object\": {"
    "      \"array\": [ \"foo\", 3.1416 ],"
    "      \"null\": null," "      \"true\": true," "      \"false\": false" "  }" "}  ";

static const char jstr_arrarr_explicit[] =
    "[" "  [ \"a\", 0 ]," "  [ \"b\", 1 ]," "  [ \"c\", 2 ]" "]";

static const char jstr_arrarr_implicit[] =
    "[" "  [ \"letter\", \"number\"]," "  [ \"a\", 0 ]," "  [ \"b\", 1 ]," "  [ \"c\", 2 ]" "]";

static const char jstr_arrobj[] =
    "["
    "   {"
    "       \"name\": \"foo\","
    "       \"prop\": 1"
    "   },"
    "   {"
    "       \"prop\": 2,"
    "       \"name\": \"bar\""
    "   }," "   {" "       \"name\": \"baz\"," "       \"prop\": 3" "   }" "]";

static const char jstr_arrobj_bad[] =
    "[" "   {" "       \"name\": \"foo\"," "       \"name\": 1" "   }" "]";

static const char jstr_arrobj_bad2[] =
    "["
    "   {"
    "       \"name\": \"foo\","
    "       \"prop\": 1" "   }," "   {" "       \"name\": \"bar\"," "       \"name\": 2" "   }" "]";

static const char jstr_multitable[] =
    "{"
    "   \"tableA\": ["
    "       [ \"letter\", \"number\"],"
    "       [ \"a\", 0 ],"
    "       [ \"b\", 1 ],"
    "       [ \"c\", 2 ]"
    "   ],"
    "   \"tableB\": ["
    "       {"
    "           \"name\": \"foo\","
    "           \"prop\": 1"
    "       },"
    "       {"
    "           \"name\": \"bar\","
    "           \"prop\": 2"
    "       },"
    "       {" "           \"name\": \"baz\"," "           \"prop\": 3" "       }" "   ]" "}";

Test(base, t3__json_parse,.disabled = false,.description = "Valid JSON")
{
    const char *jstr = jstr_valid;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_assert(eq(int, ret, JSON_PARSE_OK));

}

Test(base, t4__valid_json_format_arrarr,.disabled = false,.description =
     "Valid array of arrays format")
{
    const char *jstr = jstr_arrarr_explicit;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrarr(ntok, tokens);
    cr_assert(eq(int, valid, true));

}

Test(base, t5__valid_json_format_arrarr,.disabled = false,.description = "Empty string")
{
    const char *jstr = "";
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrarr(ntok, tokens);
    cr_assert(eq(int, valid, false));

}

Test(base, t6__valid_json_format_arrarr,.disabled = false,.description =
     "Invalid array of arrays format")
{
    const char *jstr = jstr_valid;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrarr(ntok, tokens);
    cr_assert(eq(int, valid, false));

}

Test(base, t7__valid_json_format_arrobj,.disabled = false,.description =
     "Valid array of objects format")
{
    const char *jstr = jstr_arrobj;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrobj(ntok, tokens);
    cr_assert(eq(int, valid, true));

}

Test(base, t8__valid_json_format_arrobj,.disabled = false,.description = "Empty string")
{
    const char *jstr = "";
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrobj(ntok, tokens);
    cr_assert(eq(int, valid, false));

}

Test(base, t9__valid_json_format_arrobj,.disabled = false,.description =
     "Invalid array of objects format")
{
    const char *jstr = jstr_valid;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_format_arrobj(ntok, tokens);
    cr_assert(eq(int, valid, false));

}

Test(base, t10__parse_json_strval,.disabled = false,.description = "Unicode escape: spaces")
{
    static const char jstr[] = "\"\\u0020 \\u0020\"";
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(all(eq(int, ret, JSON_PARSE_OK), eq(sz, ntok, 1)
              ));
    char *val = parse_json_strval(jstr, &tokens[0]);
    cr_assert(eq(str, val, "   "));

}

Test(base, t11__parse_json_strval,.disabled = false,.description =
     "Unicode escape: universal character name")
{
    static const char jstr[] = "\"\\u0024\"";
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(all(eq(int, ret, JSON_PARSE_OK), eq(sz, ntok, 1)
              ));
    char *val = parse_json_strval(jstr, &tokens[0]);
    cr_assert(eq(str, val, "\u0024"));

}

Test(base, t12__parse_json_strval,.disabled = false,.description = "Unicode escape: high surrogate")
{
    static const char jstr[] = "\"\\uD805\"";
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(all(eq(int, ret, JSON_PARSE_OK), eq(sz, ntok, 1)
              ));
    char *val = parse_json_strval(jstr, &tokens[0]);
    cr_assert(eq(str, val, "\xED\xA0\x85"));

}

Test(base, t13__parse_json_format_arrarr,.disabled = false,.description =
     "Array of arrays (explicit columns)")
{
    char *jstr = strdup(jstr_arrarr_explicit);
    cr_expect(ne(ptr, jstr, NULL));

    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    {
        int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    size_t tab_ncol = 0;
    vec_vec_string *svv_rows = NULL;
    {
        int ret = parse_json_format_arrarr(jstr, ntok, tokens, &tab_ncol, &svv_rows);
        cr_assert(eq(int, ret, JSON_PARSE_OK));
    }

    cr_assert(all(eq(sz, sv_len(svv_rows), 3), eq(sz, tab_ncol, 2)
              ));
    const vec_string *row0 = sv_at_ptr(svv_rows, 0);
    const vec_string *row1 = sv_at_ptr(svv_rows, 1);
    const vec_string *row2 = sv_at_ptr(svv_rows, 2);

    cr_assert(all(eq(str, sv_at_ptr(row0, 0), "a"),
                  eq(str, sv_at_ptr(row0, 1), "0"),
                  eq(str, sv_at_ptr(row1, 0), "b"),
                  eq(str, sv_at_ptr(row1, 1), "1"),
                  eq(str, sv_at_ptr(row2, 0), "c"), eq(str, sv_at_ptr(row2, 1), "2")
              ));

}

Test(base, t14__parse_json_format_arrarr,.disabled = false,.description =
     "Array of arrays (implicit columns)")
{
    char *jstr = strdup(jstr_arrarr_implicit);
    cr_expect(ne(ptr, jstr, NULL));

    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    {
        int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    int ret;
    size_t tab_ncol = 0;
    vec_vec_string *svv_rows = NULL;
    ret = parse_json_format_arrarr(jstr, ntok, tokens, &tab_ncol, &svv_rows);
    cr_assert(eq(int, ret, JSON_PARSE_OK));

    size_t nnondef = 0;
    vec_namarg *sv_colnarg = NULL;
    ret = table_firstrow_colnames(&svv_rows, &sv_colnarg, &nnondef);
    cr_expect(eq(int, ret, 0));

    cr_assert(all(eq(sz, sv_len(svv_rows), 3),
                  eq(sz, tab_ncol, 2),
                  eq(sz, sv_len(sv_colnarg), tab_ncol),
                  eq(str, VNARG_GETNAME(sv_colnarg, 0), "letter"),
                  eq(str, VNARG_GETNAME(sv_colnarg, 1), "number")
              ));
    const vec_string *row0 = sv_at_ptr(svv_rows, 0);
    const vec_string *row1 = sv_at_ptr(svv_rows, 1);
    const vec_string *row2 = sv_at_ptr(svv_rows, 2);

    cr_assert(all(eq(str, sv_at_ptr(row0, 0), "a"),
                  eq(str, sv_at_ptr(row0, 1), "0"),
                  eq(str, sv_at_ptr(row1, 0), "b"),
                  eq(str, sv_at_ptr(row1, 1), "1"),
                  eq(str, sv_at_ptr(row2, 0), "c"), eq(str, sv_at_ptr(row2, 1), "2")
              ));

}

Test(base, t15__parse_json_format_arrobj,.disabled = false,.description = "Array of objects")
{
    char *jstr = strdup(jstr_arrobj);
    cr_expect(ne(ptr, jstr, NULL));

    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    {
        int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    size_t ierr = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    {
        int ret = parse_json_format_arrobj(jstr, ntok, tokens, &sv_colnarg, &svv_rows, &ierr);
        cr_assert(eq(int, ret, JSON_PARSE_OK));
    }

    cr_assert(all(eq(sz, sv_len(svv_rows), 3),
                  eq(sz, sv_len(sv_colnarg), 2),
                  eq(str, VNARG_GETNAME(sv_colnarg, 0), "name"),
                  eq(str, VNARG_GETNAME(sv_colnarg, 1), "prop")
              ));
    const vec_string *row0 = sv_at_ptr(svv_rows, 0);
    const vec_string *row1 = sv_at_ptr(svv_rows, 1);
    const vec_string *row2 = sv_at_ptr(svv_rows, 2);

    cr_assert(all(eq(str, sv_at_ptr(row0, 0), "foo"),
                  eq(str, sv_at_ptr(row0, 1), "1"),
                  eq(str, sv_at_ptr(row1, 0), "bar"),
                  eq(str, sv_at_ptr(row1, 1), "2"),
                  eq(str, sv_at_ptr(row2, 0), "baz"), eq(str, sv_at_ptr(row2, 1), "3")
              ));

}

Test(base, t16__parse_json_format_arrobj,.disabled = false,.description =
     "Array of objects, invalid duplicate column in first row")
{
    char *jstr = strdup(jstr_arrobj_bad);
    cr_expect(ne(ptr, jstr, NULL));

    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    {
        int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    size_t ierr = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    int ret = parse_json_format_arrobj(jstr, ntok, tokens, &sv_colnarg, &svv_rows, &ierr);
    cr_assert(all(eq(int, ret, JSON_PARSE_DUPLICATE), eq(sz, ierr, 0)
              ));

}

Test(base, t17__parse_json_format_arrobj,.disabled = false,.description =
     "Array of objects, invalid duplicate column in subsequent row")
{
    char *jstr = strdup(jstr_arrobj_bad2);
    cr_expect(ne(ptr, jstr, NULL));

    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    {
        int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    size_t ierr = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    int ret = parse_json_format_arrobj(jstr, ntok, tokens, &sv_colnarg, &svv_rows, &ierr);
    cr_assert(all(eq(int, ret, JSON_PARSE_DUPLICATE), eq(sz, ierr, 0)
              ));

}

Test(base, t18__table_json_parse,.disabled = false,.description = "Array of arrays")
{
    const char *jstr = jstr_arrarr_explicit;
    size_t jstrlen = strlen(jstr);

    size_t ierr;
    size_t tab_ncol = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    {
        int ret = table_json_parse(jstr, jstrlen, &tab_ncol, &svv_rows, &sv_colnarg, &ierr);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    cr_assert(all(eq(sz, sv_len(svv_rows), 3), eq(sz, tab_ncol, 2), eq(sz, sv_len(sv_colnarg), 0)
              ));
    const vec_string *row0 = sv_at_ptr(svv_rows, 0);
    const vec_string *row1 = sv_at_ptr(svv_rows, 1);
    const vec_string *row2 = sv_at_ptr(svv_rows, 2);

    cr_assert(all(eq(str, sv_at_ptr(row0, 0), "a"),
                  eq(str, sv_at_ptr(row0, 1), "0"),
                  eq(str, sv_at_ptr(row1, 0), "b"),
                  eq(str, sv_at_ptr(row1, 1), "1"),
                  eq(str, sv_at_ptr(row2, 0), "c"), eq(str, sv_at_ptr(row2, 1), "2")
              ));

}

Test(base, t19__table_json_parse,.disabled = false,.description = "Array of objects")
{
    const char *jstr = jstr_arrobj;
    size_t jstrlen = strlen(jstr);

    size_t ierr;
    size_t tab_ncol = 0;
    vec_namarg *sv_colnarg = NULL;
    vec_vec_string *svv_rows = NULL;
    {
        int ret = table_json_parse(jstr, jstrlen, &tab_ncol, &svv_rows, &sv_colnarg, &ierr);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    cr_assert(all(eq(sz, sv_len(svv_rows), 3),
                  eq(sz, tab_ncol, 2),
                  eq(sz, sv_len(sv_colnarg), 2),
                  eq(str, VNARG_GETNAME(sv_colnarg, 0), "name"),
                  eq(str, VNARG_GETNAME(sv_colnarg, 1), "prop")
              ));
    const vec_string *row0 = sv_at_ptr(svv_rows, 0);
    const vec_string *row1 = sv_at_ptr(svv_rows, 1);
    const vec_string *row2 = sv_at_ptr(svv_rows, 2);

    cr_assert(all(eq(str, sv_at_ptr(row0, 0), "foo"),
                  eq(str, sv_at_ptr(row0, 1), "1"),
                  eq(str, sv_at_ptr(row1, 0), "bar"),
                  eq(str, sv_at_ptr(row1, 1), "2"),
                  eq(str, sv_at_ptr(row2, 0), "baz"), eq(str, sv_at_ptr(row2, 1), "3")
              ));

}

Test(base, t20__valid_json_multitable_format,.disabled = false,.description =
     "Valid multitable format")
{
    const char *jstr = jstr_multitable;
    size_t ntok = 0;
    jsmntok_t *tokens = NULL;
    int ret = json_parse(strlen(jstr), jstr, &tokens, &ntok, NULL);
    cr_expect(eq(int, ret, JSON_PARSE_OK));
    bool valid = valid_json_multitable_format(ntok, tokens);
    cr_assert(eq(int, valid, true));

}

Test(base, t21__multitable_json_parse,.disabled = false,.description = "Multiple tables")
{
    const char *jstr = jstr_multitable;
    size_t jstrlen = strlen(jstr);

    size_t ierr;
    vec_table *(tables) = sv_alloc(sizeof(struct table), 2, NULL);
    if (sv_void == (tables)) {
    }

    cr_expect(ne(ptr, tables, NULL));
    {
        int ret = multitable_json_parse(jstr, jstrlen, &tables, &ierr);
        cr_expect(eq(int, ret, JSON_PARSE_OK));
    }

    cr_assert(eq(sz, sv_len(tables), 2));
    const struct table *tabA = sv_at(tables, 0);
    const struct table *tabB = sv_at(tables, 1);

    cr_assert(all(eq(str, tabA->name, "tableA"),
                  eq(sz, TABLE_NCOLS(*tabA), 2),
                  eq(sz, TABLE_NROWS(*tabA), 3),
                  eq(str, tabB->name, "tableB"),
                  eq(sz, TABLE_NCOLS(*tabB), 2), eq(sz, TABLE_NROWS(*tabB), 3)
              ));

}
