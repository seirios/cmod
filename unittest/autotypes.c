#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include "cmod.ext.h"
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"
#include "parts/autoarr_arr_cdi.h"
#include "parts/autoarr_arr_cdd.h"
#include "parts/variant_mod_case.h"
#include "util_string.h"
#ifndef CMOD_AUTOTYPES_H
#define CMOD_AUTOTYPES_H
/* tagged unions */
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
/* C parsing */
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"

#include "parts/autoarr_arr_cdi.h"

#include "parts/autoarr_arr_cdd.h"

srt_string *(cini_to_str) (const struct c_initializer * ini);
int (arr_cdi_to_str) (const arr_cdi * x, const vec_string * name, srt_string * buf[static 1],
                      bool is_comparison);
int (arr_cdd_to_decl) (arr_cdd * x, struct decl * decl);
srt_string *(decl_to_str) (const struct decl * decl, const char *fname, const char *fsuffix,
                           bool is_statement, bool const_named);
/* C% switch */
#include "parts/variant_mod_case.h"
#endif

struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };

char *cr_user_xint_tostr(const struct xint *val)
{
    char buf[128] = { 0 };
    switch (val->type) {
    case ENUM_XINT(INVALID):
        (void)snprintf(buf, sizeof(buf), "xint (%d) invalid", val->type);
        break;
    case ENUM_XINT(UNSIGNED):
        (void)snprintf(buf, sizeof(buf), "xint (%d) = %" PRIuMAX, val->type, val->value.Unsigned);
        break;
    case ENUM_XINT(SIGNED):
        (void)snprintf(buf, sizeof(buf), "xint (%d) = %" PRIiMAX, val->type, val->value.Signed);
        break;
    }
    return strdup(buf);
}

int cr_user_xint_eq(const struct xint *lhs, const struct xint *rhs)
{
    if (lhs->type != rhs->type)
        return false;
    switch (lhs->type) {
    case ENUM_XINT(INVALID):
        break;
    case ENUM_XINT(UNSIGNED):
        if (lhs->value.Unsigned != rhs->value.Unsigned)
            return false;
        break;
    case ENUM_XINT(SIGNED):
        if (lhs->value.Signed != rhs->value.Signed)
            return false;
        break;
    }
    return true;
}

Test(base, t3__xint,.disabled = false)
{
    struct xint xi = {.type = ENUM_XINT(INVALID) };
    struct xint xu = XINT_UNSIGNED_set(123456);
    struct xint xs = XINT_SIGNED_set(123456);
    {
        struct xint test = xi;
        xint_free(&test);
        cr_assert(eq(type(struct xint), test, (struct xint) { 0 }));
    }
    cr_assert(ne(type(struct xint), xu, xs));

}
