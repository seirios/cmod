#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <strings.h>
#include "cmod.ext.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include "retval.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#if HAVE_LIBSECCOMP

#include <seccomp.h>
#include <sys/ptrace.h>
#ifndef SYS_SECCOMP
#define SYS_SECCOMP 1
#endif                          // SYS_SECCOMP
#endif                          // HAVE_LIBSECCOMP

#include "array_type.h"
#include <signal.h>
#include <stdbool.h>
#include "keyword_opts.h"
#include "parts/struct_param.h"
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"
#include "parts/autoarr_arr_cdi.h"
#include "parts/autoarr_arr_cdd.h"
#include "parts/variant_mod_case.h"
#include "parts/struct_iftrie_params.h"
#include "parts/typedef_namix.h"
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include "util_string.h"
#include "util_snippet.h"
struct param *glob_pp = &(struct param) { 0 };
struct cmod_ralloc_params ralloc_glob = { 0 };

Test(base, t4__cmp_namix_by_name,.disabled = false)
{
    namix arr[4] = {
        {.name = NULL},
        {.name = "bar"},
        {.name = "foo"},
        {.name = "baz"}
    };
    qsort(arr, 4, sizeof(*arr), cmp_namix_by_name);
    cr_assert(all(eq(str, arr[0].name, "bar"),
                  eq(str, arr[1].name, "baz"),
                  eq(str, arr[2].name, "foo"), eq(ptr, arr[3].name, NULL)
              ));

}

Test(base, t5__cmp_namix_as_float,.disabled = false)
{
    namix arr[4] = {
        {.name = "12"},
        {.name = "bad"},
        {.name = "135.1"},
        {.name = "3.1416"}
    };
    qsort(arr, 4, sizeof(*arr), cmp_namix_as_float);
    cr_assert(all(eq(str, arr[0].name, "3.1416"),
                  eq(str, arr[1].name, "12"),
                  eq(str, arr[2].name, "135.1"), eq(ptr, arr[3].name, "bad")
              ));

}

Test(base, t6__cmp_namix_as_int,.disabled = false)
{
    glob_pp->cmp_base = 8;
    namix arr[4] = {
        {.name = "07"},
        {.name = "02"},
        {.name = "10"},
        {.name = "05"}
    };
    qsort(arr, 4, sizeof(*arr), cmp_namix_as_int);
    cr_assert(all(eq(str, arr[0].name, "02"),
                  eq(str, arr[1].name, "05"), eq(str, arr[2].name, "07"), eq(ptr, arr[3].name, "10")
              ));

}

#include <stdio.h>
