#include <string.h>
#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include "table_tsv.h"
#include "parser.h"
#include "scanner.h"
struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };

Test(base, t2__table_tsv_parse,.disabled = false,.description = "Correct input (3x2 table)")
{
    const char tsv[] = "a\tb\tc\n1\t2\t3\n";

    size_t ncol = 0;
    vec_vec_string *tbody = NULL;
    int ret = table_tsv_parse(tsv, strlen(tsv), &ncol, &tbody);
    cr_assert(all(eq(int, ret, TSV_PARSE_OK), eq(sz, sv_len(tbody), 2), eq(sz, ncol, 3)
              ));

}

Test(base, t3__table_tsv_parse,.disabled = false,.description = "Invalid short second row")
{
    const char tsv[] = "a\tb\tc\n1\t2\n";

    size_t ncol = 0;
    vec_vec_string *tbody = NULL;
    int ret = table_tsv_parse(tsv, strlen(tsv), &ncol, &tbody);
    cr_assert(all(eq(int, ret, TSV_PARSE_BADROW), eq(sz, sv_len(tbody), 0), eq(sz, ncol, 0)
              ));

}

Test(base, t4__table_tsv_parse,.disabled = false,.description = "Empty table")
{
    const char tsv[] = "";

    size_t ncol = 0;
    vec_vec_string *tbody = NULL;
    int ret = table_tsv_parse(tsv, strlen(tsv), &ncol, &tbody);
    cr_assert(all(eq(int, ret, TSV_PARSE_OK), eq(sz, sv_len(tbody), 0), eq(sz, ncol, 0)
              ));

}

Test(base, t5__table_tsv_parse,.disabled = false,.description = "Embedded NUL")
{
    const char tsv[] = "\0\n";

    size_t ncol = 0;
    vec_vec_string *tbody = NULL;
    int ret = table_tsv_parse(tsv, 2, &ncol, &tbody);
    cr_assert(all(eq(int, ret, TSV_PARSE_OK), eq(sz, sv_len(tbody), 1), eq(sz, ncol, 1)
              ));

}
