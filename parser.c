/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* "%code top" blocks.  */
#line 129 "parser.y"


#include <assert.h>
#include <inttypes.h>

#include "cmod.ext.h"
#include "parser_api.h"
#include "parser.h"
#define YY_HEADER_EXPORT_START_CONDITIONS
#include "scanner.h"
#include "filestack.h"
#include "validate_utf8.h"
#include "util_string.h"
#include "util_scanner.h"
#include "util_snippet.h"

#include "table_tsv.h"
#include "table_json.h"
#include "table_parse.h"
#define JSMN_HEADER
#include "jsmn.h"

#include "array_type.h"


/* logging macros */
#ifndef INFO0
#define INFO0(x) \
    { if(!(pp->silent > 0))\
        yyinfo(yyget_lloc(yyscanner),yyscanner,NULL,x); }
#endif
#ifndef INFO
#define INFO(x, ...) \
    { if(!(pp->silent > 0))\
        yyinfo(yyget_lloc(yyscanner),yyscanner,NULL,x,__VA_ARGS__); }
#endif
#ifndef WARN0
#define WARN0(x) \
    { if(!(pp->silent > 1))\
        yywarn(yyget_lloc(yyscanner),yyscanner,NULL,x); }
#endif
#ifndef WARN
#define WARN(x, ...) \
    { if(!(pp->silent > 1))\
        yywarn(yyget_lloc(yyscanner),yyscanner,NULL,x,__VA_ARGS__); }
#endif
#ifndef ERROR0
#define ERROR0(x) \
    { if(!(pp->silent > 2))\
        yyerror(yyget_lloc(yyscanner),yyscanner,NULL,x); }
#endif
#ifndef ERROR
#define ERROR(x, ...) \
    { if(!(pp->silent > 2))\
        yyerror(yyget_lloc(yyscanner),yyscanner,NULL,x,__VA_ARGS__); }
#endif
#ifndef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!pp->verbose))\
        yyverbose(yyget_lloc(yyscanner),yyscanner,NULL,x); }
#endif
#ifndef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!pp->verbose))\
        yyverbose(yyget_lloc(yyscanner),yyscanner,NULL,x,__VA_ARGS__); }
#endif
#ifndef DEBUG0
#define DEBUG0(x) \
    { if(!(pp->silent > 3))\
        yydebug(yyget_lloc(yyscanner),yyscanner,NULL,x); }
#endif
#ifndef DEBUG
#define DEBUG(x, ...) \
    { if(!(pp->silent > 3))\
        yydebug(yyget_lloc(yyscanner),yyscanner,NULL,x,__VA_ARGS__); }
#endif

#ifndef ABORT_PARSE
#define ABORT_PARSE(x, ...) { \
    pp->errlloc = yylloc; \
    ERROR(x,__VA_ARGS__); \
if(!(pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error_line(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif
#ifndef ABORT_PARSE0
#define ABORT_PARSE0(x) { \
    pp->errlloc = yylloc; \
    ERROR0(x); \
if(!(pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error_line(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif
#ifndef ABORT_PARSE_LOC
#define ABORT_PARSE_LOC(x, ...) { \
    ERROR(x,__VA_ARGS__); \
if(!(pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error_line(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif
#ifndef ABORT_PARSE_LOC0
#define ABORT_PARSE_LOC0(x) { \
    ERROR0(x); \
if(!(pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error_line(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif



static int get_error_context(const char*, size_t, size_t nline, srt_string *[static nline]);
static void print_error_line(YYLTYPE*, void*);

#line 194 "parser.c"




# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_SCANNER_ERROR = 3,              /* SCANNER_ERROR  */
  YYSYMBOL_4_newline_ = 4,                 /* "newline"  */
  YYSYMBOL_TSV_VALUE = 5,                  /* "TSV value"  */
  YYSYMBOL_CHAR = 6,                       /* "character"  */
  YYSYMBOL_DLLARG_TRANSFORMS = 7,          /* "argument modifiers"  */
  YYSYMBOL_C_CONST = 8,                    /* "const"  */
  YYSYMBOL_C_RESTRICT = 9,                 /* "restrict"  */
  YYSYMBOL_C_VOLATILE = 10,                /* "volatile"  */
  YYSYMBOL_C_ATOMIC = 11,                  /* "_Atomic"  */
  YYSYMBOL_C_VOID = 12,                    /* "void"  */
  YYSYMBOL_C_CHAR = 13,                    /* "char"  */
  YYSYMBOL_C_SHORT = 14,                   /* "short"  */
  YYSYMBOL_C_INT = 15,                     /* "int"  */
  YYSYMBOL_C_LONG = 16,                    /* "long"  */
  YYSYMBOL_C_SIGNED = 17,                  /* "signed"  */
  YYSYMBOL_C_UNSIGNED = 18,                /* "unsigned"  */
  YYSYMBOL_C_BOOL = 19,                    /* "_Bool"  */
  YYSYMBOL_C_FLOAT = 20,                   /* "float"  */
  YYSYMBOL_C_DOUBLE = 21,                  /* "double"  */
  YYSYMBOL_C_COMPLEX = 22,                 /* "_Complex"  */
  YYSYMBOL_C_STRUCT = 23,                  /* "struct"  */
  YYSYMBOL_C_UNION = 24,                   /* "union"  */
  YYSYMBOL_C_ENUM = 25,                    /* "enum"  */
  YYSYMBOL_C_TYPEDEF = 26,                 /* "typedef"  */
  YYSYMBOL_C_EXTERN = 27,                  /* "extern"  */
  YYSYMBOL_C_STATIC = 28,                  /* "static"  */
  YYSYMBOL_C_THREAD_LOCAL = 29,            /* "_Thread_local"  */
  YYSYMBOL_C_AUTO = 30,                    /* "auto"  */
  YYSYMBOL_C_REGISTER = 31,                /* "register"  */
  YYSYMBOL_C_INLINE = 32,                  /* "inline"  */
  YYSYMBOL_C_NORETURN = 33,                /* "_Noreturn"  */
  YYSYMBOL_C_ALIGNAS = 34,                 /* "_Alignas"  */
  YYSYMBOL_C_IF = 35,                      /* "if"  */
  YYSYMBOL_C_ELSE = 36,                    /* "else"  */
  YYSYMBOL_C_SWITCH = 37,                  /* "switch"  */
  YYSYMBOL_C_DO = 38,                      /* "do"  */
  YYSYMBOL_C_FOR = 39,                     /* "for"  */
  YYSYMBOL_C_WHILE = 40,                   /* "while"  */
  YYSYMBOL_C_GOTO = 41,                    /* "goto"  */
  YYSYMBOL_C_CONTINUE = 42,                /* "continue"  */
  YYSYMBOL_C_BREAK = 43,                   /* "break"  */
  YYSYMBOL_C_RETURN = 44,                  /* "return"  */
  YYSYMBOL_C_CASE = 45,                    /* "case"  */
  YYSYMBOL_C_DEFAULT = 46,                 /* "default"  */
  YYSYMBOL_C_GENERIC = 47,                 /* "_Generic"  */
  YYSYMBOL_C_STATIC_ASSERT = 48,           /* "_Static_assert"  */
  YYSYMBOL_C_FUNC_NAME = 49,               /* "__func__"  */
  YYSYMBOL_C_SIZEOF = 50,                  /* "sizeof"  */
  YYSYMBOL_C_ALIGNOF = 51,                 /* "_Alignof"  */
  YYSYMBOL_C_IMAGINARY = 52,               /* "_Imaginary"  */
  YYSYMBOL_53_ = 53,                       /* '='  */
  YYSYMBOL_C_MUL_ASSIGN = 54,              /* "*="  */
  YYSYMBOL_C_DIV_ASSIGN = 55,              /* "/="  */
  YYSYMBOL_C_MOD_ASSIGN = 56,              /* "%="  */
  YYSYMBOL_C_ADD_ASSIGN = 57,              /* "+="  */
  YYSYMBOL_C_SUB_ASSIGN = 58,              /* "-="  */
  YYSYMBOL_C_LEFT_ASSIGN = 59,             /* "<<="  */
  YYSYMBOL_C_RIGHT_ASSIGN = 60,            /* ">>="  */
  YYSYMBOL_C_AND_ASSIGN = 61,              /* "&="  */
  YYSYMBOL_C_XOR_ASSIGN = 62,              /* "^="  */
  YYSYMBOL_C_OR_ASSIGN = 63,               /* "|="  */
  YYSYMBOL_C_LEFT_OP = 64,                 /* "<<"  */
  YYSYMBOL_C_RIGHT_OP = 65,                /* ">>"  */
  YYSYMBOL_C_INCR_OP = 66,                 /* "++"  */
  YYSYMBOL_C_DECR_OP = 67,                 /* "--"  */
  YYSYMBOL_68_ = 68,                       /* '.'  */
  YYSYMBOL_C_PTR_OP = 69,                  /* "->"  */
  YYSYMBOL_C_AND_OP = 70,                  /* "&&"  */
  YYSYMBOL_C_OR_OP = 71,                   /* "||"  */
  YYSYMBOL_72_ = 72,                       /* '!'  */
  YYSYMBOL_73_ = 73,                       /* '<'  */
  YYSYMBOL_74_ = 74,                       /* '>'  */
  YYSYMBOL_C_LE_OP = 75,                   /* "<="  */
  YYSYMBOL_C_GE_OP = 76,                   /* ">="  */
  YYSYMBOL_C_EQ_OP = 77,                   /* "=="  */
  YYSYMBOL_C_NE_OP = 78,                   /* "!="  */
  YYSYMBOL_79_ = 79,                       /* '&'  */
  YYSYMBOL_80_ = 80,                       /* '^'  */
  YYSYMBOL_81_ = 81,                       /* '|'  */
  YYSYMBOL_82_ = 82,                       /* '~'  */
  YYSYMBOL_83_ = 83,                       /* '+'  */
  YYSYMBOL_84_ = 84,                       /* '-'  */
  YYSYMBOL_85_ = 85,                       /* '*'  */
  YYSYMBOL_86_ = 86,                       /* '/'  */
  YYSYMBOL_87_ = 87,                       /* '%'  */
  YYSYMBOL_88_ = 88,                       /* '?'  */
  YYSYMBOL_89_ = 89,                       /* ':'  */
  YYSYMBOL_90_ = 90,                       /* '{'  */
  YYSYMBOL_91_ = 91,                       /* '}'  */
  YYSYMBOL_92_ = 92,                       /* '('  */
  YYSYMBOL_93_ = 93,                       /* ')'  */
  YYSYMBOL_94_ = 94,                       /* '['  */
  YYSYMBOL_95_ = 95,                       /* ']'  */
  YYSYMBOL_96_ = 96,                       /* ','  */
  YYSYMBOL_97_ = 97,                       /* ';'  */
  YYSYMBOL_C_ELLIPSIS = 98,                /* "..."  */
  YYSYMBOL_C_IDENTIFIER = 99,              /* "C identifier"  */
  YYSYMBOL_C_TYPEDEF_NAME = 100,           /* "C typedef name"  */
  YYSYMBOL_C_ENUMERATION_CONSTANT = 101,   /* "C enum constant"  */
  YYSYMBOL_C_STRING_LITERAL = 102,         /* "C string literal"  */
  YYSYMBOL_C_INT_CONSTANT = 103,           /* "C integer constant"  */
  YYSYMBOL_C_FP_CONSTANT = 104,            /* "C floating-point constant"  */
  YYSYMBOL_MOD_INCLUDE = 105,              /* "% include"  */
  YYSYMBOL_MOD_ONCE = 106,                 /* "% once"  */
  YYSYMBOL_MOD_SNIPPET = 107,              /* "% snippet"  */
  YYSYMBOL_MOD_RECALL = 108,               /* "% recall"  */
  YYSYMBOL_MOD_TABLE = 109,                /* "% table"  */
  YYSYMBOL_MOD_TABLE_STACK = 110,          /* "% table-stack"  */
  YYSYMBOL_MOD_MAP = 111,                  /* "% map"  */
  YYSYMBOL_MOD_PIPE = 112,                 /* "% pipe"  */
  YYSYMBOL_MOD_UNITTEST = 113,             /* "% unittest"  */
  YYSYMBOL_MOD_DSL_DEF = 114,              /* "% dsl-def"  */
  YYSYMBOL_MOD_DSL = 115,                  /* "% dsl"  */
  YYSYMBOL_MOD_INTOP = 116,                /* "% intop"  */
  YYSYMBOL_MOD_DELAY = 117,                /* "% delay"  */
  YYSYMBOL_MOD_DEFINED = 118,              /* "% defined"  */
  YYSYMBOL_MOD_STRCMP = 119,               /* "% strcmp"  */
  YYSYMBOL_MOD_STRSTR = 120,               /* "% strstr"  */
  YYSYMBOL_MOD_STRLEN = 121,               /* "% strlen"  */
  YYSYMBOL_MOD_STRSUB = 122,               /* "% strsub"  */
  YYSYMBOL_MOD_TABLE_SIZE = 123,           /* "% table-size"  */
  YYSYMBOL_MOD_TABLE_LENGTH = 124,         /* "% table-length"  */
  YYSYMBOL_MOD_TABLE_GET = 125,            /* "% table-get"  */
  YYSYMBOL_MOD_TYPEDEF = 126,              /* "% typedef"  */
  YYSYMBOL_MOD_PROTO = 127,                /* "% proto"  */
  YYSYMBOL_MOD_DEF = 128,                  /* "% def"  */
  YYSYMBOL_MOD_UNUSED = 129,               /* "% unused"  */
  YYSYMBOL_MOD_PREFIX = 130,               /* "% prefix"  */
  YYSYMBOL_MOD_ENUM = 131,                 /* "% enum"  */
  YYSYMBOL_MOD_STRIN = 132,                /* "% strin"  */
  YYSYMBOL_MOD_FOREACH = 133,              /* "% foreach"  */
  YYSYMBOL_MOD_SWITCH = 134,               /* "% switch"  */
  YYSYMBOL_MOD_FREE = 135,                 /* "% free"  */
  YYSYMBOL_MOD_ARRLEN = 136,               /* "% arrlen"  */
  YYSYMBOL_MOD_RECALL_END = 137,           /* "|%"  */
  YYSYMBOL_FROM = 138,                     /* "<-"  */
  YYSYMBOL_PERIOD = 139,                   /* "."  */
  YYSYMBOL_MOD_BRACE_OPEN = 140,           /* "%{"  */
  YYSYMBOL_MOD_BRACE_CLOSE = 141,          /* "%}"  */
  YYSYMBOL_MOD_HERESTR_OPEN = 142,         /* "%<<"  */
  YYSYMBOL_MOD_HERESTR_CLOSE = 143,        /* ">>%"  */
  YYSYMBOL_MOD_HERESTR_EMPTY = 144,        /* "%nul"  */
  YYSYMBOL_MOD_ROWNO = 145,                /* "%NR"  */
  YYSYMBOL_OPT_ASSIGN = 146,               /* "?="  */
  YYSYMBOL_DEF_ASSIGN = 147,               /* ":="  */
  YYSYMBOL_ASTERISK = 148,                 /* "*"  */
  YYSYMBOL_MOD_TABLE_OPEN = 149,           /* "%TSV|JSON{"  */
  YYSYMBOL_DLLARG_SPECIAL = 150,           /* "special argument reference"  */
  YYSYMBOL_DLLARG = 151,                   /* "argument reference"  */
  YYSYMBOL_RANGE = 152,                    /* "integer range"  */
  YYSYMBOL_IDENTIFIER = 153,               /* "identifier"  */
  YYSYMBOL_IDENTIFIER_EXT = 154,           /* "extended identifier"  */
  YYSYMBOL_SPECIAL_IDENTIFIER = 155,       /* "special identifier"  */
  YYSYMBOL_FILE_PATH = 156,                /* "file path"  */
  YYSYMBOL_SIGNED_INT = 157,               /* "signed integer"  */
  YYSYMBOL_UNSIGNED_INT = 158,             /* "unsigned integer"  */
  YYSYMBOL_159_table_like_ = 159,          /* "table-like"  */
  YYSYMBOL_160_recall_like_ = 160,         /* "recall-like"  */
  YYSYMBOL_161_snippet_like_ = 161,        /* "snippet-like"  */
  YYSYMBOL_162_ = 162,                     /* '$'  */
  YYSYMBOL_163_ = 163,                     /* '#'  */
  YYSYMBOL_YYACCEPT = 164,                 /* $accept  */
  YYSYMBOL_opt_c_identifier = 165,         /* opt_c_identifier  */
  YYSYMBOL_opt_comma = 166,                /* opt_comma  */
  YYSYMBOL_c_name = 167,                   /* c_name  */
  YYSYMBOL_opt_c_name = 168,               /* opt_c_name  */
  YYSYMBOL_c_name_comma_list = 169,        /* c_name_comma_list  */
  YYSYMBOL_numeric_constant = 170,         /* numeric_constant  */
  YYSYMBOL_enumeration_constant = 171,     /* enumeration_constant  */
  YYSYMBOL_string = 172,                   /* string  */
  YYSYMBOL_primary_expression = 173,       /* primary_expression  */
  YYSYMBOL_postfix_expression = 174,       /* postfix_expression  */
  YYSYMBOL_opt_argument_expression_list = 175, /* opt_argument_expression_list  */
  YYSYMBOL_argument_expression_list = 176, /* argument_expression_list  */
  YYSYMBOL_unary_expression = 177,         /* unary_expression  */
  YYSYMBOL_unary_operator = 178,           /* unary_operator  */
  YYSYMBOL_cast_expression = 179,          /* cast_expression  */
  YYSYMBOL_multiplicative_expression = 180, /* multiplicative_expression  */
  YYSYMBOL_additive_expression = 181,      /* additive_expression  */
  YYSYMBOL_shift_expression = 182,         /* shift_expression  */
  YYSYMBOL_relational_expression = 183,    /* relational_expression  */
  YYSYMBOL_equality_expression = 184,      /* equality_expression  */
  YYSYMBOL_and_expression = 185,           /* and_expression  */
  YYSYMBOL_exclusive_or_expression = 186,  /* exclusive_or_expression  */
  YYSYMBOL_inclusive_or_expression = 187,  /* inclusive_or_expression  */
  YYSYMBOL_logical_and_expression = 188,   /* logical_and_expression  */
  YYSYMBOL_logical_or_expression = 189,    /* logical_or_expression  */
  YYSYMBOL_conditional_expression = 190,   /* conditional_expression  */
  YYSYMBOL_opt_assignment_expression = 191, /* opt_assignment_expression  */
  YYSYMBOL_assignment_expression = 192,    /* assignment_expression  */
  YYSYMBOL_assignment_operator = 193,      /* assignment_operator  */
  YYSYMBOL_expression = 194,               /* expression  */
  YYSYMBOL_constant_expression = 195,      /* constant_expression  */
  YYSYMBOL_constant_expression_flat = 196, /* constant_expression_flat  */
  YYSYMBOL_declaration = 197,              /* declaration  */
  YYSYMBOL_declaration_specifier = 198,    /* declaration_specifier  */
  YYSYMBOL_declaration_specifiers = 199,   /* declaration_specifiers  */
  YYSYMBOL_declaration_specifiers_checked = 200, /* declaration_specifiers_checked  */
  YYSYMBOL_storage_class_specifier = 201,  /* storage_class_specifier  */
  YYSYMBOL_simple_type_specifier = 202,    /* simple_type_specifier  */
  YYSYMBOL_type_specifier = 203,           /* type_specifier  */
  YYSYMBOL_struct_or_union_specifier = 204, /* struct_or_union_specifier  */
  YYSYMBOL_struct_or_union = 205,          /* struct_or_union  */
  YYSYMBOL_struct_declaration_list = 206,  /* struct_declaration_list  */
  YYSYMBOL_struct_declaration = 207,       /* struct_declaration  */
  YYSYMBOL_specifier_qualifier_list = 208, /* specifier_qualifier_list  */
  YYSYMBOL_opt_struct_declarator_list = 209, /* opt_struct_declarator_list  */
  YYSYMBOL_struct_declarator_list = 210,   /* struct_declarator_list  */
  YYSYMBOL_struct_declarator = 211,        /* struct_declarator  */
  YYSYMBOL_enum_specifier = 212,           /* enum_specifier  */
  YYSYMBOL_enumerator_list = 213,          /* enumerator_list  */
  YYSYMBOL_enumerator = 214,               /* enumerator  */
  YYSYMBOL_atomic_type_specifier = 215,    /* atomic_type_specifier  */
  YYSYMBOL_type_qualifier = 216,           /* type_qualifier  */
  YYSYMBOL_function_specifier = 217,       /* function_specifier  */
  YYSYMBOL_alignment_specifier = 218,      /* alignment_specifier  */
  YYSYMBOL_declarator = 219,               /* declarator  */
  YYSYMBOL_direct_declarator = 220,        /* direct_declarator  */
  YYSYMBOL_array_direct_declarator = 221,  /* array_direct_declarator  */
  YYSYMBOL_function_direct_declarator = 222, /* function_direct_declarator  */
  YYSYMBOL_pointer = 223,                  /* pointer  */
  YYSYMBOL_pointer_elem = 224,             /* pointer_elem  */
  YYSYMBOL_opt_type_qualifier_list = 225,  /* opt_type_qualifier_list  */
  YYSYMBOL_type_qualifier_list = 226,      /* type_qualifier_list  */
  YYSYMBOL_parameter_type_list = 227,      /* parameter_type_list  */
  YYSYMBOL_parameter_type_list_flat = 228, /* parameter_type_list_flat  */
  YYSYMBOL_parameter_list = 229,           /* parameter_list  */
  YYSYMBOL_parameter_declaration = 230,    /* parameter_declaration  */
  YYSYMBOL_parameter_declaration_typed = 231, /* parameter_declaration_typed  */
  YYSYMBOL_opt_identifier_list = 232,      /* opt_identifier_list  */
  YYSYMBOL_identifier_list = 233,          /* identifier_list  */
  YYSYMBOL_type_name = 234,                /* type_name  */
  YYSYMBOL_abstract_declarator = 235,      /* abstract_declarator  */
  YYSYMBOL_direct_abstract_declarator = 236, /* direct_abstract_declarator  */
  YYSYMBOL_initializer = 237,              /* initializer  */
  YYSYMBOL_initializer_flat = 238,         /* initializer_flat  */
  YYSYMBOL_compound_initializer_list = 239, /* compound_initializer_list  */
  YYSYMBOL_compound_initializer = 240,     /* compound_initializer  */
  YYSYMBOL_compound_literal = 241,         /* compound_literal  */
  YYSYMBOL_braced_init_list = 242,         /* braced_init_list  */
  YYSYMBOL_initializer_list = 243,         /* initializer_list  */
  YYSYMBOL_designation = 244,              /* designation  */
  YYSYMBOL_designator = 245,               /* designator  */
  YYSYMBOL_paren_c_expression = 246,       /* paren_c_expression  */
  YYSYMBOL_c_expression_list = 247,        /* c_expression_list  */
  YYSYMBOL_paren_c_expression_list = 248,  /* paren_c_expression_list  */
  YYSYMBOL_opt_pipe = 249,                 /* opt_pipe  */
  YYSYMBOL_mod_assign = 250,               /* mod_assign  */
  YYSYMBOL_identifier_ext = 251,           /* identifier_ext  */
  YYSYMBOL_option = 252,                   /* option  */
  YYSYMBOL_option_comma_list = 253,        /* option_comma_list  */
  YYSYMBOL_option_comma_list_nodang = 254, /* option_comma_list_nodang  */
  YYSYMBOL_keyword_options = 255,          /* keyword_options  */
  YYSYMBOL_opt_keyword_options = 256,      /* opt_keyword_options  */
  YYSYMBOL_opt_identifier = 257,           /* opt_identifier  */
  YYSYMBOL_cmod_identifier = 258,          /* cmod_identifier  */
  YYSYMBOL_opt_cmod_identifier = 259,      /* opt_cmod_identifier  */
  YYSYMBOL_cmod_identifier_comma_list = 260, /* cmod_identifier_comma_list  */
  YYSYMBOL_cmod_identifier_comma_list_nodang = 261, /* cmod_identifier_comma_list_nodang  */
  YYSYMBOL_paren_cmod_identifier_comma_list = 262, /* paren_cmod_identifier_comma_list  */
  YYSYMBOL_special_id_list = 263,          /* special_id_list  */
  YYSYMBOL_special_id_list_nodang = 264,   /* special_id_list_nodang  */
  YYSYMBOL_paren_special_id_list = 265,    /* paren_special_id_list  */
  YYSYMBOL_integer = 266,                  /* integer  */
  YYSYMBOL_integer_string = 267,           /* integer_string  */
  YYSYMBOL_integer_comma_list = 268,       /* integer_comma_list  */
  YYSYMBOL_integer_comma_list_nodang = 269, /* integer_comma_list_nodang  */
  YYSYMBOL_paren_integer_comma_list = 270, /* paren_integer_comma_list  */
  YYSYMBOL_paren_unsigned_int = 271,       /* paren_unsigned_int  */
  YYSYMBOL_opt_UNSIGNED_INT = 272,         /* opt_UNSIGNED_INT  */
  YYSYMBOL_mod_herestring = 273,           /* mod_herestring  */
  YYSYMBOL_opt_mod_herestring = 274,       /* opt_mod_herestring  */
  YYSYMBOL_opt_herestring = 275,           /* opt_herestring  */
  YYSYMBOL_herestring = 276,               /* herestring  */
  YYSYMBOL_table_like = 277,               /* table_like  */
  YYSYMBOL_table = 278,                    /* table  */
  YYSYMBOL_table_selection = 279,          /* table_selection  */
  YYSYMBOL_heretab_verbatim_char_list = 280, /* heretab_verbatim_char_list  */
  YYSYMBOL_heretab_verbatim = 281,         /* heretab_verbatim  */
  YYSYMBOL_lambda_item = 282,              /* lambda_item  */
  YYSYMBOL_lambda_item_comma_list = 283,   /* lambda_item_comma_list  */
  YYSYMBOL_lambda_table_column = 284,      /* lambda_table_column  */
  YYSYMBOL_lambda_table_column_list = 285, /* lambda_table_column_list  */
  YYSYMBOL_lambda_table = 286,             /* lambda_table  */
  YYSYMBOL_recall_like = 287,              /* recall_like  */
  YYSYMBOL_recall_end = 288,               /* recall_end  */
  YYSYMBOL_snippet_like = 289,             /* snippet_like  */
  YYSYMBOL_opt_DLLARG = 290,               /* opt_DLLARG  */
  YYSYMBOL_dllarg = 291,                   /* dllarg  */
  YYSYMBOL_dllarg_list = 292,              /* dllarg_list  */
  YYSYMBOL_opt_dllarg_list = 293,          /* opt_dllarg_list  */
  YYSYMBOL_snippet_body = 294,             /* snippet_body  */
  YYSYMBOL_opt_snippet_body = 295,         /* opt_snippet_body  */
  YYSYMBOL_cmod_plain_argument = 296,      /* cmod_plain_argument  */
  YYSYMBOL_cmod_named_argument = 297,      /* cmod_named_argument  */
  YYSYMBOL_cmod_argument_comma_list = 298, /* cmod_argument_comma_list  */
  YYSYMBOL_cmod_argument_comma_list_nodang = 299, /* cmod_argument_comma_list_nodang  */
  YYSYMBOL_cmod_arguments = 300,           /* cmod_arguments  */
  YYSYMBOL_cmod_arguments_unique = 301,    /* cmod_arguments_unique  */
  YYSYMBOL_cmod_function_arguments = 302,  /* cmod_function_arguments  */
  YYSYMBOL_cmod_formal_arguments = 303,    /* cmod_formal_arguments  */
  YYSYMBOL_empty_cmod_arguments = 304,     /* empty_cmod_arguments  */
  YYSYMBOL_snippet_formal_arguments = 305, /* snippet_formal_arguments  */
  YYSYMBOL_snippet_formal_arguments_inout = 306, /* snippet_formal_arguments_inout  */
  YYSYMBOL_opt_table_columns = 307,        /* opt_table_columns  */
  YYSYMBOL_table_columns = 308,            /* table_columns  */
  YYSYMBOL_snippet_actual_arguments = 309, /* snippet_actual_arguments  */
  YYSYMBOL_snippet_actual_arguments_list = 310, /* snippet_actual_arguments_list  */
  YYSYMBOL_snippet_actual_arguments_inout = 311, /* snippet_actual_arguments_inout  */
  YYSYMBOL_snippet_actual_arguments_inout_idcap = 312, /* snippet_actual_arguments_inout_idcap  */
  YYSYMBOL_mod_case = 313,                 /* mod_case  */
  YYSYMBOL_case_list = 314,                /* case_list  */
  YYSYMBOL_opt_case_list = 315,            /* opt_case_list  */
  YYSYMBOL_switch_body = 316,              /* switch_body  */
  YYSYMBOL_cmod_file = 317,                /* cmod_file  */
  YYSYMBOL_mod_statement_list = 318,       /* mod_statement_list  */
  YYSYMBOL_print_mod_statement = 319,      /* print_mod_statement  */
  YYSYMBOL_mod_statement = 320,            /* mod_statement  */
  YYSYMBOL_kw_defined = 321,               /* kw_defined  */
  YYSYMBOL_kw_delay = 322,                 /* kw_delay  */
  YYSYMBOL_kw_dsl = 323,                   /* kw_dsl  */
  YYSYMBOL_kw_dsl_def = 324,               /* kw_dsl_def  */
  YYSYMBOL_kw_include = 325,               /* kw_include  */
  YYSYMBOL_kw_intop = 326,                 /* kw_intop  */
  YYSYMBOL_kw_map = 327,                   /* kw_map  */
  YYSYMBOL_kw_once = 328,                  /* kw_once  */
  YYSYMBOL_kw_pipe = 329,                  /* kw_pipe  */
  YYSYMBOL_kw_recall = 330,                /* kw_recall  */
  YYSYMBOL_kw_snippet = 331,               /* kw_snippet  */
  YYSYMBOL_kw_strcmp = 332,                /* kw_strcmp  */
  YYSYMBOL_kw_strlen = 333,                /* kw_strlen  */
  YYSYMBOL_kw_strstr = 334,                /* kw_strstr  */
  YYSYMBOL_kw_strsub = 335,                /* kw_strsub  */
  YYSYMBOL_kw_table = 336,                 /* kw_table  */
  YYSYMBOL_kw_table_get = 337,             /* kw_table_get  */
  YYSYMBOL_kw_table_length = 338,          /* kw_table_length  */
  YYSYMBOL_kw_table_size = 339,            /* kw_table_size  */
  YYSYMBOL_kw_table_stack = 340,           /* kw_table_stack  */
  YYSYMBOL_kw_unittest = 341,              /* kw_unittest  */
  YYSYMBOL_kw_arrlen = 342,                /* kw_arrlen  */
  YYSYMBOL_kw_def = 343,                   /* kw_def  */
  YYSYMBOL_kw_enum = 344,                  /* kw_enum  */
  YYSYMBOL_kw_foreach = 345,               /* kw_foreach  */
  YYSYMBOL_kw_free = 346,                  /* kw_free  */
  YYSYMBOL_kw_prefix = 347,                /* kw_prefix  */
  YYSYMBOL_kw_proto = 348,                 /* kw_proto  */
  YYSYMBOL_kw_strin = 349,                 /* kw_strin  */
  YYSYMBOL_kw_switch = 350,                /* kw_switch  */
  YYSYMBOL_kw_typedef = 351,               /* kw_typedef  */
  YYSYMBOL_kw_unused = 352                 /* kw_unused  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  136
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1839

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  164
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  189
/* YYNRULES -- Number of rules.  */
#define YYNRULES  528
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  837

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   391


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       4,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    72,     2,   163,   162,    87,    79,     2,
      92,    93,    85,    83,    96,    84,    68,    86,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    89,    97,
      73,    53,    74,    88,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    94,     2,    95,    80,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    90,    81,    91,    82,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    69,    70,    71,    75,    76,    77,    78,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   855,   855,   859,   863,   864,   868,   869,   872,   876,
     879,   883,   894,   895,   896,   900,   904,   905,   916,   921,
     926,   931,   948,   949,   960,   972,   980,   988,   994,  1004,
    1008,  1011,  1012,  1024,  1025,  1034,  1043,  1051,  1059,  1070,
    1084,  1085,  1086,  1087,  1088,  1089,  1092,  1093,  1110,  1111,
    1120,  1129,  1141,  1142,  1151,  1163,  1164,  1173,  1185,  1186,
    1195,  1204,  1213,  1225,  1226,  1235,  1247,  1248,  1260,  1261,
    1273,  1274,  1286,  1287,  1299,  1300,  1312,  1313,  1330,  1334,
    1337,  1338,  1350,  1351,  1352,  1353,  1354,  1355,  1356,  1357,
    1358,  1359,  1360,  1363,  1364,  1376,  1380,  1395,  1415,  1422,
    1429,  1436,  1448,  1455,  1460,  1466,  1484,  1485,  1486,  1487,
    1488,  1489,  1492,  1493,  1494,  1495,  1496,  1497,  1498,  1499,
    1500,  1501,  1502,  1505,  1512,  1524,  1536,  1548,  1555,  1571,
    1581,  1582,  1585,  1586,  1595,  1609,  1614,  1619,  1624,  1632,
    1636,  1639,  1640,  1651,  1664,  1680,  1690,  1691,  1702,  1707,
    1721,  1735,  1736,  1737,  1738,  1741,  1742,  1746,  1757,  1771,
    1772,  1776,  1779,  1782,  1785,  1800,  1812,  1825,  1834,  1848,
    1849,  1865,  1866,  1875,  1887,  1891,  1894,  1899,  1907,  1908,
    1919,  1970,  1976,  1983,  1992,  2001,  2005,  2012,  2025,  2029,
    2032,  2037,  2045,  2051,  2055,  2061,  2062,  2067,  2077,  2082,
    2092,  2102,  2116,  2127,  2141,  2157,  2162,  2167,  2227,  2232,
    2243,  2254,  2268,  2279,  2293,  2309,  2314,  2319,  2384,  2385,
    2389,  2405,  2409,  2417,  2418,  2422,  2436,  2440,  2444,  2448,
    2452,  2459,  2465,  2466,  2467,  2480,  2486,  2490,  2497,  2506,
    2507,  2511,  2512,  2513,  2514,  2515,  2519,  2520,  2525,  2528,
    2531,  2535,  2539,  2543,  2550,  2551,  2555,  2559,  2565,  2571,
    2575,  2580,  2584,  2588,  2589,  2596,  2600,  2603,  2604,  2608,
    2612,  2618,  2625,  2626,  2630,  2634,  2640,  2648,  2649,  2653,
    2683,  2684,  2688,  2692,  2698,  2703,  2709,  2713,  2719,  2732,
    2733,  2737,  2741,  2744,  2759,  2774,  2789,  2790,  2791,  2795,
    2969,  3036,  3041,  3045,  3046,  3054,  3069,  3074,  3117,  3118,
    3124,  3130,  3134,  3141,  3242,  3371,  3372,  3377,  3431,  3742,
    3746,  3749,  3750,  3757,  3762,  3767,  3773,  3787,  3794,  3800,
    3801,  3806,  3820,  3821,  3827,  3830,  3833,  3836,  3839,  3842,
    3845,  3848,  3862,  3876,  3890,  3904,  3918,  3935,  3939,  3940,
    3944,  3951,  3962,  3970,  3995,  4003,  4013,  4014,  4044,  4087,
    4092,  4093,  4131,  4132,  4133,  4177,  4181,  4184,  4214,  4215,
    4246,  4250,  4257,  4264,  4265,  4275,  4336,  4339,  4342,  4348,
    4352,  4358,  4362,  4364,  4372,  4373,  4377,  4378,  4382,  4394,
    4395,  4396,  4397,  4398,  4399,  4400,  4401,  4402,  4403,  4404,
    4405,  4406,  4407,  4408,  4409,  4410,  4411,  4412,  4413,  4414,
    4415,  4416,  4417,  4418,  4419,  4420,  4421,  4422,  4423,  4424,
    4425,  4426,  4438,  4469,  4506,  4518,  4522,  4543,  4555,  4559,
    4580,  4592,  4596,  4633,  4645,  4649,  4670,  4682,  4686,  4713,
    4725,  4729,  4781,  4818,  4830,  4834,  4855,  4867,  4871,  4892,
    4928,  4940,  4944,  4980,  4992,  4996,  5023,  5044,  5065,  5077,
    5081,  5108,  5120,  5124,  5151,  5163,  5167,  5194,  5206,  5210,
    5237,  5249,  5253,  5280,  5301,  5322,  5334,  5338,  5375,  5387,
    5391,  5432,  5444,  5448,  5479,  5491,  5495,  5526,  5538,  5542,
    5563,  5575,  5579,  5600,  5612,  5616,  5637,  5658,  5679,  5700,
    5712,  5716,  5753,  5765,  5769,  5800,  5812,  5816,  5847,  5859,
    5863,  5884,  5896,  5900,  5921,  5933,  5937,  5974,  5986,  5990,
    6027,  6039,  6043,  6074,  6095,  6107,  6111,  6142,  6154
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  "end of file", "error", "invalid token", "SCANNER_ERROR", "newline",
  "TSV value", "character", "argument modifiers", "const", "restrict",
  "volatile", "_Atomic", "void", "char", "short", "int", "long", "signed",
  "unsigned", "_Bool", "float", "double", "_Complex", "struct", "union",
  "enum", "typedef", "extern", "static", "_Thread_local", "auto",
  "register", "inline", "_Noreturn", "_Alignas", "if", "else", "switch",
  "do", "for", "while", "goto", "continue", "break", "return", "case",
  "default", "_Generic", "_Static_assert", "__func__", "sizeof",
  "_Alignof", "_Imaginary", "'='", "*=", "/=", "%=", "+=", "-=", "<<=",
  ">>=", "&=", "^=", "|=", "<<", ">>", "++", "--", "'.'", "->", "&&", "||",
  "'!'", "'<'", "'>'", "<=", ">=", "==", "!=", "'&'", "'^'", "'|'", "'~'",
  "'+'", "'-'", "'*'", "'/'", "'%'", "'?'", "':'", "'{'", "'}'", "'('",
  "')'", "'['", "']'", "','", "';'", "...", "C identifier",
  "C typedef name", "C enum constant", "C string literal",
  "C integer constant", "C floating-point constant", "% include", "% once",
  "% snippet", "% recall", "% table", "% table-stack", "% map", "% pipe",
  "% unittest", "% dsl-def", "% dsl", "% intop", "% delay", "% defined",
  "% strcmp", "% strstr", "% strlen", "% strsub", "% table-size",
  "% table-length", "% table-get", "% typedef", "% proto", "% def",
  "% unused", "% prefix", "% enum", "% strin", "% foreach", "% switch",
  "% free", "% arrlen", "|%", "<-", ".", "%{", "%}", "%<<", ">>%", "%nul",
  "%NR", "?=", ":=", "*", "%TSV|JSON{", "special argument reference",
  "argument reference", "integer range", "identifier",
  "extended identifier", "special identifier", "file path",
  "signed integer", "unsigned integer", "table-like", "recall-like",
  "snippet-like", "'$'", "'#'", "$accept", "opt_c_identifier", "opt_comma",
  "c_name", "opt_c_name", "c_name_comma_list", "numeric_constant",
  "enumeration_constant", "string", "primary_expression",
  "postfix_expression", "opt_argument_expression_list",
  "argument_expression_list", "unary_expression", "unary_operator",
  "cast_expression", "multiplicative_expression", "additive_expression",
  "shift_expression", "relational_expression", "equality_expression",
  "and_expression", "exclusive_or_expression", "inclusive_or_expression",
  "logical_and_expression", "logical_or_expression",
  "conditional_expression", "opt_assignment_expression",
  "assignment_expression", "assignment_operator", "expression",
  "constant_expression", "constant_expression_flat", "declaration",
  "declaration_specifier", "declaration_specifiers",
  "declaration_specifiers_checked", "storage_class_specifier",
  "simple_type_specifier", "type_specifier", "struct_or_union_specifier",
  "struct_or_union", "struct_declaration_list", "struct_declaration",
  "specifier_qualifier_list", "opt_struct_declarator_list",
  "struct_declarator_list", "struct_declarator", "enum_specifier",
  "enumerator_list", "enumerator", "atomic_type_specifier",
  "type_qualifier", "function_specifier", "alignment_specifier",
  "declarator", "direct_declarator", "array_direct_declarator",
  "function_direct_declarator", "pointer", "pointer_elem",
  "opt_type_qualifier_list", "type_qualifier_list", "parameter_type_list",
  "parameter_type_list_flat", "parameter_list", "parameter_declaration",
  "parameter_declaration_typed", "opt_identifier_list", "identifier_list",
  "type_name", "abstract_declarator", "direct_abstract_declarator",
  "initializer", "initializer_flat", "compound_initializer_list",
  "compound_initializer", "compound_literal", "braced_init_list",
  "initializer_list", "designation", "designator", "paren_c_expression",
  "c_expression_list", "paren_c_expression_list", "opt_pipe", "mod_assign",
  "identifier_ext", "option", "option_comma_list",
  "option_comma_list_nodang", "keyword_options", "opt_keyword_options",
  "opt_identifier", "cmod_identifier", "opt_cmod_identifier",
  "cmod_identifier_comma_list", "cmod_identifier_comma_list_nodang",
  "paren_cmod_identifier_comma_list", "special_id_list",
  "special_id_list_nodang", "paren_special_id_list", "integer",
  "integer_string", "integer_comma_list", "integer_comma_list_nodang",
  "paren_integer_comma_list", "paren_unsigned_int", "opt_UNSIGNED_INT",
  "mod_herestring", "opt_mod_herestring", "opt_herestring", "herestring",
  "table_like", "table", "table_selection", "heretab_verbatim_char_list",
  "heretab_verbatim", "lambda_item", "lambda_item_comma_list",
  "lambda_table_column", "lambda_table_column_list", "lambda_table",
  "recall_like", "recall_end", "snippet_like", "opt_DLLARG", "dllarg",
  "dllarg_list", "opt_dllarg_list", "snippet_body", "opt_snippet_body",
  "cmod_plain_argument", "cmod_named_argument", "cmod_argument_comma_list",
  "cmod_argument_comma_list_nodang", "cmod_arguments",
  "cmod_arguments_unique", "cmod_function_arguments",
  "cmod_formal_arguments", "empty_cmod_arguments",
  "snippet_formal_arguments", "snippet_formal_arguments_inout",
  "opt_table_columns", "table_columns", "snippet_actual_arguments",
  "snippet_actual_arguments_list", "snippet_actual_arguments_inout",
  "snippet_actual_arguments_inout_idcap", "mod_case", "case_list",
  "opt_case_list", "switch_body", "cmod_file", "mod_statement_list",
  "print_mod_statement", "mod_statement", "kw_defined", "kw_delay",
  "kw_dsl", "kw_dsl_def", "kw_include", "kw_intop", "kw_map", "kw_once",
  "kw_pipe", "kw_recall", "kw_snippet", "kw_strcmp", "kw_strlen",
  "kw_strstr", "kw_strsub", "kw_table", "kw_table_get", "kw_table_length",
  "kw_table_size", "kw_table_stack", "kw_unittest", "kw_arrlen", "kw_def",
  "kw_enum", "kw_foreach", "kw_free", "kw_prefix", "kw_proto", "kw_strin",
  "kw_switch", "kw_typedef", "kw_unused", YY_NULLPTR
  };
  return yy_sname[yysymbol];
}
#endif

#define YYPACT_NINF (-709)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-386)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     590,  -709,    36,    58,   109,   139,    35,   143,    73,    99,
      66,    45,    70,   196,    39,   102,   359,   362,   367,   420,
     421,   160,   247,   464,   758,   344,    54,   135,    77,   276,
     263,   424,   427,   440,    50,   622,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
     296,   -91,  -709,   -18,   519,   753,   537,   753,    31,   753,
     540,   753,   570,   753,   630,   459,   658,   -22,   680,    83,
     689,    94,   761,   113,   125,   178,   255,   -22,   257,   408,
     262,   183,   318,   183,   371,   183,   416,   183,   442,   194,
     444,   753,   445,   753,    61,  1268,   104,  1442,    76,   324,
     108,   190,   124,   221,   790,   158,   828,   753,   446,   224,
     452,   254,   138,   254,   454,   283,  -709,  -709,  -709,  -709,
     320,  -709,   292,   300,   388,  -709,  -709,   284,  -709,  -709,
    -709,  -709,   430,  -709,  -709,  -709,   286,  -709,  -709,  -709,
    -709,   183,    47,  -709,  -709,   252,   275,  -709,  -709,   194,
    -709,  -709,    98,   720,   345,  -709,   356,   418,  -709,  -709,
    -709,   250,  -709,  -709,  -709,  -709,   291,  -709,  -709,  -709,
     212,  -709,  -709,  -709,   -22,  -709,  -709,   320,  -709,  -709,
     380,  -709,  -709,   305,  -709,  -709,  -709,   419,  -709,  -709,
    -709,   753,  -709,   183,  -709,  -709,  -709,   487,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,   434,  -709,  -709,   183,  -709,
    -709,  -709,  -709,  -709,   437,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,   494,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,   456,  -709,   754,
    -709,   759,  -709,  -709,  1442,   218,  -709,  -709,  -709,  -709,
     494,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,   -17,   461,   462,  -709,  -709,  -709,   788,  -709,
    -709,   439,  -709,  -709,   525,  -709,  -709,   459,  -709,  -709,
     254,  -709,  -709,  1587,   441,  -709,  -709,   486,  -709,  -709,
    1587,  -709,  -709,  -709,  -709,  -709,  -709,   691,  -709,   -91,
    -709,  -709,  -709,   595,   413,  -709,  -709,   470,   598,  -709,
    -709,  -709,   -32,  -709,  -709,  -709,   345,   606,   613,   624,
    -709,  -709,  -709,   614,  -709,  -709,   -11,  -709,  -709,   577,
     586,  -709,  -709,  -709,   593,  -709,  -709,  -709,   504,  -709,
     558,   567,  -709,   753,   753,   534,  -709,  -709,   544,  -709,
     546,  -709,  -709,   830,   534,   459,  -709,  -709,   746,   751,
     699,  -709,   768,  -709,  -709,   708,  -709,  -709,  1508,  -709,
     777,   786,   787,   494,  -709,  -709,   616,   218,  -709,   783,
     287,   218,  -709,   777,   804,  -709,   820,   833,  -709,   829,
    -709,  -709,   459,    38,   534,  -709,  1626,   837,  1665,  1665,
    -709,  -709,  -709,  -709,  -709,  -709,   787,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,   581,  -709,  1587,  -709,   500,
     809,   844,   582,   835,   851,   852,   856,   870,    -5,  -709,
    -709,  -709,    -7,  1038,  -709,  -709,   853,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,   183,  -709,   534,   183,  -709,   183,
    -709,   940,  -709,  -709,  -709,  -709,  -709,    98,   872,   523,
    -709,   720,  -709,   941,   -93,  -709,  -709,  -709,  -709,  -709,
     947,  -709,   305,   859,  -709,  -709,   860,   858,  -709,  1469,
    -709,   862,   857,   864,   867,  -709,  -709,  -709,  -709,   616,
     868,  -709,  1295,    84,  -709,  -709,   287,  -709,  1508,  -709,
    -709,  -709,   958,  -709,  -709,  -709,  -709,   787,  -709,  1508,
    1587,  -709,  -709,   998,  -709,  -709,    12,   873,  -709,  -709,
     866,   869,  1587,  1587,  -709,  1587,  1587,  1587,  1587,  1587,
    1587,  1587,  1587,  1587,  1587,  1587,  1587,  1587,  1587,  1587,
    1587,  1587,  1587,  1587,  -709,  1587,  1308,   787,   240,  -709,
    -709,  -709,   412,  -709,  -709,  1038,   822,  -709,   965,  -709,
    -709,   183,  -709,  -709,  -709,  -709,   504,  -709,  -709,  -709,
    -709,  -709,  -709,   504,  -709,  -709,  -709,    81,  -709,   -93,
     831,  -709,  -709,  -709,  -709,   815,  1175,   587,  -709,  -709,
      -4,  -709,   311,  -709,  -709,   920,   882,  -709,  -709,  -709,
    -709,  -709,   483,  -709,   886,   884,  -709,  -709,   888,   887,
     616,  1704,   561,   805,  -709,  1487,  -709,   889,   893,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    1587,  -709,  1587,  1587,  -709,  -709,   899,   901,  -709,   823,
    -709,  -709,  -709,   500,   500,   809,   809,   844,   844,   844,
     844,   582,   582,   835,   851,   852,   856,   870,   306,  -709,
     900,   902,  1587,  -709,  -709,  -709,   904,  1498,   932,   905,
     368,  -709,  -709,  -709,  -709,  -709,  -709,  -709,   912,  -709,
    -709,  -709,  -709,   910,   911,   999,   917,  -709,   918,   832,
     311,  1322,   892,  1587,   857,   924,  1135,   966,   483,  -709,
    -709,  1415,  -709,  1587,   933,   937,  -709,  1587,  -709,  -709,
     936,   943,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    1587,  -709,  1587,  -709,  -709,   942,  1308,   944,  -709,  -709,
    1543,  1508,  -709,   -37,  -709,  -709,   949,   999,  -709,  -709,
    1587,  -709,   950,  -709,   953,   999,   952,  -709,   967,   939,
    -709,  -709,  -709,  1498,  -709,  -709,   968,  -709,  -709,   969,
    -709,   218,  -709,  -709,  -709,  -709,  1498,  -709,  -709,   974,
     945,   977,   914,  -709,   975,   978,  -709,  -709,   979,   999,
    -709,  -709,  1587,  -709,   980,  -709,  -709,  -709,  -709,  -709,
    -709,   986,  -709,  -709,   988,   919,  -709,  -709,  -709,   982,
     985,  -709,  -709,   994,  -709,  -709,  -709
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,   421,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   386,   388,   389,   390,
     391,   392,   393,   394,   395,   396,   397,   398,   399,   400,
     401,   402,   403,   404,   405,   406,   407,   408,   409,   410,
     411,   412,   413,   414,   415,   416,   417,   418,   419,   420,
       0,     0,   260,     0,     0,     0,     0,     0,     0,     0,
       0,   265,     0,     0,     0,   365,     0,     0,     0,   261,
       0,     0,     0,     0,     0,     0,     0,   291,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     2,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     1,   387,   437,   436,
     248,   256,     0,   254,     0,   447,   446,     0,   294,   246,
     247,   263,     0,   264,   459,   458,   362,   454,   315,   316,
     453,   372,     0,   476,   475,   266,     0,   488,   487,     0,
     444,   443,   303,     0,   269,   298,   267,   239,   295,   296,
     311,     0,   297,   355,   358,   367,     0,   366,   451,   450,
       0,   491,   490,   262,   291,   434,   433,     0,   431,   430,
       0,   440,   439,     0,   438,   428,   427,     0,   292,   425,
     424,     0,   269,     0,   422,   462,   461,     0,   357,   460,
     356,   468,   467,   466,   465,   464,   463,   471,   470,   469,
     485,   484,   483,   482,   481,     0,   479,   478,     0,   525,
     524,   151,   152,   153,   154,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   130,   131,     8,   106,
     107,   108,   109,   110,   111,   155,   156,     0,     6,   127,
      10,     0,   523,   103,   105,     0,    98,   123,   102,   125,
       8,   126,   124,    99,   100,   101,   515,   514,   127,   513,
     500,   499,     3,     0,     0,   528,   527,   190,     0,   512,
     511,     0,   503,   502,     0,   518,   517,   365,   506,   505,
       0,   521,   520,     0,     0,   509,   508,     0,   494,   493,
       0,   492,   241,   243,   245,   242,   244,     0,   258,   255,
     435,   293,   445,     0,   362,   361,   360,   363,     0,   369,
     368,   370,   373,   375,   314,   452,   365,     0,     0,     0,
     301,   307,   306,   304,   305,   308,     0,   339,   340,   335,
     336,   338,   277,   278,     0,   279,   337,   334,   350,   351,
       0,   348,   300,   268,   240,     0,   312,   313,     0,   289,
       0,   290,   448,     0,     0,   365,   429,   282,     0,   280,
       0,   426,     0,   423,   359,     0,   480,   477,     0,     7,
     145,     0,     0,     0,   522,   104,   174,     0,   161,     0,
     160,     0,   171,   129,     0,   495,     0,     0,   498,     0,
     526,   510,   365,     0,     0,    17,     0,     0,     0,     0,
      45,    40,    44,    42,    43,    41,     0,    18,    14,    16,
      12,    13,    19,    20,    22,    33,    46,     0,    48,    52,
      55,    58,    63,    66,    68,    70,    72,    74,    76,    95,
      96,   236,     0,   381,   519,   507,     0,   250,   251,   252,
     249,   253,   257,   457,   362,   456,     0,     0,   455,     0,
     371,     0,   472,   474,   486,   302,   310,   303,     0,     0,
     354,   349,   270,     0,   329,   442,   299,   288,   449,   489,
       0,   284,   281,     0,   271,   274,     0,   272,   135,   193,
     136,     0,     0,     0,     0,    11,   154,   176,   173,   175,
       0,    97,   188,   174,   163,   164,   159,   172,     0,   497,
     496,   191,     0,   332,   333,   516,   504,     0,    37,     0,
       0,    34,    35,    46,    80,    93,     0,     0,    27,    28,
       0,     0,    29,     0,    36,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   238,     0,     0,     0,     0,   221,
     224,   223,     0,   378,   379,   382,     0,   235,     0,   317,
     364,   374,   473,   309,   335,   336,   347,   346,   342,   343,
     345,   344,   341,   352,   353,   441,   321,   319,   327,   330,
       0,   432,   283,   285,   276,   273,     0,     0,   137,   138,
     195,   192,   196,   150,    15,   148,     4,   146,   158,   157,
     177,   162,   185,   180,     0,   178,   187,   181,     0,   189,
     174,    78,   175,     0,   132,   139,   501,     0,     0,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
       0,    21,     0,     0,    25,    26,     0,    30,    31,     0,
      49,    50,    51,    53,    54,    56,    57,    59,    60,    61,
      62,    64,    65,    67,    69,    71,    73,    75,     0,   237,
       0,     0,     0,   218,   227,   219,     4,     0,     0,     0,
       0,   377,   376,   380,   383,   318,   320,   322,     0,   328,
     331,   275,   206,     0,     0,     0,    41,   198,     0,     0,
     194,     0,     0,     0,     5,     0,     0,   183,   195,   186,
     169,     0,   170,     0,    41,     0,    79,     0,   128,   133,
       0,   140,   141,   143,    38,    39,    81,    94,    47,    24,
       0,    23,     0,   233,   234,     0,     5,     0,   228,   231,
       0,     0,   222,     0,   207,   197,     0,     0,   205,   200,
       0,   199,     0,   216,     0,     0,    41,   208,     0,     0,
     149,   147,   144,     0,   179,   182,     0,   168,   165,     0,
     134,     0,    32,    77,   232,   229,     0,   226,   225,     0,
       0,     0,   286,   202,     0,     0,   201,   217,     0,     0,
     215,   210,     0,   209,     0,   220,   184,   166,   167,   142,
     230,     0,   324,   323,   287,     0,   203,   204,   212,     0,
       0,   211,   325,     0,   213,   214,   326
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -709,  -709,   390,  -227,   810,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -403,  -709,  -432,   363,   365,   198,   372,
     524,   526,   527,   522,   535,  -709,  -306,  -709,  -191,  -709,
    -497,  -391,  -303,   989,   825,  -709,   353,  -709,  -709,  -112,
    -709,  -709,  -709,   451,  -463,  -709,  -709,   317,  -709,  -709,
     385,  -709,  -115,  -709,  -709,  -274,  -393,  -709,  -709,  -481,
    -395,  -485,  -502,  -709,  -493,  -709,  -709,   381,  -709,   574,
    -392,  -437,  -572,  -488,  -709,  -709,  -439,  -709,  -708,  -709,
     355,  -709,  -709,   800,  -103,  -709,   -95,  -160,   785,  -709,
    -709,  -709,  1806,  -709,   -36,  -709,   -12,  -709,   -60,  -709,
    -709,  -709,  -184,   626,  -709,  -709,  -709,  -709,  -709,   916,
    -709,   925,   -75,  -253,   772,  -709,  -709,  -709,   637,  -709,
     946,  -709,  -709,  -170,   963,  -709,  -709,   517,  -709,  -709,
    -117,  -709,  -217,   638,  -709,  -709,  -155,   177,    11,  -148,
    -152,   654,  -302,  -709,   -97,  -315,   653,  -709,  -709,   548,
    -709,  -709,  -709,  -709,  -709,  1099,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,
    -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709,  -709
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,   294,   725,   270,   401,   271,   442,   625,   443,   444,
     445,   666,   667,   446,   447,   448,   449,   450,   451,   452,
     453,   454,   455,   456,   457,   458,   544,   735,   545,   660,
     546,   460,   461,   272,   273,   274,   632,   276,   277,   508,
     279,   280,   643,   644,   509,   740,   741,   742,   281,   626,
     627,   282,   510,   284,   285,   520,   410,   524,   525,   411,
     412,   518,   519,   633,   713,   635,   636,   637,   638,   298,
     511,   714,   622,   694,   816,   578,   695,   580,   581,   696,
     697,   698,   321,   582,   314,   375,   327,   151,   141,   142,
     143,    72,    73,   194,   174,   166,   175,   176,   214,   506,
     507,   396,   365,   366,   388,   389,   204,   391,   825,   381,
     382,   207,   153,   177,   178,   179,   353,   354,   355,   356,
     180,   181,   182,   162,   160,   475,   708,   608,   609,   610,
     495,   535,   368,   369,   370,   371,   183,   184,   219,   185,
     220,   337,   338,   186,   187,   341,   342,   343,   344,   584,
     585,   586,   464,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     283,   409,   283,   278,   336,   278,   339,   459,   335,   340,
     514,   513,   190,   364,   459,   554,   527,   466,   526,   387,
     383,   642,   208,   538,   579,   541,   542,   480,   620,   634,
     317,   400,   476,   543,   157,   158,    80,    70,   641,   152,
      96,   156,   533,   161,   547,   165,    90,   169,   720,   232,
     136,   158,   798,   413,   423,   120,   669,   364,   606,    74,
     217,   334,   140,   212,   239,   645,   572,    88,   347,   607,
     346,    92,   621,   415,    84,   212,   688,   212,   124,   290,
     486,   406,   416,   573,   377,   487,   574,   213,   616,   575,
     617,   307,   241,   242,   243,   516,   459,   352,   367,   235,
      86,   238,   385,    98,   350,   661,   479,   286,   662,   349,
      76,   295,   640,   798,   223,   719,   226,   800,   229,   208,
     147,   801,   148,   670,   671,   672,   802,   299,   201,    71,
      71,  -259,   500,    71,   543,   620,   122,   543,   144,    71,
      78,   315,   367,   470,    82,   647,   579,   648,    71,   543,
     543,   728,    71,  -259,   161,   733,   720,   459,   240,   283,
      71,   110,   278,  -259,    71,  -259,   291,    71,   159,   532,
     543,    71,   588,   543,   543,   212,   515,  -259,   494,  -259,
     645,  -259,   336,  -259,   159,   699,   335,   339,  -259,  -259,
     340,  -259,  -259,    71,  -259,   729,    71,    94,  -259,   392,
    -259,   287,  -259,    71,   493,   296,  -259,   424,  -259,   758,
    -259,  -259,  -259,   767,   543,  -259,   379,  -259,   202,  -259,
     779,   300,  -259,  -259,   393,   527,  -259,  -259,   774,    71,
    -259,   748,   706,    71,  -259,   316,   193,    71,   543,   707,
     147,  -259,   148,  -259,  -259,   728,  -259,   197,   112,   397,
     351,  -259,   471,  -259,    71,  -259,  -259,   543,   205,   543,
     209,   762,  -259,  -259,   128,   215,   200,   499,   795,   459,
     203,   596,   689,   809,   603,   217,   480,   126,   218,   459,
     218,  -259,   218,  -259,   218,  -259,   211,  -259,  -259,   297,
      71,   517,  -259,  -259,   543,   815,  -259,  -259,   474,   138,
     139,   755,  -259,   406,  -259,   322,   534,   536,   820,   323,
     407,   304,   543,  -259,  -259,   324,   543,   408,   612,   543,
     301,   221,   336,   310,   339,   336,   335,   340,   748,   335,
     543,   364,   780,   527,   543,   526,   700,   492,   161,   322,
     172,    71,   173,   323,   173,   118,   313,   543,   206,   324,
     210,   372,   380,   543,   147,   216,   148,    71,   727,   589,
     100,   668,  -259,   102,   543,   149,   150,   543,   104,   799,
      71,   743,   543,   322,   224,   320,   543,   323,   217,   522,
     543,   523,   147,   324,   148,   693,   459,   328,   583,  -259,
     218,  -259,   330,   543,   619,   752,   329,   618,   325,   326,
    -259,  -259,   662,   721,   630,   722,   543,   283,   517,   543,
     278,   222,   352,   367,   602,   218,   367,   459,  -259,   227,
    -259,   106,   108,   292,   293,   130,   718,   331,   132,  -259,
    -259,   348,   325,   326,   332,  -259,   339,   173,    71,   340,
     378,   134,   333,  -259,  -259,   230,   793,   233,   236,   308,
     736,  -259,   373,    71,  -259,   311,    71,   318,   576,  -259,
     761,    71,   362,   363,   225,   114,   325,   326,   275,   746,
     275,   747,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,   374,
     211,   283,   517,   701,   278,   217,   693,   702,   575,   228,
     583,   390,  -259,  -259,    71,    71,  -259,   743,    71,  -259,
     380,    71,   145,   146,   766,   517,   395,   630,   772,   398,
     619,   778,  -259,   618,    71,   231,   421,   234,   237,   309,
     154,   155,   786,   163,   164,   312,   789,   319,   402,   172,
     147,   173,   148,   418,   147,   147,   148,   148,    71,   792,
     417,   149,   150,  -259,  -259,   693,   149,   150,   406,   241,
     242,   243,   516,   167,   168,   726,   804,   617,   422,   805,
     394,   463,   408,   465,   808,   555,   556,   557,   814,   737,
    -384,     1,   693,   268,   399,   241,   242,   243,   516,   473,
     517,   147,   478,   148,   630,   693,   283,   517,   477,   278,
     482,   283,   149,   150,   278,   715,   283,   483,   829,   278,
     485,   830,  -385,     1,   241,   242,   243,   516,   484,   147,
    -246,   148,   357,   170,   171,   358,   425,   426,   427,  -247,
     359,   360,   361,   489,   362,   363,   488,   548,   549,   550,
     551,   490,   630,   428,   429,   562,   563,   564,   565,   430,
     517,   188,   189,   491,   630,   147,   431,   148,   597,   432,
     433,   434,   716,   552,   494,   553,   598,   599,   600,   436,
     362,   363,   717,   191,   192,   496,   437,   497,   438,   439,
     440,   441,   195,   196,   630,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,   116,
     677,   678,   679,   680,   198,   199,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,  -259,
    -259,  -259,  -259,   302,   303,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,   254,   255,
     256,   257,   258,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   305,   306,   147,   498,   148,   425,   426,   427,   501,
     241,   242,   243,   516,   467,   468,   469,   502,   362,   363,
      -7,    -7,    71,   428,   429,   403,   404,   503,  -259,   430,
     770,   504,   147,   505,   148,   357,   431,    -9,   358,   432,
     433,   434,   435,   359,   360,   361,   512,   362,   363,   436,
     521,   425,   426,   427,   419,   420,   437,   288,   438,   439,
     440,   441,   558,   559,   528,   147,   738,   148,   428,   429,
     241,   242,   243,   516,   430,   288,   149,   150,   560,   561,
     529,   431,   566,   567,   432,   433,   434,   435,   751,   662,
     775,   673,   674,   530,   436,   675,   676,   771,   531,   539,
     568,   437,   569,   438,   439,   440,   441,   570,   681,   682,
     571,   425,   426,   427,   592,   605,   587,   241,   242,   243,
     516,   611,   613,   614,   615,   623,   624,   628,   428,   429,
     629,   631,   646,   704,   430,   664,   663,   812,   665,   705,
     711,   431,   710,   723,   432,   433,   434,   776,   724,   730,
     731,   732,   744,   419,   436,   759,   745,   777,   425,   426,
     427,   437,   749,   438,   439,   440,   441,   750,   760,   753,
     756,   754,   763,   764,   765,   428,   429,   241,   242,   243,
     516,   430,   768,   769,   147,   782,   148,   357,   431,   783,
     358,   432,   433,   434,   435,   594,   595,   361,   787,   362,
     363,   436,   788,   790,   813,   797,   822,   794,   437,   791,
     438,   439,   440,   441,   803,   806,   807,   810,   425,   426,
     427,   649,   650,   651,   652,   653,   654,   655,   656,   657,
     658,   659,   811,   817,   818,   428,   429,   821,   823,   833,
     826,   430,   824,   827,   828,   831,   576,   834,   431,   832,
     835,   432,   433,   434,   435,   836,   757,   425,   426,   427,
     414,   436,   683,   686,   739,   684,   639,   685,   437,   405,
     438,   439,   440,   441,   428,   429,   289,   687,   819,   781,
     430,   796,   785,   462,   472,   601,   386,   431,   481,   384,
     432,   433,   434,   435,   593,   345,   709,   376,   576,   604,
     577,   590,   591,   703,   137,     0,     0,   437,     0,   438,
     439,   440,   441,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     147,     0,   148,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     406,     0,     0,     0,     0,     0,     0,   726,   712,   617,
       0,     0,     0,     0,   408,   288,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     406,     0,     0,     0,     0,     0,     0,   616,   712,   617,
       0,     0,     0,     0,     0,   288,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   263,   264,   265,   266,   267,   425,   426,   427,
       0,     0,     0,     0,     0,     0,     0,   268,   269,     0,
       0,     0,     0,     0,   428,   429,   690,   691,     0,     0,
     430,     0,     0,     0,     0,     0,     0,   431,     0,     0,
     432,   433,   434,   435,   297,   288,     0,     0,   576,     0,
     577,     0,   692,     0,     0,     0,     0,   437,     0,   438,
     439,   440,   441,     0,     0,   773,     0,     0,     0,     0,
       0,     0,   288,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   263,   264,   265,   266,   267,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
     254,   255,   256,   257,   258,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,   254,   255,
     256,   257,   258,   784,     0,   288,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,     0,     0,     0,     0,     0,     0,
       0,     0,   288,     0,     0,     0,     0,   425,   426,   427,
       0,     0,     0,     0,   406,     0,     0,     0,     0,     0,
       0,   616,     0,   617,   428,   429,     0,     0,     0,   288,
     430,     0,   406,     0,     0,     0,     0,   431,     0,   407,
     432,   433,   434,   435,     0,     0,   408,   288,   576,     0,
     577,     0,   425,   426,   427,     0,     0,   437,     0,   438,
     439,   440,   441,     0,     0,     0,     0,     0,   288,   428,
     429,     0,     0,     0,     0,   430,     0,     0,     0,     0,
       0,     0,   431,     0,     0,   432,   433,   434,   435,     0,
       0,     0,     0,   576,     0,   436,   425,   426,   427,     0,
       0,     0,   437,     0,   438,   439,   440,   441,     0,     0,
       0,     0,     0,   428,   429,     0,     0,     0,     0,   430,
       0,     0,     0,     0,     0,     0,   431,     0,     0,   432,
     433,   434,   435,     0,     0,   425,   426,   427,     0,   436,
       0,     0,     0,     0,     0,     0,   437,     0,   438,   439,
     440,   441,   428,   429,     0,     0,     0,     0,   430,     0,
       0,     0,     0,     0,     0,   431,     0,     0,   432,   433,
     434,   435,     0,     0,   425,   426,   427,     0,   537,     0,
       0,     0,     0,     0,     0,   437,     0,   438,   439,   440,
     441,   428,   429,     0,     0,     0,     0,   430,     0,     0,
       0,     0,     0,     0,   431,     0,     0,   432,   433,   434,
     435,     0,     0,   425,   426,   427,     0,   540,     0,     0,
       0,     0,     0,     0,   437,     0,   438,   439,   440,   441,
     428,   429,     0,     0,     0,     0,   430,     0,     0,     0,
       0,     0,     0,   431,     0,     0,   432,   433,   434,   734,
       0,     0,     0,     0,     0,     0,   436,     0,     0,     0,
       0,     0,     0,   437,     0,   438,   439,   440,   441,    75,
      77,    79,    81,    83,    85,    87,    89,    91,    93,    95,
      97,    99,   101,   103,   105,   107,   109,   111,   113,   115,
     117,   119,   121,   123,   125,   127,   129,   131,   133,   135
};

static const yytype_int16 yycheck[] =
{
     115,   275,   117,   115,   156,   117,   161,   313,   156,   161,
     402,   402,    87,   173,   320,   447,   411,   320,   411,   203,
     190,   523,    97,   426,   463,   428,   429,   342,   509,   522,
     133,   258,   334,   436,     3,     4,     1,     1,   523,    75,
       1,    77,     4,    79,   436,    81,     1,    83,   620,   109,
       0,     4,   760,   280,   307,     1,   553,   217,   151,     1,
      92,   156,   153,    99,     3,   528,    71,     1,   165,   162,
     165,     1,   509,    90,     1,   111,   573,   113,     1,     3,
      91,    85,    99,    88,   181,    96,    93,    99,    92,    96,
      94,   127,     8,     9,    10,    11,   402,   172,   173,   111,
       1,   113,   197,     1,     6,    93,   138,     3,    96,   169,
       1,     3,    28,   821,   103,   617,   105,   154,   107,   194,
     142,   158,   144,   555,   556,   557,   163,     3,     3,    94,
      94,    92,   385,    94,   537,   616,     1,   540,   156,    94,
       1,     3,   217,   327,     1,   537,   585,   539,    94,   552,
     553,   632,    94,    99,   190,   640,   728,   463,    97,   274,
      94,     1,   274,    90,    94,    92,    90,    94,   137,   422,
     573,    94,   474,   576,   577,   211,   403,   142,   140,   144,
     643,   142,   334,   144,   137,   577,   334,   342,   153,   154,
     342,   156,   156,    94,    92,   632,    94,     1,   153,   211,
     142,    97,   144,    94,   374,    97,   140,   310,   142,   697,
     144,   153,   154,   715,   617,   142,     4,   144,    93,   153,
     722,    97,   149,   153,   213,   620,   153,   154,   721,    94,
     153,   663,   151,    94,    99,    97,   153,    94,   641,   158,
     142,   142,   144,   144,   142,   726,   144,   153,     1,   238,
     152,   142,   327,   144,    94,   153,   154,   660,     3,   662,
       3,   700,   153,   154,     1,     3,   153,   384,   756,   575,
      92,   488,   575,   775,   491,    92,   591,     1,   101,   585,
     103,   142,   105,   144,   107,   142,    92,   144,    92,    99,
      94,   406,   153,   154,   697,   783,   153,   154,   334,     3,
       4,   692,   142,    85,   144,    53,   423,   424,   796,    57,
      92,   153,   715,   153,   154,    63,   719,    99,   502,   722,
      99,     3,   474,    99,   479,   477,   474,   479,   760,   477,
     733,   491,   723,   728,   737,   728,    96,   373,   374,    53,
      90,    94,    92,    57,    92,     1,    92,   750,    93,    63,
      93,   174,   140,   756,   142,    93,   144,    94,   632,   476,
       1,   552,    99,     1,   767,   153,   154,   770,     1,   761,
      94,   645,   775,    53,     3,    92,   779,    57,    92,    92,
     783,    94,   142,    63,   144,   576,   692,    95,   463,   142,
     213,   144,     4,   796,   509,    89,    96,   509,   146,   147,
     153,   154,    96,    92,   519,    94,   809,   522,   523,   812,
     522,    93,   487,   488,   489,   238,   491,   723,   142,     3,
     144,     1,     1,    99,   100,     1,   617,   143,     1,   153,
     154,   156,   146,   147,     4,    91,   591,    92,    94,   591,
     149,     1,   156,    99,   100,     3,   752,     3,     3,     3,
     641,    92,    96,    94,    92,     3,    94,     3,    90,    92,
      92,    94,   157,   158,    93,     1,   146,   147,   115,   660,
     117,   662,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    81,
      92,   616,   617,   578,   616,    92,   697,   582,    96,    93,
     585,    92,    92,    92,    94,    94,    92,   791,    94,    92,
     140,    94,     3,     4,   715,   640,    92,   642,   719,    92,
     645,   722,    92,   645,    94,    93,    97,    93,    93,    93,
       3,     4,   733,     3,     4,    93,   737,    93,    92,    90,
     142,    92,   144,    91,   142,   142,   144,   144,    94,   750,
      99,   153,   154,    99,   100,   756,   153,   154,    85,     8,
       9,    10,    11,     3,     4,    92,   767,    94,    53,   770,
      93,   140,    99,    97,   775,    85,    86,    87,   779,    28,
       0,     1,   783,    99,   100,     8,     9,    10,    11,     4,
     715,   142,     4,   144,   719,   796,   721,   722,   138,   721,
       4,   726,   153,   154,   726,    28,   731,     4,   809,   731,
       6,   812,     0,     1,     8,     9,    10,    11,     4,   142,
      53,   144,   145,     3,     4,   148,    49,    50,    51,    53,
     153,   154,   155,   139,   157,   158,    53,    66,    67,    68,
      69,    93,   767,    66,    67,    73,    74,    75,    76,    72,
     775,     3,     4,    96,   779,   142,    79,   144,   145,    82,
      83,    84,    85,    92,   140,    94,   153,   154,   155,    92,
     157,   158,    95,     3,     4,   141,    99,   141,   101,   102,
     103,   104,     3,     4,   809,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     1,
     562,   563,   564,   565,     3,     4,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,     3,     4,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,     3,     4,   142,     4,   144,    49,    50,    51,    93,
       8,     9,    10,    11,   153,   154,   155,    96,   157,   158,
      96,    97,    94,    66,    67,    96,    97,   158,   100,    72,
      28,    93,   142,   155,   144,   145,    79,    90,   148,    82,
      83,    84,    85,   153,   154,   155,    90,   157,   158,    92,
      97,    49,    50,    51,    96,    97,    99,   100,   101,   102,
     103,   104,    83,    84,    90,   142,    91,   144,    66,    67,
       8,     9,    10,    11,    72,   100,   153,   154,    64,    65,
      90,    79,    77,    78,    82,    83,    84,    85,    95,    96,
      28,   558,   559,    90,    92,   560,   561,    95,    99,    92,
      79,    99,    80,   101,   102,   103,   104,    81,   566,   567,
      70,    49,    50,    51,     4,     4,    93,     8,     9,    10,
      11,     4,    93,    93,    96,    93,    99,    93,    66,    67,
      93,    93,     4,   141,    72,    99,    93,    28,    99,     4,
     155,    79,   141,    53,    82,    83,    84,    85,    96,    93,
      96,    93,    93,    96,    92,    53,    93,    95,    49,    50,
      51,    99,    93,   101,   102,   103,   104,    96,    93,    99,
      96,    99,    90,    93,    93,    66,    67,     8,     9,    10,
      11,    72,    95,    95,   142,    91,   144,   145,    79,    53,
     148,    82,    83,    84,    85,   153,   154,   155,    95,   157,
     158,    92,    95,    97,    95,    91,    91,    95,    99,    96,
     101,   102,   103,   104,    95,    95,    93,    95,    49,    50,
      51,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    95,    95,    95,    66,    67,    93,    91,   150,
      95,    72,   158,    95,    95,    95,    90,    95,    79,    91,
      95,    82,    83,    84,    85,    91,   696,    49,    50,    51,
     280,    92,   568,   571,   643,   569,   522,   570,    99,   274,
     101,   102,   103,   104,    66,    67,   117,   572,   791,   724,
      72,   756,   731,   313,   329,   489,   200,    79,   346,   194,
      82,    83,    84,    85,   487,   162,   609,   181,    90,   491,
      92,   477,   479,   585,    35,    -1,    -1,    99,    -1,   101,
     102,   103,   104,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     142,    -1,   144,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      85,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,    94,
      -1,    -1,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      85,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,    94,
      -1,    -1,    -1,    -1,    -1,   100,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    49,    50,    51,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    99,   100,    -1,
      -1,    -1,    -1,    -1,    66,    67,    68,    69,    -1,    -1,
      72,    -1,    -1,    -1,    -1,    -1,    -1,    79,    -1,    -1,
      82,    83,    84,    85,    99,   100,    -1,    -1,    90,    -1,
      92,    -1,    94,    -1,    -1,    -1,    -1,    99,    -1,   101,
     102,   103,   104,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      -1,    -1,   100,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    98,    -1,   100,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   100,    -1,    -1,    -1,    -1,    49,    50,    51,
      -1,    -1,    -1,    -1,    85,    -1,    -1,    -1,    -1,    -1,
      -1,    92,    -1,    94,    66,    67,    -1,    -1,    -1,   100,
      72,    -1,    85,    -1,    -1,    -1,    -1,    79,    -1,    92,
      82,    83,    84,    85,    -1,    -1,    99,   100,    90,    -1,
      92,    -1,    49,    50,    51,    -1,    -1,    99,    -1,   101,
     102,   103,   104,    -1,    -1,    -1,    -1,    -1,   100,    66,
      67,    -1,    -1,    -1,    -1,    72,    -1,    -1,    -1,    -1,
      -1,    -1,    79,    -1,    -1,    82,    83,    84,    85,    -1,
      -1,    -1,    -1,    90,    -1,    92,    49,    50,    51,    -1,
      -1,    -1,    99,    -1,   101,   102,   103,   104,    -1,    -1,
      -1,    -1,    -1,    66,    67,    -1,    -1,    -1,    -1,    72,
      -1,    -1,    -1,    -1,    -1,    -1,    79,    -1,    -1,    82,
      83,    84,    85,    -1,    -1,    49,    50,    51,    -1,    92,
      -1,    -1,    -1,    -1,    -1,    -1,    99,    -1,   101,   102,
     103,   104,    66,    67,    -1,    -1,    -1,    -1,    72,    -1,
      -1,    -1,    -1,    -1,    -1,    79,    -1,    -1,    82,    83,
      84,    85,    -1,    -1,    49,    50,    51,    -1,    92,    -1,
      -1,    -1,    -1,    -1,    -1,    99,    -1,   101,   102,   103,
     104,    66,    67,    -1,    -1,    -1,    -1,    72,    -1,    -1,
      -1,    -1,    -1,    -1,    79,    -1,    -1,    82,    83,    84,
      85,    -1,    -1,    49,    50,    51,    -1,    92,    -1,    -1,
      -1,    -1,    -1,    -1,    99,    -1,   101,   102,   103,   104,
      66,    67,    -1,    -1,    -1,    -1,    72,    -1,    -1,    -1,
      -1,    -1,    -1,    79,    -1,    -1,    82,    83,    84,    85,
      -1,    -1,    -1,    -1,    -1,    -1,    92,    -1,    -1,    -1,
      -1,    -1,    -1,    99,    -1,   101,   102,   103,   104,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,     1,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   317,   318,   319,   320,   321,   322,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,   338,   339,   340,   341,   342,
     343,   344,   345,   346,   347,   348,   349,   350,   351,   352,
       1,    94,   255,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     1,   256,     1,   256,
       1,   256,     1,   256,     1,   256,     0,   319,     3,     4,
     153,   252,   253,   254,   156,     3,     4,   142,   144,   153,
     154,   251,   258,   276,     3,     4,   258,     3,     4,   137,
     288,   258,   287,     3,     4,   258,   259,     3,     4,   258,
       3,     4,    90,    92,   258,   260,   261,   277,   278,   279,
     284,   285,   286,   300,   301,   303,   307,   308,     3,     4,
     276,     3,     4,   153,   257,     3,     4,   153,     3,     4,
     153,     3,    93,    92,   270,     3,    93,   275,   276,     3,
      93,    92,   258,   260,   262,     3,    93,    92,   301,   302,
     304,     3,    93,   302,     3,    93,   302,     3,    93,   302,
       3,    93,   262,     3,    93,   260,     3,    93,   260,     3,
      97,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    99,   100,
     167,   169,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   212,   215,   216,   217,   218,     3,    97,   100,   197,
       3,    90,    99,   100,   165,     3,    97,    99,   233,     3,
      97,    99,     3,     4,   153,     3,     4,   258,     3,    93,
      99,     3,    93,    92,   248,     3,    97,   248,     3,    93,
      92,   246,    53,    57,    63,   146,   147,   250,    95,    96,
       4,   143,     4,   156,   250,   303,   304,   305,   306,   300,
     304,   309,   310,   311,   312,   288,   250,   308,   156,   262,
       6,   152,   276,   280,   281,   282,   283,   145,   148,   153,
     154,   155,   157,   158,   251,   266,   267,   276,   296,   297,
     298,   299,   301,    96,    81,   249,   284,   308,   149,     4,
     140,   273,   274,   287,   275,   250,   273,   266,   268,   269,
      92,   271,   260,   302,    93,    92,   265,   302,    92,   100,
     167,   168,    92,    96,    97,   198,    85,    92,    99,   219,
     220,   223,   224,   167,   168,    90,    99,    99,    91,    96,
      97,    97,    53,   277,   248,    49,    50,    51,    66,    67,
      72,    79,    82,    83,    84,    85,    92,    99,   101,   102,
     103,   104,   170,   172,   173,   174,   177,   178,   179,   180,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     195,   196,   247,   140,   316,    97,   196,   153,   154,   155,
     266,   276,   252,     4,   258,   289,   306,   138,     4,   138,
     309,   278,     4,     4,     4,     6,    91,    96,    53,   139,
      93,    96,   258,   287,   140,   294,   141,   141,     4,   294,
     277,    93,    96,   158,    93,   155,   263,   264,   203,   208,
     216,   234,    90,   195,   234,   167,    11,   216,   225,   226,
     219,    97,    92,    94,   221,   222,   220,   224,    90,    90,
      90,    99,   277,     4,   294,   295,   294,    92,   177,    92,
      92,   177,   177,   177,   190,   192,   194,   234,    66,    67,
      68,    69,    92,    94,   179,    85,    86,    87,    83,    84,
      64,    65,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    70,    71,    88,    93,    96,    90,    92,   239,   240,
     241,   242,   247,   276,   313,   314,   315,    93,   306,   294,
     305,   310,     4,   282,   153,   154,   296,   145,   153,   154,
     155,   267,   276,   296,   297,     4,   151,   162,   291,   292,
     293,     4,   266,    93,    93,    96,    92,    94,   203,   216,
     223,   235,   236,    93,    99,   171,   213,   214,    93,    93,
     216,    93,   200,   227,   228,   229,   230,   231,   232,   233,
      28,   225,   226,   206,   207,   208,     4,   234,   234,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
     193,    93,    96,    93,    99,    99,   175,   176,   192,   194,
     179,   179,   179,   180,   180,   181,   181,   182,   182,   182,
     182,   183,   183,   184,   185,   186,   187,   188,   194,   196,
      68,    69,    94,   192,   237,   240,   243,   244,   245,   234,
      96,   276,   276,   313,   141,     4,   151,   158,   290,   291,
     141,   155,    93,   228,   235,    28,    85,    95,   192,   226,
     236,    92,    94,    53,    96,   166,    92,   219,   223,   235,
      93,    96,    93,   225,    85,   191,   192,    28,    91,   207,
     209,   210,   211,   219,    93,    93,   192,   192,   179,    93,
      96,    95,    89,    99,    99,   195,    96,   166,   237,    53,
      93,    92,   240,    90,    93,    93,   192,   226,    95,    95,
      28,    95,   192,    93,   228,    28,    85,    95,   192,   226,
     195,   214,    91,    53,    98,   231,   192,    95,    95,   192,
      97,    96,   192,   190,    95,   237,   244,    91,   242,   234,
     154,   158,   163,    95,   192,   192,    95,    93,   192,   226,
      95,    95,    28,    95,   192,   237,   238,    95,    95,   211,
     237,    93,    91,    91,   158,   272,    95,    95,    95,   192,
     192,    95,    91,   150,    95,    95,    91
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   164,   165,   165,   166,   166,   167,   167,   168,   168,
     169,   169,   170,   170,   170,   171,   172,   172,   173,   173,
     173,   173,   174,   174,   174,   174,   174,   174,   174,   175,
     175,   176,   176,   177,   177,   177,   177,   177,   177,   177,
     178,   178,   178,   178,   178,   178,   179,   179,   180,   180,
     180,   180,   181,   181,   181,   182,   182,   182,   183,   183,
     183,   183,   183,   184,   184,   184,   185,   185,   186,   186,
     187,   187,   188,   188,   189,   189,   190,   190,   191,   191,
     192,   192,   193,   193,   193,   193,   193,   193,   193,   193,
     193,   193,   193,   194,   194,   195,   196,   197,   198,   198,
     198,   198,   198,   199,   199,   200,   201,   201,   201,   201,
     201,   201,   202,   202,   202,   202,   202,   202,   202,   202,
     202,   202,   202,   203,   203,   203,   203,   203,   204,   204,
     205,   205,   206,   206,   207,   208,   208,   208,   208,   209,
     209,   210,   210,   211,   212,   212,   213,   213,   214,   214,
     215,   216,   216,   216,   216,   217,   217,   218,   218,   219,
     219,   220,   220,   220,   220,   221,   221,   221,   221,   222,
     222,   223,   223,   224,   225,   225,   226,   226,   227,   227,
     228,   229,   229,   230,   230,   230,   230,   231,   232,   232,
     233,   233,   234,   234,   235,   235,   235,   236,   236,   236,
     236,   236,   236,   236,   236,   236,   236,   236,   236,   236,
     236,   236,   236,   236,   236,   236,   236,   236,   237,   237,
     238,   239,   239,   240,   240,   241,   242,   243,   243,   243,
     243,   244,   245,   245,   245,   246,   247,   247,   248,   249,
     249,   250,   250,   250,   250,   250,   251,   251,   252,   252,
     252,   252,   252,   252,   253,   253,   254,   254,   255,   256,
     256,   257,   257,   258,   258,   259,   259,   260,   260,   261,
     261,   262,   263,   263,   264,   264,   265,   266,   266,   267,
     268,   268,   269,   269,   270,   271,   272,   272,   273,   274,
     274,   275,   275,   276,   276,   277,   277,   277,   277,   278,
     279,   280,   280,   281,   281,   282,   282,   282,   283,   283,
     284,   285,   285,   286,   287,   288,   288,   289,   289,   290,
     290,   291,   291,   291,   291,   291,   291,   292,   292,   293,
     293,   294,   295,   295,   296,   296,   296,   296,   296,   296,
     296,   296,   296,   296,   296,   296,   296,   297,   298,   298,
     299,   299,   299,   299,   300,   301,   302,   302,   303,   304,
     305,   305,   306,   306,   306,   307,   307,   308,   309,   309,
     310,   310,   311,   311,   311,   312,   313,   313,   313,   314,
     314,   315,   315,   316,   317,   317,   318,   318,   319,   320,
     320,   320,   320,   320,   320,   320,   320,   320,   320,   320,
     320,   320,   320,   320,   320,   320,   320,   320,   320,   320,
     320,   320,   320,   320,   320,   320,   320,   320,   320,   320,
     320,   320,   321,   321,   321,   321,   322,   322,   322,   323,
     323,   323,   324,   324,   324,   325,   325,   325,   326,   326,
     326,   327,   327,   327,   327,   328,   328,   328,   329,   329,
     329,   329,   330,   330,   330,   331,   331,   331,   331,   331,
     332,   332,   332,   333,   333,   333,   334,   334,   334,   335,
     335,   335,   336,   336,   336,   336,   336,   337,   337,   337,
     338,   338,   338,   339,   339,   339,   340,   340,   340,   341,
     341,   341,   342,   342,   342,   343,   343,   343,   343,   343,
     343,   344,   344,   344,   345,   345,   345,   346,   346,   346,
     347,   347,   347,   348,   348,   348,   349,   349,   349,   350,
     350,   350,   351,   351,   351,   351,   352,   352,   352
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     1,     0,     1,     1,     1,     0,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     4,     4,     3,     3,     2,     2,     0,
       1,     1,     3,     1,     2,     2,     2,     2,     4,     4,
       1,     1,     1,     1,     1,     1,     1,     4,     1,     3,
       3,     3,     1,     3,     3,     1,     3,     3,     1,     3,
       3,     3,     3,     1,     3,     3,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     5,     0,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     1,     1,     3,     1,     1,
       1,     1,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     5,     2,
       1,     1,     1,     2,     3,     1,     1,     2,     2,     0,
       1,     1,     3,     1,     6,     2,     1,     3,     1,     3,
       4,     1,     1,     1,     1,     1,     1,     4,     4,     2,
       1,     1,     3,     2,     2,     4,     5,     5,     4,     3,
       3,     1,     2,     2,     0,     1,     1,     2,     1,     3,
       1,     1,     3,     2,     4,     1,     2,     1,     0,     1,
       1,     3,     2,     1,     2,     1,     1,     3,     2,     3,
       3,     4,     4,     5,     5,     3,     2,     3,     3,     4,
       4,     5,     5,     6,     6,     4,     3,     4,     1,     1,
       1,     1,     3,     1,     1,     4,     4,     1,     2,     3,
       4,     2,     3,     2,     2,     3,     1,     3,     3,     0,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       3,     3,     3,     3,     1,     2,     1,     3,     3,     0,
       1,     0,     1,     1,     1,     0,     1,     1,     2,     1,
       3,     3,     1,     2,     1,     3,     3,     1,     1,     1,
       1,     2,     1,     3,     3,     3,     0,     1,     2,     1,
       1,     0,     1,     2,     1,     1,     1,     1,     1,     3,
       2,     1,     2,     0,     1,     1,     1,     1,     1,     3,
       3,     1,     2,     2,     2,     1,     1,     2,     3,     0,
       1,     1,     2,     5,     5,     6,     7,     1,     2,     0,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     3,     3,     3,     3,     3,     3,     1,     2,
       1,     1,     3,     3,     3,     1,     1,     1,     1,     2,
       1,     1,     0,     1,     3,     0,     1,     1,     1,     1,
       1,     2,     0,     1,     3,     1,     2,     2,     1,     1,
       2,     0,     1,     3,     0,     1,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     4,     3,     3,     4,     3,     3,     4,
       3,     3,     6,     3,     3,     4,     3,     3,     3,     3,
       3,     6,     5,     3,     3,     4,     3,     3,     4,     5,
       3,     3,     4,     3,     3,     5,     5,     5,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     5,     6,     5,     3,     3,     4,     3,     3,
       4,     3,     3,     3,     3,     3,     5,     3,     3,     5,
       3,     3,     3,     3,     3,     4,     5,     5,     4,     3,
       3,     6,     3,     3,     5,     3,     3,     4,     3,     3,
       4,     3,     3,     3,     3,     3,     5,     3,     3,     4,
       3,     3,     4,     3,     3,     3,     4,     3,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, yyscanner, pp, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, yyscanner, pp); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, void* yyscanner, struct param *pp)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (yyscanner);
  YY_USE (pp);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_SCANNER_ERROR: /* SCANNER_ERROR  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2132 "parser.c"
        break;

    case YYSYMBOL_4_newline_: /* "newline"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2138 "parser.c"
        break;

    case YYSYMBOL_TSV_VALUE: /* "TSV value"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2144 "parser.c"
        break;

    case YYSYMBOL_CHAR: /* "character"  */
#line 292 "parser.y"
         { fprintf(yyo, "<c> " "'%c' (%02x)", ((*yyvaluep).c),(unsigned char)((*yyvaluep).c)); }
#line 2150 "parser.c"
        break;

    case YYSYMBOL_DLLARG_TRANSFORMS: /* "argument modifiers"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2156 "parser.c"
        break;

    case YYSYMBOL_C_CONST: /* "const"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2162 "parser.c"
        break;

    case YYSYMBOL_C_RESTRICT: /* "restrict"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2168 "parser.c"
        break;

    case YYSYMBOL_C_VOLATILE: /* "volatile"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2174 "parser.c"
        break;

    case YYSYMBOL_C_ATOMIC: /* "_Atomic"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2180 "parser.c"
        break;

    case YYSYMBOL_C_VOID: /* "void"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2186 "parser.c"
        break;

    case YYSYMBOL_C_CHAR: /* "char"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2192 "parser.c"
        break;

    case YYSYMBOL_C_SHORT: /* "short"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2198 "parser.c"
        break;

    case YYSYMBOL_C_INT: /* "int"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2204 "parser.c"
        break;

    case YYSYMBOL_C_LONG: /* "long"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2210 "parser.c"
        break;

    case YYSYMBOL_C_SIGNED: /* "signed"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2216 "parser.c"
        break;

    case YYSYMBOL_C_UNSIGNED: /* "unsigned"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2222 "parser.c"
        break;

    case YYSYMBOL_C_BOOL: /* "_Bool"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2228 "parser.c"
        break;

    case YYSYMBOL_C_FLOAT: /* "float"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2234 "parser.c"
        break;

    case YYSYMBOL_C_DOUBLE: /* "double"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2240 "parser.c"
        break;

    case YYSYMBOL_C_COMPLEX: /* "_Complex"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2246 "parser.c"
        break;

    case YYSYMBOL_C_STRUCT: /* "struct"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2252 "parser.c"
        break;

    case YYSYMBOL_C_UNION: /* "union"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2258 "parser.c"
        break;

    case YYSYMBOL_C_ENUM: /* "enum"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2264 "parser.c"
        break;

    case YYSYMBOL_C_TYPEDEF: /* "typedef"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2270 "parser.c"
        break;

    case YYSYMBOL_C_EXTERN: /* "extern"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2276 "parser.c"
        break;

    case YYSYMBOL_C_STATIC: /* "static"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2282 "parser.c"
        break;

    case YYSYMBOL_C_THREAD_LOCAL: /* "_Thread_local"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2288 "parser.c"
        break;

    case YYSYMBOL_C_AUTO: /* "auto"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2294 "parser.c"
        break;

    case YYSYMBOL_C_REGISTER: /* "register"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2300 "parser.c"
        break;

    case YYSYMBOL_C_INLINE: /* "inline"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2306 "parser.c"
        break;

    case YYSYMBOL_C_NORETURN: /* "_Noreturn"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2312 "parser.c"
        break;

    case YYSYMBOL_C_ALIGNAS: /* "_Alignas"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2318 "parser.c"
        break;

    case YYSYMBOL_C_IF: /* "if"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2324 "parser.c"
        break;

    case YYSYMBOL_C_ELSE: /* "else"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2330 "parser.c"
        break;

    case YYSYMBOL_C_SWITCH: /* "switch"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2336 "parser.c"
        break;

    case YYSYMBOL_C_DO: /* "do"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2342 "parser.c"
        break;

    case YYSYMBOL_C_FOR: /* "for"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2348 "parser.c"
        break;

    case YYSYMBOL_C_WHILE: /* "while"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2354 "parser.c"
        break;

    case YYSYMBOL_C_GOTO: /* "goto"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2360 "parser.c"
        break;

    case YYSYMBOL_C_CONTINUE: /* "continue"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2366 "parser.c"
        break;

    case YYSYMBOL_C_BREAK: /* "break"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2372 "parser.c"
        break;

    case YYSYMBOL_C_RETURN: /* "return"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2378 "parser.c"
        break;

    case YYSYMBOL_C_CASE: /* "case"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2384 "parser.c"
        break;

    case YYSYMBOL_C_DEFAULT: /* "default"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2390 "parser.c"
        break;

    case YYSYMBOL_C_GENERIC: /* "_Generic"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2396 "parser.c"
        break;

    case YYSYMBOL_C_STATIC_ASSERT: /* "_Static_assert"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2402 "parser.c"
        break;

    case YYSYMBOL_C_FUNC_NAME: /* "__func__"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2408 "parser.c"
        break;

    case YYSYMBOL_C_SIZEOF: /* "sizeof"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2414 "parser.c"
        break;

    case YYSYMBOL_C_ALIGNOF: /* "_Alignof"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2420 "parser.c"
        break;

    case YYSYMBOL_C_IMAGINARY: /* "_Imaginary"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2426 "parser.c"
        break;

    case YYSYMBOL_53_: /* '='  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2432 "parser.c"
        break;

    case YYSYMBOL_C_MUL_ASSIGN: /* "*="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2438 "parser.c"
        break;

    case YYSYMBOL_C_DIV_ASSIGN: /* "/="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2444 "parser.c"
        break;

    case YYSYMBOL_C_MOD_ASSIGN: /* "%="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2450 "parser.c"
        break;

    case YYSYMBOL_C_ADD_ASSIGN: /* "+="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2456 "parser.c"
        break;

    case YYSYMBOL_C_SUB_ASSIGN: /* "-="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2462 "parser.c"
        break;

    case YYSYMBOL_C_LEFT_ASSIGN: /* "<<="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2468 "parser.c"
        break;

    case YYSYMBOL_C_RIGHT_ASSIGN: /* ">>="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2474 "parser.c"
        break;

    case YYSYMBOL_C_AND_ASSIGN: /* "&="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2480 "parser.c"
        break;

    case YYSYMBOL_C_XOR_ASSIGN: /* "^="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2486 "parser.c"
        break;

    case YYSYMBOL_C_OR_ASSIGN: /* "|="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2492 "parser.c"
        break;

    case YYSYMBOL_C_LEFT_OP: /* "<<"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2498 "parser.c"
        break;

    case YYSYMBOL_C_RIGHT_OP: /* ">>"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2504 "parser.c"
        break;

    case YYSYMBOL_C_INCR_OP: /* "++"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2510 "parser.c"
        break;

    case YYSYMBOL_C_DECR_OP: /* "--"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2516 "parser.c"
        break;

    case YYSYMBOL_68_: /* '.'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2522 "parser.c"
        break;

    case YYSYMBOL_C_PTR_OP: /* "->"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2528 "parser.c"
        break;

    case YYSYMBOL_C_AND_OP: /* "&&"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2534 "parser.c"
        break;

    case YYSYMBOL_C_OR_OP: /* "||"  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2540 "parser.c"
        break;

    case YYSYMBOL_72_: /* '!'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2546 "parser.c"
        break;

    case YYSYMBOL_73_: /* '<'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2552 "parser.c"
        break;

    case YYSYMBOL_74_: /* '>'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2558 "parser.c"
        break;

    case YYSYMBOL_C_LE_OP: /* "<="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2564 "parser.c"
        break;

    case YYSYMBOL_C_GE_OP: /* ">="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2570 "parser.c"
        break;

    case YYSYMBOL_C_EQ_OP: /* "=="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2576 "parser.c"
        break;

    case YYSYMBOL_C_NE_OP: /* "!="  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2582 "parser.c"
        break;

    case YYSYMBOL_79_: /* '&'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2588 "parser.c"
        break;

    case YYSYMBOL_80_: /* '^'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2594 "parser.c"
        break;

    case YYSYMBOL_81_: /* '|'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2600 "parser.c"
        break;

    case YYSYMBOL_82_: /* '~'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2606 "parser.c"
        break;

    case YYSYMBOL_83_: /* '+'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2612 "parser.c"
        break;

    case YYSYMBOL_84_: /* '-'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2618 "parser.c"
        break;

    case YYSYMBOL_85_: /* '*'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2624 "parser.c"
        break;

    case YYSYMBOL_86_: /* '/'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2630 "parser.c"
        break;

    case YYSYMBOL_87_: /* '%'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2636 "parser.c"
        break;

    case YYSYMBOL_88_: /* '?'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2642 "parser.c"
        break;

    case YYSYMBOL_89_: /* ':'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2648 "parser.c"
        break;

    case YYSYMBOL_90_: /* '{'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2654 "parser.c"
        break;

    case YYSYMBOL_91_: /* '}'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2660 "parser.c"
        break;

    case YYSYMBOL_92_: /* '('  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2666 "parser.c"
        break;

    case YYSYMBOL_93_: /* ')'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2672 "parser.c"
        break;

    case YYSYMBOL_94_: /* '['  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2678 "parser.c"
        break;

    case YYSYMBOL_95_: /* ']'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2684 "parser.c"
        break;

    case YYSYMBOL_96_: /* ','  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2690 "parser.c"
        break;

    case YYSYMBOL_97_: /* ';'  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2696 "parser.c"
        break;

    case YYSYMBOL_C_ELLIPSIS: /* "..."  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 2702 "parser.c"
        break;

    case YYSYMBOL_C_IDENTIFIER: /* "C identifier"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2708 "parser.c"
        break;

    case YYSYMBOL_C_TYPEDEF_NAME: /* "C typedef name"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2714 "parser.c"
        break;

    case YYSYMBOL_C_ENUMERATION_CONSTANT: /* "C enum constant"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2720 "parser.c"
        break;

    case YYSYMBOL_C_STRING_LITERAL: /* "C string literal"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2726 "parser.c"
        break;

    case YYSYMBOL_C_INT_CONSTANT: /* "C integer constant"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2732 "parser.c"
        break;

    case YYSYMBOL_C_FP_CONSTANT: /* "C floating-point constant"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 2738 "parser.c"
        break;

    case YYSYMBOL_MOD_INCLUDE: /* "% include"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2744 "parser.c"
        break;

    case YYSYMBOL_MOD_ONCE: /* "% once"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2750 "parser.c"
        break;

    case YYSYMBOL_MOD_SNIPPET: /* "% snippet"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2756 "parser.c"
        break;

    case YYSYMBOL_MOD_RECALL: /* "% recall"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2762 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE: /* "% table"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2768 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_STACK: /* "% table-stack"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2774 "parser.c"
        break;

    case YYSYMBOL_MOD_MAP: /* "% map"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2780 "parser.c"
        break;

    case YYSYMBOL_MOD_PIPE: /* "% pipe"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2786 "parser.c"
        break;

    case YYSYMBOL_MOD_UNITTEST: /* "% unittest"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2792 "parser.c"
        break;

    case YYSYMBOL_MOD_DSL_DEF: /* "% dsl-def"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2798 "parser.c"
        break;

    case YYSYMBOL_MOD_DSL: /* "% dsl"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2804 "parser.c"
        break;

    case YYSYMBOL_MOD_INTOP: /* "% intop"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2810 "parser.c"
        break;

    case YYSYMBOL_MOD_DELAY: /* "% delay"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2816 "parser.c"
        break;

    case YYSYMBOL_MOD_DEFINED: /* "% defined"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2822 "parser.c"
        break;

    case YYSYMBOL_MOD_STRCMP: /* "% strcmp"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2828 "parser.c"
        break;

    case YYSYMBOL_MOD_STRSTR: /* "% strstr"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2834 "parser.c"
        break;

    case YYSYMBOL_MOD_STRLEN: /* "% strlen"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2840 "parser.c"
        break;

    case YYSYMBOL_MOD_STRSUB: /* "% strsub"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2846 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_SIZE: /* "% table-size"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2852 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_LENGTH: /* "% table-length"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2858 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_GET: /* "% table-get"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2864 "parser.c"
        break;

    case YYSYMBOL_MOD_TYPEDEF: /* "% typedef"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2870 "parser.c"
        break;

    case YYSYMBOL_MOD_PROTO: /* "% proto"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2876 "parser.c"
        break;

    case YYSYMBOL_MOD_DEF: /* "% def"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2882 "parser.c"
        break;

    case YYSYMBOL_MOD_UNUSED: /* "% unused"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2888 "parser.c"
        break;

    case YYSYMBOL_MOD_PREFIX: /* "% prefix"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2894 "parser.c"
        break;

    case YYSYMBOL_MOD_ENUM: /* "% enum"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2900 "parser.c"
        break;

    case YYSYMBOL_MOD_STRIN: /* "% strin"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2906 "parser.c"
        break;

    case YYSYMBOL_MOD_FOREACH: /* "% foreach"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2912 "parser.c"
        break;

    case YYSYMBOL_MOD_SWITCH: /* "% switch"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2918 "parser.c"
        break;

    case YYSYMBOL_MOD_FREE: /* "% free"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2924 "parser.c"
        break;

    case YYSYMBOL_MOD_ARRLEN: /* "% arrlen"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2930 "parser.c"
        break;

    case YYSYMBOL_MOD_RECALL_END: /* "|%"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2936 "parser.c"
        break;

    case YYSYMBOL_FROM: /* "<-"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2942 "parser.c"
        break;

    case YYSYMBOL_PERIOD: /* "."  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2948 "parser.c"
        break;

    case YYSYMBOL_MOD_BRACE_OPEN: /* "%{"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2954 "parser.c"
        break;

    case YYSYMBOL_MOD_BRACE_CLOSE: /* "%}"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2960 "parser.c"
        break;

    case YYSYMBOL_MOD_HERESTR_OPEN: /* "%<<"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2966 "parser.c"
        break;

    case YYSYMBOL_MOD_HERESTR_CLOSE: /* ">>%"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2972 "parser.c"
        break;

    case YYSYMBOL_MOD_HERESTR_EMPTY: /* "%nul"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2978 "parser.c"
        break;

    case YYSYMBOL_MOD_ROWNO: /* "%NR"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2984 "parser.c"
        break;

    case YYSYMBOL_OPT_ASSIGN: /* "?="  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2990 "parser.c"
        break;

    case YYSYMBOL_DEF_ASSIGN: /* ":="  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 2996 "parser.c"
        break;

    case YYSYMBOL_ASTERISK: /* "*"  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 3002 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_OPEN: /* "%TSV|JSON{"  */
#line 292 "parser.y"
         { fprintf(yyo, "<c> " "'%c' (%02x)", ((*yyvaluep).c),(unsigned char)((*yyvaluep).c)); }
#line 3008 "parser.c"
        break;

    case YYSYMBOL_DLLARG_SPECIAL: /* "special argument reference"  */
#line 294 "parser.y"
         { fprintf(yyo, "<i> " "%d", ((*yyvaluep).i)); }
#line 3014 "parser.c"
        break;

    case YYSYMBOL_DLLARG: /* "argument reference"  */
#line 310 "parser.y"
         { fprintf(yyo, "<dll> " "\"%s\" : %zu @ %zu {tran=%d fold=%d wrap=%d trim=%d}", ((*yyvaluep).dll).name ? ((*yyvaluep).dll).name : "", ((*yyvaluep).dll).num, ((*yyvaluep).dll).pos, ((*yyvaluep).dll).argtran, ((*yyvaluep).dll).argfold, ((*yyvaluep).dll).argwrap, ((*yyvaluep).dll).argtrim); }
#line 3020 "parser.c"
        break;

    case YYSYMBOL_RANGE: /* "integer range"  */
#line 296 "parser.y"
         { fprintf(yyo, "<range> " "%"PRIiMAX"..%"PRIiMAX"..%"PRIiMAX, ((*yyvaluep).range).lo, ((*yyvaluep).range).hi, ((*yyvaluep).range).inc); }
#line 3026 "parser.c"
        break;

    case YYSYMBOL_IDENTIFIER: /* "identifier"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3032 "parser.c"
        break;

    case YYSYMBOL_IDENTIFIER_EXT: /* "extended identifier"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3038 "parser.c"
        break;

    case YYSYMBOL_SPECIAL_IDENTIFIER: /* "special identifier"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3044 "parser.c"
        break;

    case YYSYMBOL_FILE_PATH: /* "file path"  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3050 "parser.c"
        break;

    case YYSYMBOL_SIGNED_INT: /* "signed integer"  */
#line 361 "parser.y"
         { fprintf(yyo, "<xint> " "(type %d)", ((*yyvaluep).xint).type); }
#line 3056 "parser.c"
        break;

    case YYSYMBOL_UNSIGNED_INT: /* "unsigned integer"  */
#line 361 "parser.y"
         { fprintf(yyo, "<xint> " "(type %d)", ((*yyvaluep).xint).type); }
#line 3062 "parser.c"
        break;

    case YYSYMBOL_159_table_like_: /* "table-like"  */
#line 334 "parser.y"
         { fprintf(yyo, "<tlike> " "%s (%zu columns)", ((*yyvaluep).tlike).tab.name ? ((*yyvaluep).tlike).tab.name : "<none>", sv_len(((*yyvaluep).tlike).sel)); }
#line 3068 "parser.c"
        break;

    case YYSYMBOL_160_recall_like_: /* "recall-like"  */
#line 351 "parser.y"
         { fprintf(yyo, "<rlike> " "%s (%zu arg lists)", ((*yyvaluep).rlike).name ? ((*yyvaluep).rlike).name : "<none>", sv_len(((*yyvaluep).rlike).arg_lists)); }
#line 3074 "parser.c"
        break;

    case YYSYMBOL_161_snippet_like_: /* "snippet-like"  */
#line 316 "parser.y"
         { fprintf(yyo, "<snip> " "%s (%zu in, %zu out)", ((*yyvaluep).snip).name ? ((*yyvaluep).snip).name : "<none>", SNIPPET_NARGIN(&((*yyvaluep).snip)), SNIPPET_NARGOUT(&((*yyvaluep).snip))); }
#line 3080 "parser.c"
        break;

    case YYSYMBOL_162_: /* '$'  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 3086 "parser.c"
        break;

    case YYSYMBOL_163_: /* '#'  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 3092 "parser.c"
        break;

    case YYSYMBOL_opt_c_identifier: /* opt_c_identifier  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3098 "parser.c"
        break;

    case YYSYMBOL_opt_comma: /* opt_comma  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3104 "parser.c"
        break;

    case YYSYMBOL_c_name: /* c_name  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3110 "parser.c"
        break;

    case YYSYMBOL_opt_c_name: /* opt_c_name  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3116 "parser.c"
        break;

    case YYSYMBOL_c_name_comma_list: /* c_name_comma_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3122 "parser.c"
        break;

    case YYSYMBOL_numeric_constant: /* numeric_constant  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3128 "parser.c"
        break;

    case YYSYMBOL_enumeration_constant: /* enumeration_constant  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3134 "parser.c"
        break;

    case YYSYMBOL_string: /* string  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3140 "parser.c"
        break;

    case YYSYMBOL_primary_expression: /* primary_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3146 "parser.c"
        break;

    case YYSYMBOL_postfix_expression: /* postfix_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3152 "parser.c"
        break;

    case YYSYMBOL_opt_argument_expression_list: /* opt_argument_expression_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3158 "parser.c"
        break;

    case YYSYMBOL_argument_expression_list: /* argument_expression_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3164 "parser.c"
        break;

    case YYSYMBOL_unary_expression: /* unary_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3170 "parser.c"
        break;

    case YYSYMBOL_unary_operator: /* unary_operator  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3176 "parser.c"
        break;

    case YYSYMBOL_cast_expression: /* cast_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3182 "parser.c"
        break;

    case YYSYMBOL_multiplicative_expression: /* multiplicative_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3188 "parser.c"
        break;

    case YYSYMBOL_additive_expression: /* additive_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3194 "parser.c"
        break;

    case YYSYMBOL_shift_expression: /* shift_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3200 "parser.c"
        break;

    case YYSYMBOL_relational_expression: /* relational_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3206 "parser.c"
        break;

    case YYSYMBOL_equality_expression: /* equality_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3212 "parser.c"
        break;

    case YYSYMBOL_and_expression: /* and_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3218 "parser.c"
        break;

    case YYSYMBOL_exclusive_or_expression: /* exclusive_or_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3224 "parser.c"
        break;

    case YYSYMBOL_inclusive_or_expression: /* inclusive_or_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3230 "parser.c"
        break;

    case YYSYMBOL_logical_and_expression: /* logical_and_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3236 "parser.c"
        break;

    case YYSYMBOL_logical_or_expression: /* logical_or_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3242 "parser.c"
        break;

    case YYSYMBOL_conditional_expression: /* conditional_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3248 "parser.c"
        break;

    case YYSYMBOL_opt_assignment_expression: /* opt_assignment_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3254 "parser.c"
        break;

    case YYSYMBOL_assignment_expression: /* assignment_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3260 "parser.c"
        break;

    case YYSYMBOL_assignment_operator: /* assignment_operator  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3266 "parser.c"
        break;

    case YYSYMBOL_expression: /* expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3272 "parser.c"
        break;

    case YYSYMBOL_constant_expression: /* constant_expression  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3278 "parser.c"
        break;

    case YYSYMBOL_constant_expression_flat: /* constant_expression_flat  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3284 "parser.c"
        break;

    case YYSYMBOL_declaration: /* declaration  */
#line 304 "parser.y"
         { fprintf(yyo, "<decl> " "%s", ((*yyvaluep).decl).name); }
#line 3290 "parser.c"
        break;

    case YYSYMBOL_declaration_specifier: /* declaration_specifier  */
#line 357 "parser.y"
         { fprintf(yyo, "<cspc> " "(type %d)", ((*yyvaluep).cspc).type); }
#line 3296 "parser.c"
        break;

    case YYSYMBOL_declaration_specifiers: /* declaration_specifiers  */
#line 471 "parser.y"
         { fprintf(yyo, "<vcspc> " "(len %zu)", sv_len(((*yyvaluep).vcspc))); }
#line 3302 "parser.c"
        break;

    case YYSYMBOL_declaration_specifiers_checked: /* declaration_specifiers_checked  */
#line 471 "parser.y"
         { fprintf(yyo, "<vcspc> " "(len %zu)", sv_len(((*yyvaluep).vcspc))); }
#line 3308 "parser.c"
        break;

    case YYSYMBOL_storage_class_specifier: /* storage_class_specifier  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3314 "parser.c"
        break;

    case YYSYMBOL_simple_type_specifier: /* simple_type_specifier  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3320 "parser.c"
        break;

    case YYSYMBOL_type_specifier: /* type_specifier  */
#line 359 "parser.y"
         { fprintf(yyo, "<ctyp> " "(type %d)", ((*yyvaluep).ctyp).type); }
#line 3326 "parser.c"
        break;

    case YYSYMBOL_struct_or_union_specifier: /* struct_or_union_specifier  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3332 "parser.c"
        break;

    case YYSYMBOL_struct_or_union: /* struct_or_union  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3338 "parser.c"
        break;

    case YYSYMBOL_struct_declaration_list: /* struct_declaration_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3344 "parser.c"
        break;

    case YYSYMBOL_struct_declaration: /* struct_declaration  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3350 "parser.c"
        break;

    case YYSYMBOL_specifier_qualifier_list: /* specifier_qualifier_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3356 "parser.c"
        break;

    case YYSYMBOL_opt_struct_declarator_list: /* opt_struct_declarator_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3362 "parser.c"
        break;

    case YYSYMBOL_struct_declarator_list: /* struct_declarator_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3368 "parser.c"
        break;

    case YYSYMBOL_struct_declarator: /* struct_declarator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3374 "parser.c"
        break;

    case YYSYMBOL_enum_specifier: /* enum_specifier  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3380 "parser.c"
        break;

    case YYSYMBOL_enumerator_list: /* enumerator_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3386 "parser.c"
        break;

    case YYSYMBOL_enumerator: /* enumerator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3392 "parser.c"
        break;

    case YYSYMBOL_atomic_type_specifier: /* atomic_type_specifier  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3398 "parser.c"
        break;

    case YYSYMBOL_type_qualifier: /* type_qualifier  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3404 "parser.c"
        break;

    case YYSYMBOL_function_specifier: /* function_specifier  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3410 "parser.c"
        break;

    case YYSYMBOL_alignment_specifier: /* alignment_specifier  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3416 "parser.c"
        break;

    case YYSYMBOL_declarator: /* declarator  */
#line 308 "parser.y"
         { fprintf(yyo, "<acdd> " "(len %zu)", ((*yyvaluep).acdd).len); }
#line 3422 "parser.c"
        break;

    case YYSYMBOL_direct_declarator: /* direct_declarator  */
#line 308 "parser.y"
         { fprintf(yyo, "<acdd> " "(len %zu)", ((*yyvaluep).acdd).len); }
#line 3428 "parser.c"
        break;

    case YYSYMBOL_array_direct_declarator: /* array_direct_declarator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3434 "parser.c"
        break;

    case YYSYMBOL_function_direct_declarator: /* function_direct_declarator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3440 "parser.c"
        break;

    case YYSYMBOL_pointer: /* pointer  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3446 "parser.c"
        break;

    case YYSYMBOL_pointer_elem: /* pointer_elem  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3452 "parser.c"
        break;

    case YYSYMBOL_opt_type_qualifier_list: /* opt_type_qualifier_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3458 "parser.c"
        break;

    case YYSYMBOL_type_qualifier_list: /* type_qualifier_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3464 "parser.c"
        break;

    case YYSYMBOL_parameter_type_list: /* parameter_type_list  */
#line 422 "parser.y"
         { fprintf(yyo, "<vdecl> " "(len %zu)", sv_len(((*yyvaluep).vdecl))); }
#line 3470 "parser.c"
        break;

    case YYSYMBOL_parameter_type_list_flat: /* parameter_type_list_flat  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3476 "parser.c"
        break;

    case YYSYMBOL_parameter_list: /* parameter_list  */
#line 422 "parser.y"
         { fprintf(yyo, "<vdecl> " "(len %zu)", sv_len(((*yyvaluep).vdecl))); }
#line 3482 "parser.c"
        break;

    case YYSYMBOL_parameter_declaration: /* parameter_declaration  */
#line 304 "parser.y"
         { fprintf(yyo, "<decl> " "%s", ((*yyvaluep).decl).name); }
#line 3488 "parser.c"
        break;

    case YYSYMBOL_parameter_declaration_typed: /* parameter_declaration_typed  */
#line 304 "parser.y"
         { fprintf(yyo, "<decl> " "%s", ((*yyvaluep).decl).name); }
#line 3494 "parser.c"
        break;

    case YYSYMBOL_opt_identifier_list: /* opt_identifier_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3500 "parser.c"
        break;

    case YYSYMBOL_identifier_list: /* identifier_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3506 "parser.c"
        break;

    case YYSYMBOL_type_name: /* type_name  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3512 "parser.c"
        break;

    case YYSYMBOL_abstract_declarator: /* abstract_declarator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3518 "parser.c"
        break;

    case YYSYMBOL_direct_abstract_declarator: /* direct_abstract_declarator  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3524 "parser.c"
        break;

    case YYSYMBOL_initializer: /* initializer  */
#line 355 "parser.y"
         { fprintf(yyo, "<cini> " "(type %d)", ((*yyvaluep).cini).type); }
#line 3530 "parser.c"
        break;

    case YYSYMBOL_initializer_flat: /* initializer_flat  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3536 "parser.c"
        break;

    case YYSYMBOL_compound_initializer_list: /* compound_initializer_list  */
#line 414 "parser.y"
         { fprintf(yyo, "<vacdi> " "(len %zu)", sv_len(((*yyvaluep).vacdi))); }
#line 3542 "parser.c"
        break;

    case YYSYMBOL_compound_initializer: /* compound_initializer  */
#line 306 "parser.y"
         { fprintf(yyo, "<acdi> " "(len %zu)", ((*yyvaluep).acdi).len); }
#line 3548 "parser.c"
        break;

    case YYSYMBOL_compound_literal: /* compound_literal  */
#line 306 "parser.y"
         { fprintf(yyo, "<acdi> " "(len %zu)", ((*yyvaluep).acdi).len); }
#line 3554 "parser.c"
        break;

    case YYSYMBOL_braced_init_list: /* braced_init_list  */
#line 306 "parser.y"
         { fprintf(yyo, "<acdi> " "(len %zu)", ((*yyvaluep).acdi).len); }
#line 3560 "parser.c"
        break;

    case YYSYMBOL_initializer_list: /* initializer_list  */
#line 306 "parser.y"
         { fprintf(yyo, "<acdi> " "(len %zu)", ((*yyvaluep).acdi).len); }
#line 3566 "parser.c"
        break;

    case YYSYMBOL_designation: /* designation  */
#line 353 "parser.y"
         { fprintf(yyo, "<cdsg> " "(type %d)", ((*yyvaluep).cdsg).type); }
#line 3572 "parser.c"
        break;

    case YYSYMBOL_designator: /* designator  */
#line 353 "parser.y"
         { fprintf(yyo, "<cdsg> " "(type %d)", ((*yyvaluep).cdsg).type); }
#line 3578 "parser.c"
        break;

    case YYSYMBOL_paren_c_expression: /* paren_c_expression  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3584 "parser.c"
        break;

    case YYSYMBOL_c_expression_list: /* c_expression_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3590 "parser.c"
        break;

    case YYSYMBOL_paren_c_expression_list: /* paren_c_expression_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3596 "parser.c"
        break;

    case YYSYMBOL_opt_pipe: /* opt_pipe  */
#line 298 "parser.y"
         { fprintf(yyo, "<ctxt> " "%c%s%c", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"', ((*yyvaluep).ctxt) ? ((*yyvaluep).ctxt) : "<none>", ((*yyvaluep).ctxt) && ((*yyvaluep).ctxt)[0] && !((*yyvaluep).ctxt)[1] ? '\'' : '"'); }
#line 3602 "parser.c"
        break;

    case YYSYMBOL_mod_assign: /* mod_assign  */
#line 292 "parser.y"
         { fprintf(yyo, "<c> " "'%c' (%02x)", ((*yyvaluep).c),(unsigned char)((*yyvaluep).c)); }
#line 3608 "parser.c"
        break;

    case YYSYMBOL_identifier_ext: /* identifier_ext  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3614 "parser.c"
        break;

    case YYSYMBOL_option: /* option  */
#line 365 "parser.y"
         { fprintf(yyo, "<opt> " "(type %d)", ((*yyvaluep).opt).type); }
#line 3620 "parser.c"
        break;

    case YYSYMBOL_option_comma_list: /* option_comma_list  */
#line 463 "parser.y"
         { fprintf(yyo, "<vopt> " "(len %zu)", sv_len(((*yyvaluep).vopt))); }
#line 3626 "parser.c"
        break;

    case YYSYMBOL_option_comma_list_nodang: /* option_comma_list_nodang  */
#line 463 "parser.y"
         { fprintf(yyo, "<vopt> " "(len %zu)", sv_len(((*yyvaluep).vopt))); }
#line 3632 "parser.c"
        break;

    case YYSYMBOL_keyword_options: /* keyword_options  */
#line 463 "parser.y"
         { fprintf(yyo, "<vopt> " "(len %zu)", sv_len(((*yyvaluep).vopt))); }
#line 3638 "parser.c"
        break;

    case YYSYMBOL_opt_keyword_options: /* opt_keyword_options  */
#line 463 "parser.y"
         { fprintf(yyo, "<vopt> " "(len %zu)", sv_len(((*yyvaluep).vopt))); }
#line 3644 "parser.c"
        break;

    case YYSYMBOL_opt_identifier: /* opt_identifier  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3650 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier: /* cmod_identifier  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3656 "parser.c"
        break;

    case YYSYMBOL_opt_cmod_identifier: /* opt_cmod_identifier  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3662 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier_comma_list: /* cmod_identifier_comma_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3668 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier_comma_list_nodang: /* cmod_identifier_comma_list_nodang  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3674 "parser.c"
        break;

    case YYSYMBOL_paren_cmod_identifier_comma_list: /* paren_cmod_identifier_comma_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3680 "parser.c"
        break;

    case YYSYMBOL_special_id_list: /* special_id_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3686 "parser.c"
        break;

    case YYSYMBOL_special_id_list_nodang: /* special_id_list_nodang  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3692 "parser.c"
        break;

    case YYSYMBOL_paren_special_id_list: /* paren_special_id_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3698 "parser.c"
        break;

    case YYSYMBOL_integer: /* integer  */
#line 361 "parser.y"
         { fprintf(yyo, "<xint> " "(type %d)", ((*yyvaluep).xint).type); }
#line 3704 "parser.c"
        break;

    case YYSYMBOL_integer_string: /* integer_string  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3710 "parser.c"
        break;

    case YYSYMBOL_integer_comma_list: /* integer_comma_list  */
#line 373 "parser.y"
         { fprintf(yyo, "<vxint> " "(len %zu)", sv_len(((*yyvaluep).vxint))); }
#line 3716 "parser.c"
        break;

    case YYSYMBOL_integer_comma_list_nodang: /* integer_comma_list_nodang  */
#line 373 "parser.y"
         { fprintf(yyo, "<vxint> " "(len %zu)", sv_len(((*yyvaluep).vxint))); }
#line 3722 "parser.c"
        break;

    case YYSYMBOL_paren_integer_comma_list: /* paren_integer_comma_list  */
#line 373 "parser.y"
         { fprintf(yyo, "<vxint> " "(len %zu)", sv_len(((*yyvaluep).vxint))); }
#line 3728 "parser.c"
        break;

    case YYSYMBOL_paren_unsigned_int: /* paren_unsigned_int  */
#line 361 "parser.y"
         { fprintf(yyo, "<xint> " "(type %d)", ((*yyvaluep).xint).type); }
#line 3734 "parser.c"
        break;

    case YYSYMBOL_opt_UNSIGNED_INT: /* opt_UNSIGNED_INT  */
#line 361 "parser.y"
         { fprintf(yyo, "<xint> " "(type %d)", ((*yyvaluep).xint).type); }
#line 3740 "parser.c"
        break;

    case YYSYMBOL_mod_herestring: /* mod_herestring  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3746 "parser.c"
        break;

    case YYSYMBOL_opt_mod_herestring: /* opt_mod_herestring  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3752 "parser.c"
        break;

    case YYSYMBOL_opt_herestring: /* opt_herestring  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3758 "parser.c"
        break;

    case YYSYMBOL_herestring: /* herestring  */
#line 300 "parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<none>"); }
#line 3764 "parser.c"
        break;

    case YYSYMBOL_table_like: /* table_like  */
#line 334 "parser.y"
         { fprintf(yyo, "<tlike> " "%s (%zu columns)", ((*yyvaluep).tlike).tab.name ? ((*yyvaluep).tlike).tab.name : "<none>", sv_len(((*yyvaluep).tlike).sel)); }
#line 3770 "parser.c"
        break;

    case YYSYMBOL_table: /* table  */
#line 314 "parser.y"
         { fprintf(yyo, "<tab> " "%s (%zu cols, %zu rows)", ((*yyvaluep).tab).name ? ((*yyvaluep).tab).name : "<none>", sv_len(((*yyvaluep).tab).sv_colnarg), sv_len(((*yyvaluep).tab).svv_rows)); }
#line 3776 "parser.c"
        break;

    case YYSYMBOL_table_selection: /* table_selection  */
#line 334 "parser.y"
         { fprintf(yyo, "<tlike> " "%s (%zu columns)", ((*yyvaluep).tlike).tab.name ? ((*yyvaluep).tlike).tab.name : "<none>", sv_len(((*yyvaluep).tlike).sel)); }
#line 3782 "parser.c"
        break;

    case YYSYMBOL_heretab_verbatim_char_list: /* heretab_verbatim_char_list  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3788 "parser.c"
        break;

    case YYSYMBOL_heretab_verbatim: /* heretab_verbatim  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3794 "parser.c"
        break;

    case YYSYMBOL_lambda_item: /* lambda_item  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3800 "parser.c"
        break;

    case YYSYMBOL_lambda_item_comma_list: /* lambda_item_comma_list  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3806 "parser.c"
        break;

    case YYSYMBOL_lambda_table_column: /* lambda_table_column  */
#line 385 "parser.y"
         { fprintf(yyo, "<vstr> " "(len %zu)", sv_len(((*yyvaluep).vstr))); }
#line 3812 "parser.c"
        break;

    case YYSYMBOL_lambda_table_column_list: /* lambda_table_column_list  */
#line 406 "parser.y"
         { fprintf(yyo, "<vvstr> " "(len %zu)", sv_len(((*yyvaluep).vvstr))); }
#line 3818 "parser.c"
        break;

    case YYSYMBOL_lambda_table: /* lambda_table  */
#line 314 "parser.y"
         { fprintf(yyo, "<tab> " "%s (%zu cols, %zu rows)", ((*yyvaluep).tab).name ? ((*yyvaluep).tab).name : "<none>", sv_len(((*yyvaluep).tab).sv_colnarg), sv_len(((*yyvaluep).tab).svv_rows)); }
#line 3824 "parser.c"
        break;

    case YYSYMBOL_recall_like: /* recall_like  */
#line 351 "parser.y"
         { fprintf(yyo, "<rlike> " "%s (%zu arg lists)", ((*yyvaluep).rlike).name ? ((*yyvaluep).rlike).name : "<none>", sv_len(((*yyvaluep).rlike).arg_lists)); }
#line 3830 "parser.c"
        break;

    case YYSYMBOL_recall_end: /* recall_end  */
#line 292 "parser.y"
         { fprintf(yyo, "<c> " "'%c' (%02x)", ((*yyvaluep).c),(unsigned char)((*yyvaluep).c)); }
#line 3836 "parser.c"
        break;

    case YYSYMBOL_snippet_like: /* snippet_like  */
#line 316 "parser.y"
         { fprintf(yyo, "<snip> " "%s (%zu in, %zu out)", ((*yyvaluep).snip).name ? ((*yyvaluep).snip).name : "<none>", SNIPPET_NARGIN(&((*yyvaluep).snip)), SNIPPET_NARGOUT(&((*yyvaluep).snip))); }
#line 3842 "parser.c"
        break;

    case YYSYMBOL_opt_DLLARG: /* opt_DLLARG  */
#line 310 "parser.y"
         { fprintf(yyo, "<dll> " "\"%s\" : %zu @ %zu {tran=%d fold=%d wrap=%d trim=%d}", ((*yyvaluep).dll).name ? ((*yyvaluep).dll).name : "", ((*yyvaluep).dll).num, ((*yyvaluep).dll).pos, ((*yyvaluep).dll).argtran, ((*yyvaluep).dll).argfold, ((*yyvaluep).dll).argwrap, ((*yyvaluep).dll).argtrim); }
#line 3848 "parser.c"
        break;

    case YYSYMBOL_dllarg: /* dllarg  */
#line 310 "parser.y"
         { fprintf(yyo, "<dll> " "\"%s\" : %zu @ %zu {tran=%d fold=%d wrap=%d trim=%d}", ((*yyvaluep).dll).name ? ((*yyvaluep).dll).name : "", ((*yyvaluep).dll).num, ((*yyvaluep).dll).pos, ((*yyvaluep).dll).argtran, ((*yyvaluep).dll).argfold, ((*yyvaluep).dll).argwrap, ((*yyvaluep).dll).argtrim); }
#line 3854 "parser.c"
        break;

    case YYSYMBOL_dllarg_list: /* dllarg_list  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 3860 "parser.c"
        break;

    case YYSYMBOL_opt_dllarg_list: /* opt_dllarg_list  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 3866 "parser.c"
        break;

    case YYSYMBOL_snippet_body: /* snippet_body  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3872 "parser.c"
        break;

    case YYSYMBOL_opt_snippet_body: /* opt_snippet_body  */
#line 302 "parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 3878 "parser.c"
        break;

    case YYSYMBOL_cmod_plain_argument: /* cmod_plain_argument  */
#line 312 "parser.y"
         { fprintf(yyo, "<narg> " "%s = %s (%d)", ((*yyvaluep).narg).name ? ((*yyvaluep).narg).name : "<none>", ((*yyvaluep).narg).value.Text ? ((*yyvaluep).narg).value.Text : "<none>", ((*yyvaluep).narg).type); }
#line 3884 "parser.c"
        break;

    case YYSYMBOL_cmod_named_argument: /* cmod_named_argument  */
#line 312 "parser.y"
         { fprintf(yyo, "<narg> " "%s = %s (%d)", ((*yyvaluep).narg).name ? ((*yyvaluep).narg).name : "<none>", ((*yyvaluep).narg).value.Text ? ((*yyvaluep).narg).value.Text : "<none>", ((*yyvaluep).narg).type); }
#line 3890 "parser.c"
        break;

    case YYSYMBOL_cmod_argument_comma_list: /* cmod_argument_comma_list  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3896 "parser.c"
        break;

    case YYSYMBOL_cmod_argument_comma_list_nodang: /* cmod_argument_comma_list_nodang  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3902 "parser.c"
        break;

    case YYSYMBOL_cmod_arguments: /* cmod_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3908 "parser.c"
        break;

    case YYSYMBOL_cmod_arguments_unique: /* cmod_arguments_unique  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3914 "parser.c"
        break;

    case YYSYMBOL_cmod_function_arguments: /* cmod_function_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3920 "parser.c"
        break;

    case YYSYMBOL_cmod_formal_arguments: /* cmod_formal_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3926 "parser.c"
        break;

    case YYSYMBOL_empty_cmod_arguments: /* empty_cmod_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3932 "parser.c"
        break;

    case YYSYMBOL_snippet_formal_arguments: /* snippet_formal_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3938 "parser.c"
        break;

    case YYSYMBOL_snippet_formal_arguments_inout: /* snippet_formal_arguments_inout  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3944 "parser.c"
        break;

    case YYSYMBOL_opt_table_columns: /* opt_table_columns  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3950 "parser.c"
        break;

    case YYSYMBOL_table_columns: /* table_columns  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3956 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments: /* snippet_actual_arguments  */
#line 438 "parser.y"
         { fprintf(yyo, "<vnarg> " "(len %zu)", sv_len(((*yyvaluep).vnarg))); }
#line 3962 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_list: /* snippet_actual_arguments_list  */
#line 455 "parser.y"
         { fprintf(yyo, "<vvnarg> " "(len %zu)", sv_len(((*yyvaluep).vvnarg))); }
#line 3968 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_inout: /* snippet_actual_arguments_inout  */
#line 455 "parser.y"
         { fprintf(yyo, "<vvnarg> " "(len %zu)", sv_len(((*yyvaluep).vvnarg))); }
#line 3974 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_inout_idcap: /* snippet_actual_arguments_inout_idcap  */
#line 455 "parser.y"
         { fprintf(yyo, "<vvnarg> " "(len %zu)", sv_len(((*yyvaluep).vvnarg))); }
#line 3980 "parser.c"
        break;

    case YYSYMBOL_mod_case: /* mod_case  */
#line 363 "parser.y"
         { fprintf(yyo, "<mcase> " "(type %d)", ((*yyvaluep).mcase).type); }
#line 3986 "parser.c"
        break;

    case YYSYMBOL_case_list: /* case_list  */
#line 430 "parser.y"
         { fprintf(yyo, "<vmcase> " "(len %zu)", sv_len(((*yyvaluep).vmcase))); }
#line 3992 "parser.c"
        break;

    case YYSYMBOL_opt_case_list: /* opt_case_list  */
#line 430 "parser.y"
         { fprintf(yyo, "<vmcase> " "(len %zu)", sv_len(((*yyvaluep).vmcase))); }
#line 3998 "parser.c"
        break;

    case YYSYMBOL_switch_body: /* switch_body  */
#line 430 "parser.y"
         { fprintf(yyo, "<vmcase> " "(len %zu)", sv_len(((*yyvaluep).vmcase))); }
#line 4004 "parser.c"
        break;

    case YYSYMBOL_cmod_file: /* cmod_file  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4010 "parser.c"
        break;

    case YYSYMBOL_mod_statement_list: /* mod_statement_list  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4016 "parser.c"
        break;

    case YYSYMBOL_print_mod_statement: /* print_mod_statement  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4022 "parser.c"
        break;

    case YYSYMBOL_mod_statement: /* mod_statement  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4028 "parser.c"
        break;

    case YYSYMBOL_kw_defined: /* kw_defined  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4034 "parser.c"
        break;

    case YYSYMBOL_kw_delay: /* kw_delay  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4040 "parser.c"
        break;

    case YYSYMBOL_kw_dsl: /* kw_dsl  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4046 "parser.c"
        break;

    case YYSYMBOL_kw_dsl_def: /* kw_dsl_def  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4052 "parser.c"
        break;

    case YYSYMBOL_kw_include: /* kw_include  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4058 "parser.c"
        break;

    case YYSYMBOL_kw_intop: /* kw_intop  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4064 "parser.c"
        break;

    case YYSYMBOL_kw_map: /* kw_map  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4070 "parser.c"
        break;

    case YYSYMBOL_kw_once: /* kw_once  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4076 "parser.c"
        break;

    case YYSYMBOL_kw_pipe: /* kw_pipe  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4082 "parser.c"
        break;

    case YYSYMBOL_kw_recall: /* kw_recall  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4088 "parser.c"
        break;

    case YYSYMBOL_kw_snippet: /* kw_snippet  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4094 "parser.c"
        break;

    case YYSYMBOL_kw_strcmp: /* kw_strcmp  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4100 "parser.c"
        break;

    case YYSYMBOL_kw_strlen: /* kw_strlen  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4106 "parser.c"
        break;

    case YYSYMBOL_kw_strstr: /* kw_strstr  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4112 "parser.c"
        break;

    case YYSYMBOL_kw_strsub: /* kw_strsub  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4118 "parser.c"
        break;

    case YYSYMBOL_kw_table: /* kw_table  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4124 "parser.c"
        break;

    case YYSYMBOL_kw_table_get: /* kw_table_get  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4130 "parser.c"
        break;

    case YYSYMBOL_kw_table_length: /* kw_table_length  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4136 "parser.c"
        break;

    case YYSYMBOL_kw_table_size: /* kw_table_size  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4142 "parser.c"
        break;

    case YYSYMBOL_kw_table_stack: /* kw_table_stack  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4148 "parser.c"
        break;

    case YYSYMBOL_kw_unittest: /* kw_unittest  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4154 "parser.c"
        break;

    case YYSYMBOL_kw_arrlen: /* kw_arrlen  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4160 "parser.c"
        break;

    case YYSYMBOL_kw_def: /* kw_def  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4166 "parser.c"
        break;

    case YYSYMBOL_kw_enum: /* kw_enum  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4172 "parser.c"
        break;

    case YYSYMBOL_kw_foreach: /* kw_foreach  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4178 "parser.c"
        break;

    case YYSYMBOL_kw_free: /* kw_free  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4184 "parser.c"
        break;

    case YYSYMBOL_kw_prefix: /* kw_prefix  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4190 "parser.c"
        break;

    case YYSYMBOL_kw_proto: /* kw_proto  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4196 "parser.c"
        break;

    case YYSYMBOL_kw_strin: /* kw_strin  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4202 "parser.c"
        break;

    case YYSYMBOL_kw_switch: /* kw_switch  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4208 "parser.c"
        break;

    case YYSYMBOL_kw_typedef: /* kw_typedef  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4214 "parser.c"
        break;

    case YYSYMBOL_kw_unused: /* kw_unused  */
#line 472 "parser.y"
         { fprintf(yyo, "<>"); }
#line 4220 "parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, void* yyscanner, struct param *pp)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, yyscanner, pp);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, void* yyscanner, struct param *pp)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), yyscanner, pp);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, yyscanner, pp); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yysymbol_kind_t yytoken;
  YYLTYPE *yylloc;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  int yyn = yypact[+*yyctx->yyssp];
  if (!yypact_value_is_default (yyn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;
      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYSYMBOL_YYerror
            && !yytable_value_is_error (yytable[yyx + yyn]))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = YY_CAST (yysymbol_kind_t, yyx);
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




/* The kind of the lookahead of this context.  */
static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx)
{
  return yyctx->yytoken;
}

/* The location of the lookahead of this context.  */
static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx)
{
  return yyctx->yylloc;
}

/* User defined function to report a syntax error.  */
static int
yyreport_syntax_error (const yypcontext_t *yyctx, void* yyscanner, struct param *pp);

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, void* yyscanner, struct param *pp)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (yyscanner);
  YY_USE (pp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_CHAR: /* "character"  */
#line 291 "parser.y"
            {  ((*yyvaluep).c) = '\0'; }
#line 4427 "parser.c"
        break;

    case YYSYMBOL_DLLARG_TRANSFORMS: /* "argument modifiers"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 4433 "parser.c"
        break;

    case YYSYMBOL_C_CONST: /* "const"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4439 "parser.c"
        break;

    case YYSYMBOL_C_RESTRICT: /* "restrict"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4445 "parser.c"
        break;

    case YYSYMBOL_C_VOLATILE: /* "volatile"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4451 "parser.c"
        break;

    case YYSYMBOL_C_ATOMIC: /* "_Atomic"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4457 "parser.c"
        break;

    case YYSYMBOL_C_VOID: /* "void"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4463 "parser.c"
        break;

    case YYSYMBOL_C_CHAR: /* "char"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4469 "parser.c"
        break;

    case YYSYMBOL_C_SHORT: /* "short"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4475 "parser.c"
        break;

    case YYSYMBOL_C_INT: /* "int"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4481 "parser.c"
        break;

    case YYSYMBOL_C_LONG: /* "long"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4487 "parser.c"
        break;

    case YYSYMBOL_C_SIGNED: /* "signed"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4493 "parser.c"
        break;

    case YYSYMBOL_C_UNSIGNED: /* "unsigned"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4499 "parser.c"
        break;

    case YYSYMBOL_C_BOOL: /* "_Bool"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4505 "parser.c"
        break;

    case YYSYMBOL_C_FLOAT: /* "float"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4511 "parser.c"
        break;

    case YYSYMBOL_C_DOUBLE: /* "double"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4517 "parser.c"
        break;

    case YYSYMBOL_C_COMPLEX: /* "_Complex"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4523 "parser.c"
        break;

    case YYSYMBOL_C_STRUCT: /* "struct"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4529 "parser.c"
        break;

    case YYSYMBOL_C_UNION: /* "union"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4535 "parser.c"
        break;

    case YYSYMBOL_C_ENUM: /* "enum"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4541 "parser.c"
        break;

    case YYSYMBOL_C_TYPEDEF: /* "typedef"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4547 "parser.c"
        break;

    case YYSYMBOL_C_EXTERN: /* "extern"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4553 "parser.c"
        break;

    case YYSYMBOL_C_STATIC: /* "static"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4559 "parser.c"
        break;

    case YYSYMBOL_C_THREAD_LOCAL: /* "_Thread_local"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4565 "parser.c"
        break;

    case YYSYMBOL_C_AUTO: /* "auto"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4571 "parser.c"
        break;

    case YYSYMBOL_C_REGISTER: /* "register"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4577 "parser.c"
        break;

    case YYSYMBOL_C_INLINE: /* "inline"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4583 "parser.c"
        break;

    case YYSYMBOL_C_NORETURN: /* "_Noreturn"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4589 "parser.c"
        break;

    case YYSYMBOL_C_ALIGNAS: /* "_Alignas"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4595 "parser.c"
        break;

    case YYSYMBOL_C_IF: /* "if"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4601 "parser.c"
        break;

    case YYSYMBOL_C_ELSE: /* "else"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4607 "parser.c"
        break;

    case YYSYMBOL_C_SWITCH: /* "switch"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4613 "parser.c"
        break;

    case YYSYMBOL_C_DO: /* "do"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4619 "parser.c"
        break;

    case YYSYMBOL_C_FOR: /* "for"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4625 "parser.c"
        break;

    case YYSYMBOL_C_WHILE: /* "while"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4631 "parser.c"
        break;

    case YYSYMBOL_C_GOTO: /* "goto"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4637 "parser.c"
        break;

    case YYSYMBOL_C_CONTINUE: /* "continue"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4643 "parser.c"
        break;

    case YYSYMBOL_C_BREAK: /* "break"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4649 "parser.c"
        break;

    case YYSYMBOL_C_RETURN: /* "return"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4655 "parser.c"
        break;

    case YYSYMBOL_C_CASE: /* "case"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4661 "parser.c"
        break;

    case YYSYMBOL_C_DEFAULT: /* "default"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4667 "parser.c"
        break;

    case YYSYMBOL_C_GENERIC: /* "_Generic"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4673 "parser.c"
        break;

    case YYSYMBOL_C_STATIC_ASSERT: /* "_Static_assert"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4679 "parser.c"
        break;

    case YYSYMBOL_C_FUNC_NAME: /* "__func__"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4685 "parser.c"
        break;

    case YYSYMBOL_C_SIZEOF: /* "sizeof"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4691 "parser.c"
        break;

    case YYSYMBOL_C_ALIGNOF: /* "_Alignof"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4697 "parser.c"
        break;

    case YYSYMBOL_C_IMAGINARY: /* "_Imaginary"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4703 "parser.c"
        break;

    case YYSYMBOL_53_: /* '='  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4709 "parser.c"
        break;

    case YYSYMBOL_C_MUL_ASSIGN: /* "*="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4715 "parser.c"
        break;

    case YYSYMBOL_C_DIV_ASSIGN: /* "/="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4721 "parser.c"
        break;

    case YYSYMBOL_C_MOD_ASSIGN: /* "%="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4727 "parser.c"
        break;

    case YYSYMBOL_C_ADD_ASSIGN: /* "+="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4733 "parser.c"
        break;

    case YYSYMBOL_C_SUB_ASSIGN: /* "-="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4739 "parser.c"
        break;

    case YYSYMBOL_C_LEFT_ASSIGN: /* "<<="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4745 "parser.c"
        break;

    case YYSYMBOL_C_RIGHT_ASSIGN: /* ">>="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4751 "parser.c"
        break;

    case YYSYMBOL_C_AND_ASSIGN: /* "&="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4757 "parser.c"
        break;

    case YYSYMBOL_C_XOR_ASSIGN: /* "^="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4763 "parser.c"
        break;

    case YYSYMBOL_C_OR_ASSIGN: /* "|="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4769 "parser.c"
        break;

    case YYSYMBOL_C_LEFT_OP: /* "<<"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4775 "parser.c"
        break;

    case YYSYMBOL_C_RIGHT_OP: /* ">>"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4781 "parser.c"
        break;

    case YYSYMBOL_C_INCR_OP: /* "++"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4787 "parser.c"
        break;

    case YYSYMBOL_C_DECR_OP: /* "--"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4793 "parser.c"
        break;

    case YYSYMBOL_68_: /* '.'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4799 "parser.c"
        break;

    case YYSYMBOL_C_PTR_OP: /* "->"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4805 "parser.c"
        break;

    case YYSYMBOL_C_AND_OP: /* "&&"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4811 "parser.c"
        break;

    case YYSYMBOL_C_OR_OP: /* "||"  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4817 "parser.c"
        break;

    case YYSYMBOL_72_: /* '!'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4823 "parser.c"
        break;

    case YYSYMBOL_73_: /* '<'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4829 "parser.c"
        break;

    case YYSYMBOL_74_: /* '>'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4835 "parser.c"
        break;

    case YYSYMBOL_C_LE_OP: /* "<="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4841 "parser.c"
        break;

    case YYSYMBOL_C_GE_OP: /* ">="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4847 "parser.c"
        break;

    case YYSYMBOL_C_EQ_OP: /* "=="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4853 "parser.c"
        break;

    case YYSYMBOL_C_NE_OP: /* "!="  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4859 "parser.c"
        break;

    case YYSYMBOL_79_: /* '&'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4865 "parser.c"
        break;

    case YYSYMBOL_80_: /* '^'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4871 "parser.c"
        break;

    case YYSYMBOL_81_: /* '|'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4877 "parser.c"
        break;

    case YYSYMBOL_82_: /* '~'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4883 "parser.c"
        break;

    case YYSYMBOL_83_: /* '+'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4889 "parser.c"
        break;

    case YYSYMBOL_84_: /* '-'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4895 "parser.c"
        break;

    case YYSYMBOL_85_: /* '*'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4901 "parser.c"
        break;

    case YYSYMBOL_86_: /* '/'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4907 "parser.c"
        break;

    case YYSYMBOL_87_: /* '%'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4913 "parser.c"
        break;

    case YYSYMBOL_88_: /* '?'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4919 "parser.c"
        break;

    case YYSYMBOL_89_: /* ':'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4925 "parser.c"
        break;

    case YYSYMBOL_90_: /* '{'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4931 "parser.c"
        break;

    case YYSYMBOL_91_: /* '}'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4937 "parser.c"
        break;

    case YYSYMBOL_92_: /* '('  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4943 "parser.c"
        break;

    case YYSYMBOL_93_: /* ')'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4949 "parser.c"
        break;

    case YYSYMBOL_94_: /* '['  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4955 "parser.c"
        break;

    case YYSYMBOL_95_: /* ']'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4961 "parser.c"
        break;

    case YYSYMBOL_96_: /* ','  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4967 "parser.c"
        break;

    case YYSYMBOL_97_: /* ';'  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4973 "parser.c"
        break;

    case YYSYMBOL_C_ELLIPSIS: /* "..."  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 4979 "parser.c"
        break;

    case YYSYMBOL_C_IDENTIFIER: /* "C identifier"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 4985 "parser.c"
        break;

    case YYSYMBOL_C_TYPEDEF_NAME: /* "C typedef name"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 4991 "parser.c"
        break;

    case YYSYMBOL_C_ENUMERATION_CONSTANT: /* "C enum constant"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 4997 "parser.c"
        break;

    case YYSYMBOL_C_STRING_LITERAL: /* "C string literal"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5003 "parser.c"
        break;

    case YYSYMBOL_C_INT_CONSTANT: /* "C integer constant"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5009 "parser.c"
        break;

    case YYSYMBOL_C_FP_CONSTANT: /* "C floating-point constant"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5015 "parser.c"
        break;

    case YYSYMBOL_MOD_TABLE_OPEN: /* "%TSV|JSON{"  */
#line 291 "parser.y"
            {  ((*yyvaluep).c) = '\0'; }
#line 5021 "parser.c"
        break;

    case YYSYMBOL_DLLARG_SPECIAL: /* "special argument reference"  */
#line 293 "parser.y"
            {  ((*yyvaluep).i) = 0; }
#line 5027 "parser.c"
        break;

    case YYSYMBOL_DLLARG: /* "argument reference"  */
#line 309 "parser.y"
            { dllarg_clear(&((*yyvaluep).dll)); ((*yyvaluep).dll) = (struct dllarg){ 0 }; }
#line 5033 "parser.c"
        break;

    case YYSYMBOL_RANGE: /* "integer range"  */
#line 295 "parser.y"
            {  ((*yyvaluep).range) = (struct range){ 0 }; }
#line 5039 "parser.c"
        break;

    case YYSYMBOL_IDENTIFIER: /* "identifier"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5045 "parser.c"
        break;

    case YYSYMBOL_IDENTIFIER_EXT: /* "extended identifier"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5051 "parser.c"
        break;

    case YYSYMBOL_SPECIAL_IDENTIFIER: /* "special identifier"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5057 "parser.c"
        break;

    case YYSYMBOL_FILE_PATH: /* "file path"  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5063 "parser.c"
        break;

    case YYSYMBOL_SIGNED_INT: /* "signed integer"  */
#line 360 "parser.y"
            { xint_free(&((*yyvaluep).xint)); ((*yyvaluep).xint) = (struct xint){ 0 }; }
#line 5069 "parser.c"
        break;

    case YYSYMBOL_UNSIGNED_INT: /* "unsigned integer"  */
#line 360 "parser.y"
            { xint_free(&((*yyvaluep).xint)); ((*yyvaluep).xint) = (struct xint){ 0 }; }
#line 5075 "parser.c"
        break;

    case YYSYMBOL_159_table_like_: /* "table-like"  */
#line 317 "parser.y"
            { ((*yyvaluep).tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).sel); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).lst); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).tlike).lst));
}
 ((*yyvaluep).tlike) = (struct table_like){ 0 }; }
#line 5097 "parser.c"
        break;

    case YYSYMBOL_160_recall_like_: /* "recall-like"  */
#line 335 "parser.y"
            { ((*yyvaluep).rlike).snip = NULL; free(((*yyvaluep).rlike).name); ((*yyvaluep).rlike).name = NULL; {
    for(size_t i = 0; i < sv_len(((*yyvaluep).rlike).arg_lists); ++i) {
        const void *_item = sv_at(((*yyvaluep).rlike).arg_lists,i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).rlike).arg_lists));
}
 ((*yyvaluep).rlike) = (struct recall_like){ 0 }; }
#line 5118 "parser.c"
        break;

    case YYSYMBOL_161_snippet_like_: /* "snippet-like"  */
#line 315 "parser.y"
            { snippet_clear(&((*yyvaluep).snip)); ((*yyvaluep).snip) = (struct snippet){ 0 }; }
#line 5124 "parser.c"
        break;

    case YYSYMBOL_opt_c_identifier: /* opt_c_identifier  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5130 "parser.c"
        break;

    case YYSYMBOL_opt_comma: /* opt_comma  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5136 "parser.c"
        break;

    case YYSYMBOL_c_name: /* c_name  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5142 "parser.c"
        break;

    case YYSYMBOL_opt_c_name: /* opt_c_name  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5148 "parser.c"
        break;

    case YYSYMBOL_c_name_comma_list: /* c_name_comma_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5164 "parser.c"
        break;

    case YYSYMBOL_numeric_constant: /* numeric_constant  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5170 "parser.c"
        break;

    case YYSYMBOL_enumeration_constant: /* enumeration_constant  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5176 "parser.c"
        break;

    case YYSYMBOL_string: /* string  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5182 "parser.c"
        break;

    case YYSYMBOL_primary_expression: /* primary_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5198 "parser.c"
        break;

    case YYSYMBOL_postfix_expression: /* postfix_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5214 "parser.c"
        break;

    case YYSYMBOL_opt_argument_expression_list: /* opt_argument_expression_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5230 "parser.c"
        break;

    case YYSYMBOL_argument_expression_list: /* argument_expression_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5246 "parser.c"
        break;

    case YYSYMBOL_unary_expression: /* unary_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5262 "parser.c"
        break;

    case YYSYMBOL_unary_operator: /* unary_operator  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5268 "parser.c"
        break;

    case YYSYMBOL_cast_expression: /* cast_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5284 "parser.c"
        break;

    case YYSYMBOL_multiplicative_expression: /* multiplicative_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5300 "parser.c"
        break;

    case YYSYMBOL_additive_expression: /* additive_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5316 "parser.c"
        break;

    case YYSYMBOL_shift_expression: /* shift_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5332 "parser.c"
        break;

    case YYSYMBOL_relational_expression: /* relational_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5348 "parser.c"
        break;

    case YYSYMBOL_equality_expression: /* equality_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5364 "parser.c"
        break;

    case YYSYMBOL_and_expression: /* and_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5380 "parser.c"
        break;

    case YYSYMBOL_exclusive_or_expression: /* exclusive_or_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5396 "parser.c"
        break;

    case YYSYMBOL_inclusive_or_expression: /* inclusive_or_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5412 "parser.c"
        break;

    case YYSYMBOL_logical_and_expression: /* logical_and_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5428 "parser.c"
        break;

    case YYSYMBOL_logical_or_expression: /* logical_or_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5444 "parser.c"
        break;

    case YYSYMBOL_conditional_expression: /* conditional_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5460 "parser.c"
        break;

    case YYSYMBOL_opt_assignment_expression: /* opt_assignment_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5476 "parser.c"
        break;

    case YYSYMBOL_assignment_expression: /* assignment_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5492 "parser.c"
        break;

    case YYSYMBOL_assignment_operator: /* assignment_operator  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5498 "parser.c"
        break;

    case YYSYMBOL_expression: /* expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5514 "parser.c"
        break;

    case YYSYMBOL_constant_expression: /* constant_expression  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5530 "parser.c"
        break;

    case YYSYMBOL_constant_expression_flat: /* constant_expression_flat  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 5536 "parser.c"
        break;

    case YYSYMBOL_declaration: /* declaration  */
#line 303 "parser.y"
            { decl_clear(&((*yyvaluep).decl)); ((*yyvaluep).decl) = (struct decl){ 0 }; }
#line 5542 "parser.c"
        break;

    case YYSYMBOL_declaration_specifier: /* declaration_specifier  */
#line 356 "parser.y"
            { c_specifier_free(&((*yyvaluep).cspc)); ((*yyvaluep).cspc) = (struct c_specifier){ 0 }; }
#line 5548 "parser.c"
        break;

    case YYSYMBOL_declaration_specifiers: /* declaration_specifiers  */
#line 464 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vcspc)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vcspc),i);
        c_specifier_free((struct c_specifier*)_item);    }
    sv_free(&(((*yyvaluep).vcspc)));
}
 ((*yyvaluep).vcspc) = NULL; }
#line 5560 "parser.c"
        break;

    case YYSYMBOL_declaration_specifiers_checked: /* declaration_specifiers_checked  */
#line 464 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vcspc)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vcspc),i);
        c_specifier_free((struct c_specifier*)_item);    }
    sv_free(&(((*yyvaluep).vcspc)));
}
 ((*yyvaluep).vcspc) = NULL; }
#line 5572 "parser.c"
        break;

    case YYSYMBOL_storage_class_specifier: /* storage_class_specifier  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5578 "parser.c"
        break;

    case YYSYMBOL_simple_type_specifier: /* simple_type_specifier  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5584 "parser.c"
        break;

    case YYSYMBOL_type_specifier: /* type_specifier  */
#line 358 "parser.y"
            { c_typespec_free(&((*yyvaluep).ctyp)); ((*yyvaluep).ctyp) = (struct c_typespec){ 0 }; }
#line 5590 "parser.c"
        break;

    case YYSYMBOL_struct_or_union_specifier: /* struct_or_union_specifier  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5606 "parser.c"
        break;

    case YYSYMBOL_struct_or_union: /* struct_or_union  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5612 "parser.c"
        break;

    case YYSYMBOL_struct_declaration_list: /* struct_declaration_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5628 "parser.c"
        break;

    case YYSYMBOL_struct_declaration: /* struct_declaration  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5644 "parser.c"
        break;

    case YYSYMBOL_specifier_qualifier_list: /* specifier_qualifier_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5660 "parser.c"
        break;

    case YYSYMBOL_opt_struct_declarator_list: /* opt_struct_declarator_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5676 "parser.c"
        break;

    case YYSYMBOL_struct_declarator_list: /* struct_declarator_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5692 "parser.c"
        break;

    case YYSYMBOL_struct_declarator: /* struct_declarator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5708 "parser.c"
        break;

    case YYSYMBOL_enum_specifier: /* enum_specifier  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5724 "parser.c"
        break;

    case YYSYMBOL_enumerator_list: /* enumerator_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5740 "parser.c"
        break;

    case YYSYMBOL_enumerator: /* enumerator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5756 "parser.c"
        break;

    case YYSYMBOL_atomic_type_specifier: /* atomic_type_specifier  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5772 "parser.c"
        break;

    case YYSYMBOL_type_qualifier: /* type_qualifier  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5778 "parser.c"
        break;

    case YYSYMBOL_function_specifier: /* function_specifier  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 5784 "parser.c"
        break;

    case YYSYMBOL_alignment_specifier: /* alignment_specifier  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5800 "parser.c"
        break;

    case YYSYMBOL_declarator: /* declarator  */
#line 307 "parser.y"
            { arr_cdd_free(&((*yyvaluep).acdd)); ((*yyvaluep).acdd) = (arr_cdd){ 0 }; }
#line 5806 "parser.c"
        break;

    case YYSYMBOL_direct_declarator: /* direct_declarator  */
#line 307 "parser.y"
            { arr_cdd_free(&((*yyvaluep).acdd)); ((*yyvaluep).acdd) = (arr_cdd){ 0 }; }
#line 5812 "parser.c"
        break;

    case YYSYMBOL_array_direct_declarator: /* array_direct_declarator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5828 "parser.c"
        break;

    case YYSYMBOL_function_direct_declarator: /* function_direct_declarator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5844 "parser.c"
        break;

    case YYSYMBOL_pointer: /* pointer  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5860 "parser.c"
        break;

    case YYSYMBOL_pointer_elem: /* pointer_elem  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5876 "parser.c"
        break;

    case YYSYMBOL_opt_type_qualifier_list: /* opt_type_qualifier_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5892 "parser.c"
        break;

    case YYSYMBOL_type_qualifier_list: /* type_qualifier_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5908 "parser.c"
        break;

    case YYSYMBOL_parameter_type_list: /* parameter_type_list  */
#line 415 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdecl)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdecl),i);
        decl_clear((struct decl*)(_item));    }
    sv_free(&(((*yyvaluep).vdecl)));
}
 ((*yyvaluep).vdecl) = NULL; }
#line 5920 "parser.c"
        break;

    case YYSYMBOL_parameter_type_list_flat: /* parameter_type_list_flat  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5936 "parser.c"
        break;

    case YYSYMBOL_parameter_list: /* parameter_list  */
#line 415 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdecl)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdecl),i);
        decl_clear((struct decl*)(_item));    }
    sv_free(&(((*yyvaluep).vdecl)));
}
 ((*yyvaluep).vdecl) = NULL; }
#line 5948 "parser.c"
        break;

    case YYSYMBOL_parameter_declaration: /* parameter_declaration  */
#line 303 "parser.y"
            { decl_clear(&((*yyvaluep).decl)); ((*yyvaluep).decl) = (struct decl){ 0 }; }
#line 5954 "parser.c"
        break;

    case YYSYMBOL_parameter_declaration_typed: /* parameter_declaration_typed  */
#line 303 "parser.y"
            { decl_clear(&((*yyvaluep).decl)); ((*yyvaluep).decl) = (struct decl){ 0 }; }
#line 5960 "parser.c"
        break;

    case YYSYMBOL_opt_identifier_list: /* opt_identifier_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5976 "parser.c"
        break;

    case YYSYMBOL_identifier_list: /* identifier_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 5992 "parser.c"
        break;

    case YYSYMBOL_type_name: /* type_name  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6008 "parser.c"
        break;

    case YYSYMBOL_abstract_declarator: /* abstract_declarator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6024 "parser.c"
        break;

    case YYSYMBOL_direct_abstract_declarator: /* direct_abstract_declarator  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6040 "parser.c"
        break;

    case YYSYMBOL_initializer: /* initializer  */
#line 354 "parser.y"
            { c_initializer_free(&((*yyvaluep).cini)); ((*yyvaluep).cini) = (struct c_initializer){ 0 }; }
#line 6046 "parser.c"
        break;

    case YYSYMBOL_initializer_flat: /* initializer_flat  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6052 "parser.c"
        break;

    case YYSYMBOL_compound_initializer_list: /* compound_initializer_list  */
#line 407 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vacdi)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vacdi),i);
        arr_cdi_free((arr_cdi*)(_item));    }
    sv_free(&(((*yyvaluep).vacdi)));
}
 ((*yyvaluep).vacdi) = NULL; }
#line 6064 "parser.c"
        break;

    case YYSYMBOL_compound_initializer: /* compound_initializer  */
#line 305 "parser.y"
            { arr_cdi_free(&((*yyvaluep).acdi)); ((*yyvaluep).acdi) = (arr_cdi){ 0 }; }
#line 6070 "parser.c"
        break;

    case YYSYMBOL_compound_literal: /* compound_literal  */
#line 305 "parser.y"
            { arr_cdi_free(&((*yyvaluep).acdi)); ((*yyvaluep).acdi) = (arr_cdi){ 0 }; }
#line 6076 "parser.c"
        break;

    case YYSYMBOL_braced_init_list: /* braced_init_list  */
#line 305 "parser.y"
            { arr_cdi_free(&((*yyvaluep).acdi)); ((*yyvaluep).acdi) = (arr_cdi){ 0 }; }
#line 6082 "parser.c"
        break;

    case YYSYMBOL_initializer_list: /* initializer_list  */
#line 305 "parser.y"
            { arr_cdi_free(&((*yyvaluep).acdi)); ((*yyvaluep).acdi) = (arr_cdi){ 0 }; }
#line 6088 "parser.c"
        break;

    case YYSYMBOL_designation: /* designation  */
#line 352 "parser.y"
            { c_designator_free(&((*yyvaluep).cdsg)); ((*yyvaluep).cdsg) = (struct c_designator){ 0 }; }
#line 6094 "parser.c"
        break;

    case YYSYMBOL_designator: /* designator  */
#line 352 "parser.y"
            { c_designator_free(&((*yyvaluep).cdsg)); ((*yyvaluep).cdsg) = (struct c_designator){ 0 }; }
#line 6100 "parser.c"
        break;

    case YYSYMBOL_paren_c_expression: /* paren_c_expression  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6106 "parser.c"
        break;

    case YYSYMBOL_c_expression_list: /* c_expression_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6122 "parser.c"
        break;

    case YYSYMBOL_paren_c_expression_list: /* paren_c_expression_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6138 "parser.c"
        break;

    case YYSYMBOL_opt_pipe: /* opt_pipe  */
#line 297 "parser.y"
            {  ((*yyvaluep).ctxt) = NULL; }
#line 6144 "parser.c"
        break;

    case YYSYMBOL_mod_assign: /* mod_assign  */
#line 291 "parser.y"
            {  ((*yyvaluep).c) = '\0'; }
#line 6150 "parser.c"
        break;

    case YYSYMBOL_identifier_ext: /* identifier_ext  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6156 "parser.c"
        break;

    case YYSYMBOL_option: /* option  */
#line 364 "parser.y"
            { cmod_option_free(&((*yyvaluep).opt)); ((*yyvaluep).opt) = (struct cmod_option){ 0 }; }
#line 6162 "parser.c"
        break;

    case YYSYMBOL_option_comma_list: /* option_comma_list  */
#line 456 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vopt)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&(((*yyvaluep).vopt)));
}
 ((*yyvaluep).vopt) = NULL; }
#line 6174 "parser.c"
        break;

    case YYSYMBOL_option_comma_list_nodang: /* option_comma_list_nodang  */
#line 456 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vopt)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&(((*yyvaluep).vopt)));
}
 ((*yyvaluep).vopt) = NULL; }
#line 6186 "parser.c"
        break;

    case YYSYMBOL_keyword_options: /* keyword_options  */
#line 456 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vopt)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&(((*yyvaluep).vopt)));
}
 ((*yyvaluep).vopt) = NULL; }
#line 6198 "parser.c"
        break;

    case YYSYMBOL_opt_keyword_options: /* opt_keyword_options  */
#line 456 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vopt)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&(((*yyvaluep).vopt)));
}
 ((*yyvaluep).vopt) = NULL; }
#line 6210 "parser.c"
        break;

    case YYSYMBOL_opt_identifier: /* opt_identifier  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6216 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier: /* cmod_identifier  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6222 "parser.c"
        break;

    case YYSYMBOL_opt_cmod_identifier: /* opt_cmod_identifier  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6228 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier_comma_list: /* cmod_identifier_comma_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6244 "parser.c"
        break;

    case YYSYMBOL_cmod_identifier_comma_list_nodang: /* cmod_identifier_comma_list_nodang  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6260 "parser.c"
        break;

    case YYSYMBOL_paren_cmod_identifier_comma_list: /* paren_cmod_identifier_comma_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6276 "parser.c"
        break;

    case YYSYMBOL_special_id_list: /* special_id_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6292 "parser.c"
        break;

    case YYSYMBOL_special_id_list_nodang: /* special_id_list_nodang  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6308 "parser.c"
        break;

    case YYSYMBOL_paren_special_id_list: /* paren_special_id_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6324 "parser.c"
        break;

    case YYSYMBOL_integer: /* integer  */
#line 360 "parser.y"
            { xint_free(&((*yyvaluep).xint)); ((*yyvaluep).xint) = (struct xint){ 0 }; }
#line 6330 "parser.c"
        break;

    case YYSYMBOL_integer_string: /* integer_string  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6336 "parser.c"
        break;

    case YYSYMBOL_integer_comma_list: /* integer_comma_list  */
#line 366 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vxint)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vxint),i);
        xint_free((struct xint*)_item);    }
    sv_free(&(((*yyvaluep).vxint)));
}
 ((*yyvaluep).vxint) = NULL; }
#line 6348 "parser.c"
        break;

    case YYSYMBOL_integer_comma_list_nodang: /* integer_comma_list_nodang  */
#line 366 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vxint)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vxint),i);
        xint_free((struct xint*)_item);    }
    sv_free(&(((*yyvaluep).vxint)));
}
 ((*yyvaluep).vxint) = NULL; }
#line 6360 "parser.c"
        break;

    case YYSYMBOL_paren_integer_comma_list: /* paren_integer_comma_list  */
#line 366 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vxint)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vxint),i);
        xint_free((struct xint*)_item);    }
    sv_free(&(((*yyvaluep).vxint)));
}
 ((*yyvaluep).vxint) = NULL; }
#line 6372 "parser.c"
        break;

    case YYSYMBOL_paren_unsigned_int: /* paren_unsigned_int  */
#line 360 "parser.y"
            { xint_free(&((*yyvaluep).xint)); ((*yyvaluep).xint) = (struct xint){ 0 }; }
#line 6378 "parser.c"
        break;

    case YYSYMBOL_opt_UNSIGNED_INT: /* opt_UNSIGNED_INT  */
#line 360 "parser.y"
            { xint_free(&((*yyvaluep).xint)); ((*yyvaluep).xint) = (struct xint){ 0 }; }
#line 6384 "parser.c"
        break;

    case YYSYMBOL_mod_herestring: /* mod_herestring  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6390 "parser.c"
        break;

    case YYSYMBOL_opt_mod_herestring: /* opt_mod_herestring  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6396 "parser.c"
        break;

    case YYSYMBOL_opt_herestring: /* opt_herestring  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6402 "parser.c"
        break;

    case YYSYMBOL_herestring: /* herestring  */
#line 299 "parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 6408 "parser.c"
        break;

    case YYSYMBOL_table_like: /* table_like  */
#line 317 "parser.y"
            { ((*yyvaluep).tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).sel); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).lst); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).tlike).lst));
}
 ((*yyvaluep).tlike) = (struct table_like){ 0 }; }
#line 6430 "parser.c"
        break;

    case YYSYMBOL_table: /* table  */
#line 313 "parser.y"
            { table_clear(&((*yyvaluep).tab)); ((*yyvaluep).tab) = (struct table){ 0 }; }
#line 6436 "parser.c"
        break;

    case YYSYMBOL_table_selection: /* table_selection  */
#line 317 "parser.y"
            { ((*yyvaluep).tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).sel); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len(((*yyvaluep).tlike).lst); ++i) {
        const void *_item = sv_at(((*yyvaluep).tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).tlike).lst));
}
 ((*yyvaluep).tlike) = (struct table_like){ 0 }; }
#line 6458 "parser.c"
        break;

    case YYSYMBOL_heretab_verbatim_char_list: /* heretab_verbatim_char_list  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6464 "parser.c"
        break;

    case YYSYMBOL_heretab_verbatim: /* heretab_verbatim  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6470 "parser.c"
        break;

    case YYSYMBOL_lambda_item: /* lambda_item  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6486 "parser.c"
        break;

    case YYSYMBOL_lambda_item_comma_list: /* lambda_item_comma_list  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6502 "parser.c"
        break;

    case YYSYMBOL_lambda_table_column: /* lambda_table_column  */
#line 374 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(((*yyvaluep).vstr)));
}
 ((*yyvaluep).vstr) = NULL; }
#line 6518 "parser.c"
        break;

    case YYSYMBOL_lambda_table_column_list: /* lambda_table_column_list  */
#line 386 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vvstr)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vvstr),i);
        {
    vec_string* vec = *(vec_string**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).vvstr)));
}
 ((*yyvaluep).vvstr) = NULL; }
#line 6543 "parser.c"
        break;

    case YYSYMBOL_lambda_table: /* lambda_table  */
#line 313 "parser.y"
            { table_clear(&((*yyvaluep).tab)); ((*yyvaluep).tab) = (struct table){ 0 }; }
#line 6549 "parser.c"
        break;

    case YYSYMBOL_recall_like: /* recall_like  */
#line 335 "parser.y"
            { ((*yyvaluep).rlike).snip = NULL; free(((*yyvaluep).rlike).name); ((*yyvaluep).rlike).name = NULL; {
    for(size_t i = 0; i < sv_len(((*yyvaluep).rlike).arg_lists); ++i) {
        const void *_item = sv_at(((*yyvaluep).rlike).arg_lists,i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).rlike).arg_lists));
}
 ((*yyvaluep).rlike) = (struct recall_like){ 0 }; }
#line 6570 "parser.c"
        break;

    case YYSYMBOL_recall_end: /* recall_end  */
#line 291 "parser.y"
            {  ((*yyvaluep).c) = '\0'; }
#line 6576 "parser.c"
        break;

    case YYSYMBOL_snippet_like: /* snippet_like  */
#line 315 "parser.y"
            { snippet_clear(&((*yyvaluep).snip)); ((*yyvaluep).snip) = (struct snippet){ 0 }; }
#line 6582 "parser.c"
        break;

    case YYSYMBOL_opt_DLLARG: /* opt_DLLARG  */
#line 309 "parser.y"
            { dllarg_clear(&((*yyvaluep).dll)); ((*yyvaluep).dll) = (struct dllarg){ 0 }; }
#line 6588 "parser.c"
        break;

    case YYSYMBOL_dllarg: /* dllarg  */
#line 309 "parser.y"
            { dllarg_clear(&((*yyvaluep).dll)); ((*yyvaluep).dll) = (struct dllarg){ 0 }; }
#line 6594 "parser.c"
        break;

    case YYSYMBOL_snippet_body: /* snippet_body  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6600 "parser.c"
        break;

    case YYSYMBOL_opt_snippet_body: /* opt_snippet_body  */
#line 301 "parser.y"
            { ss_free(&(((*yyvaluep).str))); ((*yyvaluep).str) = NULL; }
#line 6606 "parser.c"
        break;

    case YYSYMBOL_cmod_plain_argument: /* cmod_plain_argument  */
#line 311 "parser.y"
            { namarg_free(&((*yyvaluep).narg)); ((*yyvaluep).narg) = (struct namarg){ 0 }; }
#line 6612 "parser.c"
        break;

    case YYSYMBOL_cmod_named_argument: /* cmod_named_argument  */
#line 311 "parser.y"
            { namarg_free(&((*yyvaluep).narg)); ((*yyvaluep).narg) = (struct namarg){ 0 }; }
#line 6618 "parser.c"
        break;

    case YYSYMBOL_cmod_argument_comma_list: /* cmod_argument_comma_list  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6630 "parser.c"
        break;

    case YYSYMBOL_cmod_argument_comma_list_nodang: /* cmod_argument_comma_list_nodang  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6642 "parser.c"
        break;

    case YYSYMBOL_cmod_arguments: /* cmod_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6654 "parser.c"
        break;

    case YYSYMBOL_cmod_arguments_unique: /* cmod_arguments_unique  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6666 "parser.c"
        break;

    case YYSYMBOL_cmod_function_arguments: /* cmod_function_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6678 "parser.c"
        break;

    case YYSYMBOL_cmod_formal_arguments: /* cmod_formal_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6690 "parser.c"
        break;

    case YYSYMBOL_empty_cmod_arguments: /* empty_cmod_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6702 "parser.c"
        break;

    case YYSYMBOL_snippet_formal_arguments: /* snippet_formal_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6714 "parser.c"
        break;

    case YYSYMBOL_snippet_formal_arguments_inout: /* snippet_formal_arguments_inout  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6726 "parser.c"
        break;

    case YYSYMBOL_opt_table_columns: /* opt_table_columns  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6738 "parser.c"
        break;

    case YYSYMBOL_table_columns: /* table_columns  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6750 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments: /* snippet_actual_arguments  */
#line 431 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(((*yyvaluep).vnarg)));
}
 ((*yyvaluep).vnarg) = NULL; }
#line 6762 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_list: /* snippet_actual_arguments_list  */
#line 439 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vvnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vvnarg),i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).vvnarg)));
}
 ((*yyvaluep).vvnarg) = NULL; }
#line 6783 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_inout: /* snippet_actual_arguments_inout  */
#line 439 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vvnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vvnarg),i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).vvnarg)));
}
 ((*yyvaluep).vvnarg) = NULL; }
#line 6804 "parser.c"
        break;

    case YYSYMBOL_snippet_actual_arguments_inout_idcap: /* snippet_actual_arguments_inout_idcap  */
#line 439 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vvnarg)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vvnarg),i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(((*yyvaluep).vvnarg)));
}
 ((*yyvaluep).vvnarg) = NULL; }
#line 6825 "parser.c"
        break;

    case YYSYMBOL_mod_case: /* mod_case  */
#line 362 "parser.y"
            { mod_case_free(&((*yyvaluep).mcase)); ((*yyvaluep).mcase) = (struct mod_case){ 0 }; }
#line 6831 "parser.c"
        break;

    case YYSYMBOL_case_list: /* case_list  */
#line 423 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vmcase)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vmcase),i);
        mod_case_free((struct mod_case*)_item);    }
    sv_free(&(((*yyvaluep).vmcase)));
}
 ((*yyvaluep).vmcase) = NULL; }
#line 6843 "parser.c"
        break;

    case YYSYMBOL_opt_case_list: /* opt_case_list  */
#line 423 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vmcase)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vmcase),i);
        mod_case_free((struct mod_case*)_item);    }
    sv_free(&(((*yyvaluep).vmcase)));
}
 ((*yyvaluep).vmcase) = NULL; }
#line 6855 "parser.c"
        break;

    case YYSYMBOL_switch_body: /* switch_body  */
#line 423 "parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vmcase)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vmcase),i);
        mod_case_free((struct mod_case*)_item);    }
    sv_free(&(((*yyvaluep).vmcase)));
}
 ((*yyvaluep).vmcase) = NULL; }
#line 6867 "parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (void* yyscanner, struct param *pp)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 10 "parser.y"
{ pp = yyget_extra(yyscanner); }

#line 6964 "parser.c"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* opt_c_identifier: %empty  */
#line 855 "parser.y"
             {
        (yyval.txt) = NULL;
        
    }
#line 7180 "parser.c"
    break;

  case 3: /* opt_c_identifier: "C identifier"  */
#line 859 "parser.y"
                   { (yyval.txt) = (yyvsp[0].txt);  }
#line 7186 "parser.c"
    break;

  case 4: /* opt_comma: %empty  */
#line 863 "parser.y"
             { (yyval.ctxt) = NULL; }
#line 7192 "parser.c"
    break;

  case 8: /* opt_c_name: %empty  */
#line 872 "parser.y"
             {
        (yyval.txt) = NULL;
        
    }
#line 7201 "parser.c"
    break;

  case 9: /* opt_c_name: c_name  */
#line 876 "parser.y"
             { (yyval.txt) = (yyvsp[0].txt);  }
#line 7207 "parser.c"
    break;

  case 10: /* c_name_comma_list: c_name  */
#line 879 "parser.y"
             {     ((yyval.vstr)) = sv_alloc_t(SV_PTR,4);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }
 }
#line 7216 "parser.c"
    break;

  case 11: /* c_name_comma_list: c_name_comma_list ',' c_name  */
#line 883 "parser.y"
                                   {
        (yyval.vstr) = (yyvsp[-2].vstr);     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 7226 "parser.c"
    break;

  case 17: /* string: "__func__"  */
#line 905 "parser.y"
                                               {
                (yyval.txt) = (NULL != ((yyvsp[0].ctxt))) ? rstrdup((yyvsp[0].ctxt)) : NULL;
    if(NULL != ((yyvsp[0].ctxt)) && NULL == ((yyval.txt))) { YYNOMEM; }
   

    }
#line 7237 "parser.c"
    break;

  case 18: /* primary_expression: "C identifier"  */
#line 916 "parser.y"
                   {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 7247 "parser.c"
    break;

  case 19: /* primary_expression: numeric_constant  */
#line 921 "parser.y"
                       {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 7257 "parser.c"
    break;

  case 20: /* primary_expression: string  */
#line 926 "parser.y"
             {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 7267 "parser.c"
    break;

  case 21: /* primary_expression: '(' expression ')'  */
#line 931 "parser.y"
                         {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 7282 "parser.c"
    break;

  case 23: /* postfix_expression: postfix_expression '[' expression ']'  */
#line 949 "parser.y"
                                            {
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7298 "parser.c"
    break;

  case 24: /* postfix_expression: postfix_expression '(' opt_argument_expression_list ')'  */
#line 960 "parser.y"
                                                              {
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(NULL != ((yyvsp[-1].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));
 }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7315 "parser.c"
    break;

  case 25: /* postfix_expression: postfix_expression '.' "C identifier"  */
#line 972 "parser.y"
                                          {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

    }
#line 7328 "parser.c"
    break;

  case 26: /* postfix_expression: postfix_expression "->" "C identifier"  */
#line 980 "parser.y"
                                               {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

    }
#line 7341 "parser.c"
    break;

  case 27: /* postfix_expression: postfix_expression "++"  */
#line 988 "parser.y"
                                   {
            (yyval.vstr) = (yyvsp[-1].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7352 "parser.c"
    break;

  case 28: /* postfix_expression: postfix_expression "--"  */
#line 994 "parser.y"
                                   {
            (yyval.vstr) = (yyvsp[-1].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7363 "parser.c"
    break;

  case 29: /* opt_argument_expression_list: %empty  */
#line 1004 "parser.y"
             {
        (yyval.vstr) = NULL;
        
    }
#line 7372 "parser.c"
    break;

  case 30: /* opt_argument_expression_list: argument_expression_list  */
#line 1008 "parser.y"
                               { (yyval.vstr) = (yyvsp[0].vstr);  }
#line 7378 "parser.c"
    break;

  case 32: /* argument_expression_list: argument_expression_list ',' assignment_expression  */
#line 1012 "parser.y"
                                                         {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7392 "parser.c"
    break;

  case 34: /* unary_expression: "++" unary_expression  */
#line 1025 "parser.y"
                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7406 "parser.c"
    break;

  case 35: /* unary_expression: "--" unary_expression  */
#line 1034 "parser.y"
                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7420 "parser.c"
    break;

  case 36: /* unary_expression: unary_operator cast_expression  */
#line 1043 "parser.y"
                                     {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 7433 "parser.c"
    break;

  case 37: /* unary_expression: "sizeof" unary_expression  */
#line 1051 "parser.y"
                                {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 7446 "parser.c"
    break;

  case 38: /* unary_expression: "sizeof" '(' type_name ')'  */
#line 1059 "parser.y"
                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7462 "parser.c"
    break;

  case 39: /* unary_expression: "_Alignof" '(' type_name ')'  */
#line 1070 "parser.y"
                                  {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 7478 "parser.c"
    break;

  case 47: /* cast_expression: '(' type_name ')' cast_expression  */
#line 1093 "parser.y"
                                        {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7497 "parser.c"
    break;

  case 49: /* multiplicative_expression: multiplicative_expression '*' cast_expression  */
#line 1111 "parser.y"
                                                    {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7511 "parser.c"
    break;

  case 50: /* multiplicative_expression: multiplicative_expression '/' cast_expression  */
#line 1120 "parser.y"
                                                    {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7525 "parser.c"
    break;

  case 51: /* multiplicative_expression: multiplicative_expression '%' cast_expression  */
#line 1129 "parser.y"
                                                    {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7539 "parser.c"
    break;

  case 53: /* additive_expression: additive_expression '+' multiplicative_expression  */
#line 1142 "parser.y"
                                                        {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7553 "parser.c"
    break;

  case 54: /* additive_expression: additive_expression '-' multiplicative_expression  */
#line 1151 "parser.y"
                                                        {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7567 "parser.c"
    break;

  case 56: /* shift_expression: shift_expression "<<" additive_expression  */
#line 1164 "parser.y"
                                                     {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7581 "parser.c"
    break;

  case 57: /* shift_expression: shift_expression ">>" additive_expression  */
#line 1173 "parser.y"
                                                      {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7595 "parser.c"
    break;

  case 59: /* relational_expression: relational_expression '<' shift_expression  */
#line 1186 "parser.y"
                                                 {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7609 "parser.c"
    break;

  case 60: /* relational_expression: relational_expression '>' shift_expression  */
#line 1195 "parser.y"
                                                 {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7623 "parser.c"
    break;

  case 61: /* relational_expression: relational_expression "<=" shift_expression  */
#line 1204 "parser.y"
                                                     {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7637 "parser.c"
    break;

  case 62: /* relational_expression: relational_expression ">=" shift_expression  */
#line 1213 "parser.y"
                                                     {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7651 "parser.c"
    break;

  case 64: /* equality_expression: equality_expression "==" relational_expression  */
#line 1226 "parser.y"
                                                        {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7665 "parser.c"
    break;

  case 65: /* equality_expression: equality_expression "!=" relational_expression  */
#line 1235 "parser.y"
                                                        {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7679 "parser.c"
    break;

  case 67: /* and_expression: and_expression '&' equality_expression  */
#line 1248 "parser.y"
                                             {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7693 "parser.c"
    break;

  case 69: /* exclusive_or_expression: exclusive_or_expression '^' and_expression  */
#line 1261 "parser.y"
                                                 {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7707 "parser.c"
    break;

  case 71: /* inclusive_or_expression: inclusive_or_expression '|' exclusive_or_expression  */
#line 1274 "parser.y"
                                                          {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7721 "parser.c"
    break;

  case 73: /* logical_and_expression: logical_and_expression "&&" inclusive_or_expression  */
#line 1287 "parser.y"
                                                          {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7735 "parser.c"
    break;

  case 75: /* logical_or_expression: logical_or_expression "||" logical_and_expression  */
#line 1300 "parser.y"
                                                        {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7749 "parser.c"
    break;

  case 77: /* conditional_expression: logical_or_expression '?' expression ':' conditional_expression  */
#line 1313 "parser.y"
                                                                      {
            (yyval.vstr) = (yyvsp[-4].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7768 "parser.c"
    break;

  case 78: /* opt_assignment_expression: %empty  */
#line 1330 "parser.y"
             {
        (yyval.vstr) = NULL;
        
    }
#line 7777 "parser.c"
    break;

  case 79: /* opt_assignment_expression: assignment_expression  */
#line 1334 "parser.y"
                            { (yyval.vstr) = (yyvsp[0].vstr);  }
#line 7783 "parser.c"
    break;

  case 81: /* assignment_expression: unary_expression assignment_operator assignment_expression  */
#line 1338 "parser.y"
                                                                 {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7797 "parser.c"
    break;

  case 94: /* expression: expression ',' assignment_expression  */
#line 1364 "parser.y"
                                           {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

    }
#line 7811 "parser.c"
    break;

  case 96: /* constant_expression_flat: constant_expression  */
#line 1380 "parser.y"
                          { {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[0].vstr),' '))) { YYNOMEM; }
    if(NULL == (((yyval.txt)) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) free(sv_at_ptr((yyvsp[0].vstr),i));
    sv_free(&(yyvsp[0].vstr));
}

 }
#line 7826 "parser.c"
    break;

  case 97: /* declaration: declaration_specifiers_checked declarator ';'  */
#line 1395 "parser.y"
                                                    { (void)(yyvsp[0].ctxt);
        (yyval.decl) = (struct decl){ .sv_spec= (yyvsp[-2].vcspc), .pointer= (yyvsp[-1].acdd).pointer };
        (yyvsp[-2].vcspc) = NULL; (yyvsp[-1].acdd).pointer = NULL; // hand-over
        if(arr_cdd_to_decl(&(yyvsp[-1].acdd),&(yyval.decl))) {
            pp->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("invalid C declarator");
        }
        arr_cdd_free(&(yyvsp[-1].acdd));
            for(size_t i = 0; i < sv_len(((yyval.decl)).sv_spec); ++i) {
        const struct c_specifier *spec = sv_at(((yyval.decl)).sv_spec,i);
        if(spec->type == ENUM_C_SPECIFIER(TYPE)) {
            ((yyval.decl)).type = C_TYPESPEC_get(C_SPECIFIER_TYPE_get(*spec));
            break;
        }
    }
    }
#line 7847 "parser.c"
    break;

  case 98: /* declaration_specifier: storage_class_specifier  */
#line 1415 "parser.y"
                              {
        char *        val = (NULL != ((yyvsp[0].ctxt))) ? rstrdup((yyvsp[0].ctxt)) : NULL;
    if(NULL != ((yyvsp[0].ctxt)) && NULL == (val)) { YYNOMEM; }
   

        (yyval.cspc) = C_SPECIFIER_STORAGE_set(val);
    }
#line 7859 "parser.c"
    break;

  case 99: /* declaration_specifier: type_qualifier  */
#line 1422 "parser.y"
                     {
        char *        val = (NULL != ((yyvsp[0].ctxt))) ? rstrdup((yyvsp[0].ctxt)) : NULL;
    if(NULL != ((yyvsp[0].ctxt)) && NULL == (val)) { YYNOMEM; }
   

        (yyval.cspc) = C_SPECIFIER_QUALIFIER_set(val);
    }
#line 7871 "parser.c"
    break;

  case 100: /* declaration_specifier: function_specifier  */
#line 1429 "parser.y"
                         {
        char *        val = (NULL != ((yyvsp[0].ctxt))) ? rstrdup((yyvsp[0].ctxt)) : NULL;
    if(NULL != ((yyvsp[0].ctxt)) && NULL == (val)) { YYNOMEM; }
   

        (yyval.cspc) = C_SPECIFIER_FUNCTION_set(val);
    }
#line 7883 "parser.c"
    break;

  case 101: /* declaration_specifier: alignment_specifier  */
#line 1436 "parser.y"
                          {
        char *val = NULL; {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[0].vstr),' '))) { YYNOMEM; }
    if(NULL == ((val) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) free(sv_at_ptr((yyvsp[0].vstr),i));
    sv_free(&(yyvsp[0].vstr));
}

        (yyval.cspc) = C_SPECIFIER_ALIGNMENT_set(val);
    }
#line 7900 "parser.c"
    break;

  case 102: /* declaration_specifier: type_specifier  */
#line 1448 "parser.y"
                     {
        (yyval.cspc) = C_SPECIFIER_TYPE_set((yyvsp[0].ctyp));
        (yyvsp[0].ctyp) = (struct c_typespec){ 0 }; // hand-over
    }
#line 7909 "parser.c"
    break;

  case 103: /* declaration_specifiers: declaration_specifier  */
#line 1455 "parser.y"
                            {
            ((yyval.vcspc)) = sv_alloc(sizeof(struct c_specifier),2,NULL);
    if(sv_void == ((yyval.vcspc))) { YYNOMEM; }
            if(!sv_push(&((yyval.vcspc)),&((yyvsp[0].cspc)))) { YYNOMEM; }
    }
#line 7919 "parser.c"
    break;

  case 104: /* declaration_specifiers: declaration_specifiers declaration_specifier  */
#line 1460 "parser.y"
                                                   {
        (yyval.vcspc) = (yyvsp[-1].vcspc);     if(!sv_push(&((yyval.vcspc)),&((yyvsp[0].cspc)))) { YYNOMEM; }
    }
#line 7927 "parser.c"
    break;

  case 105: /* declaration_specifiers_checked: declaration_specifiers  */
#line 1466 "parser.y"
                             {
        size_t ntyp = 0;
        // NOTE: we have to allow up to four type specifiers, e.g. unsigned long long int
        // FIXME: we are NOT checking for valid sequences of type specifiers
        for(size_t i = 0; i < sv_len((yyvsp[0].vcspc)) && ntyp <= 4; ++i) {
            const struct c_specifier *spec = sv_at((yyvsp[0].vcspc),i);
            ntyp += (ENUM_C_SPECIFIER(TYPE) == spec->type);
        }
        if(ntyp > 4)
            ABORT_PARSE0("too many type specifiers in C declaration");
        (yyval.vcspc) = (yyvsp[0].vcspc);
    }
#line 7944 "parser.c"
    break;

  case 123: /* type_specifier: simple_type_specifier  */
#line 1505 "parser.y"
                            {
        char *        val = (NULL != ((yyvsp[0].ctxt))) ? rstrdup((yyvsp[0].ctxt)) : NULL;
    if(NULL != ((yyvsp[0].ctxt)) && NULL == (val)) { YYNOMEM; }
   

        (yyval.ctyp) = C_TYPESPEC_SIMPLE_set(val);
    }
#line 7956 "parser.c"
    break;

  case 124: /* type_specifier: atomic_type_specifier  */
#line 1512 "parser.y"
                            {
        char *val = NULL; {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[0].vstr),' '))) { YYNOMEM; }
    if(NULL == ((val) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) free(sv_at_ptr((yyvsp[0].vstr),i));
    sv_free(&(yyvsp[0].vstr));
}

        (yyval.ctyp) = C_TYPESPEC_ATOMIC_set(val);
    }
#line 7973 "parser.c"
    break;

  case 125: /* type_specifier: struct_or_union_specifier  */
#line 1524 "parser.y"
                                {
        char *val = NULL; {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[0].vstr),' '))) { YYNOMEM; }
    if(NULL == ((val) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) free(sv_at_ptr((yyvsp[0].vstr),i));
    sv_free(&(yyvsp[0].vstr));
}

        (yyval.ctyp) = C_TYPESPEC_COMPOUND_set(val);
    }
#line 7990 "parser.c"
    break;

  case 126: /* type_specifier: enum_specifier  */
#line 1536 "parser.y"
                     {
        char *val = NULL; {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[0].vstr),' '))) { YYNOMEM; }
    if(NULL == ((val) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) free(sv_at_ptr((yyvsp[0].vstr),i));
    sv_free(&(yyvsp[0].vstr));
}

        (yyval.ctyp) = C_TYPESPEC_ENUM_set(val);
    }
#line 8007 "parser.c"
    break;

  case 127: /* type_specifier: "C typedef name"  */
#line 1548 "parser.y"
                     {
        (yyval.ctyp) = C_TYPESPEC_TYPEDEF_set((yyvsp[0].txt));
        (yyvsp[0].txt) = NULL; // hand-over
    }
#line 8016 "parser.c"
    break;

  case 128: /* struct_or_union_specifier: struct_or_union opt_c_name '{' struct_declaration_list '}'  */
#line 1555 "parser.y"
                                                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-4].ctxt))) { YYNOMEM; }

    if(NULL != ((yyvsp[-3].txt))) {     if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[-3].txt))) { YYNOMEM; }
 }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8037 "parser.c"
    break;

  case 129: /* struct_or_union_specifier: struct_or_union c_name  */
#line 1571 "parser.y"
                             {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 8049 "parser.c"
    break;

  case 133: /* struct_declaration_list: struct_declaration_list struct_declaration  */
#line 1586 "parser.y"
                                                 {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8060 "parser.c"
    break;

  case 134: /* struct_declaration: specifier_qualifier_list opt_struct_declarator_list ';'  */
#line 1595 "parser.y"
                                                              {
            (yyval.vstr) = (yyvsp[-2].vstr);

    if(NULL != ((yyvsp[-1].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));
 }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8075 "parser.c"
    break;

  case 135: /* specifier_qualifier_list: type_specifier  */
#line 1609 "parser.y"
                     {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),C_TYPESPEC_get((yyvsp[0].ctyp)))) { YYNOMEM; }

 }
#line 8085 "parser.c"
    break;

  case 136: /* specifier_qualifier_list: type_qualifier  */
#line 1614 "parser.y"
                     {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8095 "parser.c"
    break;

  case 137: /* specifier_qualifier_list: specifier_qualifier_list type_specifier  */
#line 1619 "parser.y"
                                              {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(!sv_push_ptr(&((yyval.vstr)),C_TYPESPEC_get((yyvsp[0].ctyp)))) { YYNOMEM; }

 }
#line 8105 "parser.c"
    break;

  case 138: /* specifier_qualifier_list: specifier_qualifier_list type_qualifier  */
#line 1624 "parser.y"
                                              {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8115 "parser.c"
    break;

  case 139: /* opt_struct_declarator_list: %empty  */
#line 1632 "parser.y"
             {
        (yyval.vstr) = NULL;
        
    }
#line 8124 "parser.c"
    break;

  case 140: /* opt_struct_declarator_list: struct_declarator_list  */
#line 1636 "parser.y"
                             { (yyval.vstr) = (yyvsp[0].vstr);  }
#line 8130 "parser.c"
    break;

  case 142: /* struct_declarator_list: struct_declarator_list ',' struct_declarator  */
#line 1640 "parser.y"
                                                   {     (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8143 "parser.c"
    break;

  case 143: /* struct_declarator: declarator  */
#line 1651 "parser.y"
                 { // flatten declarator
        struct decl tmp = { 0 };
        if(arr_cdd_to_decl(&(yyvsp[0].acdd),&tmp))
            ABORT_PARSE0("invalid C declarator");
        arr_cdd_free(&(yyvsp[0].acdd));
        (yyval.vstr) = tmp.sv_decl;
        tmp.sv_decl = NULL; // hand-over
        decl_clear(&tmp);
    }
#line 8157 "parser.c"
    break;

  case 144: /* enum_specifier: "enum" opt_c_name '{' enumerator_list opt_comma '}'  */
#line 1664 "parser.y"
                                                          { (void)(yyvsp[-1].ctxt);
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-5].ctxt))) { YYNOMEM; }

    if(NULL != ((yyvsp[-4].txt))) {     if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[-4].txt))) { YYNOMEM; }
 }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8178 "parser.c"
    break;

  case 145: /* enum_specifier: "enum" c_name  */
#line 1680 "parser.y"
                    {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 8190 "parser.c"
    break;

  case 147: /* enumerator_list: enumerator_list ',' enumerator  */
#line 1691 "parser.y"
                                     {     (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8203 "parser.c"
    break;

  case 148: /* enumerator: enumeration_constant  */
#line 1702 "parser.y"
                           {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 8213 "parser.c"
    break;

  case 149: /* enumerator: enumeration_constant '=' constant_expression  */
#line 1707 "parser.y"
                                                   {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[-2].txt))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8229 "parser.c"
    break;

  case 150: /* atomic_type_specifier: "_Atomic" '(' type_name ')'  */
#line 1721 "parser.y"
                                  {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8245 "parser.c"
    break;

  case 157: /* alignment_specifier: "_Alignas" '(' type_name ')'  */
#line 1746 "parser.y"
                                   {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8261 "parser.c"
    break;

  case 158: /* alignment_specifier: "_Alignas" '(' constant_expression ')'  */
#line 1757 "parser.y"
                                             {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8277 "parser.c"
    break;

  case 159: /* declarator: pointer direct_declarator  */
#line 1771 "parser.y"
                                { (yyval.acdd) = (yyvsp[0].acdd); (yyval.acdd).pointer = (yyvsp[-1].vstr); }
#line 8283 "parser.c"
    break;

  case 161: /* direct_declarator: "C identifier"  */
#line 1776 "parser.y"
                   {
        (yyval.acdd) = arr_cdd_add((arr_cdd){ 0 },C_DECLARATOR_IDENT_set((yyvsp[0].txt)));
    }
#line 8291 "parser.c"
    break;

  case 162: /* direct_declarator: '(' declarator ')'  */
#line 1779 "parser.y"
                         { (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt); // NOTE: does not store parentheses
        (yyval.acdd) = arr_cdd_add((arr_cdd){ 0 },C_DECLARATOR_DECL_set(arr_cdd_box((yyvsp[-1].acdd))));
    }
#line 8299 "parser.c"
    break;

  case 163: /* direct_declarator: direct_declarator array_direct_declarator  */
#line 1782 "parser.y"
                                                {
        (yyval.acdd) = arr_cdd_add((yyvsp[-1].acdd),C_DECLARATOR_ARRAY_set((yyvsp[0].vstr)));
    }
#line 8307 "parser.c"
    break;

  case 164: /* direct_declarator: direct_declarator function_direct_declarator  */
#line 1785 "parser.y"
                                                   {
        (yyval.acdd) = arr_cdd_add((yyvsp[-1].acdd),
                C_DECLARATOR_FUNC_xset((yyvsp[0].vstr), .typ= pp->param_types,
                                           .ids= pp->param_names,
                                           .ini= pp->param_inits,
                                           .ptr= pp->param_pntrs));
        pp->param_types = NULL; // hand-over
        pp->param_names = NULL; // hand-over
        pp->param_inits = NULL; // hand-over
        pp->param_pntrs = NULL; // hand-over
    }
#line 8323 "parser.c"
    break;

  case 165: /* array_direct_declarator: '[' opt_type_qualifier_list opt_assignment_expression ']'  */
#line 1800 "parser.y"
                                                                { (void)(yyvsp[-3].ctxt); (void)(yyvsp[0].ctxt);
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(NULL != ((yyvsp[-2].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));
 }

    if(NULL != ((yyvsp[-1].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));
 }

    }
#line 8340 "parser.c"
    break;

  case 166: /* array_direct_declarator: '[' "static" opt_type_qualifier_list assignment_expression ']'  */
#line 1812 "parser.y"
                                                                     { (void)(yyvsp[-4].ctxt); (void)(yyvsp[0].ctxt);
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(NULL != ((yyvsp[-2].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));
 }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    }
#line 8358 "parser.c"
    break;

  case 167: /* array_direct_declarator: '[' type_qualifier_list "static" assignment_expression ']'  */
#line 1825 "parser.y"
                                                                 { (void)(yyvsp[-4].ctxt); (void)(yyvsp[0].ctxt);
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    }
#line 8372 "parser.c"
    break;

  case 168: /* array_direct_declarator: '[' opt_type_qualifier_list '*' ']'  */
#line 1834 "parser.y"
                                          { (void)(yyvsp[-3].ctxt); (void)(yyvsp[0].ctxt);
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(NULL != ((yyvsp[-2].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));
 }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    }
#line 8387 "parser.c"
    break;

  case 169: /* function_direct_declarator: '(' parameter_type_list_flat ')'  */
#line 1848 "parser.y"
                                       { (yyval.vstr) = (yyvsp[-1].vstr); (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt); }
#line 8393 "parser.c"
    break;

  case 170: /* function_direct_declarator: '(' opt_identifier_list ')'  */
#line 1849 "parser.y"
                                  { (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt);
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

        for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
            char *item = sv_at_ptr((yyvsp[-1].vstr),i);
            if(i > 0)     if(!vstr_addstr(&((yyval.vstr)),",")) { YYNOMEM; }

 // add comma
                if(!sv_push_ptr(&((yyval.vstr)),item)) { YYNOMEM; }

        }
        sv_free(&(yyvsp[-1].vstr)); // hand-over
    }
#line 8411 "parser.c"
    break;

  case 172: /* pointer: pointer pointer_elem  */
#line 1866 "parser.y"
                           {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8422 "parser.c"
    break;

  case 173: /* pointer_elem: '*' opt_type_qualifier_list  */
#line 1875 "parser.y"
                                  {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt))) { YYNOMEM; }

    if(NULL != ((yyvsp[0].vstr))) {     if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));
 }

 }
#line 8436 "parser.c"
    break;

  case 174: /* opt_type_qualifier_list: %empty  */
#line 1887 "parser.y"
             {
        (yyval.vstr) = NULL;
        
    }
#line 8445 "parser.c"
    break;

  case 175: /* opt_type_qualifier_list: type_qualifier_list  */
#line 1891 "parser.y"
                          { (yyval.vstr) = (yyvsp[0].vstr);  }
#line 8451 "parser.c"
    break;

  case 176: /* type_qualifier_list: type_qualifier  */
#line 1894 "parser.y"
                     {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8461 "parser.c"
    break;

  case 177: /* type_qualifier_list: type_qualifier_list type_qualifier  */
#line 1899 "parser.y"
                                         {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8471 "parser.c"
    break;

  case 179: /* parameter_type_list: parameter_list ',' "..."  */
#line 1908 "parser.y"
                               { (void)(yyvsp[-1].ctxt);
        struct decl tmp = { 0 };
            if(NULL == (tmp.sv_decl = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

            if(!vstr_addstr(&(tmp.sv_decl),(yyvsp[0].ctxt))) { YYNOMEM; }

        (yyval.vdecl) = (yyvsp[-2].vdecl);     if(!sv_push(&((yyval.vdecl)),&(tmp))) { YYNOMEM; }
    }
#line 8484 "parser.c"
    break;

  case 180: /* parameter_type_list_flat: parameter_type_list  */
#line 1919 "parser.y"
                          {
        size_t nparam = sv_len((yyvsp[0].vdecl));
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

        if(NULL == pp->param_types) {     if(NULL == (pp->param_types = sv_alloc_t(SV_PTR,nparam))) { YYNOMEM; }

 }
        if(NULL == pp->param_names) {     if(NULL == (pp->param_names = sv_alloc_t(SV_PTR,nparam))) { YYNOMEM; }

 }
        if(NULL == pp->param_inits) {     if(NULL == (pp->param_inits = sv_alloc_t(SV_PTR,nparam))) { YYNOMEM; }

 }
        if(NULL == pp->param_pntrs) {     (pp->param_pntrs) = sv_alloc_t(SV_PTR,nparam);
    if(sv_void == (pp->param_pntrs)) { YYNOMEM; }
 }
        for(size_t i = 0; i < nparam; ++i) {
            struct decl *dcl = (struct decl*)sv_at((yyvsp[0].vdecl),i);
            srt_string *ss = decl_to_str(dcl,dcl->name ? dcl->name : "","",false,false);
            if(ss_void == ss) YYNOMEM;
            if(NULL != dcl->name) { // capture type, pointer, name and initializer
                    if(!vstr_addstr(&(pp->param_types),dcl->type)) { YYNOMEM; }

                dcl->type = NULL; // unborrow
                    if(!sv_push_ptr(&(pp->param_pntrs),dcl->pointer)) { YYNOMEM; }
                dcl->pointer = NULL; // hand-over
                    if(!vstr_addstr(&(pp->param_names),dcl->name)) { YYNOMEM; }

                dcl->name = NULL; // unborrow
                    if(!sv_push_ptr(&(pp->param_inits),dcl->init)) { YYNOMEM; }

                dcl->init = NULL; // hand-over
            }
            const char *item = ss_to_c(ss);
            if(i > 0)     if(!vstr_addstr(&((yyval.vstr)),",")) { YYNOMEM; }

 // add comma
                if(!vstr_addstr(&((yyval.vstr)),item)) { YYNOMEM; }

            ss_free(&ss);
        }
        {
    for(size_t i = 0; i < sv_len((yyvsp[0].vdecl)); ++i) {
        const void *_item = sv_at((yyvsp[0].vdecl),i);
        decl_clear((struct decl*)(_item));    }
    sv_free(&((yyvsp[0].vdecl)));
}
    }
#line 8537 "parser.c"
    break;

  case 181: /* parameter_list: parameter_declaration_typed  */
#line 1970 "parser.y"
                                  {
            ((yyval.vdecl)) = sv_alloc(sizeof(struct decl),8,NULL);
    if(sv_void == ((yyval.vdecl))) { YYNOMEM; }
            if(!sv_push(&((yyval.vdecl)),&((yyvsp[0].decl)))) { YYNOMEM; }
        (yyvsp[0].decl) = (struct decl){ 0 }; // hand-over
    }
#line 8548 "parser.c"
    break;

  case 182: /* parameter_list: parameter_list ',' parameter_declaration_typed  */
#line 1976 "parser.y"
                                                     { (void)(yyvsp[-1].ctxt);
        (yyval.vdecl) = (yyvsp[-2].vdecl);     if(!sv_push(&((yyval.vdecl)),&((yyvsp[0].decl)))) { YYNOMEM; }
        (yyvsp[0].decl) = (struct decl){ 0 }; // hand-over
    }
#line 8557 "parser.c"
    break;

  case 183: /* parameter_declaration: declaration_specifiers_checked declarator  */
#line 1983 "parser.y"
                                                {
        (yyval.decl) = (struct decl){ .sv_spec= (yyvsp[-1].vcspc), .pointer= (yyvsp[0].acdd).pointer };
        (yyvsp[-1].vcspc) = NULL; (yyvsp[0].acdd).pointer = NULL; // hand-over
        if(arr_cdd_to_decl(&(yyvsp[0].acdd),&(yyval.decl))) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("invalid C declarator");
        }
        arr_cdd_free(&(yyvsp[0].acdd));
    }
#line 8571 "parser.c"
    break;

  case 184: /* parameter_declaration: declaration_specifiers_checked declarator '=' initializer_flat  */
#line 1992 "parser.y"
                                                                                 { (void)(yyvsp[-1].ctxt);
        (yyval.decl) = (struct decl){ .sv_spec= (yyvsp[-3].vcspc), .pointer= (yyvsp[-2].acdd).pointer, .init= (yyvsp[0].txt) };
        (yyvsp[-3].vcspc) = NULL; (yyvsp[-2].acdd).pointer = NULL; (yyvsp[0].txt) = NULL; // hand-over
        if(arr_cdd_to_decl(&(yyvsp[-2].acdd),&(yyval.decl))) {
            pp->errlloc = (yylsp[-2]);
            ABORT_PARSE_LOC0("invalid C declarator");
        }
        arr_cdd_free(&(yyvsp[-2].acdd));
    }
#line 8585 "parser.c"
    break;

  case 185: /* parameter_declaration: declaration_specifiers_checked  */
#line 2001 "parser.y"
                                     {
        (yyval.decl) = (struct decl){ .sv_spec= (yyvsp[0].vcspc) };
        (yyvsp[0].vcspc) = NULL; // hand-over
    }
#line 8594 "parser.c"
    break;

  case 186: /* parameter_declaration: declaration_specifiers_checked abstract_declarator  */
#line 2005 "parser.y"
                                                         {
        (yyval.decl) = (struct decl){ .sv_spec= (yyvsp[-1].vcspc), .sv_decl= (yyvsp[0].vstr) };
        (yyvsp[-1].vcspc) = NULL; (yyvsp[0].vstr) = NULL; // hand-over
    }
#line 8603 "parser.c"
    break;

  case 187: /* parameter_declaration_typed: parameter_declaration  */
#line 2012 "parser.y"
                            {
        (yyval.decl) = (yyvsp[0].decl);
            for(size_t i = 0; i < sv_len(((yyval.decl)).sv_spec); ++i) {
        const struct c_specifier *spec = sv_at(((yyval.decl)).sv_spec,i);
        if(spec->type == ENUM_C_SPECIFIER(TYPE)) {
            ((yyval.decl)).type = C_TYPESPEC_get(C_SPECIFIER_TYPE_get(*spec));
            break;
        }
    }
    }
#line 8618 "parser.c"
    break;

  case 188: /* opt_identifier_list: %empty  */
#line 2025 "parser.y"
             {
        (yyval.vstr) = NULL;
        
    }
#line 8627 "parser.c"
    break;

  case 189: /* opt_identifier_list: identifier_list  */
#line 2029 "parser.y"
                      { (yyval.vstr) = (yyvsp[0].vstr);  }
#line 8633 "parser.c"
    break;

  case 190: /* identifier_list: "C identifier"  */
#line 2032 "parser.y"
                   {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 8643 "parser.c"
    break;

  case 191: /* identifier_list: identifier_list ',' "C identifier"  */
#line 2037 "parser.y"
                                       { (void)(yyvsp[-1].ctxt);     (yyval.vstr) = (yyvsp[-2].vstr);

    if(!sv_push_ptr(&((yyval.vstr)),(yyvsp[0].txt))) { YYNOMEM; }

 }
#line 8653 "parser.c"
    break;

  case 192: /* type_name: specifier_qualifier_list abstract_declarator  */
#line 2045 "parser.y"
                                                   {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8664 "parser.c"
    break;

  case 194: /* abstract_declarator: pointer direct_abstract_declarator  */
#line 2055 "parser.y"
                                         {     (yyval.vstr) = (yyvsp[-1].vstr);

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[0].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[0].vstr)));

 }
#line 8675 "parser.c"
    break;

  case 197: /* direct_abstract_declarator: '(' abstract_declarator ')'  */
#line 2067 "parser.y"
                                  {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8690 "parser.c"
    break;

  case 198: /* direct_abstract_declarator: '[' ']'  */
#line 2077 "parser.y"
              {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8700 "parser.c"
    break;

  case 199: /* direct_abstract_declarator: '[' type_qualifier_list ']'  */
#line 2082 "parser.y"
                                  {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8715 "parser.c"
    break;

  case 200: /* direct_abstract_declarator: '[' assignment_expression ']'  */
#line 2092 "parser.y"
                                    {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8730 "parser.c"
    break;

  case 201: /* direct_abstract_declarator: '[' type_qualifier_list assignment_expression ']'  */
#line 2102 "parser.y"
                                                        {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8749 "parser.c"
    break;

  case 202: /* direct_abstract_declarator: '[' "static" assignment_expression ']'  */
#line 2116 "parser.y"
                                             {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8765 "parser.c"
    break;

  case 203: /* direct_abstract_declarator: '[' "static" type_qualifier_list assignment_expression ']'  */
#line 2127 "parser.y"
                                                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-4].ctxt),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8784 "parser.c"
    break;

  case 204: /* direct_abstract_declarator: '[' type_qualifier_list "static" assignment_expression ']'  */
#line 2141 "parser.y"
                                                                 {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-4].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-3].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-3].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8805 "parser.c"
    break;

  case 205: /* direct_abstract_declarator: '[' '*' ']'  */
#line 2157 "parser.y"
                  {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8815 "parser.c"
    break;

  case 206: /* direct_abstract_declarator: '(' ')'  */
#line 2162 "parser.y"
              {     if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8825 "parser.c"
    break;

  case 207: /* direct_abstract_declarator: '(' parameter_type_list_flat ')'  */
#line 2167 "parser.y"
                                        {
            if(NULL == ((yyval.vstr) = sv_alloc_t(SV_PTR,4))) { YYNOMEM; }

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

        {
    for(size_t i = 0; i < sv_len(pp->param_types); ++i) {
        const void *_item = sv_at(pp->param_types,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_types));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_names); ++i) {
        const void *_item = sv_at(pp->param_names,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_names));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_inits); ++i) {
        const void *_item = sv_at(pp->param_inits,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_inits));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_pntrs); ++i) {
        const void *_item = sv_at(pp->param_pntrs,i);
        {
    vec_string* vec = *(vec_string**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(pp->param_pntrs));
}
    }
#line 8890 "parser.c"
    break;

  case 208: /* direct_abstract_declarator: direct_abstract_declarator '[' ']'  */
#line 2227 "parser.y"
                                         {     (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 8900 "parser.c"
    break;

  case 209: /* direct_abstract_declarator: direct_abstract_declarator '[' type_qualifier_list ']'  */
#line 2232 "parser.y"
                                                             {
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8916 "parser.c"
    break;

  case 210: /* direct_abstract_declarator: direct_abstract_declarator '[' assignment_expression ']'  */
#line 2243 "parser.y"
                                                               {
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8932 "parser.c"
    break;

  case 211: /* direct_abstract_declarator: direct_abstract_declarator '[' type_qualifier_list assignment_expression ']'  */
#line 2254 "parser.y"
                                                                                   {
            (yyval.vstr) = (yyvsp[-4].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8951 "parser.c"
    break;

  case 212: /* direct_abstract_declarator: direct_abstract_declarator '[' "static" assignment_expression ']'  */
#line 2268 "parser.y"
                                                                        {
            (yyval.vstr) = (yyvsp[-4].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-3].ctxt),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8967 "parser.c"
    break;

  case 213: /* direct_abstract_declarator: direct_abstract_declarator '[' "static" type_qualifier_list assignment_expression ']'  */
#line 2279 "parser.y"
                                                                                            {
            (yyval.vstr) = (yyvsp[-5].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-4].ctxt),(yyvsp[-3].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-2].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-2].vstr)));

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 8986 "parser.c"
    break;

  case 214: /* direct_abstract_declarator: direct_abstract_declarator '[' type_qualifier_list "static" assignment_expression ']'  */
#line 2293 "parser.y"
                                                                                            {
            (yyval.vstr) = (yyvsp[-5].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-4].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-3].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-3].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

    }
#line 9007 "parser.c"
    break;

  case 215: /* direct_abstract_declarator: direct_abstract_declarator '[' '*' ']'  */
#line 2309 "parser.y"
                                             {     (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 9017 "parser.c"
    break;

  case 216: /* direct_abstract_declarator: direct_abstract_declarator '(' ')'  */
#line 2314 "parser.y"
                                         {     (yyval.vstr) = (yyvsp[-2].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-1].ctxt),(yyvsp[0].ctxt))) { YYNOMEM; }

 }
#line 9027 "parser.c"
    break;

  case 217: /* direct_abstract_declarator: direct_abstract_declarator '(' parameter_type_list_flat ')'  */
#line 2319 "parser.y"
                                                                  {
            (yyval.vstr) = (yyvsp[-3].vstr);

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[-2].ctxt))) { YYNOMEM; }

    if(sv_void == sv_cat(&((yyval.vstr)),(yyvsp[-1].vstr))) { YYNOMEM; }
    sv_free(&((yyvsp[-1].vstr)));

    if(!vstr_addstr(&((yyval.vstr)),(yyvsp[0].ctxt))) { YYNOMEM; }

        {
    for(size_t i = 0; i < sv_len(pp->param_types); ++i) {
        const void *_item = sv_at(pp->param_types,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_types));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_names); ++i) {
        const void *_item = sv_at(pp->param_names,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_names));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_inits); ++i) {
        const void *_item = sv_at(pp->param_inits,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(pp->param_inits));
}
        {
    for(size_t i = 0; i < sv_len(pp->param_pntrs); ++i) {
        const void *_item = sv_at(pp->param_pntrs,i);
        {
    vec_string* vec = *(vec_string**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(vec));
}
}
    }
    sv_free(&(pp->param_pntrs));
}
    }
#line 9092 "parser.c"
    break;

  case 218: /* initializer: assignment_expression  */
#line 2384 "parser.y"
                            { (yyval.cini) = C_INITIALIZER_EXPR_set((yyvsp[0].vstr)); }
#line 9098 "parser.c"
    break;

  case 219: /* initializer: compound_initializer  */
#line 2385 "parser.y"
                           { (yyval.cini) = C_INITIALIZER_COMP_set(arr_cdi_box((yyvsp[0].acdi))); }
#line 9104 "parser.c"
    break;

  case 220: /* initializer_flat: initializer  */
#line 2389 "parser.y"
                  {
        srt_string *tmp = cini_to_str(&(yyvsp[0].cini));
            {
    const char *ctmp = ss_to_c(tmp);
            (yyval.txt) = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == ((yyval.txt))) {  }
   
}
    ss_free(&(tmp));
    if(NULL == (yyval.txt)) { YYNOMEM; }

        c_initializer_free(&(yyvsp[0].cini));
    }
#line 9122 "parser.c"
    break;

  case 221: /* compound_initializer_list: compound_initializer  */
#line 2405 "parser.y"
                           {     ((yyval.vacdi)) = sv_alloc(sizeof(struct arr_cdi),2,NULL);
    if(sv_void == ((yyval.vacdi))) { YYNOMEM; }
     if(!sv_push(&((yyval.vacdi)),&((yyvsp[0].acdi)))) { YYNOMEM; }
 }
#line 9131 "parser.c"
    break;

  case 222: /* compound_initializer_list: compound_initializer_list ',' compound_initializer  */
#line 2409 "parser.y"
                                                         {
        (yyval.vacdi) = (yyvsp[-2].vacdi);     if(!sv_push(&((yyval.vacdi)),&((yyvsp[0].acdi)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9141 "parser.c"
    break;

  case 225: /* compound_literal: '(' type_name ')' braced_init_list  */
#line 2422 "parser.y"
                                         { (void)(yyvsp[-3].ctxt); (void)(yyvsp[-1].ctxt);
        (yyval.acdi) = (yyvsp[0].acdi); {
    srt_string *_ss = NULL;
    if(ss_void == (_ss = vstr_join((yyvsp[-2].vstr),' '))) { YYNOMEM; }
    if(NULL == (((yyval.acdi).cast) = rstrdup(ss_to_c(_ss)))) { YYNOMEM; }
    ss_free(&_ss);
    for(size_t i = 0; i < sv_len((yyvsp[-2].vstr)); ++i) free(sv_at_ptr((yyvsp[-2].vstr),i));
    sv_free(&(yyvsp[-2].vstr));
}

    }
#line 9157 "parser.c"
    break;

  case 226: /* braced_init_list: '{' initializer_list opt_comma '}'  */
#line 2436 "parser.y"
                                         { (yyval.acdi) = (yyvsp[-2].acdi); (void)(yyvsp[-3].ctxt); (void)(yyvsp[-1].ctxt); (void)(yyvsp[0].ctxt); }
#line 9163 "parser.c"
    break;

  case 227: /* initializer_list: initializer  */
#line 2440 "parser.y"
                  {
        (yyval.acdi) = arr_cdi_add((arr_cdi){ 0 },
            (struct c_designated_initializer){ .dsg= (struct c_designator){ 0 }, .ini= (yyvsp[0].cini) });
    }
#line 9172 "parser.c"
    break;

  case 228: /* initializer_list: designation initializer  */
#line 2444 "parser.y"
                              {
        (yyval.acdi) = arr_cdi_add((arr_cdi){ 0 },
            (struct c_designated_initializer){ .dsg= (yyvsp[-1].cdsg), .ini= (yyvsp[0].cini) });
    }
#line 9181 "parser.c"
    break;

  case 229: /* initializer_list: initializer_list ',' initializer  */
#line 2448 "parser.y"
                                       { (void)(yyvsp[-1].ctxt);
        (yyval.acdi) = arr_cdi_add((yyvsp[-2].acdi),
            (struct c_designated_initializer){ .dsg= (struct c_designator){ 0 }, .ini= (yyvsp[0].cini) });
    }
#line 9190 "parser.c"
    break;

  case 230: /* initializer_list: initializer_list ',' designation initializer  */
#line 2452 "parser.y"
                                                   { (void)(yyvsp[-2].ctxt);
        (yyval.acdi) = arr_cdi_add((yyvsp[-3].acdi),
            (struct c_designated_initializer){ .dsg= (yyvsp[-1].cdsg), .ini= (yyvsp[0].cini) });
    }
#line 9199 "parser.c"
    break;

  case 231: /* designation: designator '='  */
#line 2459 "parser.y"
                                                  { (yyval.cdsg) = (yyvsp[-1].cdsg); (void)(yyvsp[0].ctxt); }
#line 9205 "parser.c"
    break;

  case 232: /* designator: '[' constant_expression ']'  */
#line 2465 "parser.y"
                                  { (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt); (yyval.cdsg) = C_DESIGNATOR_ARRAY_set((yyvsp[-1].vstr)); }
#line 9211 "parser.c"
    break;

  case 233: /* designator: '.' "C identifier"  */
#line 2466 "parser.y"
                       { (void)(yyvsp[-1].ctxt); (yyval.cdsg) = C_DESIGNATOR_FIELD_set((yyvsp[0].txt)); }
#line 9217 "parser.c"
    break;

  case 234: /* designator: "->" "C identifier"  */
#line 2467 "parser.y"
                                    { (void)(yyvsp[-1].ctxt); (yyval.cdsg) = C_DESIGNATOR_PTR_set((yyvsp[0].txt)); }
#line 9223 "parser.c"
    break;

  case 235: /* paren_c_expression: '(' constant_expression_flat ')'  */
#line 2480 "parser.y"
                                                               {
        (yyval.txt) = (yyvsp[-1].txt);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9232 "parser.c"
    break;

  case 236: /* c_expression_list: constant_expression_flat  */
#line 2486 "parser.y"
                               {     ((yyval.vstr)) = sv_alloc_t(SV_PTR,2);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }
 }
#line 9241 "parser.c"
    break;

  case 237: /* c_expression_list: c_expression_list ',' constant_expression_flat  */
#line 2490 "parser.y"
                                                     {
        (yyval.vstr) = (yyvsp[-2].vstr);     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9251 "parser.c"
    break;

  case 238: /* paren_c_expression_list: '(' c_expression_list ')'  */
#line 2497 "parser.y"
                                                             {
        (yyval.vstr) = (yyvsp[-1].vstr);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9260 "parser.c"
    break;

  case 239: /* opt_pipe: %empty  */
#line 2506 "parser.y"
             { (yyval.ctxt) = NULL; }
#line 9266 "parser.c"
    break;

  case 241: /* mod_assign: '='  */
#line 2511 "parser.y"
          { (yyval.c) = '='; (void)(yyvsp[0].ctxt); }
#line 9272 "parser.c"
    break;

  case 242: /* mod_assign: "?="  */
#line 2512 "parser.y"
           { (yyval.c) = '?'; }
#line 9278 "parser.c"
    break;

  case 243: /* mod_assign: "+="  */
#line 2513 "parser.y"
           { (yyval.c) = '+'; (void)(yyvsp[0].ctxt); }
#line 9284 "parser.c"
    break;

  case 244: /* mod_assign: ":="  */
#line 2514 "parser.y"
           { (yyval.c) = ':'; }
#line 9290 "parser.c"
    break;

  case 245: /* mod_assign: "|="  */
#line 2515 "parser.y"
           { (yyval.c) = '|'; (void)(yyvsp[0].ctxt); }
#line 9296 "parser.c"
    break;

  case 248: /* option: "identifier"  */
#line 2525 "parser.y"
                 {
        (yyval.opt) = CMOD_OPTION_UNVALUED_xset(.name= (yyvsp[0].txt));
    }
#line 9304 "parser.c"
    break;

  case 249: /* option: "identifier" mod_assign integer  */
#line 2528 "parser.y"
                                    {
        (yyval.opt) = CMOD_OPTION_XINT_xset((yyvsp[0].xint),.name= (yyvsp[-2].txt),.assign= (yyvsp[-1].c));
    }
#line 9312 "parser.c"
    break;

  case 250: /* option: "identifier" mod_assign "identifier"  */
#line 2531 "parser.y"
                                       {
        srt_string *ss = ss_dup_c((yyvsp[0].txt)); if(ss_void == ss) YYNOMEM; free((yyvsp[0].txt));
        (yyval.opt) = CMOD_OPTION_STRING_xset(ss,.name= (yyvsp[-2].txt),.assign= (yyvsp[-1].c));
    }
#line 9321 "parser.c"
    break;

  case 251: /* option: "identifier" mod_assign "extended identifier"  */
#line 2535 "parser.y"
                                           {
        srt_string *ss = ss_dup_c((yyvsp[0].txt)); if(ss_void == ss) YYNOMEM; free((yyvsp[0].txt));
        (yyval.opt) = CMOD_OPTION_STRING_xset(ss,.name= (yyvsp[-2].txt),.assign= (yyvsp[-1].c));
    }
#line 9330 "parser.c"
    break;

  case 252: /* option: "identifier" mod_assign "special identifier"  */
#line 2539 "parser.y"
                                               {
        srt_string *ss = ss_dup_c((yyvsp[0].txt)); if(ss_void == ss) YYNOMEM; free((yyvsp[0].txt));
        (yyval.opt) = CMOD_OPTION_STRING_xset(ss,.name= (yyvsp[-2].txt),.assign= (yyvsp[-1].c),.is_special= true);
    }
#line 9339 "parser.c"
    break;

  case 253: /* option: "identifier" mod_assign herestring  */
#line 2543 "parser.y"
                                       {
        srt_string *ss = ss_dup_c((yyvsp[0].txt)); if(ss_void == ss) YYNOMEM; free((yyvsp[0].txt));
        (yyval.opt) = CMOD_OPTION_STRING_xset(ss,.name= (yyvsp[-2].txt),.assign= (yyvsp[-1].c),.is_verbatim= true);
    }
#line 9348 "parser.c"
    break;

  case 255: /* option_comma_list: option_comma_list_nodang ','  */
#line 2551 "parser.y"
                                   { (yyval.vopt) = (yyvsp[-1].vopt); (void)(yyvsp[0].ctxt); }
#line 9354 "parser.c"
    break;

  case 256: /* option_comma_list_nodang: option  */
#line 2555 "parser.y"
             {     ((yyval.vopt)) = sv_alloc(sizeof(struct cmod_option),4,NULL);
    if(sv_void == ((yyval.vopt))) { YYNOMEM; }
     if(!sv_push(&((yyval.vopt)),&((yyvsp[0].opt)))) { YYNOMEM; }
 }
#line 9363 "parser.c"
    break;

  case 257: /* option_comma_list_nodang: option_comma_list_nodang ',' option  */
#line 2559 "parser.y"
                                          {
        (yyval.vopt) = (yyvsp[-2].vopt);     if(!sv_push(&((yyval.vopt)),&((yyvsp[0].opt)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9373 "parser.c"
    break;

  case 258: /* keyword_options: '[' option_comma_list ']'  */
#line 2565 "parser.y"
                                                     {
        (yyval.vopt) = (yyvsp[-1].vopt);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9382 "parser.c"
    break;

  case 259: /* opt_keyword_options: %empty  */
#line 2571 "parser.y"
             {
        (yyval.vopt) = NULL;
        
    }
#line 9391 "parser.c"
    break;

  case 260: /* opt_keyword_options: keyword_options  */
#line 2575 "parser.y"
                      { (yyval.vopt) = (yyvsp[0].vopt);  }
#line 9397 "parser.c"
    break;

  case 261: /* opt_identifier: %empty  */
#line 2580 "parser.y"
             {
        (yyval.txt) = NULL;
        
    }
#line 9406 "parser.c"
    break;

  case 262: /* opt_identifier: "identifier"  */
#line 2584 "parser.y"
                 { (yyval.txt) = (yyvsp[0].txt);  }
#line 9412 "parser.c"
    break;

  case 264: /* cmod_identifier: herestring  */
#line 2589 "parser.y"
                 {
        if(pp->hstr_nl) ERROR0("newline not allowed in cmod_identifier");
        (yyval.txt) = (yyvsp[0].txt);
    }
#line 9421 "parser.c"
    break;

  case 265: /* opt_cmod_identifier: %empty  */
#line 2596 "parser.y"
             {
        (yyval.txt) = NULL;
        
    }
#line 9430 "parser.c"
    break;

  case 266: /* opt_cmod_identifier: cmod_identifier  */
#line 2600 "parser.y"
                      { (yyval.txt) = (yyvsp[0].txt);  }
#line 9436 "parser.c"
    break;

  case 268: /* cmod_identifier_comma_list: cmod_identifier_comma_list_nodang ','  */
#line 2604 "parser.y"
                                            { (yyval.vstr) = (yyvsp[-1].vstr); (void)(yyvsp[0].ctxt); }
#line 9442 "parser.c"
    break;

  case 269: /* cmod_identifier_comma_list_nodang: cmod_identifier  */
#line 2608 "parser.y"
                      {     ((yyval.vstr)) = sv_alloc_t(SV_PTR,8);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }
 }
#line 9451 "parser.c"
    break;

  case 270: /* cmod_identifier_comma_list_nodang: cmod_identifier_comma_list_nodang ',' cmod_identifier  */
#line 2612 "parser.y"
                                                            {
        (yyval.vstr) = (yyvsp[-2].vstr);     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9461 "parser.c"
    break;

  case 271: /* paren_cmod_identifier_comma_list: '(' cmod_identifier_comma_list ')'  */
#line 2618 "parser.y"
                                                                               {
        (yyval.vstr) = (yyvsp[-1].vstr);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9470 "parser.c"
    break;

  case 273: /* special_id_list: special_id_list_nodang ','  */
#line 2626 "parser.y"
                                 { (yyval.vstr) = (yyvsp[-1].vstr); (void)(yyvsp[0].ctxt); }
#line 9476 "parser.c"
    break;

  case 274: /* special_id_list_nodang: "special identifier"  */
#line 2630 "parser.y"
                         {     ((yyval.vstr)) = sv_alloc_t(SV_PTR,8);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }
 }
#line 9485 "parser.c"
    break;

  case 275: /* special_id_list_nodang: special_id_list_nodang ',' "special identifier"  */
#line 2634 "parser.y"
                                                    {
        (yyval.vstr) = (yyvsp[-2].vstr);     if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9495 "parser.c"
    break;

  case 276: /* paren_special_id_list: '(' special_id_list ')'  */
#line 2640 "parser.y"
                                                         {
        (yyval.vstr) = (yyvsp[-1].vstr);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9504 "parser.c"
    break;

  case 279: /* integer_string: integer  */
#line 2653 "parser.y"
              {
        switch((yyvsp[0].xint).type) { // convert integer to string
            case ENUM_XINT(SIGNED):
            {
                char buf[STRIMAX_LEN]; int off;
                (off) = strimax(XINT_SIGNED_get((yyvsp[0].xint)),buf,"");

                const char *tmp = buf + off;
                        (yyval.txt) = (NULL != (tmp)) ? rstrdup(tmp) : NULL;
    if(NULL != (tmp) && NULL == ((yyval.txt))) { YYNOMEM; }
   
            }
                break;
            case ENUM_XINT(UNSIGNED):
            {
                char buf[STRUMAX_LEN]; int off;
                (off) = strumax(XINT_UNSIGNED_get((yyvsp[0].xint)),buf,"");

                const char *tmp = buf + off;
                        (yyval.txt) = (NULL != (tmp)) ? rstrdup(tmp) : NULL;
    if(NULL != (tmp) && NULL == ((yyval.txt))) { YYNOMEM; }
   
            }
                break;
            case ENUM_XINT(INVALID): /* cannot happen */ break;
        }
    }
#line 9536 "parser.c"
    break;

  case 281: /* integer_comma_list: integer_comma_list_nodang ','  */
#line 2684 "parser.y"
                                    { (yyval.vxint) = (yyvsp[-1].vxint); (void)(yyvsp[0].ctxt); }
#line 9542 "parser.c"
    break;

  case 282: /* integer_comma_list_nodang: integer  */
#line 2688 "parser.y"
              {     ((yyval.vxint)) = sv_alloc(sizeof(struct xint),2,NULL);
    if(sv_void == ((yyval.vxint))) { YYNOMEM; }
     if(!sv_push(&((yyval.vxint)),&((yyvsp[0].xint)))) { YYNOMEM; }
 }
#line 9551 "parser.c"
    break;

  case 283: /* integer_comma_list_nodang: integer_comma_list_nodang ',' integer  */
#line 2692 "parser.y"
                                            {
        (yyval.vxint) = (yyvsp[-2].vxint);     if(!sv_push(&((yyval.vxint)),&((yyvsp[0].xint)))) { YYNOMEM; }

        (void)(yyvsp[-1].ctxt);
    }
#line 9561 "parser.c"
    break;

  case 284: /* paren_integer_comma_list: '(' integer_comma_list ')'  */
#line 2698 "parser.y"
                                                               {
        (yyval.vxint) = (yyvsp[-1].vxint);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9570 "parser.c"
    break;

  case 285: /* paren_unsigned_int: '(' "unsigned integer" ')'  */
#line 2703 "parser.y"
                                                   {
        (yyval.xint) = (yyvsp[-1].xint);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 9579 "parser.c"
    break;

  case 286: /* opt_UNSIGNED_INT: %empty  */
#line 2709 "parser.y"
             {
        (yyval.xint) = (struct xint){ 0 };
        
    }
#line 9588 "parser.c"
    break;

  case 287: /* opt_UNSIGNED_INT: "unsigned integer"  */
#line 2713 "parser.y"
                   { (yyval.xint) = (yyvsp[0].xint);  }
#line 9594 "parser.c"
    break;

  case 288: /* mod_herestring: "%{" "%}"  */
#line 2719 "parser.y"
                {
        ss_cat_cn1(&pp->hstr_buf,'\0'); // NUL-terminate
        if(!is_valid_utf8((const unsigned char*)ss_get_buffer_r(pp->hstr_buf),ss_len(pp->hstr_buf))) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("invalid UTF-8 input");
        }
                if(ss_void == ((yyval.str) = ss_dup(pp->hstr_buf))) { YYNOMEM; }
    (yyval.str) = ss_shrink(&((yyval.str)));
    ss_clear(pp->hstr_buf);
    }
#line 9609 "parser.c"
    break;

  case 289: /* opt_mod_herestring: "newline"  */
#line 2732 "parser.y"
           { (yyval.str) = NULL; }
#line 9615 "parser.c"
    break;

  case 291: /* opt_herestring: %empty  */
#line 2737 "parser.y"
             {
        (yyval.txt) = NULL;
        
    }
#line 9624 "parser.c"
    break;

  case 292: /* opt_herestring: herestring  */
#line 2741 "parser.y"
                 { (yyval.txt) = (yyvsp[0].txt);  }
#line 9630 "parser.c"
    break;

  case 293: /* herestring: "%<<" ">>%"  */
#line 2744 "parser.y"
                  {
        ss_cat_cn1(&pp->hstr_buf,'\0'); // NUL-terminate
        if(!is_valid_utf8((const unsigned char*)ss_get_buffer_r(pp->hstr_buf),ss_len(pp->hstr_buf))) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("invalid UTF-8 input");
        }
            {
    const char *ctmp = ss_to_c(pp->hstr_buf);
            (yyval.txt) = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == ((yyval.txt))) {  }
   
}
    ss_clear(pp->hstr_buf);
    if(NULL == (yyval.txt)) { YYNOMEM; }
    }
#line 9650 "parser.c"
    break;

  case 294: /* herestring: "%nul"  */
#line 2759 "parser.y"
             { // empty string
        ss_cat_cn1(&pp->hstr_buf,'\0'); // NUL-terminate
            {
    const char *ctmp = ss_to_c(pp->hstr_buf);
            (yyval.txt) = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == ((yyval.txt))) {  }
   
}
    ss_clear(pp->hstr_buf);
    if(NULL == (yyval.txt)) { YYNOMEM; }
    }
#line 9666 "parser.c"
    break;

  case 295: /* table_like: table  */
#line 2774 "parser.y"
            {
        if(sv_len((yyvsp[0].tab).svv_rows) > 0 && NULL == (yyvsp[0].tab).sv_colnarg)
            {
    int ret = table_firstrow_colnames(&((yyvsp[0].tab).svv_rows),&((yyvsp[0].tab).sv_colnarg),&((yyvsp[0].tab).nnondef));
    const char *errstr = NULL;
    if(ret)     ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[0]);
    switch(ret) {
        case 1: errstr = "out of memory" " in"; break;
        case 4: errstr = "expected an extended identifier as column name in"; break;
        default: errstr = "unspecific error in"; break;
    }
    if(ret) ABORT_PARSE_LOC("%s table `%s`",errstr,"<anonymous>");
}
        (yyval.tlike) = (struct table_like){ .tab= (yyvsp[0].tab) };
    }
#line 9686 "parser.c"
    break;

  case 297: /* table_like: lambda_table  */
#line 2790 "parser.y"
                   { (yyval.tlike) = (struct table_like){ .tab= (yyvsp[0].tab), .is_lambda=true }; }
#line 9692 "parser.c"
    break;

  case 298: /* table_like: cmod_identifier_comma_list  */
#line 2791 "parser.y"
                                 { (yyval.tlike) = (struct table_like){ .lst= (yyvsp[0].vstr) }; }
#line 9698 "parser.c"
    break;

  case 299: /* table: opt_table_columns "%TSV|JSON{" "%}"  */
#line 2795 "parser.y"
                                          { // TSV or JSON table
        struct table tab = { 0 };
        size_t tab_ncol = 0;

        enum table_fmt format = ('j' == (yyvsp[-1].c)) ? TABLE_FMT_JSON : TABLE_FMT_TSV;
        const char *table_name = (TABLE_FMT_JSON == format) ? "<json table>" : "<tsv table>";
        if(NULL == (yyvsp[-2].vnarg)) { // implicit columns, all are non-default
            if(TABLE_FMT_TSV == format) {
                { // parse TSV table
    const char *tsv = ss_get_buffer_r(pp->hstr_buf);
    enum tsv_parse ret = table_tsv_parse(tsv,ss_len(pp->hstr_buf),&(tab_ncol),&(tab.svv_rows));
    switch(ret) {
        case TSV_PARSE_OK: break;
        case TSV_PARSE_OOM: YYNOMEM; break;
        case TSV_PARSE_BADROW:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("incorrect row size in TSV table `%s`",table_name);
            break;
        case TSV_PARSE_NDEFAULT: /* not happening here */ break;
                    case TSV_PARSE_INIT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("failed initializing scanner"" in TSV table `%s`"": %s (%d)",table_name,strerror(errno),errno);
            break;
           }
}
                if(sv_len(tab.svv_rows) > 0) { // get length from first row
                    const vec_string *firstrow = sv_at_ptr(tab.svv_rows,0);
                    tab.nnondef = sv_len(firstrow);
                }
            } else {
                { // parse JSON table
    size_t ierr;
    const char *jstr = ss_get_buffer_r(pp->hstr_buf);
    enum json_parse ret = table_json_parse(jstr,ss_len(pp->hstr_buf),&(tab_ncol),&(tab.svv_rows),&(tab.sv_colnarg),&ierr);
    switch(ret) {
        case JSON_PARSE_OK: break;
        case JSON_PARSE_OOM: YYNOMEM; break;
        case JSON_PARSE_BADROW:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("incorrect row size in JSON table `%s`",table_name);
            break;
        case JSON_PARSE_NDEFAULT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("expected %zu (got %zu) non-default columns in JSON table `%s`",
                     tab.nnondef,ierr,table_name);
            break;
                    case JSON_PARSE_INVALID:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("invalid JSON token"" in JSON table `%s`"" at position %zu",table_name,ierr);
            break;
           case JSON_PARSE_PARTIAL:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("partial JSON input"" in JSON table `%s`"" at position %zu",table_name,ierr);
            break;
           case JSON_PARSE_DUPLICATE:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("duplicate column name"" in JSON table `%s`"" at position %zu",table_name,ierr);
            break;
           case JSON_PARSE_BADCOLUMN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("unknown or mismatched column name"" in JSON table `%s`"" at position %zu",table_name,ierr);
            break;
               case JSON_PARSE_FORMAT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("bad format"" in JSON table `%s`",table_name)
            break;
           case JSON_PARSE_UNKNOWN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("unknown JSON error"" in JSON table `%s`",table_name)
            break;
           case JSON_PARSE_MULTI:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("bad multi-table format"" in JSON table `%s`",table_name)
            break;
       }
}
                if(NULL != tab.sv_colnarg) { // arrobj JSON format
                    tab.nnondef = sv_len(tab.sv_colnarg);
                } else if(sv_len(tab.svv_rows) > 0) { // get length from first row
                    const vec_string *firstrow = sv_at_ptr(tab.svv_rows,0);
                    tab.nnondef = sv_len(firstrow);
                }
            }
        } else { // explicit columns, maybe with default values
            {
    size_t ierr = 0;
    union table_body tbody = { .verbatim = pp->hstr_buf };
    int ret = table_build(tbody,(yyvsp[-2].vnarg),format,&(tab),&ierr);
    switch(ret) {
        case TABLE_PARSE_OK: break;
        case TABLE_PARSE_OOM: YYNOMEM; break;
        case TABLE_PARSE_BADROW:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("incorrect row size in table body");
            break;
        case TABLE_PARSE_NDEFAULT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-2]);
            ABORT_PARSE_LOC("expected %zu (got %zu) non-default columns in table body",tab.nnondef,ierr);
            break;
        default: switch(format) {
            case TABLE_FMT_TSV:
                switch(ret) {
                                case TSV_PARSE_INIT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("failed initializing scanner"" in TSV table body"": %s (%d)",strerror(errno),errno);
            break;
                           default:
                            assert(0 && "C% internal error"  "");
                        break;
                }
                break;
            case TABLE_FMT_JSON:
                switch(ret) {
                                case JSON_PARSE_INVALID:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("invalid JSON token"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_PARTIAL:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("partial JSON input"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_DUPLICATE:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("duplicate column name"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_BADCOLUMN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("unknown or mismatched column name"" in JSON table body"" at position %zu",ierr);
            break;
               case JSON_PARSE_FORMAT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("bad format"" in JSON table body")
            break;
           case JSON_PARSE_UNKNOWN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("unknown JSON error"" in JSON table body")
            break;
           case JSON_PARSE_MULTI:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("bad multi-table format"" in JSON table body")
            break;
                       default:
                            assert(0 && "C% internal error"  "");
                        break;
                }
                break;
            case TABLE_FMT_LAMBDA:
                /* no lambda-specific error codes */
                break;
        }
    }
}
        }
        if(NULL == tab.shm_colnam) {
        (tab.shm_colnam) = shm_alloc(SHM_SU,sv_len(tab.sv_colnarg));
    if(NULL == (tab.shm_colnam)) { YYNOMEM; }
    {
const size_t _len = sv_len(tab . sv_colnarg);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _13 = _idx;
const size_t _idx = 0; (void)_idx;
        {
    const srt_string *_ss = ss_crefs(((const struct namarg*)sv_at(tab . sv_colnarg,_13))->name);
        if(!shm_insert_su(&(tab.shm_colnam),_ss,_13)) { YYNOMEM; }
}
   }
}
}
        (yyval.tab) = tab;
        ss_clear(pp->hstr_buf);
    }
#line 9874 "parser.c"
    break;

  case 300: /* table_selection: cmod_identifier cmod_arguments_unique  */
#line 2969 "parser.y"
                                            {
        struct table_like tlike = { .sel= (yyvsp[0].vnarg) };

        struct table *tab = NULL;
        {
    setix _found = { 0 };
    {
    const srt_string *_ss = ss_crefs((yyvsp[-1].txt));
        if(NULL == pp->shm_tab) {  }
    _found.set = shm_atp_su(pp->shm_tab,_ss,&(_found).ix);
}

    if(_found.set) tab = (void*)sv_at(pp->sv_tab,_found.ix); 
}

        if(NULL == tab) { // table not found
            tlike.tab = (struct table){ .name=(yyvsp[-1].txt) }; (yyvsp[-1].txt) = NULL; // hand-over
            goto table_selection_done;
        }
        tlike.tab = *tab;

        // resolve column indices
        {
const size_t _len = sv_len(tlike . sel);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _0 = _idx;
const size_t _idx = 0; (void)_idx;
            if(((struct namarg*)sv_at(tlike . sel,_0))->type != ENUM_NAMARG(SPECIAL)) {
                pp->errlloc = (yylsp[0]);
                ABORT_PARSE_LOC("expected special identifier at position %zu, did you forget the #?",
                                _0);
            }
            char *colnam = CMOD_ARG_SPECIAL_get(*((struct namarg*)sv_at(tlike . sel,_0)));
            setix found = { 0 };
            {
    const srt_string *_ss = ss_crefs(colnam);
        if(NULL == tlike.tab.shm_colnam) {  }
    found.set = shm_atp_su(tlike.tab.shm_colnam,_ss,&(found).ix);
}

            if(!found.set) {
                {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = colnam;
    mutpp->errlloc = (yylsp[0]);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                ABORT_PARSE_LOC("requested column `%s` not found in table `%s`",
                                colnam,tlike.tab.name);
            }
            free(colnam); // clear
            *((struct namarg*)sv_at(tlike . sel,_0)) = CMOD_ARG_ARGLINK_xset(found.ix,.name= ((struct namarg*)sv_at(tlike . sel,_0))->name); // change type
       }
}

table_selection_done:
        free((yyvsp[-1].txt));
        (yyval.tlike) = tlike;
    }
#line 9942 "parser.c"
    break;

  case 301: /* heretab_verbatim_char_list: "character"  */
#line 3036 "parser.y"
           {
            ((yyval.str)) = ss_alloc(8);
    if(ss_void == ((yyval.str))) { YYNOMEM; }
        (yyval.str) = ss_cat_cn1(&(yyval.str),(yyvsp[0].c));
    }
#line 9952 "parser.c"
    break;

  case 302: /* heretab_verbatim_char_list: heretab_verbatim_char_list "character"  */
#line 3041 "parser.y"
                                      { (yyval.str) = ss_cat_cn1(&(yyvsp[-1].str),(yyvsp[0].c)); }
#line 9958 "parser.c"
    break;

  case 303: /* heretab_verbatim: %empty  */
#line 3045 "parser.y"
             { (yyval.str) = NULL; }
#line 9964 "parser.c"
    break;

  case 304: /* heretab_verbatim: heretab_verbatim_char_list  */
#line 3046 "parser.y"
                                 {
        (yyval.str) = ss_cat_cn1(&(yyvsp[0].str),'\0'); // NUL-terminate
        if(!is_valid_utf8((const unsigned char*)ss_get_buffer_r((yyval.str)), ss_len((yyval.str))))
            ABORT_PARSE0("invalid UTF-8 input");
    }
#line 9974 "parser.c"
    break;

  case 305: /* lambda_item: heretab_verbatim  */
#line 3054 "parser.y"
                       {
        char *str = NULL;
            {
    const char *ctmp = ss_to_c((yyvsp[0].str));
            str = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == (str)) {  }
   
}
    ss_free(&((yyvsp[0].str)));
    if(NULL == str) { YYNOMEM; }

            ((yyval.vstr)) = sv_alloc_t(SV_PTR,1);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
            if(!sv_push(&((yyval.vstr)),&(str))) { YYNOMEM; }
    }
#line 9994 "parser.c"
    break;

  case 306: /* lambda_item: herestring  */
#line 3069 "parser.y"
                 {
            ((yyval.vstr)) = sv_alloc_t(SV_PTR,1);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
            if(!sv_push(&((yyval.vstr)),&((yyvsp[0].txt)))) { YYNOMEM; }
    }
#line 10004 "parser.c"
    break;

  case 307: /* lambda_item: "integer range"  */
#line 3074 "parser.y"
            {
            ((yyval.vstr)) = sv_alloc_t(SV_PTR,16);
    if(sv_void == ((yyval.vstr))) { YYNOMEM; }
        {
    bool reverse = ((yyvsp[0].range).lo > (yyvsp[0].range).hi);

    if((!reverse && (yyvsp[0].range).inc < 0) || (reverse && (yyvsp[0].range).inc >= 0))
        ABORT_PARSE("incompatible increment %"PRIiMAX" for integer range %"PRIiMAX"..%"PRIiMAX,
                    (yyvsp[0].range).inc,(yyvsp[0].range).lo,(yyvsp[0].range).hi);

    intmax_t lo  = reverse ? (yyvsp[0].range).hi               : (yyvsp[0].range).lo;
    intmax_t hi  = reverse ? (yyvsp[0].range).lo               : (yyvsp[0].range).hi;
    intmax_t add = reverse ? (yyvsp[0].range).lo + (yyvsp[0].range).hi : 0;
    intmax_t sgn = reverse ? -1                        : 1;
    intmax_t inc = ((yyvsp[0].range).inc == 0) ? 1 : (reverse ? -(yyvsp[0].range).inc : (yyvsp[0].range).inc);

    size_t niter = (hi - lo) / inc + (((hi - lo) % inc > 0) ? 1 : 0);
    if(niter > pp->iter_limit)
        ABORT_PARSE("integer range with %zu elements exceeds -L/--iter-limit=%zu",
                    niter,pp->iter_limit);

    char buf[STRUMAX_LEN];
    const char *cnum = NULL;
    srt_string *num = ss_alloc_into_ext_buf(buf,sizeof(buf));
    for(intmax_t i = lo; i < hi; i += inc) {
        intmax_t val = add + sgn * i;
        if(val >= 0) cnum = uint_tostr(val,buf);
        else {
            ss_clear(num);
            ss_cat_int(&num,val);
            cnum = ss_to_c(num);
        }
        char *        item = (NULL != (cnum)) ? rstrdup(cnum) : NULL;
    if(NULL != (cnum) && NULL == (item)) { YYNOMEM; }
   

            if(!sv_push(&((yyval.vstr)),&(item))) { YYNOMEM; }
    }
}
    }
#line 10049 "parser.c"
    break;

  case 308: /* lambda_item_comma_list: lambda_item  */
#line 3117 "parser.y"
                  { (yyval.vstr) = NULL; sv_cat(&((yyval.vstr)),(yyvsp[0].vstr)); sv_free(&((yyvsp[0].vstr))); }
#line 10055 "parser.c"
    break;

  case 309: /* lambda_item_comma_list: lambda_item_comma_list ',' lambda_item  */
#line 3118 "parser.y"
                                             {
        (yyval.vstr) = (yyvsp[-2].vstr); sv_cat(&((yyval.vstr)),(yyvsp[0].vstr)); sv_free(&((yyvsp[0].vstr)));
        (void)(yyvsp[-1].ctxt);
    }
#line 10064 "parser.c"
    break;

  case 310: /* lambda_table_column: '{' lambda_item_comma_list '}'  */
#line 3124 "parser.y"
                                                              {
        (yyval.vstr) = (yyvsp[-1].vstr);
        (void) (yyvsp[-2].ctxt); (void) (yyvsp[0].ctxt);
    }
#line 10073 "parser.c"
    break;

  case 311: /* lambda_table_column_list: lambda_table_column  */
#line 3130 "parser.y"
                          {     ((yyval.vvstr)) = sv_alloc_t(SV_PTR,2);
    if(sv_void == ((yyval.vvstr))) { YYNOMEM; }
     if(!sv_push(&((yyval.vvstr)),&((yyvsp[0].vstr)))) { YYNOMEM; }
 }
#line 10082 "parser.c"
    break;

  case 312: /* lambda_table_column_list: lambda_table_column_list lambda_table_column  */
#line 3134 "parser.y"
                                                   {
        (yyval.vvstr) = (yyvsp[-1].vvstr);     if(!sv_push(&((yyval.vvstr)),&((yyvsp[0].vstr)))) { YYNOMEM; }

    }
#line 10091 "parser.c"
    break;

  case 313: /* lambda_table: lambda_table_column_list table_columns  */
#line 3141 "parser.y"
                                             {
        struct table tab = { 0 };

        const vec_string *firstcol = sv_at_ptr((yyvsp[-1].vvstr),0);
        size_t nrow = sv_len(firstcol);
        for(size_t i = 1; i < sv_len((yyvsp[-1].vvstr)); ++i) { // compare with subsequent columns
            const vec_string *col = sv_at_ptr((yyvsp[-1].vvstr),i);
            if(sv_len(col) != nrow) {
                pp->errlloc = (yylsp[-1]);
                ABORT_PARSE_LOC0("wrong column size in lambda table");
            }
        }
        {
    size_t ierr = 0;
    union table_body tbody = { .lambda = &(yyvsp[-1].vvstr) };
    int ret = table_build(tbody,(yyvsp[0].vnarg),TABLE_FMT_LAMBDA,&(tab),&ierr);
    switch(ret) {
        case TABLE_PARSE_OK: break;
        case TABLE_PARSE_OOM: YYNOMEM; break;
        case TABLE_PARSE_BADROW:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("incorrect row size in table body");
            break;
        case TABLE_PARSE_NDEFAULT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC("expected %zu (got %zu) non-default columns in table body",tab.nnondef,ierr);
            break;
        default: switch(TABLE_FMT_LAMBDA) {
            case TABLE_FMT_TSV:
                switch(ret) {
                                case TSV_PARSE_INIT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("failed initializing scanner"" in TSV table body"": %s (%d)",strerror(errno),errno);
            break;
                           default:
                            assert(0 && "C% internal error"  "");
                        break;
                }
                break;
            case TABLE_FMT_JSON:
                switch(ret) {
                                case JSON_PARSE_INVALID:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("invalid JSON token"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_PARTIAL:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("partial JSON input"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_DUPLICATE:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("duplicate column name"" in JSON table body"" at position %zu",ierr);
            break;
           case JSON_PARSE_BADCOLUMN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("unknown or mismatched column name"" in JSON table body"" at position %zu",ierr);
            break;
               case JSON_PARSE_FORMAT:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("bad format"" in JSON table body")
            break;
           case JSON_PARSE_UNKNOWN:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("unknown JSON error"" in JSON table body")
            break;
           case JSON_PARSE_MULTI:
                ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC0("bad multi-table format"" in JSON table body")
            break;
                       default:
                            assert(0 && "C% internal error"  "");
                        break;
                }
                break;
            case TABLE_FMT_LAMBDA:
                /* no lambda-specific error codes */
                break;
        }
    }
}
        if(NULL == tab.shm_colnam) {
        (tab.shm_colnam) = shm_alloc(SHM_SU,sv_len(tab.sv_colnarg));
    if(NULL == (tab.shm_colnam)) { YYNOMEM; }
    {
const size_t _len = sv_len(tab . sv_colnarg);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _14 = _idx;
const size_t _idx = 0; (void)_idx;
        {
    const srt_string *_ss = ss_crefs(((const struct namarg*)sv_at(tab . sv_colnarg,_14))->name);
        if(!shm_insert_su(&(tab.shm_colnam),_ss,_14)) { YYNOMEM; }
}
   }
}
}
        (yyval.tab) = tab;
    }
#line 10193 "parser.c"
    break;

  case 314: /* recall_like: cmod_identifier snippet_actual_arguments_inout_idcap  */
#line 3242 "parser.y"
                                                           {
        struct recall_like rlike = { .name= (yyvsp[-1].txt) };

        {
    setix _found = { 0 };
    {
    const srt_string *_ss = ss_crefs(rlike.name);
        if(NULL == pp->shm_snip) {  }
    _found.set = shm_atp_su(pp->shm_snip,_ss,&(_found).ix);
}

    if(_found.set) rlike.snip = (void*)sv_at(pp->sv_snip,_found.ix); 
}
        if(NULL == rlike.snip) { // snippet not found
            rlike.arg_lists = (yyvsp[0].vvnarg); (yyvsp[0].vvnarg) = NULL; // hand-over
            goto recall_like_done;
        }

        if(SNIPPET_NARGOUT(rlike.snip) == 0 && pp->nout > 0) { // idcap would fail here
            pp->errlloc = (yylsp[-1]);
            ABORT_PARSE_LOC("snippet `%s` takes no output arguments",rlike.snip->name);
        }

        size_t nlist = sv_len((yyvsp[0].vvnarg)) - pp->nout;
            (rlike.arg_lists) = sv_alloc_t(SV_PTR,nlist);
    if(sv_void == (rlike.arg_lists)) { YYNOMEM; }
        for(size_t i = 0; i < nlist; ++i) { // check and collect arguments
            vec_namarg *argvals = NULL;
            if(SNIPPET_HAS_FORMAL_ARGS(rlike.snip)) {
                    (argvals) = sv_alloc(sizeof(struct namarg),rlike.snip->nargs,NULL);
    if(sv_void == (argvals)) { YYNOMEM; }

                for(size_t j = 0; j < rlike.snip->nargs; ++j) {
                    struct namarg empty = { 0 };
                        if(!sv_push(&(argvals),&(empty))) { YYNOMEM; }

                }

                vec_namarg *sv_outargs = (pp->nout > 0) ? sv_at_ptr((yyvsp[0].vvnarg),i) : NULL;
                {
    const char *err = NULL;
    switch(collect_actual_args(sv_outargs,argvals,rlike.snip,0,SNIPPET_NARGOUT(rlike.snip),&err)) {
        case 2:
            ABORT_PARSE("snippet `%s` takes up to %zu positional output arguments",
                        rlike.snip->name,SNIPPET_NARGOUT(rlike.snip));
            break;
        case 3:
            ABORT_PARSE("unknown named output argument `%s` passed to snippet `%s`",
                        err,rlike.snip->name);
            break;
    }
}


                vec_namarg *sv_inpargs = sv_at_ptr((yyvsp[0].vvnarg),pp->nout + i);
                {
    const char *err = NULL;
    switch(collect_actual_args(sv_inpargs,argvals,rlike.snip,SNIPPET_NARGOUT(rlike.snip),SNIPPET_NARGIN(rlike.snip),&err)) {
        case 2:
            ABORT_PARSE("snippet `%s` takes up to %zu positional input arguments",
                        rlike.snip->name,SNIPPET_NARGIN(rlike.snip));
            break;
        case 3:
            ABORT_PARSE("unknown named input argument `%s` passed to snippet `%s`",
                        err,rlike.snip->name);
            break;
    }
}

            } else { // positional references only
                const vec_namarg *inpargs = sv_at_ptr((yyvsp[0].vvnarg),i);
                size_t nval = sv_len(inpargs);
                { // count empty positional references
                    size_t nempty = 0;
                    for(size_t i = 0; i < sms_len(rlike.snip->sms_dllarg); ++i)
                        if(sms_it_u(rlike.snip->sms_dllarg,i) >= nval)
                            ++nempty;
                    if(nempty > 0)
                        VERBOSE("empty positional references (%zu) in snippet `%s`",
                                nempty,rlike.snip->name);
                }

                /* collect argument values */
                    (argvals) = sv_alloc(sizeof(struct namarg),nval,NULL);
    if(sv_void == (argvals)) { YYNOMEM; }

                {
const size_t _len = sv_len(inpargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _1 = _idx;
const size_t _idx = 0; (void)_idx;
                    if(NULL != ((struct namarg*)sv_at(inpargs,_1))->name) {
                        pp->errlloc = (yylsp[-1]);
                        ABORT_PARSE_LOC("unexpected named argument for snippet `%s` with no formal arguments",
                                        rlike.snip->name);
                    }
                    struct namarg argval = { 0 };
                    get_actual_argval(((struct namarg*)sv_at(inpargs,_1)),&argval);
                        if(!sv_push(&(argvals),&(argval))) { YYNOMEM; }

                    *((struct namarg*)sv_at(inpargs,_1)) = (struct namarg){ 0 }; // hand-over
               }
}
            }
                if(!sv_push(&(rlike.arg_lists),&(argvals))) { YYNOMEM; }
        }

recall_like_done:
        {
    for(size_t i = 0; i < sv_len((yyvsp[0].vvnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vvnarg),i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&((yyvsp[0].vvnarg)));
}

        (yyval.rlike) = rlike;
    }
#line 10324 "parser.c"
    break;

  case 315: /* recall_end: "newline"  */
#line 3371 "parser.y"
                { (yyval.c) = '\n'; }
#line 10330 "parser.c"
    break;

  case 316: /* recall_end: "|%"  */
#line 3372 "parser.y"
                     { (yyval.c) = 'e'; }
#line 10336 "parser.c"
    break;

  case 317: /* snippet_like: snippet_formal_arguments_inout snippet_body  */
#line 3377 "parser.y"
                                                  {
        srt_string *snippet_body = (yyvsp[0].str);
        struct snippet snip = { 0 };
        if(pp->subparse) { // pass-through
            snip.body = snippet_body;
            snip.sv_argl = pp->sv_dllarg;
        } else { // define snippet
                    snip = (struct snippet){
        .name="<anonymous>", .body=snippet_body,
        .sv_forl=(yyvsp[-1].vnarg), .sv_argl=pp->sv_dllarg,
        .redef=false, .nout=pp->nout
    };
    { // check snippet
        size_t ierr = 0;
        const char *err = NULL;
        int ret = snip_def_check(&snip,false,&ierr,&err); // is_lambda = false
        if(ret >= 2)     ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[0]);
        switch(ret) {
            case 0:
                if(ierr == 0) break;
                VERBOSE("unused arguments (%zu) in snippet `%s`",ierr,snip.name);
                break;
            case 1: YYNOMEM;; break;
            case 2:
                ABORT_PARSE_LOC("formal argument at position %zu has no name in snippet `%s`",ierr,"<anonymous>");
                break;
            case 3:
                ABORT_PARSE_LOC("invalid special argument %s in snippet `%s`",err,"<anonymous>");
                break;
            case 4:
                ABORT_PARSE_LOC("reference to undefined formal argument `%s` in snippet `%s`",err,"<anonymous>");
                break;
            case 5:
                ABORT_PARSE_LOC("positional reference %zu out of bounds in snippet `%s`",ierr,"<anonymous>");
                break;
            case 6:
                ABORT_PARSE_LOC("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
            case 7:
                ABORT_PARSE_LOC("invalid arglink to self in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
            case 8:
                ABORT_PARSE_LOC("arglink is too deep in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
        }
    }

            snip.name = NULL; // unborrow "<anonymous>"
        }
        (yyval.snip) = snip;

        (yyvsp[0].str) = NULL; // hand-over
            pp->sv_dllarg = NULL; // hand-over
    }
#line 10395 "parser.c"
    break;

  case 318: /* snippet_like: cmod_identifier snippet_formal_arguments_inout "newline"  */
#line 3431 "parser.y"
                                                          {
        struct snippet *parent = NULL;
        {
    setix _found = { 0 };
    {
    const srt_string *_ss = ss_crefs((yyvsp[-2].txt));
        if(NULL == pp->shm_snip) {  }
    _found.set = shm_atp_su(pp->shm_snip,_ss,&(_found).ix);
}

    if(_found.set) parent = (void*)sv_at(pp->sv_snip,_found.ix); 
}

        if(NULL == parent) {
            pp->errlloc = (yylsp[-2]);
            ABORT_PARSE_LOC("undefined parent snippet `%s`",(yyvsp[-2].txt));
        }

        /* duplicate stuff from parent snippet */
        srt_string *snippet_body = NULL; // duplicate body
            if(ss_void == (snippet_body = ss_dup(parent->body))) { YYNOMEM; }


        vec_dllarg *sv_argl = NULL; // duplicate argument reference list
        {
    sv_argl = sv_dup(parent->sv_argl); // duplicate buffer
    if(sv_void == sv_argl && NULL != parent->sv_argl) { YYNOMEM; }
    for(size_t i = 0; i < sv_len(sv_argl); ++i) {
        struct dllarg* _item = sv_elem_addr(sv_argl,i);
        const struct dllarg _item_old = *_item;
        if(dllarg_dup(&(_item_old),&(*_item))) { YYNOMEM; }    }
}


        size_t nout = 0;
        vec_namarg *sv_forl_ord = NULL;
        const vec_namarg *formal_argument_list = (yyvsp[-1].vnarg);
        /* handle formal argument list */
        if(sv_len(parent->sv_forl) > 0) { // has formal arguments
            assert(sv_len(parent->sv_forl) == parent->nargs);

            vec_namarg *sv_forl = NULL; // duplicate formal argument list
            {
    sv_forl = sv_dup(parent->sv_forl); // duplicate buffer
    if(sv_void == sv_forl && NULL != parent->sv_forl) { YYNOMEM; }
    for(size_t i = 0; i < sv_len(sv_forl); ++i) {
        struct namarg* _item = sv_elem_addr(sv_forl,i);
        const struct namarg _item_old = *_item;
        if(namarg_dup(&(_item_old),&(*_item))) { YYNOMEM; }    }
}


            vec_namarg *head_in = NULL;
                (head_in) = sv_alloc(sizeof(struct namarg),2,NULL);
    if(sv_void == (head_in)) { YYNOMEM; }

            vec_namarg *head_out = NULL;
                (head_out) = sv_alloc(sizeof(struct namarg),2,NULL);
    if(sv_void == (head_out)) { YYNOMEM; }

            vec_namarg *tail_in = NULL;
                (tail_in) = sv_alloc(sizeof(struct namarg),2,NULL);
    if(sv_void == (tail_in)) { YYNOMEM; }

            vec_namarg *tail_out = NULL;
                (tail_out) = sv_alloc(sizeof(struct namarg),2,NULL);
    if(sv_void == (tail_out)) { YYNOMEM; }


            /* check and update formal arguments */
            {
const size_t _len = sv_len(formal_argument_list);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _2 = _idx;
const size_t _idx = 0; (void)_idx;
                bool is_outarg = (_2 < pp->nout);
                setix found = { 0 };
                {
    const srt_string *_ss = ss_crefs(((struct namarg*)sv_at(formal_argument_list,_2))->name);
        if(NULL == parent->shm_forl) {  }
    found.set = shm_atp_su(parent->shm_forl,_ss,&(found).ix);
}

                if(!found.set) { // is new
                    if(((struct namarg*)sv_at(formal_argument_list,_2))->type == ENUM_NAMARG(NOVALUE)) { // no default, add to head
                        if(is_outarg) {     if(!sv_push(&(head_out),&(*((struct namarg*)sv_at(formal_argument_list,_2))))) { YYNOMEM; }
 }
                        else {     if(!sv_push(&(head_in),&(*((struct namarg*)sv_at(formal_argument_list,_2))))) { YYNOMEM; }
 }
                    } else { // default, add to tail
                        if(is_outarg) {     if(!sv_push(&(tail_out),&(*((struct namarg*)sv_at(formal_argument_list,_2))))) { YYNOMEM; }
 }
                        else {     if(!sv_push(&(tail_in),&(*((struct namarg*)sv_at(formal_argument_list,_2))))) { YYNOMEM; }
 }
                    }
                    *((struct namarg*)sv_at(formal_argument_list,_2)) = (struct namarg){ 0 }; // hand-over
                } else { // exists
                    struct namarg *oldarg = (struct namarg*)sv_at(sv_forl,found.ix);

                    if(is_outarg && found.ix >= parent->nout) {
                        {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = ((struct namarg*)sv_at(formal_argument_list,_2))->name;
    mutpp->errlloc = (yylsp[-1]);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                        ABORT_PARSE_LOC("formal argument `%s` is an input argument in parent snippet `%s`",
                                        ((struct namarg*)sv_at(formal_argument_list,_2))->name,parent->name);
                    } else if(!is_outarg && found.ix < parent->nout) {
                        {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = ((struct namarg*)sv_at(formal_argument_list,_2))->name;
    mutpp->errlloc = (yylsp[-1]);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                        ABORT_PARSE_LOC("formal argument `%s` is an output argument in parent snippet `%s`",
                                        ((struct namarg*)sv_at(formal_argument_list,_2))->name,parent->name);
                    } else if(((struct namarg*)sv_at(formal_argument_list,_2))->type == ENUM_NAMARG(NOVALUE) && oldarg->type != ENUM_NAMARG(NOVALUE)) {
                        {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = ((struct namarg*)sv_at(formal_argument_list,_2))->name;
    mutpp->errlloc = (yylsp[-1]);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                        ABORT_PARSE_LOC("formal argument `%s` has default value in parent snippet `%s`",
                                        ((struct namarg*)sv_at(formal_argument_list,_2))->name,parent->name);
                    }

                    if(((struct namarg*)sv_at(formal_argument_list,_2))->type == ENUM_NAMARG(NOVALUE)) { // existing param, move to head
                        if(is_outarg) {     if(!sv_push(&(head_out),&(*oldarg))) { YYNOMEM; }
 }
                        else {     if(!sv_push(&(head_in),&(*oldarg))) { YYNOMEM; }
 }
                        oldarg->name = NULL; // SENTINEL for moved
                    } else {
                        enum namarg_type oldtype = oldarg->type;
                        namarg_free(oldarg);   // clear
                        *oldarg = *((struct namarg*)sv_at(formal_argument_list,_2));  // replace
                        if(oldtype == ENUM_NAMARG(NOVALUE)) { // now has default value, move to tail
                            if(is_outarg) {     if(!sv_push(&(tail_out),&(*oldarg))) { YYNOMEM; }
 }
                            else {     if(!sv_push(&(tail_in),&(*oldarg))) { YYNOMEM; }
 }
                            oldarg->name = NULL; // SENTINEL for moved
                        }
                        *((struct namarg*)sv_at(formal_argument_list,_2)) = (struct namarg){ 0 }; // hand-over
                    }
                }
           }
}

            /* add parent's arguments in order (except those moved) */
            {
const size_t _len = sv_len(sv_forl);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _3 = _idx;
const size_t _idx = 0; (void)_idx;
                if(NULL == (*(const struct namarg*)sv_at(sv_forl,_3)).name) continue; // skip SENTINEL for moved
                if(_3 < parent->nout) {     if(!sv_push(&(head_out),&((*(const struct namarg*)sv_at(sv_forl,_3))))) { YYNOMEM; }
 }
                else {     if(!sv_push(&(head_in),&((*(const struct namarg*)sv_at(sv_forl,_3))))) { YYNOMEM; }
 }
           }
}
            sv_free(&sv_forl); // hand-over

            /* append lists in order */
            nout = sv_len(head_out) + sv_len(tail_out);
            sv_cat(&sv_forl_ord,head_out,tail_out,head_in,tail_in);
            sv_free(&head_out,&tail_out,&head_in,&tail_in); // hand-over
        } else if(sv_len(formal_argument_list) > 0) {
            /* NOTE: makes no sense here to add new formal arguments when there was none */
            pp->errlloc = (yylsp[-2]);
            ABORT_PARSE_LOC("parent snippet `%s` has no formal arguments",parent->name);
        }

        { // update arglinks inherited from parent
            const size_t nnew = sv_len(sv_forl_ord);
            /* setup hash map with new names */
            srt_hmap *    (newnames) = shm_alloc(SHM_SU,nnew);
    if(NULL == (newnames)) { YYNOMEM; }

            for(size_t i = 0; i < nnew; ++i) {
                const struct namarg *narg = sv_at(sv_forl_ord,i);
                {
    const srt_string *_ss = ss_crefs(narg->name);
        if(!shm_insert_su(&(newnames),_ss,i)) { YYNOMEM; }
}

            }
            /* update arglinks */
            for(size_t i = 0; i < nnew; ++i) {
                struct namarg *narg = (struct namarg*)sv_at(sv_forl_ord,i);
                if(narg->type == ENUM_NAMARG(ARGLINK)) {
                    const struct namarg *oldarg = sv_at(parent->sv_forl,CMOD_ARG_ARGLINK_get(*narg));
                    setix ixnew = { 0 }; {
    const srt_string *_ss = ss_crefs(oldarg->name);
        if(NULL == newnames) {  }
    ixnew.set = shm_atp_su(newnames,_ss,&(ixnew).ix);
}

                    assert(ixnew.set); // must be present, cannot lose arguments
                    *narg = CMOD_ARG_ARGLINK_xset(ixnew.ix,.name=narg->name); // update arglink
                } else if(narg->type == ENUM_NAMARG(EXPR)) {
                    for(size_t j = 0; j < sv_len(CMOD_ARG_EXPR_get(*narg)); ++j) {
                        struct namarg *earg = (struct namarg*)sv_at(CMOD_ARG_EXPR_get(*narg),j);
                        if(earg->type != ENUM_NAMARG(ARGLINK)) continue; // not arglink
                        const struct namarg *oldarg = sv_at(parent->sv_forl,CMOD_ARG_ARGLINK_get(*earg));
                        setix ixnew = { 0 }; {
    const srt_string *_ss = ss_crefs(oldarg->name);
        if(NULL == newnames) {  }
    ixnew.set = shm_atp_su(newnames,_ss,&(ixnew).ix);
}

                        assert(ixnew.set); // must be present, cannot lose arguments
                        *earg = CMOD_ARG_ARGLINK_xset(ixnew.ix,.name=earg->name); // update arglink
                    }
                }
            }
            shm_free(&newnames);
        }

#if DEBUG
        for(size_t i = 0; i < sv_len(sv_forl_ord); ++i) {
            const struct namarg *narg = sv_at(sv_forl_ord,i);
            fprintf(stderr,"%s (%d)",narg->name,narg->type);
            if(narg->type == ENUM_NAMARG(ARGLINK)) {
                fprintf(stderr," -> %zu\n",CMOD_ARG_ARGLINK_get(*narg));
            } else if(narg->type == ENUM_NAMARG(EXPR)) {
                fprintf(stderr,"\n");
                const vec_namarg *vnarg = CMOD_ARG_EXPR_get(*narg);
                for(size_t j = 0; j < sv_len(vnarg); ++j) {
                    const struct namarg *enarg = sv_at(vnarg,j);
                    fprintf(stderr,"  %d",enarg->type);
                    if(enarg->type == ENUM_NAMARG(ARGLINK))
                        fprintf(stderr," -> %zu",CMOD_ARG_ARGLINK_get(*enarg));
                    fprintf(stderr,"\n");
                }
            } else fprintf(stderr,"\n");
        }
        fprintf(stderr,"\n");
#endif
        struct snippet snip = { 0 };
                snip = (struct snippet){
        .name="<anonymous>", .body=snippet_body,
        .sv_forl=sv_forl_ord, .sv_argl=sv_argl,
        .redef=false, .nout=nout
    };
    { // check snippet
        size_t ierr = 0;
        const char *err = NULL;
        int ret = snip_def_check(&snip,false,&ierr,&err); // is_lambda = false
        if(ret >= 2)     ((struct param*)yyget_extra(yyscanner))->errlloc = (yylsp[-2]);
        switch(ret) {
            case 0:
                if(ierr == 0) break;
                VERBOSE("unused arguments (%zu) in snippet `%s`",ierr,snip.name);
                break;
            case 1: YYNOMEM;; break;
            case 2:
                ABORT_PARSE_LOC("formal argument at position %zu has no name in snippet `%s`",ierr,"<anonymous>");
                break;
            case 3:
                ABORT_PARSE_LOC("invalid special argument %s in snippet `%s`",err,"<anonymous>");
                break;
            case 4:
                ABORT_PARSE_LOC("reference to undefined formal argument `%s` in snippet `%s`",err,"<anonymous>");
                break;
            case 5:
                ABORT_PARSE_LOC("positional reference %zu out of bounds in snippet `%s`",ierr,"<anonymous>");
                break;
            case 6:
                ABORT_PARSE_LOC("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
            case 7:
                ABORT_PARSE_LOC("invalid arglink to self in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
            case 8:
                ABORT_PARSE_LOC("arglink is too deep in formal argument `%s` of snippet `%s`",err,"<anonymous>");
                break;
        }
    }

        snip.name = NULL; // unborrow
        (yyval.snip) = snip;

        free((yyvsp[-2].txt));
        {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].vnarg)));
}

    }
#line 10707 "parser.c"
    break;

  case 319: /* opt_DLLARG: %empty  */
#line 3742 "parser.y"
             {
        (yyval.dll) = (struct dllarg){ 0 };
        
    }
#line 10716 "parser.c"
    break;

  case 320: /* opt_DLLARG: "argument reference"  */
#line 3746 "parser.y"
             { (yyval.dll) = (yyvsp[0].dll);  }
#line 10722 "parser.c"
    break;

  case 322: /* dllarg: '$' "unsigned integer"  */
#line 3750 "parser.y"
                       {
        (yyval.dll) = (struct dllarg){
            .pos= pp->count - 1,
            .num= XINT_UNSIGNED_get((yyvsp[0].xint))
        };
        yyclearin; yyset_start(SNIPPET,yyscanner); // NOTE: scanner hack!
    }
#line 10734 "parser.c"
    break;

  case 323: /* dllarg: '$' opt_DLLARG '{' "unsigned integer" '}'  */
#line 3757 "parser.y"
                                          { (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt);
        (yyval.dll) = (yyvsp[-3].dll);
        (yyval.dll).pos= pp->count - 1;
        (yyval.dll).num= XINT_UNSIGNED_get((yyvsp[-1].xint));
    }
#line 10744 "parser.c"
    break;

  case 324: /* dllarg: '$' opt_DLLARG '{' "extended identifier" '}'  */
#line 3762 "parser.y"
                                            { (void)(yyvsp[-2].ctxt); (void)(yyvsp[0].ctxt);
        (yyval.dll) = (yyvsp[-3].dll);
        (yyval.dll).pos= pp->count - 1;
        (yyval.dll).name= (yyvsp[-1].txt); (yyvsp[-1].txt) = NULL; // hand-over
    }
#line 10754 "parser.c"
    break;

  case 325: /* dllarg: '$' opt_DLLARG '{' '#' "unsigned integer" '}'  */
#line 3767 "parser.y"
                                              { (void)(yyvsp[-3].ctxt); (void)(yyvsp[0].ctxt);
        (yyval.dll) = (yyvsp[-4].dll);
        (yyval.dll).type= DLLTYPE_ARGNAME;
        (yyval.dll).pos= pp->count - 1;
        (yyval.dll).num= XINT_UNSIGNED_get((yyvsp[-1].xint));
    }
#line 10765 "parser.c"
    break;

  case 326: /* dllarg: '$' opt_DLLARG '{' '#' opt_UNSIGNED_INT "special argument reference" '}'  */
#line 3773 "parser.y"
                                                                 { (void)(yyvsp[-4].ctxt); (void)(yyvsp[0].ctxt);
        (yyval.dll) = (yyvsp[-5].dll);
        (yyval.dll).type= (yyvsp[-1].i);
        (yyval.dll).pos= pp->count - 1;
        (yyval.dll).rep= (ENUM_XINT(INVALID) == (yyvsp[-2].xint).type) ? 0 : XINT_UNSIGNED_get((yyvsp[-2].xint));
        if((yyval.dll).rep > pp->iter_limit &&
            ( DLLTYPE_M == (yyval.dll).type||DLLTYPE_B == (yyval.dll).type||DLLTYPE_D == (yyval.dll).type||DLLTYPE_N == (yyval.dll).type||DLLTYPE_S == (yyval.dll).type||DLLTYPE_T == (yyval.dll).type )) {
            pp->errlloc = (yylsp[-2]);
            ABORT_PARSE_LOC("special argument repeat count exceeds -L/--iter-limit=%zu",pp->iter_limit);
        }
    }
#line 10781 "parser.c"
    break;

  case 327: /* dllarg_list: dllarg  */
#line 3787 "parser.y"
             {
        if(NULL == pp->sv_dllarg) {
                (pp->sv_dllarg) = sv_alloc(sizeof(struct dllarg),64,NULL);
    if(sv_void == (pp->sv_dllarg)) { YYNOMEM; }
        }
            if(!sv_push(&(pp->sv_dllarg),&((yyvsp[0].dll)))) { YYNOMEM; }
    }
#line 10793 "parser.c"
    break;

  case 328: /* dllarg_list: dllarg_list dllarg  */
#line 3794 "parser.y"
                         {
            if(!sv_push(&(pp->sv_dllarg),&((yyvsp[0].dll)))) { YYNOMEM; }
    }
#line 10801 "parser.c"
    break;

  case 331: /* snippet_body: "%{" opt_dllarg_list "%}"  */
#line 3806 "parser.y"
                                {
        ss_cat_cn1(&pp->hstr_buf,'\0'); // NUL-terminate
        if(!is_valid_utf8((const unsigned char*)ss_get_buffer_r(pp->hstr_buf),ss_len(pp->hstr_buf))) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("invalid UTF-8 input");
        }
                if(ss_void == ((yyval.str) = ss_dup(pp->hstr_buf))) { YYNOMEM; }
    (yyval.str) = ss_shrink(&((yyval.str)));
    ss_clear(pp->hstr_buf);
        (yyloc) = (yylsp[-1]);
    }
#line 10817 "parser.c"
    break;

  case 332: /* opt_snippet_body: "newline"  */
#line 3820 "parser.y"
           { (yyval.str) = NULL; }
#line 10823 "parser.c"
    break;

  case 334: /* cmod_plain_argument: herestring  */
#line 3827 "parser.y"
               {
        (yyval.narg) = CMOD_ARG_VERBATIM_set((yyvsp[0].txt));
    }
#line 10831 "parser.c"
    break;

  case 335: /* cmod_plain_argument: "identifier"  */
#line 3830 "parser.y"
                {
        (yyval.narg) = CMOD_ARG_ID_set((yyvsp[0].txt));
    }
#line 10839 "parser.c"
    break;

  case 336: /* cmod_plain_argument: "extended identifier"  */
#line 3833 "parser.y"
                    {
        (yyval.narg) = CMOD_ARG_IDEXT_set((yyvsp[0].txt));
    }
#line 10847 "parser.c"
    break;

  case 337: /* cmod_plain_argument: integer_string  */
#line 3836 "parser.y"
                    {
        (yyval.narg) = CMOD_ARG_INTSTR_set((yyvsp[0].txt));
    }
#line 10855 "parser.c"
    break;

  case 338: /* cmod_plain_argument: "special identifier"  */
#line 3839 "parser.y"
                        {
        (yyval.narg) = CMOD_ARG_SPECIAL_set((yyvsp[0].txt));
    }
#line 10863 "parser.c"
    break;

  case 339: /* cmod_plain_argument: "%NR"  */
#line 3842 "parser.y"
           {
        (yyval.narg) = CMOD_ARG_ROWNO_set();
    }
#line 10871 "parser.c"
    break;

  case 340: /* cmod_plain_argument: "*"  */
#line 3845 "parser.y"
         {
        (yyval.narg) = CMOD_ARG_OPTIONAL_set();
    }
#line 10879 "parser.c"
    break;

  case 341: /* cmod_plain_argument: cmod_plain_argument "." herestring  */
#line 3848 "parser.y"
                                         {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_VERBATIM_set(
                            (yyvsp[0].txt));
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10898 "parser.c"
    break;

  case 342: /* cmod_plain_argument: cmod_plain_argument "." "identifier"  */
#line 3862 "parser.y"
                                         {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_VERBATIM_set(
                            (yyvsp[0].txt));
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10917 "parser.c"
    break;

  case 343: /* cmod_plain_argument: cmod_plain_argument "." "extended identifier"  */
#line 3876 "parser.y"
                                             {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_VERBATIM_set(
                            (yyvsp[0].txt));
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10936 "parser.c"
    break;

  case 344: /* cmod_plain_argument: cmod_plain_argument "." integer_string  */
#line 3890 "parser.y"
                                             {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_VERBATIM_set(
                            (yyvsp[0].txt));
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10955 "parser.c"
    break;

  case 345: /* cmod_plain_argument: cmod_plain_argument "." "special identifier"  */
#line 3904 "parser.y"
                                                 {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_SPECIAL_set(
                            (yyvsp[0].txt));
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10974 "parser.c"
    break;

  case 346: /* cmod_plain_argument: cmod_plain_argument "." "%NR"  */
#line 3918 "parser.y"
                                    {
        if((yyvsp[-2].narg).type != ENUM_NAMARG(EXPR)) { // initialize expression
            vec_namarg *expr = NULL;
                (expr) = sv_alloc(sizeof(struct namarg),4,NULL);
    if(sv_void == (expr)) { YYNOMEM; }

                if(!sv_push(&(expr),&((yyvsp[-2].narg)))) { YYNOMEM; }

            (yyval.narg) = CMOD_ARG_EXPR_set(expr);
        }
        struct namarg new = CMOD_ARG_ROWNO_set(
                            );
            if(!sv_push(&(CMOD_ARG_EXPR_get((yyval.narg))),&(new))) { YYNOMEM; }
    }
#line 10993 "parser.c"
    break;

  case 347: /* cmod_named_argument: identifier_ext '=' cmod_plain_argument  */
#line 3935 "parser.y"
                                             { (void)(yyvsp[-1].ctxt); (yyval.narg) = (yyvsp[0].narg); (yyval.narg).name = (yyvsp[-2].txt); }
#line 10999 "parser.c"
    break;

  case 349: /* cmod_argument_comma_list: cmod_argument_comma_list_nodang ','  */
#line 3940 "parser.y"
                                          { (yyval.vnarg) = (yyvsp[-1].vnarg); (void)(yyvsp[0].ctxt); }
#line 11005 "parser.c"
    break;

  case 350: /* cmod_argument_comma_list_nodang: cmod_plain_argument  */
#line 3944 "parser.y"
                          {
            ((yyval.vnarg)) = sv_alloc(sizeof(struct namarg),32,NULL);
    if(sv_void == ((yyval.vnarg))) { YYNOMEM; }
            (pp->shm_dup) = shm_alloc(SHM_SU,64);
    if(NULL == (pp->shm_dup)) { YYNOMEM; }
            if(!sv_push(&((yyval.vnarg)),&((yyvsp[0].narg)))) { YYNOMEM; }
    }
#line 11017 "parser.c"
    break;

  case 351: /* cmod_argument_comma_list_nodang: cmod_named_argument  */
#line 3951 "parser.y"
                          {
            ((yyval.vnarg)) = sv_alloc(sizeof(struct namarg),32,NULL);
    if(sv_void == ((yyval.vnarg))) { YYNOMEM; }
            (pp->shm_dup) = shm_alloc(SHM_SU,64);
    if(NULL == (pp->shm_dup)) { YYNOMEM; }
            if(!sv_push(&((yyval.vnarg)),&((yyvsp[0].narg)))) { YYNOMEM; }
        {
    const srt_string *_ss = ss_crefs((yyvsp[0].narg).name);
        if(!shm_insert_su(&(pp->shm_dup),_ss,sv_len((yyval.vnarg)) - 1)) { YYNOMEM; }
}
    }
#line 11033 "parser.c"
    break;

  case 352: /* cmod_argument_comma_list_nodang: cmod_argument_comma_list_nodang ',' cmod_plain_argument  */
#line 3962 "parser.y"
                                                              {
        (yyval.vnarg) = (yyvsp[-2].vnarg); (void)(yyvsp[-1].ctxt);
        if(NULL != ((const struct namarg*)sv_at((yyval.vnarg),sv_len((yyval.vnarg)) - 1))->name) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("plain argument not allowed after named argument");
        }
            if(!sv_push(&((yyval.vnarg)),&((yyvsp[0].narg)))) { YYNOMEM; }
    }
#line 11046 "parser.c"
    break;

  case 353: /* cmod_argument_comma_list_nodang: cmod_argument_comma_list_nodang ',' cmod_named_argument  */
#line 3970 "parser.y"
                                                              {
        (yyval.vnarg) = (yyvsp[-2].vnarg); (void)(yyvsp[-1].ctxt);
        setix found = { 0 };
        {
    const srt_string *_ss = ss_crefs((yyvsp[0].narg).name);
        if(NULL == pp->shm_dup) {  }
    found.set = shm_atp_su(pp->shm_dup,_ss,&(found).ix);
}

        if(!found.set) { // new => insert
                if(!sv_push(&((yyval.vnarg)),&((yyvsp[0].narg)))) { YYNOMEM; }
            {
    const srt_string *_ss = ss_crefs((yyvsp[0].narg).name);
        if(!shm_insert_su(&(pp->shm_dup),_ss,sv_len((yyval.vnarg)) - 1)) { YYNOMEM; }
}
        } else { // existing => replace
            if(!pp->idup.set) pp->idup = found; // store duplicate index (first time only)
            struct namarg *old = (struct namarg*)sv_at((yyval.vnarg),found.ix);
            namarg_free(old); // clear
            *old = (yyvsp[0].narg);        // replace
        }
    }
#line 11073 "parser.c"
    break;

  case 354: /* cmod_arguments: '(' cmod_argument_comma_list ')'  */
#line 3995 "parser.y"
                                       {
        (void)(yyvsp[-2].ctxt); (yyval.vnarg) = (yyvsp[-1].vnarg); (void)(yyvsp[0].ctxt);
        shm_free(&pp->shm_dup);
    }
#line 11082 "parser.c"
    break;

  case 355: /* cmod_arguments_unique: cmod_arguments  */
#line 4003 "parser.y"
                     {
        (yyval.vnarg) = (yyvsp[0].vnarg);
        if(pp->idup.set) // duplicate named argument
            ABORT_PARSE("duplicate named argument `%s`",
                        ((const struct namarg*)sv_at((yyval.vnarg),pp->idup.ix))->name);
    }
#line 11093 "parser.c"
    break;

  case 357: /* cmod_function_arguments: cmod_arguments_unique  */
#line 4014 "parser.y"
                            {
        vec_namarg *namargs = (yyval.vnarg) = (yyvsp[0].vnarg);
        {
const size_t _len = sv_len(namargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _4 = _idx;
const size_t _idx = 0; (void)_idx;
            switch(((struct namarg*)sv_at(namargs,_4))->type) {
                case ENUM_NAMARG(ARGLINK): break; // cannot happen
                case ENUM_NAMARG(ID): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_4)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_ID_get(*((struct namarg*)sv_at(namargs,_4))),.name= ((struct namarg*)sv_at(namargs,_4))->name);
                    break;
                case ENUM_NAMARG(IDEXT): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_4)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_IDEXT_get(*((struct namarg*)sv_at(namargs,_4))),.name= ((struct namarg*)sv_at(namargs,_4))->name);
                    break;
                            case ENUM_NAMARG(VERBATIM): /* FALLTHROUGH */
                           case ENUM_NAMARG(INTSTR): /* FALLTHROUGH */
                           case ENUM_NAMARG(SPECIAL): /* FALLTHROUGH */
                break; // OK
                default:
                    ABORT_PARSE("unexpected %s value",namarg_type_strenum(((struct namarg*)sv_at(namargs,_4))->type));
                    break;
            }
       }
}
    }
#line 11124 "parser.c"
    break;

  case 358: /* cmod_formal_arguments: cmod_arguments_unique  */
#line 4044 "parser.y"
                            {
        vec_namarg *namargs = (yyval.vnarg) = (yyvsp[0].vnarg);
        srt_set *sms_dup = NULL;
            (sms_dup) = sms_alloc(SMS_S,sv_len(namargs));
    if(NULL == (sms_dup)) { YYNOMEM; }

        {
const size_t _len = sv_len(namargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _5 = _idx;
const size_t _idx = 0; (void)_idx;
            if(NULL == ((struct namarg*)sv_at(namargs,_5))->name) {
                if(((struct namarg*)sv_at(namargs,_5))->type != ENUM_NAMARG(ID) && ((struct namarg*)sv_at(namargs,_5))->type != ENUM_NAMARG(IDEXT))
                    ABORT_PARSE0("expected extended identifier as formal argument name");
                char *val_as_name = CMOD_ARG_IDEXT_get(*((struct namarg*)sv_at(namargs,_5)));
                *((struct namarg*)sv_at(namargs,_5)) = CMOD_ARG_NOVALUE_xset(.name= val_as_name); // swap name and value
            }
            /* check duplicates, needed cos we swapped names and values */
            bool found = false;
                found = sms_count_s(sms_dup,ss_crefs(((struct namarg*)sv_at(namargs,_5))->name));

            if(found) {
                {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = ((struct namarg*)sv_at(namargs,_5))->name;
    mutpp->errlloc = (yyloc);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                ABORT_PARSE_LOC("duplicate formal argument or column name `%s`",((struct namarg*)sv_at(namargs,_5))->name);
            }
                if(!sms_insert_s(&(sms_dup),ss_crefs(((struct namarg*)sv_at(namargs,_5))->name))) { YYNOMEM; };

       }
}
        sms_free(&sms_dup);
    }
#line 11169 "parser.c"
    break;

  case 359: /* empty_cmod_arguments: '(' ')'  */
#line 4087 "parser.y"
              { (yyval.vnarg) = NULL; (void)(yyvsp[-1].ctxt); (void)(yyvsp[0].ctxt); }
#line 11175 "parser.c"
    break;

  case 361: /* snippet_formal_arguments: cmod_formal_arguments  */
#line 4093 "parser.y"
                            {
        vec_namarg *namargs = (yyval.vnarg) = (yyvsp[0].vnarg);
        {
const size_t _len = sv_len(namargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _6 = _idx;
const size_t _idx = 0; (void)_idx;
            switch(((struct namarg*)sv_at(namargs,_6))->type) {
                case ENUM_NAMARG(ARGLINK): break; // cannot happen
                case ENUM_NAMARG(ID): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_6)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_ID_get(*((struct namarg*)sv_at(namargs,_6))),.name= ((struct namarg*)sv_at(namargs,_6))->name);
                    break;
                case ENUM_NAMARG(IDEXT): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_6)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_IDEXT_get(*((struct namarg*)sv_at(namargs,_6))),.name= ((struct namarg*)sv_at(namargs,_6))->name);
                    break;
                case ENUM_NAMARG(EXPR):
                    for(size_t i = 0; i < sv_len(CMOD_ARG_EXPR_get(*((struct namarg*)sv_at(namargs,_6)))); ++i) {
                        const struct namarg *earg = sv_at(CMOD_ARG_EXPR_get(*((struct namarg*)sv_at(namargs,_6))),i);
                        if(earg->type == ENUM_NAMARG(ROWNO))
                            ABORT_PARSE0("unexpected row number in expression");
                    }
                    break;
                            case ENUM_NAMARG(NOVALUE): /* FALLTHROUGH */
                           case ENUM_NAMARG(VERBATIM): /* FALLTHROUGH */
                           case ENUM_NAMARG(INTSTR): /* FALLTHROUGH */
                           case ENUM_NAMARG(SPECIAL): /* FALLTHROUGH */
                           case ENUM_NAMARG(OPTIONAL): /* FALLTHROUGH */
                break; // OK
                default:
                    ABORT_PARSE("unexpected %s argument value",namarg_type_strenum(((struct namarg*)sv_at(namargs,_6))->type));
                    break;
            }
       }
}
    }
#line 11215 "parser.c"
    break;

  case 362: /* snippet_formal_arguments_inout: %empty  */
#line 4131 "parser.y"
             { (yyval.vnarg) = NULL; }
#line 11221 "parser.c"
    break;

  case 363: /* snippet_formal_arguments_inout: snippet_formal_arguments  */
#line 4132 "parser.y"
                               { (yyval.vnarg) = (yyvsp[0].vnarg); }
#line 11227 "parser.c"
    break;

  case 364: /* snippet_formal_arguments_inout: snippet_formal_arguments "<-" snippet_formal_arguments  */
#line 4133 "parser.y"
                                                             {
        { // check duplicate names across both lists
            srt_set *sms_dup = NULL;
                (sms_dup) = sms_alloc(SMS_S,sv_len((yyvsp[-2].vnarg)) + sv_len((yyvsp[0].vnarg)));
    if(NULL == (sms_dup)) { YYNOMEM; }

            YYLTYPE namarg_loc[2] = { (yylsp[-2]), (yylsp[0]) }; // iterable
            const vec_namarg *namarg_lists[2] = { (yyvsp[-2].vnarg), (yyvsp[0].vnarg) }; // iterable
            for(int i = 0; i < 2; ++i) {
                {
const size_t _len = sv_len(namarg_lists [ i ]);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _7 = _idx;
const size_t _idx = 0; (void)_idx;
                    bool found = false;
                        found = sms_count_s(sms_dup,ss_crefs(((struct namarg*)sv_at(namarg_lists [ i ],_7))->name));

                    if(found) {
                        {
    struct param *mutpp = yyget_extra(yyscanner);
    mutpp->errtxt = ((struct namarg*)sv_at(namarg_lists [ i ],_7))->name;
    mutpp->errlloc = namarg_loc[i];
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,mutpp->errlloc.first_line,&(mutpp->errlloc));
        if(found) {  }
    }
}
                        ABORT_PARSE_LOC("duplicate formal argument `%s`",((struct namarg*)sv_at(namarg_lists [ i ],_7))->name);
                    }
                        if(!sms_insert_s(&(sms_dup),ss_crefs(((struct namarg*)sv_at(namarg_lists [ i ],_7))->name))) { YYNOMEM; };

               }
}
            }
            sms_free(&sms_dup);
        }
        (yyval.vnarg) = (yyvsp[-2].vnarg); pp->nout = sv_len((yyvsp[-2].vnarg)); (yyvsp[-2].vnarg) = NULL; // hand-over
        sv_cat(&(yyval.vnarg),(yyvsp[0].vnarg)); sv_free(&(yyvsp[0].vnarg)); // hand-over and clear
    }
#line 11272 "parser.c"
    break;

  case 365: /* opt_table_columns: %empty  */
#line 4177 "parser.y"
             {
        (yyval.vnarg) = NULL;
        
    }
#line 11281 "parser.c"
    break;

  case 366: /* opt_table_columns: table_columns  */
#line 4181 "parser.y"
                    { (yyval.vnarg) = (yyvsp[0].vnarg);  }
#line 11287 "parser.c"
    break;

  case 367: /* table_columns: cmod_formal_arguments  */
#line 4184 "parser.y"
                            { // table-specific checks
        vec_namarg *namargs = (yyval.vnarg) = (yyvsp[0].vnarg);
        {
const size_t _len = sv_len(namargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _8 = _idx;
const size_t _idx = 0; (void)_idx;
            switch(((struct namarg*)sv_at(namargs,_8))->type) {
                case ENUM_NAMARG(ARGLINK): break; // cannot happen
                case ENUM_NAMARG(ID): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_8)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_ID_get(*((struct namarg*)sv_at(namargs,_8))),.name= ((struct namarg*)sv_at(namargs,_8))->name);
                    break;
                case ENUM_NAMARG(IDEXT): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_8)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_IDEXT_get(*((struct namarg*)sv_at(namargs,_8))),.name= ((struct namarg*)sv_at(namargs,_8))->name);
                    break;
                            case ENUM_NAMARG(NOVALUE): /* FALLTHROUGH */
                           case ENUM_NAMARG(VERBATIM): /* FALLTHROUGH */
                           case ENUM_NAMARG(INTSTR): /* FALLTHROUGH */
                break; // OK
                default:
                    ABORT_PARSE("unexpected %s column value",namarg_type_strenum(((struct namarg*)sv_at(namargs,_8))->type));
                    break;
            }
       }
}
    }
#line 11318 "parser.c"
    break;

  case 369: /* snippet_actual_arguments: cmod_arguments  */
#line 4215 "parser.y"
                     { // recall-like checks
        const vec_namarg *namargs = (yyval.vnarg) = (yyvsp[0].vnarg);
        {
const size_t _len = sv_len(namargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _9 = _idx;
const size_t _idx = 0; (void)_idx;
            switch(((struct namarg*)sv_at(namargs,_9))->type) {
                case ENUM_NAMARG(ARGLINK): break; // cannot happen
                case ENUM_NAMARG(ID): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_9)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_ID_get(*((struct namarg*)sv_at(namargs,_9))),.name= ((struct namarg*)sv_at(namargs,_9))->name);
                    break;
                case ENUM_NAMARG(IDEXT): // change type to VERBATIM
                    *((struct namarg*)sv_at(namargs,_9)) = CMOD_ARG_VERBATIM_xset(CMOD_ARG_IDEXT_get(*((struct namarg*)sv_at(namargs,_9))),.name= ((struct namarg*)sv_at(namargs,_9))->name);
                    break;
                            case ENUM_NAMARG(VERBATIM): /* FALLTHROUGH */
                           case ENUM_NAMARG(INTSTR): /* FALLTHROUGH */
                           case ENUM_NAMARG(SPECIAL): /* FALLTHROUGH */
                           case ENUM_NAMARG(ROWNO): /* FALLTHROUGH */
                           case ENUM_NAMARG(EXPR): /* FALLTHROUGH */
                break; // OK
                default:
                    ABORT_PARSE("unexpected %s value",namarg_type_strenum(((struct namarg*)sv_at(namargs,_9))->type));
                    break;
            }
       }
}
    }
#line 11351 "parser.c"
    break;

  case 370: /* snippet_actual_arguments_list: snippet_actual_arguments  */
#line 4246 "parser.y"
                               {     ((yyval.vvnarg)) = sv_alloc_t(SV_PTR,4);
    if(sv_void == ((yyval.vvnarg))) { YYNOMEM; }
     if(!sv_push(&((yyval.vvnarg)),&((yyvsp[0].vnarg)))) { YYNOMEM; }
 }
#line 11360 "parser.c"
    break;

  case 371: /* snippet_actual_arguments_list: snippet_actual_arguments_list snippet_actual_arguments  */
#line 4250 "parser.y"
                                                             {
        (yyval.vvnarg) = (yyvsp[-1].vvnarg);     if(!sv_push(&((yyval.vvnarg)),&((yyvsp[0].vnarg)))) { YYNOMEM; }

    }
#line 11369 "parser.c"
    break;

  case 372: /* snippet_actual_arguments_inout: %empty  */
#line 4257 "parser.y"
             { // append empty list
        pp->nout = 0;
            ((yyval.vvnarg)) = sv_alloc_t(SV_PTR,1);
    if(sv_void == ((yyval.vvnarg))) { YYNOMEM; }
        vec_namarg *empty = NULL;
            if(!sv_push(&((yyval.vvnarg)),&(empty))) { YYNOMEM; }
    }
#line 11381 "parser.c"
    break;

  case 373: /* snippet_actual_arguments_inout: snippet_actual_arguments_list  */
#line 4264 "parser.y"
                                    { pp->nout = 0; (yyval.vvnarg) = (yyvsp[0].vvnarg); }
#line 11387 "parser.c"
    break;

  case 374: /* snippet_actual_arguments_inout: snippet_actual_arguments_list "<-" snippet_actual_arguments_list  */
#line 4265 "parser.y"
                                                                       {
        if((pp->nout = sv_len((yyvsp[-2].vvnarg))) != sv_len((yyvsp[0].vvnarg))) {
            pp->errlloc = (yylsp[0]);
            ABORT_PARSE_LOC0("different number of input and output argument lists");
        }
        (yyval.vvnarg) = (yyvsp[-2].vvnarg); (yyvsp[-2].vvnarg) = NULL; // hand-over
        sv_cat(&(yyval.vvnarg),(yyvsp[0].vvnarg)); sv_free(&(yyvsp[0].vvnarg)); // hand-over and clear
    }
#line 11400 "parser.c"
    break;

  case 375: /* snippet_actual_arguments_inout_idcap: snippet_actual_arguments_inout  */
#line 4275 "parser.y"
                                     {
        const size_t nidcap = sv_len(pp->sv_idlist);
        if(nidcap > 0) { // handle idcap
            const bool has_out = (pp->nout > 0);
            size_t narglist = sv_len((yyvsp[0].vvnarg)) + (has_out ? 0 : sv_len((yyvsp[0].vvnarg)));
            vec_vec_namarg *    (arglists) = sv_alloc_t(SV_PTR,narglist);
    if(sv_void == (arglists)) { YYNOMEM; }
;

            pp->nout = has_out ? pp->nout : sv_len((yyvsp[0].vvnarg));
            for(size_t i = 0; i < pp->nout; ++i) { // prepend identifiers to each output list
                vec_namarg *    (sv_outargs) = sv_alloc(sizeof(struct namarg),nidcap,NULL);
    if(sv_void == (sv_outargs)) { YYNOMEM; }
;
                {
const size_t _len = sv_len(pp -> sv_idlist);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _10 = _idx;
const size_t _idx = 0; (void)_idx;
                    char *        txt = (NULL != ((*(char**)sv_at(pp -> sv_idlist,_10)))) ? rstrdup((*(char**)sv_at(pp -> sv_idlist,_10))) : NULL;
    if(NULL != ((*(char**)sv_at(pp -> sv_idlist,_10))) && NULL == (txt)) { YYNOMEM; }
   
; // duplicate
                    struct namarg item = CMOD_ARG_VERBATIM_set(txt);
                        if(!sv_push(&(sv_outargs),&(item))) { YYNOMEM; }

               }
}
                if(has_out) {
                    vec_namarg *outargs = sv_at_ptr((yyvsp[0].vvnarg),i);
                    {
const size_t _len = sv_len(outargs);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _11 = _idx;
const size_t _idx = 0; (void)_idx;
                        struct namarg *item = ((struct namarg*)sv_at(outargs,_11));
                            if(!sv_push(&(sv_outargs),&(*item))) { YYNOMEM; }

                        *item = (struct namarg){ 0 }; // hand-over
                   }
}
                    sv_free(&outargs); // clear
                }
                    if(!sv_push(&(arglists),&(sv_outargs))) { YYNOMEM; }
;
            }
            for(size_t i = (has_out ? pp->nout : 0); i < sv_len((yyvsp[0].vvnarg)); ++i) {
                vec_namarg **item = (vec_namarg**)sv_at((yyvsp[0].vvnarg),i);
                    if(!sv_push(&(arglists),&(*item))) { YYNOMEM; }
;
                *item = NULL; // hand-over
            }
            sv_free(&(yyvsp[0].vvnarg)); // clear
            (yyvsp[0].vvnarg) = arglists;
        }
        (yyval.vvnarg) = (yyvsp[0].vvnarg);
    }
#line 11462 "parser.c"
    break;

  case 376: /* mod_case: c_expression_list herestring  */
#line 4336 "parser.y"
                                   {
        (yyval.mcase) = MOD_CASE_EXPRL_xset((yyvsp[-1].vstr),.body= (yyvsp[0].txt));
    }
#line 11470 "parser.c"
    break;

  case 377: /* mod_case: compound_initializer_list herestring  */
#line 4339 "parser.y"
                                           {
        (yyval.mcase) = MOD_CASE_VACDI_xset((yyvsp[-1].vacdi),.body= (yyvsp[0].txt));
    }
#line 11478 "parser.c"
    break;

  case 378: /* mod_case: herestring  */
#line 4342 "parser.y"
                 {
        (yyval.mcase) = MOD_CASE_DEFAULT_xset(.body=(yyvsp[0].txt));
    }
#line 11486 "parser.c"
    break;

  case 379: /* case_list: mod_case  */
#line 4348 "parser.y"
               {     ((yyval.vmcase)) = sv_alloc(sizeof(struct mod_case),4,NULL);
    if(sv_void == ((yyval.vmcase))) { YYNOMEM; }
     if(!sv_push(&((yyval.vmcase)),&((yyvsp[0].mcase)))) { YYNOMEM; }
 }
#line 11495 "parser.c"
    break;

  case 380: /* case_list: case_list mod_case  */
#line 4352 "parser.y"
                         {
        (yyval.vmcase) = (yyvsp[-1].vmcase);     if(!sv_push(&((yyval.vmcase)),&((yyvsp[0].mcase)))) { YYNOMEM; }

    }
#line 11504 "parser.c"
    break;

  case 381: /* opt_case_list: %empty  */
#line 4358 "parser.y"
             {
        (yyval.vmcase) = NULL;
        
    }
#line 11513 "parser.c"
    break;

  case 382: /* opt_case_list: case_list  */
#line 4362 "parser.y"
                { (yyval.vmcase) = (yyvsp[0].vmcase);  }
#line 11519 "parser.c"
    break;

  case 383: /* switch_body: "%{" opt_case_list "%}"  */
#line 4364 "parser.y"
                                               {
        (yyval.vmcase) = (yyvsp[-1].vmcase);
        
    }
#line 11528 "parser.c"
    break;

  case 384: /* cmod_file: %empty  */
#line 4372 "parser.y"
             { pp->stop_parse = true; }
#line 11534 "parser.c"
    break;

  case 385: /* cmod_file: mod_statement_list  */
#line 4373 "parser.y"
                         { pp->stop_parse = false; }
#line 11540 "parser.c"
    break;

  case 388: /* print_mod_statement: mod_statement  */
#line 4382 "parser.y"
                    {
        size_t buflen = 0;
        if((buflen = ss_len(pp->kw_buf)) > 0) {
            ssize_t nwrite = ss_write(yyget_out(yyscanner),pp->kw_buf,0,buflen);
            if(nwrite < 0 || (size_t)nwrite != buflen)
                ABORT_PARSE0("failed writing keyword buffer to output file");
        }
        ss_clear(pp->kw_buf);
    }
#line 11554 "parser.c"
    break;

  case 421: /* mod_statement: error  */
#line 4426 "parser.y"
               { // intentional error triggered by keyword action
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        // else keep going
    }
#line 11566 "parser.c"
    break;

  case 422: /* kw_defined: "% defined" opt_keyword_options paren_cmod_identifier_comma_list  */
#line 4438 "parser.y"
                                                                     {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_defined_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vstr), &(yylsp[0]),        pp->inactive_defined,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[0].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[0].vstr)));
}
 (yyvsp[0].vstr) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11602 "parser.c"
    break;

  case 423: /* kw_defined: "% defined" opt_keyword_options cmod_identifier_comma_list cmod_function_arguments  */
#line 4469 "parser.y"
                                                                                        {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_defined_action_1(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_defined,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11644 "parser.c"
    break;

  case 424: /* kw_defined: "% defined" error ')'  */
#line 4506 "parser.y"
                            { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 11661 "parser.c"
    break;

  case 425: /* kw_defined: "% defined" error SCANNER_ERROR  */
#line 4518 "parser.y"
                                      { YYABORT; }
#line 11667 "parser.c"
    break;

  case 426: /* kw_delay: "% delay" opt_keyword_options opt_herestring paren_unsigned_int  */
#line 4522 "parser.y"
                                                                    {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_delay_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].xint), &(yylsp[0]),        pp->inactive_delay,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;           xint_free(&(yyvsp[0].xint)); (yyvsp[0].xint) = (struct xint){ 0 };       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11693 "parser.c"
    break;

  case 427: /* kw_delay: "% delay" error ')'  */
#line 4543 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 11710 "parser.c"
    break;

  case 428: /* kw_delay: "% delay" error SCANNER_ERROR  */
#line 4555 "parser.y"
                                    { YYABORT; }
#line 11716 "parser.c"
    break;

  case 429: /* kw_dsl: "% dsl" opt_keyword_options "identifier" mod_herestring  */
#line 4559 "parser.y"
                                                          {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_dsl_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_dsl,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11742 "parser.c"
    break;

  case 430: /* kw_dsl: "% dsl" error "newline"  */
#line 4580 "parser.y"
                         { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 11759 "parser.c"
    break;

  case 431: /* kw_dsl: "% dsl" error SCANNER_ERROR  */
#line 4592 "parser.y"
                                  { YYABORT; }
#line 11765 "parser.c"
    break;

  case 432: /* kw_dsl_def: "% dsl-def" opt_keyword_options "identifier" mod_assign table_like "newline"  */
#line 4596 "parser.y"
                                                                          {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_dsl_def_action_0(yyscanner,pp,&(yyvsp[-4].vopt),&(yylsp[-4]),
                    &(yyvsp[-3].txt), &(yylsp[-3]),            &(yyvsp[-2].c), &(yylsp[-2]),            &(yyvsp[-1].tlike), &(yylsp[-1]),                    pp->inactive_dsl_def,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-3].txt)); (yyvsp[-3].txt) = NULL;            (yyvsp[-2].c) = '\0';           (yyvsp[-1].tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).sel); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).lst); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].tlike).lst));
}
 (yyvsp[-1].tlike) = (struct table_like){ 0 };                  {
    for(size_t i = 0; i < sv_len((yyvsp[-4].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-4].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-4].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11807 "parser.c"
    break;

  case 433: /* kw_dsl_def: "% dsl-def" error "newline"  */
#line 4633 "parser.y"
                             { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 11824 "parser.c"
    break;

  case 434: /* kw_dsl_def: "% dsl-def" error SCANNER_ERROR  */
#line 4645 "parser.y"
                                      { YYABORT; }
#line 11830 "parser.c"
    break;

  case 435: /* kw_include: "% include" opt_keyword_options "file path" "newline"  */
#line 4649 "parser.y"
                                                   {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_include_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),                    pp->inactive_include,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11856 "parser.c"
    break;

  case 436: /* kw_include: "% include" error "newline"  */
#line 4670 "parser.y"
                             { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 11873 "parser.c"
    break;

  case 437: /* kw_include: "% include" error SCANNER_ERROR  */
#line 4682 "parser.y"
                                      { YYABORT; }
#line 11879 "parser.c"
    break;

  case 438: /* kw_intop: "% intop" opt_keyword_options paren_integer_comma_list  */
#line 4686 "parser.y"
                                                           {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_intop_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vxint), &(yylsp[0]),        pp->inactive_intop,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vxint)); ++i) {
        const void *_item = sv_at((yyvsp[0].vxint),i);
        xint_free((struct xint*)_item);    }
    sv_free(&((yyvsp[0].vxint)));
}
 (yyvsp[0].vxint) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11911 "parser.c"
    break;

  case 439: /* kw_intop: "% intop" error ')'  */
#line 4713 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 11928 "parser.c"
    break;

  case 440: /* kw_intop: "% intop" error SCANNER_ERROR  */
#line 4725 "parser.y"
                                    { YYABORT; }
#line 11934 "parser.c"
    break;

  case 441: /* kw_map: "% map" opt_keyword_options table_like '|' recall_like "newline"  */
#line 4729 "parser.y"
                                                                {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_map_action_0(yyscanner,pp,&(yyvsp[-4].vopt),&(yylsp[-4]),
                    &(yyvsp[-3].tlike), &(yylsp[-3]),            &(yyvsp[-2].ctxt), &(yylsp[-2]),            &(yyvsp[-1].rlike), &(yylsp[-1]),                    pp->inactive_map,&pp->kw_buf);

    /* cleanup */
            (yyvsp[-3].tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len((yyvsp[-3].tlike).sel); ++i) {
        const void *_item = sv_at((yyvsp[-3].tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-3].tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len((yyvsp[-3].tlike).lst); ++i) {
        const void *_item = sv_at((yyvsp[-3].tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-3].tlike).lst));
}
 (yyvsp[-3].tlike) = (struct table_like){ 0 };            (yyvsp[-2].ctxt) = NULL;           (yyvsp[-1].rlike).snip = NULL; free((yyvsp[-1].rlike).name); (yyvsp[-1].rlike).name = NULL; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].rlike).arg_lists); ++i) {
        const void *_item = sv_at((yyvsp[-1].rlike).arg_lists,i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&((yyvsp[-1].rlike).arg_lists));
}
 (yyvsp[-1].rlike) = (struct recall_like){ 0 };                  {
    for(size_t i = 0; i < sv_len((yyvsp[-4].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-4].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-4].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 11991 "parser.c"
    break;

  case 442: /* kw_map: "% map" opt_keyword_options table_like opt_pipe snippet_body  */
#line 4781 "parser.y"
                                                                  {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_map_action_1(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].tlike), &(yylsp[-2]),            &(yyvsp[-1].ctxt), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_map,&pp->kw_buf);

    /* cleanup */
            (yyvsp[-2].tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len((yyvsp[-2].tlike).sel); ++i) {
        const void *_item = sv_at((yyvsp[-2].tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-2].tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len((yyvsp[-2].tlike).lst); ++i) {
        const void *_item = sv_at((yyvsp[-2].tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-2].tlike).lst));
}
 (yyvsp[-2].tlike) = (struct table_like){ 0 };            (yyvsp[-1].ctxt) = NULL;           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12033 "parser.c"
    break;

  case 443: /* kw_map: "% map" error "newline"  */
#line 4818 "parser.y"
                         { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12050 "parser.c"
    break;

  case 444: /* kw_map: "% map" error SCANNER_ERROR  */
#line 4830 "parser.y"
                                  { YYABORT; }
#line 12056 "parser.c"
    break;

  case 445: /* kw_once: "% once" opt_keyword_options cmod_identifier "newline"  */
#line 4834 "parser.y"
                                                      {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_once_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),                    pp->inactive_once,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12082 "parser.c"
    break;

  case 446: /* kw_once: "% once" error "newline"  */
#line 4855 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12099 "parser.c"
    break;

  case 447: /* kw_once: "% once" error SCANNER_ERROR  */
#line 4867 "parser.y"
                                   { YYABORT; }
#line 12105 "parser.c"
    break;

  case 448: /* kw_pipe: "% pipe" opt_keyword_options herestring opt_mod_herestring  */
#line 4871 "parser.y"
                                                               {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_pipe_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_pipe,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12131 "parser.c"
    break;

  case 449: /* kw_pipe: "% pipe" opt_keyword_options herestring recall_like "newline"  */
#line 4892 "parser.y"
                                                              {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_pipe_action_1(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].rlike), &(yylsp[-1]),                    pp->inactive_pipe,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           (yyvsp[-1].rlike).snip = NULL; free((yyvsp[-1].rlike).name); (yyvsp[-1].rlike).name = NULL; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].rlike).arg_lists); ++i) {
        const void *_item = sv_at((yyvsp[-1].rlike).arg_lists,i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&((yyvsp[-1].rlike).arg_lists));
}
 (yyvsp[-1].rlike) = (struct recall_like){ 0 };                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12172 "parser.c"
    break;

  case 450: /* kw_pipe: "% pipe" error "newline"  */
#line 4928 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12189 "parser.c"
    break;

  case 451: /* kw_pipe: "% pipe" error SCANNER_ERROR  */
#line 4940 "parser.y"
                                   { YYABORT; }
#line 12195 "parser.c"
    break;

  case 452: /* kw_recall: "% recall" opt_keyword_options recall_like recall_end  */
#line 4944 "parser.y"
                                                          {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_recall_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].rlike), &(yylsp[-1]),            &(yyvsp[0].c), &(yylsp[0]),        pp->inactive_recall,&pp->kw_buf);

    /* cleanup */
            (yyvsp[-1].rlike).snip = NULL; free((yyvsp[-1].rlike).name); (yyvsp[-1].rlike).name = NULL; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].rlike).arg_lists); ++i) {
        const void *_item = sv_at((yyvsp[-1].rlike).arg_lists,i);
        {
    vec_namarg* vec = *(vec_namarg**)(_item);
    {
    for(size_t i = 0; i < sv_len(vec); ++i) {
        const void *_item = sv_at(vec,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&(vec));
}
}
    }
    sv_free(&((yyvsp[-1].rlike).arg_lists));
}
 (yyvsp[-1].rlike) = (struct recall_like){ 0 };            (yyvsp[0].c) = '\0';       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12236 "parser.c"
    break;

  case 453: /* kw_recall: "% recall" error recall_end  */
#line 4980 "parser.y"
                                  { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        (void)(yyvsp[0].c);
    }
#line 12253 "parser.c"
    break;

  case 454: /* kw_recall: "% recall" error SCANNER_ERROR  */
#line 4992 "parser.y"
                                     { YYABORT; }
#line 12259 "parser.c"
    break;

  case 455: /* kw_snippet: "% snippet" opt_keyword_options cmod_identifier snippet_formal_arguments_inout "newline"  */
#line 4996 "parser.y"
                                                                                        {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_snippet_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].vnarg), &(yylsp[-1]),                    pp->inactive_snippet,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].vnarg)));
}
 (yyvsp[-1].vnarg) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12291 "parser.c"
    break;

  case 456: /* kw_snippet: "% snippet" opt_keyword_options cmod_identifier mod_assign snippet_like  */
#line 5023 "parser.y"
                                                                             {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_snippet_action_1(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].c), &(yylsp[-1]),            &(yyvsp[0].snip), &(yylsp[0]),        pp->inactive_snippet,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;            (yyvsp[-1].c) = '\0';           snippet_clear(&(yyvsp[0].snip)); (yyvsp[0].snip) = (struct snippet){ 0 };       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12317 "parser.c"
    break;

  case 457: /* kw_snippet: "% snippet" opt_keyword_options cmod_identifier "file path" "newline"  */
#line 5044 "parser.y"
                                                                    {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_snippet_action_2(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].txt), &(yylsp[-1]),                    pp->inactive_snippet,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12343 "parser.c"
    break;

  case 458: /* kw_snippet: "% snippet" error "newline"  */
#line 5065 "parser.y"
                             { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12360 "parser.c"
    break;

  case 459: /* kw_snippet: "% snippet" error SCANNER_ERROR  */
#line 5077 "parser.y"
                                      { YYABORT; }
#line 12366 "parser.c"
    break;

  case 460: /* kw_strcmp: "% strcmp" opt_keyword_options cmod_function_arguments  */
#line 5081 "parser.y"
                                                           {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_strcmp_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_strcmp,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12398 "parser.c"
    break;

  case 461: /* kw_strcmp: "% strcmp" error ')'  */
#line 5108 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12415 "parser.c"
    break;

  case 462: /* kw_strcmp: "% strcmp" error SCANNER_ERROR  */
#line 5120 "parser.y"
                                     { YYABORT; }
#line 12421 "parser.c"
    break;

  case 463: /* kw_strlen: "% strlen" opt_keyword_options cmod_function_arguments  */
#line 5124 "parser.y"
                                                           {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_strlen_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_strlen,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12453 "parser.c"
    break;

  case 464: /* kw_strlen: "% strlen" error ')'  */
#line 5151 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12470 "parser.c"
    break;

  case 465: /* kw_strlen: "% strlen" error SCANNER_ERROR  */
#line 5163 "parser.y"
                                     { YYABORT; }
#line 12476 "parser.c"
    break;

  case 466: /* kw_strstr: "% strstr" opt_keyword_options cmod_function_arguments  */
#line 5167 "parser.y"
                                                           {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_strstr_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_strstr,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12508 "parser.c"
    break;

  case 467: /* kw_strstr: "% strstr" error ')'  */
#line 5194 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12525 "parser.c"
    break;

  case 468: /* kw_strstr: "% strstr" error SCANNER_ERROR  */
#line 5206 "parser.y"
                                     { YYABORT; }
#line 12531 "parser.c"
    break;

  case 469: /* kw_strsub: "% strsub" opt_keyword_options cmod_function_arguments  */
#line 5210 "parser.y"
                                                           {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_strsub_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_strsub,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12563 "parser.c"
    break;

  case 470: /* kw_strsub: "% strsub" error ')'  */
#line 5237 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12580 "parser.c"
    break;

  case 471: /* kw_strsub: "% strsub" error SCANNER_ERROR  */
#line 5249 "parser.y"
                                     { YYABORT; }
#line 12586 "parser.c"
    break;

  case 472: /* kw_table: "% table" opt_keyword_options cmod_identifier table_columns "newline"  */
#line 5253 "parser.y"
                                                                     {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].vnarg), &(yylsp[-1]),                    pp->inactive_table,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].vnarg)));
}
 (yyvsp[-1].vnarg) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12618 "parser.c"
    break;

  case 473: /* kw_table: "% table" opt_keyword_options cmod_identifier mod_assign table "newline"  */
#line 5280 "parser.y"
                                                                         {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_action_1(yyscanner,pp,&(yyvsp[-4].vopt),&(yylsp[-4]),
                    &(yyvsp[-3].txt), &(yylsp[-3]),            &(yyvsp[-2].c), &(yylsp[-2]),            &(yyvsp[-1].tab), &(yylsp[-1]),                    pp->inactive_table,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-3].txt)); (yyvsp[-3].txt) = NULL;            (yyvsp[-2].c) = '\0';           table_clear(&(yyvsp[-1].tab)); (yyvsp[-1].tab) = (struct table){ 0 };                  {
    for(size_t i = 0; i < sv_len((yyvsp[-4].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-4].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-4].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12644 "parser.c"
    break;

  case 474: /* kw_table: "% table" opt_keyword_options opt_cmod_identifier "file path" "newline"  */
#line 5301 "parser.y"
                                                                      {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_action_2(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].txt), &(yylsp[-1]),                    pp->inactive_table,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12670 "parser.c"
    break;

  case 475: /* kw_table: "% table" error "newline"  */
#line 5322 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12687 "parser.c"
    break;

  case 476: /* kw_table: "% table" error SCANNER_ERROR  */
#line 5334 "parser.y"
                                    { YYABORT; }
#line 12693 "parser.c"
    break;

  case 477: /* kw_table_get: "% table-get" opt_keyword_options cmod_identifier_comma_list cmod_function_arguments  */
#line 5338 "parser.y"
                                                                                         {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_get_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].vnarg), &(yylsp[0]),        pp->inactive_table_get,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[0].vnarg)); ++i) {
        const void *_item = sv_at((yyvsp[0].vnarg),i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[0].vnarg)));
}
 (yyvsp[0].vnarg) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12735 "parser.c"
    break;

  case 478: /* kw_table_get: "% table-get" error ')'  */
#line 5375 "parser.y"
                              { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12752 "parser.c"
    break;

  case 479: /* kw_table_get: "% table-get" error SCANNER_ERROR  */
#line 5387 "parser.y"
                                        { YYABORT; }
#line 12758 "parser.c"
    break;

  case 480: /* kw_table_length: "% table-length" opt_keyword_options cmod_identifier_comma_list paren_special_id_list  */
#line 5391 "parser.y"
                                                                                          {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_length_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].vstr), &(yylsp[0]),        pp->inactive_table_length,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[0].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[0].vstr)));
}
 (yyvsp[0].vstr) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12804 "parser.c"
    break;

  case 481: /* kw_table_length: "% table-length" error ')'  */
#line 5432 "parser.y"
                                 { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12821 "parser.c"
    break;

  case 482: /* kw_table_length: "% table-length" error SCANNER_ERROR  */
#line 5444 "parser.y"
                                           { YYABORT; }
#line 12827 "parser.c"
    break;

  case 483: /* kw_table_size: "% table-size" opt_keyword_options paren_cmod_identifier_comma_list  */
#line 5448 "parser.y"
                                                                        {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_size_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].vstr), &(yylsp[0]),        pp->inactive_table_size,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[0].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[0].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[0].vstr)));
}
 (yyvsp[0].vstr) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12863 "parser.c"
    break;

  case 484: /* kw_table_size: "% table-size" error ')'  */
#line 5479 "parser.y"
                               { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 12880 "parser.c"
    break;

  case 485: /* kw_table_size: "% table-size" error SCANNER_ERROR  */
#line 5491 "parser.y"
                                         { YYABORT; }
#line 12886 "parser.c"
    break;

  case 486: /* kw_table_stack: "% table-stack" opt_keyword_options cmod_identifier paren_cmod_identifier_comma_list "newline"  */
#line 5495 "parser.y"
                                                                                              {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_table_stack_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].vstr), &(yylsp[-1]),                    pp->inactive_table_stack,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;                  {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12922 "parser.c"
    break;

  case 487: /* kw_table_stack: "% table-stack" error "newline"  */
#line 5526 "parser.y"
                                 { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12939 "parser.c"
    break;

  case 488: /* kw_table_stack: "% table-stack" error SCANNER_ERROR  */
#line 5538 "parser.y"
                                          { YYABORT; }
#line 12945 "parser.c"
    break;

  case 489: /* kw_unittest: "% unittest" opt_keyword_options opt_identifier opt_herestring snippet_body  */
#line 5542 "parser.y"
                                                                                {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_unittest_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_unittest,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 12971 "parser.c"
    break;

  case 490: /* kw_unittest: "% unittest" error "newline"  */
#line 5563 "parser.y"
                              { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 12988 "parser.c"
    break;

  case 491: /* kw_unittest: "% unittest" error SCANNER_ERROR  */
#line 5575 "parser.y"
                                       { YYABORT; }
#line 12994 "parser.c"
    break;

  case 492: /* kw_arrlen: "% arrlen" opt_keyword_options paren_c_expression  */
#line 5579 "parser.y"
                                                      {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_arrlen_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].txt), &(yylsp[0]),        pp->inactive_arrlen,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[0].txt)); (yyvsp[0].txt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13020 "parser.c"
    break;

  case 493: /* kw_arrlen: "% arrlen" error ')'  */
#line 5600 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13037 "parser.c"
    break;

  case 494: /* kw_arrlen: "% arrlen" error SCANNER_ERROR  */
#line 5612 "parser.y"
                                     { YYABORT; }
#line 13043 "parser.c"
    break;

  case 495: /* kw_def: "% def" opt_keyword_options "C identifier" '{'  */
#line 5616 "parser.y"
                                                 {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_def_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_def,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13069 "parser.c"
    break;

  case 496: /* kw_def: "% def" opt_keyword_options "C typedef name" "C identifier" '{'  */
#line 5637 "parser.y"
                                                                 {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_def_action_1(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_def,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13095 "parser.c"
    break;

  case 497: /* kw_def: "% def" opt_keyword_options "C identifier" "C identifier" '{'  */
#line 5658 "parser.y"
                                                               {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_def_action_2(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_def,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13121 "parser.c"
    break;

  case 498: /* kw_def: "% def" opt_keyword_options opt_c_identifier '}'  */
#line 5679 "parser.y"
                                                      {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_def_action_3(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_def,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13147 "parser.c"
    break;

  case 499: /* kw_def: "% def" error '{'  */
#line 5700 "parser.y"
                        { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13164 "parser.c"
    break;

  case 500: /* kw_def: "% def" error SCANNER_ERROR  */
#line 5712 "parser.y"
                                  { YYABORT; }
#line 13170 "parser.c"
    break;

  case 501: /* kw_enum: "% enum" opt_keyword_options "identifier" '=' table_like "newline"  */
#line 5716 "parser.y"
                                                                {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_enum_action_0(yyscanner,pp,&(yyvsp[-4].vopt),&(yylsp[-4]),
                    &(yyvsp[-3].txt), &(yylsp[-3]),            &(yyvsp[-2].ctxt), &(yylsp[-2]),            &(yyvsp[-1].tlike), &(yylsp[-1]),                    pp->inactive_enum,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-3].txt)); (yyvsp[-3].txt) = NULL;            (yyvsp[-2].ctxt) = NULL;           (yyvsp[-1].tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).sel); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).lst); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].tlike).lst));
}
 (yyvsp[-1].tlike) = (struct table_like){ 0 };                  {
    for(size_t i = 0; i < sv_len((yyvsp[-4].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-4].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-4].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13212 "parser.c"
    break;

  case 502: /* kw_enum: "% enum" error "newline"  */
#line 5753 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 13229 "parser.c"
    break;

  case 503: /* kw_enum: "% enum" error SCANNER_ERROR  */
#line 5765 "parser.y"
                                   { YYABORT; }
#line 13235 "parser.c"
    break;

  case 504: /* kw_foreach: "% foreach" opt_keyword_options "C identifier" paren_c_expression_list snippet_body  */
#line 5769 "parser.y"
                                                                                      {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_foreach_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_foreach,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13271 "parser.c"
    break;

  case 505: /* kw_foreach: "% foreach" error ')'  */
#line 5800 "parser.y"
                            { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13288 "parser.c"
    break;

  case 506: /* kw_foreach: "% foreach" error SCANNER_ERROR  */
#line 5812 "parser.y"
                                      { YYABORT; }
#line 13294 "parser.c"
    break;

  case 507: /* kw_free: "% free" opt_keyword_options paren_c_expression_list ';'  */
#line 5816 "parser.y"
                                                             {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_free_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_free,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13330 "parser.c"
    break;

  case 508: /* kw_free: "% free" error ';'  */
#line 5847 "parser.y"
                         { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13347 "parser.c"
    break;

  case 509: /* kw_free: "% free" error SCANNER_ERROR  */
#line 5859 "parser.y"
                                   { YYABORT; }
#line 13353 "parser.c"
    break;

  case 510: /* kw_prefix: "% prefix" opt_keyword_options "C identifier" ';'  */
#line 5863 "parser.y"
                                                    {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_prefix_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].txt), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_prefix,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-1].txt)); (yyvsp[-1].txt) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13379 "parser.c"
    break;

  case 511: /* kw_prefix: "% prefix" error ';'  */
#line 5884 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13396 "parser.c"
    break;

  case 512: /* kw_prefix: "% prefix" error SCANNER_ERROR  */
#line 5896 "parser.y"
                                     { YYABORT; }
#line 13402 "parser.c"
    break;

  case 513: /* kw_proto: "% proto" opt_keyword_options declaration  */
#line 5900 "parser.y"
                                              {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_proto_action_0(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].decl), &(yylsp[0]),        pp->inactive_proto,&pp->kw_buf);

    /* cleanup */
            decl_clear(&(yyvsp[0].decl)); (yyvsp[0].decl) = (struct decl){ 0 };       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13428 "parser.c"
    break;

  case 514: /* kw_proto: "% proto" error ';'  */
#line 5921 "parser.y"
                          { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13445 "parser.c"
    break;

  case 515: /* kw_proto: "% proto" error SCANNER_ERROR  */
#line 5933 "parser.y"
                                    { YYABORT; }
#line 13451 "parser.c"
    break;

  case 516: /* kw_strin: "% strin" opt_keyword_options cmod_identifier table_like opt_snippet_body  */
#line 5937 "parser.y"
                                                                              {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_strin_action_0(yyscanner,pp,&(yyvsp[-3].vopt),&(yylsp[-3]),
                    &(yyvsp[-2].txt), &(yylsp[-2]),            &(yyvsp[-1].tlike), &(yylsp[-1]),            &(yyvsp[0].str), &(yylsp[0]),        pp->inactive_strin,&pp->kw_buf);

    /* cleanup */
            free((yyvsp[-2].txt)); (yyvsp[-2].txt) = NULL;           (yyvsp[-1].tlike).tab = (struct table){ 0 }; {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).sel); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).sel,i);
        namarg_free((struct namarg*)_item);    }
    sv_free(&((yyvsp[-1].tlike).sel));
}
 {
    for(size_t i = 0; i < sv_len((yyvsp[-1].tlike).lst); ++i) {
        const void *_item = sv_at((yyvsp[-1].tlike).lst,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].tlike).lst));
}
 (yyvsp[-1].tlike) = (struct table_like){ 0 };           ss_free(&((yyvsp[0].str))); (yyvsp[0].str) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-3].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-3].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-3].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13493 "parser.c"
    break;

  case 517: /* kw_strin: "% strin" error "newline"  */
#line 5974 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        
        
    }
#line 13510 "parser.c"
    break;

  case 518: /* kw_strin: "% strin" error SCANNER_ERROR  */
#line 5986 "parser.y"
                                    { YYABORT; }
#line 13516 "parser.c"
    break;

  case 519: /* kw_switch: "% switch" opt_keyword_options paren_c_expression_list switch_body  */
#line 5990 "parser.y"
                                                                       {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_switch_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].vmcase), &(yylsp[0]),        pp->inactive_switch,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;           {
    for(size_t i = 0; i < sv_len((yyvsp[0].vmcase)); ++i) {
        const void *_item = sv_at((yyvsp[0].vmcase),i);
        mod_case_free((struct mod_case*)_item);    }
    sv_free(&((yyvsp[0].vmcase)));
}
 (yyvsp[0].vmcase) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13558 "parser.c"
    break;

  case 520: /* kw_switch: "% switch" error ')'  */
#line 6027 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13575 "parser.c"
    break;

  case 521: /* kw_switch: "% switch" error SCANNER_ERROR  */
#line 6039 "parser.y"
                                     { YYABORT; }
#line 13581 "parser.c"
    break;

  case 522: /* kw_typedef: "% typedef" opt_keyword_options c_name_comma_list ';'  */
#line 6043 "parser.y"
                                                          {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_typedef_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_typedef,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13617 "parser.c"
    break;

  case 523: /* kw_typedef: "% typedef" opt_keyword_options declaration  */
#line 6074 "parser.y"
                                                 {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_typedef_action_1(yyscanner,pp,&(yyvsp[-1].vopt),&(yylsp[-1]),
                    &(yyvsp[0].decl), &(yylsp[0]),        pp->inactive_typedef,&pp->kw_buf);

    /* cleanup */
            decl_clear(&(yyvsp[0].decl)); (yyvsp[0].decl) = (struct decl){ 0 };       {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-1].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13643 "parser.c"
    break;

  case 524: /* kw_typedef: "% typedef" error ';'  */
#line 6095 "parser.y"
                            { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13660 "parser.c"
    break;

  case 525: /* kw_typedef: "% typedef" error SCANNER_ERROR  */
#line 6107 "parser.y"
                                      { YYABORT; }
#line 13666 "parser.c"
    break;

  case 526: /* kw_unused: "% unused" opt_keyword_options identifier_list ';'  */
#line 6111 "parser.y"
                                                       {
        pp->kwlloc = (yyloc); // capture full keyword location
        if(pp->kwlloc.last_column == 1) --pp->kwlloc.last_line; // discount final newline
            int ret = cmod_unused_action_0(yyscanner,pp,&(yyvsp[-2].vopt),&(yylsp[-2]),
                    &(yyvsp[-1].vstr), &(yylsp[-1]),            &(yyvsp[0].ctxt), &(yylsp[0]),        pp->inactive_unused,&pp->kw_buf);

    /* cleanup */
            {
    for(size_t i = 0; i < sv_len((yyvsp[-1].vstr)); ++i) {
        const void *_item = sv_at((yyvsp[-1].vstr),i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&((yyvsp[-1].vstr)));
}
 (yyvsp[-1].vstr) = NULL;            (yyvsp[0].ctxt) = NULL;       {
    for(size_t i = 0; i < sv_len((yyvsp[-2].vopt)); ++i) {
        const void *_item = sv_at((yyvsp[-2].vopt),i);
        cmod_option_free((struct cmod_option*)_item);    }
    sv_free(&((yyvsp[-2].vopt)));
}

    switch(ret) {
            case 1: YYACCEPT; break;
           case -1: YYABORT; break;
           case -2: YYNOMEM; break;
           case -3: YYERROR; break;
       }
    }
#line 13702 "parser.c"
    break;

  case 527: /* kw_unused: "% unused" error ';'  */
#line 6142 "parser.y"
                           { // syntax error
        ++pp->nerr; // count parsing error
        if(pp->errabort) YYABORT;
        else if(pp->errnomem) YYNOMEM;
        else if(pp->erraccept || pp->exit_on_first_err) YYACCEPT;
        else { // else keep going
            yyclearin; // clear lookahead token
            yyset_start(INITIAL,yyscanner); // reset scanner to INITIAL state
        }
        (void)(yyvsp[0].ctxt);
        
    }
#line 13719 "parser.c"
    break;

  case 528: /* kw_unused: "% unused" error SCANNER_ERROR  */
#line 6154 "parser.y"
                                     { YYABORT; }
#line 13725 "parser.c"
    break;


#line 13729 "parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yytoken, &yylloc};
        if (yyreport_syntax_error (&yyctx, yyscanner, pp) == 2)
          YYNOMEM;
      }
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, yyscanner, pp);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, yyscanner, pp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, yyscanner, pp, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, yyscanner, pp);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, yyscanner, pp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 6157 "parser.y"

#include "scanner.h"

static int yyreport_syntax_error (const yypcontext_t *yyctx, yyscan_t yyscanner, struct param *pp) {
    /* scanner already printed error message */
    if(yypcontext_token(yyctx) == YYSYMBOL_SCANNER_ERROR) return 0;

    int rc = 0;
    pp = yyget_extra(yyscanner);
    YYSTYPE *yylval = yyget_lval(yyscanner);
    YYLTYPE *yylloc = yypcontext_location(yyctx);

#if DEBUG
    debug("%d:%d-%d:%d %s",
        ((*yylloc)).first_line,((*yylloc)).first_column,
        ((*yylloc)).last_line,((*yylloc)).last_column,
        "error");
    debug("%d:%d-%d:%d %s",
        (pp->kwlloc).first_line,(pp->kwlloc).first_column,
        (pp->kwlloc).last_line,(pp->kwlloc).last_column,
        "kwlloc");
#endif

    srt_string *    (msg) = ss_alloc(0);
    if(ss_void == (msg)) { return -1; }
;
    ss_cat_c(&msg,"syntax error");
    { // unexpected token
        yysymbol_kind_t unextok = yypcontext_token(yyctx);
        if(unextok != YYSYMBOL_YYEMPTY) {
            ss_cat_c(&msg,", unexpected ");

            if(pp->pretty) ss_cat_c(&msg,"\x1B[1m");
            ss_cat_c(&msg,yysymbol_name(unextok));
            if(pp->pretty) ss_cat_c(&msg,"\x1B[22m");

            if(unextok == YYSYMBOL_IDENTIFIER_EXT
                || unextok == YYSYMBOL_IDENTIFIER
                || unextok == YYSYMBOL_C_IDENTIFIER
                || unextok == YYSYMBOL_C_TYPEDEF_NAME
                || unextok == YYSYMBOL_C_ENUMERATION_CONSTANT)
                ss_cat_c(&msg," `",yylval->txt,"`");
        }
    }
    { // state
        yysymbol_kind_t sym = YY_ACCESSING_SYMBOL(*yyctx->yyssp);
        ss_cat_c(&msg," after ");

        if(pp->pretty) ss_cat_c(&msg,"\x1B[1m");
        const char *symname = yysymbol_name(sym);
        if('%' == symname[0] && ' ' == symname[1]) { // C% keyword
            ss_cat_cn1(&msg,'%');
            symname += 2;
        }
        ss_cat_c(&msg,symname);
        if(pp->pretty) ss_cat_c(&msg,"\x1B[22m");
    }

    { // expected tokens
        ss_cat_c(&msg,", expecting ");
        int nmax = yypcontext_expected_tokens(yyctx,NULL,0);
        yysymbol_kind_t expected[nmax];
        int n = yypcontext_expected_tokens(yyctx,expected,nmax);
        if(n < 0) rc = n; // forward error to yyparse
        else if(n == 0) ss_cat_c(&msg,"nothing");
        else if(n > 0) {
            if(pp->pretty) ss_cat_c(&msg,"\x1B[1m");
            if(YYSYMBOL_MOD_HERESTR_CLOSE == expected[0])
                ss_cat_c(&msg,pp->hstr_id);
            ss_cat_c(&msg,yysymbol_name(expected[0]));
            if(pp->pretty) ss_cat_c(&msg,"\x1B[22m");

            for(int i = 1; i < n; ++i) {
                ss_cat_c(&msg," or ");
                if(pp->pretty) ss_cat_c(&msg,"\x1B[1m");
                if(YYSYMBOL_MOD_HERESTR_CLOSE == expected[i])
                    ss_cat_c(&msg,pp->hstr_id);
                ss_cat_c(&msg,yysymbol_name(expected[i]));
                if(pp->pretty) ss_cat_c(&msg,"\x1B[22m");
            }
        }
    }
    ERROR("%s",ss_to_c(msg));
    ss_free(&msg);

    pp->kwlloc.last_line = yylloc->last_line; // capture full error context line
    pp->errlloc = *yylloc;
    if(!(pp->silent > 2)) {
    struct param *mutpp = yyget_extra(yyscanner);
    print_error(&(mutpp->errlloc),yyscanner);
    mutpp->errtxt = NULL;
}

    return rc;
}

static int get_error_context(const char *fpath, size_t first_line, size_t nline, srt_string *errctx[static nline]) {
    if(NULL == fpath) return 1; // invalid path
    FILE *fp = fopen(fpath,"r");
    if(NULL == fp) return 1; // failed opening file for read

    size_t lineno = 0;
    size_t linecnt = 0;
    char *line = NULL;
    ssize_t nread = 0;
    size_t len = 0;
    while((nread = getline(&line,&len,fp)) != -1) {
        if(++lineno < first_line) continue; // skip lines above
        else if(linecnt >= nline) break;
        srt_string *buf= ss_dup_c(line);
        errctx[linecnt++] = buf; // hand-over
    };
    free(line);

    (void)fclose(fp);

    if(linecnt != nline) {
#if DEBUG
        debug("expected %zu lines, got %zu",nline,linecnt);
#endif
        return 2; // not enough lines
    }

    return 0;
}

/* use current input line only */
static void print_error_line(YYLTYPE *yylloc, yyscan_t yyscanner) {
    const struct param *pp = yyget_extra(yyscanner);
#if DEBUG
    debug("%d:%d-%d:%d %s",
        ((*yylloc)).first_line,((*yylloc)).first_column,
        ((*yylloc)).last_line,((*yylloc)).last_column,
        "");
#endif
    (void)fprintf(stderr,"%" ERRMSG_SEP_LEN "d | ",yylloc->first_line);
    ss_write(stderr,pp->line,0,ss_len(pp->line));
    (void)fputc('\n',stderr);
    const char *errline = ss_to_c(pp->line);
        if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,(*yylloc).first_line,&((*yylloc)));
        if(found) {  }
    }
        print_wiggly_underline(yylloc->first_column,yylloc->last_column);
}

/* re-read context from input file (fallback to current input line) */
void print_error(YYLTYPE *yylloc, yyscan_t yyscanner) {
    const struct param *pp = yyget_extra(yyscanner);
#if DEBUG
    debug("%d:%d-%d:%d %s",
        ((*yylloc)).first_line,((*yylloc)).first_column,
        ((*yylloc)).last_line,((*yylloc)).last_column,
        "error");
    debug("%d:%d-%d:%d %s",
        (pp->kwlloc).first_line,(pp->kwlloc).first_column,
        (pp->kwlloc).last_line,(pp->kwlloc).last_column,
        "kwlloc");
#endif
    /* retrieve error context from input file */
    int ret = -1;
    size_t nctxline = pp->kwlloc.last_line - pp->kwlloc.first_line + 1;
    srt_string *errctx[nctxline]; memset(errctx,0x0,nctxline * sizeof(*errctx));
    const char *fpath = (NULL == pp->curbs) ? NULL : pp->curbs->fpath;
    if(!(ret = get_error_context(fpath,pp->kwlloc.first_line,nctxline,errctx))) {
        for(size_t i = 0; i < nctxline; ++i) {
            int lineno = pp->kwlloc.first_line + i;
            if(lineno < yylloc->first_line || lineno > yylloc->last_line) continue;
            if(NULL != pp->errtxt) { // search for text in error line
                const char *errline = ss_to_c(errctx[i]);
                    if(NULL != pp->errtxt && NULL != errline) {
        bool found = errline_find_name(errline,pp->errtxt,lineno,&((*yylloc)));
        if(found) { break; }
    }
                yylloc->first_column = 1; // start at beginning of following line
            }
        }
        for(size_t i = 0; i < nctxline; ++i) {
            int lineno = pp->kwlloc.first_line + i;
            if(lineno == yylloc->first_line) {
                (void)fprintf(stderr,"%" ERRMSG_SEP_LEN "d | ",lineno);
                ss_write(stderr,errctx[i],0,ss_len(errctx[i]));
                    print_wiggly_underline(yylloc->first_column,yylloc->last_column);
            } else {
                if(i == 0 || lineno == yylloc->first_line + 1)
                    (void)fprintf(stderr,"%" ERRMSG_SEP_LEN "d | ",lineno);
                else (void)fputs(ERRMSG_SEP " | ",stderr);
                ss_write(stderr,errctx[i],0,ss_len(errctx[i]));
            }
            ss_free(&errctx[i]);
        }
    } else { // fallback: current input line only
        if(NULL != fpath) VERBOSE("failed getting error context from input file (%d)",ret);
        print_error_line(yylloc,yyscanner);
    }
}

int collect_actual_args(vec_namarg *arglist, vec_namarg *argvals, const struct snippet *snip,
                        size_t lo, size_t nargs, const char *err[static 1]) {
    size_t npos = 0;
    {
const size_t _len = sv_len(arglist);
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _12 = _idx;
const size_t _idx = 0; (void)_idx;
        setix found = { 0 };
        if(NULL == ((struct namarg*)sv_at(arglist,_12))->name) {
            found = SETIX_set(lo + npos);
            if(++npos > nargs) return 2; // ERROR: too many positional arguments
        } else {
            {
    const srt_string *_ss = ss_crefs(((struct namarg*)sv_at(arglist,_12))->name);
        if(NULL == snip->shm_forl) {  }
    found.set = shm_atp_su(snip->shm_forl,_ss,&(found).ix);
}

            if(!found.set || found.ix < lo || found.ix >= lo + nargs) {
                *err = ((struct namarg*)sv_at(arglist,_12))->name; // blame
                return 3; // ERROR: argument not found
            }
        }
        struct namarg *varg = (struct namarg*)sv_at(argvals,found.ix);
        namarg_free(varg); // clear

        struct namarg argval = { 0 };
        get_actual_argval(((struct namarg*)sv_at(arglist,_12)),&argval);
        *varg = argval; // replace
        *((struct namarg*)sv_at(arglist,_12)) = (struct namarg){ 0 }; // hand-over
   }
}

    return 0;
}

int errline_find_name(const char *errline, const char *errtxt, int first_line, YYLTYPE *loc) {
    bool found = false;
    char cnext, cprev;
    const size_t errlen = strlen(errtxt);
    const char *line_loc = strstr(errline,errtxt);
    while(!found && NULL != line_loc) {
       ptrdiff_t ix = line_loc - errline;
#if DEBUG
       debug("context line(%jd): %s",ix,line_loc);
#endif
       if(ix + 1 >= loc->first_column // 1-based
               /* previous char (if present) is space, delimiter, comma or equals */
               && (ix == 0
                   || (ix > 0 && (isspace((unsigned char)(cprev = errline[ix - 1]))
                           || is_cdelim((unsigned char)cprev)
                           || ',' == cprev || '=' == cprev)))
               /* next char is space, delimiter, comma, equals or NUL */
               && (isspace((unsigned char)(cnext = errline[ix + errlen]))
                   || is_cdelim((unsigned char)cnext)
                   || ',' == cnext || '=' == cnext
                   || '\0' == cnext)
         ) {
           loc->first_column = ix + 1; // 1-based
           loc->last_column = ix + errlen + 1; // 1-based
           loc->first_line = first_line;
           found = true;
       } else line_loc = strstr(line_loc + 1,errtxt);
    }

    return found;
}
