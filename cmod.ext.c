#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <strings.h>
#endif

#include "cmod.ext.h"

#include "parts/typedef_namix.h"

#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>

#include "util_string.h"
#include "util_snippet.h"

static int cmp_size_t(const void *keyval, const void *datum)
{
    const size_t a = *(const size_t *)keyval;
    const size_t b = *(const size_t *)datum;
    return (a < b) ? -1 : ((a > b) ? 1 : 0);
}

/* string stuff */

const char *(strget_array_snip) (const void *array, size_t ix) {
    const vec_snippet * x = (const vec_snippet *)array;
    return ((const struct snippet *)sv_at(x, ix))->name;
}

const char *(strget_array_tab) (const void *array, size_t ix) {
    const vec_table * x = (const vec_table *)array;
    return ((const struct table *)sv_at(x, ix))->name;
}

const char *(strget_array_decl) (const void *array, size_t ix) {
    const srt_vector * x = (const srt_vector *)array;   // vec_proto or vec_type
    return ((const struct decl *)sv_at(x, ix))->name;
}

const char *(strget_array_fdef) (const void *array, size_t ix) {
    const vec_fdef * x = (const vec_fdef *)array;
    return ((const struct fdef *)sv_at(x, ix))->name;
}

const char *(strget_array_opt) (const void *array, size_t ix) {
    const struct option *x = (const struct option *)array;
    return x[ix].name;
}

/* namix stuff */
int cmp_namix_by_name(const void *keyval, const void *datum)
{
    struct namix *a = (struct namix *)keyval;
    struct namix *b = (struct namix *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}

int cmp_namix_as_float(const void *keyval, const void *datum)
{
    const namix *a = (const namix *)keyval;
    const namix *b = (const namix *)datum;

    double fa = 0.0;
    int reta = strtod_check(a->name, &fa);
    double fb = 0.0;
    int retb = strtod_check(b->name, &fb);

    if (reta && retb)
        return strcmp(a->name, b->name);        // both bad, sort as strings
    else if (reta)
        return 1;       // bad A, move to end
    else if (retb)
        return -1;      // bad B, move to end
    else
        return (fa < fb) ? -1 : ((fa > fb) ? 1 : 0);    // both good
}

int cmp_namix_as_int(const void *keyval, const void *datum)
{
    const namix *a = (const namix *)keyval;
    const namix *b = (const namix *)datum;

    intmax_t ia = 0;
    int reta = strtoimax_check_base(a->name, &ia, glob_pp->cmp_base);
    intmax_t ib = 0;
    int retb = strtoimax_check_base(b->name, &ib, glob_pp->cmp_base);

    if (reta && retb)
        return strcmp(a->name, b->name);        // both bad, sort as strings
    else if (reta)
        return 1;       // bad A, move to end
    else if (retb)
        return -1;      // bad B, move to end
    else
        return (ia < ib) ? -1 : ((ia > ib) ? 1 : 0);    // both good
}

/* error reporting stuff */
void print_wiggly_underline(size_t first, size_t last)
{
    (void)fputs(ERRMSG_SEP " | ", stderr);
    for (size_t i = 1; i < first; ++i)
        (void)fputc(' ', stderr);
    (void)fputc('^', stderr);
    for (size_t i = first + 1; i < last; ++i)
        (void)fputc('~', stderr);
    (void)fputc('\n', stderr);
}

/* file I/O stuff */
const struct ret_f0 open_ret_f = {
    .name = "open"
};

struct ret_f1 {
    char *name;
    const struct ret_f1 *p[1 + 1];
};

const struct ret_f1 file_open_write_ret_f = {
    .name = "file_open_write",
    .p[1] = (const struct ret_f1 *)&open_ret_f,
};

ret_t file_open_write(const char *pathname, bool overwrite, int fd_out[static 1])
{
    ret_t ret = 0;

    int flags = O_CREAT | O_WRONLY;
    flags |= overwrite ? O_TRUNC : O_EXCL;

    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

    errno = 0;
    int fd = open(pathname, flags, mode);
    if (fd == -1) {     // failed
        if (errno == EEXIST) {  // file exists, OK
            *fd_out = -1;
        }
        else {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

        }
    }
    else {      // success
        *fd_out = fd;
    }

    return ret;
}

/* map stuff */
/** set map iterator **/
static srt_bool get_unique_ix(const srt_string *k, uint64_t v, void *context)
{
    (void)k;
    size_t *order = ((size_t **)context)[0];
    size_t *n = ((size_t **)context)[1];
    order[(*n)++] = v;
    return S_TRUE;
}

int map_setup_order(const struct table *tab, size_t *order_o[static 1], size_t nrows_o[static 1],
                    const char *err[static 1], const struct map_cmod_opts *const opts)
{
    int rc = 0; // return code
    size_t *order = NULL;
    size_t nrows = TABLE_NROWS(*tab);

    if (opts->head_count > nrows || opts->tail_count > nrows
        || opts->nohead_count > nrows || opts->notail_count > nrows) {
        rc = 3;
        goto cleanup;   // ERROR: out-of-bounds row range
    }

    size_t lo = 0;      // inclusive low index
    if (opts->nohead_count > 0)
        lo = opts->nohead_count;
    else if (opts->tail_count > 0)
        lo = nrows - opts->tail_count;

    size_t hi = nrows;  // exclusive high index
    if (opts->head_count > 0)
        hi = opts->head_count;
    else if (opts->notail_count)
        hi = nrows - opts->notail_count;

    nrows = (hi > lo) ? hi - lo : 0;
    (order) = rmalloc((nrows) * sizeof(*order));
    if (NULL == (order)) {
        rc = 1;
        goto cleanup;
    }
    ;
    /* the first nrows elements of order contain the row selection */
    for (size_t i = 0; i < nrows; ++i)
        order[i] = lo + i;      // init order

    if (NULL != opts->notnull_column) { // filter non-empty values
        /* resolve and check notnull column index */
        size_t notnull_index = 0;
        if (tab_icol_from_opt(tab, opts->notnull_column, &notnull_index)) {
            *err = "notnull";   // blame option
            rc = 4;
            goto cleanup;       // ERROR: bad index
        };
        size_t nbad = 0;
        for (size_t i = 0; i < nrows; ++i) {
            size_t ix = order[i];
            const char *value = table_getcell(tab, ix, notnull_index);
            if ('\0' == value[0]) {
                ++nbad;
                order[i] = SIZE_MAX;
            }   // SENTINEL
        }
        if (nbad > 0) {
            qsort(order, nrows, sizeof(*order), cmp_size_t);    // move all SENTINEL to tail
            nrows -= nbad;      // remove from order
        }
    }

    if (opts->inv_bool) {       // invert row selection
        size_t nrows_old = nrows;
        nrows = TABLE_NROWS(*tab) - nrows_old;

        size_t *inv_order = NULL;
        (inv_order) = rmalloc((nrows) * sizeof(*inv_order));
        if (NULL == (inv_order)) {
            rc = 1;
            goto cleanup;
        }
        ;

        size_t k = 0, j = 0;
        for (size_t i = 0; i < nrows_old; ++i, ++j) {
            while (order[i] != j)
                inv_order[k++] = j++;
        }
        for (; j < TABLE_NROWS(*tab); ++j)
            inv_order[k++] = j;

        assert(nrows == k);
        free(order);
        order = inv_order;      // replace
    }

    if (nrows == 0) {
        *nrows_o = 0;
        *order_o = NULL;
        rc = 2;
        goto cleanup;   // NOOP: empty row range
    }

    if (NULL != opts->uniq_column) {    // filter unique values
        /* resolve and check uniq column index */
        size_t uniq_index = 0;
        if (tab_icol_from_opt(tab, opts->uniq_column, &uniq_index)) {
            *err = "uniq";      // blame option
            rc = 4;
            goto cleanup;       // ERROR: bad index
        };
        /* get indices for first occurrence of values in column */
        hmap_su *uniq = NULL;
        (uniq) = shm_alloc(SHM_SU, nrows);
        if (NULL == (uniq)) {
            rc = 1;
            goto cleanup;
        }

        for (size_t i = 0; i < nrows; ++i) {
            size_t ix = order[nrows - i - 1];   // reverse for first occurrence
            const char *value = table_getcell(tab, ix, uniq_index);
            {
                const srt_string *_ss = ss_crefs(value);
                if (!shm_insert_su(&(uniq), _ss, ix)) {
                    rc = 1;
                    goto cleanup;
                }
            }

        }
        nrows = shm_len(uniq);  // number of unique rows
        {       // get unique indices from hash map
            size_t n = 0;
            size_t *ctx[2] = { order, &n };
            shm_itp_su(uniq, 0, nrows, get_unique_ix, ctx);
        }
        shm_free(&uniq);
        qsort(order, nrows, sizeof(*order), cmp_size_t);        // increasing order
    }

    if (NULL != opts->sort_column) {    // sort rows
        /* resolve and check uniq column index */
        size_t sort_index = 0;
        if (tab_icol_from_opt(tab, opts->sort_column, &sort_index)) {
            *err = "sort";      // blame option
            rc = 4;
            goto cleanup;       // ERROR: bad index
        };
        /* collect values in sort key column */
        namix *(sortand) = rmalloc((nrows) * sizeof(*sortand));
        if (NULL == (sortand)) {
            rc = 1;
            goto cleanup;
        }
        ;
        for (size_t i = 0; i < nrows; ++i) {
            size_t ix = order[i];
            sortand[i] = (namix) {
                .name = (char *)table_getcell(tab, ix, sort_index),     // const borrow
            .ix = ix};
        }
        cmp_func_t *cmpf = cmp_namix_by_name;   // sort by string
        if (opts->num_index.set) {      // sort by numeric value
            cmpf = (opts->num_index.ix == 0) ? cmp_namix_as_float       // floating-point
                : cmp_namix_as_int;
            glob_pp->cmp_base = (opts->num_index.ix >= 2 && opts->num_index.ix <= 36)
                ? opts->num_index.ix : 0;       // special value
        }
        qsort(sortand, nrows, sizeof(*sortand), cmpf);
        for (size_t i = 0; i < nrows; i++)
            order[i] = sortand[i].ix;   // update order
        {
            free(sortand);
            (sortand) = NULL;
        }
    }

    if (opts->rev_bool)
        for (size_t i = 0; i < nrows / 2; ++i) {        // reverse order
            size_t tmp = order[i];
            order[i] = order[nrows - 1 - i];
            order[nrows - 1 - i] = tmp;
        }

    *order_o = order;
    order = NULL;       // hand-over
    *nrows_o = nrows;

 cleanup:
    {
        free(order);
        (order) = NULL;
    }

    return rc;
}

int map_table_lambda(const struct table *tab, const struct snippet *snip, const vec_namarg *selcols,
                     size_t nrows, size_t order[static nrows], const bool has_rowno,
                     const struct map_cmod_opts *const opts, vec_string *buffers[static 1],
                     srt_vector *buflens[static 1])
{
    int rc = 0; // return code
    vec_string *rowbufs = NULL;
    srt_vector *rowbuflens = NULL;

    const size_t ncol = sv_len(selcols);
    (rowbufs) = sv_alloc_t(SV_PTR, nrows);
    if (sv_void == (rowbufs)) {
        rc = 1;
        goto cleanup;
    }
    ;
    (rowbuflens) = sv_alloc_t(SV_U64, nrows);
    if (sv_void == (rowbuflens)) {
        rc = 1;
        goto cleanup;
    }
    ;

    for (size_t i = 0; i < nrows; ++i) {        // eval snippet once per row
        size_t nr = (opts->ord_bool ? i : order[i]) + opts->add1_count;
        const char *rowno = NULL;
        if (has_rowno) {
            char rowno_buf[STRUMAX_LEN];
            rowno = uint_tostr(nr, rowno_buf);
        }

        /* setup arguments (const borrow) */
        const char *values[ncol];
        for (size_t j = 0; j < ncol; ++j) {
            const struct namarg *selcol = sv_at(selcols, j);
            values[j] = table_getcell(tab, order[i], CMOD_ARG_ARGLINK_get(*selcol));
        }

        /* substitute arguments */
        size_t buflen = 0;
        char *buf = snip_get_buf_rowno(snip, ncol, values, &buflen, rowno);
        if (!sv_push(&(rowbufs), &(buf))) {
            rc = 1;
            goto cleanup;
        }

        if (!sv_push_u64(&(rowbuflens), buflen)) {
            rc = 1;
            goto cleanup;
        }

    }

    *buffers = rowbufs;
    *buflens = rowbuflens;
    rowbufs = NULL;     // hand-over
    rowbuflens = NULL;  // hand-over

 cleanup:
    {
        for (size_t i = 0; i < sv_len(rowbufs); ++i) {
            const void *_item = sv_at(rowbufs, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(rowbufs));
    }
    sv_free(&rowbuflens);

    return rc;
}

/* all arguments to VERBATIM */
static int bind_map_arg(struct namarg *item, const struct namarg *narg, const struct table *tab,
                        size_t irow, const char *rowno)
{
    char *value = NULL;
    switch (narg->type) {
    case CMOD_ARG_ARGLINK:     // was CMOD_ARG_SPECIAL
        value = (char *)table_getcell(tab, irow, CMOD_ARG_ARGLINK_get(*narg));
        *item = CMOD_ARG_VERBATIM_xset(value,.name = narg->name);       // borrow
        break;
    case CMOD_ARG_ROWNO:
        *item = CMOD_ARG_VERBATIM_xset((char *)rowno,.name = narg->name);       // const borrow
        break;
    case CMOD_ARG_EXPR:
        {
            srt_string *buf = NULL;
            (buf) = ss_alloc(8);
            if (ss_void == (buf)) {
                return 1;
            }

            for (size_t i = 0; i < sv_len(CMOD_ARG_EXPR_get(*narg)); ++i) {     // evaluate expression
                const struct namarg *varg = sv_at(CMOD_ARG_EXPR_get(*narg), i);
                const char *text = NULL;
                switch (varg->type) {
                case CMOD_ARG_ROWNO:
                    text = rowno;
                    break;
                case CMOD_ARG_ARGLINK: // was CMOD_ARG_SPECIAL
                    text = table_getcell(tab, irow, CMOD_ARG_ARGLINK_get(*varg));
                    break;
                case CMOD_ARG_ID:
                    text = CMOD_ARG_ID_get(*varg);
                    break;
                case CMOD_ARG_IDEXT:
                    text = CMOD_ARG_IDEXT_get(*varg);
                    break;
                case CMOD_ARG_INTSTR:
                    text = CMOD_ARG_INTSTR_get(*varg);
                    break;
                case CMOD_ARG_VERBATIM:
                    text = CMOD_ARG_VERBATIM_get(*varg);
                    break;
                default:
                    return -1;
                }
                ss_cat_c(&buf, text);
            }
            char *txt = NULL;
            {
                const char *ctmp = ss_to_c(buf);
                txt = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (txt)) {
                }

            }
            ss_free(&(buf));
            if (NULL == txt) {
                return 1;
            }

            *item = CMOD_ARG_VERBATIM_xset(txt,.name = narg->name);
        }
        break;
    case CMOD_ARG_NOVALUE:     /* FALLTHROUGH */
    case CMOD_ARG_VERBATIM:
        *item = *narg;  // pass-through (borrow)
        break;
    default:
        return -1;
    }
    return 0;
}

/* recall snippet once per row taking values from table columns */
int map_defined_snippet(const struct table *tab, const struct snippet *snip,
                        const vec_namarg *argvals, size_t nrows, size_t order[static nrows],
                        const bool has_rowno, const struct map_cmod_opts *const opts,
                        vec_string *buffers[static 1], srt_vector *buflens[static 1])
{
    int rc = 0; // return code
    vec_string *rowbufs = NULL;
    srt_vector *rowbuflens = NULL;

    const size_t nargs = sv_len(argvals);
    (rowbufs) = sv_alloc_t(SV_PTR, nrows);
    if (sv_void == (rowbufs)) {
        rc = 1;
        goto cleanup;
    }
    ;
    (rowbuflens) = sv_alloc_t(SV_U64, nrows);
    if (sv_void == (rowbuflens)) {
        rc = 1;
        goto cleanup;
    }
    ;

    for (size_t j = 0; j < nrows; ++j) {        // recall snippet once per row
        /* generate row number string */
        size_t nr = (opts->ord_bool ? j : order[j]) + opts->add1_count;
        const char *rowno = NULL;
        if (has_rowno) {
            char rowno_buf[STRUMAX_LEN];
            rowno = uint_tostr(nr, rowno_buf);
        }

        /* resolve arguments */
        vec_namarg *rowvals = NULL;
        (rowvals) = sv_alloc(sizeof(struct namarg), nargs, NULL);
        if (sv_void == (rowvals)) {
            rc = 1;
            goto loop_cleanup;
        }

        {
            const size_t _len = sv_len(argvals);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _0 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct namarg value = { 0 };
                switch (bind_map_arg
                        (&value, ((const struct namarg *)sv_at(argvals, _0)), tab, order[j],
                         rowno)) {
                case -1:
                    assert(0 && "C% internal error" "");
                case 1:
                    rc = 1;
                    goto loop_cleanup;
                    break;      // ERROR: out of memory
                }
                if (!sv_push(&(rowvals), &(value))) {
                    rc = 1;
                    goto loop_cleanup;
                }

            }
        }

        /* substitute arguments */
        size_t buflen = 0;
        char *buf = NULL;
        if (snippet_subst(snip, rowvals, &buf, &buflen) || NULL == buf) {
            rc = 1;
            goto loop_cleanup;
        }
        if (!sv_push(&(rowbufs), &(buf))) {
            rc = 1;
            goto loop_cleanup;
        }
        ;
        if (!sv_push_u64(&(rowbuflens), buflen)) {
            rc = 1;
            goto cleanup;
        }

 loop_cleanup:
        /* cleanup expression values */
        {
            const size_t _len = sv_len(argvals);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _1 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (((const struct namarg *)sv_at(argvals, _1))->type != CMOD_ARG_EXPR)
                    continue;   // not an expression
                struct namarg *varg = (struct namarg *)sv_at(rowvals, _1);
                free(CMOD_ARG_VERBATIM_get(*varg));
            }
        }
        sv_free(&rowvals);      // unborrow contents
        if (rc)
            goto cleanup;
    }

    *buffers = rowbufs;
    *buflens = rowbuflens;
    rowbufs = NULL;     // hand-over
    rowbuflens = NULL;  // hand-over

 cleanup:
    {
        for (size_t i = 0; i < sv_len(rowbufs); ++i) {
            const void *_item = sv_at(rowbufs, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(rowbufs));
    }
    sv_free(&rowbuflens);

    return rc;
}

/* table stuff */
int tab_icol_from_opt(const struct table *tab, const struct cmod_option *opt, size_t out[static 1])
{
    setix index = { 0 };
    switch (opt->type) {
    case ENUM_CMOD_OPTION(XINT):       // get index as integer
        switch (CMOD_OPTION_XINT_get(*opt).type) {
        case ENUM_XINT(UNSIGNED):
            index.ix = XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opt));
            if (index.ix >= TABLE_NCOLS(*tab))
                return 1;       // ERROR: out of bounds
            break;
        default:
            return 1;
            break;
        }
        break;
    case ENUM_CMOD_OPTION(STRING):     // get column index by name
        if (NULL == tab->shm_colnam) {
        }
        index.set = shm_atp_su(tab->shm_colnam, CMOD_OPTION_STRING_get(*opt), &(index).ix);
        if (!index.set)
            return 1;   // ERROR: column not found
        break;
    case ENUM_CMOD_OPTION(UNVALUED):
        return 1;
        break;  // should not happen
    }
    *out = index.ix;
    return 0;
}

int tab_find_icol_irow(const struct table tab[static 1], const char *search_key, uint64_t icol,
                       uint64_t irow_o[static 1], const struct table_get_cmod_opts *const opts,
                       int ierr[static 1], char *err[static 1])
{
    uint64_t irow = 0;
    bool row_found = false;

    if (opts->regex_bool) {     // regex
        regex_t preg;
        /* set flags */
        long cflags = REG_NOSUB | (opts->icase_bool ? REG_ICASE : 0L)
            | (opts->ere_bool ? REG_EXTENDED : 0L);
        /* compile regex */
        int ret = regcomp(&preg, search_key, cflags);
        if (ret) {
            size_t errmsg_len = regerror(ret, &preg, NULL, 0);
            char errmsg[errmsg_len];    // VLA
            regerror(ret, &preg, errmsg, errmsg_len);
            *ierr = ret;
            char *errmsg_p = rstrdup(errmsg);
            if (NULL == (errmsg_p)) {
                error0("out of memory");
            }
            ;
            *err = errmsg_p;
            return 2;   // regex error
        }
        /* search rows for regex match */
        const vec_string **buf = (const vec_string **)sv_get_buffer_r(tab->svv_rows);
        {
            (row_found) = false;
            for (size_t i = (0);
                 ((irow) = i) < (TABLE_NROWS(*tab))
                 && !((row_found) =
                      ((regexec((&preg), sv_at_ptr((buf)[i], icol), 0, NULL, 0) == 0))); ++i) ;
            (irow) = (row_found) ? (irow) : (irow) - (0);
        }

        regfree(&preg);
    }
    else {      // no regex
        if (!opts->icase_bool && tab->key.set && tab->key.ix == icol) { // hash map optimization
            uint64_t irow64 = 0;
            row_found = shm_atp_su(tab->hmap, ss_crefs(search_key), &irow64);
            irow = irow64;
        }
        else {  // search rows for value
            const vec_string **buf = (const vec_string **)sv_get_buffer_r(tab->svv_rows);
            int (*_strcmp_f)(const char *, const char *) =(opts->icase_bool) ? strcasecmp : strcmp;
            {
                (row_found) = false;
                for (size_t i = (0);
                     ((irow) = i) < (TABLE_NROWS(*tab))
                     && !((row_found) =
                          ((_strcmp_f((search_key), sv_at_ptr((buf)[i], icol)) == 0))); ++i) ;
                (irow) = (row_found) ? (irow) : (irow) - (0);
            }

        }
    }

    *irow_o = irow;

    return !row_found;  // 0 if found, 1 otherwise
}

/* string representation stuff */

srt_string *hstr_str_repr(const char *hstr, srt_string **sout)
{
    srt_string **s = NULL;
    srt_string *snew = NULL;
    if (NULL != sout)
        s = sout;
    else {
        (snew) = ss_alloc(16);
        if (ss_void == (snew)) {
            return ss_void;
        }
        ;
        s = &snew;
    }
    if (NULL != strchr(hstr, '\n'))
        ss_cat_c(s, "%" "<< ", hstr, " >>" "%");
    else
        ss_cat_c(s, "`", hstr, "`");
    goto end;
 end:;

    return *s;
}

srt_string *namarg_str_repr(const struct namarg *narg, const vec_namarg *vnarg, srt_string **sout)
{
    srt_string **s = NULL;
    srt_string *snew = NULL;
    if (NULL != sout)
        s = sout;
    else {
        (snew) = ss_alloc(16);
        if (ss_void == (snew)) {
            return ss_void;
        }
        ;
        s = &snew;
    }
    if (NULL != narg->name) {
        ss_cat_c(s, narg->name);
        if (narg->type != CMOD_ARG_NOVALUE)
            ss_cat_cn1(s, '=');
    }
    switch (narg->type) {
    case CMOD_ARG_NOVALUE:
        break;
    case CMOD_ARG_ARGLINK:
        ss_cat_cn1(s, '#');
        ss_cat_c(s, ((struct namarg *)sv_at(vnarg, CMOD_ARG_ARGLINK_get(*narg)))->name);
        break;
    case CMOD_ARG_ROWNO:
        ss_cat_c(s, "%NR");
        break;
    case CMOD_ARG_OPTIONAL:
        ss_cat_c(s, "*");
        break;
    case CMOD_ARG_VERBATIM:
        hstr_str_repr(CMOD_ARG_VERBATIM_get(*narg), s);
        break;
    case CMOD_ARG_ID:
        ss_cat_c(s, CMOD_ARG_ID_get(*narg));
        break;
    case CMOD_ARG_IDEXT:
        ss_cat_c(s, "`", CMOD_ARG_IDEXT_get(*narg), "`");
        break;
    case CMOD_ARG_INTSTR:
        ss_cat_c(s, CMOD_ARG_INTSTR_get(*narg));
        break;
    case CMOD_ARG_SPECIAL:
        ss_cat_c(s, "#", CMOD_ARG_SPECIAL_get(*narg));
        break;
    case CMOD_ARG_EXPR:
        for (size_t i = 0; i < sv_len(CMOD_ARG_EXPR_get(*narg)); ++i) {
            if (i > 0)
                ss_cat_c(s, " . ");
            namarg_str_repr(sv_at(CMOD_ARG_EXPR_get(*narg), i), vnarg, s);      // recurse
        }
        break;
    }
    goto end;
 end:;

    return *s;
}

// TODO: expressions?
srt_string *vec_namarg_str_repr(const vec_namarg *vnarg, setix first, setix last, srt_string **sout)
{
    srt_string **s = NULL;
    srt_string *snew = NULL;
    if (NULL != sout)
        s = sout;
    else {
        (snew) = ss_alloc(16);
        if (ss_void == (snew)) {
            return ss_void;
        }
        ;
        s = &snew;
    }
    size_t i_first = first.set ? first.ix : 0;
    size_t i_last = last.set ? last.ix : sv_len(vnarg);
    for (size_t i = i_first; i < i_last; ++i) {
        const struct namarg *narg = sv_at(vnarg, i);
        if (narg->type == CMOD_ARG_NOVALUE && NULL == narg->name)
            continue;
        if (i > i_first)
            ss_cat_cn1(s, ',');
        namarg_str_repr(narg, vnarg, s);
    }
    goto end;
 end:;

    return *s;
}

srt_string *dllarg_str_repr(const struct dllarg *arg, srt_string **sout)
{
    srt_string **s = NULL;
    srt_string *snew = NULL;
    if (NULL != sout)
        s = sout;
    else {
        (snew) = ss_alloc(16);
        if (ss_void == (snew)) {
            return ss_void;
        }
        ;
        s = &snew;
    }
    ss_cat_cn1(s, '$');

    int nmod = 0;
    nmod += arg->mod.tran;
    switch (arg->mod.tran) {    // str_tran
    case TRAN_NONE:
        break;
    case TRAN_EMPTY:
        ss_cat_cn1(s, 'E');
        break;
    case TRAN_CID:
        ss_cat_cn1(s, 'C');
        break;
    case TRAN_SNIPPET:
        ss_cat_cn1(s, 'e');
        break;
    case TRAN_CSTR:
        ss_cat_cn1(s, 'g');
        break;
    case TRAN_JSON:
        ss_cat_cn1(s, 'j');
        break;
    case TRAN_HEX:
        ss_cat_cn1(s, 'x');
        break;
    case TRAN_B64:
        ss_cat_cn1(s, 'X');
        break;
    case TRAN_LZH:
        ss_cat_cn1(s, 'l');
        break;
    }
    nmod += arg->mod.fold;
    switch (arg->mod.fold) {    // str_fold
    case FOLD_NONE:
        break;
    case FOLD_LOWER:
        ss_cat_cn1(s, 'L');
        break;
    case FOLD_UPPER:
        ss_cat_cn1(s, 'U');
        break;
    }
    nmod += arg->mod.pre;
    switch (arg->mod.pre) {     // str_pre
    case PRE_NONE:
        break;
    case PRE_HTAG:
        ss_cat_cn1(s, 'h');
        break;
    case PRE_BSLASH:
        ss_cat_cn1(s, 'q');
        break;
    case PRE_DOLLAR:
        ss_cat_cn1(s, 'd');
        break;
    case PRE_DOT:
        ss_cat_cn1(s, 't');
        break;
    case PRE_MOD:
        ss_cat_cn1(s, 'm');
        break;
    case PRE_ASTER:
        ss_cat_cn1(s, 'a');
        break;
    case PRE_AMPER:
        ss_cat_cn1(s, 'r');
        break;
    }
    {   // str_trim
        nmod += arg->mod.trim;
        if ((((arg->mod.trim) & (TRIM_SPACE)) == (TRIM_SPACE)))
            ss_cat_cn1(s, 'T');
        if ((((arg->mod.trim) & (TRIM_LAST)) == (TRIM_LAST)))
            ss_cat_cn1(s, '_');
        if ((((arg->mod.trim) & (TRIM_NL)) == (TRIM_NL)))
            ss_cat_cn1(s, 'n');
    }
    {   // str_wrap
        nmod += arg->mod.wrap;
        if ((((arg->mod.wrap) & (WRAP_SPACE)) == (WRAP_SPACE)))
            ss_cat_cn1(s, 'w');
        if ((((arg->mod.wrap) & (WRAP_SQUOT)) == (WRAP_SQUOT)))
            ss_cat_cn1(s, 's');
        if ((((arg->mod.wrap) & (WRAP_DQUOT)) == (WRAP_DQUOT)))
            ss_cat_cn1(s, 'S');
        if ((((arg->mod.wrap) & (WRAP_PAREN)) == (WRAP_PAREN)))
            ss_cat_cn1(s, 'P');
        if ((((arg->mod.wrap) & (WRAP_BRACK)) == (WRAP_BRACK)))
            ss_cat_cn1(s, 'K');
        if ((((arg->mod.wrap) & (WRAP_ANGLE)) == (WRAP_ANGLE)))
            ss_cat_cn1(s, 'A');
        if ((((arg->mod.wrap) & (WRAP_BRACE)) == (WRAP_BRACE)))
            ss_cat_cn1(s, 'B');
        if ((((arg->mod.wrap) & (WRAP_BTICK)) == (WRAP_BTICK)))
            ss_cat_cn1(s, 'b');
        if ((((arg->mod.wrap) & (WRAP_HSTR)) == (WRAP_HSTR)))
            ss_cat_cn1(s, 'H');
    }
    {   // str_lead
        nmod += arg->mod.lead;
        if ((((arg->mod.lead) & (LEAD_PTR)) == (LEAD_PTR)))
            ss_cat_cn1(s, 'p');
        if ((((arg->mod.lead) & (LEAD_REF)) == (LEAD_REF)))
            ss_cat_cn1(s, 'R');
        if ((((arg->mod.lead) & (LEAD_DOLLAR)) == (LEAD_DOLLAR)))
            ss_cat_cn1(s, 'D');
        if ((((arg->mod.lead) & (LEAD_NAME)) == (LEAD_NAME)))
            ss_cat_cn1(s, 'z');
        if ((((arg->mod.lead) & (LEAD_COMMA)) == (LEAD_COMMA)))
            ss_cat_cn1(s, 'c');
    }
    {   // str_trail
        nmod += arg->mod.trail;
        if ((((arg->mod.trail) & (TRAIL_COMMA)) == (TRAIL_COMMA)))
            ss_cat_cn1(s, 'Z');
        if ((((arg->mod.trail) & (TRAIL_SPACE)) == (TRAIL_SPACE)))
            ss_cat_cn1(s, 'W');
        if ((((arg->mod.trail) & (TRAIL_NL)) == (TRAIL_NL)))
            ss_cat_cn1(s, 'N');
    }

    /* unmodified positional reference */
    if (0 == nmod && DLLTYPE_NORMAL == arg->type && NULL == arg->name) {
        ss_cat_int(s, arg->num);
        goto end;
    }

    ss_cat_cn1(s, '{');
    switch (arg->type) {
    case DLLTYPE_NORMAL:
        if (NULL == arg->name)
            ss_cat_int(s, arg->num);
        else
            ss_cat_c(s, arg->name);
        break;
    case DLLTYPE_ARGNAME:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->num);
        break;
    case DLLTYPE_M:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "M");
        break;
    case DLLTYPE_B:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "B");
        break;
    case DLLTYPE_D:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "D");
        break;
    case DLLTYPE_N:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "N");
        break;
    case DLLTYPE_S:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "S");
        break;
    case DLLTYPE_T:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "T");
        break;
    case DLLTYPE_U:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "U");
        break;
    case DLLTYPE_X:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "X");
        break;
    case DLLTYPE_COMMA:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "COMMA");
        break;
    case DLLTYPE_ARGV:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "ARGV");
        break;
    case DLLTYPE_ARGV_OUT:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "ARGV_OUT");
        break;
    case DLLTYPE_ARGV_IN:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "ARGV_IN");
        break;
    case DLLTYPE_RARGV:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "RARGV");
        break;
    case DLLTYPE_RARGV_OUT:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "RARGV_OUT");
        break;
    case DLLTYPE_RARGV_IN:
        ss_cat_cn1(s, '#');
        ss_cat_int(s, arg->rep);        // repeat
        ss_cat_c(s, "RARGV_IN");
        break;
    case DLLTYPE_NAME:
        ss_cat_c(s, "#" "NAME");
        break;
    case DLLTYPE_NR:
        ss_cat_c(s, "#" "NR");
        break;
    case DLLTYPE_ARGC:
        ss_cat_c(s, "#" "ARGC");
        break;
    case DLLTYPE_ARGC_OUT:
        ss_cat_c(s, "#" "ARGC_OUT");
        break;
    case DLLTYPE_ARGC_IN:
        ss_cat_c(s, "#" "ARGC_IN");
        break;
    }
    ss_cat_cn1(s, '}');
    goto end;
 end:;

    return *s;
}

srt_string *snippet_body_str_repr(const struct snippet *snip, const vec_dllarg *sv_argl,
                                  srt_string **sout)
{
    srt_string **s = NULL;
    srt_string *snew = NULL;
    if (NULL != sout)
        s = sout;
    else {
        (snew) = ss_alloc(16);
        if (ss_void == (snew)) {
            return ss_void;
        }
        ;
        s = &snew;
    }
    const size_t ndll = sv_len(sv_argl);
    if (0 == ndll) {
        ss_cat_substr(s, snip->body, 0, ss_len(snip->body) - 1);        // exclude NUL byte
        goto end;
    }

    /* setup dummy snippet */
    struct snippet tmp = { 0 };
    if (snippet_dup(snip, &tmp)) {
        if (NULL == sout)
            ss_free(&snew);
        return ss_void;
    }

    /* setup dummy arguments */
    {
        const size_t _len = sv_len(tmp.sv_argl);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _2 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            *((struct dllarg *)sv_at(tmp.sv_argl, _2)) = (struct dllarg) {
                .name = ((struct dllarg *)sv_at(tmp.sv_argl, _2))->name,
                .pos = ((struct dllarg *)sv_at(tmp.sv_argl, _2))->pos,
                .num = _2
            };
        }
    }

    {   // evaluate snippet arguments with their own invocations
        srt_string *svals[ndll];
        const char *vals[ndll];
        {
            const size_t _len = sv_len(sv_argl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _3 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                svals[_3] = dllarg_str_repr(((const struct dllarg *)sv_at(sv_argl, _3)), NULL);
                vals[_3] = ss_to_c(svals[_3]);
            }
        }

        size_t buflen = 0;
        char *buf = snip_get_buf(&tmp, ndll, vals, &buflen);
        ss_cat_cn(s, buf, buflen - 1);  // exclude NUL byte
        free(buf);

        for (size_t i = 0; i < ndll; ++i)
            ss_free(&svals[i]);
    }

    snippet_clear(&tmp);
    goto end;
 end:;

    return *s;
}

/* iftrie stuff */
static inline void ss_cat_strcmp(srt_string **ss,       // output buffer
                                 size_t len,    // length of comparandum
                                 const char str[static len],    // comparandum
                                 const char *var,       // variable name
                                 size_t ichar,  // character offset
                                 size_t nexpand,        // expand shorter strings to single-char comparisons
                                 bool terminal) // will consume whole string
{
    if (len == 0) {
        const char *and = (terminal) ? " && " : "";

        if (ss_void == ss_cat_c(ss, and)) {
        }

        if (ss_void == ss_cat_c(ss, "'")) {
        }

        if (ss_void == ss_cat_c(ss, "\\0")) {
        }

        if (ss_void == ss_cat_c(ss, "' == *((")) {
        }

        if (ss_void == ss_cat_c(ss, var)) {
        }

        if (ss_void == ss_cat_c(ss, ") + ")) {
        }

        if (ss_void == ss_cat_int(ss, ichar)) {
        }

        if (ss_void == ss_cat_cn1(ss, ')')) {
        }

    }
    else {
        char single[4] = { '\'', 0, '\'', '\0' };
        if (!terminal) {
            single[1] = *str;

            if (ss_void == ss_cat_c(ss, single)) {
            }

            if (ss_void == ss_cat_c(ss, " == *((")) {
            }

            if (ss_void == ss_cat_c(ss, var)) {
            }

            if (ss_void == ss_cat_c(ss, ") + ")) {
            }

            if (ss_void == ss_cat_int(ss, ichar)) {
            }

            if (ss_void == ss_cat_cn1(ss, ')')) {
            }

            ++str;
            ++ichar;
            --len;
        }
        if (len > 1 && len >= nexpand) {

            if (ss_void == ss_cat_c(ss, " && strncmp((")) {
            }

            if (ss_void == ss_cat_c(ss, var)) {
            }

            if (ss_void == ss_cat_c(ss, ") + ")) {
            }

            if (ss_void == ss_cat_int(ss, ichar)) {
            }

            if (ss_void == ss_cat_c(ss, ",\"")) {
            }

            if (ss_void == ss_cat_cn(ss, str, len)) {
            }

            if (ss_void == ss_cat_c(ss, "\",")) {
            }

            if (ss_void == ss_cat_int(ss, len + terminal)) {
            }

            if (ss_void == ss_cat_c(ss, ") == 0")) {
            }

        }
        else {
            for (size_t i = 0; i < len; ++i, ++ichar, ++str) {
                single[1] = *str;

                if (ss_void == ss_cat_c(ss, " && ")) {
                }

                if (ss_void == ss_cat_c(ss, single)) {
                }

                if (ss_void == ss_cat_c(ss, " == *((")) {
                }

                if (ss_void == ss_cat_c(ss, var)) {
                }

                if (ss_void == ss_cat_c(ss, ") + ")) {
                }

                if (ss_void == ss_cat_int(ss, ichar)) {
                }

                if (ss_void == ss_cat_cn1(ss, ')')) {
                }

            }
            if (terminal) {

                if (ss_void == ss_cat_c(ss, " && '")) {
                }

                if (ss_void == ss_cat_c(ss, "\\0")) {
                }

                if (ss_void == ss_cat_c(ss, "' == *((")) {
                }

                if (ss_void == ss_cat_c(ss, var)) {
                }

                if (ss_void == ss_cat_c(ss, ") + ")) {
                }

                if (ss_void == ss_cat_int(ss, ichar)) {
                }

                if (ss_void == ss_cat_cn1(ss, ')')) {
                }

            }
        }
    }
}

// TODO: UTF-8 version?
int ss_cat_iftrie_ascii_aux(srt_string **ss, size_t iter, size_t ichar, const vec_u64 *class,
                            const struct iftrie_params *ifp)
{
    int rc = 0; // return code

    /* collect strings based on first char */
    vec_u64 *fcharix[96] = { 0 };
    if (iter == 0) {
        for (uint64_t ix = 0; ix < ifp->arrlen; ++ix) {
            const char *str = ifp->words[ix];
            int c = (unsigned char)str[ichar];
            vec_u64 **v = &fcharix[ifp->c2ix[c]];
            if (NULL == *v) {
                (*v) = sv_alloc_t(SV_U64, 0);
                if (sv_void == (*v)) {
                    rc = 1;
                    goto cleanup;
                }
            }
            if (!sv_push_u64(v, ix)) {
                rc = 1;
                goto cleanup;
            }

        }
    }
    else {
        for (size_t i = 0; i < sv_len(class); ++i) {
            uint64_t ix = sv_at_u64(class, i);
            const char *str = ifp->words[ix];
            int c = (unsigned char)str[ichar];
            vec_u64 **v = &fcharix[ifp->c2ix[c]];
            if (NULL == *v) {
                (*v) = sv_alloc_t(SV_U64, 0);
                if (sv_void == (*v)) {
                    rc = 1;
                    goto cleanup;
                }
            }
            if (!sv_push_u64(v, ix)) {
                rc = 1;
                goto cleanup;
            }

        }
    }

    vec_u64 *(inuse) = sv_alloc_t(SV_U64, 0);
    if (sv_void == (inuse)) {
        rc = 1;
        goto cleanup;
    }
    ;
    vec_u64 *(plens) = sv_alloc_t(SV_U64, 0);
    if (sv_void == (plens)) {
        rc = 1;
        goto cleanup;
    }
    ;
    /* compute longest common prefixes for non-empty classes */
    {
        const size_t _len = (sizeof(fcharix) / sizeof(*(fcharix)));
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _4 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (sv_len((((fcharix))[_4])) == 0)
                continue;
            uint64_t plen = 0;  // prefix length
            const char *const lcp = ifp->words[sv_at_u64((((fcharix))[_4]), 0)] + ichar;
            if ('\0' == *lcp) ; // empty string, do nothing
            else if (sv_len((((fcharix))[_4])) == 1)
                plen = 1;       // single string in class
            else {      // multiple strings
                plen = strlen(lcp);
                for (size_t ix = 1; ix < sv_len((((fcharix))[_4])); ++ix) {     // find common prefix
                    uint64_t len = 0;
                    for (const char *p = lcp, *c =
                         ifp->words[sv_at_u64((((fcharix))[_4]), ix)] + ichar;
                         '\0' != *p && *p == *c; ++c, ++p, ++len) ;
                    if (len < plen)
                        plen = len;
                }
            }
            if (!sv_push_u64(&(inuse), _4)) {
                rc = 1;
                goto cleanup;
            }

            if (!sv_push_u64(&(plens), plen)) {
                rc = 1;
                goto cleanup;
            }

        }
    }
    /* sort classes */
    size_t *(order) = rmalloc((sv_len(inuse)) * sizeof(*order));
    if (NULL == (order)) {
        rc = 1;
        goto cleanup;
    }
    ;
    for (size_t i = 0; i < sv_len(inuse); ++i)
        order[i] = i;
    if (ifp->sort_by_size) {    // larger classes first
        for (size_t i = (0) + 1 + (0), j; i < (sv_len(inuse)); i += (1)) {
            size_t t = (order)[i];
            for (j = i;
                 j >= (0) + (1)
                 && (sv_len(fcharix[sv_at_u64(inuse, (order)[j - (1)])]) <
                     sv_len(fcharix[sv_at_u64(inuse, t)])); j -= (1))
                (order)[j] = (order)[j - (1)];
            (order)[j] = t;
        }
    }
    else {      // shorter prefixes first
        for (size_t i = (0) + 1 + (0), j; i < (sv_len(inuse)); i += (1)) {
            size_t t = (order)[i];
            for (j = i;
                 j >= (0) + (1) && (sv_at_u64(plens, (order)[j - (1)]) > sv_at_u64(plens, t));
                 j -= (1))
                (order)[j] = (order)[j - (1)];
            (order)[j] = t;
        }
    }
    /* print comparisons */
    {
        const size_t _len = sv_len(inuse);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _5 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            vec_u64 *class = fcharix[sv_at_u64(inuse, (((order))[_5]))];        // string class
            const char *lcp = ifp->words[sv_at_u64(class, 0)] + ichar;  // first word in class
            const uint64_t plen = sv_at_u64(plens, (((order))[_5]));    // longest common prefix length

            /* print conditional */
            ss_cat_c(ss, (_5 == 0) ? "if(" : "else if(");
            ss_cat_strcmp(ss, plen, lcp, ifp->var, ichar, ifp->nexpand, false);
            if (plen > 0 && sv_len(class) == 1 && !ifp->fuzzy) {        // terminate early
                ++lcp;
                ss_cat_strcmp(ss, strlen(lcp), lcp, ifp->var, ichar + 1, ifp->nexpand, true);
            }
            ss_cat_c(ss, ") {\n");
            /* print action */
            if (plen == 0 || sv_len(class) == 1) {
                const char *action = ifp->actions[sv_at_u64(class, 0)];
                if (NULL != action)
                    ss_cat_c(ss, action);
                ss_cat_cn1(ss, '\n');
            }
            else if ((rc = ss_cat_iftrie_ascii_aux(ss, iter + 1, ichar + plen, class, ifp))) {  // recurse
                goto cleanup;   // return from the deep
            }
            ss_cat_c(ss, "}\n");

            sv_free(&class);
        }
    }
    sv_free(&inuse);
    sv_free(&plens);
    free(order);

    ss_cat_c(ss, "else {\n", "goto ", ifp->label, ";\n");       // use label
    if (iter == 0) {
        ss_cat_c(ss, ifp->label, ": ;\n");      // no-op
        if (NULL != ifp->action_else && strlen(ifp->action_else) > 0)
            ss_cat_c(ss, ifp->action_else, "\n");
    }
    ss_cat_c(ss, "}\n");

 cleanup:

    return rc;
}
