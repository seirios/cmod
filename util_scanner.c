
#include <stdio.h>

#ifndef info0
#define info0(M) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include <stdarg.h>
#include <assert.h>

#ifndef CMOD_UTIL_SCANNER_H
#define CMOD_UTIL_SCANNER_H
#include "cmod.ext.h"
void workspace_clear(struct param *x);
void yyset_start(int, void *);
#endif
#include "parser.h"
#include "scanner.h"

/* params stuff */
void workspace_clear(struct param *x)
{
    {
        for (size_t i = 0; i < sv_len(x->sv_dllarg); ++i) {
            const void *_item = sv_at(x->sv_dllarg, i);
            dllarg_clear((struct dllarg *)(_item));
        }
        sv_free(&(x->sv_dllarg));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_idlist); ++i) {
            const void *_item = sv_at(x->sv_idlist, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_idlist));
    }
    shm_free(&x->shm_dup);
    x->kwname = NULL; {
        for (size_t i = 0; i < sv_len(x->param_types); ++i) {
            const void *_item = sv_at(x->param_types, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->param_types));
    }
    {
        for (size_t i = 0; i < sv_len(x->param_pntrs); ++i) {
            const void *_item = sv_at(x->param_pntrs, i);
            {
                vec_string *vec = *(vec_string **) (_item);
                {
                    for (size_t i = 0; i < sv_len(vec); ++i) {
                        const void *_item = sv_at(vec, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(vec));
                }
            }
        }
        sv_free(&(x->param_pntrs));
    }
    {
        for (size_t i = 0; i < sv_len(x->param_names); ++i) {
            const void *_item = sv_at(x->param_names, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->param_names));
    }
    {
        for (size_t i = 0; i < sv_len(x->param_inits); ++i) {
            const void *_item = sv_at(x->param_inits, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->param_inits));
    }
    ss_clear(x->kw_buf);
    ss_clear(x->hstr_buf);
    free(x->hstr_id);
    x->hstr_id = NULL;
    x->kwlloc = (YYLTYPE) {
    1, 1, 1, 1};
    x->idup = SETIX_none;
    x->count = 0;
    x->nout = 0;
    x->brc_lvl = 0;
    x->par_lvl = 0;
    x->sqr_lvl = 0;
    x->exit_on = 0;
    x->mb_start = 0;
    x->opt_start = 0;
    x->hstr_start = 0;
    x->hstr_exit_on = 0;
    x->ccode_start = 0;
    x->ccode_exit_on = 0;
    x->ccode_force_exit_on = 0;
    x->cmp_base = 0;
    x->blockcomm = 0;
    x->nlcomm = 0;
    x->dllspecial = 0;
    x->ignore_enum_constants = 0;
    x->redef_nowarn = 0;
    x->is_shorthand = 0;
}

void yyinfo(YYLTYPE *yylloc, yyscan_t yyscanner, const struct param *pp, const char *fmt, ...)
{
    pp = yyget_extra(yyscanner);

    int lineno = 1;
    int colno = 1;
    if (pp->in_parse) { // yylloc exists inside yyparse
        yylloc = yyget_lloc(yyscanner);
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr, "%s[%s] %5s: %s:%d:%d: ",
                  pp->pretty ? "\x1B[39m" : "",
                  "C%", "INFO", pp->namein ? pp->namein : "<none>", lineno, colno);
    va_list ap;
    va_start(ap, fmt);
    (void)vfprintf(stderr, fmt, ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n", stderr);
}

void yywarn(YYLTYPE *yylloc, yyscan_t yyscanner, const struct param *pp, const char *fmt, ...)
{
    pp = yyget_extra(yyscanner);

    int lineno = 1;
    int colno = 1;
    if (pp->in_parse) { // yylloc exists inside yyparse
        yylloc = yyget_lloc(yyscanner);
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr, "%s[%s] %5s: %s:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;11m" : "",
                  "C%", "WARN", pp->namein ? pp->namein : "<none>", lineno, colno);
    va_list ap;
    va_start(ap, fmt);
    (void)vfprintf(stderr, fmt, ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n", stderr);
}

void yyerror(YYLTYPE *yylloc, yyscan_t yyscanner, const struct param *pp, const char *fmt, ...)
{
    pp = yyget_extra(yyscanner);

    int lineno = 1;
    int colno = 1;
    if (pp->in_parse) { // yylloc exists inside yyparse
        yylloc = yyget_lloc(yyscanner);
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr, "%s[%s] %5s: %s:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;9m" : "",
                  "C%", "ERROR", pp->namein ? pp->namein : "<none>", lineno, colno);
    va_list ap;
    va_start(ap, fmt);
    (void)vfprintf(stderr, fmt, ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n", stderr);
}

void yyverbose(YYLTYPE *yylloc, yyscan_t yyscanner, const struct param *pp, const char *fmt, ...)
{
    pp = yyget_extra(yyscanner);

    int lineno = 1;
    int colno = 1;
    if (pp->in_parse) { // yylloc exists inside yyparse
        yylloc = yyget_lloc(yyscanner);
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr, "%s[%s] %5s: %s:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;12m" : "",
                  "C%", "VERB", pp->namein ? pp->namein : "<none>", lineno, colno);
    va_list ap;
    va_start(ap, fmt);
    (void)vfprintf(stderr, fmt, ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n", stderr);
}

#undef LOGGING_FMT
