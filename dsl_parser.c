/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* "%code top" blocks.  */
#line 101 "dsl_parser.y"

#include <signal.h>
#include <stdbool.h>

#include "dsl.ext.h"
#include "dsl_parser.h"
#include "dsl_scanner.h"
#include "util_snippet.h"
#include "ralloc.h"

/* logging macros */
#ifndef INFO0
#define INFO0(x) \
    { if(!(pp->silent > 0))\
        dsl_info(dsl_get_lloc(yyscanner),yyscanner,pp,x); }
#endif
#ifndef INFO
#define INFO(x, ...) \
    { if(!(pp->silent > 0))\
        dsl_info(dsl_get_lloc(yyscanner),yyscanner,pp,x,__VA_ARGS__); }
#endif
#ifndef WARN0
#define WARN0(x) \
    { if(!(pp->silent > 1))\
        dsl_warn(dsl_get_lloc(yyscanner),yyscanner,pp,x); }
#endif
#ifndef WARN
#define WARN(x, ...) \
    { if(!(pp->silent > 1))\
        dsl_warn(dsl_get_lloc(yyscanner),yyscanner,pp,x,__VA_ARGS__); }
#endif
#ifndef ERROR0
#define ERROR0(x) \
    { if(!(pp->silent > 2))\
        dsl_error(dsl_get_lloc(yyscanner),yyscanner,pp,x); }
#endif
#ifndef ERROR
#define ERROR(x, ...) \
    { if(!(pp->silent > 2))\
        dsl_error(dsl_get_lloc(yyscanner),yyscanner,pp,x,__VA_ARGS__); }
#endif
#ifndef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!pp->verbose))\
        dsl_verbose(dsl_get_lloc(yyscanner),yyscanner,pp,x); }
#endif
#ifndef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!pp->verbose))\
        dsl_verbose(dsl_get_lloc(yyscanner),yyscanner,pp,x,__VA_ARGS__); }
#endif
#ifndef DEBUG0
#define DEBUG0(x) \
    { if(!(pp->silent > 3))\
        dsl_debug(dsl_get_lloc(yyscanner),yyscanner,pp,x); }
#endif
#ifndef DEBUG
#define DEBUG(x, ...) \
    { if(!(pp->silent > 3))\
        dsl_debug(dsl_get_lloc(yyscanner),yyscanner,pp,x,__VA_ARGS__); }
#endif

#ifndef ABORT_PARSE
#define ABORT_PARSE(x, ...) { \
    ERROR(x,__VA_ARGS__); \
if(!(pp->silent > 2)) { \
    struct dsl_param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif
#ifndef ABORT_PARSE0
#define ABORT_PARSE0(x) { \
    ERROR0(x); \
if(!(pp->silent > 2)) { \
    struct dsl_param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif

static void print_error(YYLTYPE*, void*);

#line 154 "dsl_parser.c"
/* Substitute the type names.  */
#define YYSTYPE         DSL_STYPE
#define YYLTYPE         DSL_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         dsl_parse
#define yylex           dsl_lex
#define yyerror         dsl_error
#define yydebug         dsl_debug
#define yynerrs         dsl_nerrs


# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "dsl_parser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_SCANNER_ERROR = 3,              /* SCANNER_ERROR  */
  YYSYMBOL_HERESTR_OPEN = 4,               /* HERESTR_OPEN  */
  YYSYMBOL_HERESTR_CLOSE = 5,              /* HERESTR_CLOSE  */
  YYSYMBOL_STMT_SEP = 6,                   /* STMT_SEP  */
  YYSYMBOL_CHAR = 7,                       /* CHAR  */
  YYSYMBOL_IDENTIFIER = 8,                 /* IDENTIFIER  */
  YYSYMBOL_IDENTIFIER_EXT = 9,             /* IDENTIFIER_EXT  */
  YYSYMBOL_STRING_LITERAL = 10,            /* STRING_LITERAL  */
  YYSYMBOL_INT_NUMBER = 11,                /* INT_NUMBER  */
  YYSYMBOL_FP_NUMBER = 12,                 /* FP_NUMBER  */
  YYSYMBOL_TREE_LEVEL = 13,                /* TREE_LEVEL  */
  YYSYMBOL_OP_QUOTE = 14,                  /* OP_QUOTE  */
  YYSYMBOL_OP_NAMESPACE = 15,              /* OP_NAMESPACE  */
  YYSYMBOL_OP_SEPARATOR = 16,              /* OP_SEPARATOR  */
  YYSYMBOL_OP_CONTAINER_OPEN = 17,         /* OP_CONTAINER_OPEN  */
  YYSYMBOL_OP_CONTAINER_CLOSE = 18,        /* OP_CONTAINER_CLOSE  */
  YYSYMBOL_OP_FUNCTION = 19,               /* OP_FUNCTION  */
  YYSYMBOL_OP_INDEX_OPEN = 20,             /* OP_INDEX_OPEN  */
  YYSYMBOL_OP_INDEX_CLOSE = 21,            /* OP_INDEX_CLOSE  */
  YYSYMBOL_OP_POSTFIX = 22,                /* OP_POSTFIX  */
  YYSYMBOL_OP_CHAIN_LEFT = 23,             /* OP_CHAIN_LEFT  */
  YYSYMBOL_OP_CHAIN_RIGHT = 24,            /* OP_CHAIN_RIGHT  */
  YYSYMBOL_OP_UNARY = 25,                  /* OP_UNARY  */
  YYSYMBOL_OP_MULTIPLICATIVE = 26,         /* OP_MULTIPLICATIVE  */
  YYSYMBOL_OP_ADDITIVE = 27,               /* OP_ADDITIVE  */
  YYSYMBOL_OP_SHIFT = 28,                  /* OP_SHIFT  */
  YYSYMBOL_OP_RELATIONAL = 29,             /* OP_RELATIONAL  */
  YYSYMBOL_OP_EQUALITY = 30,               /* OP_EQUALITY  */
  YYSYMBOL_OP_BITWISE_AND = 31,            /* OP_BITWISE_AND  */
  YYSYMBOL_OP_BITWISE_XOR = 32,            /* OP_BITWISE_XOR  */
  YYSYMBOL_OP_BITWISE_OR = 33,             /* OP_BITWISE_OR  */
  YYSYMBOL_OP_LOGICAL_AND = 34,            /* OP_LOGICAL_AND  */
  YYSYMBOL_OP_LOGICAL_OR = 35,             /* OP_LOGICAL_OR  */
  YYSYMBOL_OP_ASSIGNMENT = 36,             /* OP_ASSIGNMENT  */
  YYSYMBOL_OP_END = 37,                    /* OP_END  */
  YYSYMBOL_OP_BLOCK_OPEN = 38,             /* OP_BLOCK_OPEN  */
  YYSYMBOL_OP_BLOCK_CLOSE = 39,            /* OP_BLOCK_CLOSE  */
  YYSYMBOL_OP_COMMENT_EOL = 40,            /* OP_COMMENT_EOL  */
  YYSYMBOL_OP_COMMENT = 41,                /* OP_COMMENT  */
  YYSYMBOL_42_n_ = 42,                     /* '\n'  */
  YYSYMBOL_43_ = 43,                       /* ';'  */
  YYSYMBOL_44_ = 44,                       /* '('  */
  YYSYMBOL_45_ = 45,                       /* ')'  */
  YYSYMBOL_46_ = 46,                       /* ','  */
  YYSYMBOL_YYACCEPT = 47,                  /* $accept  */
  YYSYMBOL_inline_dsl = 48,                /* inline_dsl  */
  YYSYMBOL_dsl_code = 49,                  /* dsl_code  */
  YYSYMBOL_cexpr_stmt_list = 50,           /* cexpr_stmt_list  */
  YYSYMBOL_cexpr_code = 51,                /* cexpr_code  */
  YYSYMBOL_custom_stmt_list = 52,          /* custom_stmt_list  */
  YYSYMBOL_custom_code = 53,               /* custom_code  */
  YYSYMBOL_asm_stmt_list = 54,             /* asm_stmt_list  */
  YYSYMBOL_asm_code = 55,                  /* asm_code  */
  YYSYMBOL_sexpr_stmt_list = 56,           /* sexpr_stmt_list  */
  YYSYMBOL_sexpr_code = 57,                /* sexpr_code  */
  YYSYMBOL_tree_stmt_list = 58,            /* tree_stmt_list  */
  YYSYMBOL_tree_code = 59,                 /* tree_code  */
  YYSYMBOL_tree_stmt = 60,                 /* tree_stmt  */
  YYSYMBOL_toplevel_call_tree = 61,        /* toplevel_call_tree  */
  YYSYMBOL_custom_compound_stmt_list = 62, /* custom_compound_stmt_list  */
  YYSYMBOL_custom_compound_stmt = 63,      /* custom_compound_stmt  */
  YYSYMBOL_custom_stmt = 64,               /* custom_stmt  */
  YYSYMBOL_cexpr_compound_stmt_list = 65,  /* cexpr_compound_stmt_list  */
  YYSYMBOL_cexpr_compound_stmt = 66,       /* cexpr_compound_stmt  */
  YYSYMBOL_cexpr_stmt = 67,                /* cexpr_stmt  */
  YYSYMBOL_quotable = 68,                  /* quotable  */
  YYSYMBOL_quoted_expression = 69,         /* quoted_expression  */
  YYSYMBOL_namespace_expression = 70,      /* namespace_expression  */
  YYSYMBOL_container_expression = 71,      /* container_expression  */
  YYSYMBOL_primary_expression = 72,        /* primary_expression  */
  YYSYMBOL_callable = 73,                  /* callable  */
  YYSYMBOL_function_call = 74,             /* function_call  */
  YYSYMBOL_postfix_expression = 75,        /* postfix_expression  */
  YYSYMBOL_index_expression = 76,          /* index_expression  */
  YYSYMBOL_chain_expression = 77,          /* chain_expression  */
  YYSYMBOL_unary_expression = 78,          /* unary_expression  */
  YYSYMBOL_multiplicative_expression = 79, /* multiplicative_expression  */
  YYSYMBOL_additive_expression = 80,       /* additive_expression  */
  YYSYMBOL_shift_expression = 81,          /* shift_expression  */
  YYSYMBOL_relational_expression = 82,     /* relational_expression  */
  YYSYMBOL_equality_expression = 83,       /* equality_expression  */
  YYSYMBOL_and_expression = 84,            /* and_expression  */
  YYSYMBOL_xor_expression = 85,            /* xor_expression  */
  YYSYMBOL_or_expression = 86,             /* or_expression  */
  YYSYMBOL_logical_and_expression = 87,    /* logical_and_expression  */
  YYSYMBOL_logical_or_expression = 88,     /* logical_or_expression  */
  YYSYMBOL_assignment_expression = 89,     /* assignment_expression  */
  YYSYMBOL_expression_separator_list = 90, /* expression_separator_list  */
  YYSYMBOL_opt_expression_separator_list = 91, /* opt_expression_separator_list  */
  YYSYMBOL_expression = 92,                /* expression  */
  YYSYMBOL_sexpr_stmt = 93,                /* sexpr_stmt  */
  YYSYMBOL_sexpr = 94,                     /* sexpr  */
  YYSYMBOL_sexpr_argument_list = 95,       /* sexpr_argument_list  */
  YYSYMBOL_opt_sexpr_argument_list = 96,   /* opt_sexpr_argument_list  */
  YYSYMBOL_sexpr_argument = 97,            /* sexpr_argument  */
  YYSYMBOL_asm_stmt = 98,                  /* asm_stmt  */
  YYSYMBOL_asm_argument_comma_list = 99,   /* asm_argument_comma_list  */
  YYSYMBOL_opt_asm_argument_comma_list = 100, /* opt_asm_argument_comma_list  */
  YYSYMBOL_asm_argument = 101,             /* asm_argument  */
  YYSYMBOL_literal = 102,                  /* literal  */
  YYSYMBOL_herestring = 103,               /* herestring  */
  YYSYMBOL_verbatim = 104,                 /* verbatim  */
  YYSYMBOL_verbatim_char_list = 105,       /* verbatim_char_list  */
  YYSYMBOL_number = 106,                   /* number  */
  YYSYMBOL_identifier_ext = 107,           /* identifier_ext  */
  YYSYMBOL_identifier = 108                /* identifier  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined DSL_LTYPE_IS_TRIVIAL && DSL_LTYPE_IS_TRIVIAL \
             && defined DSL_STYPE_IS_TRIVIAL && DSL_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  85
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   198

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  47
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  62
/* YYNRULES -- Number of rules.  */
#define YYNRULES  120
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  164

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   296


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      42,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      44,    45,     2,     2,    46,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    43,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41
};

#if DSL_DEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   333,   333,   334,   335,   339,   340,   341,   342,   343,
     347,   351,   357,   358,   361,   365,   371,   372,   375,   379,
     385,   388,   392,   398,   401,   405,   411,   415,   425,   436,
     460,   474,   506,   510,   516,   527,   532,   536,   542,   553,
     557,   570,   571,   575,   582,   588,   589,   599,   615,   616,
     617,   618,   622,   662,   678,   679,   683,   684,   688,   701,
     702,   720,   738,   748,   760,   761,   768,   769,   778,   779,
     788,   789,   798,   799,   808,   809,   818,   819,   828,   829,
     838,   839,   848,   849,   858,   859,   868,   869,   878,   882,
     887,   891,   895,   898,   908,   912,   928,   932,   938,   942,
     945,   946,   947,   952,   968,   972,   980,   984,   987,   988,
     992,   993,  1009,  1013,  1017,  1020,  1021,  1025,  1026,  1030,
    1034
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  "end of file", "error", "invalid token", "SCANNER_ERROR",
  "HERESTR_OPEN", "HERESTR_CLOSE", "STMT_SEP", "CHAR", "IDENTIFIER",
  "IDENTIFIER_EXT", "STRING_LITERAL", "INT_NUMBER", "FP_NUMBER",
  "TREE_LEVEL", "OP_QUOTE", "OP_NAMESPACE", "OP_SEPARATOR",
  "OP_CONTAINER_OPEN", "OP_CONTAINER_CLOSE", "OP_FUNCTION",
  "OP_INDEX_OPEN", "OP_INDEX_CLOSE", "OP_POSTFIX", "OP_CHAIN_LEFT",
  "OP_CHAIN_RIGHT", "OP_UNARY", "OP_MULTIPLICATIVE", "OP_ADDITIVE",
  "OP_SHIFT", "OP_RELATIONAL", "OP_EQUALITY", "OP_BITWISE_AND",
  "OP_BITWISE_XOR", "OP_BITWISE_OR", "OP_LOGICAL_AND", "OP_LOGICAL_OR",
  "OP_ASSIGNMENT", "OP_END", "OP_BLOCK_OPEN", "OP_BLOCK_CLOSE",
  "OP_COMMENT_EOL", "OP_COMMENT", "'\\n'", "';'", "'('", "')'", "','",
  "$accept", "inline_dsl", "dsl_code", "cexpr_stmt_list", "cexpr_code",
  "custom_stmt_list", "custom_code", "asm_stmt_list", "asm_code",
  "sexpr_stmt_list", "sexpr_code", "tree_stmt_list", "tree_code",
  "tree_stmt", "toplevel_call_tree", "custom_compound_stmt_list",
  "custom_compound_stmt", "custom_stmt", "cexpr_compound_stmt_list",
  "cexpr_compound_stmt", "cexpr_stmt", "quotable", "quoted_expression",
  "namespace_expression", "container_expression", "primary_expression",
  "callable", "function_call", "postfix_expression", "index_expression",
  "chain_expression", "unary_expression", "multiplicative_expression",
  "additive_expression", "shift_expression", "relational_expression",
  "equality_expression", "and_expression", "xor_expression",
  "or_expression", "logical_and_expression", "logical_or_expression",
  "assignment_expression", "expression_separator_list",
  "opt_expression_separator_list", "expression", "sexpr_stmt", "sexpr",
  "sexpr_argument_list", "opt_sexpr_argument_list", "sexpr_argument",
  "asm_stmt", "asm_argument_comma_list", "opt_asm_argument_comma_list",
  "asm_argument", "literal", "herestring", "verbatim",
  "verbatim_char_list", "number", "identifier_ext", "identifier", YY_NULLPTR
  };
  return yy_sname[yysymbol];
}
#endif

#define YYPACT_NINF (-80)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-53)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      17,     9,    13,   -80,    35,   -80,   -80,   -80,   158,   134,
     134,   134,    15,    22,   -80,   134,   -80,   134,   -80,    21,
     -80,     1,   -80,    39,   -80,   -80,    19,    11,   -80,   -80,
      25,   -80,   -80,   -80,    38,   -80,    20,    50,   -80,    -5,
     -80,    12,    33,    48,    49,    59,    66,    67,    47,    68,
      65,    69,    82,   -80,   -27,   -80,   -80,   -80,    60,   -80,
     -80,   -80,   -80,   -80,    96,    97,   -80,   -80,    73,   100,
     -80,   -80,   -80,   107,   -80,   -80,   -80,   106,   109,   -80,
     -80,   -80,    54,   122,    29,   -80,   -80,    81,   -80,    88,
      71,   -80,   -80,    87,   -80,    60,    77,   134,   -80,   134,
     -80,   127,   -80,   134,   -80,   146,   146,   134,   134,   134,
     134,   134,   134,   134,   134,   134,   134,   134,   -80,   -80,
     -80,   -80,   -80,    71,   -80,   -80,   134,   -80,   -80,   -80,
     -80,    29,    92,   -80,   -80,   -80,    98,    99,   -80,   128,
     -80,    50,   -80,    50,   -80,   -80,   -80,   -80,    48,    49,
      59,    66,    67,    47,    68,    65,    69,   -80,   -80,   -80,
     -80,   -80,   -80,   -80
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     3,   113,   120,   106,    44,   117,   118,   113,    91,
       0,     0,     0,     0,     2,    12,     7,    16,     8,    20,
       5,    23,     6,    26,     9,    24,    27,    17,    32,    14,
      13,    36,    10,    50,    49,    51,    54,     0,    56,    59,
      57,    64,    66,    68,    70,    72,    74,    76,    78,    80,
      82,    84,    86,    93,     0,    21,    94,    18,    48,   111,
     110,    45,     4,   115,     0,   114,   119,    29,   107,     0,
     104,   108,   109,     0,    41,    40,    42,    92,     0,    88,
      48,    65,     0,     0,    98,     1,    11,     0,    15,     0,
     106,    19,    22,     0,    25,     0,     0,     0,    33,     0,
      37,     0,    53,    91,    55,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    35,    39,
      28,   112,   116,     0,   103,    43,    90,    47,    38,    34,
     100,    99,     0,    96,   101,   102,     0,     0,    46,     0,
      52,    60,    62,    61,    63,    87,    67,    66,    69,    71,
      73,    75,    77,    79,    81,    83,    85,   105,    89,    97,
      95,    31,    30,    58
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -80,   -80,   -80,   143,   -80,   152,   -80,   -80,   -80,   -80,
     -80,   -80,   -80,   130,   -80,   -80,   137,   -11,   -80,   125,
     -12,   -80,   -80,   -80,   -23,   -49,   -46,   -14,   -80,   -80,
     -80,    -1,    58,    61,    57,    62,    63,    64,    70,    56,
      72,   -80,    74,   -80,    76,     2,   154,   -79,   -80,   -80,
      42,   161,   -80,   -80,    75,     0,   -80,   169,   -80,   -80,
     -77,    -7
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,    13,    14,    82,    16,    83,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    73,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    77,    78,    87,    55,    56,   131,   132,
     133,    57,    68,    69,    70,    80,    59,    64,    65,    60,
      72,    61
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      58,    76,    54,    86,    71,   130,    88,   135,    74,    81,
     118,    79,    62,    54,   102,   103,   119,   104,     1,    89,
      63,     2,    85,    95,    84,     3,     4,     5,     6,     7,
      90,     8,    96,     2,     9,   105,   106,   -52,    66,     2,
       6,     7,    10,     2,    66,    12,     6,     7,    93,    97,
       6,     7,   130,   101,   135,    11,   140,   140,     2,   141,
     143,    12,     3,    99,     5,     6,     7,     9,     8,   107,
      86,     9,    88,    12,   108,     2,   109,    67,   113,    10,
      66,     2,     6,     7,   134,    89,   136,   110,     6,     7,
      71,   142,   144,   128,   138,   111,   137,   112,   115,    89,
     114,   121,   120,   116,   122,    79,   124,   146,   147,   147,
     147,   147,   147,   147,   147,   147,   147,   117,   102,   123,
     102,   125,   126,    71,   119,   118,     2,   127,   158,    67,
       3,   134,     5,     6,     7,     3,     8,   160,     2,     9,
     161,   162,     3,    15,     5,     6,     7,    10,     8,   163,
       2,     9,    17,    94,     3,   100,     5,     6,     7,    10,
       8,   129,     2,     9,    98,    63,     3,   148,   150,     6,
       7,   149,   155,   159,   151,    92,   152,    75,   153,   139,
      91,   145,     0,     0,     0,   154,     0,     0,     0,   156,
       0,     0,     0,     0,     0,     0,     0,     0,   157
};

static const yytype_int16 yycheck[] =
{
       0,     8,     0,    15,     4,    84,    17,    84,     8,    10,
      37,     9,     3,    11,    37,    20,    43,    22,     1,    17,
       7,     4,     0,    23,     9,     8,     9,    10,    11,    12,
       9,    14,    13,     4,    17,    23,    24,    17,     9,     4,
      11,    12,    25,     4,     9,    44,    11,    12,     9,    38,
      11,    12,   131,    15,   131,    38,   105,   106,     4,   105,
     106,    44,     8,    38,    10,    11,    12,    17,    14,    36,
      82,    17,    83,    44,    26,     4,    27,    42,    31,    25,
       9,     4,    11,    12,    84,    83,     9,    28,    11,    12,
      90,   105,   106,    39,   101,    29,    96,    30,    33,    97,
      32,     5,    42,    34,     7,   103,     6,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,    35,   141,    46,
     143,    14,    16,   123,    43,    37,     4,    18,   126,    42,
       8,   131,    10,    11,    12,     8,    14,    45,     4,    17,
      42,    42,     8,     0,    10,    11,    12,    25,    14,    21,
       4,    17,     0,    23,     8,    30,    10,    11,    12,    25,
      14,    39,     4,    17,    27,     7,     8,   109,   111,    11,
      12,   110,   116,   131,   112,    21,   113,     8,   114,   103,
      19,   107,    -1,    -1,    -1,   115,    -1,    -1,    -1,   117,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   123
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     1,     4,     8,     9,    10,    11,    12,    14,    17,
      25,    38,    44,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    92,    93,    94,    98,   102,   103,
     106,   108,     3,     7,   104,   105,     9,    42,    99,   100,
     101,   102,   107,    68,   102,   104,   108,    90,    91,    92,
     102,    78,    50,    52,     9,     0,    67,    92,    64,    92,
       9,    98,    93,     9,    60,   102,    13,    38,    63,    38,
      66,    15,    71,    20,    22,    23,    24,    36,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    37,    43,
      42,     5,     7,    46,     6,    14,    16,    18,    39,    39,
      94,    95,    96,    97,   102,   107,     9,   102,   108,    91,
      72,    73,    74,    73,    74,    89,    78,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,   101,    92,    97,
      45,    42,    42,    21
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    47,    48,    48,    48,    49,    49,    49,    49,    49,
      50,    50,    51,    51,    52,    52,    53,    53,    54,    54,
      55,    56,    56,    57,    58,    58,    59,    60,    61,    61,
      61,    61,    62,    62,    63,    64,    65,    65,    66,    67,
      68,    68,    68,    69,    69,    70,    70,    71,    72,    72,
      72,    72,    73,    74,    75,    75,    75,    75,    76,    77,
      77,    77,    77,    77,    78,    78,    79,    79,    80,    80,
      81,    81,    82,    82,    83,    83,    84,    84,    85,    85,
      86,    86,    87,    87,    88,    88,    89,    89,    90,    90,
      90,    91,    91,    92,    93,    94,    95,    95,    96,    96,
      97,    97,    97,    98,    99,    99,   100,   100,   101,   101,
     102,   102,   103,   104,   104,   105,   105,   106,   106,   107,
     108
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     2,     1,     1,     1,     2,     1,     1,     1,     2,
       1,     1,     2,     1,     1,     2,     1,     1,     2,     2,
       4,     4,     1,     2,     3,     2,     1,     2,     3,     2,
       1,     1,     1,     3,     1,     1,     3,     3,     1,     1,
       1,     1,     1,     2,     1,     2,     1,     1,     4,     1,
       3,     3,     3,     3,     1,     2,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       2,     0,     1,     1,     1,     4,     1,     2,     0,     1,
       1,     1,     1,     3,     1,     3,     0,     1,     1,     1,
       1,     1,     3,     0,     1,     1,     2,     1,     1,     1,
       1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = DSL_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == DSL_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, yyscanner, pp, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use DSL_error or DSL_UNDEF. */
#define YYERRCODE DSL_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if DSL_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YYLOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YYLOCATION_PRINT

#  if defined YY_LOCATION_PRINT

   /* Temporary convenience wrapper in case some people defined the
      undocumented and private YY_LOCATION_PRINT macros.  */
#   define YYLOCATION_PRINT(File, Loc)  YY_LOCATION_PRINT(File, *(Loc))

#  elif defined DSL_LTYPE_IS_TRIVIAL && DSL_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
}

#   define YYLOCATION_PRINT  yy_location_print_

    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT(File, Loc)  YYLOCATION_PRINT(File, &(Loc))

#  else

#   define YYLOCATION_PRINT(File, Loc) ((void) 0)
    /* Temporary convenience wrapper in case some people defined the
       undocumented and private YY_LOCATION_PRINT macros.  */
#   define YY_LOCATION_PRINT  YYLOCATION_PRINT

#  endif
# endif /* !defined YYLOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, yyscanner, pp); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, void* yyscanner, struct dsl_param *pp)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (yylocationp);
  YY_USE (yyscanner);
  YY_USE (pp);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_SCANNER_ERROR: /* SCANNER_ERROR  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1116 "dsl_parser.c"
        break;

    case YYSYMBOL_HERESTR_OPEN: /* HERESTR_OPEN  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1122 "dsl_parser.c"
        break;

    case YYSYMBOL_HERESTR_CLOSE: /* HERESTR_CLOSE  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1128 "dsl_parser.c"
        break;

    case YYSYMBOL_STMT_SEP: /* STMT_SEP  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1134 "dsl_parser.c"
        break;

    case YYSYMBOL_CHAR: /* CHAR  */
#line 197 "dsl_parser.y"
         { fprintf(yyo, "<c> " "'%c'", ((*yyvaluep).c)); }
#line 1140 "dsl_parser.c"
        break;

    case YYSYMBOL_IDENTIFIER: /* IDENTIFIER  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1146 "dsl_parser.c"
        break;

    case YYSYMBOL_IDENTIFIER_EXT: /* IDENTIFIER_EXT  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1152 "dsl_parser.c"
        break;

    case YYSYMBOL_STRING_LITERAL: /* STRING_LITERAL  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1158 "dsl_parser.c"
        break;

    case YYSYMBOL_INT_NUMBER: /* INT_NUMBER  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1164 "dsl_parser.c"
        break;

    case YYSYMBOL_FP_NUMBER: /* FP_NUMBER  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1170 "dsl_parser.c"
        break;

    case YYSYMBOL_TREE_LEVEL: /* TREE_LEVEL  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1176 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_QUOTE: /* OP_QUOTE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1182 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_NAMESPACE: /* OP_NAMESPACE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1188 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_SEPARATOR: /* OP_SEPARATOR  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1194 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CONTAINER_OPEN: /* OP_CONTAINER_OPEN  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1200 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CONTAINER_CLOSE: /* OP_CONTAINER_CLOSE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1206 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_FUNCTION: /* OP_FUNCTION  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1212 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_INDEX_OPEN: /* OP_INDEX_OPEN  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1218 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_INDEX_CLOSE: /* OP_INDEX_CLOSE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1224 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_POSTFIX: /* OP_POSTFIX  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1230 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CHAIN_LEFT: /* OP_CHAIN_LEFT  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1236 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CHAIN_RIGHT: /* OP_CHAIN_RIGHT  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1242 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_UNARY: /* OP_UNARY  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1248 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_MULTIPLICATIVE: /* OP_MULTIPLICATIVE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1254 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_ADDITIVE: /* OP_ADDITIVE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1260 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_SHIFT: /* OP_SHIFT  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1266 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_RELATIONAL: /* OP_RELATIONAL  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1272 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_EQUALITY: /* OP_EQUALITY  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1278 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_AND: /* OP_BITWISE_AND  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1284 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_XOR: /* OP_BITWISE_XOR  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1290 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_OR: /* OP_BITWISE_OR  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1296 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_LOGICAL_AND: /* OP_LOGICAL_AND  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1302 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_LOGICAL_OR: /* OP_LOGICAL_OR  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1308 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_ASSIGNMENT: /* OP_ASSIGNMENT  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1314 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_END: /* OP_END  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1320 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BLOCK_OPEN: /* OP_BLOCK_OPEN  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1326 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BLOCK_CLOSE: /* OP_BLOCK_CLOSE  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1332 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_COMMENT_EOL: /* OP_COMMENT_EOL  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1338 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_COMMENT: /* OP_COMMENT  */
#line 213 "dsl_parser.y"
         { fprintf(yyo, "<ix> " "index %"PRIu64, ((*yyvaluep).ix)); }
#line 1344 "dsl_parser.c"
        break;

    case YYSYMBOL_42_n_: /* '\n'  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1350 "dsl_parser.c"
        break;

    case YYSYMBOL_43_: /* ';'  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1356 "dsl_parser.c"
        break;

    case YYSYMBOL_44_: /* '('  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1362 "dsl_parser.c"
        break;

    case YYSYMBOL_45_: /* ')'  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1368 "dsl_parser.c"
        break;

    case YYSYMBOL_46_: /* ','  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1374 "dsl_parser.c"
        break;

    case YYSYMBOL_inline_dsl: /* inline_dsl  */
#line 214 "dsl_parser.y"
         { fprintf(yyo, "<>"); }
#line 1380 "dsl_parser.c"
        break;

    case YYSYMBOL_dsl_code: /* dsl_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1386 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_stmt_list: /* cexpr_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1392 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_code: /* cexpr_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1398 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_stmt_list: /* custom_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1404 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_code: /* custom_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1410 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_stmt_list: /* asm_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1416 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_code: /* asm_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1422 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_stmt_list: /* sexpr_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1428 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_code: /* sexpr_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1434 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_stmt_list: /* tree_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1440 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_code: /* tree_code  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1446 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_stmt: /* tree_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1452 "dsl_parser.c"
        break;

    case YYSYMBOL_toplevel_call_tree: /* toplevel_call_tree  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1458 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_compound_stmt_list: /* custom_compound_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1464 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_compound_stmt: /* custom_compound_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1470 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_stmt: /* custom_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1476 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_compound_stmt_list: /* cexpr_compound_stmt_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1482 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_compound_stmt: /* cexpr_compound_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1488 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_stmt: /* cexpr_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1494 "dsl_parser.c"
        break;

    case YYSYMBOL_quotable: /* quotable  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1500 "dsl_parser.c"
        break;

    case YYSYMBOL_quoted_expression: /* quoted_expression  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1506 "dsl_parser.c"
        break;

    case YYSYMBOL_namespace_expression: /* namespace_expression  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1512 "dsl_parser.c"
        break;

    case YYSYMBOL_container_expression: /* container_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1518 "dsl_parser.c"
        break;

    case YYSYMBOL_primary_expression: /* primary_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1524 "dsl_parser.c"
        break;

    case YYSYMBOL_callable: /* callable  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1530 "dsl_parser.c"
        break;

    case YYSYMBOL_function_call: /* function_call  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1536 "dsl_parser.c"
        break;

    case YYSYMBOL_postfix_expression: /* postfix_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1542 "dsl_parser.c"
        break;

    case YYSYMBOL_index_expression: /* index_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1548 "dsl_parser.c"
        break;

    case YYSYMBOL_chain_expression: /* chain_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1554 "dsl_parser.c"
        break;

    case YYSYMBOL_unary_expression: /* unary_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1560 "dsl_parser.c"
        break;

    case YYSYMBOL_multiplicative_expression: /* multiplicative_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1566 "dsl_parser.c"
        break;

    case YYSYMBOL_additive_expression: /* additive_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1572 "dsl_parser.c"
        break;

    case YYSYMBOL_shift_expression: /* shift_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1578 "dsl_parser.c"
        break;

    case YYSYMBOL_relational_expression: /* relational_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1584 "dsl_parser.c"
        break;

    case YYSYMBOL_equality_expression: /* equality_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1590 "dsl_parser.c"
        break;

    case YYSYMBOL_and_expression: /* and_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1596 "dsl_parser.c"
        break;

    case YYSYMBOL_xor_expression: /* xor_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1602 "dsl_parser.c"
        break;

    case YYSYMBOL_or_expression: /* or_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1608 "dsl_parser.c"
        break;

    case YYSYMBOL_logical_and_expression: /* logical_and_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1614 "dsl_parser.c"
        break;

    case YYSYMBOL_logical_or_expression: /* logical_or_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1620 "dsl_parser.c"
        break;

    case YYSYMBOL_assignment_expression: /* assignment_expression  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1626 "dsl_parser.c"
        break;

    case YYSYMBOL_expression_separator_list: /* expression_separator_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1632 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_expression_separator_list: /* opt_expression_separator_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1638 "dsl_parser.c"
        break;

    case YYSYMBOL_expression: /* expression  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1644 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_stmt: /* sexpr_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1650 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr: /* sexpr  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1656 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_argument_list: /* sexpr_argument_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1662 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_sexpr_argument_list: /* opt_sexpr_argument_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1668 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_argument: /* sexpr_argument  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1674 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_stmt: /* asm_stmt  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1680 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_argument_comma_list: /* asm_argument_comma_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1686 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_asm_argument_comma_list: /* opt_asm_argument_comma_list  */
#line 211 "dsl_parser.y"
         { fprintf(yyo, "<vdast> " "(len %zu)", sv_len(((*yyvaluep).vdast))); }
#line 1692 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_argument: /* asm_argument  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1698 "dsl_parser.c"
        break;

    case YYSYMBOL_literal: /* literal  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1704 "dsl_parser.c"
        break;

    case YYSYMBOL_herestring: /* herestring  */
#line 201 "dsl_parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 1710 "dsl_parser.c"
        break;

    case YYSYMBOL_verbatim: /* verbatim  */
#line 201 "dsl_parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 1716 "dsl_parser.c"
        break;

    case YYSYMBOL_verbatim_char_list: /* verbatim_char_list  */
#line 201 "dsl_parser.y"
         { fprintf(yyo, "<str> " "(len %zu)", ss_len(((*yyvaluep).str))); }
#line 1722 "dsl_parser.c"
        break;

    case YYSYMBOL_number: /* number  */
#line 199 "dsl_parser.y"
         { fprintf(yyo, "<txt> " "\"%s\"", ((*yyvaluep).txt) ? ((*yyvaluep).txt) : "<null>"); }
#line 1728 "dsl_parser.c"
        break;

    case YYSYMBOL_identifier_ext: /* identifier_ext  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1734 "dsl_parser.c"
        break;

    case YYSYMBOL_identifier: /* identifier  */
#line 203 "dsl_parser.y"
         { fprintf(yyo, "<node> " "%s (len %zu)", (((*yyvaluep).node).type == DSL_AST_NODE_CONST) ? "CONST" : "CALL",sv_len(((*yyvaluep).node).child)); }
#line 1740 "dsl_parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, void* yyscanner, struct dsl_param *pp)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YYLOCATION_PRINT (yyo, yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, yyscanner, pp);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, void* yyscanner, struct dsl_param *pp)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), yyscanner, pp);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, yyscanner, pp); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !DSL_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !DSL_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yysymbol_kind_t yytoken;
  YYLTYPE *yylloc;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  int yyn = yypact[+*yyctx->yyssp];
  if (!yypact_value_is_default (yyn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;
      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYSYMBOL_YYerror
            && !yytable_value_is_error (yytable[yyx + yyn]))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = YY_CAST (yysymbol_kind_t, yyx);
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif



static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yystrlen (yysymbol_name (yyarg[yyi]));
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp = yystpcpy (yyp, yysymbol_name (yyarg[yyi++]));
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, void* yyscanner, struct dsl_param *pp)
{
  YY_USE (yyvaluep);
  YY_USE (yylocationp);
  YY_USE (yyscanner);
  YY_USE (pp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_CHAR: /* CHAR  */
#line 196 "dsl_parser.y"
            {  }
#line 2095 "dsl_parser.c"
        break;

    case YYSYMBOL_IDENTIFIER: /* IDENTIFIER  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2101 "dsl_parser.c"
        break;

    case YYSYMBOL_IDENTIFIER_EXT: /* IDENTIFIER_EXT  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2107 "dsl_parser.c"
        break;

    case YYSYMBOL_STRING_LITERAL: /* STRING_LITERAL  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2113 "dsl_parser.c"
        break;

    case YYSYMBOL_INT_NUMBER: /* INT_NUMBER  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2119 "dsl_parser.c"
        break;

    case YYSYMBOL_FP_NUMBER: /* FP_NUMBER  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2125 "dsl_parser.c"
        break;

    case YYSYMBOL_TREE_LEVEL: /* TREE_LEVEL  */
#line 212 "dsl_parser.y"
            {  }
#line 2131 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_QUOTE: /* OP_QUOTE  */
#line 212 "dsl_parser.y"
            {  }
#line 2137 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_NAMESPACE: /* OP_NAMESPACE  */
#line 212 "dsl_parser.y"
            {  }
#line 2143 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_SEPARATOR: /* OP_SEPARATOR  */
#line 212 "dsl_parser.y"
            {  }
#line 2149 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CONTAINER_OPEN: /* OP_CONTAINER_OPEN  */
#line 212 "dsl_parser.y"
            {  }
#line 2155 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CONTAINER_CLOSE: /* OP_CONTAINER_CLOSE  */
#line 212 "dsl_parser.y"
            {  }
#line 2161 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_FUNCTION: /* OP_FUNCTION  */
#line 212 "dsl_parser.y"
            {  }
#line 2167 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_INDEX_OPEN: /* OP_INDEX_OPEN  */
#line 212 "dsl_parser.y"
            {  }
#line 2173 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_INDEX_CLOSE: /* OP_INDEX_CLOSE  */
#line 212 "dsl_parser.y"
            {  }
#line 2179 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_POSTFIX: /* OP_POSTFIX  */
#line 212 "dsl_parser.y"
            {  }
#line 2185 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CHAIN_LEFT: /* OP_CHAIN_LEFT  */
#line 212 "dsl_parser.y"
            {  }
#line 2191 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_CHAIN_RIGHT: /* OP_CHAIN_RIGHT  */
#line 212 "dsl_parser.y"
            {  }
#line 2197 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_UNARY: /* OP_UNARY  */
#line 212 "dsl_parser.y"
            {  }
#line 2203 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_MULTIPLICATIVE: /* OP_MULTIPLICATIVE  */
#line 212 "dsl_parser.y"
            {  }
#line 2209 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_ADDITIVE: /* OP_ADDITIVE  */
#line 212 "dsl_parser.y"
            {  }
#line 2215 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_SHIFT: /* OP_SHIFT  */
#line 212 "dsl_parser.y"
            {  }
#line 2221 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_RELATIONAL: /* OP_RELATIONAL  */
#line 212 "dsl_parser.y"
            {  }
#line 2227 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_EQUALITY: /* OP_EQUALITY  */
#line 212 "dsl_parser.y"
            {  }
#line 2233 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_AND: /* OP_BITWISE_AND  */
#line 212 "dsl_parser.y"
            {  }
#line 2239 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_XOR: /* OP_BITWISE_XOR  */
#line 212 "dsl_parser.y"
            {  }
#line 2245 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BITWISE_OR: /* OP_BITWISE_OR  */
#line 212 "dsl_parser.y"
            {  }
#line 2251 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_LOGICAL_AND: /* OP_LOGICAL_AND  */
#line 212 "dsl_parser.y"
            {  }
#line 2257 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_LOGICAL_OR: /* OP_LOGICAL_OR  */
#line 212 "dsl_parser.y"
            {  }
#line 2263 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_ASSIGNMENT: /* OP_ASSIGNMENT  */
#line 212 "dsl_parser.y"
            {  }
#line 2269 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_END: /* OP_END  */
#line 212 "dsl_parser.y"
            {  }
#line 2275 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BLOCK_OPEN: /* OP_BLOCK_OPEN  */
#line 212 "dsl_parser.y"
            {  }
#line 2281 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_BLOCK_CLOSE: /* OP_BLOCK_CLOSE  */
#line 212 "dsl_parser.y"
            {  }
#line 2287 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_COMMENT_EOL: /* OP_COMMENT_EOL  */
#line 212 "dsl_parser.y"
            {  }
#line 2293 "dsl_parser.c"
        break;

    case YYSYMBOL_OP_COMMENT: /* OP_COMMENT  */
#line 212 "dsl_parser.y"
            {  }
#line 2299 "dsl_parser.c"
        break;

    case YYSYMBOL_dsl_code: /* dsl_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2311 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_stmt_list: /* cexpr_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2323 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_code: /* cexpr_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2335 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_stmt_list: /* custom_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2347 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_code: /* custom_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2359 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_stmt_list: /* asm_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2371 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_code: /* asm_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2383 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_stmt_list: /* sexpr_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2395 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_code: /* sexpr_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2407 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_stmt_list: /* tree_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2419 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_code: /* tree_code  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2431 "dsl_parser.c"
        break;

    case YYSYMBOL_tree_stmt: /* tree_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2437 "dsl_parser.c"
        break;

    case YYSYMBOL_toplevel_call_tree: /* toplevel_call_tree  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2449 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_compound_stmt_list: /* custom_compound_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2461 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_compound_stmt: /* custom_compound_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2467 "dsl_parser.c"
        break;

    case YYSYMBOL_custom_stmt: /* custom_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2473 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_compound_stmt_list: /* cexpr_compound_stmt_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2485 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_compound_stmt: /* cexpr_compound_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2491 "dsl_parser.c"
        break;

    case YYSYMBOL_cexpr_stmt: /* cexpr_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2497 "dsl_parser.c"
        break;

    case YYSYMBOL_quotable: /* quotable  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2503 "dsl_parser.c"
        break;

    case YYSYMBOL_quoted_expression: /* quoted_expression  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2509 "dsl_parser.c"
        break;

    case YYSYMBOL_namespace_expression: /* namespace_expression  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2515 "dsl_parser.c"
        break;

    case YYSYMBOL_container_expression: /* container_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2527 "dsl_parser.c"
        break;

    case YYSYMBOL_primary_expression: /* primary_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2539 "dsl_parser.c"
        break;

    case YYSYMBOL_callable: /* callable  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2545 "dsl_parser.c"
        break;

    case YYSYMBOL_function_call: /* function_call  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2551 "dsl_parser.c"
        break;

    case YYSYMBOL_postfix_expression: /* postfix_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2563 "dsl_parser.c"
        break;

    case YYSYMBOL_index_expression: /* index_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2575 "dsl_parser.c"
        break;

    case YYSYMBOL_chain_expression: /* chain_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2587 "dsl_parser.c"
        break;

    case YYSYMBOL_unary_expression: /* unary_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2599 "dsl_parser.c"
        break;

    case YYSYMBOL_multiplicative_expression: /* multiplicative_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2611 "dsl_parser.c"
        break;

    case YYSYMBOL_additive_expression: /* additive_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2623 "dsl_parser.c"
        break;

    case YYSYMBOL_shift_expression: /* shift_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2635 "dsl_parser.c"
        break;

    case YYSYMBOL_relational_expression: /* relational_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2647 "dsl_parser.c"
        break;

    case YYSYMBOL_equality_expression: /* equality_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2659 "dsl_parser.c"
        break;

    case YYSYMBOL_and_expression: /* and_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2671 "dsl_parser.c"
        break;

    case YYSYMBOL_xor_expression: /* xor_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2683 "dsl_parser.c"
        break;

    case YYSYMBOL_or_expression: /* or_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2695 "dsl_parser.c"
        break;

    case YYSYMBOL_logical_and_expression: /* logical_and_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2707 "dsl_parser.c"
        break;

    case YYSYMBOL_logical_or_expression: /* logical_or_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2719 "dsl_parser.c"
        break;

    case YYSYMBOL_assignment_expression: /* assignment_expression  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2731 "dsl_parser.c"
        break;

    case YYSYMBOL_expression_separator_list: /* expression_separator_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2743 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_expression_separator_list: /* opt_expression_separator_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2755 "dsl_parser.c"
        break;

    case YYSYMBOL_expression: /* expression  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2761 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_stmt: /* sexpr_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2767 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr: /* sexpr  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2773 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_argument_list: /* sexpr_argument_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2785 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_sexpr_argument_list: /* opt_sexpr_argument_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2797 "dsl_parser.c"
        break;

    case YYSYMBOL_sexpr_argument: /* sexpr_argument  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2803 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_stmt: /* asm_stmt  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2809 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_argument_comma_list: /* asm_argument_comma_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2821 "dsl_parser.c"
        break;

    case YYSYMBOL_opt_asm_argument_comma_list: /* opt_asm_argument_comma_list  */
#line 204 "dsl_parser.y"
            { {
    for(size_t i = 0; i < sv_len(((*yyvaluep).vdast)); ++i) {
        const void *_item = sv_at(((*yyvaluep).vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&(((*yyvaluep).vdast)));
}
 }
#line 2833 "dsl_parser.c"
        break;

    case YYSYMBOL_asm_argument: /* asm_argument  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2839 "dsl_parser.c"
        break;

    case YYSYMBOL_literal: /* literal  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2845 "dsl_parser.c"
        break;

    case YYSYMBOL_herestring: /* herestring  */
#line 200 "dsl_parser.y"
            { ss_free(&(((*yyvaluep).str))); }
#line 2851 "dsl_parser.c"
        break;

    case YYSYMBOL_verbatim: /* verbatim  */
#line 200 "dsl_parser.y"
            { ss_free(&(((*yyvaluep).str))); }
#line 2857 "dsl_parser.c"
        break;

    case YYSYMBOL_verbatim_char_list: /* verbatim_char_list  */
#line 200 "dsl_parser.y"
            { ss_free(&(((*yyvaluep).str))); }
#line 2863 "dsl_parser.c"
        break;

    case YYSYMBOL_number: /* number  */
#line 198 "dsl_parser.y"
            { free(((*yyvaluep).txt)); ((*yyvaluep).txt) = NULL; }
#line 2869 "dsl_parser.c"
        break;

    case YYSYMBOL_identifier_ext: /* identifier_ext  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2875 "dsl_parser.c"
        break;

    case YYSYMBOL_identifier: /* identifier  */
#line 202 "dsl_parser.y"
            { dsl_ast_free(&((*yyvaluep).node)); }
#line 2881 "dsl_parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (void* yyscanner, struct dsl_param *pp)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined DSL_LTYPE_IS_TRIVIAL && DSL_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    /* The location stack: array, bottom, top.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp = yyls;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = DSL_EMPTY; /* Cause a token to be read.  */


/* User initialization code.  */
#line 11 "dsl_parser.y"
{ pp = dsl_get_extra(yyscanner); }

#line 2981 "dsl_parser.c"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == DSL_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, yyscanner);
    }

  if (yychar <= DSL_EOF)
    {
      yychar = DSL_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == DSL_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = DSL_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = DSL_EMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* inline_dsl: dsl_code  */
#line 333 "dsl_parser.y"
               { pp->astlist = (yyvsp[0].vdast); }
#line 3194 "dsl_parser.c"
    break;

  case 3: /* inline_dsl: error  */
#line 334 "dsl_parser.y"
            { YYABORT; }
#line 3200 "dsl_parser.c"
    break;

  case 4: /* inline_dsl: error SCANNER_ERROR  */
#line 335 "dsl_parser.y"
                          { YYABORT; }
#line 3206 "dsl_parser.c"
    break;

  case 10: /* cexpr_stmt_list: cexpr_stmt  */
#line 347 "dsl_parser.y"
                 {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3215 "dsl_parser.c"
    break;

  case 11: /* cexpr_stmt_list: cexpr_stmt_list cexpr_stmt  */
#line 351 "dsl_parser.y"
                                 {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3224 "dsl_parser.c"
    break;

  case 14: /* custom_stmt_list: custom_stmt  */
#line 361 "dsl_parser.y"
                  {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3233 "dsl_parser.c"
    break;

  case 15: /* custom_stmt_list: custom_stmt_list custom_stmt  */
#line 365 "dsl_parser.y"
                                   {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3242 "dsl_parser.c"
    break;

  case 18: /* asm_stmt_list: asm_stmt  */
#line 375 "dsl_parser.y"
               {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3251 "dsl_parser.c"
    break;

  case 19: /* asm_stmt_list: asm_stmt_list asm_stmt  */
#line 379 "dsl_parser.y"
                             {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3260 "dsl_parser.c"
    break;

  case 21: /* sexpr_stmt_list: sexpr_stmt  */
#line 388 "dsl_parser.y"
                 {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3269 "dsl_parser.c"
    break;

  case 22: /* sexpr_stmt_list: sexpr_stmt_list sexpr_stmt  */
#line 392 "dsl_parser.y"
                                 {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3278 "dsl_parser.c"
    break;

  case 24: /* tree_stmt_list: tree_stmt  */
#line 401 "dsl_parser.y"
                {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3287 "dsl_parser.c"
    break;

  case 25: /* tree_stmt_list: tree_stmt_list tree_stmt  */
#line 405 "dsl_parser.y"
                               {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3296 "dsl_parser.c"
    break;

  case 27: /* tree_stmt: toplevel_call_tree  */
#line 415 "dsl_parser.y"
                         {
        (yyval.node) = *pp->expr;
        { free(pp -> expr); (pp -> expr) = NULL; } // hand-over
        sv_free(&(yyvsp[0].vdast));
    }
#line 3306 "dsl_parser.c"
    break;

  case 28: /* toplevel_call_tree: literal '\n'  */
#line 425 "dsl_parser.y"
                   { /* NOTE: better error reporting here than in tree_stmt */
        pp->level = 0;
        pp->expr = malloc(sizeof(struct dsl_ast));
        if(NULL == pp->expr) YYNOMEM;
        *pp->expr = (yyvsp[-1].node);
        /* init tip vector */
            ((yyval.vdast)) = sv_alloc_t(SV_PTR,1);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
        /* update tip at this level */
            if(!sv_push(&((yyval.vdast)),&(pp->expr))) { YYNOMEM; }
    }
#line 3322 "dsl_parser.c"
    break;

  case 29: /* toplevel_call_tree: IDENTIFIER_EXT '\n'  */
#line 436 "dsl_parser.y"
                          {
        pp->level = 0;
        pp->expr = malloc(sizeof(struct dsl_ast));
        if(NULL == pp->expr) YYNOMEM;
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[-1].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[-1].txt),(pp->lang)->name);

        srt_vector *    (child) = sv_alloc(sizeof(struct dsl_ast),2,NULL);
    if(sv_void == (child)) { YYNOMEM; }
;
        *pp->expr = DSL_AST_NODE_CALL_xset(found.ix,.child= child);
        /* init tip vector */
            ((yyval.vdast)) = sv_alloc_t(SV_PTR,4);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
        /* update tip at this level */
            if(!sv_push(&((yyval.vdast)),&(pp->expr))) { YYNOMEM; }
        free((yyvsp[-1].txt));
    }
#line 3351 "dsl_parser.c"
    break;

  case 30: /* toplevel_call_tree: toplevel_call_tree TREE_LEVEL literal '\n'  */
#line 460 "dsl_parser.y"
                                                 {
        if((yyvsp[-2].ix) > pp->level + 1)
            ABORT_PARSE0("unexpected depth jump in tree member");
        /* append child to parent */
        struct dsl_ast *parent = sv_at_ptr((yyvsp[-3].vdast),(yyvsp[-2].ix) - 1);
        if(parent->type != DSL_AST_NODE_CALL)
            ABORT_PARSE0("unexpected child of non-call node");
            if(!sv_push(&(parent->child),&((yyvsp[-1].node)))) { YYNOMEM; }
        /* update tip at this level */
        if(!sv_set_ptr(&(yyvsp[-3].vdast),(yyvsp[-2].ix),
            (struct dsl_ast*)sv_at(parent->child,sv_len(parent->child) - 1))) YYNOMEM;
        pp->level = (yyvsp[-2].ix);
        (yyval.vdast) = (yyvsp[-3].vdast);
    }
#line 3370 "dsl_parser.c"
    break;

  case 31: /* toplevel_call_tree: toplevel_call_tree TREE_LEVEL IDENTIFIER_EXT '\n'  */
#line 474 "dsl_parser.y"
                                                        {
        if((yyvsp[-2].ix) > pp->level + 1)
            ABORT_PARSE0("unexpected depth jump in tree member");
        /* append child to parent */
        struct dsl_ast *parent = sv_at_ptr((yyvsp[-3].vdast),(yyvsp[-2].ix) - 1);
        if(parent->type != DSL_AST_NODE_CALL)
            ABORT_PARSE0("unexpected child of non-call node");
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[-1].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[-1].txt),(pp->lang)->name);

        srt_vector *    (child) = sv_alloc(sizeof(struct dsl_ast),2,NULL);
    if(sv_void == (child)) { YYNOMEM; }
;
        struct dsl_ast call = DSL_AST_NODE_CALL_xset(found.ix,.child= child);
            if(!sv_push(&(parent->child),&(call))) { YYNOMEM; }
        /* update tip at this level */
        if(!sv_set_ptr(&(yyvsp[-3].vdast),(yyvsp[-2].ix),
            (struct dsl_ast*)sv_at(parent->child,sv_len(parent->child) - 1))) YYNOMEM;
        pp->level = (yyvsp[-2].ix);
        free((yyvsp[-1].txt));
        (yyval.vdast) = (yyvsp[-3].vdast);
    }
#line 3403 "dsl_parser.c"
    break;

  case 32: /* custom_compound_stmt_list: custom_compound_stmt  */
#line 506 "dsl_parser.y"
                           {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3412 "dsl_parser.c"
    break;

  case 33: /* custom_compound_stmt_list: custom_compound_stmt_list custom_compound_stmt  */
#line 510 "dsl_parser.y"
                                                     {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3421 "dsl_parser.c"
    break;

  case 34: /* custom_compound_stmt: OP_BLOCK_OPEN custom_stmt_list OP_BLOCK_CLOSE  */
#line 516 "dsl_parser.y"
                                                    {
        if(sv_at_ptr(pp->lang->kw_snip,(yyvsp[-2].ix)) != sv_at_ptr(pp->lang->kw_snip,(yyvsp[0].ix)))
            ABORT_PARSE("mismatched closing operator `%s` to block operator `%s`",
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[0].ix)),
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[-2].ix)));
        (yyval.node) = DSL_AST_NODE_CALL_xset((yyvsp[-2].ix),.child=(yyvsp[-1].vdast));
        (void)(yyvsp[0].ix);
    }
#line 3434 "dsl_parser.c"
    break;

  case 35: /* custom_stmt: expression OP_END  */
#line 527 "dsl_parser.y"
                        { (yyval.node) = (yyvsp[-1].node); (void)(yyvsp[0].ix); }
#line 3440 "dsl_parser.c"
    break;

  case 36: /* cexpr_compound_stmt_list: cexpr_compound_stmt  */
#line 532 "dsl_parser.y"
                          {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),8,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3449 "dsl_parser.c"
    break;

  case 37: /* cexpr_compound_stmt_list: cexpr_compound_stmt_list cexpr_compound_stmt  */
#line 536 "dsl_parser.y"
                                                   {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3458 "dsl_parser.c"
    break;

  case 38: /* cexpr_compound_stmt: OP_BLOCK_OPEN cexpr_stmt_list OP_BLOCK_CLOSE  */
#line 542 "dsl_parser.y"
                                                   {
        if(sv_at_ptr(pp->lang->kw_snip,(yyvsp[-2].ix)) != sv_at_ptr(pp->lang->kw_snip,(yyvsp[0].ix)))
            ABORT_PARSE("mismatched closing operator `%s` to block operator `%s`",
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[0].ix)),
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[-2].ix)));
        (yyval.node) = DSL_AST_NODE_CALL_xset((yyvsp[-2].ix),.child=(yyvsp[-1].vdast));
        (void)(yyvsp[0].ix);
    }
#line 3471 "dsl_parser.c"
    break;

  case 40: /* quotable: verbatim  */
#line 557 "dsl_parser.y"
               {
        char *str = NULL;
            {
    const char *ctmp = ss_to_c((yyvsp[0].str));
            str = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == (str)) {  }
   
}
    ss_free(&((yyvsp[0].str)));
    if(NULL == str) { YYNOMEM; }

        (yyval.node) = DSL_AST_NODE_CONST_set(str);
    }
#line 3489 "dsl_parser.c"
    break;

  case 43: /* quoted_expression: OP_QUOTE quotable OP_QUOTE  */
#line 575 "dsl_parser.y"
                                 {
        if((yyvsp[-2].ix) != (yyvsp[0].ix))
            ABORT_PARSE("mismatched closing operator `%s` to quote operator `%s`",
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[0].ix)),
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[-2].ix)));
        if(dsl_ast_new_call1((yyvsp[-2].ix),&(yyvsp[-1].node),&(yyval.node))) YYNOMEM;
    }
#line 3501 "dsl_parser.c"
    break;

  case 44: /* quoted_expression: STRING_LITERAL  */
#line 582 "dsl_parser.y"
                     {
        (yyval.node) = DSL_AST_NODE_CONST_set((yyvsp[0].txt));
    }
#line 3509 "dsl_parser.c"
    break;

  case 46: /* namespace_expression: namespace_expression OP_NAMESPACE identifier  */
#line 589 "dsl_parser.y"
                                                   {
            if(pp->lang->type != DSL_TYPE_custom) {
        ERROR0("cexpr syntax error: namespace_expression is not supported");
        YYERROR;
    }
        if(dsl_ast_new_call2((yyvsp[-1].ix),&(yyvsp[-2].node),&(yyvsp[0].node),&(yyval.node))) YYNOMEM;
    }
#line 3521 "dsl_parser.c"
    break;

  case 47: /* container_expression: OP_CONTAINER_OPEN opt_expression_separator_list OP_CONTAINER_CLOSE  */
#line 599 "dsl_parser.y"
                                                                         {
        if(pp->lang->type == DSL_TYPE_custom && NULL != sv_at_ptr(pp->lang->kw_snip,(yyvsp[-2].ix))) {
            if(sv_at_ptr(pp->lang->kw_snip,(yyvsp[-2].ix)) != sv_at_ptr(pp->lang->kw_snip,(yyvsp[0].ix))) {
                const char *closing = sv_at_ptr(pp->lang->kw_name,(yyvsp[0].ix));
                ABORT_PARSE("mismatched closing operator `%s` to container operator `%s`",
                            (NULL == closing) ? "<none>" : closing,
                            (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[-2].ix)));
            }
            struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-2].ix),.child=(yyvsp[-1].vdast));
            if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        } else (yyval.vdast) = (yyvsp[-1].vdast);
        (void)(yyvsp[0].ix);
    }
#line 3539 "dsl_parser.c"
    break;

  case 48: /* primary_expression: literal  */
#line 615 "dsl_parser.y"
              { if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM; }
#line 3545 "dsl_parser.c"
    break;

  case 49: /* primary_expression: namespace_expression  */
#line 616 "dsl_parser.y"
                           { if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM; }
#line 3551 "dsl_parser.c"
    break;

  case 50: /* primary_expression: quoted_expression  */
#line 617 "dsl_parser.y"
                        { if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM; }
#line 3557 "dsl_parser.c"
    break;

  case 52: /* callable: primary_expression  */
#line 622 "dsl_parser.y"
                         {
        if(sv_len((yyvsp[0].vdast)) > 1) ABORT_PARSE0("unexpected compound expression");
        struct dsl_ast *expr = (struct dsl_ast*)sv_at((yyvsp[0].vdast),0);
        {
    size_t ierr = 0;
    size_t errix = 0;
    int ret = dsl_ast_eval(expr,pp->lang,&ierr,&errix);
    switch(ret) {
        case 1: YYNOMEM; break;
        case 2:
        {
            const char *name_err = sv_at_ptr((pp->lang)->kw_name,errix);
            const struct snippet *snip_err = sv_at_ptr((pp->lang)->kw_snip,errix);
            if(SNIPPET_NARGIN_NODEF(snip_err) == snip_err->nargs) {
                ABORT_PARSE("expected exactly %zu (got %zu) arguments for keyword `%s` in DSL `%s`",
                              snip_err->nargs,ierr,name_err,(pp->lang)->name);
            } else {
                ABORT_PARSE("expected between %zu and %zu (got %zu) arguments for keyword `%s` in DSL `%s`",
                              SNIPPET_NARGIN_NODEF(snip_err),snip_err->nargs,ierr,name_err,(pp->lang)->name);
            }
        }
            break;
        case 3:
            ABORT_PARSE("evaluation depth limit (%zu) exceeded for DSL `%s`",
                     (pp->lang)->max_depth,(pp->lang)->name);
            break;
    }
}
        (yyval.txt) = expr->value.txt; expr->value.txt = NULL;
        {
    for(size_t i = 0; i < sv_len((yyvsp[0].vdast)); ++i) {
        const void *_item = sv_at((yyvsp[0].vdast),i);
        dsl_ast_free((struct dsl_ast*)_item);    }
    sv_free(&((yyvsp[0].vdast)));
}

    }
#line 3599 "dsl_parser.c"
    break;

  case 53: /* function_call: callable container_expression  */
#line 662 "dsl_parser.y"
                                    {
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[-1].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[-1].txt),(pp->lang)->name);

        (yyval.node) = DSL_AST_NODE_CALL_xset(found.ix,.child=(yyvsp[0].vdast));
        free((yyvsp[-1].txt));
    }
#line 3617 "dsl_parser.c"
    break;

  case 55: /* postfix_expression: postfix_expression OP_POSTFIX  */
#line 679 "dsl_parser.y"
                                    {
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[0].ix),.child=(yyvsp[-1].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
    }
#line 3626 "dsl_parser.c"
    break;

  case 56: /* postfix_expression: function_call  */
#line 683 "dsl_parser.y"
                    { if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM; }
#line 3632 "dsl_parser.c"
    break;

  case 58: /* index_expression: postfix_expression OP_INDEX_OPEN opt_expression_separator_list OP_INDEX_CLOSE  */
#line 688 "dsl_parser.y"
                                                                                    {
        if(sv_at_ptr(pp->lang->kw_snip,(yyvsp[-2].ix)) != sv_at_ptr(pp->lang->kw_snip,(yyvsp[0].ix)))
            ABORT_PARSE("mismatched closing index operator `%s` to index operator `%s`",
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[0].ix)),
                        (char*)sv_at_ptr(pp->lang->kw_name,(yyvsp[-2].ix)));
        if(sv_void == sv_cat(&(yyvsp[-3].vdast),(yyvsp[-1].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-2].ix),.child=(yyvsp[-3].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[-1].vdast));
    }
#line 3647 "dsl_parser.c"
    break;

  case 60: /* chain_expression: chain_expression OP_CHAIN_LEFT callable  */
#line 702 "dsl_parser.y"
                                              {
            if(pp->lang->type != DSL_TYPE_custom) {
        ERROR0("cexpr syntax error: chain_expression is not supported");
        YYERROR;
    }
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[0].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[0].txt),(pp->lang)->name);

        struct dsl_ast expr = DSL_AST_NODE_CALL_xset(found.ix,.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        free((yyvsp[0].txt)); (void)(yyvsp[-1].ix);
    }
#line 3670 "dsl_parser.c"
    break;

  case 61: /* chain_expression: chain_expression OP_CHAIN_RIGHT callable  */
#line 720 "dsl_parser.y"
                                               {
            if(pp->lang->type != DSL_TYPE_custom) {
        ERROR0("cexpr syntax error: chain_expression is not supported");
        YYERROR;
    }
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[0].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[0].txt),(pp->lang)->name);

        struct dsl_ast expr = DSL_AST_NODE_CALL_xset(found.ix,.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        free((yyvsp[0].txt)); (void)(yyvsp[-1].ix);
    }
#line 3693 "dsl_parser.c"
    break;

  case 62: /* chain_expression: chain_expression OP_CHAIN_LEFT function_call  */
#line 738 "dsl_parser.y"
                                                   {
            if(pp->lang->type != DSL_TYPE_custom) {
        ERROR0("cexpr syntax error: chain_expression is not supported");
        YYERROR;
    }
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].node).child)) YYNOMEM;
        sv_free(&(yyvsp[0].node).child); (yyvsp[0].node).child = (yyvsp[-2].vdast);
        if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM;
        (void)(yyvsp[-1].ix);
    }
#line 3708 "dsl_parser.c"
    break;

  case 63: /* chain_expression: chain_expression OP_CHAIN_RIGHT function_call  */
#line 748 "dsl_parser.y"
                                                    {
            if(pp->lang->type != DSL_TYPE_custom) {
        ERROR0("cexpr syntax error: chain_expression is not supported");
        YYERROR;
    }
        if(sv_void == sv_cat(&(yyvsp[0].node).child,(yyvsp[-2].vdast))) YYNOMEM;
        if(dsl_ast_to_list(&(yyvsp[0].node),&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[-2].vdast)); (void)(yyvsp[-1].ix);
    }
#line 3722 "dsl_parser.c"
    break;

  case 65: /* unary_expression: OP_UNARY unary_expression  */
#line 761 "dsl_parser.y"
                                {
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[0].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
    }
#line 3731 "dsl_parser.c"
    break;

  case 67: /* multiplicative_expression: multiplicative_expression OP_MULTIPLICATIVE unary_expression  */
#line 769 "dsl_parser.y"
                                                                   {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3742 "dsl_parser.c"
    break;

  case 69: /* additive_expression: additive_expression OP_ADDITIVE multiplicative_expression  */
#line 779 "dsl_parser.y"
                                                                {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3753 "dsl_parser.c"
    break;

  case 71: /* shift_expression: shift_expression OP_SHIFT additive_expression  */
#line 789 "dsl_parser.y"
                                                    {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3764 "dsl_parser.c"
    break;

  case 73: /* relational_expression: relational_expression OP_RELATIONAL shift_expression  */
#line 799 "dsl_parser.y"
                                                           {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3775 "dsl_parser.c"
    break;

  case 75: /* equality_expression: equality_expression OP_EQUALITY relational_expression  */
#line 809 "dsl_parser.y"
                                                            {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3786 "dsl_parser.c"
    break;

  case 77: /* and_expression: and_expression OP_BITWISE_AND equality_expression  */
#line 819 "dsl_parser.y"
                                                        {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3797 "dsl_parser.c"
    break;

  case 79: /* xor_expression: xor_expression OP_BITWISE_XOR and_expression  */
#line 829 "dsl_parser.y"
                                                   {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3808 "dsl_parser.c"
    break;

  case 81: /* or_expression: or_expression OP_BITWISE_OR xor_expression  */
#line 839 "dsl_parser.y"
                                                 {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3819 "dsl_parser.c"
    break;

  case 83: /* logical_and_expression: logical_and_expression OP_LOGICAL_AND or_expression  */
#line 849 "dsl_parser.y"
                                                          {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3830 "dsl_parser.c"
    break;

  case 85: /* logical_or_expression: logical_or_expression OP_LOGICAL_OR logical_and_expression  */
#line 859 "dsl_parser.y"
                                                                 {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3841 "dsl_parser.c"
    break;

  case 87: /* assignment_expression: unary_expression OP_ASSIGNMENT assignment_expression  */
#line 869 "dsl_parser.y"
                                                           {
        if(sv_void == sv_cat(&(yyvsp[-2].vdast),(yyvsp[0].vdast))) YYNOMEM;
        struct dsl_ast expr = DSL_AST_NODE_CALL_xset((yyvsp[-1].ix),.child=(yyvsp[-2].vdast));
        if(dsl_ast_to_list(&expr,&(yyval.vdast))) YYNOMEM;
        sv_free(&(yyvsp[0].vdast));
    }
#line 3852 "dsl_parser.c"
    break;

  case 88: /* expression_separator_list: expression  */
#line 878 "dsl_parser.y"
                 {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),4,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3861 "dsl_parser.c"
    break;

  case 89: /* expression_separator_list: expression_separator_list OP_SEPARATOR expression  */
#line 882 "dsl_parser.y"
                                                        {
        (yyval.vdast) = (yyvsp[-2].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

        (void)(yyvsp[-1].ix);
    }
#line 3871 "dsl_parser.c"
    break;

  case 90: /* expression_separator_list: expression_separator_list OP_SEPARATOR  */
#line 887 "dsl_parser.y"
                                                 { (yyval.vdast) = (yyvsp[-1].vdast);(void)(yyvsp[0].ix); }
#line 3877 "dsl_parser.c"
    break;

  case 91: /* opt_expression_separator_list: %empty  */
#line 891 "dsl_parser.y"
             {
        (yyval.vdast) = NULL;
        
    }
#line 3886 "dsl_parser.c"
    break;

  case 92: /* opt_expression_separator_list: expression_separator_list  */
#line 895 "dsl_parser.y"
                                { (yyval.vdast) = (yyvsp[0].vdast);  }
#line 3892 "dsl_parser.c"
    break;

  case 93: /* expression: assignment_expression  */
#line 898 "dsl_parser.y"
                            {
        /* at this point the expression is reduced to a single element */
        if(sv_len((yyvsp[0].vdast)) > 1) ABORT_PARSE0("unexpected compound expression");
        (yyval.node) = *(const struct dsl_ast*)sv_at((yyvsp[0].vdast),0);
        sv_free(&(yyvsp[0].vdast));
    }
#line 3903 "dsl_parser.c"
    break;

  case 95: /* sexpr: '(' IDENTIFIER_EXT opt_sexpr_argument_list ')'  */
#line 912 "dsl_parser.y"
                                                     {
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[-2].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[-2].txt),(pp->lang)->name);

        (yyval.node) = DSL_AST_NODE_CALL_xset(found.ix,.child=(yyvsp[-1].vdast));
        free((yyvsp[-2].txt));
    }
#line 3921 "dsl_parser.c"
    break;

  case 96: /* sexpr_argument_list: sexpr_argument  */
#line 928 "dsl_parser.y"
                     {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),4,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3930 "dsl_parser.c"
    break;

  case 97: /* sexpr_argument_list: sexpr_argument_list sexpr_argument  */
#line 932 "dsl_parser.y"
                                         {
        (yyval.vdast) = (yyvsp[-1].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

    }
#line 3939 "dsl_parser.c"
    break;

  case 98: /* opt_sexpr_argument_list: %empty  */
#line 938 "dsl_parser.y"
             {
        (yyval.vdast) = NULL;
        
    }
#line 3948 "dsl_parser.c"
    break;

  case 99: /* opt_sexpr_argument_list: sexpr_argument_list  */
#line 942 "dsl_parser.y"
                          { (yyval.vdast) = (yyvsp[0].vdast);  }
#line 3954 "dsl_parser.c"
    break;

  case 103: /* asm_stmt: IDENTIFIER_EXT opt_asm_argument_comma_list STMT_SEP  */
#line 952 "dsl_parser.y"
                                                          {
        setix found = { 0 };     {
    const srt_string *_ss = ss_crefs((yyvsp[-2].txt));
        if(NULL == (pp->lang)->kw_map) {  }
    found.set = shm_atp_su((pp->lang)->kw_map,_ss,&(found).ix);
}
    if(! (found).set)
        ABORT_PARSE("%s:%d: unknown keyword `%s` in DSL `%s`",
                    __FILE__,__LINE__,(yyvsp[-2].txt),(pp->lang)->name);

        (yyval.node) = DSL_AST_NODE_CALL_xset(found.ix,.child=(yyvsp[-1].vdast));
        free((yyvsp[-2].txt));
    }
#line 3972 "dsl_parser.c"
    break;

  case 104: /* asm_argument_comma_list: asm_argument  */
#line 968 "dsl_parser.y"
                   {     ((yyval.vdast)) = sv_alloc(sizeof(struct dsl_ast),4,NULL);
    if(sv_void == ((yyval.vdast))) { YYNOMEM; }
     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }
 }
#line 3981 "dsl_parser.c"
    break;

  case 105: /* asm_argument_comma_list: asm_argument_comma_list ',' asm_argument  */
#line 972 "dsl_parser.y"
                                               {
        (yyval.vdast) = (yyvsp[-2].vdast);     if(!sv_push(&((yyval.vdast)),&((yyvsp[0].node)))) { YYNOMEM; }

        
    }
#line 3991 "dsl_parser.c"
    break;

  case 106: /* opt_asm_argument_comma_list: %empty  */
#line 980 "dsl_parser.y"
             {
        (yyval.vdast) = NULL;
        
    }
#line 4000 "dsl_parser.c"
    break;

  case 107: /* opt_asm_argument_comma_list: asm_argument_comma_list  */
#line 984 "dsl_parser.y"
                              { (yyval.vdast) = (yyvsp[0].vdast);  }
#line 4006 "dsl_parser.c"
    break;

  case 110: /* literal: number  */
#line 992 "dsl_parser.y"
             { (yyval.node) = DSL_AST_NODE_CONST_set((yyvsp[0].txt)); }
#line 4012 "dsl_parser.c"
    break;

  case 111: /* literal: herestring  */
#line 993 "dsl_parser.y"
                 {
        char *str = NULL;
            {
    const char *ctmp = ss_to_c((yyvsp[0].str));
            str = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
    if(NULL != (ctmp) && NULL == (str)) {  }
   
}
    ss_free(&((yyvsp[0].str)));
    if(NULL == str) { YYNOMEM; }

        (yyval.node) = DSL_AST_NODE_CONST_set(str);
    }
#line 4030 "dsl_parser.c"
    break;

  case 112: /* herestring: HERESTR_OPEN verbatim HERESTR_CLOSE  */
#line 1009 "dsl_parser.y"
                                          { (yyval.str) = (yyvsp[-1].str); }
#line 4036 "dsl_parser.c"
    break;

  case 113: /* verbatim: %empty  */
#line 1013 "dsl_parser.y"
             {
        (yyval.str) = NULL;
        
    }
#line 4045 "dsl_parser.c"
    break;

  case 114: /* verbatim: verbatim_char_list  */
#line 1017 "dsl_parser.y"
                         { (yyval.str) = (yyvsp[0].str);  }
#line 4051 "dsl_parser.c"
    break;

  case 115: /* verbatim_char_list: CHAR  */
#line 1020 "dsl_parser.y"
           { (yyval.str) = ss_dup_cn(&(yyvsp[0].c),1); }
#line 4057 "dsl_parser.c"
    break;

  case 116: /* verbatim_char_list: verbatim_char_list CHAR  */
#line 1021 "dsl_parser.y"
                              { (yyval.str) = ss_cat_cn1(&(yyvsp[-1].str),(yyvsp[0].c)); }
#line 4063 "dsl_parser.c"
    break;

  case 119: /* identifier_ext: IDENTIFIER_EXT  */
#line 1030 "dsl_parser.y"
                     { (yyval.node) = DSL_AST_NODE_CONST_set((yyvsp[0].txt)); }
#line 4069 "dsl_parser.c"
    break;

  case 120: /* identifier: IDENTIFIER  */
#line 1034 "dsl_parser.y"
                 { (yyval.node) = DSL_AST_NODE_CONST_set((yyvsp[0].txt)); }
#line 4075 "dsl_parser.c"
    break;


#line 4079 "dsl_parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == DSL_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yytoken, &yylloc};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (&yylloc, yyscanner, pp, yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= DSL_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == DSL_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, yyscanner, pp);
          yychar = DSL_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, yyscanner, pp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, yyscanner, pp, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != DSL_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, yyscanner, pp);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, yyscanner, pp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

#line 1037 "dsl_parser.y"

#include <stdarg.h>
#include "dsl_scanner.h"

int cmp_dsl_by_name(const void *keyval, const void *datum) {
    struct dsl *a = (struct dsl*)keyval;
    struct dsl *b = (struct dsl*)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr,bstr) // both not-null
            : ((NULL != astr) ? -1   // null B, move to end
               : ((NULL != bstr) ? 1 // null A, move to end
                  : 0)));            // both null, do nothing
}

void dsl_info(YYLTYPE *yylloc, yyscan_t yyscanner, const struct dsl_param *pp, const char *fmt, ...) {
    (void)yyscanner; // unused
    (void)print_error; // unused

    int lineno = 1;
    int colno = 1;
    if(pp->in_parse) { // yylloc exists inside yyparse
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr,"%s[%s] %5s: <dsl-%s>:%d:%d: ",
                  pp->pretty ? "\x1B[39m" : "",
                  "C%","INFO",pp->lang->name,lineno,colno);
    va_list ap;
    va_start(ap,fmt);
    (void)vfprintf(stderr,fmt,ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n",stderr);
}
void dsl_warn(YYLTYPE *yylloc, yyscan_t yyscanner, const struct dsl_param *pp, const char *fmt, ...) {
    (void)yyscanner; // unused
    (void)print_error; // unused

    int lineno = 1;
    int colno = 1;
    if(pp->in_parse) { // yylloc exists inside yyparse
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr,"%s[%s] %5s: <dsl-%s>:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;11m" : "",
                  "C%","WARN",pp->lang->name,lineno,colno);
    va_list ap;
    va_start(ap,fmt);
    (void)vfprintf(stderr,fmt,ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n",stderr);
}
void dsl_error(YYLTYPE *yylloc, yyscan_t yyscanner, const struct dsl_param *pp, const char *fmt, ...) {
    (void)yyscanner; // unused
    (void)print_error; // unused

    int lineno = 1;
    int colno = 1;
    if(pp->in_parse) { // yylloc exists inside yyparse
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr,"%s[%s] %5s: <dsl-%s>:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;9m" : "",
                  "C%","ERROR",pp->lang->name,lineno,colno);
    va_list ap;
    va_start(ap,fmt);
    (void)vfprintf(stderr,fmt,ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n",stderr);
}
void dsl_verbose(YYLTYPE *yylloc, yyscan_t yyscanner, const struct dsl_param *pp, const char *fmt, ...) {
    (void)yyscanner; // unused
    (void)print_error; // unused

    int lineno = 1;
    int colno = 1;
    if(pp->in_parse) { // yylloc exists inside yyparse
        lineno = yylloc->first_line;
        colno = yylloc->first_column;
    }

    (void)fprintf(stderr,"%s[%s] %5s: <dsl-%s>:%d:%d: ",
                  pp->pretty ? "\x1B[38;5;12m" : "",
                  "C%","VERB",pp->lang->name,lineno,colno);
    va_list ap;
    va_start(ap,fmt);
    (void)vfprintf(stderr,fmt,ap);
    va_end(ap);
    (void)fputs(pp->pretty ? "\x1B[39m\n" : "\n",stderr);
}
#undef LOGGING_FMT

static void print_error(YYLTYPE *yylloc, yyscan_t yyscanner) {
    (void)yylloc;
    (void)yyscanner;
}
