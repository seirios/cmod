
#ifndef CMOD_DSL_PARAM_H
#define CMOD_DSL_PARAM_H

#include <stdbool.h>

#include "parts/typedef_srt_alias.h"

#include "libsrt/sstring.h"
#include "libsrt/shmap.h"

#include "dsl.h"

/* Setup locations */
#if ! defined DSL_LTYPE && ! defined DSL_LTYPE_IS_DECLARED
typedef struct DSL_LTYPE DSL_LTYPE;
struct DSL_LTYPE {
    int first_line;
    int first_column;
    int last_line;
    int last_column;
};
#define DSL_LTYPE_IS_DECLARED 1
#define DSL_LTYPE_IS_TRIVIAL 1
#endif

struct dsl_param {
    const struct dsl *lang;     // pointer to DSL
    FILE *fpout;        // output stream
    srt_string *line;   // current input line
    srt_vector *astlist;        // parsed DSL statements
    const char *errtxt; // error text, to be underlined
    char *block_comment;        // block comment operator
    struct ast *expr;   // expression
    DSL_LTYPE errlloc;  // error location
    ssize_t nerr;       // number of errors seen
    size_t level;       // current tree depth
    int init;   // scanner start condition
    int silent; // silence level
    int hstr_start;     // start after HERESTR
    bool errabort;      // abort on error?
    bool pretty;        // pretty print?
    bool verbose;       // verbose logging?
    bool in_parse;      // inside parse?
};

#endif
