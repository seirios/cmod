CMOD ?= cmod
CMODFLAGS ?=

INDENT ?= indent
INDENT_FLAGS := -linux -nut -i4 -l100 -nce -c0 -cd0

# C% generic rules for C
%.c: %.cm
	$(CMOD) $(CMODFLAGS) -o $@ $<
	@$(INDENT) $(INDENT_FLAGS) $@

%.h: %.hm
	$(CMOD) $(CMODFLAGS) -o $@ $<
	@$(INDENT) $(INDENT_FLAGS) $@

# C% generic rules for Flex/Bison
%.l: %.lm
	$(CMOD) $(CMODFLAGS) -o $@ $<

%.y: %.ym
	$(CMOD) $(CMODFLAGS) -o $@ $<
