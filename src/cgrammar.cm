%include "cmod/autoarr.hm"
%include "cmod/ralloc.hm"
%include "cmod/variant.hm"

%include "cgrammar.hm"
%include "cmod.ext_tab.hm"

%include [c,sys] "assert.h"
%include [c] "ralloc.h"
%include [c] "cmod.ext.h"
%include [c] "util_string.h"

%include "autotypes.hm"

%recall std:variant_funcs (c_designator) <- (c_designator)
%recall std:variant_funcs (c_initializer) <- (c_initializer)
%recall std:variant_funcs (c_declarator) <- (c_declarator)
%recall std:variant_funcs (c_specifier) <- (c_specifier:type,c_specifier:value,
                                            name:type_col = type)
%recall std:variant_funcs (c_typespec) <- (c_typespec:type,c_typespec:value,
                                           name:type_col = type)

%recall std:autoarr_funcs:box (arr_cdi)
%recall std:autoarr_funcs (arr_cdi)
%recall std:autoarr_funcs:box (arr_cdd)
%recall std:autoarr_funcs (arr_cdd)

/* reduce designated initializer to string */
%def arr_cdi_to_str {
    for(size_t i = 0; i < x->len; ++i) {
        srt_string *prefix = NULL;
        srt_string *suffix = NULL;

        const struct c_designator *dsg = &x->arr[i].dsg;
        const struct c_initializer *ini = &x->arr[i].ini;

        %dsl str %{
            oom(`return 1;`);
            new(prefix);
            new(suffix);
        %}

        switch(ini->type) {
            case ENUM_C_INITIALIZER(EXPR):
                switch(dsg->type) {
                    case ENUM_C_DESIGNATOR(ARRAY):
                    {
                        srt_string *tmp = vstr_join(C_DESIGNATOR_ARRAY_get(*dsg),' ');
                        if(ss_void == tmp) return 1;
                        %dsl str %{
                            :suffix < `[` <= tmp < `]`;
                        %}
                        ss_free(&tmp);
                    }
                        break;
                    case ENUM_C_DESIGNATOR(FIELD):
                        %dsl str %{
                            :suffix < `.` << `C_DESIGNATOR_FIELD_get(*dsg)`;
                        %}
                        break;
                    case ENUM_C_DESIGNATOR(PTR):
                        %dsl str %{
                            :suffix <- `->` << `C_DESIGNATOR_PTR_get(*dsg)`;
                        %}
                        break;
                    case ENUM_C_DESIGNATOR(INVALID):
                        if(is_comparison) return 2; // cannot be empty for comparison
                        break;
                }
                break;
            case ENUM_C_INITIALIZER(COMP):
            {
                arr_cdi* comp = C_INITIALIZER_COMP_get(*ini);
                switch(dsg->type) {
                    case ENUM_C_DESIGNATOR(ARRAY):
                    {
                        if(NULL != comp->cast) return 4;
                        srt_string *tmp = vstr_join(C_DESIGNATOR_ARRAY_get(*dsg),' ');
                        if(ss_void == tmp) return 1;
                        %dsl str %{
                            :suffix < `[` <= tmp < `]`;
                        %}
                        ss_free(&tmp);
                    }
                        break;
                    case ENUM_C_DESIGNATOR(FIELD):
                        if(NULL != comp->cast) return 4;
                        %dsl str %{
                            :suffix < `.` << `C_DESIGNATOR_FIELD_get(*dsg)`;
                        %}
                        break;
                    case ENUM_C_DESIGNATOR(PTR):
                    {
                        const char *close = (NULL != comp->cast) ? ")))" : "))";
                        %dsl str %{
                            :prefix <- `(*(`;
                        %}
                        if(NULL != comp->cast) { %dsl str %{
                            :prefix < `(` << `comp->cast` <- `*)(`;
                        %}}
                        %dsl str %{
                            :suffix < `.` << `C_DESIGNATOR_PTR_get(*dsg)` << close;
                        %}
                    }
                        break;
                    case ENUM_C_DESIGNATOR(INVALID): return 2; break;
                }
            }
                break;
            case ENUM_C_INITIALIZER(INVALID): break;
        }

        vec_string *new_name = NULL;
        char *csuffix = NULL;
        char *cprefix = NULL;
        { // add prefix and suffix
            csuffix <- %| cmod:ss_strdup_free (suffix,oom=`return 1;`) |%
            cprefix <- %| cmod:ss_strdup_free (prefix,oom=`return 1;`) |%
            vec_string *tmp <- %| cmod:sv_alloc_ptr (`sv_len(name) + 2`,oom=`return 1;`) |%
            sv_push(&tmp,&cprefix);
            sv_cat(&tmp,name); // borrow name
            sv_push(&tmp,&csuffix);
            new_name = tmp;
        }

        int ret;
        switch(ini->type) {
            case ENUM_C_INITIALIZER(EXPR):
            {
                const vec_string *expr = C_INITIALIZER_EXPR_get(*ini);
                const char *firstitem = sv_at_ptr(expr,0);
                bool is_string_literal = ('"' == firstitem[0]);
                if(is_comparison)
                    ss_cat_c(buf,(i > 0) ? " && (" : "(",
                             is_string_literal ? "strncmp(" : "");
                %foreach [const,deref,svec=`char*`] x (new_name) %{
                    ss_cat_c(buf,${x});
                %}
                const char *cmp_eq = is_string_literal ? "," : " == ";
                const char *nocmp_eq = (dsg->type != ENUM_C_DESIGNATOR(INVALID)) ? " = " : "";
                ss_cat_c(buf,is_comparison ? cmp_eq : nocmp_eq);
                %foreach [deref,const,svec=`char*`] x (expr) %{
                    ss_cat_c(buf,${x});
                %}
                if(is_comparison && is_string_literal) {
                    ss_cat_cn1(buf,',');
                    ss_cat_int(buf,strlen(firstitem) - 1);
                    ss_cat_c(buf,") == 0)");
                } else ss_cat_c(buf,is_comparison ? ")" : ", ");
            }
                break;
            case ENUM_C_INITIALIZER(COMP):
                if(is_comparison) ss_cat_c(buf,(i > 0) ? "\n && (" : "\n (");
                if((ret = arr_cdi_to_str(C_INITIALIZER_COMP_get(*ini),new_name,buf,is_comparison))) // recurse
                    return ret; // return from the deep
                if(is_comparison) ss_cat_c(buf,")\n");
                break;
            case ENUM_C_INITIALIZER(INVALID): break;
        }

        { // remove prefix and suffix
            sv_free(&new_name); // unborrow name
            free(cprefix);
            free(csuffix);
        }
    }

    return 0;
}

%def arr_cdd_to_decl {
    %dsl vstr %{
        OOM `return 1;`;
        OPTINIT `decl->sv_decl`, 32;
    %}

    /* concatenate pointer */
    %dsl vstr %{
        CAT `x->pointer`,`decl->sv_decl`;
    %}

    int ret;
    for(size_t i = 0; i < x->len; ++i) {
        struct c_declarator *dcl = &x->arr[i];
        switch(dcl->type) {
            case ENUM_C_DECLARATOR(IDENT):
                decl->name = C_DECLARATOR_IDENT_get(*dcl); // const borrow
                decl->iname = SETIX_set(sv_len(decl->sv_decl)); // store name index
                %recall cmod:sv_push (`decl->sv_decl`) <- (`decl->name`,oom=`return 1;`)
                C_DECLARATOR_IDENT_get(*dcl) = NULL; // hand-over
                break;
            case ENUM_C_DECLARATOR(ARRAY):
                %dsl vstr %{
                    ADDSTR `"["`,`decl->sv_decl`;
                    CAT `C_DECLARATOR_ARRAY_get(*dcl)`,`decl->sv_decl`;
                    ADDSTR `"]"`,`decl->sv_decl`;
                %}
                break;
            case ENUM_C_DECLARATOR(FUNC):
                /* save function arguments (identifiers, initializers, types, start/end indices) */
                if(NULL == decl->sv_fargs) { // only for first declarator (outermost)
                    decl->sv_fargs = dcl->ids;      dcl->ids = NULL; // hand-over
                    decl->sv_fargs_init = dcl->ini; dcl->ini = NULL; // hand-over
                    decl->sv_fargs_type = dcl->typ; dcl->typ = NULL; // hand-over
                    decl->sv_fargs_ptrs = dcl->ptr; dcl->ptr = NULL; // hand-over
                    /* inclusive start index */
                    decl->fargs_i0 = SETIX_set(sv_len(decl->sv_decl) + 1);
                    /* inclusive end index */
                    decl->fargs_i1 = SETIX_set((sv_len(decl->sv_decl) + 1) +
                                               sv_len(C_DECLARATOR_FUNC_get(*dcl)) - 1);
                }
                %dsl vstr %{
                    ADDSTR `"("`,`decl->sv_decl`;
                    CAT `C_DECLARATOR_FUNC_get(*dcl)`,`decl->sv_decl`;
                    ADDSTR `")"`,`decl->sv_decl`;
                %}
                break;
            case ENUM_C_DECLARATOR(DECL):
                %dsl vstr %{ ADDSTR `"("`,`decl->sv_decl`; %}
                if((ret = arr_cdd_to_decl(C_DECLARATOR_DECL_get(*dcl),decl))) // recurse
                    return ret; // return from the deep
                %dsl vstr %{ ADDSTR `")"`,`decl->sv_decl`; %}
                break;
            case ENUM_C_DECLARATOR(INVALID):
                %recall cmod:internal_error_abort
                break;
        }
    }

    return 0;
}

%def decl_to_str {
    srt_string *ss <- %| cmod:ss_alloc (oom=`return ss_void;`) |%

    if(NULL != decl->sv_spec) {
        %foreach [const,svec=`struct c_specifier`] spec (decl->sv_spec) %{
            switch(${spec}->type) {
%map [notnull = #text] c_specifier:type %%{
                case ENUM_C_SPECIFIER($$U{name}):
                    ss_cat_c(&ss,C_SPECIFIER_$$U{name}_get($p{spec})," ");
                    break;
%%}
                case ENUM_C_SPECIFIER(TYPE):
                    ss_cat_c(&ss,C_TYPESPEC_get(C_SPECIFIER_TYPE_get($p{spec}))," ");
                    break;
                case ENUM_C_SPECIFIER(INVALID):
                    %recall cmod:internal_error_abort
                    break;
            }
        %}
    }

    for(size_t i = 0; i < sv_len(decl->pointer); ++i)
        ss_cat_c(&ss,sv_at_ptr(decl->pointer,i)," ");

    static const char argv_suffix[] = "__args";
    if(decl->fargs_i0.set && decl->fargs_i1.set && decl->iname.set) { // function declarator
        for(size_t i = 0; i < decl->iname.ix; ++i)
            ss_cat_c(&ss,sv_at_ptr(decl->sv_decl,i)," ");

        if(is_statement) ss_cat_c(&ss,"( ",fname,fsuffix," ) ");
        else ss_cat_c(&ss,fname,fsuffix," ");

        for(size_t i = decl->iname.ix + 1; i < decl->fargs_i0.ix; ++i)
            ss_cat_c(&ss,sv_at_ptr(decl->sv_decl,i)," ");

        if(decl->named) {
            ss_cat_c(&ss,const_named ? "const " : "","struct ",
                     decl->name,argv_suffix," ");
            ss_cat(&ss,decl->wrapper);
            ss_cat_cn1(&ss,' ');
        } else for(size_t i = decl->fargs_i0.ix; i <= decl->fargs_i1.ix; ++i)
            ss_cat_c(&ss,sv_at_ptr(decl->sv_decl,i)," ");

        for(size_t i = decl->fargs_i1.ix + 1; i < sv_len(decl->sv_decl); ++i)
            ss_cat_c(&ss,sv_at_ptr(decl->sv_decl,i)," ");
    } else if(NULL != decl->sv_decl) { // non-function declarator
        %foreach [const,deref,svec=`char*`] item (decl->sv_decl) %{
            ss_cat_c(&ss,${item}," ");
        %}
    }

    if(is_statement) ss_cat_cn1(&ss,';');

    return ss;
}

%def cini_to_str {
    if(NULL == ini || ini->type == ENUM_C_INITIALIZER(INVALID))
        return ss_void;
    srt_string *ss <- %| cmod:ss_alloc (oom=`return ss_void;`) |%

    srt_string *tmp = NULL;
    switch(ini->type) {
        case ENUM_C_INITIALIZER(EXPR):
            tmp = vstr_join(C_INITIALIZER_EXPR_get(*ini),' ');
            if(ss_void == tmp) return ss_void;
            ss_cat(&ss,tmp);
            ss_free(&tmp);
            break;
        case ENUM_C_INITIALIZER(COMP):
            if(!arr_cdi_to_str(C_INITIALIZER_COMP_get(*ini),NULL,&tmp,false)) {
                char *cast = C_INITIALIZER_COMP_get(*ini)->cast;
                if(NULL != cast) ss_cat_c(&ss,"(",cast,")");
                ss_cat_c(&ss,"{ ");
                ss_cat(&ss,tmp);
                ss_cat_c(&ss," }");
                ss_free(&tmp);
            }
            break;
        case ENUM_C_INITIALIZER(INVALID): /* handled above */ break;
    }

    return ss;
}
