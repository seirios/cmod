%include "util_string.hm"
%include "cmod.ext_tab.hm"

%include "cmod/sort.hm"
%include "cmod/bitwise.hm"
%include [c] "ralloc.h"
%include [c,sys] "assert.h"

%unittest [verbatim] %{
#include "util_string.h"
%include "cmod.ext_tab.hm"

struct param *glob_pp = NULL;
struct cmod_ralloc_params ralloc_glob = { 0 };
%}

/*  compare srt_strings as C strings */
int cmp_sstr(const void *keyval, const void *datum) {
    const srt_string *a = *(const srt_string**)keyval;
    const srt_string *b = *(const srt_string**)datum;
    const char *astr = ss_to_c(a);
    const char *bstr = ss_to_c(b);
    return ((NULL != astr && NULL != bstr) ? strcmp(astr,bstr) // both good
            : ((NULL != astr) ? -1   // bad B, move to end
               : ((NULL != bstr) ? 1 // bad A, move to end
                  : 0)));            // both bad, do nothing
}

%unittest ascidchar %{
    cr_assert(all(
        eq(int,${#NAME}('5',true),'_'),
        eq(int,${#NAME}('5',false),'5'),
        eq(int,${#NAME}('a',true),'a'),
        eq(int,${#NAME}('a',false),'a'),
        eq(int,${#NAME}('_',true),'_'),
        eq(int,${#NAME}('_',false),'_'),
        eq(int,${#NAME}('@',true),'_'),
        eq(int,${#NAME}('@',false),'_')
    ));
%}

/* check if string is empty or whitespace */
%def strisspace {
    if(NULL == s) return true;
    for(const char *c = s; '\0' != *c; c++)
        if(!isspace((unsigned char)*c))
            return false;
    return true;
}

%unittest strisspace %{
    cr_assert(all(
        eq(int,${#NAME}(NULL),true),
        eq(int,${#NAME}(""),true),
        eq(int,${#NAME}("   "),true),
        eq(int,${#NAME}(" \f\t\v\n "),true),
        eq(int,${#NAME}("a"),false),
        eq(int,${#NAME}(" # "),false)
    ));
%}

/* check if string is C_IDENTIFIER */
%def striscid {
    if(NULL == s) return false;
    const char *c = s;
    if('_' != *c && !isalpha((unsigned char)*c))
        return false;
    for(++c; '\0' != *c; ++c)
        if('_' != *c && !isalnum((unsigned char)*c))
            return false;
    return true;
}

%unittest striscid %{
    cr_assert(all(
        eq(int,${#NAME}(NULL),false),
        eq(int,${#NAME}(""),false),
        eq(int,${#NAME}("_cid"),true),
        eq(int,${#NAME}("_5a_"),true),
        eq(int,${#NAME}("var5"),true),
        eq(int,${#NAME}("5a"),false),
        eq(int,${#NAME}("@var"),false)
    ));
%}


inline static int is_char_ex(int c) {
    return ('?' == c || '!' == c || '@' == c || '&' == c || ':' == c);
}

inline static int is_char_ext(int c) {
    return (is_char_ex(c) || '-' == c || '+' == c || '.' == c);
}

/* check if string is IDENTIFIER_EXT */
%def strisidext {
    if(NULL == s) return false;
    const char *c = s;
    if('_' != *c && !isalpha((unsigned char)*c)
                 && !is_char_ex((unsigned char)*c))
        return false;
    for(++c; '\0' != *c && '/' != *c; ++c)
        if('_' != *c && !isalnum((unsigned char)*c)
                     && !is_char_ext((unsigned char)*c))
            return false;
    if('\0' == *c) return true;
    if(!isdigit((unsigned char)*++c)) return false; // at least one digit
    for(++c; '\0' != *c; ++c)
        if(!isdigit((unsigned char)*c))
            return false;
    return true;
}

%unittest strisidext %{
    cr_assert(all(
        eq(int,${#NAME}(NULL),false),
        eq(int,${#NAME}("_cid"),true),
        eq(int,${#NAME}("_5a_"),true),
        eq(int,${#NAME}("var5"),true),
        eq(int,${#NAME}("5a"),false),
        eq(int,${#NAME}("@var"),true),
        eq(int,${#NAME}("_:?!@&+-.what?"),true),
        eq(int,${#NAME}("_:?!@&+-.what?/123"),true),
        eq(int,${#NAME}("_:?!@&+-.what?/a"),false),
        eq(int,${#NAME}("_:?!@&+-.what?/"),false),
        eq(int,${#NAME}("_:?!@/+-.what?/"),false)
    ));
%}


/* check if string is C_IDENTIFIER with optional slash + number at end */
%def striscidslash {
    if(NULL == s) return false;
    const char *c = s;
    if('_' != *c && !isalpha((unsigned char)*c))
        return false;
    for(++c; '\0' != *c && '/' != *c; ++c)
        if('_' != *c && !isalnum((unsigned char)*c))
            return false;
    if('\0' == *c) return true;
    if(!isdigit((unsigned char)*++c)) return false; // at least one digit
    for(++c; '\0' != *c; ++c)
        if(!isdigit((unsigned char)*c))
            return false;
    return true;
}

%unittest striscidslash %{
    cr_assert(all(
        eq(int,${#NAME}(NULL),false),
        eq(int,${#NAME}(""),false),
        eq(int,${#NAME}("_cid"),true),
        eq(int,${#NAME}("_5a_"),true),
        eq(int,${#NAME}("var/5"),true),
        eq(int,${#NAME}("a/a"),false),
        eq(int,${#NAME}("@var"),false)
    ));
%}



/* unescape C escapes */
#define IMPL_MAXHEX 16
#define INRANGE(c,a,b) ((c) >= (a) && (c) <= (b))
#define IS_OCTAL(c) INRANGE(c,'0','7')
#define IS_HEXADECIMAL(c) (INRANGE(c,'0','9') \
    || INRANGE(c,'a','f') || INRANGE(c,'A','F'))
#define IS_C_SRC_CHAR(c) (INRANGE(c,'A','Z') \
    || INRANGE(c,'a','z') || INRANGE(c,'0','9') || (c) == ' ' \
    || (INRANGE(c,'!','/') && (c) != '$') \
    || INRANGE(c,':','?') || INRANGE(c,'[','_') || INRANGE(c,'{','~') \
    || (c) == '\t' || (c) == '\v' || (c) == '\f')
%def strcunesc {
    int rc = 0;
    srt_string *ss <- %| cmod:ss_alloc (len,oom = `rc = 1; goto fail;`) |%;

    unsigned long byte;
    unsigned long long uchar;
    size_t ioct = 0;
    char octbuf[4]; // up to 3 octal chars
    size_t ihex = 0;
    char hexbuf[IMPL_MAXHEX + 1]; // up to IMPL_MAXHEX hex chars
    size_t iuni = 0;
    char unibuf[9]; // up to 8 hex chars

    bool inclass;
    enum {
        NORMAL,
        ESCAPE,
        OCTAL,
        HEXADECIMAL,
        UNICODE_SHORT,
        UNICODE_LONG
    } mode = NORMAL;

    for(size_t i = 0; i < len; ++i) {
        const char c = s[i];
begin:
        switch(mode) {
            case NORMAL:
                switch(c) {
                    case '\\':
                        mode = ESCAPE;
                        continue;
                    default:
                        if(!IS_C_SRC_CHAR(c)) {
                            rc = 2; // ERROR: stray char
                            goto fail;
                        }
                        ss_cat_cn1(&ss,c);
                        mode = NORMAL;
                        continue;
                }
                break;
            case ESCAPE:
                switch(c) {
                    case '\\': /* FALLTHROUGH */
                    case '\'': /* FALLTHROUGH */
                    case '\"': /* FALLTHROUGH */
                    case  '?': /* FALLTHROUGH */
                        ss_cat_cn1(&ss,c);
                        mode = NORMAL;
                        continue;
                %map {a,b,f,n,r,t,v}(x) %{
                    case $s{x}:
                        ss_cat_cn1(&ss,$qs{x});
                        mode = NORMAL;
                        continue;
                %}
                    case '0': /* FALLTHROUGH */
                    case '1': /* FALLTHROUGH */
                    case '2': /* FALLTHROUGH */
                    case '3': /* FALLTHROUGH */
                    case '4': /* FALLTHROUGH */
                    case '5': /* FALLTHROUGH */
                    case '6': /* FALLTHROUGH */
                    case '7': /* FALLTHROUGH */
                        octbuf[ioct++] = c;
                        mode = OCTAL;
                        continue;
                    case 'x':
                        mode = HEXADECIMAL;
                        continue;
                    case 'u':
                        mode = UNICODE_SHORT;
                        continue;
                    case 'U':
                        mode = UNICODE_LONG;
                        continue;
                    default:
                        rc = 3; // ERROR: invalid escape
                        goto fail;
                }
                break;
            case OCTAL:
                if((inclass = IS_OCTAL(c)))
                    octbuf[ioct++] = c;
                if(!inclass || ioct == 3) {
%snippet [here] octal_action = %{
                    assert(ioct > 0);
                    octbuf[ioct] = '\0'; // NUL terminate
                    byte = strtoul(octbuf,NULL,8);
                    if(byte > 0xff) {
                        rc = 5; // ERROR: out of range
                        goto fail;
                    }
                    ss_cat_cn1(&ss,byte);
%}
                    mode = NORMAL;
                    ioct = 0; // reset
                }
                if(inclass) continue;
                else goto begin;
            case HEXADECIMAL:
                if((inclass = IS_HEXADECIMAL(c)))
                    hexbuf[ihex++] = c;
                if(!inclass || ihex == IMPL_MAXHEX) {
%snippet [here] hexadecimal_action = %{
                    if(ihex == 0) {
                        rc = 4; // ERROR: incomplete escape
                        goto fail;
                    }
                    hexbuf[ihex] = '\0'; // NUL terminate
                    byte = strtoul(hexbuf,NULL,16);
                    if(byte > 0xff) {
                        rc = 5; // ERROR: out of range
                        goto fail;
                    }
                    ss_cat_cn1(&ss,byte);
%}
                    mode = NORMAL;
                    ihex = 0; // reset
                }
                if(inclass) continue;
                else goto begin;
                break;
            case UNICODE_SHORT: /* FALLTHROUGH */
            case UNICODE_LONG:  /* FALLTHROUGH */
                if((inclass = IS_HEXADECIMAL(c)))
                    unibuf[iuni++] = c;
                if(!inclass || (mode == UNICODE_SHORT && iuni == 4)
                            || (mode == UNICODE_LONG  && iuni == 8)) {
%snippet [here] unicode_action = %{
                    if(iuni != 4 && iuni != 8) {
                        rc = 4; // ERROR: incomplete escape
                        goto fail;
                    }
                    unibuf[iuni] = '\0'; // NUL terminate
                    uchar = strtoull(unibuf,NULL,16);
                    if((uchar < 0xA0 && uchar != 0x24 && uchar != 0x40 && uchar != 0x60) // basic or control
                            || (uchar >= 0xD800 && uchar <= 0xDFFF) // high surrogate
                            || uchar > 0x10FFFF // not Unicode code point
                        ) {
                        rc = 6; // ERROR: out of range
                        goto fail;
                    }
                    if(EOF == ss_putchar(&ss,uchar)) { // add UTF8-encoded character
                        rc = 1; // ERROR: string overflow (out of memory)
                        goto fail;
                    }
%}
                    mode = NORMAL;
                    iuni = 0; // reset
                }
                if(inclass) continue;
                else goto begin;
                break;
        }
    }

    /* end of input */
    switch(mode) {
        case NORMAL: /* all good */ break;
        case ESCAPE:
            rc = 4; // ERROR: incomplete escape
            goto fail;
        case OCTAL:
            %recall octal_action
            break;
        case HEXADECIMAL:
            %recall hexadecimal_action
            break;
        case UNICODE_SHORT: /* FALLTHROUGH */
        case UNICODE_LONG:  /* FALLTHROUGH */
            %recall unicode_action
            break;
    }

    *out = ss;
    ss = NULL; // hand-over
fail:
    ss_free(&ss);

    return rc;
}

%unittest strcunesc %{
%table testcases:pass = (esc,unesc) %%TSV{
	noescapes							noescapes
	line\\nbreak						line\nbreak
	double\\\\backslash					double\\backslash
	nul\\0byte							nul
	letter\\x41Z						letterAZ
	letter\\102Z						letterBZ
	\\x1BisEscape						\x1BisEscape
	inRange\\x000000000000005a			inRangeZ
	max16hexChar\\x0000000000000073e	max16hexCharse
	uchar\\u0394						uchar\u0394
	uchar\\U00000394					uchar\U00000394
%%}
%table testcases:fail = (ret,esc) %%TSV{
	2	stray\x1B
	3	invalid\\e
	4	incomplete\\x
	4	incomplete\\u000
	4	incomplete\\U000000
	5	outOfRange\\567
	5	outOfRange\\xFFF
	6	outOfRange\\U001a
	6	outOfRange\\u0000001a
	6	outOfRange\\U0011ffff
%%}
    srt_string *s[%tabsize (testcases:pass)] = { 0 };
    cr_assert(all(
%map [sep=`,`] testcases:pass %%{
        eq(int,0,${#NAME}(strlen($$S{esc}),$$S{esc},&s[$${#NR}]))
%%}
    ));
    cr_assert(all(
        eq(sz,3,strlen(ss_to_c(s[3]))),
        eq(sz,8,strlen(ss_to_c(s[5]))),
%map [sep=`,`] testcases:pass %%{
        eq(str,(char*)ss_to_c(s[$${#NR}]),$$S{unesc})
%%}
    ));
    cr_assert(all(
%map [sep=`,`] testcases:fail %%{
        eq(int,$${ret},${#NAME}(strlen($$S{esc}),$$S{esc},&s[0]))
%%}
    ));
%}

/* escape with C escapes (bytes) */
%def strcesc {
    int rc = 0;
    srt_string *ss <- %| cmod:ss_alloc (len,oom = `rc = 1; goto fail;`) |%;

    for(size_t i = 0; i < len; ++i) {
        const char c = s[i];
        switch(c) {
        %map {a,b,f,n,r,t,v}(x) %{
            case '\${x}':
                ss_cat_cn1(&ss,'\\');
                ss_cat_cn1(&ss,$s{x});
                break;
        %}
        %map {\,',",?}(x) %{
            case '\${x}':
                ss_cat_cn1(&ss,'\\');
                ss_cat_cn1(&ss,$sq{x});
                break;
        %}
            default:
                if(isprint((unsigned char)c)) ss_cat_cn1(&ss,c);
                else if('\0' == c) {
                    ss_cat_cn1(&ss,'\\');
                    ss_cat_cn1(&ss,'0');
                } else { // encode as hex byte
                    uint8_t u = c;
                    ss_cat_cn1(&ss,'\\');
                    ss_cat_cn1(&ss,'x');
                    switch(u / 16) {
                    %map {0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f}(x) %{
                        case ${#NR}: ss_cat_cn1(&ss,$s{x}); break;
                    %}
                    }
                    switch(u % 16) {
                    %map {0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f}(x) %{
                        case ${#NR}: ss_cat_cn1(&ss,$s{x}); break;
                    %}
                    }
                }
                break;
        }
    }

    *out = ss;
    ss = NULL; // hand-over
fail:
    ss_free(&ss);

    return rc;
}

%unittest strcesc %{
    const char str[] = "\x1b""ade""\xff";
    srt_string *ss = NULL;
    int ret = ${#NAME}(strlen(str),str,&ss);
    cr_assert(all(
        eq(int,ret,0),
        eq(str,(char*)ss_to_c(ss),"\\x1bade\\xff")
    ));
%}

%unittest strcesc %{
    const char str[] = "\\\"\?";
    srt_string *ss = NULL;
    int ret = ${#NAME}(strlen(str),str,&ss);
    cr_assert(all(
        eq(int,ret,0),
        eq(str,(char*)ss_to_c(ss),"\\\\\\\"\\\?")
    ));
%}


/* split string in-place by separator */
%def strsplit {
    vec_string *parts <- %| cmod:sv_alloc_ptr (8,oom = `goto fail;`) |%
    bool inword = false;
    for(char *c = s; '\0' != *c; c++) {
        if(!issep((unsigned char)*c) && !inword) {
            parts <- %| cmod:sv_push (c,oom = `goto fail;`) |%
            inword = true;
        } else if(issep((unsigned char)*c) && inword) {
            *c = '\0'; // space to NUL
            inword = false;
        }
    }
    goto pass;
fail:
    sv_free(&parts);
pass:
    return parts;
}

%unittest [verbatim] strsplit %{
static int iscommasep(int c) { return c == ','; }
%}

%unittest strsplit %{
    char buf[] = "foo,bar,baz";
    vec_string *spl = ${#NAME}(buf,iscommasep);
    cr_assert(all(
        eq(sz,sv_len(spl),3),
        eq(str,sv_at_ptr(spl,0),"foo"),
        eq(str,sv_at_ptr(spl,1),"bar"),
        eq(str,sv_at_ptr(spl,2),"baz")
    ));
%}

/* compute Levenshtein distance of two strings (naive Wagner-Fischer) */
%def strdist {
    size_t m = strlen(s1);
    size_t n = strlen(s2);

    if(m == 0) return n;
    if(n == 0) return m;

    size_t d[m + 1][n + 1]; // VLA
    memset(d,0x0,((m + 1) * (n + 1)) * sizeof(size_t)); // clear

    for(size_t i = 1; i <= m; ++i) d[i][0] = i;
    for(size_t j = 1; j <= n; ++j) d[0][j] = j;

    for(size_t j = 1; j <= n; ++j)
        for(size_t i = 1; i <= m; ++i) {
            size_t min[3] = {
                d[i - 1][j] + 1,                           // deletion
                d[i][j - 1] + 1,                           // insertion
                d[i - 1][j - 1] + (s1[i - 1] != s2[j - 1]) // substitution
            };
            %recall std:sort_bitonic:3 (min,size_t)
            d[i][j] = min[0];
        }

    return d[m][n];
}

%unittest strdist %{
    cr_assert(all(
        eq(sz,${#NAME}("same","same"),0),
        eq(sz,${#NAME}("samf","same"),1),
        eq(sz,${#NAME}("smea","same"),2),
        eq(sz,${#NAME}("eams","same"),2),
        eq(sz,${#NAME}("kitten","sitting"),3)
    ));
%}

/* concatenate string line-by-line */
%def ss_cat_byline {
    const srt_string *nl = ss_crefs("\n");
    size_t nnl = ss_split(str, nl, NULL, 0);
    srt_string_ref linrefs[nnl]; // VLA
    nnl = ss_split(str, nl, linrefs, nnl);
    for(size_t i = 0; i < nnl; ++i) {
        const srt_string *line = ss_ref(&linrefs[i]);
        if(str_bool) ss_cat_cn1(ss,'"');
        ss_cat_c(ss,pre);
        ss_cat(ss,line);
        if(str_bool) ss_cat_cn1(ss,'"');
        /* all lines but last are sure to have newline after split */
        if(!supnl_bool && i < nnl - 1)
            ss_cat_cn1(ss,'\n');
    }
    if(!supnl_bool && '\n' == ss_at(str, ss_len(str) - 1)) // last newline
        ss_cat_cn1(ss,'\n');
}

// TODO: unit test
%unittest ss_cat_byline %{
%}

/* parse regex substitution string */
%def strregsub_parse {
    srt_string *ssub = NULL;
    if(NULL == str || NULL == sub) goto fail;
    ssub <- %| cmod:ss_alloc (64,oom=`goto fail;`) |%;

    for(const char *c = sub; '\0' != *c; ++c) {
        if('\\' != *c) { // not escaped
            ss_cat_cn1(&ssub,*c);
            continue;
        }
        const char e = *++c; // escaped
        if(e >= '0' && e <= '9') { // submatch
            int ix = e - '0';
            if(pmatch[ix].rm_so == -1) continue;
            size_t pmatch_len = pmatch[ix].rm_eo - pmatch[ix].rm_so;
            ss_cat_cn(&ssub,&str[pmatch[ix].rm_so],pmatch_len);
        } else {
            char u; // unescaped char
            switch(e) {
                case '\\': u = '\\';   break;
                case 'e':  u = '\x1b'; break;
            %map {a,b,f,n,r,t,v}(x) %{
                case $s{x}: u = $qs{x}; break;
            %}
                default: goto fail; break;
            }
            ss_cat_cn1(&ssub,u); // append unescaped
        }
    }

    return ss_check(&ssub);

fail:
    ss_free(&ssub);
    return ss_void;
}

// TODO: unit test
%unittest strregsub_parse %{
%}


/* find and replace (all) regex matches */
%def strregsub {
    int rc = 0;
    bool notfound = false;

    srt_string *ssub = NULL; // parsed replacement string

    regmatch_t pmatch[10] = {{ 0 }}; // up to 9 sub-matches: \1 to \9
    const char *match = (regexec(&preg,rinp,%arrlen(pmatch),pmatch,0) == 0)
                      ? rinp + pmatch[0].rm_so : NULL;
    if(NULL == match) notfound = true;
    else do { // substitution
        /* append input up to match */
        ptrdiff_t delta = match - rinp;
        ss_cat_cn(newbuf,rinp,delta);

        /* parse replacement string */
        ssub = strregsub_parse(rinp,sub,pmatch);
        if(ss_void == ssub) { rc = 1; goto fail; }
        ss_cat(newbuf,ssub); // append
        ss_free(&ssub);

        rinp += pmatch[0].rm_eo; // advance after match
        if('\0' == *rinp || pmatch[0].rm_eo == 0) // consumed string or zero-length match
            break;
#if DEBUG
        fprintf(stderr,"`%s` %jd %jd\n",
                rinp,(intmax_t)pmatch[0].rm_so,(intmax_t)pmatch[0].rm_eo);
#endif
    } while(opt_global_bool
            && NULL != (match = ((regexec(&preg,rinp,%arrlen(pmatch),pmatch,0) == 0)
                              ? rinp + pmatch[0].rm_so : NULL)));

    *endptr = rinp;
    *notfound_o = notfound;
fail:
    ss_free(&ssub);

    return rc;
}

%unittest [verbatim] strregsub %{
%typedef srt_string;
%proto [named] static int test_${#NAME}(const char *inp, const char *pat, const char *sub, bool global, bool icase, bool ere, bool newline, srt_string **out);
%def test_${#NAME} {
    int rc = 0;

    srt_string *newbuf = NULL;

    srt_string *buf = ss_alloc(0);
    if(NULL == buf) { rc = 1; goto fail; }
    ss_cat_c(&buf,argv.inp); // copy input to buffer
    ss_cat_cn1(&buf,'\0'); // NUL-terminate

    regex_t preg;
    int cflags = 0L
            | (argv.icase ? REG_ICASE : 0L)
            | (argv.ere ? REG_EXTENDED : 0L)
            | (argv.newline ? REG_NEWLINE : 0L);
    if(regcomp(&preg,argv.pat,cflags)) { rc = -1; goto fail; }

    const char *rinp = ss_get_buffer_r(buf);
    bool notfound = false;

    if((rc = strregsub(rinp,preg,argv.sub,argv.global,&newbuf,&rinp,&notfound)))
        goto fail;
    ss_cat_c(&newbuf,rinp);

    *argv.out = newbuf;
    newbuf = NULL; // hand-over

fail:
    ss_free(&buf);
    ss_free(&newbuf);

    return rc;
}
%}

%unittest strregsub `Replacement without options` %{
    srt_string *out = NULL;
    cr_assert(all(
        eq(int,test_${#NAME}("bANANa","[AN]","_",.out=&out),0),
        eq(str,(char*)ss_to_c(out),"b_NANa")
    ));
%}

%unittest strregsub `Global replacement` %{
    srt_string *out = NULL;
    cr_assert(all(
        eq(int,test_${#NAME}("bANANa","[AN]","_",.global=true,.out=&out),0),
        eq(str,(char*)ss_to_c(out),"b____a")
    ));
%}

%unittest strregsub `Global and case-insensitive replacement` %{
    srt_string *out = NULL;
    cr_assert(all(
        eq(int,test_${#NAME}("bANANa","[AN]","_",.icase=true,.global=true,.out=&out),0),
        eq(str,(char*)ss_to_c(out),"b_____")
    ));
%}


%table "str_enums.json"

%def parse_str_mod {
    if(NULL == str) return 0;
    for(const char *s = str; '\0' != *s; ++s) {
        switch(*s) {
        %map str_enums %{
            %map ${name} %%{
            case $$s{char}:
                out->$L{prefix} %? ($b{stacking},true,`|=`,`=`) ${prefix}_$${value};
                break;
            %%}
        %}
            default:
                *err = *s; // blame
                return 1;
                break;
        }
    }
    return 0;
}

%def ss_cat_mod {
    if(!s || !src) return ss_void;

    bool failed = false;
    srt_string *y = NULL;

    if(mod.tran == TRAN_EMPTY && '\0' == ss_at(src,0))
        return ss_check(s); // do nothing

    y = ss_dup(src); // init transformed string
    if(NULL == y) return ss_void;

    /* apply trimming */
    %switch [bitflag,each] (mod.trim) %{
        TRIM_SPACE %<< ss_trim(&y); >>%
        TRIM_LAST %<< ss_popchar(&y); >>%
        TRIM_NL %<< ss_replace(&y,0,ss_crefs("\n"),ss_crefs("")); >>%
    %}

    { // apply transformation
        switch(mod.tran) {
            case TRAN_NONE: break;
            case TRAN_EMPTY: /* handled above */ break;
            case TRAN_SNIPPET:
            {
                srt_string *z <- %| cmod:ss_alloc (oom=`failed = true; goto cleanup;`) |%;
                char *buf = ss_get_buffer(y);
                for(size_t i = 0; i < ss_len(y); ++i) { // char by char
                    if('%' == buf[i] || '$' == buf[i])
                        ss_cat_cn1(&z,buf[i]); // escape with itself
                    ss_cat_cn1(&z,buf[i]);
                }
                ss_free(&y);
                y = z;
            }
                break;
            case TRAN_CID:
            {
                char *buf = ss_get_buffer(y);
                for(size_t i = 0; i < ss_len(y); ++i) // char by char
                    buf[i] = ascidchar((unsigned char)buf[i],i == 0);
            }
                break;
            case TRAN_CSTR:
                ss_enc_esc_c(&y);
                break;
            case TRAN_JSON:
                ss_enc_esc_json(&y,y);
                break;
            case TRAN_HEX:
                ss_enc_hex(&y,y);
                break;
            case TRAN_B64:
                ss_enc_b64(&y,y);
                break;
            case TRAN_LZH:
                ss_enc_lzh(&y,y);
                ss_enc_hex(&y,y);
                break;
        }
    }

    /* apply case folding */
    %@(1)switch (mod.fold) %{
        FOLD_NONE %<<>>%
        %map str_fold %{ FOLD_${value} %<< ss_${function}(&y); >>% %}
    %}

    /* append lead (stacking) */
    %@(1)switch [bitflag,each] (mod.lead) %{
        %map [rev] str_lead %{ LEAD_${value} %<< ss_cat_c(s,${lead}); >>% %}
    %}

    /* append wrap start (stacking) */
    %@(1)switch [bitflag,each] (mod.wrap) %{
        %map [rev] str_wrap %{ WRAP_${value} %<< ss_cat_c(s,$S{start}); >>% %}
    %}

    /* append prefix */
    %@(1)switch (mod.pre) %{
        PRE_NONE %<<>>%
        %map str_pre %{ PRE_${value} %<< ss_cat_cn1(s,$s{prefix}); >>% %}
    %}

    /* append modified string */
    ss_cat(s,y);

    /* append wrap end (stacking) */
    %map str_wrap %{
    if(%| std:bit_isset(`mod.wrap`,WRAP_${value}) |%) ss_cat_c(s,$S{end});
    %}

    /* append trail (stacking) */
    %@(1)switch [bitflag,each] (mod.trail) %{
        %map str_trail %{ TRAIL_${value} %<< ss_cat_c(s,${trail}); >>% %}
    %}

cleanup:
    ss_free(&y);

    return failed ? ss_void : ss_check(s);
}

%unittest [verbatim] ss_cat_mod %{
srt_string *s = NULL;

void init_${#NAME}(void) {
    ss_free(&s);
    s = ss_dup_c("test");
}
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,NULL,NULL,(struct str_mod){0,0,0,0,0,0,0});
    cr_assert(all(
        eq(ptr,res,ss_void),
        eq(sz,ss_len(s),4),
        eq(str,(char*)ss_to_c(s),"test");
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),12),
        eq(str,(char*)ss_to_c(s),"test str@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,TRAN_CID,0,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),12),
        eq(str,(char*)ss_to_c(s),"test_str_ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,FOLD_UPPER,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),12),
        eq(str,(char*)ss_to_c(s),"test STR@ING")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,0,WRAP_HSTR,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),20),
        eq(str,(char*)ss_to_c(s),"test%<<  str@ing >>%")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,0,WRAP_HSTR | WRAP_SQUOT,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),22),
        eq(str,(char*)ss_to_c(s),"test%<< ' str@ing' >>%")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,PRE_HTAG,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),13),
        eq(str,(char*)ss_to_c(s),"test# str@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,0,0,LEAD_COMMA,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),13),
        eq(str,(char*)ss_to_c(s),"test, str@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),NULL,
                               (struct str_mod){0,0,0,0,0,LEAD_NAME,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),12),
        eq(str,(char*)ss_to_c(s),"test str@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),"arg",
                               (struct str_mod){0,0,0,0,0,LEAD_NAME,TRAIL_COMMA});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),17),
        eq(str,(char*)ss_to_c(s),"testarg= str@ing,")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing"),"arg",
                               (struct str_mod){0,0,0,0,0,LEAD_NAME | LEAD_COMMA,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),17),
        eq(str,(char*)ss_to_c(s),"test,arg= str@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing  "),NULL,
                               (struct str_mod){TRIM_SPACE,0,0,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),11),
        eq(str,(char*)ss_to_c(s),"teststr@ing")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str@ing  "),NULL,
                               (struct str_mod){TRIM_SPACE|TRIM_LAST,0,0,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),10),
        eq(str,(char*)ss_to_c(s),"teststr@in")
    ));
%}

%unittest [init=init_ss_cat_mod] ss_cat_mod %{
    srt_string *res = ${#NAME}(&s,ss_crefs(" str%%ing"),NULL,
                               (struct str_mod){0,TRAN_SNIPPET,0,0,0,0,0});
    cr_assert(all(
        ne(ptr,res,ss_void),
        eq(sz,ss_len(s),13),
        eq(str,(char*)ss_to_c(s),"test str%%%ing")
    ));
%}


%def ss_dec_esc_c {
    if(!s) return ss_void;
    srt_string *ss = NULL;
    if(strcunesc(ss_len(*s),ss_get_buffer_r(*s),&ss) == 0) {
        ss_cpy(s,ss);
        ss_free(&ss);
        return ss_check(s);
    } else return ss_void;
}

%unittest ss_dec_esc_c `Escaped newline` %{
    srt_string *s = ss_dup_c("a\\nb");
    srt_string *sunesc = ${#NAME}(&s);
    cr_assert(all(
        ne(ptr,sunesc,ss_void),
        eq(str,(char*)ss_to_c(s),"a\nb")
    ));
%}

%unittest ss_dec_esc_c `Escaped byte` %{
    srt_string *s = ss_dup_c("a\\x1bi");
    srt_string *sunesc = ${#NAME}(&s);
    cr_assert(all(
        ne(ptr,sunesc,ss_void),
        eq(str,(char*)ss_to_c(s),"a""\x1b""i")
    ));
%}


%def ss_enc_esc_c {
    if(!s) return ss_void;
    srt_string *ss = NULL;
    if(strcesc(ss_len(*s),ss_get_buffer_r(*s),&ss) == 0) {
        ss_cpy(s,ss);
        ss_free(&ss);
        return ss_check(s);
    } else return ss_void;
}

%unittest ss_enc_esc_c `Escaped byte` %{
    srt_string *s = ss_dup_c("a\x1bi");
    srt_string *sesc = ${#NAME}(&s);
    cr_assert(all(
        ne(ptr,sesc,ss_void),
        eq(str,(char*)ss_to_c(s),"a\\x1bi")
    ));
%}


%def strget_f strget_array {
    const char **x = (const char**)array;
    return x[ix];
}

%def str_fuzzy_match {
    if(NULL == key || '\0' == key[0]) {
        *match_o = NULL;
        return false;
    }

    bool is_close = false;
    const char *fuzzy_match = NULL;
    for(size_t ix = 0, mindist = SIZE_MAX, minsubdist = SIZE_MAX; ix < arrlen; ++ix) {
        const char *str = getter(array,ix);
        size_t dist = strdist(key,str);
        if(strstr(str,key) == str && dist < minsubdist) { // starts with => is close
            minsubdist = dist;
            fuzzy_match = str;
            is_close = true;
        } else if(minsubdist == SIZE_MAX && dist < mindist) { // update distance, is close?
            mindist = dist;
            fuzzy_match = str;
            is_close = ((double)dist / strlen(str) <= threshold);
        }
    }

    *match_o = fuzzy_match;

    return is_close;
}

%unittest str_fuzzy_match %{
    static const char *pool[] = { "mine", "fine", "mein", "fein" };
    const char *match[5] = { 0 };

    bool found0 = ${#NAME}("",pool,%arrlen(pool),strget_array,0.5,&match[0]);
    bool found1 = ${#NAME}("sein",pool,%arrlen(pool),strget_array,0.5,&match[1]);
    bool found2 = ${#NAME}("fin",pool,%arrlen(pool),strget_array,0.5,&match[2]);
    bool found3 = ${#NAME}("waytoolong",pool,%arrlen(pool),strget_array,0.5,&match[3]);
    bool found4 = ${#NAME}("waytoolong",pool,%arrlen(pool),strget_array,5,&match[4]);

    cr_assert(all(
        eq(int,found0,0),
        eq(ptr,(void*)match[0],NULL),
        eq(int,found1,1),
        eq(str,(char*)match[1],"mein"),
        eq(int,found2,1),
        eq(str,(char*)match[2],"fine"),
        eq(int,found3,0),
        eq(int,found4,1),
        eq(str,(char*)match[4],"mine")
    ));
%}


%def vstr_join {
    srt_string *ss <- %| cmod:ss_alloc (oom=`return ss_void;`) |%;
    for(size_t i = 0; i < sv_len(sv); ++i) {
        const char *str = sv_at_ptr(sv,i);
        if(i > 0) ss_putchar(&ss,sep);
        ss_cat_c(&ss,str);
    }
    return ss;
}

%unittest vstr_join %{
    static const char *strings[] = { "my", "join", "test" };
    sv_strings <- %| cmod:static_string_array:to_vstr (strings,oom=``) |%
    cr_expect(eq(sz,sv_len(sv_strings),3));
    srt_string *ss = ${#NAME}(sv_strings,'_');
    cr_assert(all(
        ne(ptr,ss,ss_void),
        eq(str,(char*)ss_to_c(ss),"my_join_test")
    ));
%}

%def vstr_addstr_aux {
    if(!v || !*v) return S_FALSE;
    va_list ap;
    va_start(ap, c1);
    char *tmp = NULL;
    size_t op_cnt = 0;
    size_t push_cnt = 0;
    const void *next = c1;
    while(!s_varg_tail_ptr_tag(next)) { // last element tag
        if(next) {
            tmp = rstrdup(next);
            if(tmp && sv_push_ptr(v,tmp)) ++push_cnt;
            else %free(tmp);
        } else { // empty
            if(sv_push_ptr(v,tmp)) ++push_cnt;
        }
        next = (void*)va_arg(ap, void*);
        op_cnt++;
    }
    va_end(ap);
    return (op_cnt == push_cnt); // true when all arguments were pushed
}

%unittest vstr_addstr %{
    vec_string *x = NULL;
    x <- %| cmod:sv_alloc_ptr (oom=``) |%
    cr_expect(ne(ptr,x,NULL));

    bool ret = ${#NAME}(&x,"my","addstr","test");

    cr_assert(all(
        eq(int,ret,1),
        eq(sz,sv_len(x),3),
        eq(str,sv_at_ptr(x,0),"my"),
        eq(str,sv_at_ptr(x,1),"addstr"),
        eq(str,sv_at_ptr(x,2),"test")
    ));
%}

/* string <-> integer conversion */
%recall std:strtofp_check_funcs (static = ``,inline = ``)
%recall std:strtoint_check_funcs (static = ``,inline = ``)
%recall std:inttostr_funcs (static = ``,inline = ``)

%def uint_tostr {
    /* static strings for first 100 unsigned integers */
    static const char uint100[][3] = {
    %map [sep=`,`,nbrk=10,brk=`\n`] {0..100}(x) %{ $S{x} %}
    };
    if(n < 100) return uint100[n];
    else { // generate it
        int offset;
        buf, offset <- %| std:strumax (n) |%
        return buf + offset;
    }
}

%unittest uint_tostr %{
    char buf[STRUMAX_LEN];
    cr_assert(all(
        eq(str,(char*)uint_tostr(0,buf),"0"),
        eq(str,(char*)uint_tostr(123,buf),"123"),
        eq(str,(char*)uint_tostr(168984654,buf),"168984654")
    ));
%}
