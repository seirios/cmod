%once cmod_yylval

%comment Parser value types

%include "cgrammar.hm"

%table variants = (alias,name,description) %tsv{
	xint	xint			integer (any signedness)
	mcase	mod_case		%switch case
	opt		cmod_option		C% keyword option
%}

%table vectors = (alias,type,what) %tsv{
	vxint	vec_xint		integers (any signedness)
	vstr	vec_string		strings
	vvstr	vec_vec_string	vectors of strings
	vacdi	vec_arr_cdi		auto-growing arrays of C designated initializers
	vdecl	vec_decl		C declarators
	vmcase	vec_mcase		%switch cases
	vnarg	vec_namarg		snippet named arguments
	vvnarg	vec_vec_namarg	vectors of snippet named arguments
	vopt	vec_cmopt		C% keyword options
	vcspc	vec_cspec		C declaration specifiers
%}

%@(1)table [redef,hash = #name] "yylval.json"
%@(1)table yylval_variants = %json{
[
%map [sep=`,`] c_variants, variants %{
    {
        "name": $S{alias},
        "type": "struct ${name}",
        "description": $S{description},
        "free": "${name}_free(${#D}R{x});",
        "clear": "${#D}{x} = (struct ${name}){ 0 };",
        "print_fmt": "\"(type %d)\"",
        "print_val": "${#D}{x}.type"
    }
%}
]
%}
%@(1)table yylval_vectors = %json{
[
%map [sep=`,`] vectors %{
    {
        "name": $S{alias},
        "type": "${type}*",
        "description": "vector of ${what}",
        "free": "%| free_$C{type}p (${#D}b{x}) |%",
        "clear": "${#D}{x} = NULL;",
        "print_fmt": "\"(len %zu)\"",
        "print_val": "sv_len(${#D}{x})"
    }
%}
]
%}
%table-stack [hash = #name,lazy] yylval (yylval,yylval_variants,yylval_vectors)

#ifndef CMOD_YYLVAL_H
#define CMOD_YYLVAL_H

%include [c] "parts/struct_range.h"
%include [c] "parts/struct_table_like.h"
%include [c] "parts/struct_recall_like.h"
%include [c] "parts/variant_c_typespec.h"
%include [c] "parts/variant_c_specifier.h"

#endif

%snippet yylval_free: = (x) %{%}
%map [lazy] yylval %{
%snippet yylval_free:${name} = (x) %%{ ${free} ${clear} %%}
%}

%comment String representation of grammar elements used in keywords
%comment Snippet name must match grammar element name
%snippet yylval:str_repr:opt_pipe = (str) <- (var) %{
    if(NULL != ${var}) ss_cat_c(${str}," | ");
%}

%snippet yylval:str_repr:mod_assign = (str) <- (var) %{
    switch($p{var}) {
        case '=': ss_cat_c(${str},"  = ");  break;
        case '?': ss_cat_c(${str}," ?= "); break;
        case '+': ss_cat_c(${str}," += "); break;
        case ':': ss_cat_c(${str}," := "); break;
        case '|': ss_cat_c(${str}," |= "); break;
    }
%}

%snippet yylval:str_repr:declaration = (str) <- (var) %{
    srt_string *tmp = decl_to_str($P{var},${var}->name,"",true,false);
    ss_cat(${str},tmp);
    ss_free(&tmp);
%}

%snippet yylval:str_repr:recall_end = (str) <- (var) %{
    ss_cat_c(${str},('e' == $p{var}) ? "|""%" : "\n");
%}

%snippet yylval:str_repr:recall_like = (str) <- (var) %{
    ss_cat_c(${str},${var}->name," ");
    if(NULL != ${var}->snip) { // arguments were processed
        const size_t nout = SNIPPET_NARGOUT(${var}->snip);
        if(nout > 0) {
            for(size_t i = 0; i < sv_len(${var}->arg_lists); ++i) {
                ss_cat_cn1(${str},'(');
                vec_namarg_str_repr(sv_at_ptr(${var}->arg_lists,i),
                                    SETIX_none,SETIX_set(nout),${str});
                ss_cat_cn1(${str},')');
            }
            ss_cat_c(${str}," <- ");
        }
        for(size_t i = 0; i < sv_len(${var}->arg_lists); ++i) {
            ss_cat_cn1(${str},'(');
            vec_namarg_str_repr(sv_at_ptr(${var}->arg_lists,i),
                                SETIX_set(nout),SETIX_none,${str});
            ss_cat_cn1(${str},')');
        }
    } else { // arguments were not processed
        if(pp->nout > 0) {
            for(size_t i = 0; i < pp->nout; ++i) {
                ss_cat_cn1(${str},'(');
                vec_namarg_str_repr(sv_at_ptr(${var}->arg_lists,i),
                                    SETIX_none,SETIX_none,${str});
                ss_cat_cn1(${str},')');
            }
            ss_cat_c(${str}," <- ");
        }
        for(size_t i = pp->nout; i < sv_len(${var}->arg_lists); ++i) {
            ss_cat_cn1(${str},'(');
            vec_namarg_str_repr(sv_at_ptr(${var}->arg_lists,i),
                                SETIX_none,SETIX_none,${str});
            ss_cat_cn1(${str},')');
        }
    }
%}

%snippet yylval:str_repr:table_like = (str) <- (var) %{
    if(NULL != ${var}->lst) { // table name list
        for(size_t i = 0; i < sv_len(${var}->lst); ++i) {
            if(i > 0) ss_cat_cn1(${str},',');
            hstr_str_repr(sv_at_ptr(${var}->lst,i),${str});
        }
    } else if(NULL != ${var}->sel) {
        ss_cat_c(${str},${var}->tab.name," ");
        ss_cat_cn1(${str},'(');
        for(size_t i = 0; i < sv_len(${var}->sel); ++i) {
            if(i > 0) ss_cat_cn1(${str},',');
            namarg_str_repr(sv_at(${var}->sel,i),${var}->tab.sv_colnarg,${str});
        }
        ss_cat_cn1(${str},')');
    } else { // table
        if(${var}->is_lambda) {
            if(debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(${str},"<lambda table>");
            } else ss_cat_c(${str},"<lambda table>");
        } else {
            %recall yylval:str_repr:table ($b{str}) <- (`(&${var}->tab)`)
        }
    }
%}

%snippet yylval:str_repr:snippet_formal_arguments_inout = (str) <- (var,nout = `pp->nout`) %{
    if(${nout} > 0) {
        ss_cat_cn1(${str},'(');
        for(size_t i = 0; i < ${nout}; ++i) {
            if(i > 0) ss_cat_cn1(${str},',');
            namarg_str_repr(sv_at($p{var},i),$p{var},${str});
        }
        ss_cat_cn1(${str},')');
        ss_cat_c(${str}," <- ");
    }
    ss_cat_cn1(${str},'(');
    for(size_t i = ${nout}; i < sv_len($p{var}); ++i) {
        if(i > ${nout}) ss_cat_cn1(${str},',');
        namarg_str_repr(sv_at($p{var},i),$p{var},${str});
    }
    ss_cat_c(${str},")");
%}

%snippet yylval:str_repr:snippet_like = (str) <- (var) %{
    %recall yylval:str_repr:snippet_formal_arguments_inout ($b{str})
                                                        <- (`&${var}->sv_forl`,`SNIPPET_NARGOUT(${var})`)
    ss_cat_cn1(${str},' ');
    %recall yylval:str_repr:snippet_body ($b{str}) <- (`&${var}->body`,`${var}->sv_argl`)
%}

%snippet yylval:str_repr:snippet_body = (str) <- (var,dllarg = `pp->sv_dllarg`) %{
    ss_cat_c(${str},"%""{\n");
    if(debug_level >= 2) {
        const struct snippet tmp = {
            .body= $p{var},     // const borrow
            .sv_argl= ${dllarg} // const borrow
        };
        snippet_body_str_repr(&tmp,tmp.sv_argl,${str});
    } else ss_cat_c(${str},"<snippet body>");
    ss_cat_c(${str},"%""}");
%}

%snippet yylval:str_repr:opt_snippet_body = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:snippet_body ($b{str}) <- ($b{var})
    }
%}

%snippet yylval:str_repr:mod_verbatim = (str) <- (var) %{
    ss_cat_c(${str},"%""{ ");
    if(debug_level >= 2) ss_cat_c(${str},ss_to_c($p{var}));
    else ss_cat_c(${str},"<verbatim>");
    ss_cat_c(${str}," %""}");
%}

%snippet yylval:str_repr:mod_herestring = (str) <- (var) %{
    ss_cat_c(${str},"%""{ ");
    if(debug_level >= 2) ss_cat_c(${str},ss_to_c($p{var}));
    else ss_cat_c(${str},"<verbatim>");
    ss_cat_c(${str}," %""}");
%}

%snippet yylval:str_repr:opt_mod_herestring = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:mod_herestring ($b{str}) <- ($b{var})
    }
%}

%snippet yylval:str_repr:table_columns = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    vec_namarg_str_repr($p{var},SETIX_none,SETIX_none,${str});
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:table = (str) <- (var) %{
    %recall yylval:str_repr:table_columns ($b{str}) <- (`&${var}->sv_colnarg`)
    ss_cat_c(${str}," %""{ ");
    if(debug_level >= 2) {
        /* TODO: print table body */
        ss_cat_c(${str},"<table body>");
    } else ss_cat_c(${str},"<table body>");
    ss_cat_c(${str}," %""}");
%}

%snippet yylval:str_repr:switch_body = (str) <- (var) %{
    ss_cat_c(${str},"%""{ ");
    if(debug_level >= 2) {
        /* TODO: print switch body */
        ss_cat_c(${str},"<switch body>");
    } else ss_cat_c(${str},"<switch body>");
    ss_cat_c(${str}," %""}");
%}

%snippet yylval:str_repr:IDENTIFIER = (str) <- (var) %{
    ss_cat_c(${str},$p{var});
%}

%snippet yylval:str_repr:opt_identifier = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:IDENTIFIER ($b{str}) <- ($b{var})
    }
%}

%snippet yylval:str_repr:identifier_list = (str) <- (var) %{
    for(size_t i = 0; i < sv_len($p{var}); ++i) {
        if(i > 0) ss_cat_cn1(${str},',');
        const char *x = sv_at_ptr($p{var},i);
        ss_cat_c(${str},x);
    }
%}

%snippet yylval:str_repr:C_IDENTIFIER = yylval:str_repr:IDENTIFIER (str) <- (var)
%snippet yylval:str_repr:opt_c_identifier = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:C_IDENTIFIER ($b{str}) <- ($b{var})
    }
%}
%snippet yylval:str_repr:C_TYPEDEF_NAME = yylval:str_repr:IDENTIFIER (str) <- (var)
%snippet yylval:str_repr:c_name_comma_list = yylval:str_repr:identifier_list (str) <- (var)

%snippet yylval:str_repr:FILE_PATH = (str) <- (var) %{
    ss_cat_c(${str},"\"",$p{var},"\"");
%}

%snippet yylval:str_repr:cmod_identifier = (str) <- (var) %{
    ss_cat_c(${str},"`",$p{var},"`");
%}

%snippet yylval:str_repr:opt_cmod_identifier = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:cmod_identifier ($b{str}) <- ($b{var})
    }
%}

%snippet yylval:str_repr:cmod_identifier_comma_list = (str) <- (var) %{
    for(size_t i = 0; i < sv_len($p{var}); ++i) {
        if(i > 0) ss_cat_cn1(${str},',');
        const char *x = sv_at_ptr($p{var},i);
        %recall yylval:str_repr:cmod_identifier ($b{str}) <- (`(&x)`)
    }
%}

%snippet yylval:str_repr:paren_cmod_identifier_comma_list = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    %recall yylval:str_repr:identifier_list ($b{str}) <- ($b{var})
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:herestring = (str) <- (var) %{
    hstr_str_repr($p{var},${str});
%}

%snippet yylval:str_repr:opt_herestring = (str) <- (var) %{
    if(NULL != $p{var}) {
        ss_cat_cn1(${str},' ');
        %recall yylval:str_repr:herestring ($b{str}) <- ($b{var})
    }
%}

%snippet yylval:str_repr:paren_c_expression = (str) <- (var) %{
    ss_cat_c(${str},"(",$p{var},")");
%}

%snippet yylval:str_repr:paren_c_expression_list = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    %recall yylval:str_repr:identifier_list ($b{str}) <- ($b{var})
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:cmod_function_arguments = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    vec_namarg_str_repr($p{var},SETIX_none,SETIX_none,${str});
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:paren_special_id_list = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    for(size_t i = 0; i < sv_len($p{var}); ++i) {
        if(i > 0) ss_cat_cn1(${str},',');
        const char *x = sv_at_ptr($p{var},i);
        ss_cat_c(${str},"#",x);
    }
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:paren_integer_comma_list = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    for(size_t i = 0; i < sv_len($p{var}); ++i) {
        if(i > 0) ss_cat_cn1(${str},',');
        const struct xint *x = sv_at($p{var},i);
        switch(x->type) {
            case ENUM_XINT(SIGNED):
                ss_cat_int(${str},XINT_SIGNED_get(*x));
                break;
            case ENUM_XINT(UNSIGNED):
                ss_cat_int(${str},XINT_UNSIGNED_get(*x));
                break;
            case ENUM_XINT(INVALID): break;
        }
    }
    ss_cat_cn1(${str},')');
%}

%snippet yylval:str_repr:paren_unsigned_int = (str) <- (var) %{
    ss_cat_cn1(${str},'(');
    ss_cat_int(${str},XINT_UNSIGNED_get($p{var}));
    ss_cat_cn1(${str},')');
%}
