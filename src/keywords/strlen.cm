%once keywords:strlen

%table [hash] strlen_opts = (name,type,group,description) %tsv{
	debug		count	%nul	verbose logging of keyword invocation
	str			str		%nul	apply string modifiers to keyword output
	add1		count	%nul	increase computed length by this much
	escaped		bool	%nul	interpret C escape sequences
	utf8		bool	%nul	count UTF-8 encoded Unicode characters
%}

%table strlen_rules = (number,description) %tsv{
	0	print string length
%}

%table strlen_rule_0 = (token,type,representation) %tsv{
	cmod_function_arguments		vnarg	( <verbatim string> [, <verbatim string> ] )
%}

%snippet strlen_rule_0:example = %{
char buf[${#M}strlen [add1] (`my long string`)] = "my long string";
%}

%snippet strlen_action_0 = (string_list,
                            loc:string_list) %{
    size_t len = opts.add1_count;
    %foreach [const,svec=`struct namarg`] narg (${string_list}) %%{
        if($${narg}->type != CMOD_ARG_VERBATIM) {
            %recall parser:set_errlloc ($b{loc:string_list})
            KEYWORD_ERROR("expected verbatim argument at position %zu",$${#NR});
        } else if(NULL != $${narg}->name) {
            %recall parser:set_errlloc ($b{loc:string_list})
            KEYWORD_ERROR("unexpected named verbatim argument at position %zu",$${#NR});
        }
        const char *str = CMOD_ARG_VERBATIM_get($$p{narg});
        if(opts.escaped_bool) { // count escapes as length 1
            srt_string *ss = NULL;
            ss <- %| parser:strcunesc (str) |%
            len += (opts.utf8_bool ? ss_len_u(ss) : ss_len(ss)); // no final NUL
            ss_free(&ss);
        } else {
            const srt_string *ss = ss_crefs(str);
            len += (opts.utf8_bool ? ss_len_u(ss) : ss_len(ss)); // final NUL is not counted
        }
    %%}
    BUFPUTINT(len);
%}
