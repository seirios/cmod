%once keywords:table_size

%table [hash] table_size_opts = (name,type,group,description) %tsv{
	debug	count	%nul	verbose logging of keyword invocation
	str		str		%nul	apply string modifiers to keyword output
	opt		bool	01		ignore undefined tables
	lazy	bool	01		delay evaluation until tables are defined
	rows	bool	02		get number of rows (default)
	cols	bool	02		get number of columns
	add1	count	%nul	increase computed size by this much
%}

%table table_size_rules = (number,description) %tsv{
	0	sum over multiple tables
%}

%table table_size_rule_0 = (token,type,representation) %tsv{
	paren_cmod_identifier_comma_list	vstr	( <table names> )
%}

%snippet table_size_rule_0:example = %{
${#M}table test_table_size = (idx) ${#M}tsv{
	2
	165
	98
	9878
${#M}}
${#M}table-size(test_table_size)
%}

%snippet table_size_action_0:lazy = %{
    if(opts.str_isset) kwbuf = kwbuf_save;
    %recall parser:kw_str_repr (table_size,0)
    BUFPUTS(pp->kwname); BUFCAT(str_repr);
%}

%snippet table_size_action_0 = (table_names,
                                loc:table_names) %{
    size_t nrow = 0;
    size_t ncol = 0;

    %foreach [const,deref,svec=`char*`] table_name (${table_names}) %%{
        struct table *tab = NULL;
        tab <- %| parser:table_get ($$b{table_name},$b{loc:table_names},
                                    skip = `continue;`, lazy_action = ${#NAME}:lazy) |%

        nrow += TABLE_NROWS(*tab);
        ncol += TABLE_NCOLS(*tab);
    %%}

    BUFPUTINT((opts.cols_bool ? ncol : nrow) + opts.add1_count);
%}
