# Keyword logic

Each keyword is defined in a separate `.cm` file that is included in `parser_api.cm`.

Each keyword rule is defined by a snippet which then goes into a function body.
Parameters from the parser are passed by reference (e.g. &$1), and the snippet receives
the dereferenced value (e.g. \*(&$1)), so it can modify the values in yyvsp.

Some keyword rules perform a last step of parsing in a `:prolog` snippet, which is called
before skipping the rule if inactive, so that when the label `cleanup_action` is reached
there are no jumps conditional on uninitialized values. Each rule snippet has a label
`cleanup_action` for cleanup operations which is performed when skipping the rule due to
input properties or inactivation of the keyword. Additionally, each rule function has a
`cleanup` label where the parameters from the parser are freed.
