%once keywords:intop

%table intop_opts_multi = (name,type,group,description) %tsv{
	sum			bool	01		sum of all arguments
	prod		bool	01		product of all arguments
	min			bool	01		minimum of all arguments
	max			bool	01		maximum of all arguments
%}

%table intop_opts_two = (name,type,group,description) %tsv{
	add			bool	01		addition of two arguments
	mul			bool	01		multiplication of two arguments
	sub			bool	01		subtraction of two arguments
	div			bool	01		division of two arguments
	mod			bool	01		modulo of two arguments
	pow			bool	01		power of two arguments
%}

%table intop_opts_other = (name,type,group,description) %tsv{
	debug		count	%nul	verbose logging of keyword invocation
	str			str		%nul	apply string modifiers to keyword output
	unsigned	bool	%nul	treat all arguments as unsigned
	sign		bool	%nul	always print sign
%}

%table-stack [v,hash] intop_opts (intop_opts_multi,intop_opts_two,intop_opts_other)

%table intop_rules = (number,description) %tsv{
	0	arithmetic operation
%}

%table intop_rule_0 = (token,type,representation) %tsv{
	paren_integer_comma_list	vxint	( <operand list> )
%}

%snippet intop_rule_0:example = %{
${#M}intop [sum] (5,-1,4,-8,1)
${#M}intop [prod] (5,-1,4,-8,1)
${#M}intop [min] (5,-1,4,-8,1)
${#M}intop [max] (5,-1,4,-8,1)
%}

%snippet intop:global = %{
static void ss_cat_xint(srt_string **ss, struct xint x, const char *mode) {
    if(x.type == ENUM_XINT(UNSIGNED)) {
        int buf_off; char buf[STRUMAX_LEN];
        buf, buf_off <- %| std:strumax (`XINT_UNSIGNED_get(x)`,mode) |%
        %recall std:strumax_sscat (`*ss`) <- (buf)
    } else {
        int buf_off; char buf[STRIMAX_LEN];
        buf, buf_off <- %| std:strimax (`XINT_SIGNED_get(x)`,mode) |%
        %recall std:strimax_sscat (`*ss`) <- (buf)
    }
}

static void ss_cat_umax(srt_string **ss, uintmax_t x, const char *sign) {
    ss_cat_c(ss,sign);
    int buf_off; char buf[STRUMAX_LEN];
    buf, buf_off <- %| std:strumax (x) |%
    %recall std:strumax_sscat (`*ss`) <- (buf)
}
%}

%snippet intop_action_0:opt_setup = (xint_list,
                                     loc:xint_list) %{
    if(!(%map [sep=`||`] intop_opts_multi, intop_opts_two %%{ opts.$${name}_bool %%})) {
        %recall parser:set_errlloc (LOC_OPTLIST)
        KEYWORD_ERROR0("please specify operation to perform");
    }
%}

%snippet intop_action_0 = (xint_list,
                           loc:xint_list) %{
    /* set operation name */
    const char *opname = NULL;
    %map [sep=else] intop_opts_multi, intop_opts_two %%{
        if(opts.$${name}_bool) opname = $$S{name};
    %%}
    const char *signmode = opts.sign_bool ? "+" : ""; // explicit sign or not

    /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
    if(opts.sum_bool) {
        struct xint sum = opts.unsigned_bool ? XINT_UNSIGNED_set(0) : XINT_SIGNED_set(0);
        for(size_t i = 0; i < sv_len(${xint_list}); i++) {
            const struct xint *x = sv_at(${xint_list},i);
            switch(x->type) {
                case ENUM_XINT(SIGNED):
                    if(opts.unsigned_bool) {
                        %recall parser:set_errlloc ($b{loc:xint_list})
                        KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
                    } else {
                        intmax_t addend = x->value.Signed;
                        %recall std:safe_intmax_add (`XINT_SIGNED_get(sum)`) <- (`XINT_SIGNED_get(sum)`,addend,
                                %<< KEYWORD_ERROR("[%s] signed addition overflow",opname); >>%)
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                {
                    uintmax_t addend = x->value.Unsigned;
                    if(opts.unsigned_bool) {
                        %recall std:safe_uintmax_add (`XINT_UNSIGNED_get(sum)`) <- (`XINT_UNSIGNED_get(sum)`,addend,
                                %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
                    } else { // unsigned to signed conversion
                        intmax_t s_addend;
                        %recall std:safe_uintmax_to_intmax (s_addend) <- (addend,
                                %<< KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",opname); >>%)
                        %recall std:safe_intmax_add (`XINT_SIGNED_get(sum)`) <- (`XINT_SIGNED_get(sum)`,s_addend,
                                %<< KEYWORD_ERROR("[%s] signed addition overflow",opname); >>%)
                    }
                }
                    break;
                case ENUM_XINT(INVALID):
                    %recall cmod:internal_error_abort
                    break;
            }
        }
        ss_cat_xint(kwbuf,sum,signmode);
    }

    /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
    else if(opts.prod_bool) {
        struct xint prod = opts.unsigned_bool ? XINT_UNSIGNED_set(1) : XINT_SIGNED_set(1);
        for(size_t i = 0; i < sv_len(${xint_list}); i++) {
            const struct xint *x = sv_at(${xint_list},i);
            switch(x->type) {
                case ENUM_XINT(SIGNED):
                    if(opts.unsigned_bool) {
                        %recall parser:set_errlloc ($b{loc:xint_list})
                        KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
                    } else {
                        intmax_t factor = x->value.Signed;
                        %recall std:safe_intmax_mul (`XINT_SIGNED_get(prod)`) <- (`XINT_SIGNED_get(prod)`,factor,
                                %<< KEYWORD_ERROR("[%s] signed multiplication overflow",opname); >>%)
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                {
                    uintmax_t factor = x->value.Unsigned;
                    if(opts.unsigned_bool) {
                        %recall std:safe_uintmax_mul (`XINT_UNSIGNED_get(prod)`) <- (`XINT_UNSIGNED_get(prod)`,factor,
                                %<< KEYWORD_ERROR("[%s] unsigned multiplication overflow",opname); >>%)
                    } else { // unsigned to signed conversion
                        intmax_t s_factor;
                        %recall std:safe_uintmax_to_intmax (s_factor) <- (factor,
                                %<< KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",opname); >>%)
                        %recall std:safe_intmax_mul (`XINT_SIGNED_get(prod)`) <- (`XINT_SIGNED_get(prod)`,s_factor,
                                %<< KEYWORD_ERROR("[%s] signed multiplication overflow",opname); >>%)
                    }
                }
                    break;
                case ENUM_XINT(INVALID):
                    %recall cmod:internal_error_abort
                    break;
            }
        }
        ss_cat_xint(kwbuf,prod,signmode);
    }

    /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
    else if(opts.min_bool || opts.max_bool) {
        struct xint res = opts.unsigned_bool ? XINT_UNSIGNED_set(0) : XINT_SIGNED_set(0);
        for(size_t i = 0; i < sv_len(${xint_list}); i++) {
            const struct xint *x = sv_at(${xint_list},i);
            switch(x->type) {
                case ENUM_XINT(SIGNED):
                    if(opts.unsigned_bool) {
                        %recall parser:set_errlloc ($b{loc:xint_list})
                        KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
                    } else {
                        intmax_t item = x->value.Signed;
                        if(i == 0) XINT_SIGNED_get(res) = item; // initialize result
                        else { // update result
                            if(opts.max_bool) { %| std:update_max (`XINT_SIGNED_get(res)`) <- (item) |% }
                            else { %| std:update_min (`XINT_SIGNED_get(res)`) <- (item) |% }
                        }
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                {
                    uintmax_t item = x->value.Unsigned;
                    if(opts.unsigned_bool) {
                        if(i == 0) XINT_UNSIGNED_get(res) = item; // initialize result
                        else { // update result
                            if(opts.max_bool) { %| std:update_max (`XINT_UNSIGNED_get(res)`) <- (item) |% }
                            else { %| std:update_min (`XINT_UNSIGNED_get(res)`) <- (item) |% }
                        }
                    } else { // unsigned to signed conversion
                        intmax_t s_item;
                        %recall std:safe_uintmax_to_intmax (s_item) <- (item,
                                %<< KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",opname); >>%)
                        if(opts.max_bool) { %| std:update_max (`XINT_SIGNED_get(res)`) <- (s_item) |% }
                        else { %| std:update_min (`XINT_SIGNED_get(res)`) <- (s_item) |% }
                    }
                }
                    break;
                case ENUM_XINT(INVALID):
                    %recall cmod:internal_error_abort
                    break;
            }
        }
        ss_cat_xint(kwbuf,res,signmode);
    }

    /* operands are handled as uintmax_t, sign of result is computed separately */
    else if(opts.add_bool) {
        if(sv_len(${xint_list}) != 2) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] operation takes exactly two arguments",opname);
        }
        const struct xint *xaugend = sv_at(${xint_list},0);
        const struct xint *xaddend = sv_at(${xint_list},1);

        uintmax_t sum = 0;
        bool isneg = false;

        if(opts.unsigned_bool) {
            if(xaugend->type != ENUM_XINT(UNSIGNED) || xaddend->type != ENUM_XINT(UNSIGNED)) {
                %recall parser:set_errlloc ($b{loc:xint_list})
                KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
            }
            %recall std:safe_uintmax_add (sum) <- (`xaugend->value.Unsigned`,`xaddend->value.Unsigned`,
                    %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
        } else {
            bool isneg_ag = false;
            uintmax_t abs_ag = 0;
            if(xaugend->type == ENUM_XINT(UNSIGNED)) abs_ag = xaugend->value.Unsigned;
            else {
                isneg_ag = (xaugend->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_ag) <- (`xaugend->value.Signed`)
            }

            bool isneg_ad = false;
            uintmax_t abs_ad = 0;
            if(xaddend->type == ENUM_XINT(UNSIGNED)) abs_ad = xaddend->value.Unsigned;
            else {
                isneg_ad = (xaddend->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_ad) <- (`xaddend->value.Signed`)
            }

            bool ag_smaller = (abs_ag < abs_ad);

            /**/ if(!isneg_ag && !isneg_ad) {               // +(abs(ag) + abs(ad))
                isneg = false;
                %recall std:safe_uintmax_add (sum) <- (abs_ag,abs_ad,
                        %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
            }
            else if( isneg_ag &&  isneg_ad) {               // -(abs(ag) + abs(ad))
                isneg = true;
                %recall std:safe_uintmax_add (sum) <- (abs_ag,abs_ad,
                        %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
            }
            else if( isneg_ag && !isneg_ad &&  ag_smaller) { // +(abs(ad) - abs(ag))
                isneg = false;
                %recall std:safe_uintmax_sub (sum) <- (abs_ad,abs_ag,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if(!isneg_ag &&  isneg_ad &&  ag_smaller) { // -(abs(ad) - abs(ag))
                isneg = true;
                %recall std:safe_uintmax_sub (sum) <- (abs_ad,abs_ag,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if(!isneg_ag &&  isneg_ad && !ag_smaller) { // +(abs(ag) - abs(ad))
                isneg = false;
                %recall std:safe_uintmax_sub (sum) <- (abs_ag,abs_ad,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if( isneg_ag && !isneg_ad && !ag_smaller) { // -(abs(ag) - abs(ad))
                isneg = true;
                %recall std:safe_uintmax_sub (sum) <- (abs_ag,abs_ad,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
        }
        ss_cat_umax(kwbuf,sum,isneg ? "-" : (opts.sign_bool ? "+" : ""));
    }

    /* operands are handled as uintmax_t, sign of result is computed separately */
    else if(opts.sub_bool) {
        if(sv_len(${xint_list}) != 2) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] operation takes exactly two arguments",opname);
        }
        const struct xint *xminuend = sv_at(${xint_list},0);
        const struct xint *xsubtrahend = sv_at(${xint_list},1);

        uintmax_t difference = 0;
        bool isneg = false;

        if(opts.unsigned_bool) {
            if(xminuend->type != ENUM_XINT(UNSIGNED) || xsubtrahend->type != ENUM_XINT(UNSIGNED)) {
                %recall parser:set_errlloc ($b{loc:xint_list})
                KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
            }
            %recall std:safe_uintmax_sub (difference) <- (`xminuend->value.Unsigned`,`xsubtrahend->value.Unsigned`,
                    %<< KEYWORD_ERROR("[%s] expecting positive difference",opname); >>%)
        } else {
            bool isneg_m = false;
            uintmax_t abs_m = 0;
            if(xminuend->type == ENUM_XINT(UNSIGNED)) abs_m = xminuend->value.Unsigned;
            else {
                isneg_m = (xminuend->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_m) <- (`xminuend->value.Signed`)
            }

            bool isneg_s = false;
            uintmax_t abs_s = 0;
            if(xsubtrahend->type == ENUM_XINT(UNSIGNED)) abs_s = xsubtrahend->value.Unsigned;
            else {
                isneg_s = (xsubtrahend->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_s) <- (`xsubtrahend->value.Signed`)
            }

            bool m_smaller = (abs_m < abs_s);

            /**/ if(!isneg_m &&  isneg_s) {               // +(abs(m) + abs(s))
                isneg = false;
                %recall std:safe_uintmax_add (difference) <- (abs_m,abs_s,
                        %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
            }
            else if( isneg_m && !isneg_s) {               // -(abs(m) + abs(s))
                isneg = true;
                %recall std:safe_uintmax_add (difference) <- (abs_m,abs_s,
                        %<< KEYWORD_ERROR("[%s] unsigned addition overflow",opname); >>%)
            }
            else if( isneg_m &&  isneg_s &&  m_smaller) { // +(abs(s) - abs(m))
                isneg = false;
                %recall std:safe_uintmax_sub (difference) <- (abs_s,abs_m,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if(!isneg_m && !isneg_s &&  m_smaller) { // -(abs(s) - abs(m))
                isneg = true;
                %recall std:safe_uintmax_sub (difference) <- (abs_s,abs_m,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if(!isneg_m && !isneg_s && !m_smaller) { // +(abs(m) - abs(s))
                isneg = false;
                %recall std:safe_uintmax_sub (difference) <- (abs_m,abs_s,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
            else if( isneg_m &&  isneg_s && !m_smaller) { // -(abs(m) - abs(s))
                isneg = true;
                %recall std:safe_uintmax_sub (difference) <- (abs_m,abs_s,
                        %<< KEYWORD_ERROR("[%s] unsigned subtraction overflow",opname); >>%)
            }
        }
        ss_cat_umax(kwbuf,difference,isneg ? "-" : (opts.sign_bool ? "+" : ""));
    }

    /* operands are handled as uintmax_t, sign of result is computed separately */
    else if(opts.mul_bool) {
        if(sv_len(${xint_list}) != 2) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] operation takes exactly two arguments",opname);
        }
        const struct xint *xmultiplier = sv_at(${xint_list},0);
        const struct xint *xmultiplicand = sv_at(${xint_list},1);

        uintmax_t product = 1;
        bool isneg = false;

        if(opts.unsigned_bool) {
            if(xmultiplier->type != ENUM_XINT(UNSIGNED) || xmultiplicand->type != ENUM_XINT(UNSIGNED)) {
                %recall parser:set_errlloc ($b{loc:xint_list})
                KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
            }
            %recall std:safe_uintmax_mul (product) <- (`xmultiplier->value.Unsigned`,`xmultiplicand->value.Unsigned`,
                    %<< KEYWORD_ERROR("[%s] unsigned multiplication overflow",opname); >>%)
        } else {
            bool isneg_mr = false;
            uintmax_t abs_mr = 0;
            if(xmultiplier->type == ENUM_XINT(UNSIGNED)) abs_mr = xmultiplier->value.Unsigned;
            else {
                isneg_mr = (xmultiplier->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_mr) <- (`xmultiplier->value.Signed`)
            }

            bool isneg_md = false;
            uintmax_t abs_md = 0;
            if(xmultiplicand->type == ENUM_XINT(UNSIGNED)) abs_md = xmultiplicand->value.Unsigned;
            else {
                isneg_md = (xmultiplicand->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_md) <- (`xmultiplicand->value.Signed`)
            }

            isneg = (isneg_mr != isneg_md); // opposite signs -> negative
            %recall std:safe_uintmax_mul (product) <- (abs_mr,abs_md,
                    %<< KEYWORD_ERROR("[%s] unsigned multiplication overflow",opname); >>%)
        }
        ss_cat_umax(kwbuf,product,isneg ? "-" : (opts.sign_bool ? "+" : ""));
    }

    /* operands are handled as uintmax_t, sign of result is computed separately */
    else if(opts.div_bool || opts.mod_bool) {
        if(sv_len(${xint_list}) != 2) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] operation takes exactly two arguments",opname);
        }
        const struct xint *xdividend = sv_at(${xint_list},0);
        const struct xint *xdivisor = sv_at(${xint_list},1);

        if((xdivisor->type == ENUM_XINT(UNSIGNED) && xdivisor->value.Unsigned == 0)
                || (xdivisor->type == ENUM_XINT(SIGNED) && xdivisor->value.Signed == 0)) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] attempted division by zero",opname);
        }

        uintmax_t result = 0;
        bool isneg = false;

        if(opts.unsigned_bool) {
            if(xdividend->type != ENUM_XINT(UNSIGNED) || xdivisor->type != ENUM_XINT(UNSIGNED)) {
                %recall parser:set_errlloc ($b{loc:xint_list})
                KEYWORD_ERROR("[%s] expecting only unsigned operands",opname);
            }
            result = opts.div_bool ? xdividend->value.Unsigned / xdivisor->value.Unsigned
                                  : xdividend->value.Unsigned % xdivisor->value.Unsigned;
        } else {
            bool isneg_dd = false;
            uintmax_t abs_dd = 0;
            if(xdividend->type == ENUM_XINT(UNSIGNED)) abs_dd = xdividend->value.Unsigned;
            else {
                isneg_dd = (xdividend->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_dd) <- (`xdividend->value.Signed`)
            }

            bool isneg_ds = false;
            uintmax_t abs_ds = 0;
            if(xdivisor->type == ENUM_XINT(UNSIGNED)) abs_ds = xdivisor->value.Unsigned;
            else {
                isneg_ds = (xdivisor->value.Signed < 0);
                %recall std:safe_intmax_abs:uintmax (abs_ds) <- (`xdivisor->value.Signed`)
            }

            isneg = opts.div_bool ? (isneg_dd != isneg_ds) // opposite signs -> negative
                                 :  isneg_dd; // remainder has same sign as divisor
            result = opts.div_bool ? abs_dd / abs_ds
                                  : abs_dd % abs_ds;
        }
        ss_cat_umax(kwbuf,result,isneg ? "-" : (opts.sign_bool ? "+" : ""));
    }

    /* operands are handled as uintmax_t, sign of result is computed separately */
    else if(opts.pow_bool) {
        if(sv_len(${xint_list}) != 2) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] operation takes exactly two arguments",opname);
        }
        const struct xint *xbase = sv_at(${xint_list},0);
        const struct xint *xexponent = sv_at(${xint_list},1);

        if(xexponent->type == ENUM_XINT(SIGNED)) {
            %recall parser:set_errlloc ($b{loc:xint_list})
            KEYWORD_ERROR("[%s] exponent must be unsigned",opname);
        }
        uintmax_t exponent = xexponent->value.Unsigned;

        uintmax_t base = 0;
        if(xbase->type == ENUM_XINT(UNSIGNED)) base = xbase->value.Unsigned;
        else {
            if(opts.unsigned_bool) {
                %recall parser:set_errlloc ($b{loc:xint_list})
                KEYWORD_ERROR("[%s] expecting unsigned base",opname);
            }
            %| std:safe_intmax_abs:uintmax (base) <- (`xbase->value.Signed`) |%
        }

        uintmax_t power = 1;
        bool isneg = (xbase->type == ENUM_XINT(SIGNED) && xbase->value.Signed < 0 && exponent % 2 == 1);

        for(uintmax_t i = 0; i < exponent; i++) { // unwrap integer power as multiplication
            %recall std:safe_uintmax_mul (power) <- (power,base,
                    %<< KEYWORD_ERROR("[%s] unsigned multiplication overflow",opname); >>%)
        }
        ss_cat_umax(kwbuf,power,isneg ? "-" : (opts.sign_bool ? "+" : ""));
    }
%}
