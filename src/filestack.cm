%include [c] "cmod.ext.h"
%include [c] "parser.h"
%include [c] "scanner.h"
%include [c] "filestack.h"
%include [c,sys] "limits.h"

%include "cmod.ext_tab.hm"

%include "cmod/retval.hm"

%snippet switch_buffer = (buffer) %{
    pp->curbs = ${buffer}; // update top of stack
    yy_switch_to_buffer(pp->curbs->bs,yyscanner); // switch buffer
    if(NULL != yylloc) *yylloc = pp->curbs->lloc; // update location
    if(NULL != pp->namein) free(pp->namein);
    pp->namein = rstrdup(pp->curbs->fname); // update input filename
    if(NULL == pp->namein) pp->namein = "<null>";
%}

%recall std:retval_init (pushfile)
ret_t pushfile(yyscan_t yyscanner, struct param pp[static 1], FILE fp[static 1],
               const char fname[static 1], const char *fpath,
               bool quiet, bool bs_noblank, bool bs_quiet) {
    ret_t ret = 0;

    /* NOTE: yyget_lloc returns the address of a variable local to yyparse! */
    YYLTYPE *yylloc = pp->in_parse ? yyget_lloc(yyscanner) : NULL;

    if(NULL != pp->curbs) { // stack not empty
        if(!quiet && pp->verbose)
            yyverbose(NULL,yyscanner,NULL,"switching to buffer \"%s\"",fname);
        if(NULL != yylloc) pp->curbs->lloc = *yylloc; // store location before switch
    }

    /* allocate new buffer stack entry */
    struct bufstack *bs = rcalloc(1,sizeof(struct bufstack));
    if(NULL == bs) { // fatal
        ret <- %| std:retval_blame:errno (pushfile,rcalloc) |%
        goto end;
    }
    bs->prev = pp->curbs; // save last buffer
    bs->fp = fp; // set file pointer
    bs->bs = yy_create_buffer(bs->fp,YY_BUF_SIZE,yyscanner); // allocate new buffer
    bs->fname = rstrdup(fname); // non-fatal
    bs->fpath = NULL;
    if(NULL != fpath) {
        char rpath[PATH_MAX] = { 0 };
        errno = 0;
        // FIXME: CWE-120/CWE-785 check input argument no longer than PATH_MAX
        if(rpath == realpath(fpath,rpath))
            bs->fpath = rstrdup(rpath); // non-fatal
    }
    bs->noblank = bs_noblank; // suppress empty lines
    bs->quiet = bs_quiet; // suppress all output
    bs->lloc = (YYLTYPE){ 1, 1, 1, 1 }; // initial location

    %recall switch_buffer (bs)

end:
    return ret;
}

%recall std:retval_init (fclose,static = static)
%recall std:retval_init (popfile)
ret_t popfile(yyscan_t yyscanner, struct param pp[static 1]) {
    ret_t ret = 0;

    /* NOTE: yyget_lloc returns the address of a variable local to yyparse! */
    YYLTYPE *yylloc = pp->in_parse ? yyget_lloc(yyscanner) : NULL;

    if(NULL == pp->curbs) // empty stack, no-op
        goto end;

    struct bufstack *bs = pp->curbs; // current entry

    if(NULL == bs->prev) { // last entry
        pp->curbs = NULL;
    } else { // more entries
        %recall switch_buffer (`bs->prev`)
        if(pp->verbose)
            yyverbose(NULL,yyscanner,NULL,"switching to buffer \"%s\"",pp->curbs->fname);
    }

    /* free buffer stack entry */
    errno = 0;
    if(NULL != bs->fp && fclose(bs->fp) == EOF) {
        ret <- %| std:retval_blame:errno (popfile,fclose) |%
    }
    yy_delete_buffer(bs->bs,yyscanner);
    %free(bs->fname);
    %free(bs->fpath);
    %free(bs);

end:
    return ret;
}
