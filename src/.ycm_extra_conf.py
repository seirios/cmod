from os.path import splitext

def Settings(**kwargs):
    filename = kwargs['filename']
    filext = splitext(filename)[1]
    if filext in ['.c','.h']:
        return {
            'flags': ['-x', 'c',
                      '-Wall', '-Wextra', '-Werror', '-pedantic', '-pedantic-errors',
                      '-Wno-error=override-init', '-Wno-error=override-init-side-effects',
                      '-D_POSIX_C_SOURCE=200809L', '-D_BSD_SOURCE', '-D_DEFAULT_SOURCE',
                      '-Iinclude' ]
        }
    else:
        return {
            'flags': []
        }
