include os_detect.mk

BENCHMARKS_CMOD := ./cmod
ifeq (Darwin,$(DETECTED_OS))
TIME := gtime
else
TIME := /bin/time
endif

BENCHMARKS_DIR := benchmark

BENCHMARKS := $(notdir $(subst .cm,,$(wildcard $(BENCHMARKS_DIR)/*.cm)))

benchmarks: $(BENCHMARKS_DIR)/results.txt
$(BENCHMARKS_DIR)/results.txt: $(BENCHMARKS_CMOD)
	@$(MAKE) -s benchmark > $@

benchmark: $(addprefix benchmark-,$(BENCHMARKS))

$(addprefix benchmark-,$(BENCHMARKS)): benchmark-%: $(BENCHMARKS_DIR)/%.cm
	@echo '$*'
	@$(TIME) $(BENCHMARKS_CMOD) -qqq -o/dev/null $(CMODFLAGS) $< 2>&1

# Per-case flags
benchmark-snippet_table_map_recall: CMODFLAGS :=
benchmark-strcmp: CMODFLAGS := -L 500000
benchmark-table_hash: CMODFLAGS := -L 50000
