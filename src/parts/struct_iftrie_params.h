#ifndef PARTS_STRUCT_IFTRIE_PARAMS
#define PARTS_STRUCT_IFTRIE_PARAMS

/* parameters for if-trie generation */
struct iftrie_params {
    const char **words;       // word array
    const char **actions;     // action array
    const char *action_else;  // action else
    const char *var;          // variable name
    const char *label;        // label name
    const int *c2ix;          // character-to-index mapping
    size_t arrlen;            // array length
    size_t nexpand;           // expand strcmp below this word length
    bool sort_by_size;        // sort by size?
    bool fuzzy;               // fuzzy match?
};

#endif
