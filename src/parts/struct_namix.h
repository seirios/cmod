#ifndef PARTS_STRUCT_NAMIX
#define PARTS_STRUCT_NAMIX

#include <stdlib.h>

/* index with name */
struct namix {
    char *name; // name
    size_t ix;  // index
};

#endif
