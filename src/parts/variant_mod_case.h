#ifndef PARTS_VARIANT_MOD_CASE
#define PARTS_VARIANT_MOD_CASE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>


typedef srt_vector vec_arr_cdi ;

enum mod_case_type {
MOD_CASE_TYPE_DEFAULT = 0, /* default */
MOD_CASE_TYPE_EXPRL = 1,
MOD_CASE_TYPE_VACDI = 2,
};
#define enum_mod_case_type_len 2
struct mod_case {
    enum mod_case_type type;
    union {
            vec_string* exprl;
           vec_arr_cdi* vacdi;
       } value;
            char* body;
   };

void mod_case_free(struct mod_case *x);

__attribute__ ((warn_unused_result))
int mod_case_dup(const struct mod_case *x, struct mod_case *y);

#define ENUM_MOD_CASE(x) MOD_CASE_TYPE_##x
#define MOD_CASE_DEFAULT_type .type=ENUM_MOD_CASE(DEFAULT)
#define MOD_CASE_DEFAULT_set() (struct mod_case){ MOD_CASE_DEFAULT_type }
#define MOD_CASE_DEFAULT_xset(...) (struct mod_case){ MOD_CASE_DEFAULT_type, __VA_ARGS__ }

#define MOD_CASE_EXPRL_type .type=ENUM_MOD_CASE(EXPRL)
#define MOD_CASE_EXPRL_value .value.exprl
#define MOD_CASE_EXPRL_set(x) (struct mod_case){ MOD_CASE_EXPRL_type, MOD_CASE_EXPRL_value = (x) }
#define MOD_CASE_EXPRL_xset(x, ...) (struct mod_case){ MOD_CASE_EXPRL_type, MOD_CASE_EXPRL_value = (x), __VA_ARGS__ }
#define MOD_CASE_EXPRL_get(x) ((x) MOD_CASE_EXPRL_value)
#define MOD_CASE_VACDI_type .type=ENUM_MOD_CASE(VACDI)
#define MOD_CASE_VACDI_value .value.vacdi
#define MOD_CASE_VACDI_set(x) (struct mod_case){ MOD_CASE_VACDI_type, MOD_CASE_VACDI_value = (x) }
#define MOD_CASE_VACDI_xset(x, ...) (struct mod_case){ MOD_CASE_VACDI_type, MOD_CASE_VACDI_value = (x), __VA_ARGS__ }
#define MOD_CASE_VACDI_get(x) ((x) MOD_CASE_VACDI_value)


#endif
