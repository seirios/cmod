#ifndef PARTS_STRUCT_DSL
#define PARTS_STRUCT_DSL

#include "parts/typedef_srt_alias.h"
#include "parts/enum_dsl_type.h"

/* domain-specific language */
struct dsl {
    char *name;           // language name
    hmap_su *kw_map;      // hashmap index
    vec_string *kw_name;  // keyword names, indexed by kw_map
    vec_snippet *kw_snip; // keyword snippets, indexed by kw_map
    vec_u8 *kw_prec;      // keyword precedence, indexed by kw_map
    enum dsl_type type;   // syntax type
    size_t max_depth;     // maximum evaluation depth
    char stmt_sep;        // statement separator
};

#endif
