#ifndef PARTS_VARIANT_C_TYPESPEC
#define PARTS_VARIANT_C_TYPESPEC

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

enum c_typespec_type {
C_TYPESPEC_TYPE_INVALID = 0, /* default */
C_TYPESPEC_TYPE_SIMPLE = 1,
C_TYPESPEC_TYPE_ATOMIC = 2,
C_TYPESPEC_TYPE_COMPOUND = 3,
C_TYPESPEC_TYPE_ENUM = 4,
C_TYPESPEC_TYPE_TYPEDEF = 5,
};
#define enum_c_typespec_type_len 5
struct c_typespec {
    enum c_typespec_type type;
    union {
            char* Text;
       } value;
    };

void c_typespec_free(struct c_typespec *x);

__attribute__ ((warn_unused_result))
int c_typespec_dup(const struct c_typespec *x, struct c_typespec *y);

#define ENUM_C_TYPESPEC(x) C_TYPESPEC_TYPE_##x
#define C_TYPESPEC_INVALID_type .type=ENUM_C_TYPESPEC(INVALID)
#define C_TYPESPEC_INVALID_set() (struct c_typespec){ C_TYPESPEC_INVALID_type }
#define C_TYPESPEC_INVALID_xset(...) (struct c_typespec){ C_TYPESPEC_INVALID_type, __VA_ARGS__ }

#define C_TYPESPEC_SIMPLE_type .type=ENUM_C_TYPESPEC(SIMPLE)
#define C_TYPESPEC_SIMPLE_value .value.Text
#define C_TYPESPEC_SIMPLE_set(x) (struct c_typespec){ C_TYPESPEC_SIMPLE_type, C_TYPESPEC_SIMPLE_value = (x) }
#define C_TYPESPEC_SIMPLE_xset(x, ...) (struct c_typespec){ C_TYPESPEC_SIMPLE_type, C_TYPESPEC_SIMPLE_value = (x), __VA_ARGS__ }
#define C_TYPESPEC_SIMPLE_get(x) ((x) C_TYPESPEC_SIMPLE_value)
#define C_TYPESPEC_ATOMIC_type .type=ENUM_C_TYPESPEC(ATOMIC)
#define C_TYPESPEC_ATOMIC_value .value.Text
#define C_TYPESPEC_ATOMIC_set(x) (struct c_typespec){ C_TYPESPEC_ATOMIC_type, C_TYPESPEC_ATOMIC_value = (x) }
#define C_TYPESPEC_ATOMIC_xset(x, ...) (struct c_typespec){ C_TYPESPEC_ATOMIC_type, C_TYPESPEC_ATOMIC_value = (x), __VA_ARGS__ }
#define C_TYPESPEC_ATOMIC_get(x) ((x) C_TYPESPEC_ATOMIC_value)
#define C_TYPESPEC_COMPOUND_type .type=ENUM_C_TYPESPEC(COMPOUND)
#define C_TYPESPEC_COMPOUND_value .value.Text
#define C_TYPESPEC_COMPOUND_set(x) (struct c_typespec){ C_TYPESPEC_COMPOUND_type, C_TYPESPEC_COMPOUND_value = (x) }
#define C_TYPESPEC_COMPOUND_xset(x, ...) (struct c_typespec){ C_TYPESPEC_COMPOUND_type, C_TYPESPEC_COMPOUND_value = (x), __VA_ARGS__ }
#define C_TYPESPEC_COMPOUND_get(x) ((x) C_TYPESPEC_COMPOUND_value)
#define C_TYPESPEC_ENUM_type .type=ENUM_C_TYPESPEC(ENUM)
#define C_TYPESPEC_ENUM_value .value.Text
#define C_TYPESPEC_ENUM_set(x) (struct c_typespec){ C_TYPESPEC_ENUM_type, C_TYPESPEC_ENUM_value = (x) }
#define C_TYPESPEC_ENUM_xset(x, ...) (struct c_typespec){ C_TYPESPEC_ENUM_type, C_TYPESPEC_ENUM_value = (x), __VA_ARGS__ }
#define C_TYPESPEC_ENUM_get(x) ((x) C_TYPESPEC_ENUM_value)
#define C_TYPESPEC_TYPEDEF_type .type=ENUM_C_TYPESPEC(TYPEDEF)
#define C_TYPESPEC_TYPEDEF_value .value.Text
#define C_TYPESPEC_TYPEDEF_set(x) (struct c_typespec){ C_TYPESPEC_TYPEDEF_type, C_TYPESPEC_TYPEDEF_value = (x) }
#define C_TYPESPEC_TYPEDEF_xset(x, ...) (struct c_typespec){ C_TYPESPEC_TYPEDEF_type, C_TYPESPEC_TYPEDEF_value = (x), __VA_ARGS__ }
#define C_TYPESPEC_TYPEDEF_get(x) ((x) C_TYPESPEC_TYPEDEF_value)

#define C_TYPESPEC_get(x) ((x).value.Text)

#endif
