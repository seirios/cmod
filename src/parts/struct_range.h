#ifndef PARTS_STRUCT_RANGE
#define PARTS_STRUCT_RANGE

#include <stdint.h>

/* integer range for lambda tables$ */
struct range {
    intmax_t lo;  // lower bound (inclusive)
    intmax_t hi;  // high bound (exclusive)
    intmax_t inc; // increment
};

#endif
