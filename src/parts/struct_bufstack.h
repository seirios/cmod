#ifndef PARTS_STRUCT_BUFSTACK
#define PARTS_STRUCT_BUFSTACK

#include <stdio.h>
#include <stdbool.h>

struct bufstack {
    struct bufstack *prev; // store previous buffer
    FILE *fp;              // file pointer
    char *fname;           // file name
    char *fpath;           // file path
    bool noblank;          // suppress blank lines?
    bool quiet;            // suppress all output?
    YY_BUFFER_STATE bs;    // parser buffer state
    YYLTYPE lloc;          // parser location
};

#endif
