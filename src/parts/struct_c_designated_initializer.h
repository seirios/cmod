#ifndef PARTS_STRUCT_C_DESIGNATED_INITIALIZER
#define PARTS_STRUCT_C_DESIGNATED_INITIALIZER

#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"

struct c_designated_initializer {
    struct c_designator dsg;
    struct c_initializer ini;
};

#endif
