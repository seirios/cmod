VALGRIND ?= valgrind
MEMCHECK_FLAGS := -q --error-exitcode=1 --exit-on-first-error=yes --leak-check=full --errors-for-leak-kinds=definite,possible
check_valgrind := $(shell ([ -n "$(VALGRIND)" ] && type $(VALGRIND) >/dev/null 2>/dev/null && echo 1))
export MEMCHECK := $(if $(check_valgrind),$(VALGRIND) --tool=memcheck $(MEMCHECK_FLAGS),)

# valgrind does not handle seccomp (317) syscall
ifneq (,$(MEMCHECK))
export DISABLE_SECCOMP := --disable-seccomp
endif # MEMCHECK

# valgrind does not play well with ASAN
ifeq (1,$(ASAN))
MEMCHECK :=
endif # ASAN
