![cmod](img/cmod_banner.png)
# TL;DR

C% (pronounced "c mod") is an experimental meta-programming language. The main goal of C% is to bring meta-programming to the C programming language, while keeping true to its spirit: *trust the programmer* and *don't prevent the programmer from doing what needs to be done*.

Keywords in C% begin with a **%** sign and are usually embedded into C code, where they are evaluated in-place without interfering with the surrounding code. There are currently 21 keywords that can be used for generic code generation and 11 keywords with semantics specific to C, as well as comment keywords. There is also a C% standard library with constructs intended for inclusion into C programs.

C% has two main types of objects: parameterized code snippets (**%snippet**) and data tables (**%table**, in TSV or JSON format). Snippets can be expanded in-place with arguments (**%recall**) and tables can have parameterized code mapped to them (**%map**). These two objects allow for reusability of code and data within and across compilation units. All generated code is present in the output, to allow further inspection by the programmer and the compiler.

Other notable keywords allow defining and evaluating compile-time domain-specific languages (**%dsl-def** and **%dsl**) or performing I/O with arbitrary shell commands (**%pipe**), enabling dynamic multi-language meta-programming. C% can also be used for more general automated code generation, e.g. Python code, etc.

Currently, the main use of C% is in developing its own intepreter `cmod` and other projects of mine, but I hope that you may find it useful for your own projects too!

# C%: cmod

C%: cmod (from "C with mods") is an experimental C meta-programming language and pre-processor (C% is the language and `cmod` is the pre-processor).

The aim of C% is to extend the C language via constructs that allow for faster coding, hopefully without becoming too cryptic. Currently the pre-processor is implemented as a flex/bison parser that recognizes C% keywords and generates C code. While parsing, anything that is not a C% keyword is passed through, and the final output contains no C% keywords. This means C code is not parsed (except inside C% keywords specific to C), and all generated C code is present in the output, which makes it open for inspection and is subject to validation by the C compiler.

Conventional file extensions are `.cm` for code and `.hm` for headers. The usual workflow is for `.c` or `.h` files to be generated from the corresponding `.cm` or `.hm` files with `cmod` (as in the included makefile), and then run through the C compiler. C%-only `.hm` headers containing static data tables and code snippets are also common, as are static data tables stored in TSV or JSON formats.

Beyond C meta-programming, C% generic keywords can be used with other languages such as Python, Flex / Bison specification languages, etc. Given the byte-perfect nature of the parser, all spacing is preserved and C% keywords do not introduce padding or otherwise spurious characters.

For real-world examples look under `src` for the source code of `cmod` itself (that's right, `cmod` is self-hosted!). After every source code update, `cmod` has to re-build itself with strict warning flags under memcheck, to ensure everything still works! The C% unit tests under `tests` may also serve as examples.

The C% standard library contains convenience constructs for inclusion in C programs.

Additionally, a VIM syntax highlighting file is provided to ease coding in C%.

## Rationale

I love programming in C![^1] It's a small and elegant language that lets you do whatever you want with the computer's resources and the information stored within, down to the very last bit.[^2]

Having so much freedom and power, however, comes at the cost of having to build all the structure yourself from the ground up. This tends to be a bit verbose and tiresome, so my goal with C% is to reduce this tedium and make coding in C more efficient and fun!

Some of the things that C% aims to make achievable are:

+ **Reusability**: Avoid source code duplication.
+ **Consistency**: Use the same data across different locations.
+ **Efficiency**: Perform common tasks quick and easy.
+ **Concision**: Write and work with concise code.
+ **Expressivity**: Better express the intent of code.
+ **Transparency**: Hide nothing from the programmer.
+ **Abstraction**: Handle similar things in a uniform manner.
+ **Extensibility**: Easily add new functionality.
+ **Simplicity**: Keep the language simple but powerful.

All while keeping true to the spirit of C, *trust the programmer* and *don't prevent the programmer from doing what needs to be done*!

Of course, the drawback is to have an additional processing layer in yet another language, which may over-complicate things and become an additional source of bugs... or maybe simplify things and reduce the potential for errors!

In any case, I find C% useful and enjoyable, and I hope you do too!

[^1]: C99 to be precise (although C23 looks promising!)

[^2]: As long as the compiler actually does what you expect, and the kernel can actually manage those resources and provides a way to do so, but well...

## Compilation

Requirements are lean and ubiquitous:

+ POSIX-compatible OS: GNU/Linux, macOS, Cygwin, BSD (untested)
+ C99 compiler: `gcc` or `clang`
+ GNU Make

Optional requirements:

+ [libseccomp](https://github.com/seccomp/libseccomp) (encouraged for safety)
+ [Criterion](https://github.com/Snaipe/Criterion) (unit tests only)
+ [Flex](https://github.com/westes/flex) v2.6.4 (maintainer only)
+ [Bison](https://www.gnu.org/software/bison/) v3.7+ (maintainer only)

To compile, Just Run Make™!

If things don't go well, please check that your system meets the requirements above. Please also check that the command paths inside `Makefile` and the configurable options inside `config.mk` (auto-detected) are appropriate for your system.

If you need further assistance, please [create an issue](https://gitlab.com/seirios/cmod/-/issues).

## Examples

Some initial motivating examples. Many more in the documentation below.

### Define multiple functions with the same signature (function type)

When writing a GUI, a callback must be written for each object (button, menu, etc.). These callbacks all have the same signature, and it becomes tedious to write (or even look at) them. Of course, this can be alleviated by using some sort of IDE, but that misses the point, which is to express the fact that all these functions are of the same type.

This can be achieved in C% is by using the `%typedef` and `%def` keywords:

    %typedef OBJECT;
    %typedef void CALLBACK_F(OBJECT *obj, long data);
    
    %def CALLBACK_F btn_callback {
        /* use obj and data (we already know these are the parameters!) */
    }
    
    %def CALLBACK_F menu_callback {
        /* use obj and data, again */
    }

    /* etc. */

### Define a function with named parameters

C does not support named parameters, but it's a well-known trick to wrap them in a struct and use designated initializers to name them. Of course, this has its pitfalls, but if you want to do it, you *should* be able to do it, easily.

In C% we can do the following (full example):

    #include <stdio.h>
    #include <stdlib.h>
    
    /* type of a qsort-compatible comparison function */
    %typedef int cmp_f(const void *keyval, const void *datum);
    /* in case you are like me and always forget the order of these arguments... */
    %proto [named] void qsort_n(void *base, size_t nmemb, size_t size, cmp_f *compar);
    
    %def qsort_n {
        /* argv contains the function argument values */
        return qsort(argv.base, argv.nmemb, argv.size, argv.compar);
    }
    
    %def cmp_f cmp_int {
        /* we already know the parameter names from the typedef */
        int a = *(int*)keyval;
        int b = *(int*)datum;
        return a < b ? -1 : (a > b ? 1 : 0);
    }
    
    int main(void) {
        int array[] = { 7, 3, 5, 1 };
        /* we provide the named arguments in any order */
        qsort_n(.compar = cmp_int,
                .size = sizeof(*array),
                .nmemb = %arrlen(array),
                .base = array);
        %foreach elem (array) %{
            printf("%d\n",${elem});
        %}
    }

## Known issues and limitations

+ Nested expressions are not supported, requiring explicit delayed evaluation of outer keywords.
+ Types not built-in must be manually registered  with `%typedef` for C declaration parsing to work.
+ No proper multi-byte support, but scanner is transparent to valid UTF-8 and verbatim input is checked.
+ No support of `%pipe` under Cygwin (some forking issue) and sporadic failures under `make -j`.
+ C% keywords are recognized inside C string literals, on purpose. String splitting may be required to avoid unwanted behaviors, e.g., `"%""#"`.
+ Only UNIX-style newlines (line feed) are supported.

## Some future features

+ Use PCRE2 for better Unicode support in regex.
+ Support nested keyword expansion inside argument lists.
+ Expand C% standard library (e.g. key-value stores, etc.).

## Acknowledgements

+ libsrt: safe real-time library for C (original by [faragon](https://github.com/faragon/libsrt))
+ JSMN: minimalistic JSON parser in C (original by [zserge](https://github.com/zserge/jsmn), strict-parsing version by [dominickpastore](https://github.com/dominickpastore/jsmn/tree/strict-parsing))
+ [utf8](https://github.com/cyb70289/utf8): fast UTF-8 validation with range algorithm

## Social

+ Check us out at FOSDEM'22! [C meta-programming for the masses with C%: cmod](https://fosdem.org/2022/schedule/event/lt_cmod)

## Donations

If you like this project and would like to further its development, you may donate crypto:

+ BTC: `3LGnjqEdpsaUJ7GtewDzadW9tbVgm6m99W`
+ ETH: `0xe5F008C2aD245BEBBb9820ed76DD00fe096D53Ec`
+ BCH: `bitcoincash:qz5uem9f64vp0wh5drvgypdhkucx2r40uv8xc52m7l`
+ BNB: `0xe5F008C2aD245BEBBb9820ed76DD00fe096D53Ec`

## Unfrequently asked questions

### Is C% useful at all?

It's useful enough to write its own pre-processor and make its own development easier. It's also useful for some of my other projects. I'd love that it became useful for other people's projects in the near future :)

### Won't this make my code too long or obscure?

Not necessarily. Extensive use of `%recall` / `%map` or the C% standard library may result in lots of generated code. Unless that's a concern (e.g. embedded systems), the benefits to the programmer outweight the increase in generated code length and executable size. Regarding code clarity, it depends on the complexity of the meta-program's logic, and finding the right amount of automatization is up to you; using multiple steps or pre-generating some parts may help in keeping the code clear.

### Why not use CPP macros or C functions instead of C% snippets?

Unlike snippets, pre-processor macros are ugly (all those continuation slashes), do not allow argument transformation (e.g. wrap, case fold, etc.) and cannot define other macros. Snippets would be used at least whenever a macro is preferable to a function, that is, to avoid the cost of a function call, to operate on variables disregarding type, to mix in code into a function's scope, etc.

### Can I use C% with languages other than C?

Sure you can! By passing the `-g/--generic` flag to `cmod`, C-specific keywords are disabled and you can meta-program whatever language you like where C% keywords don't have a special meaning. I've used C% successfully with Flex/Bison definition languages and with Python (where whitespace preservation comes in handy).

### Isn't `%pipe` too unsafe to be good?

Currently, `%pipe` can execute arbitrary commands without restrictions only if passed the `[unsafe]` option, and is a potential security liability if left unchecked (as much as running any shell script of unknown origin). *Always check commands before running them on your machine!* However, to make `%pipe` safer, syscall filtering with `libseccomp` has been implemented, as well as an execution timeout to avoid hangups. Notwithstanding these issues, being able to write a multi-language script that generates code is a pretty cool feature!

### What's up with the versioning scheme?

This is an experimental project, so the latest version is always the best version :p Having said that, I try to use semantic versioning but do so sloppily. If there start being other users, I will become more careful with breaking changes and such, even perhaps introduce required versions in the code.

### Shouldn't C% or `cmod` do these \*\*\* things differently?

By all means, please explain and/or propose a better way by [creating an issue](https://gitlab.com/seirios/cmod/-/issues)!

## Documentation

Below is the full documentation (automatically generated):

+ `cmod` usage (`cmod --help`)
+ C% keyword summary (`cmod --docs`)
+ C% language reference with examples (`cmod --docs-full`)
+ C% standard library reference (`cmod --docs-stdlib`)

