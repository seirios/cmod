# TODO #

# IDEAS #

## Add PCRE support (with libpcre2)
   + Escape from POSIX's Unicode-unaware hell!!!

## Do [byline] regex sub using ss_split on newlines
   + Then take ss_to_c() of each srt_string_ref
   + Properly handle last line, as in ss_cat_byline

## Check use of ss_void throughout
   + It's a valid empty string, NOT a NULL pointer!

## Check use of ss_allocation_errors and ss_encoding_errors
   + Instead of ss_void or NULL checks!

## Do without NUL bytes in handling verbatim input...
   + Use srt_string always!

## Fix mess of NUL-terminated vs non-NUL-terminated strings...
   + In regex replacement related stuff

## Beware! %snippet [now] performs only one parsing pass...
   + Should handle multiple parsing passes?
   + This would make us a buffer-to-buffer const-rsrc parser!
   + Remember to update pp->eval_limit

## Evaluate C% keywords inside cmod_arguments
   + C% expression grammar
   + Not just function-like keywords, also e.g., %| |%
   + Not just inside cmod_arguments, also inside e.g., %intop

## Some kind of pass-through DSL syntax (as is C%)?

## What about %mylang{ %} blocks instead of %dsl mylang %{ %} ?
   + Tagged mod block support [DONE]
   + Would have to be exposed in INITIAL... are we ready to leave the box?

## On table append...
   + With column names, out of order should work (using the column map)
   + With column names, subset should work (setting other columns as %nul)

## Better locations tracking by storing sub-element locations on parse
   + E.g. formal arguments and snippet body in snippet_like

## Use &(struct param){} instead of allocating global state on heap?

## Change index options used as enums to str options with checked values

## Profit from %snippet [now] in cmod's code

## Show blame in cmod_option parse error messages

## Reduce error logging redundancy to reduce object size

## Unify bools into bitflags
   + In cmod_option
   + In cmod_pp !!!

## Reduce object code size with compressed static arrays

## Remove use of VLAs?
   + To be compatible with compilers that do not implement them, e.g. CompCert

## [D] Add %tree, similar to %table but describing tree data
   + XML format? verbose but allows attributes, what parser?
   + JSON format? we already have a parser, so yes
   + Snippet code visits all leaves (in depth-first or breadth-first) manner
       + Make level and full path available to snippet, somehow
       + Attributes as variables, empty if not present
   + Execute action on enter/leave a level
   + Useful to describe UIs

## Add %fsm keyword describing finite state machines in SMC syntax, printing C code

## Add %graph similar to %table but describing graph data?
   + Useful to describe pipelines (DAGs)

## Change table implementation (?)
  + Rows stored as vector of struct table_row
  + struct table_row has a single srt_string as buffer + index array
  + Values are stored as contiguous NUL-terminated strings
    + Offsets are stored in the index array
  + More memory-efficient?

## Add some sort of lightweight object syntax
   + For rapid prototyping
   + As a built-in DSL?

## [C] Incorporate libtcc for dynamic code generation
   + Compile-time C evaluation (as with %pipe, but C-specific)
   + Run-time code evaluation, run-time optimization

## [Z] Validate value from env var TMPDIR

## [C] Parse C expressions into an AST
   + As a prelude to expression parsing in user-defined DSLs

## [X] Implement %dsl to define domain-specific languages... is this even possible ? YES!
   + %dsl [opts] <name> <defining table>
   + Pre-define grammar, assign snippets to tokens:
      + operators (with precedence, specified as an int or a char constant for a known C operator)
      + keywords (with known number of arguments)
   + Possible grammar types
      + [asm] one instruction per line [DONE]
         + allow custom separator: semicolon or colon [DONE]
      + [math] unary and binary operators
      + [sexp] pure expression tree
      + [c] C-like?
   + Parentheses open verbatim argument lists if preceded by a keyword
   + Parsing replicates the DSL keyword until no symbols inside
   + Evaluate with %dsl <name> %{ <code> %}
   + Sub-expression parsing with C% parser
      + Allow function-like keywords and %recall
      + Must parse expression as AST, then evaluate

## Fix mess of str_enums pre vs lead

## Do we really need to keep track of enum constants?
   + Let the C compiler deal with duplicates, etc.

## Add type annotations to snippet arguments / table columns?
   + Syntax: #id myarg,#int mynum

## Add variables with @ prefix ?

## Add flag --debug-timings
   + Setup global timer since program startup
   + Print time elapsed on select logging messages

## Provide better meta-programming capabilities in snippets
   + Built-in DSL inside mod-blocks with loops and conditionals? Better than %strcmp...

## Define static snippets by parsing C% code and outputing static objects

## Late-night C% ideas
   + Do make keywords output to buffers, instead of writing directly to file. Then print buffer on keyword completion (cannot be later, as scanner is echoing in the meantime).
   + Could this lead to (inside-out) nested evaluation?
   + Do we make the scanner ECHO into a buffer too? This would allow full in-memory parsing!
      + How would we handle %include with full in-buffer parse?
   + Then perhaps we could make the parser into a library, which is then used by cmod. This would allow including C% in other projects! (Is this useful?)
   + Give meaningful names to nonterminals and user-friendly string representations to tokens.
   + Text-to-object conversion with standalone parser in cmod's code itself! No more extrinsic snippet definition!

## Put columns first in lambda_table, for consistency with other tables?
## Get rid of lambda tables?

## Implement a Table class!
## Support %map [x] in rule 0 too
## Reimplement %enum as a %map under the hood
   + Allow enum to take %NR as column (for value)

## Allow multiple sort and uniq indices, to filter based on second, third, etc. columns?
## Use .hm for snippet-only headers, use .c.cm and .h.cm for source files
   + Avoid having to eat output with %delay
   + Is this possible? What about snippets that output C code AND define other snippets?
      + Avoid these?

## Remove parameter names from C declarations (prototypes and typedefs) [RLY?]
   + It's pedantically safer, as macro expansions could otherwise happen
   + No way around it... we need to parse C declarations into an AST!

## Define a DSL std:getopt to express when options are incompatible
## Allow extending already defined DSLs
## Allow user-defined dllarg transformation, with some syntax?
## Fix the exit_on newline hell!
## Uniformize C% syntax, so we avoid exit_on mess...
   + Only necessary for CMOD <-> C switching
   + If we had a separate C parser, perhaps we could capture C code verbatim
     and handle as post-processing

## Better rule documentation beyond grammar description
   + Unwrap optional clauses into separate entries

## Add top comment to generated C files
   + "This code was generated by cmod version ... under ..."

## When we have a separate C parser, use it to improve C-specific keyword parsing

## Allowing overriding default columns in tables
   + Placeholder sequence could be %sub, (or %dfl) mapping to 0x1A (SUB) character in TSV
   + Compute default values on-the-fly, except when overriden actually store it
       + Placeholder value for on-the-fly values could be NULL
       + We need a row+col value-getter function that recognizes default columns and returns its value
       + We *really* need a Table class...

## Variable number of arguments in snippets ?
   + Ellipsis notation ...
   + Reference by position only?
   + Add argument list manipulation syntax
      + Easier passing of (a part of) argument list to recall

## Add more unit tests with %unittest
   + e.g. for C parsing stuff

## Modularize code for easier understanding ':]
   + Track and split code dependencies

## Turn parser:build_table into a function (reduce code size)

## Incorporate faragon/libsrt
   + Strings (or bstrlib), instead of str
      + UTF-8 support
   + Vectors, instead of arr_\* [DONE]
   + Hash tables, instead of sorted arrays [DONE]
      + Implement %table [hash]
   + Add [utf8] to %strlen, %strcmp, etc.
      + %strlen [DONE]

## Improve wiggly error reporting for parser errors, where possible
   + Syntax errors, handled by yyreport_syntax_error
   + Semantic errors, handled manually for each keyword
   + Where resource name not found, if in same line [DONE]
   + Capture error context by seeking input file and collecting relevant lines [DONE]
   + Keep context for the whole keyword (multiple lines) [DONE]
   + Handle newlines inside a keyword
      + Tricky to know where the error originated
      + Currently, we search a string in context [DONE]
         + Error-prone, as it may give wrong matches
      + We would have to reparse the input...
         + Re-entrant scanner to the rescue!
   + Make print_wiggly_error into a function! [DONE]

## Improve file handling code... it's bad at best
## Improve %pipe implementation (what else is needed?)
## Change name of %pipe to %exec (breaking change!!!)

## splint the hell out of the code!
   + Does not support C99 syntax... :s

## Better stacking of tables with default columns ?
   + Keep default columns as default, move to end, keeping order
   + Avoid storing default values!

## Also DSLs could use herestrings tagged as #ASM, etc., since there is no argument expansion there anyways
   + Benefit of mod blocks is they don't require a newline after
   + Do we use a %dsl{ block?
   + Better yet, use %asm{, %expr{, etc. a mod block per DSL syntax type
       + Return type would be an AST, which then gets evaluated to snippet recalls
       + But... makes no sense, as the syntax type of a DSL is always known before evaluation

## Introduce %tag keyword for code annotation
   + Syntax: %tag <identifier> <constant_expression>
   + Ouputs <constant_expression> as if nothing had happened
   + Ability to report all tags across parsed files with --report-tags
   + Ability to map lambdas over tags with %map [tag] <identifier> %{%}
   + Tag stores expression and its location (line number, file name and parsing pass)

## Introduce %analysis keyword for static code analysis and code generation
   + Example: give it a formula, and let it perform numerical analysis on it to find the best way to perform the operations without loss of floating-point precision or integer overflow, then report on this and print the solution found to file.
   + Example: benchmark a given piece of code with different parameters, and find optimal solution for a given runtime / machine, report on it and print the solution found to file.
   + Example: detect safety / security violations according to coding standards.

## Keywords concatenate into string buffer instead of printing
   + Allow wrapping, etc. the printed buffer
   + Add wraping chars as options to all keywords

## Reduce code size!
   + Snippets to functions, where possible
   + Return code handling can be a mess... indeed!
   + Iterate over static arrays instead of unrolled loops
   + Use hash tables to map strings to addresses!

## Allow angled-bracket paths in %include
   + Imply [sys] keyword

# BACK BURNER IDEAS #

## Add std:dict for key-value data structure support based on fmela:libdict (BSD-2)
## Add std:interface for generating interfaces (classes) as in interface99 (MIT)

## C% stdlib wrappers for proper use of:
   + C standard library (C99)
   + POSIX API
   + Linux API

## Allow %def to define variables inside functions
   + Begin/end for type pre/post conditions
   + Pre-conditions on initial value, and then?

## Make %prefix into a non-C-specific keyword, can have other uses
## Allow specifying output file in %unittest
## Use mimalloc allocator
## Implement hash table in std:dict
## Use power-of-2 allocation in autoarr types
## Speedup adding of built-in types at startup
## Use named references in parser rules (better practice)
## Make parser API less bound to flex/bison ?
## %foreach [autoarr,const] should get the const pointer type automatically
## Change %foreach syntax (borrow from D)?
## Allow %map sort putting pointers first (for structs)
   + Just use [notnull] with sentinel for pointers and map twice

## Split C and C% parsers into separate files, included from parser.ym
## Revisit error checking for application functions (e.g. in cmod.ext.cm)

## Allow detecting duplicate type definitions
   + %typedef cannot detect previously defined type names
   + They become C_TYPEDEF_NAME instead of C_IDENTIFIER and produce a syntax error
   + Attempts to fix this will incur in reduce/reduce conflicts, requiring move to GLR parser

## Allow linking together delay values with named identifiers
   + Dangerously close to having a variable declaration syntax...

# WISHFUL THINKING #

## Unify C% syntax (single start condition)
## Proper UTF-8 support (e.g. à la TXR)
## Full C parser and static analysis
## Replace all C strings by srt_string
## In-memory parsing without tempfiles
## Build cmod with autotools
## Support i18n via GNU gettext
   + Uniformize error messages for easier translation
   + Genericize error messages for easier translation

# DISCARDED IDEAS (WON'T FIX) #

## Rename [add1] to [add] throughout
   - But would make no sense when given without value...

## Change use of | in %map to -> (breaking change!)
   - Looks bad with snippet () <- () args...

## More efficient pre-loading of standard C types
   - If only we could define a srt_hmap statically...

## Use CCODE in all MODE_C keywords
   - Cannot as %enum needs a table name...

## Faster parallel tests with parallel make (instead of make -j)
   - Is not faster...

# BAD IDEAS (TRIED AND FAILED IN TOTAL MESS) #

## Avoid duplication of constant strings by passing constant pointers that are never freed
   - Messy to keep track of which pointers are constant and which can be freed

## Add a CLI flag to emit #line directives in generated code for better error reporting
   - Line numbers end up being a total mess after multiple parsing passes

# DONE #

## Fix enum [helper] invalid values
## %strsub [icase] only through [regex]
   + Case-insensitive Unicode search is not trivial!

## Move non-regex replace to ss_replace
   + Define ss_replace_single in libsrt to perform non-global sub

## Add [str] to all options where it is meaningful
## C escaping str_tran?
   + Use character 'g'?
   + Allow embedded NULs, change strcesc to accept a length
   + Make ss_enc_esc_c use the new strcesc

## Wrap str_* fields in dllarg into struct str_mod
## Add [mod] option to str_enum-transform output keyword buffer?
   + Then deprecate %recall [str] and [supnl]
   + Then deprecate %pipe [str] and [supnl]

## Add syntax %table name := (columns) '\n' for an empty table
## Implement += for...
   + Tables: perform row stacking (column stacking with |=)
      + E.g., as in parser:opt_rule, parser:list_rule, etc.
      + Strictly same columns and shape
      + Implemented as an option [append], that the operator activates
   + Snippets: append body and dllargs
      + Ensure correct offset in dllargs
      + Strictly same formal arguments
   + DSLs: same as [extend]
   + Enums: not possible, cos we don't keep'em as objects.

## %enum should not ignore empty table and quit if [default] is set
## %enum allow specifying [helper=#] to decide what to emit
## Use strncmp with string literals in %switch [string]
## What about %snippet var '\n' meaning an empty redefinable snippet?
   + Like a variable definition
   + Then can be filled-in with +=

## Avoid generating rowno if not used
   + Check snippet dllargs for ${#NR}
   + Check map actual arguments for %NR

## Rework cmod_arg_type tables in parser
   + Say "unexpected argument of type bla instead of "expected bla or bla or bla..."

## Convert errline_find_name into a function
## Check errlloc in all of KEYWORD_ERROR
## Check errlloc in all of ABORT_PARSE
## Check free_vopt et al.
## Use snippet specializations in parser.ext.hm and cmod.ext_tab.hm
## Automatic type conditions check in %def
   + If %typedef has [begin] or [end], call it in %def
   + For input parameters or return value
   + Disable with [nocheck]

## Implement %snippet [now]
   + Parse C% from and into buffers

## Define default snippets by using actual snippet textual representation
   + Would require parsing them with the cmod parser itself...

## Use a map for storing prefixes, avoid linear search
   + Avoid memcpy with proper element deletion
   + While keeping order (sorted iteration)

## Change autoarr dup* snippets to expect destination address as output param ?
   + To avoid copying structs when duplicating...

## Add INITIAL token %dnl to delete newline
   + Function-like keywords may generate spurious newlines...

## Introduce %nul in argument lists to mean empty string
   + More visible than ``

## Allow extended identifiers as values where identifiers are allowed
   + Instead of passing them as verbatim

## Simplify handling of free/dup snippets in std:autoarr and std:variant
   + Optional table columns (free,dup), otherwise call free_* or dup_* snippet
   + User-configurable prefix for snippet names, e.g. free_ or dup_ as above

## Allow further inspection in %defined
   + Check if table has column
   + Check if snippet has formal argument

## Extract types in C parameter lists
   + Check for a single type specifier

## Move snippet load from file to new rule
## Scanner errors should not terminate program, but make parse fail
   + This way sub-parses work well

## Use goto in keyword shorthands to reduce code size
## Add flag to only expand dllarg if not empty
## Load snippet from file!
## Allow passing a recall_like to %pipe
   + Move %pipe action to function, to avoid large code duplication

## Show blame in JSON table parse error messages
## Fix %table-stack [common]
## Proper support of =, +=, ?=, and := in option list
           multiple         single
        isset   ~isset  isset   ~isset
    =   ERR     set     ERR     set
    +=  add     set     ERR     ERR
    ?=  noop    set     noop    set
    :=  reset   set     reset   set

## Unify handling of list option types
   + Split into another table

## Fix dup_* functions' mess...
## Allow multiple values in %switch cases
## Support embedded NULs (binary data) in snippets
## Ignore values of enum constants when parsing C function prototypes
## Properly parse DLLARGs in the parser, not the scanner!
   + Add syntax for UTF8-encoded Unicode chars ${#0xffffU}
   + Add syntax for bytes ${#0xffB}

## DSL improvements
   + Add container expression with custom delimiters, e.g. list
   + Allow chain, separator operators to be empty or have a snippet, with different behavior
   + Move [asm], [sexp], [math] to [syntax=``]
   + Add [tree] syntax for expressing hierarchical data or operations
   + Add support for comments in [syntax = custom]
   + Add block expressions to custom syntax

## Allow [begin]/[end] in %typedef
## Improved [return] in %def
   + End statements are always evaluated before actual return

## Print debug strrepr line by line
## Add string representation of lambda snippets
   + Add string representation of dllargs

## Fix span of lines printed in error traces
## Fail when a block comment reaches EOF
## Implement lazy keywords
   + %def: use new str_repr
   + %map
   + %table_get
   + %table_length
   + %table_size
   + %table_stack

## Add %recall [lazy]
## Add [debug] to all keywords
   + Verbose logging of keyword invocations

## Auto-generate functions to recompose the textual form of any keyword invocation
   + Define printers for option types
   + Define printers for keyword option lists
   + Define printers for keyword arguments
   + Define printers for grammar parts used in keywords

## Implement %include [quiet]
## Fix handling of dangling commas
## Put all objects into a lib for linking test execs
## Add a TL;DR to README
## Rename table_find to table_get
   + Add rule to retrieve value from row index and column name (even if not [hash])

## Make DSLs extensible!
## Allow defining DSLs with other syntax types
   + S-expression
   + Math-like operators and functions

## Parse DSL statements into a list of ASTs consisting of snippet calls with arguments
   + Two node types: CALL (snippet name) and CONST (verbatim value)
   + CALL gets evaluated into a CONST
   + Final result is a list of CONST, which becomes a list of strings

## Keywords write to a buffer instead of directly to file
   + Buffer is srt_string, which gets printed once after putting everything inside
   + Printing happens at end of keyword rule

## Allow everywhere unquoted identifiers as arguments
## Add named parameters to %defined
## Table syntax could use a mod block taggged with tsv or json
## Do not store values for default columns
## Store column names and values as vec_namarg instead of sv_colnam (vec_string)
## Consider also default column values in %tablen
## Allow [hash] with column name
   + Fail if not found
   + Fail if selected column has default value

## Unify recall and map rules (0,2)
## Both %recall and %map take a <recall-like>: <snippet name> <actual args>
   + Only difference is map accepts references to table columns, which are resolved by %map
   + %map should resolve into <table-like> <recall-like> or <table-like> <code>
   + Introduce | to split <table-like> from <recall-like>?
       %map table, table2, table3 | snippet (args)

## Unify map rules (0,2) and (1,3)
## Table from file
    + [json] and [tsv] apply to %table "path", which defaults to using file extension
    + TSV table name defaults to file basename without extension

## Use ?= instead of [opt]
## Factorize table definition and column request syntax
## Use cmod_arguments_unique in all function-like keywords, allowing named arguments in keywords themselves!
   + table-find
   + strstr
   + strcmp, just naming, no reordering
      + names are: input, match/#, then/#, else (where # is the pair position in the argument list)
      + allow passing integers, with automatic numeric comparison
   + strsub, just naming, no reordering
      + names are: input, match#, sub# (where # is the pair position in the argument list)
   + Also in keywords that name specific table columns, allow reordering
      + names for enum: name[, description[, value]]
      + names for strin: word, action
      + names for dsl_def: keyword, snippet [, action]

## %strin [nomatch] should allow any snippet having only one non-default input argument
   + Proper snippet argument substitution, not just a snip_get_buf

## Count iterations in range expansion, instead of looping
## Make namarg value into a variant, allow non-string values
   + Allow passing integers without re-conversion to string [WONTFIX]
      + Cannot do this, as we can't recognize int integers from string integers
   + Allow passing expressions, to be computed at %recall time
      + Expressions in snippet argument default values, allowing using values from other arguments. References with leading hashtag, e.g. #arg. Concatenation with . (dot) operator. For example, and argument list like:
        (type, size = `sizeof(` . #type . `)`)
      + This would avoid the need to define small snippets inside snippets with %* [redef]
      + Cannot reference self, of course
   + Allow the do-not-expand-unless-explicitly-given wildcard *

## Allow arglinks from input to output arguments, and vice-versa
   + Allow arglink to arglink (ARGLINK_MAX_FOLLOW times)
   + Disallow arglink to self

## Defining a formal argument as <arg> = *
   + Makes it not expand if not explicitly given (different from <arg> = `` which means expand to empty).
   + This allows recalling snippets inside with their own defaults, using $zb{arg};
     this would pass the argument only if given explicitly.

## %map [notnull] skips rows based on column being NUL
## Allow new arglinks in snippet generalization
## %map [uniq] and [sort] by name
## Allow dangling equals in cmod options, meaning empty string
## Allow more option types, e.g., SIGNED_INT to allow negative [add1]
## Change "invalid" stuff to "default" stuff in %enum
   + Helper functions are actually incorrect ever since we set invalid_name from option
   + Option [nodefault] to remove the default case (as if given value column)
   + Think what to do with helper when there's no default

## Automatic dependency tracking with gcc -M
   + End the full remake torment!
   + C dependencies in Makefile
   + C% dependencies in src/Makefile

## Get rid of parser:tuple_rule use
## Unify argument lists into cmod_argument_list
   + Define specific nonterminals for map, table, snippet, and recall,
     where we perform the necessary checks

## Allow list of names to %defined, check [all] or [any]
## LZ-compress keyword notes (where possible)
## Fix locations after error token
## Rethink what a "fatal" error is, allow collecting errors
## Fail on first error flag
## Introduce %map [nl2sp]
## Snippet ignore escaped newline
## std:getopt : allow storing flags in longopts
## std:getopt : fix summary message
## Better verbose option summary in cmod
## Handle -d/--disable and -e/--enable as lists
## Store flags in cmod longopts
## Allow wrapping, i.e. "generalizing" a snippet with dummy arguments
## New snippet definition syntax for specializing with values (snippet closure)
   + E.g. instead of defining snippets that recall other snippets,
          directly define a snippet as another snippet with some values given
   + Syntax: %snippet <name> <alias> (<default output values>) <- (<default input values>)

## Allow using values from more than one column in %strin
## Merge input-only and input-and-output parameter rules
## Document special dllargs
## Introduce ${#ARGC_IN}, ${#ARGC_OUT} and ${#ARGC} to count arguments
## Proper validation of column names as IDENTIFIER_EXT when read from file
## Output input file list on parser error, for debugging
## Allow referencing column names in map lambdas
   + dllarg->num = 0 no longer a sentinel, positional references begin at zero
   + separate dllarg->type field to choose between normal and special values
   + same mechanism to refer to snippet formal argument names
   + use # as prefix for special dllargs (in scanner), outside formal argument namespace
      + e.g. ${#0} is the name of the first argument
   + use ${#@} for comma-sep list of all arguments

## Error message: change "unused table columns" -> "unused columns"
## Define function for int to string conversion
   + Static data for 0-99, reused
   + Reuse static number strings when expanding ranges

## Do NOT declare table variable inside parser:table_find
## New %table-find rules using implicit key column
## Allow default arguments in %proto without [named] using shadow macro
   + Must have at least one non-default argument, cannot override defaults (for lack of VA_OPT)

## Allow setting shadowed function suffix in %proto
## Static strings for first 100 row numbers, to avoid generating each time!
## Use %strin to choose syscall classes in %pipe
## Allow %strin [nomatch] snippet for else action
## Rename %strin [default] to [none]
## Add %table-length reduction over multiple columns in multiple tables
## Use %dsl vstr instead of CPP macros in parser!
## Add verbatim syntax to asm-style DSLs
## Fix PATH to allow spacing characters (e.g. escaped spaces)
## Use unittest descriptions instead of comments above
## Improve table-related C% keywords
   + Change %table-maxlen to %table-length [max], allow also [min]
   + Change %table-nrow to %table-size [rows], allow also [cols]
   + Add shorthands
      + %tablen for %table-length
      + %tabsize for %table-size
      + %tabfind for %table-find
      + %tabstack for %table-stack

## %table-stack [rename] renames duplicate column names
## Fix array_type.h (setix) dependency mess...
## LZ-compress stdlib docs
## %recall [lz] should not output the braces (inspired from #embed)
## Rename named args struct wrapper, avoid name conflict with argv
   + Can be set with [argv]

## Warn about empty positional references when recalling a snippet with no formal arguments
## Fix snippet positional reference handling
## Add features to %def for contract programming
   + Allow specifying a [begin] snippet, to be printed at start of block
   + Allow %def <fname> } and specifying a [end] snippet, to be printed at end of block
   + Allow [return] so we can print return statement after [end] snippet
   + Change fdef representation from string set to vector+hash map of structs
      + Introduce fdef struct
         + Proto or typedef?
         + Name
         + Index pointer to proto or typedef
   + Pass function parameters as begin/end snippet arguments
      + Make sure we know the names of function parameters!
         + Change how C parsing works... do not flatten everything!
         + Use srt_vector instead of arr_str, finally a good excuse!
      + Do we store these inside the proto/typedef struct ? YES!

## Get rid of arr_str, in favor of vec_string
## JSON table colnames handling into functions
## Replace use of bool,ix with a setix
## Replace use of snprintf with ss_print
## Run generated C code through static analysis tools
   + Cppcheck
   + Flawfinder
   + Clang-tidy

## Reuse code across keyword rules for similar actions
   + In %enum
   + In %map and others using lambda tables

## Fix %enum logic with JSON arrobj format
## Rework JSON parsing
   + Columns should be matched after parsing, as in TSV parsing

## Do not use idcap syntax with snippet argument placeholders!
## Convert table row storage to vec_string
## Add unit tests for util_string functions
## Add unit test for strregsub
## Get rid of str, in favor of srt_string
## Guard all ss_alloc as cmod:ss_alloc with oom handler
## Better handling of internal errors (abort)
## Unify -g/--generic and -d/--disable so it can be overriden
## Get rid of arr_arr_str, make into srt_vector of arr_str
## Add unit tests for table_tsv and table_json
## Make typedefs over srt_vector for different contained types
## Add %table-stack [common] to use only common columns or numbers of rows
## Support %map lambda table to defined snippet
## Allow verbatim values as %map arguments
## Rework how named argument lists are used by %map, %table, %snippet, and %recall
   + Check while building for:
       + positional after named
       + overrides (last one wins)
   + Cleanly get what columns / formal arguments are requested and what values are set

## Allow the following syntax:
    %map <table> <snippet> (col1,col2,farg = `value`)
    where farg is a snippet formal argument with an assigned value

## Allow identifiers as default arguments (arglinks)
   + Take the value of the mentioned argument

## Rework all the farg.pos sort/unsort mess in %snippet
   + Use a hash map to avoid sorting

## Split parser/lexer state from workspace in pp
   + Reset automatically after every keyword parse

## Add CLI flag to provide docs per keyword
   + Split docs into per-keyword functions
   + Toggable examples
   + --docs-kw=<keyword>
   + --docs-kw-full=<keyword> (with examples)

## Accept lambda tables in %enum
## Improve default column handling
   + Support JSON tables
   + Support lambda tables

## Measure unicode char length with %strlen [utf8]
## Replace almost all autoarrs with srt_vectors
## Add hash map indexing of table rows with [hash] for fast data retrieval
## Compress static strings in print_docs
## Make %foreach index immutable by scope masking
## Benchmark old (sorted-array + bsearch) vs new (vector + hash map) resource storage models
## Use hash tables instead of sorted arrays for resource storage (i.e. snippets, tables, etc.)
## Add unit test capability with %unittest (Criterion backend)
## Use symbol locations for error reporting
## Allow mapping cartesian product over lambda table with [x]
    + Allow [rev] and [iord]

## Store input file path in buffer stack
## Include-if-present with %include [opt]
## Run C keywords examples output through indent
## Use %strin to simplify option list parsing, remove static data
   + Fix iftrie issue with strings like "par" and "ptr"!
   + Match options in parsed option list to their actions and variables

## Implement trie generation for static string comparison with %strin
   + ASCII strings only
   + Compare longest common prefix with strcmp or of char-by-char with [expand]
   + With [fuzzy] accept string when the only prefix matches

## Support keyword option defaults
## Support JSON tables in %enum for consistency
## Handle C-style escape sequences in %strlen [escaped]
## Subtract default arguments from 'wrong number of arguments' messages
## Fail on giving input / output arguments where expected none
## Improvements to identifier capture (idcap) notation
   + Allow disabling idcap notation
   + Remove recall constrains related to idcap notation

## Allow snippets with zero input arguments, only output arguments
## Fix having C_TYPEDEF_NAME as struct / enum name
   + Should not be any conflict, as they are separate namespaces in C

## Fix issue with ordering of formal arguments in reported snippets
## Capture output parameters from identifiers
   + id1, id2, id3 <- %| snip (`in1`, `in2`, `in3`) |%
   + equivalent to:
   + %| snip (`id1`, `id2`, `id3`) <- (`in1`, `in2`, `in3`) |%

## Use bison locations for better error reporting
   + Requires some effort in terms of parser/scanner macros and interaction
   + Space- and newline-eating scanner rules mess up line-column locations!
   + Print syntax errors with wiggly lines!
       + Scanner errors
       + Parser errors

## Allow line-by-line processing in %strsub
   + Replace AWK usage with %strsub [byline]
   + Replace [spc],[tab] options of %recall with %strsub [byline]

## %def should not need [named]
   + %proto or %typedef are already [named]

## Allow multiple values in %prefix
## Investigate scanner performance
   + Try flex options

## Pretty-print terminal docs with --color
## Support regex in everything doing strcmp under the hood
   + POSIX regex
   + in %table-find
   + in %strsub
       + iterate over matches and replace
       + arguments become: input, pattern, replacement
   + in %strcmp
       + arguments become: input, pattern
       + [regex] makes pattern a regex match
       + [match] returns matching part or empty, instead of 1 or 0

## Multi-pattern/replacement variant of %strsub
## Add some sort of multi-branch conditional
   + Variable argument version of %strcmp
       + Multiple <pattern>,<verbatim else-if> additional pairs
       + Last <varbatim else>
   + Move two-argument [match] functionality to %strstr
   + Remove [count] option from %strstr, use %strcmp for that
   + Remove verbatim if-else from %strstr, use %strcmp for that

## Unify %table-json as %table [json]
   + Parse TSV as post-processing

## Add sandboxing to %pipe
   + Using libseccomp, compile-time option
   + Minimal allowed set by default
       + Write to stdout/stderr
   + [allow=``] to allow given (resolved) syscall
   + [unsafe] allow all syscalls (no filtering)
   + [strict] block all but minimal syscalls

## Add [sep=``] to %map
   + Better handling of CSV lists!

## Suggest option names in getopt unknown option
## Default values in table columns (for columns having always the same value)
   + Similar to default arguments in snippets

## Add flag to disable shorthands
## Merge %strgsub and %strsubcat as %strsub
   + [g] to replace all occurrences
   + [cat] to append at end if not found
   + [sep=``] to set separator for [cat]

## Add if-else rule to %strstr
## Improve documentation on if-else rules
   + Clearer wording

## Suggest object names in error messages
## Suggest closest option name if unknown option passed
   + As in recent GCC!
   + Define fuzzy string search
   + Return closest match

## Improved syntax error reporting
## Split output parameters in snippets
   1. Definition: %snippet test (outarg1,outarg2) <- (inarg1,inarg2,inarg3) %{ body %}
       + Named and default arguments as well
   2. Recall: %recall test (`out1`,`out2`) <- (`in1`,`in2`,`in3`)
       + Allow multiple input / output argument lists (at least one pair)
       + Perform argument list checks of output vs output, input vs input
           + Extract functionality from cmod:snippet_subst into a function

## Allow named and default arguments in snippets
    + Implement parser terminal for named argument list
    + Switch %snippet to using it
    + Switch %recall to using it
    + Implement handling of default arguments in %snippet
    + Implement handling of named arguments in %recall

## Change %recall shorthand syntax from: %| %| to: %| |% (breaking change!!!)
## Snippet option to convert newlines to spaces
## Flag to colorize output
## Report parsed once, typedef, proto (all supported by %defined) as well
## Disable keywords at the level of the scanner
   + Uniformize keyword syntax in scanner
   + Use named variables for inactivation, instead of array

## Add compiled tests for stdlib
   + std:sort
   + std:search

## Skip snippets/tables from includes in reports
## Convert keyword rule snippets into functions
## Let cmod output parsing results, e.g. in order to describe stdlib files
   + Snippets and their arguments
   + Tables and their columns
   + Better stdlib documentation
   + Skip internal snippets (!cmod)

## Add std:sort for generating type-specific sort functions
## Allow prefixing std:logging macros
## Complete %intop implementation
   + Follow safe integer arithmetic guidelines from SEI CERT C

## Split std:getop_handle into std:getopt_define and std:getopt_parse
## Format generated `cmod` code with `indent`, using own style
## Avoid static data in std:getopt by defining option labels as snippets
## Add [bitflag] option to %enum
## Allow string identifiers as include/repeat guards
## Get rid of verbatim parentheses and use `` and %<< >>% instead
## Add facilities for string-variables
   + Allow snippets with verbatim_nul names

## Rework option matching into a binary search in a static array + function call based on type
## Implement %pipe (command) to pipe text to an external command and capture output
   + Environment variables passed with env = %[ VAR = val %] option
   + Print output line by line (as in %recall)
   + Support str and supnl options (as in %recall)
   + Requires serious fork/pipe/socket stuff!

## Rework cmod_option into a tagged union supporting string values too
## Add support for including JSON files (%table-json "<path>")
   + single object containing objects, define a table with property name for each of them
   + support both JSON table formats

## Use %defined [type] to avoid redefinition in std:retval
## Change HERESTR delimiter to avoid conflict with digraph
## Change %table-json start condition to HERESTR with exit_on MOD_BRACE_CLOSE
## Change option isort -> iord in %map
## Add %map [uniq=<col>] to filter unique rows by given column
   + Filter unique, then sort, if both specified

## Allow sorting by numeric value in %map
## Allow setting column to sort with in %map [sort=<col>]
## Add new kind of option (index) with action ${var} = ${val}
## Use forward struct declarations to avoid using void pointers in C parsing structs
## Use namix instead of struct p_ix
## Fix %table-json grammar from lambda -> mod_verbatim_nul, to avoid allowing DLLARG
## Verbatim text replacement in snippets
## Named arguments in snippets
## Better error reporting
## Substitute snippet arguments in string
## Escape $ as $$ and % as %% for nested snippets
## Reentrant parser/lexer
## Recursion in snippets -> Maybe implemented as successive parses of the whole file...
## Macro iteration
## Include files!
## Add option to not overwrite existing output file
## Better output file creation, e.g. with open(2) using O_CREAT | O_EXCL, etc.
## Use sorted (bsearch-able) arrays in pp for generated resources (snippets, tables, etc.)
## Better list implementation, backed by an array
## Provide generic way of giving options to mod statements (i.e. %map-sort -> %map [sorted])
## Better auto-growing string, without static variables
## Add more sorting options, e.g. reverse
## Fix line number count after %include
## Make \_NR the only reserved column, cleanup related code
## Remove reserved columns, compute \_NR on-the-fly
## Make %include into a parsing rule
## Make array types into std:autoarr types
## Full parser meta-programming
## Add keyword grammar rules to documentation
## Check error line number glitch with comments
## Implement struct literal %switch case
## Support %nul as special verbatim value meaning empty string everywhere
## Finish implementing C declarator parsing and update %proto and %typedef
## Fix order of autoarr %typedefs to avoid delaying protos
## Change %defined-\* keywords to %defined [\*], add types and enums
## Add flag to not pre-load standard C types
## Get rid of CPP usage to provide functionality
   + Store decl_spec and declarator of %typedef and %proto, and print it instead of using macros
   + Store both in a unified structure struct decl replacing struct proto

## Fix CCODE exit issues (e.g. allow semicolon-newline in %proto and %typedef, etc.)
   + Would be ideal if parser could set lexer start condition on successful parse
   + Solved by exiting only with semicolons outside braces

## Add [opt] to %proto to ignore already-defined prototypes
## Embed a license-compatible C json parser (jsmn looks promising)
## Add support for JSON table formats (%table-json)
   + as array of arrays (implicit columns)
   + as array of objects with named columns (explicit columns)

## Use a better jsmn version (dominickpastore:strict-parsing)
## Check and fix behavior of true, false, null in JSON tables
## Improve option list parsing
   + always in CMOD start condition
   + allow setting values for count options (opt = uint)
   + semantic value is a nameix { char \*name; size_t ix; }, with ix = 1 (true) for boolean
