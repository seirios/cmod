%include "cmod/search.hm"

%include "cmod.ext_tab.hm"
%include "table_parse.hm"

%include [c,sys] "assert.h"
%include [c,sys] "regex.h"
%include [c] "table_tsv.h"
%include [c] "table_json.h"
%include [c] "util_string.h"

%def table_lambda_parse {
    int rc = TABLE_PARSE_OK;

    size_t ncol = sv_len(*lambda_table);
    const vec_string *firstcol = sv_at_ptr(*lambda_table,0);
    size_t nrow = sv_len(firstcol);

    vec_vec_string *svv_rows <- %| cmod:sv_alloc_ptr (nrow,oom=`rc = TABLE_PARSE_OOM; goto cleanup;`) |%

    for(size_t i = 0; i < nrow; ++i) { // init rows
        vec_string *row <- %| cmod:sv_alloc_ptr (ncol,oom=`rc = TABLE_PARSE_OOM; goto cleanup;`) |%;
        svv_rows <- %| cmod:sv_push (row,oom=`rc = TABLE_PARSE_OOM; goto cleanup;`) |%
    }

    for(size_t j = 0; j < ncol; ++j) { // fill entries
        vec_string **row_lambda = (vec_string**)sv_at(*lambda_table,j);
        for(size_t i = 0; i < nrow; ++i) {
            vec_string **row = (vec_string**)sv_at(svv_rows,i); // DO NOT remove a star
            sv_set(row,j,sv_at(*row_lambda,i)); // transposed
        }
        sv_free(row_lambda);
    }
    sv_free(lambda_table);

    *ncol_o = ncol;
    *rows_o = svv_rows;
    svv_rows = NULL; // hand-over
cleanup:
    sv_free(&svv_rows);
    return rc;
}

%def table_firstrow_colnames {
    int rc = 0; // return code

    vec_string *firstrow = sv_at_ptr(*svv_rows,0);
    vec_namarg *sv_colnarg = NULL;
    sv_colnarg <- %| cmod:sv_alloc (`struct namarg`,`sv_len(firstrow)`,oom=`rc = 1; goto cleanup;`) |%
    %foreach [svec=`char*`] item (firstrow) %{
        if(!strisidext($p{item})) { rc = 4; goto cleanup; } // ERROR: not an extended identifier
        struct namarg colnarg = CMOD_ARG_NOVALUE_xset(.name= $p{item});
        sv_colnarg <- %| cmod:sv_push (colnarg,oom=`rc = 1; goto cleanup;`) |%
        $pP{item} = NULL; // hand-over
    %}
    sv_free(&firstrow); // clear
    /* remove first row from vector */
    vec_vec_string *svv_rows_new = NULL;
    svv_rows_new <- %| cmod:sv_alloc_ptr (`sv_len(*svv_rows) - 1`,oom=`rc = 1; goto cleanup;`) |%
    for(size_t i = 1; i < sv_len(*svv_rows); ++i) {
        const vec_string *row = sv_at_ptr(*svv_rows,i);
        svv_rows_new <- %| cmod:sv_push (row,oom=`rc = 1; goto cleanup;`) |% // hand-over
    }
    sv_free(svv_rows);

    *svv_rows = svv_rows_new;
    *colnarg_o = sv_colnarg;
    *nnondef_o = sv_len(sv_colnarg);
    sv_colnarg = NULL; // hand-over
cleanup:
    %recall free_vec_namargp (sv_colnarg)

    return rc;
}

%def table_build {
    const size_t ncolnam = sv_len(sv_colnarg);
    assert(ncolnam > 0); // we assume at least one column
    tab->sv_colnarg = sv_colnarg;

    size_t ndefault = 0; // number of columns with default value
    /* count default columns */
    %foreach [svec=`struct namarg`] colnarg (sv_colnarg) %{
        char *defval = NULL;
        switch(${colnarg}->type) {
            case CMOD_ARG_NOVALUE: break; // skip non-default column
        %map [notnull = #column_get] cmod_arg_type:valid %%{
            case ENUM_NAMARG($${type}): /* FALLTHROUGH */
                defval = CMOD_ARG_$${type}_get($p{colnarg});
                $p{colnarg} = CMOD_ARG_VERBATIM_xset(defval,.name= ${colnarg}->name); // change type
                ++ndefault;
                break;
        %%}
            default: // already checked in parser
                %recall cmod:internal_error_abort
                break;
        }
    %}
    tab->nnondef = ncolnam - ndefault; // number of non-default columns

    int ret = TABLE_PARSE_OK;
    size_t tab_ncol = 0;
    switch(format) {
        case TABLE_FMT_TSV:
        if(NULL != tbody.verbatim) {
            const char *tbody_buf = ss_get_buffer_r(tbody.verbatim);
            const size_t tbody_len = ss_len(tbody.verbatim);
            enum tsv_parse _ret = table_tsv_parse(tbody_buf,tbody_len,&tab_ncol,&tab->svv_rows);
            switch(_ret) { // map return codes
            %map table_parse:ret %{
                case TSV_PARSE_${name}: ret = TABLE_PARSE_${name}; break;
            %}
                default: ret = _ret; break;
            }
            if(TABLE_PARSE_OK != ret) return ret;
            if(sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
                *ierr = tab_ncol; return TABLE_PARSE_NDEFAULT;
            }
        }
            break;
        case TABLE_FMT_JSON:
        if(NULL != tbody.verbatim) {
            vec_namarg *json_colnam = NULL;
            const char *tbody_buf = ss_get_buffer_r(tbody.verbatim);
            const size_t tbody_len = ss_len(tbody.verbatim);
            enum json_parse _ret = table_json_parse(tbody_buf,tbody_len,&tab_ncol,&tab->svv_rows,&json_colnam,ierr);
            switch(_ret) { // map return codes
            %map table_parse:ret %{
                case JSON_PARSE_${name}: ret = TABLE_PARSE_${name}; break;
            %}
                default: ret = _ret; break;
            }
            if(TABLE_PARSE_OK != ret) return ret;
            if(sv_len(json_colnam) > 0) { // check column names extracted from JSON
                enum json_parse _ret = table_json_colnam_preset(tab->svv_rows,json_colnam,tab->nnondef,sv_colnarg,ierr);
                switch(_ret) { // map return codes
                %map table_parse:ret %{
                    case JSON_PARSE_${name}: ret = TABLE_PARSE_${name}; break;
                %}
                    default: ret = _ret; break;
                }
                if(TABLE_PARSE_OK != ret) return ret;
                %recall free_vec_namargp (json_colnam)
            }
            if(sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
                *ierr = tab_ncol; return TABLE_PARSE_NDEFAULT;
            }
        }
            break;
        case TABLE_FMT_LAMBDA:
            ret = table_lambda_parse(tbody.lambda,&tab_ncol,&tab->svv_rows);
            if(TABLE_PARSE_OK != ret) return ret;
            if(sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
                *ierr = tab_ncol; return TABLE_PARSE_NDEFAULT;
            }
            break;
    }
    sv_shrink(&tab->svv_rows); // free unused space

    return ret;
}
