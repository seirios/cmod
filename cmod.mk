CMOD ?= cmod
CMODFLAGS ?=

# C% generic rules for C
%.c: %.cm
	$(CMOD) $(CMODFLAGS) -o $@ $<

%.h: %.hm
	$(CMOD) $(CMODFLAGS) -o $@ $<

# C% generic rules for Flex/Bison
%.l: %.lm
	$(CMOD) $(CMODFLAGS) -o $@ $<

%.y: %.ym
	$(CMOD) $(CMODFLAGS) -o $@ $<
