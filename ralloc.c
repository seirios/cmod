#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#ifndef CMOD_RALLOC_H
#define CMOD_RALLOC_H
struct cmod_ralloc_params {
    volatile sig_atomic_t giveup;
    bool verbose;
    int ntimes;
    int msleep;
};

extern struct cmod_ralloc_params ralloc_glob;
__attribute__((malloc, warn_unused_result))
void *(rmalloc) (size_t size);
__attribute__((malloc, warn_unused_result))
void *(rcalloc) (size_t nmemb, size_t size);
__attribute__((warn_unused_result))
void *(rrealloc) (void *ptr, size_t size);
__attribute__((malloc, nonnull((1))))
char *(rstrdup) (const char *s);
extern const struct ret_f0 rmalloc_ret_f;
extern const struct ret_f0 rcalloc_ret_f;
extern const struct ret_f0 rrealloc_ret_f;
extern const struct ret_f0 rstrdup_ret_f;
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "cmod.ext.h"

void check_ret_t(ret_t rc, const struct ret_f *f)
{
    uint8_t n = 0;
    int saverrno = errno;
    while (rc != 0 && n < sizeof(ret_t)) {      // unwind error stack
        int8_t tmp = rc & 0xffUL;       // mask last byte
        if (tmp > 0) {  // blame
            verbose("%*s%s: error in called function `%s`", 2 * (int)n, "", f->name,
                    f->p[tmp]->name);
            rc >>= 8;   // shift last byte
            f = f->p[tmp];      // switch to struct of called function
            ++n;
        }
        else {  // actual error
            verbose("%*s%s: %s (%d)", 2 * (int)n, "", f->name,
                    (tmp == RET_ERR_ERRNO) ? strerror(saverrno) : ret_t_err_strenum(tmp),
                    (tmp == RET_ERR_ERRNO) ? saverrno : tmp);
            rc = 0;     // finish processing
        }
    }
    if (n == sizeof(ret_t))
        verbose("%*s%s: error lost in stack too deep", 2 * (int)n, "", f->name);
}

#ifndef verbose
#define verbose(M, ...) fprintf(stderr,"%s: " M "\n",__func__, ##__VA_ARGS__);
#endif
#ifndef verbose0
#define verbose0(M) fprintf(stderr,"%s: " M "\n",__func__);
#endif

void *rmalloc(size_t size)
{
    void *tmp = NULL;
    int msleep = ralloc_glob.msleep;

    int k = 0;
    do {
        errno = 0;
        if (NULL != (tmp = malloc(size)))
            break;      // OK
        if (errno == ENOMEM && !ralloc_glob.giveup && k < ralloc_glob.ntimes) { // out of memory
            if (ralloc_glob.verbose)
                verbose("out of memory, retry %d", k);

            struct timespec tm;
            tm.tv_nsec = 1000000 * (msleep % 1000);
            tm.tv_sec = msleep / 1000;
            errno = 0;
            if (!ralloc_glob.giveup && nanosleep(&tm, NULL) == -1 && ralloc_glob.verbose)
                verbose("nanosleep: %s (%d)", strerror(errno), errno);

            msleep *= 2;        // double sleep on each retry
        }
        else if (ralloc_glob.verbose && errno == 0) {
            verbose0("zero-size allocation");
            break;
        }
        else if (ralloc_glob.verbose) {
            verbose0("unknown failure");
            break;
        }
    } while (NULL == tmp && !ralloc_glob.giveup && ++k < ralloc_glob.ntimes);

    ralloc_glob.giveup = 0;

    return tmp;
}

void *rcalloc(size_t nmemb, size_t size)
{
    void *tmp = NULL;
    int msleep = ralloc_glob.msleep;

    int k = 0;
    do {
        errno = 0;
        if (NULL != (tmp = calloc(nmemb, size)))
            break;      // OK
        if (errno == ENOMEM && !ralloc_glob.giveup && k < ralloc_glob.ntimes) { // out of memory
            if (ralloc_glob.verbose)
                verbose("out of memory, retry %d", k);

            struct timespec tm;
            tm.tv_nsec = 1000000 * (msleep % 1000);
            tm.tv_sec = msleep / 1000;
            errno = 0;
            if (!ralloc_glob.giveup && nanosleep(&tm, NULL) == -1 && ralloc_glob.verbose)
                verbose("nanosleep: %s (%d)", strerror(errno), errno);

            msleep *= 2;        // double sleep on each retry
        }
        else if (ralloc_glob.verbose && errno == 0) {
            verbose0("zero-size allocation");
            break;
        }
        else if (ralloc_glob.verbose) {
            verbose0("unknown failure");
            break;
        }
    } while (NULL == tmp && !ralloc_glob.giveup && ++k < ralloc_glob.ntimes);

    ralloc_glob.giveup = 0;

    return tmp;
}

void *rrealloc(void *ptr, size_t size)
{
    void *tmp = NULL;
    int msleep = ralloc_glob.msleep;

    int k = 0;
    do {
        errno = 0;
        if (NULL != (tmp = realloc(ptr, size)))
            break;      // OK
        if (errno == ENOMEM && !ralloc_glob.giveup && k < ralloc_glob.ntimes) { // out of memory
            if (ralloc_glob.verbose)
                verbose("out of memory, retry %d", k);

            struct timespec tm;
            tm.tv_nsec = 1000000 * (msleep % 1000);
            tm.tv_sec = msleep / 1000;
            errno = 0;
            if (!ralloc_glob.giveup && nanosleep(&tm, NULL) == -1 && ralloc_glob.verbose)
                verbose("nanosleep: %s (%d)", strerror(errno), errno);

            msleep *= 2;        // double sleep on each retry
        }
        else if (ralloc_glob.verbose && errno == 0) {
            verbose0("zero-size allocation");
            break;
        }
        else if (ralloc_glob.verbose) {
            verbose0("unknown failure");
            break;
        }
    } while (NULL == tmp && !ralloc_glob.giveup && ++k < ralloc_glob.ntimes);

    ralloc_glob.giveup = 0;

    return tmp;
}

char *rstrdup(const char *s)
{
    void *tmp = NULL;
    int msleep = ralloc_glob.msleep;

    int k = 0;
    do {
        errno = 0;
        if (NULL != (tmp = strdup(s)))
            break;      // OK
        if (errno == ENOMEM && !ralloc_glob.giveup && k < ralloc_glob.ntimes) { // out of memory
            if (ralloc_glob.verbose)
                verbose("out of memory, retry %d", k);

            struct timespec tm;
            tm.tv_nsec = 1000000 * (msleep % 1000);
            tm.tv_sec = msleep / 1000;
            errno = 0;
            if (!ralloc_glob.giveup && nanosleep(&tm, NULL) == -1 && ralloc_glob.verbose)
                verbose("nanosleep: %s (%d)", strerror(errno), errno);

            msleep *= 2;        // double sleep on each retry
        }
        else if (ralloc_glob.verbose && errno == 0) {
            verbose0("zero-size allocation");
            break;
        }
        else if (ralloc_glob.verbose) {
            verbose0("unknown failure");
            break;
        }
    } while (NULL == tmp && !ralloc_glob.giveup && ++k < ralloc_glob.ntimes);

    ralloc_glob.giveup = 0;

    return tmp;
}

const struct ret_f0 rmalloc_ret_f = {
    .name = "rmalloc"
};

const struct ret_f0 rcalloc_ret_f = {
    .name = "rcalloc"
};

const struct ret_f0 rrealloc_ret_f = {
    .name = "rrealloc"
};

const struct ret_f0 rstrdup_ret_f = {
    .name = "rstrdup"
};
