
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <stdbool.h>
#ifndef CMOD_DSL_EXT_H
#define CMOD_DSL_EXT_H
#include "parts/struct_dsl_param.h"
#include "parts/variant_dsl_ast.h"

int (cmod_parse_dsl) (size_t code_len, const char code[static code_len], const struct dsl * lang,
                      bool pretty, int silent, bool verbose, srt_vector * astlist_o[static 1]);
int (dsl_ast_new_call1) (uint64_t kw_ix, const struct dsl_ast * first,
                         struct dsl_ast out[static 1]);
int (dsl_ast_new_call2) (uint64_t kw_ix, const struct dsl_ast * first,
                         const struct dsl_ast * second, struct dsl_ast out[static 1]);
int (dsl_ast_to_list) (const struct dsl_ast * node, srt_vector * out[static 1]);
#define dsl_ast_eval(a,b,c,d) dsl_ast_eval_aux(0,a,b,c,d)
int (dsl_ast_eval_aux) (size_t depth, struct dsl_ast * node, const struct dsl * lang,
                        size_t ierr[static 1], size_t errix[static 1]);
#endif

#include <assert.h>

#include "dsl_parser.h"
#define YY_HEADER_EXPORT_START_CONDITIONS
#include "dsl_scanner.h"

#include "ralloc.h"
#include "util_snippet.h"

void dsl_ast_free(struct dsl_ast *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_DSL_AST(CONST):
        free(x->value.txt);
        x->value.txt = NULL;
        break;
    case ENUM_DSL_AST(CALL):
        x->value.ix = 0;
        break;
    }

    {
        for (size_t i = 0; i < sv_len(x->child); ++i) {
            const void *_item = sv_at(x->child, i);
            dsl_ast_free((struct dsl_ast *)_item);
        }
        sv_free(&(x->child));
    }

    *x = (struct dsl_ast) { 0 };
}

int dsl_ast_dup(const struct dsl_ast *x, struct dsl_ast *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_DSL_AST(CONST):
        y->value.txt = (NULL != (x->value.txt)) ? rstrdup(x->value.txt) : NULL;
        if (NULL != (x->value.txt) && NULL == (y->value.txt)) {
            return 2;
        }

        break;
    case ENUM_DSL_AST(CALL):
        y->value.ix = x->value.ix;
        break;
    }

    {
        y->child = sv_dup(x->child);    // duplicate buffer
        if (sv_void == y->child && NULL != x->child) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->child); ++i) {
            struct dsl_ast *_item = sv_elem_addr(y->child, i);
            const struct dsl_ast _item_old = *_item;
            if (dsl_ast_dup(&(_item_old), &(*_item))) {
                return 2;
            }
        }
    }

    return 0;
}

int dsl_ast_new_call1(uint64_t kw_ix, const struct dsl_ast *first, struct dsl_ast out[static 1])
{
    int rc = 0;
    struct dsl_ast call = DSL_AST_NODE_CALL_set(kw_ix);
    (call.child) = sv_alloc(sizeof(struct dsl_ast), 1, NULL);
    if (sv_void == (call.child)) {
        rc = 1;
        goto cleanup;
    }
    if (!sv_push(&(call.child), &(*first))) {
        rc = 1;
        goto cleanup;
    }

    *out = call;
    call = (struct dsl_ast) { 0 };      // hand-over
 cleanup:
    dsl_ast_free(&call);
    return rc;
}

int dsl_ast_new_call2(uint64_t kw_ix, const struct dsl_ast *first, const struct dsl_ast *second,
                      struct dsl_ast out[static 1])
{
    int rc = 0;
    struct dsl_ast call = DSL_AST_NODE_CALL_set(kw_ix);
    (call.child) = sv_alloc(sizeof(struct dsl_ast), 2, NULL);
    if (sv_void == (call.child)) {
        rc = 1;
        goto cleanup;
    }
    if (!sv_push(&(call.child), &(*first))) {
        rc = 1;
        goto cleanup;
    }
    if (!sv_push(&(call.child), &(*second))) {
        rc = 1;
        goto cleanup;
    }

    *out = call;
    call = (struct dsl_ast) { 0 };      // hand-over
 cleanup:
    dsl_ast_free(&call);
    return rc;
}

int dsl_ast_to_list(const struct dsl_ast *node, srt_vector *out[static 1])
{
    int rc = 0;
    srt_vector *list = NULL;
    (list) = sv_alloc(sizeof(struct dsl_ast), 1, NULL);
    if (sv_void == (list)) {
        rc = 1;
        goto cleanup;
    }

    if (!sv_push(&(list), &(*node))) {
        rc = 1;
        goto cleanup;
    }

    *out = list;
    list = NULL;        // hand-over
 cleanup:
    sv_free(&list);
    return rc;
}

int dsl_ast_eval_aux(size_t depth, struct dsl_ast *node, const struct dsl *lang,
                     size_t ierr[static 1], size_t errix[static 1])
{
    int rc = 0;
    srt_string *ss = NULL;

    if (depth > lang->max_depth) {
        rc = 3;
        goto cleanup;
    }

    if (NULL == node)
        goto cleanup;   // do nothing
    switch (node->type) {
    case DSL_AST_NODE_CONST:
        break;  // nothing to do
    case DSL_AST_NODE_CALL:
        {
            const uint64_t ix = node->value.ix;
            const struct snippet *snip = sv_at_ptr(lang->kw_snip, ix);

            /* check snippet signature */
            size_t nargs = sv_len(node->child);
            if (SNIPPET_HAS_FORMAL_ARGS(snip) &&
                (nargs < SNIPPET_NARGIN_NODEF(snip) || nargs > snip->nargs)) {
                *errix = ix;    // blame
                *ierr = nargs;
                rc = 2;
                goto cleanup;
            }

            /* evaluate arguments (recurse) */
            for (size_t i = 0; i < nargs; ++i) {
                struct dsl_ast *chld = (struct dsl_ast *)sv_at(node->child, i);
                if ((rc = dsl_ast_eval_aux(depth + 1, chld, lang, ierr, errix)))
                    goto cleanup;
            }

            /* setup arguments */
            vec_namarg *sv_inpargs = NULL;
            (sv_inpargs) = sv_alloc(sizeof(struct namarg), nargs, NULL);
            if (sv_void == (sv_inpargs)) {
                rc = 1;
                goto cleanup;
            }

            for (size_t i = 0; i < nargs; ++i) {
                const struct dsl_ast *chld = sv_at(node->child, i);
                assert(chld->type == DSL_AST_NODE_CONST);
                struct namarg item = CMOD_ARG_VERBATIM_set(chld->value.txt);    // borrow
                if (!sv_push(&(sv_inpargs), &(item))) {
                    rc = 1;
                    goto cleanup;
                }

            }

            /* recall snippet */
            size_t buflen = 0;
            char *buf = NULL;
            if (snippet_subst(snip, sv_inpargs, &buf, &buflen) || NULL == buf) {
                rc = 1;
                goto cleanup;
            }
            sv_free(&sv_inpargs);       // unborrow arguments

            dsl_ast_free(node);
            *node = DSL_AST_NODE_CONST_set(buf);        // change node type
        }
        break;
    }

 cleanup:
    ss_free(&ss);
    return rc;
}

int cmod_parse_dsl(size_t code_len, const char code[static code_len], const struct dsl *lang,
                   bool pretty, int silent, bool verbose, srt_vector *astlist_o[static 1])
{
    int rc = 0;

    yyscan_t dsl_scanner = (yyscan_t) 0;
    YY_BUFFER_STATE buf = (YY_BUFFER_STATE) 0;
    struct dsl_param dsl_pp = {
        .lang = lang,
        .silent = silent,
        .pretty = pretty,
        .verbose = verbose
    };

    /* choose initial condition from DSL syntax type */
    switch (lang->type) {
    case DSL_TYPE_asm:
        dsl_pp.init = ASM;
        break;
    case DSL_TYPE_sexpr:
        dsl_pp.init = SEXPR;
        break;
    case DSL_TYPE_cexpr:
        dsl_pp.init = CEXPR;
        break;
    case DSL_TYPE_custom:
        dsl_pp.init = CUSTOM;
        break;
    case DSL_TYPE_tree:
        dsl_pp.init = TREE;
        break;
    default:
        rc = 1; // ERROR: invalid DSL type
        goto cleanup;
        break;
    }

    errno = 0;
    if (dsl_lex_init_extra(&dsl_pp, &dsl_scanner)) {
        (void)errno;
        rc = 2; // ERROR: failed init
        goto cleanup;
    }
    buf = dsl__scan_bytes(code, code_len, dsl_scanner);

    int ret;
    dsl_pp.in_parse = true;
    if ((ret = dsl_parse(dsl_scanner, &dsl_pp))) {
        rc = 2 + ret;   // ERROR: failed parse
        goto cleanup;
    }
    dsl_pp.in_parse = false;

    *astlist_o = dsl_pp.astlist;
    dsl_pp.astlist = NULL;      // hand-over
 cleanup:
    {
        for (size_t i = 0; i < sv_len(dsl_pp.astlist); ++i) {
            const void *_item = sv_at(dsl_pp.astlist, i);
            dsl_ast_free((struct dsl_ast *)_item);
        }
        sv_free(&(dsl_pp.astlist));
    }
    dsl__delete_buffer(buf, dsl_scanner);
    dsl_lex_destroy(dsl_scanner);
    ss_free(&dsl_pp.line);

    return rc;
}
