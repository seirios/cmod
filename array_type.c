#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include "ralloc.h"
#include "array_type.h"

#include "parts/variant_c_specifier.h"

void namarg_free(struct namarg *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_NAMARG(VERBATIM):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_NAMARG(ID):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_NAMARG(IDEXT):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_NAMARG(INTSTR):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_NAMARG(SPECIAL):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_NAMARG(ARGLINK):
        x->value.Ref = 0;
        break;
    case ENUM_NAMARG(EXPR):
        {
            for (size_t i = 0; i < sv_len(x->value.Expr); ++i) {
                const void *_item = sv_at(x->value.Expr, i);
                namarg_free((struct namarg *)_item);
            }
            sv_free(&(x->value.Expr));
        }

        break;
    }

    free(x->name);
    x->name = NULL;

    *x = (struct namarg) { 0 };
}

int namarg_dup(const struct namarg *x, struct namarg *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_NAMARG(VERBATIM):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_NAMARG(ID):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_NAMARG(IDEXT):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_NAMARG(INTSTR):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_NAMARG(SPECIAL):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_NAMARG(ARGLINK):
        y->value.Ref = x->value.Ref;
        break;
    case ENUM_NAMARG(EXPR):
        {
            y->value.Expr = sv_dup(x->value.Expr);      // duplicate buffer
            if (sv_void == y->value.Expr && NULL != x->value.Expr) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.Expr); ++i) {
                struct namarg *_item = sv_elem_addr(y->value.Expr, i);
                const struct namarg _item_old = *_item;
                if (namarg_dup(&(_item_old), &(*_item))) {
                    return 2;
                }
            }
        }

        break;
    }

    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    return 0;
}

void dllarg_clear(struct dllarg *x)
{
    x->type = 0;
    free(x->name);
    x->name = NULL;
    x->pos = 0;
    x->num = 0;
    x->rep = 0;
    x->mod = (struct str_mod) { 0 };
    *x = (struct dllarg) { 0 };
}

int dllarg_dup(const struct dllarg *x, struct dllarg *y)
{
    if (NULL == y)
        return 1;
    y->type = x->type;
    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    y->pos = x->pos;
    y->num = x->num;
    y->rep = x->rep;
    y->mod = x->mod;
    return 0;
}

void snippet_clear(struct snippet *x)
{
    free(x->name);
    x->name = NULL;
    ss_free(&(x->body)); {
        for (size_t i = 0; i < sv_len(x->sv_forl); ++i) {
            const void *_item = sv_at(x->sv_forl, i);
            namarg_free((struct namarg *)_item);
        }
        sv_free(&(x->sv_forl));
    }
    shm_free(&(x->shm_forl)); {
        for (size_t i = 0; i < sv_len(x->sv_argl); ++i) {
            const void *_item = sv_at(x->sv_argl, i);
            dllarg_clear((struct dllarg *)(_item));
        }
        sv_free(&(x->sv_argl));
    }
    sms_free(&(x->sms_dllarg));
    x->nargs = 0;
    x->nout = 0;
    x->ndefin = 0;
    x->ndefout = 0;
    x->redef = false;
    *x = (struct snippet) { 0 };
}

int snippet_dup(const struct snippet *x, struct snippet *y)
{
    if (NULL == y)
        return 1;
    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    y->body = ss_dup(x->body);
    if (ss_void == y->body && NULL != x->body) {
        return 2;
    }
    {
        y->sv_forl = sv_dup(x->sv_forl);        // duplicate buffer
        if (sv_void == y->sv_forl && NULL != x->sv_forl) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_forl); ++i) {
            struct namarg *_item = sv_elem_addr(y->sv_forl, i);
            const struct namarg _item_old = *_item;
            if (namarg_dup(&(_item_old), &(*_item))) {
                return 2;
            }
        }
    }
    if (NULL != (x->shm_forl) && NULL == (y->shm_forl = shm_dup(x->shm_forl))) {
        return 2;
    }
    {
        y->sv_argl = sv_dup(x->sv_argl);        // duplicate buffer
        if (sv_void == y->sv_argl && NULL != x->sv_argl) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_argl); ++i) {
            struct dllarg *_item = sv_elem_addr(y->sv_argl, i);
            const struct dllarg _item_old = *_item;
            if (dllarg_dup(&(_item_old), &(*_item))) {
                return 2;
            }
        }
    }
    if (NULL != (x->sms_dllarg) && NULL == (y->sms_dllarg = sms_dup(x->sms_dllarg))) {
        return 2;
    }
    y->nargs = x->nargs;
    y->nout = x->nout;
    y->ndefin = x->ndefin;
    y->ndefout = x->ndefout;
    y->redef = x->redef;
    return 0;
}

void table_clear(struct table *x)
{
    free(x->name);
    x->name = NULL;
    shm_free(&(x->hmap)); {
        for (size_t i = 0; i < sv_len(x->sv_colnarg); ++i) {
            const void *_item = sv_at(x->sv_colnarg, i);
            namarg_free((struct namarg *)_item);
        }
        sv_free(&(x->sv_colnarg));
    }
    shm_free(&(x->shm_colnam)); {
        for (size_t i = 0; i < sv_len(x->svv_rows); ++i) {
            const void *_item = sv_at(x->svv_rows, i);
            {
                vec_string *vec = *(vec_string **) (_item);
                {
                    for (size_t i = 0; i < sv_len(vec); ++i) {
                        const void *_item = sv_at(vec, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(vec));
                }
            }
        }
        sv_free(&(x->svv_rows));
    }
    x->key = (struct setix) { 0 };
    x->nnondef = 0;
    x->redef = false;
    *x = (struct table) { 0 };
}

int table_dup(const struct table *x, struct table *y)
{
    if (NULL == y)
        return 1;
    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    if (NULL != (x->hmap) && NULL == (y->hmap = shm_dup(x->hmap))) {
        return 2;
    }
    {
        y->sv_colnarg = sv_dup(x->sv_colnarg);  // duplicate buffer
        if (sv_void == y->sv_colnarg && NULL != x->sv_colnarg) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_colnarg); ++i) {
            struct namarg *_item = sv_elem_addr(y->sv_colnarg, i);
            const struct namarg _item_old = *_item;
            if (namarg_dup(&(_item_old), &(*_item))) {
                return 2;
            }
        }
    }
    if (NULL != (x->shm_colnam) && NULL == (y->shm_colnam = shm_dup(x->shm_colnam))) {
        return 2;
    }
    {
        y->svv_rows = sv_dup(x->svv_rows);      // duplicate buffer
        if (sv_void == y->svv_rows && NULL != x->svv_rows) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->svv_rows); ++i) {
            vec_string **_item2 = sv_elem_addr(y->svv_rows, i);
            const vec_string *_item2_old = *_item2;
            {
                *_item2 = sv_dup(_item2_old);   // duplicate buffer
                if (sv_void == *_item2 && NULL != _item2_old) {
                    return 2;
                }
                for (size_t i = 0; i < sv_len(*_item2); ++i) {
                    char **_item = sv_elem_addr(*_item2, i);
                    const char *_item_old = *_item;
                    *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                    if (NULL != (_item_old) && NULL == (*_item)) {
                        return 2;
                    }

                }
            }
        }
    }
    y->key = x->key;
    y->nnondef = x->nnondef;
    y->redef = x->redef;
    return 0;
}

void decl_clear(struct decl *x)
{
    x->type = NULL;
    x->name = NULL;
    free(x->init);
    x->init = NULL; {
        for (size_t i = 0; i < sv_len(x->pointer); ++i) {
            const void *_item = sv_at(x->pointer, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->pointer));
    }
    ss_free(&(x->wrapper));
    ss_free(&(x->fsuffix)); {
        for (size_t i = 0; i < sv_len(x->sv_begin); ++i) {
            const void *_item = sv_at(x->sv_begin, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_begin));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_end); ++i) {
            const void *_item = sv_at(x->sv_end, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_end));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_spec); ++i) {
            const void *_item = sv_at(x->sv_spec, i);
            c_specifier_free((struct c_specifier *)_item);
        }
        sv_free(&(x->sv_spec));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_decl); ++i) {
            const void *_item = sv_at(x->sv_decl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_decl));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_fargs); ++i) {
            const void *_item = sv_at(x->sv_fargs, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_fargs));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_fargs_type); ++i) {
            const void *_item = sv_at(x->sv_fargs_type, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_fargs_type));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_fargs_init); ++i) {
            const void *_item = sv_at(x->sv_fargs_init, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->sv_fargs_init));
    }
    {
        for (size_t i = 0; i < sv_len(x->sv_fargs_ptrs); ++i) {
            const void *_item = sv_at(x->sv_fargs_ptrs, i);
            {
                vec_string *vec = *(vec_string **) (_item);
                {
                    for (size_t i = 0; i < sv_len(vec); ++i) {
                        const void *_item = sv_at(vec, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(vec));
                }
            }
        }
        sv_free(&(x->sv_fargs_ptrs));
    }
    x->iname = (struct setix) { 0 };
    x->fargs_i0 = (struct setix) { 0 };
    x->fargs_i1 = (struct setix) { 0 };
    x->ndefault = 0;
    x->named = false;
    *x = (struct decl) { 0 };
}

int decl_dup(const struct decl *x, struct decl *y)
{
    if (NULL == y)
        return 1;
    y->type = x->type;
    y->name = x->name;
    y->init = (NULL != (x->init)) ? rstrdup(x->init) : NULL;
    if (NULL != (x->init) && NULL == (y->init)) {
        return 2;
    }

    {
        y->pointer = sv_dup(x->pointer);        // duplicate buffer
        if (sv_void == y->pointer && NULL != x->pointer) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->pointer); ++i) {
            char **_item = sv_elem_addr(y->pointer, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    y->wrapper = ss_dup(x->wrapper);
    if (ss_void == y->wrapper && NULL != x->wrapper) {
        return 2;
    }
    y->fsuffix = ss_dup(x->fsuffix);
    if (ss_void == y->fsuffix && NULL != x->fsuffix) {
        return 2;
    }
    {
        y->sv_begin = sv_dup(x->sv_begin);      // duplicate buffer
        if (sv_void == y->sv_begin && NULL != x->sv_begin) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_begin); ++i) {
            char **_item = sv_elem_addr(y->sv_begin, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_end = sv_dup(x->sv_end);  // duplicate buffer
        if (sv_void == y->sv_end && NULL != x->sv_end) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_end); ++i) {
            char **_item = sv_elem_addr(y->sv_end, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_spec = sv_dup(x->sv_spec);        // duplicate buffer
        if (sv_void == y->sv_spec && NULL != x->sv_spec) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_spec); ++i) {
            struct c_specifier *_item = sv_elem_addr(y->sv_spec, i);
            const struct c_specifier _item_old = *_item;
            if (c_specifier_dup(&(_item_old), &(*_item))) {
                return 2;
            }
        }
    }
    {
        y->sv_decl = sv_dup(x->sv_decl);        // duplicate buffer
        if (sv_void == y->sv_decl && NULL != x->sv_decl) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_decl); ++i) {
            char **_item = sv_elem_addr(y->sv_decl, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_fargs = sv_dup(x->sv_fargs);      // duplicate buffer
        if (sv_void == y->sv_fargs && NULL != x->sv_fargs) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_fargs); ++i) {
            char **_item = sv_elem_addr(y->sv_fargs, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_fargs_type = sv_dup(x->sv_fargs_type);    // duplicate buffer
        if (sv_void == y->sv_fargs_type && NULL != x->sv_fargs_type) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_fargs_type); ++i) {
            char **_item = sv_elem_addr(y->sv_fargs_type, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_fargs_init = sv_dup(x->sv_fargs_init);    // duplicate buffer
        if (sv_void == y->sv_fargs_init && NULL != x->sv_fargs_init) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_fargs_init); ++i) {
            char **_item = sv_elem_addr(y->sv_fargs_init, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->sv_fargs_ptrs = sv_dup(x->sv_fargs_ptrs);    // duplicate buffer
        if (sv_void == y->sv_fargs_ptrs && NULL != x->sv_fargs_ptrs) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->sv_fargs_ptrs); ++i) {
            vec_string **_item2 = sv_elem_addr(y->sv_fargs_ptrs, i);
            const vec_string *_item2_old = *_item2;
            {
                *_item2 = sv_dup(_item2_old);   // duplicate buffer
                if (sv_void == *_item2 && NULL != _item2_old) {
                    return 2;
                }
                for (size_t i = 0; i < sv_len(*_item2); ++i) {
                    char **_item = sv_elem_addr(*_item2, i);
                    const char *_item_old = *_item;
                    *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                    if (NULL != (_item_old) && NULL == (*_item)) {
                        return 2;
                    }

                }
            }
        }
    }
    y->iname = x->iname;
    y->fargs_i0 = x->fargs_i0;
    y->fargs_i1 = x->fargs_i1;
    y->ndefault = x->ndefault;
    y->named = x->named;
    return 0;
}

void fdef_clear(struct fdef *x)
{
    x->from_typedef = false;
    free(x->name);
    x->name = NULL;
    x->index = 0;
    ss_free(&(x->retexpr));
    *x = (struct fdef) { 0 };
}

int fdef_dup(const struct fdef *x, struct fdef *y)
{
    if (NULL == y)
        return 1;
    y->from_typedef = x->from_typedef;
    y->name = (NULL != (x->name)) ? rstrdup(x->name) : NULL;
    if (NULL != (x->name) && NULL == (y->name)) {
        return 2;
    }

    y->index = x->index;
    y->retexpr = ss_dup(x->retexpr);
    if (ss_void == y->retexpr && NULL != x->retexpr) {
        return 2;
    }

    return 0;
}

int cmp_dllarg_by_name(const void *keyval, const void *datum)
{
    struct dllarg *a = (struct dllarg *)keyval;
    struct dllarg *b = (struct dllarg *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}

int cmp_snippet_by_name(const void *keyval, const void *datum)
{
    struct snippet *a = (struct snippet *)keyval;
    struct snippet *b = (struct snippet *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}

int cmp_table_by_name(const void *keyval, const void *datum)
{
    struct table *a = (struct table *)keyval;
    struct table *b = (struct table *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}

int cmp_decl_by_name(const void *keyval, const void *datum)
{
    struct decl *a = (struct decl *)keyval;
    struct decl *b = (struct decl *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}

int cmp_fdef_by_name(const void *keyval, const void *datum)
{
    struct fdef *a = (struct fdef *)keyval;
    struct fdef *b = (struct fdef *)datum;
    const char *astr = a->name;
    const char *bstr = b->name;
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both not-null
            : ((NULL != astr) ? -1      // null B, move to end
               : ((NULL != bstr) ? 1    // null A, move to end
                  : 0)));       // both null, do nothing
}
