#ifndef CMOD_UTIL_STRING_H
#define CMOD_UTIL_STRING_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>

#include "parts/typedef_srt_alias.h"
#include "parts/struct_str_mod.h"
#include <stdbool.h>
#include <regex.h>
int cmp_sstr(const void *, const void *);
/* string <-> integer conversion */
__attribute__((unused))
int strtof_check(const char *, float[static 1]);
__attribute__((unused))
int strtod_check(const char *, double[static 1]);
__attribute__((unused))
int strtold_check(const char *, long double[static 1]);
#define strtoumax_check(a,b) strtoumax_check_base(a,b,1)
__attribute__((unused))
int strtoumax_check_base(const char *, uintmax_t[static 1], int base);
#define strtouint_check(a,b) strtouint_check_base(a,b,1)
__attribute__((unused))
int strtouint_check_base(const char *, unsigned[static 1], const int base);
#define strtoul_check(a,b) strtoul_check_base(a,b,1)
__attribute__((unused))
int strtoul_check_base(const char *, unsigned long[static 1], const int base);
#define strtoull_check(a,b) strtoull_check_base(a,b,1)
__attribute__((unused))
int strtoull_check_base(const char *, unsigned long long[static 1], const int base);
#define strtosize_check(a,b) strtosize_check_base(a,b,1)
__attribute__((unused))
int strtosize_check_base(const char *, size_t[static 1], const int base);
#define strtou8_check(a,b) strtou8_check_base(a,b,1)
__attribute__((unused))
int strtou8_check_base(const char *, uint8_t[static 1], const int base);
#define strtou16_check(a,b) strtou16_check_base(a,b,1)
__attribute__((unused))
int strtou16_check_base(const char *, uint16_t[static 1], const int base);
#define strtou32_check(a,b) strtou32_check_base(a,b,1)
__attribute__((unused))
int strtou32_check_base(const char *, uint32_t[static 1], const int base);
#define strtou64_check(a,b) strtou64_check_base(a,b,1)
__attribute__((unused))
int strtou64_check_base(const char *, uint64_t[static 1], const int base);
#define strtoimax_check(a,b) strtoimax_check_base(a,b,1)
__attribute__((unused))
int strtoimax_check_base(const char *, intmax_t[static 1], int base);
#define strtoint_check(a,b) strtoint_check_base(a,b,1)
__attribute__((unused))
int strtoint_check_base(const char *, int[static 1], const int base);
#define strtol_check(a,b) strtol_check_base(a,b,1)
__attribute__((unused))
int strtol_check_base(const char *, long[static 1], const int base);
#define strtoll_check(a,b) strtoll_check_base(a,b,1)
__attribute__((unused))
int strtoll_check_base(const char *, long long[static 1], const int base);
#define strtossize_check(a,b) strtossize_check_base(a,b,1)
__attribute__((unused))
int strtossize_check_base(const char *, ssize_t[static 1], const int base);
#define strtoi8_check(a,b) strtoi8_check_base(a,b,1)
__attribute__((unused))
int strtoi8_check_base(const char *, int8_t[static 1], const int base);
#define strtoi16_check(a,b) strtoi16_check_base(a,b,1)
__attribute__((unused))
int strtoi16_check_base(const char *, int16_t[static 1], const int base);
#define strtoi32_check(a,b) strtoi32_check_base(a,b,1)
__attribute__((unused))
int strtoi32_check_base(const char *, int32_t[static 1], const int base);
#define strtoi64_check(a,b) strtoi64_check_base(a,b,1)
__attribute__((unused))
int strtoi64_check_base(const char *, int64_t[static 1], const int base);
#define STRIMAX_LEN 21  /* = ceil(log10(INTMAX_MAX)) + 2 */
#define STRUMAX_LEN 25  /* = ceil(log8(UINTMAX_MAX)) + 3 */
int strimax(intmax_t, char[static STRIMAX_LEN], const char[restrict static 1]);
int strumax(uintmax_t, char[static STRUMAX_LEN], const char[restrict static 1]);
const char *(uint_tostr) (uintmax_t n, char buf[static STRUMAX_LEN]);
/* convert to valid C identifier character */
__attribute__((unused))
inline static int ascidchar(int c, bool first)
{
    return (isalpha((unsigned char)c)
            || '_' == c || (!first && isdigit((unsigned char)c)))
        ? c : ('*' == c ? 'p' : '_');
}

/* is C delimiter? */
__attribute__((unused))
inline static int is_cdelim(int c)
{
    return ('(' == c || ')' == c || '[' == c || ']' == c || '{' == c || '}' == c);
}

int (parse_str_mod) (const char *str, struct str_mod out[static 1], char err[static 1]);
bool (strisspace) (const char *s);      // s may be NULL
bool (striscid) (const char *s);        // s may be NULL
bool (striscidslash) (const char *s);   // s may be NULL
bool (strisidext) (const char *s);      // s may be NULL
int (strcunesc) (size_t len, const char s[static len], srt_string * out[static 1]);
int (strcesc) (size_t len, const char s[static len], srt_string * out[static 1]);
vec_string *(strsplit) (char *s, int (*issep) (int));
size_t (strdist) (const char s1[static 1], const char s2[static 1]);
void (ss_cat_byline) (srt_string * *ss, const srt_string * str, const char *pre, bool str_bool,
                      bool supnl_bool);
srt_string *(strregsub_parse) (const char *str, const char *sub, regmatch_t pmatch[static 10]);
int (strregsub) (const char *rinp, regex_t preg, const char *sub, bool opt_global_bool,
                 srt_string * newbuf[static 1], const char *endptr[static 1],
                 bool notfound_o[static 1]);
/* fuzzy match */
typedef const char *((strget_f)) (const void *array, size_t ix);
strget_f strget_array;
strget_f strget_array_opt;
strget_f strget_array_snip;
strget_f strget_array_tab;
strget_f strget_array_decl;
strget_f strget_array_fdef;
bool (str_fuzzy_match) (const char *key, const void *array, size_t arrlen, strget_f * getter,
                        float threshold, const char *match_o[static 1]);
/* srt_string extras */
srt_string *(ss_dec_esc_c) (srt_string * *s);
srt_string *(ss_enc_esc_c) (srt_string * *s);
#include "parts/struct_str_mod.h"
srt_string *(ss_cat_mod) (srt_string * *s, const srt_string * src, const char *name,
                          const struct str_mod mod);
/* vec_string extras */
srt_string *(vstr_join) (const vec_string * sv, int sep);
#define vstr_addstr(v, ...) vstr_addstr_aux(v, __VA_ARGS__, S_INVALID_PTR_VARG_TAIL)
bool (vstr_addstr_aux) (vec_string * *v, const char *c1, ...);
#endif
#include <signal.h>
#include <stdbool.h>

#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "ralloc.h"
#include <assert.h>

/*  compare srt_strings as C strings */
int cmp_sstr(const void *keyval, const void *datum)
{
    const srt_string *a = *(const srt_string **)keyval;
    const srt_string *b = *(const srt_string **)datum;
    const char *astr = ss_to_c(a);
    const char *bstr = ss_to_c(b);
    return ((NULL != astr && NULL != bstr) ? strcmp(astr, bstr) // both good
            : ((NULL != astr) ? -1      // bad B, move to end
               : ((NULL != bstr) ? 1    // bad A, move to end
                  : 0)));       // both bad, do nothing
}

/* check if string is empty or whitespace */
bool strisspace(const char *s)
{
    if (NULL == s)
        return true;
    for (const char *c = s; '\0' != *c; c++)
        if (!isspace((unsigned char)*c))
            return false;
    return true;
}

/* check if string is C_IDENTIFIER */
bool striscid(const char *s)
{
    if (NULL == s)
        return false;
    const char *c = s;
    if ('_' != *c && !isalpha((unsigned char)*c))
        return false;
    for (++c; '\0' != *c; ++c)
        if ('_' != *c && !isalnum((unsigned char)*c))
            return false;
    return true;
}

inline static int is_char_ex(int c)
{
    return ('?' == c || '!' == c || '@' == c || '&' == c || ':' == c);
}

inline static int is_char_ext(int c)
{
    return (is_char_ex(c) || '-' == c || '+' == c || '.' == c);
}

/* check if string is IDENTIFIER_EXT */
bool strisidext(const char *s)
{
    if (NULL == s)
        return false;
    const char *c = s;
    if ('_' != *c && !isalpha((unsigned char)*c)
        && !is_char_ex((unsigned char)*c))
        return false;
    for (++c; '\0' != *c && '/' != *c; ++c)
        if ('_' != *c && !isalnum((unsigned char)*c)
            && !is_char_ext((unsigned char)*c))
            return false;
    if ('\0' == *c)
        return true;
    if (!isdigit((unsigned char)*++c))
        return false;   // at least one digit
    for (++c; '\0' != *c; ++c)
        if (!isdigit((unsigned char)*c))
            return false;
    return true;
}

/* check if string is C_IDENTIFIER with optional slash + number at end */
bool striscidslash(const char *s)
{
    if (NULL == s)
        return false;
    const char *c = s;
    if ('_' != *c && !isalpha((unsigned char)*c))
        return false;
    for (++c; '\0' != *c && '/' != *c; ++c)
        if ('_' != *c && !isalnum((unsigned char)*c))
            return false;
    if ('\0' == *c)
        return true;
    if (!isdigit((unsigned char)*++c))
        return false;   // at least one digit
    for (++c; '\0' != *c; ++c)
        if (!isdigit((unsigned char)*c))
            return false;
    return true;
}

/* unescape C escapes */
#define IMPL_MAXHEX 16
#define INRANGE(c,a,b) ((c) >= (a) && (c) <= (b))
#define IS_OCTAL(c) INRANGE(c,'0','7')
#define IS_HEXADECIMAL(c) (INRANGE(c,'0','9') \
    || INRANGE(c,'a','f') || INRANGE(c,'A','F'))
#define IS_C_SRC_CHAR(c) (INRANGE(c,'A','Z') \
    || INRANGE(c,'a','z') || INRANGE(c,'0','9') || (c) == ' ' \
    || (INRANGE(c,'!','/') && (c) != '$') \
    || INRANGE(c,':','?') || INRANGE(c,'[','_') || INRANGE(c,'{','~') \
    || (c) == '\t' || (c) == '\v' || (c) == '\f')
int strcunesc(size_t len, const char s[static len], srt_string *out[static 1])
{
    int rc = 0;
    srt_string *(ss) = ss_alloc(len);
    if (ss_void == (ss)) {
        rc = 1;
        goto fail;
    }
    ;

    unsigned long byte;
    unsigned long long uchar;
    size_t ioct = 0;
    char octbuf[4];     // up to 3 octal chars
    size_t ihex = 0;
    char hexbuf[IMPL_MAXHEX + 1];       // up to IMPL_MAXHEX hex chars
    size_t iuni = 0;
    char unibuf[9];     // up to 8 hex chars

    bool inclass;
    enum {
        NORMAL,
        ESCAPE,
        OCTAL,
        HEXADECIMAL,
        UNICODE_SHORT,
        UNICODE_LONG
    } mode = NORMAL;

    for (size_t i = 0; i < len; ++i) {
        const char c = s[i];
 begin:
        switch (mode) {
        case NORMAL:
            switch (c) {
            case '\\':
                mode = ESCAPE;
                continue;
            default:
                if (!IS_C_SRC_CHAR(c)) {
                    rc = 2;     // ERROR: stray char
                    goto fail;
                }
                ss_cat_cn1(&ss, c);
                mode = NORMAL;
                continue;
            }
            break;
        case ESCAPE:
            switch (c) {
            case '\\': /* FALLTHROUGH */
            case '\'': /* FALLTHROUGH */
            case '\"': /* FALLTHROUGH */
            case '?':  /* FALLTHROUGH */
                ss_cat_cn1(&ss, c);
                mode = NORMAL;
                continue;
            case 'a':
                ss_cat_cn1(&ss, '\a');
                mode = NORMAL;
                continue;
            case 'b':
                ss_cat_cn1(&ss, '\b');
                mode = NORMAL;
                continue;
            case 'f':
                ss_cat_cn1(&ss, '\f');
                mode = NORMAL;
                continue;
            case 'n':
                ss_cat_cn1(&ss, '\n');
                mode = NORMAL;
                continue;
            case 'r':
                ss_cat_cn1(&ss, '\r');
                mode = NORMAL;
                continue;
            case 't':
                ss_cat_cn1(&ss, '\t');
                mode = NORMAL;
                continue;
            case 'v':
                ss_cat_cn1(&ss, '\v');
                mode = NORMAL;
                continue;
            case '0':  /* FALLTHROUGH */
            case '1':  /* FALLTHROUGH */
            case '2':  /* FALLTHROUGH */
            case '3':  /* FALLTHROUGH */
            case '4':  /* FALLTHROUGH */
            case '5':  /* FALLTHROUGH */
            case '6':  /* FALLTHROUGH */
            case '7':  /* FALLTHROUGH */
                octbuf[ioct++] = c;
                mode = OCTAL;
                continue;
            case 'x':
                mode = HEXADECIMAL;
                continue;
            case 'u':
                mode = UNICODE_SHORT;
                continue;
            case 'U':
                mode = UNICODE_LONG;
                continue;
            default:
                rc = 3; // ERROR: invalid escape
                goto fail;
            }
            break;
        case OCTAL:
            if ((inclass = IS_OCTAL(c)))
                octbuf[ioct++] = c;
            if (!inclass || ioct == 3) {
                assert(ioct > 0);
                octbuf[ioct] = '\0';    // NUL terminate
                byte = strtoul(octbuf, NULL, 8);
                if (byte > 0xff) {
                    rc = 5;     // ERROR: out of range
                    goto fail;
                }
                ss_cat_cn1(&ss, byte);
                mode = NORMAL;
                ioct = 0;       // reset
            }
            if (inclass)
                continue;
            else
                goto begin;
        case HEXADECIMAL:
            if ((inclass = IS_HEXADECIMAL(c)))
                hexbuf[ihex++] = c;
            if (!inclass || ihex == IMPL_MAXHEX) {
                if (ihex == 0) {
                    rc = 4;     // ERROR: incomplete escape
                    goto fail;
                }
                hexbuf[ihex] = '\0';    // NUL terminate
                byte = strtoul(hexbuf, NULL, 16);
                if (byte > 0xff) {
                    rc = 5;     // ERROR: out of range
                    goto fail;
                }
                ss_cat_cn1(&ss, byte);
                mode = NORMAL;
                ihex = 0;       // reset
            }
            if (inclass)
                continue;
            else
                goto begin;
            break;
        case UNICODE_SHORT:    /* FALLTHROUGH */
        case UNICODE_LONG:     /* FALLTHROUGH */
            if ((inclass = IS_HEXADECIMAL(c)))
                unibuf[iuni++] = c;
            if (!inclass || (mode == UNICODE_SHORT && iuni == 4)
                || (mode == UNICODE_LONG && iuni == 8)) {
                if (iuni != 4 && iuni != 8) {
                    rc = 4;     // ERROR: incomplete escape
                    goto fail;
                }
                unibuf[iuni] = '\0';    // NUL terminate
                uchar = strtoull(unibuf, NULL, 16);
                if ((uchar < 0xA0 && uchar != 0x24 && uchar != 0x40 && uchar != 0x60)   // basic or control
                    || (uchar >= 0xD800 && uchar <= 0xDFFF)     // high surrogate
                    || uchar > 0x10FFFF // not Unicode code point
                    ) {
                    rc = 6;     // ERROR: out of range
                    goto fail;
                }
                if (EOF == ss_putchar(&ss, uchar)) {    // add UTF8-encoded character
                    rc = 1;     // ERROR: string overflow (out of memory)
                    goto fail;
                }
                mode = NORMAL;
                iuni = 0;       // reset
            }
            if (inclass)
                continue;
            else
                goto begin;
            break;
        }
    }

    /* end of input */
    switch (mode) {
    case NORMAL:       /* all good */
        break;
    case ESCAPE:
        rc = 4; // ERROR: incomplete escape
        goto fail;
    case OCTAL:
        assert(ioct > 0);
        octbuf[ioct] = '\0';    // NUL terminate
        byte = strtoul(octbuf, NULL, 8);
        if (byte > 0xff) {
            rc = 5;     // ERROR: out of range
            goto fail;
        }
        ss_cat_cn1(&ss, byte);
        break;
    case HEXADECIMAL:
        if (ihex == 0) {
            rc = 4;     // ERROR: incomplete escape
            goto fail;
        }
        hexbuf[ihex] = '\0';    // NUL terminate
        byte = strtoul(hexbuf, NULL, 16);
        if (byte > 0xff) {
            rc = 5;     // ERROR: out of range
            goto fail;
        }
        ss_cat_cn1(&ss, byte);
        break;
    case UNICODE_SHORT:        /* FALLTHROUGH */
    case UNICODE_LONG: /* FALLTHROUGH */
        if (iuni != 4 && iuni != 8) {
            rc = 4;     // ERROR: incomplete escape
            goto fail;
        }
        unibuf[iuni] = '\0';    // NUL terminate
        uchar = strtoull(unibuf, NULL, 16);
        if ((uchar < 0xA0 && uchar != 0x24 && uchar != 0x40 && uchar != 0x60)   // basic or control
            || (uchar >= 0xD800 && uchar <= 0xDFFF)     // high surrogate
            || uchar > 0x10FFFF // not Unicode code point
            ) {
            rc = 6;     // ERROR: out of range
            goto fail;
        }
        if (EOF == ss_putchar(&ss, uchar)) {    // add UTF8-encoded character
            rc = 1;     // ERROR: string overflow (out of memory)
            goto fail;
        }
        break;
    }

    *out = ss;
    ss = NULL;  // hand-over
 fail:
    ss_free(&ss);

    return rc;
}

/* escape with C escapes (bytes) */
int strcesc(size_t len, const char s[static len], srt_string *out[static 1])
{
    int rc = 0;
    srt_string *(ss) = ss_alloc(len);
    if (ss_void == (ss)) {
        rc = 1;
        goto fail;
    }
    ;

    for (size_t i = 0; i < len; ++i) {
        const char c = s[i];
        switch (c) {
        case '\a':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'a');
            break;
        case '\b':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'b');
            break;
        case '\f':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'f');
            break;
        case '\n':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'n');
            break;
        case '\r':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'r');
            break;
        case '\t':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 't');
            break;
        case '\v':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, 'v');
            break;
        case '\\':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, '\\');
            break;
        case '\'':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, '\'');
            break;
        case '\"':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, '\"');
            break;
        case '\?':
            ss_cat_cn1(&ss, '\\');
            ss_cat_cn1(&ss, '\?');
            break;
        default:
            if (isprint((unsigned char)c))
                ss_cat_cn1(&ss, c);
            else if ('\0' == c) {
                ss_cat_cn1(&ss, '\\');
                ss_cat_cn1(&ss, '0');
            }
            else {      // encode as hex byte
                uint8_t u = c;
                ss_cat_cn1(&ss, '\\');
                ss_cat_cn1(&ss, 'x');
                switch (u / 16) {
                case 0:
                    ss_cat_cn1(&ss, '0');
                    break;
                case 1:
                    ss_cat_cn1(&ss, '1');
                    break;
                case 2:
                    ss_cat_cn1(&ss, '2');
                    break;
                case 3:
                    ss_cat_cn1(&ss, '3');
                    break;
                case 4:
                    ss_cat_cn1(&ss, '4');
                    break;
                case 5:
                    ss_cat_cn1(&ss, '5');
                    break;
                case 6:
                    ss_cat_cn1(&ss, '6');
                    break;
                case 7:
                    ss_cat_cn1(&ss, '7');
                    break;
                case 8:
                    ss_cat_cn1(&ss, '8');
                    break;
                case 9:
                    ss_cat_cn1(&ss, '9');
                    break;
                case 10:
                    ss_cat_cn1(&ss, 'a');
                    break;
                case 11:
                    ss_cat_cn1(&ss, 'b');
                    break;
                case 12:
                    ss_cat_cn1(&ss, 'c');
                    break;
                case 13:
                    ss_cat_cn1(&ss, 'd');
                    break;
                case 14:
                    ss_cat_cn1(&ss, 'e');
                    break;
                case 15:
                    ss_cat_cn1(&ss, 'f');
                    break;
                }
                switch (u % 16) {
                case 0:
                    ss_cat_cn1(&ss, '0');
                    break;
                case 1:
                    ss_cat_cn1(&ss, '1');
                    break;
                case 2:
                    ss_cat_cn1(&ss, '2');
                    break;
                case 3:
                    ss_cat_cn1(&ss, '3');
                    break;
                case 4:
                    ss_cat_cn1(&ss, '4');
                    break;
                case 5:
                    ss_cat_cn1(&ss, '5');
                    break;
                case 6:
                    ss_cat_cn1(&ss, '6');
                    break;
                case 7:
                    ss_cat_cn1(&ss, '7');
                    break;
                case 8:
                    ss_cat_cn1(&ss, '8');
                    break;
                case 9:
                    ss_cat_cn1(&ss, '9');
                    break;
                case 10:
                    ss_cat_cn1(&ss, 'a');
                    break;
                case 11:
                    ss_cat_cn1(&ss, 'b');
                    break;
                case 12:
                    ss_cat_cn1(&ss, 'c');
                    break;
                case 13:
                    ss_cat_cn1(&ss, 'd');
                    break;
                case 14:
                    ss_cat_cn1(&ss, 'e');
                    break;
                case 15:
                    ss_cat_cn1(&ss, 'f');
                    break;
                }
            }
            break;
        }
    }

    *out = ss;
    ss = NULL;  // hand-over
 fail:
    ss_free(&ss);

    return rc;
}

/* split string in-place by separator */
vec_string *strsplit(char *s, int (*issep) (int))
{
    vec_string *(parts) = sv_alloc_t(SV_PTR, 8);
    if (sv_void == (parts)) {
        goto fail;
    }

    bool inword = false;
    for (char *c = s; '\0' != *c; c++) {
        if (!issep((unsigned char)*c) && !inword) {
            if (!sv_push(&(parts), &(c))) {
                goto fail;
            }

            inword = true;
        }
        else if (issep((unsigned char)*c) && inword) {
            *c = '\0';  // space to NUL
            inword = false;
        }
    }
    goto pass;
 fail:
    sv_free(&parts);
 pass:
    return parts;
}

/* compute Levenshtein distance of two strings (naive Wagner-Fischer) */
size_t strdist(const char s1[static 1], const char s2[static 1])
{
    size_t m = strlen(s1);
    size_t n = strlen(s2);

    if (m == 0)
        return n;
    if (n == 0)
        return m;

    size_t d[m + 1][n + 1];     // VLA
    memset(d, 0x0, ((m + 1) * (n + 1)) * sizeof(size_t));       // clear

    for (size_t i = 1; i <= m; ++i)
        d[i][0] = i;
    for (size_t j = 1; j <= n; ++j)
        d[0][j] = j;

    for (size_t j = 1; j <= n; ++j)
        for (size_t i = 1; i <= m; ++i) {
            size_t min[3] = {
                d[i - 1][j] + 1,        // deletion
                d[i][j - 1] + 1,        // insertion
                d[i - 1][j - 1] + (s1[i - 1] != s2[j - 1])      // substitution
            };
            if (((min)[(0) + 0] > (min)[(0) + 2])) {
                {
                    size_t _ = ((min)[(0) + 0]);
                    ((min)[(0) + 0]) = ((min)[(0) + 2]);
                    ((min)[(0) + 2]) = _;
                }
            }

            if (((min)[(0) + 0] > (min)[(0) + 1])) {
                {
                    size_t _ = ((min)[(0) + 0]);
                    ((min)[(0) + 0]) = ((min)[(0) + 1]);
                    ((min)[(0) + 1]) = _;
                }
            }

            if (((min)[(0) + 1] > (min)[(0) + 2])) {
                {
                    size_t _ = ((min)[(0) + 1]);
                    ((min)[(0) + 1]) = ((min)[(0) + 2]);
                    ((min)[(0) + 2]) = _;
                }
            }

            d[i][j] = min[0];
        }

    return d[m][n];
}

/* concatenate string line-by-line */
void ss_cat_byline(srt_string **ss, const srt_string *str, const char *pre, bool str_bool,
                   bool supnl_bool)
{
    const srt_string *nl = ss_crefs("\n");
    size_t nnl = ss_split(str, nl, NULL, 0);
    srt_string_ref linrefs[nnl];        // VLA
    nnl = ss_split(str, nl, linrefs, nnl);
    for (size_t i = 0; i < nnl; ++i) {
        const srt_string *line = ss_ref(&linrefs[i]);
        if (str_bool)
            ss_cat_cn1(ss, '"');
        ss_cat_c(ss, pre);
        ss_cat(ss, line);
        if (str_bool)
            ss_cat_cn1(ss, '"');
        /* all lines but last are sure to have newline after split */
        if (!supnl_bool && i < nnl - 1)
            ss_cat_cn1(ss, '\n');
    }
    if (!supnl_bool && '\n' == ss_at(str, ss_len(str) - 1))     // last newline
        ss_cat_cn1(ss, '\n');
}

// TODO: unit test

/* parse regex substitution string */
srt_string *strregsub_parse(const char *str, const char *sub, regmatch_t pmatch[static 10])
{
    srt_string *ssub = NULL;
    if (NULL == str || NULL == sub)
        goto fail;
    (ssub) = ss_alloc(64);
    if (ss_void == (ssub)) {
        goto fail;
    }
    ;

    for (const char *c = sub; '\0' != *c; ++c) {
        if ('\\' != *c) {       // not escaped
            ss_cat_cn1(&ssub, *c);
            continue;
        }
        const char e = *++c;    // escaped
        if (e >= '0' && e <= '9') {     // submatch
            int ix = e - '0';
            if (pmatch[ix].rm_so == -1)
                continue;
            size_t pmatch_len = pmatch[ix].rm_eo - pmatch[ix].rm_so;
            ss_cat_cn(&ssub, &str[pmatch[ix].rm_so], pmatch_len);
        }
        else {
            char u;     // unescaped char
            switch (e) {
            case '\\':
                u = '\\';
                break;
            case 'e':
                u = '\x1b';
                break;
            case 'a':
                u = '\a';
                break;
            case 'b':
                u = '\b';
                break;
            case 'f':
                u = '\f';
                break;
            case 'n':
                u = '\n';
                break;
            case 'r':
                u = '\r';
                break;
            case 't':
                u = '\t';
                break;
            case 'v':
                u = '\v';
                break;
            default:
                goto fail;
                break;
            }
            ss_cat_cn1(&ssub, u);       // append unescaped
        }
    }

    return ss_check(&ssub);

 fail:
    ss_free(&ssub);
    return ss_void;
}

// TODO: unit test

/* find and replace (all) regex matches */
int strregsub(const char *rinp, regex_t preg, const char *sub, bool opt_global_bool,
              srt_string *newbuf[static 1], const char *endptr[static 1], bool notfound_o[static 1])
{
    int rc = 0;
    bool notfound = false;

    srt_string *ssub = NULL;    // parsed replacement string

    regmatch_t pmatch[10] = { {0} };    // up to 9 sub-matches: \1 to \9
    const char *match = (regexec(&preg, rinp, (sizeof(pmatch) / sizeof(*(pmatch))), pmatch, 0) == 0)
        ? rinp + pmatch[0].rm_so : NULL;
    if (NULL == match)
        notfound = true;
    else
        do {    // substitution
            /* append input up to match */
            ptrdiff_t delta = match - rinp;
            ss_cat_cn(newbuf, rinp, delta);

            /* parse replacement string */
            ssub = strregsub_parse(rinp, sub, pmatch);
            if (ss_void == ssub) {
                rc = 1;
                goto fail;
            }
            ss_cat(newbuf, ssub);       // append
            ss_free(&ssub);

            rinp += pmatch[0].rm_eo;    // advance after match
            if ('\0' == *rinp || pmatch[0].rm_eo == 0)  // consumed string or zero-length match
                break;
#if DEBUG
            fprintf(stderr, "`%s` %jd %jd\n",
                    rinp, (intmax_t) pmatch[0].rm_so, (intmax_t) pmatch[0].rm_eo);
#endif
        } while (opt_global_bool
                 && NULL != (match =
                             ((regexec(&preg, rinp, (sizeof(pmatch) / sizeof(*(pmatch))), pmatch, 0)
                               == 0)
                              ? rinp + pmatch[0].rm_so : NULL)));

    *endptr = rinp;
    *notfound_o = notfound;
 fail:
    ss_free(&ssub);

    return rc;
}

int parse_str_mod(const char *str, struct str_mod out[static 1], char err[static 1])
{
    if (NULL == str)
        return 0;
    for (const char *s = str; '\0' != *s; ++s) {
        switch (*s) {
        case 'T':
            out->trim |= TRIM_SPACE;
            break;
        case '_':
            out->trim |= TRIM_LAST;
            break;
        case 'n':
            out->trim |= TRIM_NL;
            break;
        case 'E':
            out->tran = TRAN_EMPTY;
            break;
        case 'C':
            out->tran = TRAN_CID;
            break;
        case 'e':
            out->tran = TRAN_SNIPPET;
            break;
        case 'g':
            out->tran = TRAN_CSTR;
            break;
        case 'j':
            out->tran = TRAN_JSON;
            break;
        case 'x':
            out->tran = TRAN_HEX;
            break;
        case 'X':
            out->tran = TRAN_B64;
            break;
        case 'l':
            out->tran = TRAN_LZH;
            break;
        case 'L':
            out->fold = FOLD_LOWER;
            break;
        case 'U':
            out->fold = FOLD_UPPER;
            break;
        case 'h':
            out->pre = PRE_HTAG;
            break;
        case 'q':
            out->pre = PRE_BSLASH;
            break;
        case 'd':
            out->pre = PRE_DOLLAR;
            break;
        case 't':
            out->pre = PRE_DOT;
            break;
        case 'm':
            out->pre = PRE_MOD;
            break;
        case 'a':
            out->pre = PRE_ASTER;
            break;
        case 'r':
            out->pre = PRE_AMPER;
            break;
        case 'w':
            out->wrap |= WRAP_SPACE;
            break;
        case 's':
            out->wrap |= WRAP_SQUOT;
            break;
        case 'S':
            out->wrap |= WRAP_DQUOT;
            break;
        case 'P':
            out->wrap |= WRAP_PAREN;
            break;
        case 'K':
            out->wrap |= WRAP_BRACK;
            break;
        case 'A':
            out->wrap |= WRAP_ANGLE;
            break;
        case 'B':
            out->wrap |= WRAP_BRACE;
            break;
        case 'b':
            out->wrap |= WRAP_BTICK;
            break;
        case 'H':
            out->wrap |= WRAP_HSTR;
            break;
        case 'p':
            out->lead |= LEAD_PTR;
            break;
        case 'R':
            out->lead |= LEAD_REF;
            break;
        case 'D':
            out->lead |= LEAD_DOLLAR;
            break;
        case 'z':
            out->lead |= LEAD_NAME;
            break;
        case 'c':
            out->lead |= LEAD_COMMA;
            break;
        case 'Z':
            out->trail |= TRAIL_COMMA;
            break;
        case 'W':
            out->trail |= TRAIL_SPACE;
            break;
        case 'N':
            out->trail |= TRAIL_NL;
            break;
        default:
            *err = *s;  // blame
            return 1;
            break;
        }
    }
    return 0;
}

srt_string *ss_cat_mod(srt_string **s, const srt_string *src, const char *name,
                       const struct str_mod mod)
{
    if (!s || !src)
        return ss_void;

    bool failed = false;
    srt_string *y = NULL;

    if (mod.tran == TRAN_EMPTY && '\0' == ss_at(src, 0))
        return ss_check(s);     // do nothing

    y = ss_dup(src);    // init transformed string
    if (NULL == y)
        return ss_void;

    /* apply trimming */
    if ((((mod.trim) & (TRIM_SPACE)) == (TRIM_SPACE))) {
        ss_trim(&y);
    }
    if ((((mod.trim) & (TRIM_LAST)) == (TRIM_LAST))) {
        ss_popchar(&y);
    }
    if ((((mod.trim) & (TRIM_NL)) == (TRIM_NL))) {
        ss_replace(&y, 0, ss_crefs("\n"), ss_crefs(""));
    }

    {   // apply transformation
        switch (mod.tran) {
        case TRAN_NONE:
            break;
        case TRAN_EMPTY:       /* handled above */
            break;
        case TRAN_SNIPPET:
            {
                srt_string *(z) = ss_alloc(0);
                if (ss_void == (z)) {
                    failed = true;
                    goto cleanup;
                }
                ;
                char *buf = ss_get_buffer(y);
                for (size_t i = 0; i < ss_len(y); ++i) {        // char by char
                    if ('%' == buf[i] || '$' == buf[i])
                        ss_cat_cn1(&z, buf[i]); // escape with itself
                    ss_cat_cn1(&z, buf[i]);
                }
                ss_free(&y);
                y = z;
            }
            break;
        case TRAN_CID:
            {
                char *buf = ss_get_buffer(y);
                for (size_t i = 0; i < ss_len(y); ++i)  // char by char
                    buf[i] = ascidchar((unsigned char)buf[i], i == 0);
            }
            break;
        case TRAN_CSTR:
            ss_enc_esc_c(&y);
            break;
        case TRAN_JSON:
            ss_enc_esc_json(&y, y);
            break;
        case TRAN_HEX:
            ss_enc_hex(&y, y);
            break;
        case TRAN_B64:
            ss_enc_b64(&y, y);
            break;
        case TRAN_LZH:
            ss_enc_lzh(&y, y);
            ss_enc_hex(&y, y);
            break;
        }
    }

    /* apply case folding */
    switch (mod.fold) {
    case FOLD_NONE:

        break;
    case FOLD_LOWER:
        ss_tolower(&y);
        break;
    case FOLD_UPPER:
        ss_toupper(&y);
        break;
    }

    /* append lead (stacking) */
    if ((((mod.lead) & (LEAD_COMMA)) == (LEAD_COMMA))) {
        ss_cat_c(s, ",");
    }
    if ((((mod.lead) & (LEAD_NAME)) == (LEAD_NAME))) {
        ss_cat_c(s, name ? name : "", name ? "=" : "");
    }
    if ((((mod.lead) & (LEAD_DOLLAR)) == (LEAD_DOLLAR))) {
        ss_cat_c(s, "$");
    }
    if ((((mod.lead) & (LEAD_REF)) == (LEAD_REF))) {
        ss_cat_c(s, "&");
    }
    if ((((mod.lead) & (LEAD_PTR)) == (LEAD_PTR))) {
        ss_cat_c(s, "*");
    }

    /* append wrap start (stacking) */
    if ((((mod.wrap) & (WRAP_HSTR)) == (WRAP_HSTR))) {
        ss_cat_c(s, "%<< ");
    }
    if ((((mod.wrap) & (WRAP_BTICK)) == (WRAP_BTICK))) {
        ss_cat_c(s, "`");
    }
    if ((((mod.wrap) & (WRAP_BRACE)) == (WRAP_BRACE))) {
        ss_cat_c(s, "{");
    }
    if ((((mod.wrap) & (WRAP_ANGLE)) == (WRAP_ANGLE))) {
        ss_cat_c(s, "<");
    }
    if ((((mod.wrap) & (WRAP_BRACK)) == (WRAP_BRACK))) {
        ss_cat_c(s, "[");
    }
    if ((((mod.wrap) & (WRAP_PAREN)) == (WRAP_PAREN))) {
        ss_cat_c(s, "(");
    }
    if ((((mod.wrap) & (WRAP_DQUOT)) == (WRAP_DQUOT))) {
        ss_cat_c(s, "\"");
    }
    if ((((mod.wrap) & (WRAP_SQUOT)) == (WRAP_SQUOT))) {
        ss_cat_c(s, "'");
    }
    if ((((mod.wrap) & (WRAP_SPACE)) == (WRAP_SPACE))) {
        ss_cat_c(s, " ");
    }

    /* append prefix */
    switch (mod.pre) {
    case PRE_NONE:

        break;
    case PRE_HTAG:
        ss_cat_cn1(s, '#');
        break;
    case PRE_BSLASH:
        ss_cat_cn1(s, '\\');
        break;
    case PRE_DOLLAR:
        ss_cat_cn1(s, '$');
        break;
    case PRE_DOT:
        ss_cat_cn1(s, '.');
        break;
    case PRE_MOD:
        ss_cat_cn1(s, '%');
        break;
    case PRE_ASTER:
        ss_cat_cn1(s, '*');
        break;
    case PRE_AMPER:
        ss_cat_cn1(s, '&');
        break;
    }

    /* append modified string */
    ss_cat(s, y);

    /* append wrap end (stacking) */
    if ((((mod.wrap) & (WRAP_SPACE)) == (WRAP_SPACE)))
        ss_cat_c(s, " ");
    if ((((mod.wrap) & (WRAP_SQUOT)) == (WRAP_SQUOT)))
        ss_cat_c(s, "'");
    if ((((mod.wrap) & (WRAP_DQUOT)) == (WRAP_DQUOT)))
        ss_cat_c(s, "\"");
    if ((((mod.wrap) & (WRAP_PAREN)) == (WRAP_PAREN)))
        ss_cat_c(s, ")");
    if ((((mod.wrap) & (WRAP_BRACK)) == (WRAP_BRACK)))
        ss_cat_c(s, "]");
    if ((((mod.wrap) & (WRAP_ANGLE)) == (WRAP_ANGLE)))
        ss_cat_c(s, ">");
    if ((((mod.wrap) & (WRAP_BRACE)) == (WRAP_BRACE)))
        ss_cat_c(s, "}");
    if ((((mod.wrap) & (WRAP_BTICK)) == (WRAP_BTICK)))
        ss_cat_c(s, "`");
    if ((((mod.wrap) & (WRAP_HSTR)) == (WRAP_HSTR)))
        ss_cat_c(s, " >>%");

    /* append trail (stacking) */
    if ((((mod.trail) & (TRAIL_COMMA)) == (TRAIL_COMMA))) {
        ss_cat_c(s, ",");
    }
    if ((((mod.trail) & (TRAIL_SPACE)) == (TRAIL_SPACE))) {
        ss_cat_c(s, " ");
    }
    if ((((mod.trail) & (TRAIL_NL)) == (TRAIL_NL))) {
        ss_cat_c(s, "\n");
    }

 cleanup:
    ss_free(&y);

    return failed ? ss_void : ss_check(s);
}

srt_string *ss_dec_esc_c(srt_string **s)
{
    if (!s)
        return ss_void;
    srt_string *ss = NULL;
    if (strcunesc(ss_len(*s), ss_get_buffer_r(*s), &ss) == 0) {
        ss_cpy(s, ss);
        ss_free(&ss);
        return ss_check(s);
    }
    else
        return ss_void;
}

srt_string *ss_enc_esc_c(srt_string **s)
{
    if (!s)
        return ss_void;
    srt_string *ss = NULL;
    if (strcesc(ss_len(*s), ss_get_buffer_r(*s), &ss) == 0) {
        ss_cpy(s, ss);
        ss_free(&ss);
        return ss_check(s);
    }
    else
        return ss_void;
}

const char *(strget_array) (const void *array, size_t ix) {
    const char **x = (const char **)array;
    return x[ix];
}

bool str_fuzzy_match(const char *key, const void *array, size_t arrlen, strget_f *getter,
                     float threshold, const char *match_o[static 1])
{
    if (NULL == key || '\0' == key[0]) {
        *match_o = NULL;
        return false;
    }

    bool is_close = false;
    const char *fuzzy_match = NULL;
    for (size_t ix = 0, mindist = SIZE_MAX, minsubdist = SIZE_MAX; ix < arrlen; ++ix) {
        const char *str = getter(array, ix);
        size_t dist = strdist(key, str);
        if (strstr(str, key) == str && dist < minsubdist) {     // starts with => is close
            minsubdist = dist;
            fuzzy_match = str;
            is_close = true;
        }
        else if (minsubdist == SIZE_MAX && dist < mindist) {    // update distance, is close?
            mindist = dist;
            fuzzy_match = str;
            is_close = ((double)dist / strlen(str) <= threshold);
        }
    }

    *match_o = fuzzy_match;

    return is_close;
}

srt_string *vstr_join(const vec_string *sv, int sep)
{
    srt_string *(ss) = ss_alloc(0);
    if (ss_void == (ss)) {
        return ss_void;
    }
    ;
    for (size_t i = 0; i < sv_len(sv); ++i) {
        const char *str = sv_at_ptr(sv, i);
        if (i > 0)
            ss_putchar(&ss, sep);
        ss_cat_c(&ss, str);
    }
    return ss;
}

bool vstr_addstr_aux(vec_string **v, const char *c1, ...)
{
    if (!v || !*v)
        return S_FALSE;
    va_list ap;
    va_start(ap, c1);
    char *tmp = NULL;
    size_t op_cnt = 0;
    size_t push_cnt = 0;
    const void *next = c1;
    while (!s_varg_tail_ptr_tag(next)) {        // last element tag
        if (next) {
            tmp = rstrdup(next);
            if (tmp && sv_push_ptr(v, tmp))
                ++push_cnt;
            else {
                free(tmp);
                (tmp) = NULL;
            }
        }
        else {  // empty
            if (sv_push_ptr(v, tmp))
                ++push_cnt;
        }
        next = (void *)va_arg(ap, void *);
        op_cnt++;
    }
    va_end(ap);
    return (op_cnt == push_cnt);        // true when all arguments were pushed
}

/* string <-> integer conversion */
__attribute__((unused))
int strtof_check(const char *str, float out[static 1])
{
    if (NULL == str || '\0' == str[0])
        return 1;       // invalid input

    errno = 0;
    char *endptr = NULL;
    float val = strtof(str, &endptr);
    if (ERANGE == errno && (val == -HUGE_VALF || val == HUGE_VALF))
        return 2;       // out of range
    else if (0 != errno && 0.0 == val)
        return 1;       // invalid input
    else if (endptr == str)
        return 3;       // garbage at start
    else if ('\0' != *endptr)
        return 4;       // garbage at end

    *out = val;

    return 0;
}

__attribute__((unused))
int strtod_check(const char *str, double out[static 1])
{
    if (NULL == str || '\0' == str[0])
        return 1;       // invalid input

    errno = 0;
    char *endptr = NULL;
    double val = strtod(str, &endptr);
    if (ERANGE == errno && (val == -HUGE_VAL || val == HUGE_VAL))
        return 2;       // out of range
    else if (0 != errno && 0.0 == val)
        return 1;       // invalid input
    else if (endptr == str)
        return 3;       // garbage at start
    else if ('\0' != *endptr)
        return 4;       // garbage at end

    *out = val;

    return 0;
}

__attribute__((unused))
int strtold_check(const char *str, long double out[static 1])
{
    if (NULL == str || '\0' == str[0])
        return 1;       // invalid input

    errno = 0;
    char *endptr = NULL;
    long double val = strtold(str, &endptr);
    if (ERANGE == errno && (val == -HUGE_VALL || val == HUGE_VALL))
        return 2;       // out of range
    else if (0 != errno && 0.0 == val)
        return 1;       // invalid input
    else if (endptr == str)
        return 3;       // garbage at start
    else if ('\0' != *endptr)
        return 4;       // garbage at end

    *out = val;

    return 0;
}

#ifndef strtoumax_check
#define strtoumax_check(a,b) strtoumax_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoumax_check_base(const char *str, uintmax_t out[static 1], int base)
{
    if (NULL == str || '\0' == str[0])
        return 1;       // invalid input

    for (; '\0' != *str && isspace((unsigned char)*str); ++str) ;       // skip leading spaces
    if ('\0' == *str || '-' == *str)
        return 1;       // negative sign or empty, invalid input

    if (base == 1)
        base = ('0' == str[0] && ('x' == str[1] || 'X' == str[1]))
            ? 16 : 10;  // hex or decimal

    errno = 0;
    char *endptr;
    uintmax_t val = strtoumax(str, &endptr, base);
    if (ERANGE == errno && (UINTMAX_MAX == val))
        return 2;       // out of range
    else if (0 != errno && 0 == val)
        return 1;       // invalid input
    else if (endptr == str)
        return 3;       // garbage at start
    else if ('\0' != *endptr)
        return 4;       // garbage at end

    *out = val;

    return 0;
}

#ifndef strtouint_check
#define strtouint_check(a,b) strtouint_check_base(a,b,1)
#endif
__attribute__((unused))
int strtouint_check_base(const char *str, unsigned out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) UINT_MAX)
        return 2;       // out of range
    *out = (unsigned)tmp;
    return 0;
}

#ifndef strtoul_check
#define strtoul_check(a,b) strtoul_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoul_check_base(const char *str, unsigned long out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) ULONG_MAX)
        return 2;       // out of range
    *out = (unsigned long)tmp;
    return 0;
}

#ifndef strtoull_check
#define strtoull_check(a,b) strtoull_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoull_check_base(const char *str, unsigned long long out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) ULLONG_MAX)
        return 2;       // out of range
    *out = (unsigned long long)tmp;
    return 0;
}

#ifndef strtosize_check
#define strtosize_check(a,b) strtosize_check_base(a,b,1)
#endif
__attribute__((unused))
int strtosize_check_base(const char *str, size_t out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) SIZE_MAX)
        return 2;       // out of range
    *out = (size_t)tmp;
    return 0;
}

#ifndef strtou8_check
#define strtou8_check(a,b) strtou8_check_base(a,b,1)
#endif
__attribute__((unused))
int strtou8_check_base(const char *str, uint8_t out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) UINT8_MAX)
        return 2;       // out of range
    *out = (uint8_t) tmp;
    return 0;
}

#ifndef strtou16_check
#define strtou16_check(a,b) strtou16_check_base(a,b,1)
#endif
__attribute__((unused))
int strtou16_check_base(const char *str, uint16_t out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) UINT16_MAX)
        return 2;       // out of range
    *out = (uint16_t) tmp;
    return 0;
}

#ifndef strtou32_check
#define strtou32_check(a,b) strtou32_check_base(a,b,1)
#endif
__attribute__((unused))
int strtou32_check_base(const char *str, uint32_t out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) UINT32_MAX)
        return 2;       // out of range
    *out = (uint32_t) tmp;
    return 0;
}

#ifndef strtou64_check
#define strtou64_check(a,b) strtou64_check_base(a,b,1)
#endif
__attribute__((unused))
int strtou64_check_base(const char *str, uint64_t out[static 1], const int base)
{
    int ret;
    uintmax_t tmp;
    if ((ret = strtoumax_check_base(str, &tmp, base)))
        return ret;
    if (tmp > (uintmax_t) UINT64_MAX)
        return 2;       // out of range
    *out = (uint64_t) tmp;
    return 0;
}

#ifndef strtoimax_check
#define strtoimax_check(a,b) strtoimax_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoimax_check_base(const char *str, intmax_t out[static 1], int base)
{
    if (NULL == str || '\0' == str[0])
        return 1;       // invalid input

    if (base == 1)
        base = 10;      // default

    errno = 0;
    char *endptr;
    intmax_t val = strtoimax(str, &endptr, base);
    if (ERANGE == errno && (INTMAX_MIN == val || INTMAX_MAX == val))
        return 2;       // out of range
    else if (0 != errno && 0 == val)
        return 1;       // invalid input
    else if (endptr == str)
        return 3;       // garbage at start
    else if ('\0' != *endptr)
        return 4;       // garbage at end

    *out = val;

    return 0;
}

#ifndef strtoint_check
#define strtoint_check(a,b) strtoint_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoint_check_base(const char *str, int out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) LONG_MIN || tmp > (intmax_t) INT_MAX)
        return 2;       // out of range
    *out = (int)tmp;
    return 0;
}

#ifndef strtol_check
#define strtol_check(a,b) strtol_check_base(a,b,1)
#endif
__attribute__((unused))
int strtol_check_base(const char *str, long out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) LONG_MIN || tmp > (intmax_t) LONG_MAX)
        return 2;       // out of range
    *out = (long)tmp;
    return 0;
}

#ifndef strtoll_check
#define strtoll_check(a,b) strtoll_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoll_check_base(const char *str, long long out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) LLONG_MIN || tmp > (intmax_t) LLONG_MAX)
        return 2;       // out of range
    *out = (long long)tmp;
    return 0;
}

#ifndef strtossize_check
#define strtossize_check(a,b) strtossize_check_base(a,b,1)
#endif
__attribute__((unused))
int strtossize_check_base(const char *str, ssize_t out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) 0 || tmp > (intmax_t) SSIZE_MAX)
        return 2;       // out of range
    *out = (ssize_t) tmp;
    return 0;
}

#ifndef strtoi8_check
#define strtoi8_check(a,b) strtoi8_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoi8_check_base(const char *str, int8_t out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) INT8_MIN || tmp > (intmax_t) INT8_MAX)
        return 2;       // out of range
    *out = (int8_t) tmp;
    return 0;
}

#ifndef strtoi16_check
#define strtoi16_check(a,b) strtoi16_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoi16_check_base(const char *str, int16_t out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) INT16_MIN || tmp > (intmax_t) INT16_MAX)
        return 2;       // out of range
    *out = (int16_t) tmp;
    return 0;
}

#ifndef strtoi32_check
#define strtoi32_check(a,b) strtoi32_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoi32_check_base(const char *str, int32_t out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) INT32_MIN || tmp > (intmax_t) INT32_MAX)
        return 2;       // out of range
    *out = (int32_t) tmp;
    return 0;
}

#ifndef strtoi64_check
#define strtoi64_check(a,b) strtoi64_check_base(a,b,1)
#endif
__attribute__((unused))
int strtoi64_check_base(const char *str, int64_t out[static 1], const int base)
{
    int ret;
    intmax_t tmp;
    if ((ret = strtoimax_check_base(str, &tmp, base)))
        return ret;
    if (tmp < (intmax_t) INT64_MIN || tmp > (intmax_t) INT64_MAX)
        return 2;       // out of range
    *out = (int64_t) tmp;
    return 0;
}

__attribute__((unused))
int strimax(intmax_t x, char buf[static STRIMAX_LEN], const char mode[restrict static 1])
{
    uintmax_t ux = (x == INTMAX_MIN) ? (uintmax_t) INTMAX_MAX + 1 : (uintmax_t) imaxabs(x);
    ;

    /* parse mode */
    bool zero_pad = false;
    bool space_sign = false;
    bool force_sign = false;
    for (const char *c = mode; '\0' != *c; ++c)
        switch (*c) {
        case '0':
            zero_pad = true;
            break;
        case '+':
            force_sign = true;
            break;
        case ' ':
            space_sign = true;
            break;
        case 'd':
            break;      // decimal (always)
        }

    int n = 0;
    char sign = (x < 0) ? '-' : (force_sign ? '+' : ' ');
    buf[STRIMAX_LEN - ++n] = '\0';      // NUL-terminate
    do {
        buf[STRIMAX_LEN - ++n] = '0' + ux % 10;
    } while (ux /= 10);
    if (zero_pad)
        while (n < STRIMAX_LEN - 1)
            buf[STRIMAX_LEN - ++n] = '0';
    if (x < 0 || force_sign || space_sign)
        buf[STRIMAX_LEN - ++n] = sign;

    return STRIMAX_LEN - n;
}

__attribute__((unused))
int strumax(uintmax_t ux, char buf[static STRUMAX_LEN], const char mode[restrict static 1])
{
    static const char lbytes[] = "0123456789abcdefx";
    static const char ubytes[] = "0123456789ABCDEFX";

    /* parse mode */
    int base = 10;
    int izero = 4;
    bool zero_pad = false;
    bool alternate = false;
    const char *bytes = lbytes;
    for (const char *c = mode; '\0' != *c; ++c)
        switch (*c) {
        case '#':
            alternate = true;
            if (base == 8)
                izero = 1;
            break;
        case '0':
            zero_pad = true;
            break;
        case 'd':
            base = 10;
            izero = 4;
            break;
        case 'o':
            base = 8;
            izero = (alternate ? 1 : 2);
            break;
        case 'x':
            base = 16;
            izero = 8;
            break;
        case 'X':
            base = 16;
            izero = 8;
            bytes = ubytes;
            break;
        }

    int n = 0;
    buf[STRUMAX_LEN - ++n] = '\0';      // NUL-terminate
    do {
        buf[STRUMAX_LEN - ++n] = bytes[ux % base];
    } while (ux /= base);
    if (zero_pad)
        while (n < STRUMAX_LEN - izero)
            buf[STRUMAX_LEN - ++n] = '0';
    if (alternate && base == 16) {
        buf[STRUMAX_LEN - ++n] = bytes[base];
        buf[STRUMAX_LEN - ++n] = '0';
    }
    else if (alternate && base == 8 && '0' != buf[STRUMAX_LEN - n])
        buf[STRUMAX_LEN - ++n] = '0';

    return STRUMAX_LEN - n;
}

const char *uint_tostr(uintmax_t n, char buf[static STRUMAX_LEN])
{
    /* static strings for first 100 unsigned integers */
    static const char uint100[][3] = {
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
            "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44",
            "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58",
            "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72",
            "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86",
            "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"
    };
    if (n < 100)
        return uint100[n];
    else {      // generate it
        int offset;
        (offset) = strumax(n, buf, "");

        return buf + offset;
    }
}
