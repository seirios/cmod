#include "validate_utf8.h"

int utf8_naive(const unsigned char *data, int len);
#if defined(__x86_64__) || defined(__aarch64__)
int utf8_range(const unsigned char *data, int len);
#endif
#if defined(__AVX2__)
int utf8_range_avx2(const unsigned char *data, int len);
#endif

bool is_valid_utf8(const unsigned char *buf, int len)
{

#if defined(__x86_64__)

#if defined(__AVX2__)   // AVX2
    return utf8_range_avx2(buf, len) == 0;
#else                           // SSE4
    return utf8_range(buf, len) == 0;
#endif

#elif defined(__aarch64__)      // NEON
    return utf8_range(buf, len) == 0;
#else                           // fallback
    return utf8_naive(buf, len) == 0;
#endif

}
