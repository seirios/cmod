
#ifndef CMOD_UTIL_STRING_H
#define CMOD_UTIL_STRING_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>

#include "parts/typedef_srt_alias.h"
#include "parts/struct_str_mod.h"

#include <stdbool.h>
#include <regex.h>

int cmp_sstr(const void *, const void *);

/* string <-> integer conversion */
__attribute__((unused))
int strtof_check(const char *, float[static 1]);
__attribute__((unused))
int strtod_check(const char *, double[static 1]);
__attribute__((unused))
int strtold_check(const char *, long double[static 1]);
#define strtoumax_check(a,b) strtoumax_check_base(a,b,1)
__attribute__((unused))
int strtoumax_check_base(const char *, uintmax_t[static 1], int base);
#define strtouint_check(a,b) strtouint_check_base(a,b,1)
__attribute__((unused))
int strtouint_check_base(const char *, unsigned[static 1], const int base);
#define strtoul_check(a,b) strtoul_check_base(a,b,1)
__attribute__((unused))
int strtoul_check_base(const char *, unsigned long[static 1], const int base);
#define strtoull_check(a,b) strtoull_check_base(a,b,1)
__attribute__((unused))
int strtoull_check_base(const char *, unsigned long long[static 1], const int base);
#define strtosize_check(a,b) strtosize_check_base(a,b,1)
__attribute__((unused))
int strtosize_check_base(const char *, size_t[static 1], const int base);
#define strtou8_check(a,b) strtou8_check_base(a,b,1)
__attribute__((unused))
int strtou8_check_base(const char *, uint8_t[static 1], const int base);
#define strtou16_check(a,b) strtou16_check_base(a,b,1)
__attribute__((unused))
int strtou16_check_base(const char *, uint16_t[static 1], const int base);
#define strtou32_check(a,b) strtou32_check_base(a,b,1)
__attribute__((unused))
int strtou32_check_base(const char *, uint32_t[static 1], const int base);
#define strtou64_check(a,b) strtou64_check_base(a,b,1)
__attribute__((unused))
int strtou64_check_base(const char *, uint64_t[static 1], const int base);
#define strtoimax_check(a,b) strtoimax_check_base(a,b,1)
__attribute__((unused))
int strtoimax_check_base(const char *, intmax_t[static 1], int base);
#define strtoint_check(a,b) strtoint_check_base(a,b,1)
__attribute__((unused))
int strtoint_check_base(const char *, int[static 1], const int base);
#define strtol_check(a,b) strtol_check_base(a,b,1)
__attribute__((unused))
int strtol_check_base(const char *, long[static 1], const int base);
#define strtoll_check(a,b) strtoll_check_base(a,b,1)
__attribute__((unused))
int strtoll_check_base(const char *, long long[static 1], const int base);
#define strtossize_check(a,b) strtossize_check_base(a,b,1)
__attribute__((unused))
int strtossize_check_base(const char *, ssize_t[static 1], const int base);
#define strtoi8_check(a,b) strtoi8_check_base(a,b,1)
__attribute__((unused))
int strtoi8_check_base(const char *, int8_t[static 1], const int base);
#define strtoi16_check(a,b) strtoi16_check_base(a,b,1)
__attribute__((unused))
int strtoi16_check_base(const char *, int16_t[static 1], const int base);
#define strtoi32_check(a,b) strtoi32_check_base(a,b,1)
__attribute__((unused))
int strtoi32_check_base(const char *, int32_t[static 1], const int base);
#define strtoi64_check(a,b) strtoi64_check_base(a,b,1)
__attribute__((unused))
int strtoi64_check_base(const char *, int64_t[static 1], const int base);
#define STRIMAX_LEN 21  /* = ceil(log10(INTMAX_MAX)) + 2 */
#define STRUMAX_LEN 25  /* = ceil(log8(UINTMAX_MAX)) + 3 */
int strimax(intmax_t, char[static STRIMAX_LEN], const char[restrict static 1]);
int strumax(uintmax_t, char[static STRUMAX_LEN], const char[restrict static 1]);
const char *(uint_tostr) (uintmax_t n, char buf[static STRUMAX_LEN]);

/* convert to valid C identifier character */
__attribute__((unused))
inline static int ascidchar(int c, bool first)
{
    return (isalpha((unsigned char)c)
            || '_' == c || (!first && isdigit((unsigned char)c)))
        ? c : ('*' == c ? 'p' : '_');
}

/* is C delimiter? */
__attribute__((unused))
inline static int is_cdelim(int c)
{
    return ('(' == c || ')' == c || '[' == c || ']' == c || '{' == c || '}' == c);
}

int (parse_str_mod) (const char *str, struct str_mod out[static 1], char err[static 1]);

bool (strisspace) (const char *s);      // s may be NULL
bool (striscid) (const char *s);        // s may be NULL
bool (striscidslash) (const char *s);   // s may be NULL
bool (strisidext) (const char *s);      // s may be NULL
int (strcunesc) (size_t len, const char s[static len], srt_string * out[static 1]);
int (strcesc) (size_t len, const char s[static len], srt_string * out[static 1]);
vec_string *(strsplit) (char *s, int (*issep) (int));
size_t (strdist) (const char s1[static 1], const char s2[static 1]);
void (ss_cat_byline) (srt_string * *ss, const srt_string * str, const char *pre, bool str_bool,
                      bool supnl_bool);

srt_string *(strregsub_parse) (const char *str, const char *sub, regmatch_t pmatch[static 10]);
int (strregsub) (const char *rinp, regex_t preg, const char *sub, bool opt_global_bool,
                 srt_string * newbuf[static 1], const char *endptr[static 1],
                 bool notfound_o[static 1]);

/* fuzzy match */
typedef const char *((strget_f)) (const void *array, size_t ix);

strget_f strget_array;
strget_f strget_array_opt;
strget_f strget_array_snip;
strget_f strget_array_tab;
strget_f strget_array_decl;
strget_f strget_array_fdef;

bool (str_fuzzy_match) (const char *key, const void *array, size_t arrlen, strget_f * getter,
                        float threshold, const char *match_o[static 1]);

/* srt_string extras */
srt_string *(ss_dec_esc_c) (srt_string * *s);
srt_string *(ss_enc_esc_c) (srt_string * *s);
#include "parts/struct_str_mod.h"
srt_string *(ss_cat_mod) (srt_string * *s, const srt_string * src, const char *name,
                          const struct str_mod mod);

/* vec_string extras */
srt_string *(vstr_join) (const vec_string * sv, int sep);
#define vstr_addstr(v, ...) vstr_addstr_aux(v, __VA_ARGS__, S_INVALID_PTR_VARG_TAIL)
bool (vstr_addstr_aux) (vec_string * *v, const char *c1, ...);

#endif
