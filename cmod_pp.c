
#include <stdio.h>

#ifndef info0
#define info0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <stdbool.h>

#include <unistd.h>

#include "retval.h"
#include "cmod.ext.h"
#include "dsl.h"
#include "util_scanner.h"

/* pp_alloc */
static const struct ret_f0 ss_alloc_ret_f = {
    .name = "ss_alloc"
};

static const struct ret_f0 sv_alloc_ret_f = {
    .name = "sv_alloc"
};

static const struct ret_f0 sv_alloc_t_ret_f = {
    .name = "sv_alloc_t"
};

static const struct ret_f0 shm_alloc_ret_f = {
    .name = "shm_alloc"
};

static const struct ret_f0 sms_alloc_ret_f = {
    .name = "sms_alloc"
};

static const struct ret_f0 sm_alloc_ret_f = {
    .name = "sm_alloc"
};

struct ret_f7 {
    char *name;
    const struct ret_f7 *p[1 + 7];
};

const struct ret_f7 pp_alloc_ret_f = {
    .name = "pp_alloc",
    .p[1] = (const struct ret_f7 *)&rcalloc_ret_f,
    .p[2] = (const struct ret_f7 *)&ss_alloc_ret_f,
    .p[3] = (const struct ret_f7 *)&sv_alloc_ret_f,
    .p[4] = (const struct ret_f7 *)&sv_alloc_t_ret_f,
    .p[5] = (const struct ret_f7 *)&shm_alloc_ret_f,
    .p[6] = (const struct ret_f7 *)&sms_alloc_ret_f,
    .p[7] = (const struct ret_f7 *)&sm_alloc_ret_f,
};

__attribute__((warn_unused_result))
ret_t pp_alloc(struct param *pp_ptr[static 1], bool isptr)
{
    ret_t ret = 0;

    if (isptr) {
        errno = 0;
        *pp_ptr = rcalloc(1, sizeof(**pp_ptr));
        if (NULL == *pp_ptr) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

            goto end;
        }
    }

    struct param *pp = *pp_ptr; // local

    if (NULL == (pp->line = ss_alloc(1024))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 1;

        goto end;
    }
    if (NULL == (pp->kw_buf = ss_alloc(256))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 1;

        goto end;
    }
    if (NULL == (pp->hstr_buf = ss_alloc(32))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 1;

        goto end;
    }

    if (NULL == (pp->sv_tmpfile = sv_alloc_t(SV_PTR, 16))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }

    if (NULL == (pp->sm_prefix_su = sm_alloc(SM_SU, 8))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 6;

        goto end;
    }
    if (NULL == (pp->sm_prefix_us = sm_alloc(SM_US, 8))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 6;

        goto end;
    }

    if (NULL == (pp->sv_dllarg = sv_alloc(sizeof(struct dllarg), 32, NULL))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }

    if (NULL == (pp->sv_snip = sv_alloc(sizeof(struct snippet), 1024, cmp_snippet_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_snip = shm_alloc(SHM_SU, 1024))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }
    if (NULL == (pp->sv_tab = sv_alloc(sizeof(struct table), 1024, cmp_table_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_tab = shm_alloc(SHM_SU, 1024))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }
    if (NULL == (pp->sv_dtype = sv_alloc(sizeof(struct decl), 512, cmp_decl_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_dtype = shm_alloc(SHM_SU, 512))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }
    if (NULL == (pp->sv_dproto = sv_alloc(sizeof(struct decl), 512, cmp_decl_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_dproto = shm_alloc(SHM_SU, 512))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }
    if (NULL == (pp->sv_dsl = sv_alloc(sizeof(struct dsl), 4, cmp_dsl_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_dsl = shm_alloc(SHM_SU, 4))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }
    if (NULL == (pp->sv_fdef = sv_alloc(sizeof(struct fdef), 512, cmp_fdef_by_name))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
    if (NULL == (pp->shm_fdef = shm_alloc(SHM_SU, 512))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 4;

        goto end;
    }

    if (NULL == (pp->sms_once = sms_alloc(SMS_S, 256))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 5;

        goto end;
    }
    if (NULL == (pp->sms_enum = sms_alloc(SMS_S, 512))) {
        ret = RET_ERR_NOMEM;
        ret = ret << 8;
        ret += 1 + 5;

        goto end;
    }

 end:
    return ret;
}

/* pp_free */
static void writerr_unlink(const char *file, bool color)
{
    char buf[1024] = { 0 };
    srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
    if (color)
        ss_cat_c(&(ss), "\x1B[38;5;11m");
    ss_cat_c(&(ss), "[" "C%" "] ", " WARN: ", "failed deleting file \"");

    ss_cat_c(&ss, file, "\": ", strerror(errno));
    if (color)
        ss_cat_c(&ss, "\x1B[39m");
    ss_cat_cn1(&ss, '\n');
    write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
}

void pp_free(struct param *pp, bool isptr)
{
    if (NULL == pp)
        return;

    if (!pp->subparse) {
        if (NULL != pp) {
            /* remove temporary files */
            if (!(pp)->keep_tmp) {
                {
                    const size_t _len = sv_len((pp)->sv_tmpfile);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _0 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        errno = 0;
                        if (unlink((*(const char **)sv_at((pp)->sv_tmpfile, _0))) == -1)        // non-fatal
                            writerr_unlink((*(const char **)sv_at((pp)->sv_tmpfile, _0)),
                                           (pp)->pretty);
                    }
                }
            }

            /* remove output file if not successful run */
            if (!(pp)->success && NULL != (pp)->nameout) {
                errno = 0;
                if (unlink((pp)->nameout) == -1)        // non-fatal
                    writerr_unlink((pp)->nameout, (pp)->pretty);
            }
        }
    }

    /* free memory */
    {
        for (size_t i = 0; i < sv_len(pp->sv_snip); ++i) {
            const void *_item = sv_at(pp->sv_snip, i);
            {
                struct snippet *item = (struct snippet *)_item;
                snippet_clear(item);
            }
        }
        sv_free(&(pp->sv_snip));
    }
    shm_free(&pp->shm_snip);
    {
        for (size_t i = 0; i < sv_len(pp->sv_tab); ++i) {
            const void *_item = sv_at(pp->sv_tab, i);
            {
                struct table *item = (struct table *)_item;
                table_clear(item);
            }
        }
        sv_free(&(pp->sv_tab));
    }
    shm_free(&pp->shm_tab);
    {
        for (size_t i = 0; i < sv_len(pp->sv_dtype); ++i) {
            const void *_item = sv_at(pp->sv_dtype, i);
            {
                struct decl *item = (struct decl *)_item;
                decl_clear(item);
            }
        }
        sv_free(&(pp->sv_dtype));
    }
    shm_free(&pp->shm_dtype);
    {
        for (size_t i = 0; i < sv_len(pp->sv_dproto); ++i) {
            const void *_item = sv_at(pp->sv_dproto, i);
            {
                struct decl *item = (struct decl *)_item;
                decl_clear(item);
            }
        }
        sv_free(&(pp->sv_dproto));
    }
    shm_free(&pp->shm_dproto);
    {
        for (size_t i = 0; i < sv_len(pp->sv_dsl); ++i) {
            const void *_item = sv_at(pp->sv_dsl, i);
            {
                struct dsl *item = (struct dsl *)_item;
                dsl_clear(item);
            }
        }
        sv_free(&(pp->sv_dsl));
    }
    shm_free(&pp->shm_dsl);
    {
        for (size_t i = 0; i < sv_len(pp->sv_fdef); ++i) {
            const void *_item = sv_at(pp->sv_fdef, i);
            {
                struct fdef *item = (struct fdef *)_item;
                fdef_clear(item);
            }
        }
        sv_free(&(pp->sv_fdef));
    }
    shm_free(&pp->shm_fdef);

    sms_free(&pp->sms_once);
    sms_free(&pp->sms_enum);

    sm_free(&pp->sm_prefix_su);
    sm_free(&pp->sm_prefix_us);

    {
        for (size_t i = 0; i < sv_len(pp->sv_ipath); ++i) {
            const void *_item = sv_at(pp->sv_ipath, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(pp->sv_ipath));
    }
    {
        for (size_t i = 0; i < sv_len(pp->sv_tmpfile); ++i) {
            const void *_item = sv_at(pp->sv_tmpfile, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(pp->sv_tmpfile));
    }

    {
        free(pp->namein);
        (pp->namein) = NULL;
    }
    {
        free(pp->nameout);
        (pp->nameout) = NULL;
    }
    {
        free(pp->nameout_tests);
        (pp->nameout_tests) = NULL;
    }

    ss_free(&pp->line);
    ss_free(&pp->kw_buf);
    ss_free(&pp->hstr_buf);

    workspace_clear(pp);

    if (isptr) {
        free(pp);
        (pp) = NULL;
    }
}

void clear_global_state(void)
{
    pp_free(glob_pp, true);
    glob_pp = NULL;
}
