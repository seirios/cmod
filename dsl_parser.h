/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_DSL_DSL_PARSER_H_INCLUDED
# define YY_DSL_DSL_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef DSL_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define DSL_DEBUG 1
#  else
#   define DSL_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define DSL_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined DSL_DEBUG */
#if DSL_DEBUG
extern int dsl_debug;
#endif

/* Token kinds.  */
#ifndef DSL_TOKENTYPE
# define DSL_TOKENTYPE
  enum dsl_tokentype
  {
    DSL_EMPTY = -2,
    DSL_EOF = 0,                   /* "end of file"  */
    DSL_error = 256,               /* error  */
    DSL_UNDEF = 257,               /* "invalid token"  */
    SCANNER_ERROR = 258,           /* SCANNER_ERROR  */
    HERESTR_OPEN = 259,            /* HERESTR_OPEN  */
    HERESTR_CLOSE = 260,           /* HERESTR_CLOSE  */
    STMT_SEP = 261,                /* STMT_SEP  */
    CHAR = 262,                    /* CHAR  */
    IDENTIFIER = 263,              /* IDENTIFIER  */
    IDENTIFIER_EXT = 264,          /* IDENTIFIER_EXT  */
    STRING_LITERAL = 265,          /* STRING_LITERAL  */
    INT_NUMBER = 266,              /* INT_NUMBER  */
    FP_NUMBER = 267,               /* FP_NUMBER  */
    TREE_LEVEL = 268,              /* TREE_LEVEL  */
    OP_QUOTE = 269,                /* OP_QUOTE  */
    OP_NAMESPACE = 270,            /* OP_NAMESPACE  */
    OP_SEPARATOR = 271,            /* OP_SEPARATOR  */
    OP_CONTAINER_OPEN = 272,       /* OP_CONTAINER_OPEN  */
    OP_CONTAINER_CLOSE = 273,      /* OP_CONTAINER_CLOSE  */
    OP_FUNCTION = 274,             /* OP_FUNCTION  */
    OP_INDEX_OPEN = 275,           /* OP_INDEX_OPEN  */
    OP_INDEX_CLOSE = 276,          /* OP_INDEX_CLOSE  */
    OP_POSTFIX = 277,              /* OP_POSTFIX  */
    OP_CHAIN_LEFT = 278,           /* OP_CHAIN_LEFT  */
    OP_CHAIN_RIGHT = 279,          /* OP_CHAIN_RIGHT  */
    OP_UNARY = 280,                /* OP_UNARY  */
    OP_MULTIPLICATIVE = 281,       /* OP_MULTIPLICATIVE  */
    OP_ADDITIVE = 282,             /* OP_ADDITIVE  */
    OP_SHIFT = 283,                /* OP_SHIFT  */
    OP_RELATIONAL = 284,           /* OP_RELATIONAL  */
    OP_EQUALITY = 285,             /* OP_EQUALITY  */
    OP_BITWISE_AND = 286,          /* OP_BITWISE_AND  */
    OP_BITWISE_XOR = 287,          /* OP_BITWISE_XOR  */
    OP_BITWISE_OR = 288,           /* OP_BITWISE_OR  */
    OP_LOGICAL_AND = 289,          /* OP_LOGICAL_AND  */
    OP_LOGICAL_OR = 290,           /* OP_LOGICAL_OR  */
    OP_ASSIGNMENT = 291,           /* OP_ASSIGNMENT  */
    OP_END = 292,                  /* OP_END  */
    OP_BLOCK_OPEN = 293,           /* OP_BLOCK_OPEN  */
    OP_BLOCK_CLOSE = 294,          /* OP_BLOCK_CLOSE  */
    OP_COMMENT_EOL = 295,          /* OP_COMMENT_EOL  */
    OP_COMMENT = 296               /* OP_COMMENT  */
  };
  typedef enum dsl_tokentype dsl_token_kind_t;
#endif

/* Value type.  */
#if ! defined DSL_STYPE && ! defined DSL_STYPE_IS_DECLARED
union DSL_STYPE
{
#line 187 "dsl_parser.y"

    char c;
    char* txt;
    srt_string* str;
    struct dsl_ast node;
    srt_vector* vdast;
    uint64_t ix;

#line 122 "dsl_parser.h"

};
typedef union DSL_STYPE DSL_STYPE;
# define DSL_STYPE_IS_TRIVIAL 1
# define DSL_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined DSL_LTYPE && ! defined DSL_LTYPE_IS_DECLARED
typedef struct DSL_LTYPE DSL_LTYPE;
struct DSL_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define DSL_LTYPE_IS_DECLARED 1
# define DSL_LTYPE_IS_TRIVIAL 1
#endif




int dsl_parse (void* yyscanner, struct dsl_param *pp);

/* "%code provides" blocks.  */
#line 16 "dsl_parser.y"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>


#include <stdio.h>

#ifndef info0
#define info0(M) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((pp->silent > 3) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((pp->silent > 3) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif

#define YYSTYPE DSL_STYPE
#define YYLTYPE DSL_LTYPE
#define yyget_extra dsl_get_extra

__attribute__ ((format(printf,4,5)))
extern void dsl_info(YYLTYPE*, void*, const struct dsl_param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
extern void dsl_warn(YYLTYPE*, void*, const struct dsl_param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
extern void dsl_error(YYLTYPE*, void*, const struct dsl_param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
extern void dsl_verbose(YYLTYPE*, void*, const struct dsl_param*, const char*, ...);

#line 235 "dsl_parser.h"

#endif /* !YY_DSL_DSL_PARSER_H_INCLUDED  */
