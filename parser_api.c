#ifndef CMOD_YYLVAL_H
#define CMOD_YYLVAL_H
#include "parts/struct_range.h"
#include "parts/struct_table_like.h"
#include "parts/struct_recall_like.h"
#include "parts/variant_c_typespec.h"
#include "parts/variant_c_specifier.h"
#endif

#include <stdio.h>

#ifndef info0
#define info0(M) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include <stdbool.h>

#include <unistd.h>
#include <libgen.h>
#include <assert.h>
#include <sys/select.h>
#include <sys/wait.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <strings.h>
#endif
#ifndef _GNU_SOURCE
extern char **environ;  // not exported by default
#endif

#include "cmod.ext.h"

#include "parser.h"
#include "scanner.h"
#include "parser_api.h"

#include "filestack.h"
#include "table_json.h"
#include "table_tsv.h"
#include "table_parse.h"
#include "util_string.h"
#include "util_snippet.h"

#include "parts/struct_dsl_param.h"
#include "dsl.ext.h"

#define JSMN_HEADER
#include "jsmn.h"

#define YYACCEPT { _ret = 1; goto cleanup; }
#define YYABORT { _ret = -1; goto cleanup; }
#define YYNOMEM { _ret = -2; goto cleanup; }
#define YYERROR { _ret = -3; goto cleanup; }

/* logging macros */
#ifndef INFO0
#define INFO0(x) \
    { if(!(glob_pp->silent > 0))\
        yyinfo(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : ""); }
#endif
#ifndef INFO
#define INFO(x, ...) \
    { if(!(glob_pp->silent > 0))\
        yyinfo(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",__VA_ARGS__); }
#endif
#ifndef WARN0
#define WARN0(x) \
    { if(!(glob_pp->silent > 1))\
        yywarn(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : ""); }
#endif
#ifndef WARN
#define WARN(x, ...) \
    { if(!(glob_pp->silent > 1))\
        yywarn(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",__VA_ARGS__); }
#endif
#ifndef ERROR0
#define ERROR0(x) \
    { if(!(glob_pp->silent > 2))\
        yyerror(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : ""); }
#endif
#ifndef ERROR
#define ERROR(x, ...) \
    { if(!(glob_pp->silent > 2))\
        yyerror(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",__VA_ARGS__); }
#endif
#ifndef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!glob_pp->verbose))\
        yyverbose(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : ""); }
#endif
#ifndef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!glob_pp->verbose))\
        yyverbose(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",__VA_ARGS__); }
#endif
#ifndef DEBUG0
#define DEBUG0(x) \
    { if(!(glob_pp->silent > 3))\
        yydebug(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : ""); }
#endif
#ifndef DEBUG
#define DEBUG(x, ...) \
    { if(!(glob_pp->silent > 3))\
        yydebug(NULL,yyscanner,NULL,"%s%s%s: " x,\
        pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",__VA_ARGS__); }
#endif

#ifndef KEYWORD_ERROR
#define KEYWORD_ERROR(x, ...) { \
    ERROR(x,__VA_ARGS__); \
if(!(glob_pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYERROR; }
#endif
#ifndef KEYWORD_ERROR0
#define KEYWORD_ERROR0(x) { \
    ERROR0(x); \
if(!(glob_pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYERROR; }
#endif

#ifndef ABORT_PARSE
#define ABORT_PARSE(x, ...) { \
    ERROR(x,__VA_ARGS__); \
if(!(glob_pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif
#ifndef ABORT_PARSE0
#define ABORT_PARSE0(x) { \
    ERROR0(x); \
if(!(glob_pp->silent > 2)) { \
    struct param *mutpp = yyget_extra(yyscanner); \
    print_error(&(mutpp->errlloc),yyscanner); \
    mutpp->errtxt = NULL; \
} \
    YYABORT; }
#endif

#ifndef BUFPUTC
#define BUFPUTC(x) ss_cat_cn1(kwbuf,x)
#endif
#ifndef BUFPUTS
#define BUFPUTS(...) ss_cat_c(kwbuf, __VA_ARGS__)
#endif
#ifndef BUFCAT
#define BUFCAT(x) ss_cat(kwbuf, x)
#endif
#ifndef BUFCATN
#define BUFCATN(x, n) ss_cat_cn(kwbuf, x, n)
#endif
#ifndef BUFPUTINT
#define BUFPUTINT(x) ss_cat_int(kwbuf, x)
#endif

#ifndef PRINTTEST0
#define PRINTTEST0(x) (NULL != pp->fpout_tests) ? (void)fputs(x,pp->fpout_tests) : (void)0
#endif
#ifndef PRINTTEST
#define PRINTTEST(x, ...) (NULL != pp->fpout_tests) ? (void)fprintf(pp->fpout_tests,x, __VA_ARGS__) : (void)0
#endif

/* child process logging macros */

#include <stdio.h>

#ifndef chld_info0
#define chld_info0(M) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef chld_warn0
#define chld_warn0(M) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef chld_error0
#define chld_error0(M) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef chld_verbose0
#define chld_verbose0(M) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef chld_debug0
#define chld_debug0(M) ((true) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef chld_info
#define chld_info(M,...) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef chld_warn
#define chld_warn(M,...) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef chld_error
#define chld_error(M,...) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef chld_verbose
#define chld_verbose(M,...) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef chld_debug
#define chld_debug(M,...) ((true) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif

#ifndef CHLD_INFO0
#define CHLD_INFO0(x) \
    chld_info("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid);
#endif
#ifndef CHLD_INFO
#define CHLD_INFO(x, ...) \
    chld_info("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid,__VA_ARGS__);
#endif
#ifndef CHLD_WARN0
#define CHLD_WARN0(x) \
    chld_warn("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid);
#endif
#ifndef CHLD_WARN
#define CHLD_WARN(x, ...) \
    chld_warn("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid,__VA_ARGS__);
#endif
#ifndef CHLD_ERROR0
#define CHLD_ERROR0(x) \
    chld_error("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid);
#endif
#ifndef CHLD_ERROR
#define CHLD_ERROR(x, ...) \
    chld_error("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid,__VA_ARGS__);
#endif
#ifndef CHLD_VERBOSE0
#define CHLD_VERBOSE0(x) \
    chld_verbose("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid);
#endif
#ifndef CHLD_VERBOSE
#define CHLD_VERBOSE(x, ...) \
    chld_verbose("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid,__VA_ARGS__);
#endif
#ifndef CHLD_DEBUG0
#define CHLD_DEBUG0(x) \
    chld_debug("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid);
#endif
#ifndef CHLD_DEBUG
#define CHLD_DEBUG(x, ...) \
    chld_debug("%s:%d:%d: %s%s%s: child PID %d " x,\
    pp->namein,pp->kwlloc.first_line,pp->kwlloc.first_column,\
    pp->pretty ? "\x1B[1m" : "",pp->kwname,pp->pretty ? "\x1B[22m" : "",cpid,__VA_ARGS__);
#endif

#ifndef CHLD_FATAL_ERROR
#define CHLD_FATAL_ERROR(x, ...) { CHLD_VERBOSE(x,__VA_ARGS__); _Exit(EXIT_FAILURE); }
#endif
#ifndef CHLD_FATAL_ERROR0
#define CHLD_FATAL_ERROR0(x) { CHLD_VERBOSE0(x); _Exit(EXIT_FAILURE); }
#endif

/* static option name arrays per keyword */
static const char *include_opt_names[] = { "debug", "sys", "blank", "quiet", "opt", "c" };

static int include_opt_compat(const struct include_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int include_opt_string(const struct include_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->sys_isset + opts->blank_isset + opts->quiet_isset +
        opts->opt_isset + opts->c_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->sys_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sys");
        if (!opts->sys_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->blank_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "blank");
        if (!opts->blank_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->quiet_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "quiet");
        if (!opts->quiet_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->c_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "c");
        if (!opts->c_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *once_opt_names[] = { "debug", "noskip" };

static int once_opt_compat(const struct once_cmod_opts *const opts, int icuropt[static 1],
                           int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int once_opt_string(const struct once_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset + opts->noskip_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->noskip_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "noskip");
        if (!opts->noskip_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *snippet_opt_names[] =
    { "debug", "opt", "redef", "append", "here", "now", "nl2sp" };

static int snippet_opt_compat(const struct snippet_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 1;
        }
    }
    if (opts->append_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    return 0;
}

static int snippet_opt_string(const struct snippet_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->opt_isset + opts->redef_isset + opts->append_isset +
        opts->here_isset + opts->now_isset + opts->nl2sp_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->redef_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "redef");
        if (!opts->redef_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->append_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "append");
        if (!opts->append_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->here_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "here");
        if (!opts->here_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->now_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "now");
        if (!opts->now_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->nl2sp_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nl2sp");
        if (!opts->nl2sp_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *recall_opt_names[] = { "debug", "str", "empty", "opt", "lazy" };

static int recall_opt_compat(const struct recall_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    return 0;
}

static int recall_opt_string(const struct recall_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->empty_isset + opts->opt_isset +
        opts->lazy_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->empty_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "empty");
        if (!opts->empty_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *table_opt_names[] = { "debug", "opt", "redef", "json", "tsv", "append", "hash" };

static int table_opt_compat(const struct table_cmod_opts *const opts, int icuropt[static 1],
                            int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->json_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->tsv_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    if (opts->opt_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 1;
        }
    }
    if (opts->append_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 5;
        }
    }
    return 0;
}

static int table_opt_string(const struct table_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->opt_isset + opts->redef_isset + opts->json_isset +
        opts->tsv_isset + opts->append_isset + opts->hash_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->redef_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "redef");
        if (!opts->redef_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->json_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "json");
        if (!opts->json_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->tsv_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "tsv");
        if (!opts->tsv_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->append_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "append" "=");
        ss_cat_int(s, opts->append_index.ix);
    };
    if (opts->hash_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "hash" "=");
        switch (opts->hash_column->type) {
        case CMOD_OPTION_TYPE_XINT:
            ss_cat_int(s, XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opts->hash_column)));
            break;
        case CMOD_OPTION_TYPE_STRING:
            ss_cat_cn1(s, '#');
            ss_cat(s, CMOD_OPTION_STRING_get(*opts->hash_column));
            break;
        default:
            return 1;
            break;
        }
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *table_stack_opt_names[] =
    { "debug", "opt", "lazy", "rows", "cols", "v", "h", "redef", "common", "rename", "hash" };

static int table_stack_opt_compat(const struct table_stack_cmod_opts *const opts,
                                  int icuropt[static 1], int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 1;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->rows_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 3;
        }
    }
    if (opts->cols_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 4;
        }
    }
    return 0;
}

static int table_stack_opt_string(const struct table_stack_cmod_opts *const opts,
                                  srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->opt_isset + opts->lazy_isset + opts->rows_isset +
        opts->cols_isset + opts->v_isset + opts->h_isset + opts->redef_isset + opts->common_isset +
        opts->rename_isset + opts->hash_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->rows_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rows");
        if (!opts->rows_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->cols_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "cols");
        if (!opts->cols_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->v_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "v");
        if (!opts->v_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->h_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "h");
        if (!opts->h_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->redef_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "redef");
        if (!opts->redef_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->common_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "common");
        if (!opts->common_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->rename_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rename");
        if (!opts->rename_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->hash_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "hash" "=");
        switch (opts->hash_column->type) {
        case CMOD_OPTION_TYPE_XINT:
            ss_cat_int(s, XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opts->hash_column)));
            break;
        case CMOD_OPTION_TYPE_STRING:
            ss_cat_cn1(s, '#');
            ss_cat(s, CMOD_OPTION_STRING_get(*opts->hash_column));
            break;
        default:
            return 1;
            break;
        }
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *map_opt_names[] =
    { "debug", "str", "opt", "lazy", "ord", "add1", "sep", "brk", "nbrk", "x", "nl2sp", "rev",
"num", "inv", "sort", "uniq", "notnull", "nohead", "notail", "head", "tail" };

static int map_opt_compat(const struct map_cmod_opts *const opts, int icuropt[static 1],
                          int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    return 0;
}

static int map_opt_string(const struct map_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->opt_isset + opts->lazy_isset + opts->ord_isset +
        opts->add1_isset + opts->sep_isset + opts->brk_isset + opts->nbrk_isset + opts->x_isset +
        opts->nl2sp_isset + opts->rev_isset + opts->num_isset + opts->inv_isset + opts->sort_isset +
        opts->uniq_isset + opts->notnull_isset + opts->nohead_isset + opts->notail_isset +
        opts->head_isset + opts->tail_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ord_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ord");
        if (!opts->ord_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->add1_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "add1" "=");
        ss_cat_int(s, opts->add1_count);
    };
    if (opts->sep_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sep" "=`");
        ss_cat(s, opts->sep_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->brk_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "brk" "=`");
        ss_cat(s, opts->brk_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->nbrk_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nbrk" "=");
        ss_cat_int(s, opts->nbrk_index.ix);
    };
    if (opts->x_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "x");
        if (!opts->x_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->nl2sp_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nl2sp");
        if (!opts->nl2sp_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->rev_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rev");
        if (!opts->rev_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->num_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "num" "=");
        ss_cat_int(s, opts->num_index.ix);
    };
    if (opts->inv_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "inv");
        if (!opts->inv_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->sort_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sort" "=");
        switch (opts->sort_column->type) {
        case CMOD_OPTION_TYPE_XINT:
            ss_cat_int(s, XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opts->sort_column)));
            break;
        case CMOD_OPTION_TYPE_STRING:
            ss_cat_cn1(s, '#');
            ss_cat(s, CMOD_OPTION_STRING_get(*opts->sort_column));
            break;
        default:
            return 1;
            break;
        }
    };
    if (opts->uniq_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "uniq" "=");
        switch (opts->uniq_column->type) {
        case CMOD_OPTION_TYPE_XINT:
            ss_cat_int(s, XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opts->uniq_column)));
            break;
        case CMOD_OPTION_TYPE_STRING:
            ss_cat_cn1(s, '#');
            ss_cat(s, CMOD_OPTION_STRING_get(*opts->uniq_column));
            break;
        default:
            return 1;
            break;
        }
    };
    if (opts->notnull_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "notnull" "=");
        switch (opts->notnull_column->type) {
        case CMOD_OPTION_TYPE_XINT:
            ss_cat_int(s, XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opts->notnull_column)));
            break;
        case CMOD_OPTION_TYPE_STRING:
            ss_cat_cn1(s, '#');
            ss_cat(s, CMOD_OPTION_STRING_get(*opts->notnull_column));
            break;
        default:
            return 1;
            break;
        }
    };
    if (opts->nohead_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nohead" "=");
        ss_cat_int(s, opts->nohead_count);
    };
    if (opts->notail_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "notail" "=");
        ss_cat_int(s, opts->notail_count);
    };
    if (opts->head_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "head" "=");
        ss_cat_int(s, opts->head_count);
    };
    if (opts->tail_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "tail" "=");
        ss_cat_int(s, opts->tail_count);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *pipe_opt_names[] =
    { "debug", "str", "wait", "env", "allow", "unsafe", "strict", "trace", "keepenv" };

static int pipe_opt_compat(const struct pipe_cmod_opts *const opts, int icuropt[static 1],
                           int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->unsafe_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 5;
        }
    }
    if (opts->strict_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 6;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 6;
        }
    }
    return 0;
}

static int pipe_opt_string(const struct pipe_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->wait_isset + opts->env_isset +
        opts->allow_isset + opts->unsafe_isset + opts->strict_isset + opts->trace_isset +
        opts->keepenv_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->wait_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "wait" "=");
        ss_cat_int(s, opts->wait_index.ix);
    };
    if (opts->env_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->env_strl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "env" "=`", (char *)sv_at_ptr(opts->env_strl, i), "`");
        }
    };
    if (opts->allow_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->allow_cidl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "allow" "=", (char *)sv_at_ptr(opts->allow_cidl, i));
        }
    };
    if (opts->unsafe_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "unsafe");
        if (!opts->unsafe_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->strict_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "strict");
        if (!opts->strict_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->trace_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "trace");
        if (!opts->trace_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->keepenv_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "keepenv");
        if (!opts->keepenv_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *unittest_opt_names[] =
    { "debug", "suite", "init", "deinit", "signal", "status", "timeout", "disabled", "verbatim" };

static int unittest_opt_compat(const struct unittest_cmod_opts *const opts, int icuropt[static 1],
                               int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int unittest_opt_string(const struct unittest_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->suite_isset + opts->init_isset + opts->deinit_isset +
        opts->signal_isset + opts->status_isset + opts->timeout_isset + opts->disabled_isset +
        opts->verbatim_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->suite_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "suite" "=");
        ss_cat(s, opts->suite_cid);
    };
    if (opts->init_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "init" "=");
        ss_cat(s, opts->init_cid);
    };
    if (opts->deinit_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "deinit" "=");
        ss_cat(s, opts->deinit_cid);
    };
    if (opts->signal_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "signal" "=");
        ss_cat(s, opts->signal_cid);
    };
    if (opts->status_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "status" "=");
        ss_cat_int(s, opts->status_int);
    };
    if (opts->timeout_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "timeout" "=");
        ss_cat_int(s, opts->timeout_count);
    };
    if (opts->disabled_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "disabled");
        if (!opts->disabled_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->verbatim_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "verbatim");
        if (!opts->verbatim_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *dsl_def_opt_names[] = { "debug", "syntax", "altsep", "extend", "maxeval" };

static int dsl_def_opt_compat(const struct dsl_def_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int dsl_def_opt_string(const struct dsl_def_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->syntax_isset + opts->altsep_isset + opts->extend_isset +
        opts->maxeval_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->syntax_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "syntax" "=");
        ss_cat(s, opts->syntax_cid);
    };
    if (opts->altsep_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "altsep");
        if (!opts->altsep_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->extend_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "extend");
        if (!opts->extend_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->maxeval_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "maxeval" "=");
        ss_cat_int(s, opts->maxeval_index.ix);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *dsl_opt_names[] = { "debug", "str", "sep" };

static int dsl_opt_compat(const struct dsl_cmod_opts *const opts, int icuropt[static 1],
                          int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int dsl_opt_string(const struct dsl_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset + opts->str_isset + opts->sep_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->sep_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sep" "=`");
        ss_cat(s, opts->sep_str);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *intop_opt_names[] =
    { "sum", "prod", "min", "max", "add", "mul", "sub", "div", "mod", "pow", "debug", "str",
"unsigned", "sign" };

static int intop_opt_compat(const struct intop_cmod_opts *const opts, int icuropt[static 1],
                            int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->sum_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 0;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 0;
        }
    }
    if (opts->prod_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 1;
        }
    }
    if (opts->min_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->max_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->add_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    if (opts->mul_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 5;
        }
    }
    if (opts->sub_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 6;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 6;
        }
    }
    if (opts->div_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 7;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 7;
        }
    }
    if (opts->mod_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 8;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 8;
        }
    }
    if (opts->pow_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 9;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 9;
        }
    }
    return 0;
}

static int intop_opt_string(const struct intop_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->sum_isset + opts->prod_isset + opts->min_isset + opts->max_isset + opts->add_isset +
        opts->mul_isset + opts->sub_isset + opts->div_isset + opts->mod_isset + opts->pow_isset +
        opts->debug_isset + opts->str_isset + opts->unsigned_isset + opts->sign_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->sum_isset && ++count) {

        ss_cat_c(s, "sum");
        if (!opts->sum_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->prod_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "prod");
        if (!opts->prod_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->min_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "min");
        if (!opts->min_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->max_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "max");
        if (!opts->max_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->add_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "add");
        if (!opts->add_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->mul_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "mul");
        if (!opts->mul_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->sub_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sub");
        if (!opts->sub_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->div_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "div");
        if (!opts->div_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->mod_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "mod");
        if (!opts->mod_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->pow_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "pow");
        if (!opts->pow_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->debug_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->unsigned_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "unsigned");
        if (!opts->unsigned_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->sign_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sign");
        if (!opts->sign_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *delay_opt_names[] = { "debug", "str" };

static int delay_opt_compat(const struct delay_cmod_opts *const opts, int icuropt[static 1],
                            int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int delay_opt_string(const struct delay_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset + opts->str_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *defined_opt_names[] =
    { "snippet", "table", "type", "proto", "dsl", "fdef", "once", "enum", "debug", "str", "not",
"any", "all", "attr" };

static int defined_opt_compat(const struct defined_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->snippet_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 0;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 0;
        }
    }
    if (opts->table_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 1;
        }
    }
    if (opts->type_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->proto_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->dsl_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    if (opts->fdef_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 5;
        }
    }
    if (opts->once_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 6;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 6;
        }
    }
    if (opts->enum_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 7;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 7;
        }
    }
    if (opts->any_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 11;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 11;
        }
    }
    if (opts->all_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 12;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 12;
        }
    }
    return 0;
}

static int defined_opt_string(const struct defined_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->snippet_isset + opts->table_isset + opts->type_isset + opts->proto_isset +
        opts->dsl_isset + opts->fdef_isset + opts->once_isset + opts->enum_isset +
        opts->debug_isset + opts->str_isset + opts->not_isset + opts->any_isset + opts->all_isset +
        opts->attr_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->snippet_isset && ++count) {

        ss_cat_c(s, "snippet");
        if (!opts->snippet_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->table_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "table");
        if (!opts->table_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->type_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "type");
        if (!opts->type_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->proto_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "proto");
        if (!opts->proto_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->dsl_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "dsl");
        if (!opts->dsl_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->fdef_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "fdef");
        if (!opts->fdef_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->once_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "once");
        if (!opts->once_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->enum_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "enum");
        if (!opts->enum_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->debug_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->not_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "not");
        if (!opts->not_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->any_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "any");
        if (!opts->any_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->all_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "all");
        if (!opts->all_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->attr_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "attr" "=`");
        ss_cat(s, opts->attr_idext);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *strcmp_opt_names[] =
    { "debug", "str", "not", "unsigned", "icase", "regex", "ere", "newline", "whole", "nchar", "eq",
"ne", "le", "lt", "gt", "ge" };

static int strcmp_opt_compat(const struct strcmp_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->regex_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 5;
        }
    }
    if (opts->eq_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 10;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 10;
        }
    }
    if (opts->ne_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 11;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 11;
        }
    }
    if (opts->le_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 12;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 12;
        }
    }
    if (opts->lt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 13;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 13;
        }
    }
    if (opts->gt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 14;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 14;
        }
    }
    if (opts->ge_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 15;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 15;
        }
    }
    return 0;
}

static int strcmp_opt_string(const struct strcmp_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->not_isset + opts->unsigned_isset +
        opts->icase_isset + opts->regex_isset + opts->ere_isset + opts->newline_isset +
        opts->whole_isset + opts->nchar_isset + opts->eq_isset + opts->ne_isset + opts->le_isset +
        opts->lt_isset + opts->gt_isset + opts->ge_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->not_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "not");
        if (!opts->not_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->unsigned_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "unsigned");
        if (!opts->unsigned_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->icase_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "icase");
        if (!opts->icase_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->regex_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "regex");
        if (!opts->regex_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ere_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ere");
        if (!opts->ere_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->newline_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "newline");
        if (!opts->newline_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->whole_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "whole");
        if (!opts->whole_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->nchar_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nchar" "=");
        ss_cat_int(s, opts->nchar_count);
    };
    if (opts->eq_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "eq");
        if (!opts->eq_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ne_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ne");
        if (!opts->ne_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->le_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "le");
        if (!opts->le_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lt");
        if (!opts->lt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->gt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "gt");
        if (!opts->gt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ge_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ge");
        if (!opts->ge_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *strstr_opt_names[] =
    { "debug", "str", "icase", "regex", "ere", "newline", "match", "submatch" };

static int strstr_opt_compat(const struct strstr_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int strstr_opt_string(const struct strstr_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->icase_isset + opts->regex_isset +
        opts->ere_isset + opts->newline_isset + opts->match_isset + opts->submatch_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->icase_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "icase");
        if (!opts->icase_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->regex_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "regex");
        if (!opts->regex_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ere_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ere");
        if (!opts->ere_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->newline_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "newline");
        if (!opts->newline_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->match_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "match" "=");
        ss_cat_int(s, opts->match_index.ix);
    };
    if (opts->submatch_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "submatch" "=");
        ss_cat_int(s, opts->submatch_index.ix);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *strlen_opt_names[] = { "debug", "str", "add1", "escaped", "utf8" };

static int strlen_opt_compat(const struct strlen_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int strlen_opt_string(const struct strlen_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->add1_isset + opts->escaped_isset +
        opts->utf8_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->add1_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "add1" "=");
        ss_cat_int(s, opts->add1_count);
    };
    if (opts->escaped_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "escaped");
        if (!opts->escaped_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->utf8_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "utf8");
        if (!opts->utf8_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *strsub_opt_names[] =
    { "debug", "str", "g", "global", "cat", "sep", "icase", "regex", "ere", "newline", "whole",
"byline", "escaped" };

static int strsub_opt_compat(const struct strsub_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int strsub_opt_string(const struct strsub_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->g_isset + opts->global_isset + opts->cat_isset +
        opts->sep_isset + opts->icase_isset + opts->regex_isset + opts->ere_isset +
        opts->newline_isset + opts->whole_isset + opts->byline_isset + opts->escaped_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->g_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "g");
        if (!opts->g_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->global_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "global");
        if (!opts->global_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->cat_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "cat");
        if (!opts->cat_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->sep_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "sep" "=`");
        ss_cat(s, opts->sep_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->icase_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "icase");
        if (!opts->icase_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->regex_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "regex");
        if (!opts->regex_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ere_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ere");
        if (!opts->ere_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->newline_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "newline");
        if (!opts->newline_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->whole_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "whole");
        if (!opts->whole_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->byline_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "byline");
        if (!opts->byline_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->escaped_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "escaped");
        if (!opts->escaped_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *table_size_opt_names[] =
    { "debug", "str", "opt", "lazy", "rows", "cols", "add1" };

static int table_size_opt_compat(const struct table_size_cmod_opts *const opts,
                                 int icuropt[static 1], int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->rows_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 4;
        }
    }
    if (opts->cols_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 5;
        }
    }
    return 0;
}

static int table_size_opt_string(const struct table_size_cmod_opts *const opts,
                                 srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->opt_isset + opts->lazy_isset +
        opts->rows_isset + opts->cols_isset + opts->add1_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->rows_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rows");
        if (!opts->rows_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->cols_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "cols");
        if (!opts->cols_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->add1_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "add1" "=");
        ss_cat_int(s, opts->add1_count);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *table_length_opt_names[] =
    { "debug", "str", "opt", "lazy", "max", "min", "add1" };

static int table_length_opt_compat(const struct table_length_cmod_opts *const opts,
                                   int icuropt[static 1], int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->max_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 4;
        }
    }
    if (opts->min_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 5;
        }
    }
    return 0;
}

static int table_length_opt_string(const struct table_length_cmod_opts *const opts,
                                   srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->opt_isset + opts->lazy_isset + opts->max_isset +
        opts->min_isset + opts->add1_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->max_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "max");
        if (!opts->max_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->min_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "min");
        if (!opts->min_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->add1_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "add1" "=");
        ss_cat_int(s, opts->add1_count);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *table_get_opt_names[] =
    { "debug", "str", "opt", "lazy", "icase", "regex", "ere", "strict" };

static int table_get_opt_compat(const struct table_get_cmod_opts *const opts, int icuropt[static 1],
                                int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->opt_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 2;
        }
    }
    if (opts->lazy_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    return 0;
}

static int table_get_opt_string(const struct table_get_cmod_opts *const opts,
                                srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->str_isset + opts->opt_isset + opts->lazy_isset +
        opts->icase_isset + opts->regex_isset + opts->ere_isset + opts->strict_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->str_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "str" "=`");
        ss_cat(s, opts->str_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->lazy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "lazy");
        if (!opts->lazy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->icase_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "icase");
        if (!opts->icase_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->regex_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "regex");
        if (!opts->regex_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->ere_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ere");
        if (!opts->ere_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->strict_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "strict");
        if (!opts->strict_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *typedef_opt_names[] =
    { "debug", "quiet", "named", "opt", "argv", "suffix", "begin", "end" };

static int typedef_opt_compat(const struct typedef_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int typedef_opt_string(const struct typedef_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->quiet_isset + opts->named_isset + opts->opt_isset +
        opts->argv_isset + opts->suffix_isset + opts->begin_isset + opts->end_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->quiet_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "quiet");
        if (!opts->quiet_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->named_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "named");
        if (!opts->named_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->argv_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "argv" "=");
        ss_cat(s, opts->argv_cid);
    };
    if (opts->suffix_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "suffix" "=");
        ss_cat(s, opts->suffix_cid);
    };
    if (opts->begin_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->begin_idextl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "begin" "=`", (char *)sv_at_ptr(opts->begin_idextl, i), "`");
        }
    };
    if (opts->end_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->end_idextl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "end" "=`", (char *)sv_at_ptr(opts->end_idextl, i), "`");
        }
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *proto_opt_names[] =
    { "debug", "quiet", "named", "opt", "argv", "suffix", "begin", "end" };

static int proto_opt_compat(const struct proto_cmod_opts *const opts, int icuropt[static 1],
                            int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int proto_opt_string(const struct proto_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->quiet_isset + opts->named_isset + opts->opt_isset +
        opts->argv_isset + opts->suffix_isset + opts->begin_isset + opts->end_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->quiet_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "quiet");
        if (!opts->quiet_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->named_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "named");
        if (!opts->named_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->opt_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "opt");
        if (!opts->opt_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->argv_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "argv" "=");
        ss_cat(s, opts->argv_cid);
    };
    if (opts->suffix_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "suffix" "=");
        ss_cat(s, opts->suffix_cid);
    };
    if (opts->begin_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->begin_idextl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "begin" "=`", (char *)sv_at_ptr(opts->begin_idextl, i), "`");
        }
    };
    if (opts->end_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        for (size_t i = 0; i < sv_len(opts->end_idextl); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            ss_cat_c(s, "end" "=`", (char *)sv_at_ptr(opts->end_idextl, i), "`");
        }
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *def_opt_names[] = { "debug", "now", "nocheck", "const", "return" };

static int def_opt_compat(const struct def_cmod_opts *const opts, int icuropt[static 1],
                          int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int def_opt_string(const struct def_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->now_isset + opts->nocheck_isset + opts->const_isset +
        opts->return_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->now_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "now");
        if (!opts->now_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->nocheck_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nocheck");
        if (!opts->nocheck_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->const_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "const");
        if (!opts->const_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->return_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "return" "=`");
        ss_cat(s, opts->return_str);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *unused_opt_names[] = { "debug" };

static int unused_opt_compat(const struct unused_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int unused_opt_string(const struct unused_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *prefix_opt_names[] = { "debug", "unset" };

static int prefix_opt_compat(const struct prefix_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int prefix_opt_string(const struct prefix_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset + opts->unset_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->unset_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "unset");
        if (!opts->unset_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *enum_opt_names[] =
    { "debug", "helper", "upper", "bitflag", "rev", "shift", "default", "prefix" };

static int enum_opt_compat(const struct enum_cmod_opts *const opts, int icuropt[static 1],
                           int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->rev_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    if (opts->helper_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 1;
        }
    }
    if (opts->bitflag_isset) {
        if (((optgrp) & (03UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 03;
            *icuropt = 3;
        }
    }
    return 0;
}

static int enum_opt_string(const struct enum_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->helper_isset + opts->upper_isset + opts->bitflag_isset +
        opts->rev_isset + opts->shift_isset + opts->default_isset + opts->prefix_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->helper_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "helper" "=");
        ss_cat_int(s, opts->helper_index.ix);
    };
    if (opts->upper_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "upper");
        if (!opts->upper_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->bitflag_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "bitflag");
        if (!opts->bitflag_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->rev_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rev");
        if (!opts->rev_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->shift_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "shift" "=");
        ss_cat_int(s, opts->shift_int);
    };
    if (opts->default_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "default" "=");
        ss_cat(s, opts->default_cid);
    };
    if (opts->prefix_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "prefix" "=`");
        ss_cat(s, opts->prefix_str);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *strin_opt_names[] = { "debug", "expand", "fuzzy", "bysize", "none", "nomatch" };

static int strin_opt_compat(const struct strin_cmod_opts *const opts, int icuropt[static 1],
                            int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int strin_opt_string(const struct strin_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->expand_isset + opts->fuzzy_isset + opts->bysize_isset +
        opts->none_isset + opts->nomatch_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->expand_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "expand" "=");
        ss_cat_int(s, opts->expand_index.ix);
    };
    if (opts->fuzzy_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "fuzzy");
        if (!opts->fuzzy_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->bysize_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "bysize");
        if (!opts->bysize_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->none_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "none" "=`");
        ss_cat(s, opts->none_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->nomatch_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "nomatch" "=`");
        ss_cat(s, opts->nomatch_idext);
        ss_cat_cn1(s, '`');
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *foreach_opt_names[] =
    { "debug", "par", "const", "autoarr", "svec", "ptr", "deref", "skip", "rev" };

static int foreach_opt_compat(const struct foreach_cmod_opts *const opts, int icuropt[static 1],
                              int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->autoarr_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->svec_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 4;
        }
    }
    if (opts->ptr_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 5;
        }
    }
    return 0;
}

static int foreach_opt_string(const struct foreach_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->debug_isset + opts->par_isset + opts->const_isset + opts->autoarr_isset +
        opts->svec_isset + opts->ptr_isset + opts->deref_isset + opts->skip_isset + opts->rev_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->par_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "par");
        if (!opts->par_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->const_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "const");
        if (!opts->const_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->autoarr_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "autoarr");
        if (!opts->autoarr_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->svec_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "svec" "=`");
        ss_cat(s, opts->svec_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->ptr_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "ptr" "=`");
        ss_cat(s, opts->ptr_str);
        ss_cat_cn1(s, '`');
    };
    if (opts->deref_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "deref" "=");
        ss_cat_int(s, opts->deref_count);
    };
    if (opts->skip_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "skip" "=");
        ss_cat_int(s, opts->skip_count);
    };
    if (opts->rev_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "rev");
        if (!opts->rev_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *switch_opt_names[] =
    { "const", "int", "array", "string", "struct", "bitflag", "debug", "substr", "each" };

static int switch_opt_compat(const struct switch_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    if (opts->string_isset) {
        if (((optgrp) & (01UL))) {
            *ibadopt = 3;
            return 1;
        }
        else {
            optgrp |= 01;
            *icuropt = 3;
        }
    }
    if (opts->substr_isset) {
        if (((optgrp) & (02UL))) {
            *ibadopt = 7;
            return 1;
        }
        else {
            optgrp |= 02;
            *icuropt = 7;
        }
    }
    if (opts->bitflag_isset) {
        if (((optgrp) & (03UL))) {
            *ibadopt = 5;
            return 1;
        }
        else {
            optgrp |= 03;
            *icuropt = 5;
        }
    }
    if (opts->each_isset) {
        if (((optgrp) & (04UL))) {
            *ibadopt = 8;
            return 1;
        }
        else {
            optgrp |= 04;
            *icuropt = 8;
        }
    }
    if (opts->const_isset) {
        if (((optgrp) & (07UL))) {
            *ibadopt = 0;
            return 1;
        }
        else {
            optgrp |= 07;
            *icuropt = 0;
        }
    }
    if (opts->int_isset) {
        if (((optgrp) & (07UL))) {
            *ibadopt = 1;
            return 1;
        }
        else {
            optgrp |= 07;
            *icuropt = 1;
        }
    }
    if (opts->array_isset) {
        if (((optgrp) & (07UL))) {
            *ibadopt = 2;
            return 1;
        }
        else {
            optgrp |= 07;
            *icuropt = 2;
        }
    }
    if (opts->struct_isset) {
        if (((optgrp) & (07UL))) {
            *ibadopt = 4;
            return 1;
        }
        else {
            optgrp |= 07;
            *icuropt = 4;
        }
    }
    return 0;
}

static int switch_opt_string(const struct switch_cmod_opts *const opts, srt_string *s[static 1])
{
    int count =
        opts->const_isset + opts->int_isset + opts->array_isset + opts->string_isset +
        opts->struct_isset + opts->bitflag_isset + opts->debug_isset + opts->substr_isset +
        opts->each_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->const_isset && ++count) {

        ss_cat_c(s, "const");
        if (!opts->const_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->int_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "int");
        if (!opts->int_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->array_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "array");
        if (!opts->array_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->string_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "string");
        if (!opts->string_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->struct_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "struct");
        if (!opts->struct_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->bitflag_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "bitflag");
        if (!opts->bitflag_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->debug_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->substr_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "substr");
        if (!opts->substr_bool)
            ss_cat_c(s, "=false");
    };
    if (opts->each_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "each");
        if (!opts->each_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *free_opt_names[] = { "debug", "const" };

static int free_opt_compat(const struct free_cmod_opts *const opts, int icuropt[static 1],
                           int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int free_opt_string(const struct free_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset + opts->const_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    if (opts->const_isset && ++count) {
        if (count > 1)
            ss_cat_cn1(s, ',');
        ss_cat_c(s, "const");
        if (!opts->const_bool)
            ss_cat_c(s, "=false");
    };
    ss_cat_cn1(s, ']');

    return 0;
}
static const char *arrlen_opt_names[] = { "debug" };

static int arrlen_opt_compat(const struct arrlen_cmod_opts *const opts, int icuropt[static 1],
                             int ibadopt[static 1])
{
    (void)opts;
    (void)icuropt;
    (void)ibadopt;
    unsigned long optgrp = 0;
    (void)optgrp;
    return 0;
}

static int arrlen_opt_string(const struct arrlen_cmod_opts *const opts, srt_string *s[static 1])
{
    int count = opts->debug_isset;
    if (count == 0)
        return 0;

    count = 0;
    ss_cat_c(s, " [");
    if (opts->debug_isset && ++count) {

        ss_cat_c(s, "debug" "=");
        ss_cat_int(s, opts->debug_count);
    };
    ss_cat_cn1(s, ']');

    return 0;
}

/* array locations for ascii printable characters (for iftrie) */
static const int loc_ascii_printable[128] = {
    [0] = 0,[32] = 1,[33] = 2,[34] = 3,[35] = 4,[36] = 5,[37] = 6,[38] = 7,[39] = 8,[40] = 9,[41] =
        10,[42] = 11,[43] = 12,[44] = 13,[45] = 14,[46] = 15,[47] = 16,[48] = 17,[49] = 18,[50] =
        19,[51] = 20,[52] = 21,[53] = 22,[54] = 23,[55] = 24,[56] = 25,[57] = 26,[58] = 27,[59] =
        28,[60] = 29,[61] = 30,[62] = 31,[63] = 32,[64] = 33,[65] = 34,[66] = 35,[67] = 36,[68] =
        37,[69] = 38,[70] = 39,[71] = 40,[72] = 41,[73] = 42,[74] = 43,[75] = 44,[76] = 45,[77] =
        46,[78] = 47,[79] = 48,[80] = 49,[81] = 50,[82] = 51,[83] = 52,[84] = 53,[85] = 54,[86] =
        55,[87] = 56,[88] = 57,[89] = 58,[90] = 59,[91] = 60,[92] = 61,[93] = 62,[94] = 63,[95] =
        64,[96] = 65,[97] = 66,[98] = 67,[99] = 68,[100] = 69,[101] = 70,[102] = 71,[103] =
        72,[104] = 73,[105] = 74,[106] = 75,[107] = 76,[108] = 77,[109] = 78,[110] = 79,[111] =
        80,[112] = 81,[113] = 82,[114] = 83,[115] = 84,[116] = 85,[117] = 86,[118] = 87,[119] =
        88,[120] = 89,[121] = 90,[122] = 91,[123] = 92,[124] = 93,[125] = 94,[126] = 95
};

/* setup option parsing */
static int opt_list_action(vec_string **x, srt_string *value)
{
    char *val = NULL;
    {
        const char *ctmp = ss_to_c(value);
        val = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
        if (NULL != (ctmp) && NULL == (val)) {
        }

    }
    ss_free(&(value));
    if (NULL == val) {
        return 1;
    }

    if (NULL == *x) {
        (*x) = sv_alloc_t(SV_PTR, 2);
        if (sv_void == (*x)) {
            return 1;
        }
    }
    if (!sv_push(&(*x), &(val))) {
        return 1;
    }

    return 0;
}

typedef int (*opt_func)(void *, struct cmod_option *, bool *);
static int option_list_action_column(const struct cmod_option * *var,
                                     struct cmod_option opt[static 1], bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    static const struct cmod_option defval = { CMOD_OPTION_XINT_type, CMOD_OPTION_XINT_value =
            {XINT_UNSIGNED_type, XINT_UNSIGNED_value = 0} };
    /* check option type */
    if (opt->type == CMOD_OPTION_TYPE_STRING && !opt->is_special)
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset

            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = has_value ? opt : &defval;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_bool(bool *var, struct cmod_option opt[static 1],
                                   bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    bool res = true;
    srt_string_ref refs[2];
    ss_cref(&refs[0], "false");
    ss_cref(&refs[1], "true");
    /* check option type */
    if (has_value
        && (opt->type != CMOD_OPTION_TYPE_STRING || opt->is_special || opt->is_verbatim
            || (ss_cmp(CMOD_OPTION_STRING_get(*opt), ss_ref(&refs[1])) != 0
                && (!(res = false)
                    && ss_cmp(CMOD_OPTION_STRING_get(*opt), ss_ref(&refs[0])) != 0))))
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset

            *var = false;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = res;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_index(setix *var, struct cmod_option opt[static 1],
                                    bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(XINT) != opt->type)
        return 2;
    if (has_value && CMOD_OPTION_XINT_get(*opt).type != XINT_TYPE_UNSIGNED)
        return 2;

    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset

            *var = SETIX_none;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = (setix) {
    .ix = (has_value ? XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opt)) : 0),.set = true};
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_int(intmax_t *var, struct cmod_option opt[static 1],
                                  bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(XINT) != opt->type)
        return 2;
    if (!has_value)
        return 2;

    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset

            *var = 0;
        }
        break;
    default:
        break;
    }
    /* set option value */
    if (CMOD_OPTION_XINT_get(*opt).type == XINT_TYPE_SIGNED)
        *var = XINT_SIGNED_get(CMOD_OPTION_XINT_get(*opt));
    else {
        if (XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opt)) <= INTMAX_MAX) {
            *var = (intmax_t) XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opt));
        }
        else {
            return 4;
        }
    }
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_str(srt_string **var, struct cmod_option opt[static 1],
                                  bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value)
        return 2;

    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset
            ss_free(&(*var));
            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = CMOD_OPTION_STRING_get(*opt);
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_cid(srt_string **var, struct cmod_option opt[static 1],
                                  bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value || !striscid(ss_to_c(CMOD_OPTION_STRING_get(*opt))))
        return 2;

    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset
            ss_free(&(*var));
            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = CMOD_OPTION_STRING_get(*opt);
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_idext(srt_string **var, struct cmod_option opt[static 1],
                                    bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value || !strisidext(ss_to_c(CMOD_OPTION_STRING_get(*opt))))
        return 2;

    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case '+':
        return 5;
        break;  // ERROR: invalid assignment
    case ':':
        if (*isset) {   // reset
            ss_free(&(*var));
            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var = CMOD_OPTION_STRING_get(*opt);
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_count(size_t *var, struct cmod_option opt[static 1],
                                    bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(XINT) != opt->type)
        return 2;
    if (has_value && CMOD_OPTION_XINT_get(*opt).type != XINT_TYPE_UNSIGNED)
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case ':':
        if (*isset) {   // reset

            *var = 0;
        }
        break;
    default:
        break;
    }
    /* set option value */
    *var += (has_value ? XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*opt)) : 1);
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_strl(vec_string **var, struct cmod_option opt[static 1],
                                   bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value)
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case ':':
        if (*isset) {   // reset
            {
                for (size_t i = 0; i < sv_len(*var); ++i) {
                    const void *_item = sv_at(*var, i);
                    {
                        char **item = (char **)(_item);
                        free(*item);
                        *item = NULL;
                    }
                }
                sv_free(&(*var));
            }

            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    int ret;
    if ((ret = opt_list_action(var, CMOD_OPTION_STRING_get(*opt))))
        return ret;
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_cidl(vec_string **var, struct cmod_option opt[static 1],
                                   bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value || !striscid(ss_to_c(CMOD_OPTION_STRING_get(*opt))))
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case ':':
        if (*isset) {   // reset
            {
                for (size_t i = 0; i < sv_len(*var); ++i) {
                    const void *_item = sv_at(*var, i);
                    {
                        char **item = (char **)(_item);
                        free(*item);
                        *item = NULL;
                    }
                }
                sv_free(&(*var));
            }

            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    int ret;
    if ((ret = opt_list_action(var, CMOD_OPTION_STRING_get(*opt))))
        return ret;
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

static int option_list_action_idextl(vec_string **var, struct cmod_option opt[static 1],
                                     bool isset[static 1])
{
    const bool has_value = (ENUM_CMOD_OPTION(UNVALUED) != opt->type);
    /* check option type */
    if (has_value && ENUM_CMOD_OPTION(STRING) != opt->type)
        return 2;
    if (!has_value || !strisidext(ss_to_c(CMOD_OPTION_STRING_get(*opt))))
        return 2;
    /* check assignment */
    switch (opt->assign) {
    case '=':
        if (*isset)
            return 3;
        break;  // ERROR: already set
    case '?':
        if (*isset)
            return 0;
        break;  // no-op
    case ':':
        if (*isset) {   // reset
            {
                for (size_t i = 0; i < sv_len(*var); ++i) {
                    const void *_item = sv_at(*var, i);
                    {
                        char **item = (char **)(_item);
                        free(*item);
                        *item = NULL;
                    }
                }
                sv_free(&(*var));
            }

            *var = NULL;
        }
        break;
    default:
        break;
    }
    /* set option value */
    int ret;
    if ((ret = opt_list_action(var, CMOD_OPTION_STRING_get(*opt))))
        return ret;
    CMOD_OPTION_STRING_get(*opt) = NULL;
    *isset = true;      // has been set
    return 0;
}

/* try opening file from CWD and include path */
static FILE *fopen_incpath(const char fname[static 1], const vec_string *ipaths, bool ipaths_only,
                           setix found_o[static 1])
{
    FILE *fp = NULL;
    setix found = { 0 };

    /* try opening from CWD */
    if (!ipaths_only && NULL != (fp = fopen(fname, "r")))
        goto end;

    /* try include search paths (in reverse) */
    {
        const size_t _len = sv_len(ipaths);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _0 = _len - (_idx + 1);
            const size_t _idx = 0;
            (void)_idx;
            char buf[PATH_MAX];
            if ((size_t)
                snprintf(buf, sizeof(buf), "%s/%s", (*(const char **)sv_at(ipaths, _0)), fname)
                >= sizeof(buf))
                goto end;       // truncation, not found
            if (NULL != (fp = fopen(buf, "r"))) {       // found
                found = SETIX_set(_0);
                break;
            }
        }
    }

 end:
    *found_o = found;

    return fp;
}

/* get full file path */
static char *fullpath_incpath(const char *fname, const vec_string *ipaths, setix index)
{
    char *fullpath = NULL;

    if (index.set && index.ix < sv_len(ipaths)) {       // get full path
        const char *ipath = sv_at_ptr(ipaths, index.ix);
        char buf[PATH_MAX];
        srt_string *path = ss_alloc_into_ext_buf(buf, sizeof(buf));
        ss_printf(&path, ss_max(path), "%s/%s", ipath, fname);  // NOTE: harmless truncation
        const char *cpath = ss_to_c(path);
        fullpath = (NULL != (cpath)) ? rstrdup(cpath) : NULL;
        if (NULL != (cpath) && NULL == (fullpath)) {
            return NULL;
        }

        ;
    }
    else {
        fullpath = (NULL != (fname)) ? rstrdup(fname) : NULL;
        if (NULL != (fname) && NULL == (fullpath)) {
            return NULL;
        }

    }

    return fullpath;
}

/* compute expression value */
static int compute_expression_value(const vec_namarg *expr, char *out[static 1], int ierr[static 1])
{
    srt_string *(buf) = ss_alloc(8);
    if (ss_void == (buf)) {
        return 1;
    }

    for (size_t i = 0; i < sv_len(expr); ++i) {
        const struct namarg *earg = sv_at(expr, i);
        if (earg->type != ENUM_NAMARG(VERBATIM)) {
            *ierr = earg->type;
            return 2;
        }       // blame
        ss_cat_c(&buf, CMOD_ARG_VERBATIM_get(*earg));
    }
    {
        const char *ctmp = ss_to_c(buf);
        *out = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
        if (NULL != (ctmp) && NULL == (*out)) {
        }

    }
    ss_free(&(buf));
    if (NULL == *out) {
        return 1;
    }
    return 0;
}

/* prepend prefix to string TODO: move to util_string.cm and add unit test */
static srt_bool iter_map_prefix(uint64_t k, const srt_string *v, void *context)
{
    (void)k;
    srt_string **buf = ((void **)context)[0];
    const int *sep = ((void **)context)[1];
    ss_cat(buf, v);     // append prefix
    ss_putchar(buf, *sep);      // append separator (unicode char)
    return S_TRUE;
}

static int prepend_cmod_prefix(const srt_map *prefixes, char *s[static 1], int sep)
{
    size_t len = sm_len(prefixes);
    if (len == 0)
        return 0;
    srt_string *(buf) = ss_alloc(8);
    if (ss_void == (buf)) {
        return 1;
    }
    ;
    // iterate over prefix map
    void *ctx[2] = { &buf, &sep };
    if (sm_itr_us(prefixes, 0, UINT64_MAX, iter_map_prefix, ctx) != len)
        return 2;
    if (NULL != *s)
        ss_cat_c(&buf, *s);     // append string
    char *snew = NULL;
    {
        const char *ctmp = ss_to_c(buf);
        snew = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
        if (NULL != (ctmp) && NULL == (snew)) {
        }

    }
    ss_free(&(buf));
    if (NULL == snew) {
        return 1;
    }

    free(*s);
    *s = snew;  // replace
    return 0;
}

/* print debug string line by line */
static void debug_byline(const srt_string *str, const char *restrict fname)
{
#ifndef debug_nofun
#define debug_nofun(M,...) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , fname ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , fname ,__VA_ARGS__)))
#endif
    const srt_string *nl = ss_crefs("\n");
    size_t nreq = ss_split(str, nl, NULL, 0);
    srt_string_ref *linrefs = malloc(nreq * sizeof(*linrefs));
    if (NULL != linrefs) {
        size_t nline = ss_split(str, nl, linrefs, nreq);
        assert(nline == nreq);
        for (size_t i = 0; i < nline; ++i) {
            const srt_string *line = ss_ref(&linrefs[i]);
            const char *buf = ss_get_buffer_r(line);    // not NUL-terminated
            const int len = ss_len(line);
            debug_nofun("%.*s", len, buf);
        }
    }
#undef debug_nofun
}

#define LOC_OPTLIST (*loc_optlist)
/* define functions for parser actions and string representations */
__attribute__((unused))
static int defined_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct defined_cmod_opts *const opts,
                             vec_string **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    defined_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_defined_action_0(void *yyscanner,
                          const struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          vec_string **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct defined_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(defined_opt_names) / sizeof(*(defined_opt_names)));
        const char **opt_names = defined_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0)) {
            if ('l' == *((opt->name) + 1) && 'l' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.all_bool, opt, &opts.all_isset);

            }
            else if ('n' == *((opt->name) + 1) && 'y' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.any_bool, opt, &opts.any_isset);

            }
            else if ('t' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                     && 'r' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_idext(&opts.attr_idext, opt, &opts.attr_isset);

            }
            else {
                goto cmod_strin_else_1;
            }
        }
        else if ('d' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && strncmp((opt->name) + 2, "bug", 4) == 0) {
                ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

            }
            else if ('s' == *((opt->name) + 1) && 'l' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.dsl_bool, opt, &opts.dsl_isset);

            }
            else {
                goto cmod_strin_else_1;
            }
        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "num", 4) == 0) {
            ret = option_list_action_bool(&opts.enum_bool, opt, &opts.enum_isset);

        }
        else if ('f' == *((opt->name) + 0) && strncmp((opt->name) + 1, "def", 4) == 0) {
            ret = option_list_action_bool(&opts.fdef_bool, opt, &opts.fdef_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.not_bool, opt, &opts.not_isset);

        }
        else if ('o' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nce", 4) == 0) {
            ret = option_list_action_bool(&opts.once_bool, opt, &opts.once_isset);

        }
        else if ('p' == *((opt->name) + 0) && strncmp((opt->name) + 1, "roto", 5) == 0) {
            ret = option_list_action_bool(&opts.proto_bool, opt, &opts.proto_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('n' == *((opt->name) + 1) && strncmp((opt->name) + 2, "ippet", 6) == 0) {
                ret = option_list_action_bool(&opts.snippet_bool, opt, &opts.snippet_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_1;
            }
        }
        else if ('t' == *((opt->name) + 0)) {
            if ('a' == *((opt->name) + 1) && strncmp((opt->name) + 2, "ble", 4) == 0) {
                ret = option_list_action_bool(&opts.table_bool, opt, &opts.table_isset);

            }
            else if ('y' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                     && 'e' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.type_bool, opt, &opts.type_isset);

            }
            else {
                goto cmod_strin_else_1;
            }
        }
        else {
            goto cmod_strin_else_1;
 cmod_strin_else_1:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (!
        (opts.snippet_bool || opts.table_bool || opts.type_bool || opts.proto_bool || opts.dsl_bool
         || opts.fdef_bool || opts.once_bool || opts.enum_bool)) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("please specify the kind of resource to check");
    }
    opts.all_bool = !opts.any_bool;     // we only use the former

    int icuropt = -1, ibadopt = -1;
    if (defined_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      defined_opt_names[icuropt], defined_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        defined_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        static const char if_true[] = "1";
        static const char if_false[] = "0";

        bool cmp = false;
        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _46 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (opts.snippet_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_snip, _ss);
                    }
                }
                else if (opts.table_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_tab, _ss);
                    }
                }
                else if (opts.type_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_dtype, _ss);
                    }
                }
                else if (opts.proto_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_dproto, _ss);
                    }
                }
                else if (opts.dsl_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_dsl, _ss);
                    }
                }
                else if (opts.fdef_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = shm_count_s(pp->shm_fdef, _ss);
                    }
                }
                else if (opts.once_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = sms_count_s(pp->sms_once, _ss);
                    }
                }
                else if (opts.enum_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _46)));
                        cmp = sms_count_s(pp->sms_enum, _ss);
                    }
                }
                if (cmp && opts.attr_isset) {   // further check for attribute
                    if (opts.snippet_bool) {    // formal argument
                        const struct snippet *snip = NULL; {
                            setix _found = { 0 };
                            {
                                const srt_string *_ss =
                                    ss_crefs((*(const char **)sv_at((*x3), _46)));
                                if (NULL == pp->shm_snip) {
                                }
                                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                            }

                            if (_found.set)
                                snip = (void *)sv_at(pp->sv_snip, _found.ix);
                        }

                        assert(NULL != snip);   // we already know it exists
                        cmp = shm_count_s(snip->shm_forl, opts.attr_idext);
                    }
                    else if (opts.table_bool) { // column
                        const struct table *tab = NULL; {
                            setix _found = { 0 };
                            {
                                const srt_string *_ss =
                                    ss_crefs((*(const char **)sv_at((*x3), _46)));
                                if (NULL == pp->shm_tab) {
                                }
                                _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                            }

                            if (_found.set)
                                tab = (void *)sv_at(pp->sv_tab, _found.ix);
                        }

                        assert(NULL != tab);    // we already know it exists
                        cmp = shm_count_s(tab->shm_colnam, opts.attr_idext);
                    }
                    else
                        WARN0("ignoring option [attr]");
                }

                if (opts.all_bool != cmp)
                    break;      // short-circuit, any is good
            }
        }
        if (opts.not_bool)
            cmp = !cmp;
        BUFPUTS(cmp ? if_true : if_false);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&(opts.attr_idext));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int defined_rule1_str(const struct param *const pp,
                             size_t debug_level,
                             const struct defined_cmod_opts *const opts,
                             vec_string **x3, vec_namarg **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    defined_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, "`", *(&x), "`");
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x4, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_defined_action_1(void *yyscanner,
                          const struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          vec_string **x3, const YYLTYPE *loc3,
                          vec_namarg **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct defined_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(defined_opt_names) / sizeof(*(defined_opt_names)));
        const char **opt_names = defined_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0)) {
            if ('l' == *((opt->name) + 1) && 'l' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.all_bool, opt, &opts.all_isset);

            }
            else if ('n' == *((opt->name) + 1) && 'y' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.any_bool, opt, &opts.any_isset);

            }
            else if ('t' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                     && 'r' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_idext(&opts.attr_idext, opt, &opts.attr_isset);

            }
            else {
                goto cmod_strin_else_2;
            }
        }
        else if ('d' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && strncmp((opt->name) + 2, "bug", 4) == 0) {
                ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

            }
            else if ('s' == *((opt->name) + 1) && 'l' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.dsl_bool, opt, &opts.dsl_isset);

            }
            else {
                goto cmod_strin_else_2;
            }
        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "num", 4) == 0) {
            ret = option_list_action_bool(&opts.enum_bool, opt, &opts.enum_isset);

        }
        else if ('f' == *((opt->name) + 0) && strncmp((opt->name) + 1, "def", 4) == 0) {
            ret = option_list_action_bool(&opts.fdef_bool, opt, &opts.fdef_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.not_bool, opt, &opts.not_isset);

        }
        else if ('o' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nce", 4) == 0) {
            ret = option_list_action_bool(&opts.once_bool, opt, &opts.once_isset);

        }
        else if ('p' == *((opt->name) + 0) && strncmp((opt->name) + 1, "roto", 5) == 0) {
            ret = option_list_action_bool(&opts.proto_bool, opt, &opts.proto_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('n' == *((opt->name) + 1) && strncmp((opt->name) + 2, "ippet", 6) == 0) {
                ret = option_list_action_bool(&opts.snippet_bool, opt, &opts.snippet_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_2;
            }
        }
        else if ('t' == *((opt->name) + 0)) {
            if ('a' == *((opt->name) + 1) && strncmp((opt->name) + 2, "ble", 4) == 0) {
                ret = option_list_action_bool(&opts.table_bool, opt, &opts.table_isset);

            }
            else if ('y' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                     && 'e' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.type_bool, opt, &opts.type_isset);

            }
            else {
                goto cmod_strin_else_2;
            }
        }
        else {
            goto cmod_strin_else_2;
 cmod_strin_else_2:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (!
        (opts.snippet_bool || opts.table_bool || opts.type_bool || opts.proto_bool || opts.dsl_bool
         || opts.fdef_bool || opts.once_bool || opts.enum_bool)) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("please specify the kind of resource to check");
    }
    opts.all_bool = !opts.any_bool;     // we only use the former

    int icuropt = -1, ibadopt = -1;
    if (defined_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      defined_opt_names[icuropt], defined_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        defined_rule1_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        const char *if_true = NULL;
        const char *if_false = NULL;
        /* check and parse cmod_function_arguments */
        {
            size_t nargs = sv_len((*x4));
            if (nargs > 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR0("expected up to two arguments:"
                               " " "[" "<true>" "]" " " "[" "<false>" "]");
            }
            {
                const char **ord[2] = { &if_true, &if_false };
                for (size_t i = 0; i < sv_len((*x4)); ++i) {
                    const struct namarg *narg = sv_at((*x4), i);
                    if (narg->type != CMOD_ARG_VERBATIM) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("expected " "verbatim" " argument at position %zu", i);
                    }
                    const char *txt = CMOD_ARG_VERBATIM_get(*narg);
                    if (NULL == narg->name)
                        *ord[i] = txt;  // const borrow
                    else if (strcmp(narg->name, "true") == 0) {
                        if (*ord[0] != NULL) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                            KEYWORD_ERROR("argument `%s` already set", "true");
                        }
                        *ord[0] = txt;  // const borrow
                    }
                    else if (strcmp(narg->name, "false") == 0) {
                        if (*ord[1] != NULL) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                            KEYWORD_ERROR("argument `%s` already set", "false");
                        }
                        *ord[1] = txt;  // const borrow
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected " "verbatim" " named argument `%s`", narg->name);
                    }
                }
                if (NULL == if_true)
                    (if_true) = "";
                if (NULL == if_false)
                    (if_false) = "";
            }
        }

        bool cmp = false;
        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _47 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (opts.snippet_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_snip, _ss);
                    }
                }
                else if (opts.table_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_tab, _ss);
                    }
                }
                else if (opts.type_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_dtype, _ss);
                    }
                }
                else if (opts.proto_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_dproto, _ss);
                    }
                }
                else if (opts.dsl_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_dsl, _ss);
                    }
                }
                else if (opts.fdef_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = shm_count_s(pp->shm_fdef, _ss);
                    }
                }
                else if (opts.once_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = sms_count_s(pp->sms_once, _ss);
                    }
                }
                else if (opts.enum_bool) {
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _47)));
                        cmp = sms_count_s(pp->sms_enum, _ss);
                    }
                }
                if (cmp && opts.attr_isset) {   // further check for attribute
                    if (opts.snippet_bool) {    // formal argument
                        const struct snippet *snip = NULL; {
                            setix _found = { 0 };
                            {
                                const srt_string *_ss =
                                    ss_crefs((*(const char **)sv_at((*x3), _47)));
                                if (NULL == pp->shm_snip) {
                                }
                                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                            }

                            if (_found.set)
                                snip = (void *)sv_at(pp->sv_snip, _found.ix);
                        }

                        assert(NULL != snip);   // we already know it exists
                        cmp = shm_count_s(snip->shm_forl, opts.attr_idext);
                    }
                    else if (opts.table_bool) { // column
                        const struct table *tab = NULL; {
                            setix _found = { 0 };
                            {
                                const srt_string *_ss =
                                    ss_crefs((*(const char **)sv_at((*x3), _47)));
                                if (NULL == pp->shm_tab) {
                                }
                                _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                            }

                            if (_found.set)
                                tab = (void *)sv_at(pp->sv_tab, _found.ix);
                        }

                        assert(NULL != tab);    // we already know it exists
                        cmp = shm_count_s(tab->shm_colnam, opts.attr_idext);
                    }
                    else
                        WARN0("ignoring option [attr]");
                }

                if (opts.all_bool != cmp)
                    break;      // short-circuit, any is good
            }
        }
        if (opts.not_bool)
            cmp = !cmp;
        BUFPUTS(cmp ? if_true : if_false);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&(opts.attr_idext));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int delay_rule0_str(const struct param *const pp,
                           size_t debug_level,
                           const struct delay_cmod_opts *const opts,
                           char **x3, struct xint *x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    delay_opt_string(opts, s);
    (void)x3;

    if (NULL != *x3) {
        ss_cat_cn1(s, ' ');
        hstr_str_repr(*x3, s);
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    ss_cat_int(s, XINT_UNSIGNED_get(*x4));
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_delay_action_0(void *yyscanner,
                        const struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        char **x3, const YYLTYPE *loc3,
                        struct xint *x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct delay_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(delay_opt_names) / sizeof(*(delay_opt_names)));
        const char **opt_names = delay_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else {
            goto cmod_strin_else_3;
 cmod_strin_else_3:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (delay_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      delay_opt_names[icuropt], delay_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        delay_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        uintmax_t num_delay = XINT_UNSIGNED_get((*x4));
        if (num_delay == 0) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR0("delay must be greater than zero");
        }
        else if (num_delay >= pp->eval_limit) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR("delay exceeds -l/--eval-limit=%zu", pp->eval_limit);
        }

        const char *text = (NULL != (*x3)) ? (*x3) : "%";
        if (num_delay-- > 1) {
            BUFPUTS(pp->kwname);
            if (NULL != (*x3))
                BUFPUTS(" %" "<< ", text, " >>" "% ");
            BUFPUTC('(');
            BUFPUTINT(num_delay);
            BUFPUTC(')');
        }
        else
            BUFPUTS(text);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int dsl_rule0_str(const struct param *const pp,
                         size_t debug_level,
                         const struct dsl_cmod_opts *const opts,
                         char **x3, srt_string **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    dsl_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{ ");
    if (debug_level >= 2)
        ss_cat_c(s, ss_to_c(*x4));
    else
        ss_cat_c(s, "<verbatim>");
    ss_cat_c(s, " %" "}");

    return 0;
}

int cmod_dsl_action_0(void *yyscanner,
                      const struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      char **x3, const YYLTYPE *loc3,
                      srt_string **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct dsl_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(dsl_opt_names) / sizeof(*(dsl_opt_names)));
        const char **opt_names = dsl_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.sep_str, opt, &opts.sep_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_4;
            }
        }
        else {
            goto cmod_strin_else_4;
 cmod_strin_else_4:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (dsl_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      dsl_opt_names[icuropt], dsl_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        dsl_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t dsl_len = ss_len((*x4)) - 1;     // minus terminating NUL
        const char *dsl_buf = ss_get_buffer_r((*x4));
        if (dsl_len == 0 || strisspace(dsl_buf)) {
            WARN0("ignoring empty DSL code");
            goto epilogue;
        }

        /* lookup DSL */
        struct dsl *lang = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_dsl) {
                }
                _found.set = shm_atp_su(pp->shm_dsl, _ss, &(_found).ix);
            }

            if (_found.set)
                lang = (void *)sv_at(pp->sv_dsl, _found.ix);
        }

        if (NULL == lang) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR("undefined language `%s`", (*x3));
        }

        /* parse DSL code */
        int ret;
        srt_vector *astlist = NULL;
        if ((ret =
             cmod_parse_dsl(dsl_len, dsl_buf, lang, pp->pretty, pp->silent, pp->verbose,
                            &astlist))) {
            const char *err = "unknown reason";
            switch (ret) {
            case 1:
                err = "invalid DSL type";
                break;
            case 2:
                err = "scanner init failed";
                break;
            case 3:
                err = "invalid input, please check error log";
                break;
            case 4:
                err = "out of memory";
                break;
            }
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR("failed evaluating language `%s`: %s", (*x3), err);
        }

        /* evaluate and print statements */
        {
            const size_t _len = sv_len(astlist);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _13 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                {
                    size_t ierr = 0;
                    size_t errix = 0;
                    int ret =
                        dsl_ast_eval(((struct dsl_ast *)sv_at(astlist, _13)), lang, &ierr, &errix);
                    switch (ret) {
                    case 1:
                        YYNOMEM;
                        break;
                    case 2:
                        {
                            const char *name_err = sv_at_ptr((lang)->kw_name, errix);
                            const struct snippet *snip_err = sv_at_ptr((lang)->kw_snip, errix);
                            if (SNIPPET_NARGIN_NODEF(snip_err) == snip_err->nargs) {
                                KEYWORD_ERROR
                                    ("expected exactly %zu (got %zu) arguments for keyword `%s` in DSL `%s`",
                                     snip_err->nargs, ierr, name_err, (lang)->name);
                            }
                            else {
                                KEYWORD_ERROR
                                    ("expected between %zu and %zu (got %zu) arguments for keyword `%s` in DSL `%s`",
                                     SNIPPET_NARGIN_NODEF(snip_err), snip_err->nargs, ierr,
                                     name_err, (lang)->name);
                            }
                        }
                        break;
                    case 3:
                        KEYWORD_ERROR("evaluation depth limit (%zu) exceeded for DSL `%s`",
                                      (lang)->max_depth, (lang)->name);
                        break;
                    }
                }
                BUFPUTS(((struct dsl_ast *)sv_at(astlist, _13))->value.txt);
                /* print separator */
                if (NULL == opts.sep_str)
                    BUFPUTC('\n');
                else
                    BUFCAT(opts.sep_str);
            }
        }

        {
            for (size_t i = 0; i < sv_len(astlist); ++i) {
                const void *_item = sv_at(astlist, i);
                dsl_ast_free((struct dsl_ast *)_item);
            }
            sv_free(&(astlist));
        }

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));
    ss_free(&(opts.sep_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int dsl_def_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct dsl_def_cmod_opts *const opts,
                             char **x3, char *x4, struct table_like *x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    dsl_def_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    switch (*x4) {
    case '=':
        ss_cat_c(s, "  = ");
        break;
    case '?':
        ss_cat_c(s, " ?= ");
        break;
    case '+':
        ss_cat_c(s, " += ");
        break;
    case ':':
        ss_cat_c(s, " := ");
        break;
    case '|':
        ss_cat_c(s, " |= ");
        break;
    }

    (void)x5;
    ss_cat_cn1(s, ' ');
    if (NULL != x5->lst) {      // table name list
        for (size_t i = 0; i < sv_len(x5->lst); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            hstr_str_repr(sv_at_ptr(x5->lst, i), s);
        }
    }
    else if (NULL != x5->sel) {
        ss_cat_c(s, x5->tab.name, " ");
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < sv_len(x5->sel); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(x5->sel, i), x5->tab.sv_colnarg, s);
        }
        ss_cat_cn1(s, ')');
    }
    else {      // table
        if (x5->is_lambda) {
            if (debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(s, "<lambda table>");
            }
            else
                ss_cat_c(s, "<lambda table>");
        }
        else {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(*&(&x5->tab)->sv_colnarg, SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
            ss_cat_c(s, " %" "{ ");
            if (debug_level >= 2) {
                /* TODO: print table body */
                ss_cat_c(s, "<table body>");
            }
            else
                ss_cat_c(s, "<table body>");
            ss_cat_c(s, " %" "}");
        }
    }

    return 0;
}

int cmod_dsl_def_action_0(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3,
                          char *x4, const YYLTYPE *loc4,
                          struct table_like *x5, const YYLTYPE *loc5,
                          bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct dsl_def_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(dsl_def_opt_names) / sizeof(*(dsl_def_opt_names)));
        const char **opt_names = dsl_def_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ltsep", 6) == 0) {
            ret = option_list_action_bool(&opts.altsep_bool, opt, &opts.altsep_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "xtend", 6) == 0) {
            ret = option_list_action_bool(&opts.extend_bool, opt, &opts.extend_isset);

        }
        else if ('m' == *((opt->name) + 0) && strncmp((opt->name) + 1, "axeval", 7) == 0) {
            ret = option_list_action_index(&opts.maxeval_index, opt, &opts.maxeval_isset);

        }
        else if ('s' == *((opt->name) + 0) && strncmp((opt->name) + 1, "yntax", 6) == 0) {
            ret = option_list_action_cid(&opts.syntax_cid, opt, &opts.syntax_isset);

        }
        else {
            goto cmod_strin_else_5;
 cmod_strin_else_5:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    switch ((*x4)) {
    case '=':  /* normal */
        break;
    case '+':
        opts.extend_bool = true;
        opts.extend_isset = true;
        break;
    default:
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
        KEYWORD_ERROR0("unsupported assignment operator");
        break;
    }

    int icuropt = -1, ibadopt = -1;
    if (dsl_def_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      dsl_def_opt_names[icuropt], dsl_def_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        dsl_def_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct dsl *olddsl = NULL;
        enum dsl_type dsl_syn = DSL_TYPE_invalid;
        char stmt_sep = '\n';
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_dsl) {
                }
                _found.set = shm_atp_su(pp->shm_dsl, _ss, &(_found).ix);
            }

            if (_found.set)
                olddsl = (void *)sv_at(pp->sv_dsl, _found.ix);
        }
        if (NULL == (olddsl)) {
            if (opts.extend_bool) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("cannot [extend] undefined DSL `%s`", (*x3));
            }
            /* set syntax type */
            const char *syntype = ss_to_c(opts.syntax_cid);
            if ('a' == *((syntype) + 0) && 's' == *((syntype) + 1) && 'm' == *((syntype) + 2)
                && '\0' == *((syntype) + 3)) {
                (dsl_syn) = DSL_TYPE_asm;

            }
            else if ('c' == *((syntype) + 0)) {
                if ('e' == *((syntype) + 1) && strncmp((syntype) + 2, "xpr", 4) == 0) {
                    (dsl_syn) = DSL_TYPE_cexpr;

                }
                else if ('u' == *((syntype) + 1) && strncmp((syntype) + 2, "stom", 5) == 0) {
                    (dsl_syn) = DSL_TYPE_custom;

                }
                else {
                    goto cmod_strin_else_44;
                }
            }
            else if ('s' == *((syntype) + 0) && strncmp((syntype) + 1, "expr", 5) == 0) {
                (dsl_syn) = DSL_TYPE_sexpr;

            }
            else if ('t' == *((syntype) + 0) && strncmp((syntype) + 1, "ree", 4) == 0) {
                (dsl_syn) = DSL_TYPE_tree;

            }
            else {
                goto cmod_strin_else_44;
 cmod_strin_else_44:;
                (dsl_syn) = DSL_TYPE_invalid;

            }
            /* set statement separator */
            switch ((dsl_syn)) {
            case DSL_TYPE_asm:
                (stmt_sep) = opts.altsep_bool ? ';' : '\n';
                break;
            case DSL_TYPE_cexpr:
                (stmt_sep) = ';';
                break;
            case DSL_TYPE_custom:
                (stmt_sep) = opts.altsep_bool ? '\0' : '\n';
                break;
            case DSL_TYPE_tree:        // depth indicator
                (stmt_sep) = opts.altsep_bool ? '\t' : ' ';
                break;
            default:
                (stmt_sep) = '\n';
                break;
            }
        }
        else {
            if (!opts.extend_bool) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("DSL `%s` already defined, did you mean to [extend]?", (*x3));
            }
            (dsl_syn) = (olddsl)->type;
            (stmt_sep) = (olddsl)->stmt_sep;
        }

        if (dsl_syn == DSL_TYPE_invalid) {
            ERROR0("please set a valid syntax type:");
            ERROR0("[asm] assembler-like statements");
            ERROR0("[sexpr] S-expression tree");
            ERROR0("[cexpr] C-like expressions");
            ERROR0("[custom] custom expressions");
            ERROR0("[tree] hierarchical expression");;
            YYERROR;
        }

        if (sv_len((*x5).lst) > 0) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR0("unexpected table name list");
        }
        else if (NULL != (*x5).sel && NULL == (*x5).tab.svv_rows) {
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = (*x5).tab.name;
                mutpp->errlloc = (*loc5);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match((*x5).tab.name, pp->sv_tab, sv_len(pp->sv_tab),
                                    strget_array_tab, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", (*x5).tab.name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown table `%s`", (*x5).tab.name);
                }
            }
        }

        struct table *tab = &(*x5).tab;
        size_t nargs = (NULL != (*x5).sel) ? sv_len((*x5).sel)
            : TABLE_NCOLS(*tab);
        if (nargs > 3) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR("expected up to %d columns", 3);
        }

        setix cols_ix[3] = { {0} };
        if (NULL != (*x5).sel) {
            {
                for (size_t i = 0; i < sv_len((*x5).sel); ++i) {
                    const struct namarg *narg = sv_at((*x5).sel, i);
                    if (narg->type != CMOD_ARG_ARGLINK)
                        assert(0 && "C% internal error" "");

                    size_t ix = CMOD_ARG_ARGLINK_get(*narg);
                    if (NULL == narg->name)
                        cols_ix[i] = SETIX_set(ix);
                    else if (strcmp(narg->name, "keyword") == 0) {
                        if (cols_ix[0].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "keyword";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "keyword");
                        }
                        cols_ix[0] = SETIX_set(ix);
                    }
                    else if (strcmp(narg->name, "snippet") == 0) {
                        if (cols_ix[1].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "snippet";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "snippet");
                        }
                        cols_ix[1] = SETIX_set(ix);
                    }
                    else if (strcmp(narg->name, "precedence") == 0) {
                        if (cols_ix[2].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "precedence";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "precedence");
                        }
                        cols_ix[2] = SETIX_set(ix);
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc5);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected special identifier named argument `%s`",
                                      narg->name);
                    }
                }
                if (!cols_ix[0].set) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR0("missing required column `" "keyword" "`");
                }
            }
        }
        else {
            {
                const srt_string *_ss = ss_crefs("keyword");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[0].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[0]).ix);
            }
            {
                const srt_string *_ss = ss_crefs("snippet");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[1].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[1]).ix);
            }
            {
                const srt_string *_ss = ss_crefs("precedence");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[2].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[2]).ix);
            }
            if (!cols_ix[0].set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                KEYWORD_ERROR("missing required column `%s`", "keyword");
            }
        }

        if (DSL_TYPE_CLIKE(dsl_syn) && !cols_ix[2].set) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR0("syntax requires precedence column");
        }

        srt_vector **kw_name_addr = NULL;
        srt_vector **kw_snip_addr = NULL;
        srt_vector **kw_prec_addr = NULL;
        srt_hmap **kw_map_addr = NULL;

        vec_string *kw_name = NULL;
        vec_snippet_ptr *kw_snip = NULL;
        vec_u8 *kw_prec = NULL;
        hmap_su *kw_map = NULL;

        if (NULL == olddsl) {   // new, allocate stores
            (kw_name) = sv_alloc_t(SV_PTR, 24);
            if (sv_void == (kw_name)) {
                YYNOMEM;
            }

            (kw_snip) = sv_alloc_t(SV_PTR, 24);
            if (sv_void == (kw_snip)) {
                YYNOMEM;
            }

            (kw_map) = shm_alloc(SHM_SU, 48);
            if (NULL == (kw_map)) {
                YYNOMEM;
            }

            if (DSL_TYPE_CLIKE(dsl_syn)) {
                (kw_prec) = sv_alloc_t(SV_U8, 24);
                if (sv_void == (kw_prec)) {
                    YYNOMEM;
                }

            }
            kw_name_addr = &kw_name;
            kw_snip_addr = &kw_snip;
            kw_prec_addr = &kw_prec;
            kw_map_addr = &kw_map;
        }
        else {  // extend, use existing stores
            kw_name_addr = &olddsl->kw_name;
            kw_snip_addr = &olddsl->kw_snip;
            kw_prec_addr = &olddsl->kw_prec;
            kw_map_addr = &olddsl->kw_map;
        }

        /* associate keyword names with snippets */
        {
            for (size_t i = 0; i < sv_len(tab->svv_rows); ++i) {
                const vec_string *row = sv_at_ptr(tab->svv_rows, i);

                const char *keyword = sv_at_ptr(row, cols_ix[0].ix);
                if ('\0' == keyword[0]) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR0("unexpected empty keyword");
                }
                {       // store keyword name
                    if (!DSL_TYPE_CLIKE(dsl_syn) && !striscidslash(keyword)) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                        KEYWORD_ERROR("keyword `%s` is not a valid identifier", keyword);
                    }
                    {   // check duplicate
                        setix found = { 0 };
                        {
                            const srt_string *_ss = ss_crefs(keyword);
                            if (NULL == kw_map) {
                            }
                            found.set = shm_atp_su(kw_map, _ss, &(found).ix);
                        }

                        if (found.set) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR("duplicate keyword `%s` in language `%s`",
                                          keyword, (*x3));
                        }
                    }
                    char *name = (NULL != (keyword)) ? rstrdup(keyword) : NULL;
                    if (NULL != (keyword) && NULL == (name)) {
                        YYNOMEM;
                    }

                    if (!sv_push(kw_name_addr, &(name))) {
                        YYNOMEM;
                    }

                }

                struct snippet *snip = NULL;
                {       // store keyword snippet
                    srt_string *tmp = NULL;
                    const char *snip_name = NULL;
                    if (!cols_ix[1].set) {      // defaults to <lang>:<keyword>
                        (tmp) = ss_alloc(16);
                        if (ss_void == (tmp)) {
                            YYNOMEM;
                        }

                        ss_cat_c(&tmp, (*x3), ":", keyword);
                        snip_name = ss_to_c(tmp);
                    }
                    else {
                        snip_name = sv_at_ptr(row, cols_ix[1].ix);
                        if ('\0' == snip_name[0])
                            snip_name = NULL;   // unset is valid
                    }
                    if (NULL != snip_name) {
                        {
                            setix _found = { 0 };
                            {
                                const srt_string *_ss = ss_crefs(snip_name);
                                if (NULL == pp->shm_snip) {
                                }
                                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                            }

                            if (_found.set)
                                snip = (void *)sv_at(pp->sv_snip, _found.ix);
                        }

                        if (NULL == snip) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR("undefined snippet `%s`", snip_name);
                        }
                        else if (snip->nout > 0) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR("snippet `%s` with output arguments is not supported",
                                          snip_name);
                        }
                        else if (snip->redef) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR("redefinable snippet `%s` is not supported", snip_name);
                        }
                    }
                    if (!sv_push(kw_snip_addr, &(snip))) {
                        YYNOMEM;
                    }

                    {
                        const srt_string *_ss = ss_crefs(keyword);
                        if (!shm_insert_su(kw_map_addr, _ss, sv_len(*kw_snip_addr) - 1)) {
                            YYNOMEM;
                        }
                    }

                    if (!cols_ix[1].set)
                        ss_free(&tmp);
                }

                /* store keyword precedence */
                if (DSL_TYPE_CLIKE(dsl_syn)) {
                    const char *prec = sv_at_ptr(row, cols_ix[2].ix);
                    bool unset = false;
                    uint8_t uprec = 0;
                    if ('"' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 0;
                        unset = false;

                    }
                    else if ('&' == *((prec) + 0)) {
                        if ('\0' == *((prec) + 1)) {
                            uprec = 17;
                            unset = false;

                        }
                        else if ('&' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 20;
                            unset = false;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if ('(' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 3;
                        unset = true;

                    }
                    else if (')' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 4;
                        unset = true;

                    }
                    else if ('*' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 12;
                        unset = false;

                    }
                    else if ('+' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 13;
                        unset = false;

                    }
                    else if (',' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 2;
                        unset = true;

                    }
                    else if ('-' == *((prec) + 0) && '>' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                        uprec = 8;
                        unset = false;

                    }
                    else if ('.' == *((prec) + 0)) {
                        if ('\0' == *((prec) + 1)) {
                            uprec = 9;
                            unset = true;

                        }
                        else if ('>' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 10;
                            unset = true;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if ('/' == *((prec) + 0)) {
                        if ('*' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 27;
                            unset = false;

                        }
                        else if ('/' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 26;
                            unset = true;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if (':' == *((prec) + 0) && ':' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                        uprec = 1;
                        unset = false;

                    }
                    else if (';' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 23;
                        unset = true;

                    }
                    else if ('<' == *((prec) + 0)) {
                        if ('\0' == *((prec) + 1)) {
                            uprec = 15;
                            unset = false;

                        }
                        else if ('<' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 14;
                            unset = false;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if ('=' == *((prec) + 0)) {
                        if ('\0' == *((prec) + 1)) {
                            uprec = 22;
                            unset = false;

                        }
                        else if ('=' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 16;
                            unset = false;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if ('[' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 6;
                        unset = false;

                    }
                    else if (']' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 7;
                        unset = false;

                    }
                    else if ('^' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 18;
                        unset = false;

                    }
                    else if ('f' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 5;
                        unset = false;

                    }
                    else if ('{' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 24;
                        unset = false;

                    }
                    else if ('|' == *((prec) + 0)) {
                        if ('\0' == *((prec) + 1)) {
                            uprec = 19;
                            unset = false;

                        }
                        else if ('|' == *((prec) + 1) && '\0' == *((prec) + 2)) {
                            uprec = 21;
                            unset = false;

                        }
                        else {
                            goto cmod_strin_else_45;
                        }
                    }
                    else if ('}' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 25;
                        unset = false;

                    }
                    else if ('~' == *((prec) + 0) && '\0' == *((prec) + 1)) {
                        uprec = 11;
                        unset = false;

                    }
                    else {
                        goto cmod_strin_else_45;
 cmod_strin_else_45:   ;
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                        KEYWORD_ERROR("unknown precedence type `%s`", prec);

                    }
                    if (NULL == snip && !unset) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                        KEYWORD_ERROR("precedence `%s` requires a snippet", prec);
                    }
                    if (!sv_push_u8(kw_prec_addr, uprec))
                        YYNOMEM;
                }
            }
        }

        if (NULL == olddsl) {   // define new
            /* create DSL struct */
            struct dsl newdsl = {
                .name = (*x3),
                .type = dsl_syn,
                .stmt_sep = stmt_sep,
                .kw_name = kw_name,
                .kw_snip = kw_snip,
                .kw_map = kw_map,
                .kw_prec = kw_prec,
                .max_depth = opts.maxeval_index.set ? opts.maxeval_index.ix : pp->eval_limit
            };
            (*x3) = NULL;       // hand-over

            /* store DSL */
            {
                const srt_string *_ss = ss_crefs((newdsl).name);
                if (!sv_push(&(pp->sv_dsl), &(newdsl))
                    || !shm_insert_su(&(pp->shm_dsl), _ss, sv_len(pp->sv_dsl) - 1)) {
                    YYNOMEM;
                }
            }
        }

        {
            for (size_t i = 0; i < sv_len((*x5).sel); ++i) {
                const void *_item = sv_at((*x5).sel, i);
                namarg_free((struct namarg *)_item);
            }
            sv_free(&((*x5).sel));
        }
        if (NULL == tab->name)  // free lamda table
            table_clear(tab);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.syntax_cid));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int include_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct include_cmod_opts *const opts,
                             char **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    include_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "\"", *x3, "\"");

    return 0;
}

int cmod_include_action_0(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct include_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(include_opt_names) / sizeof(*(include_opt_names)));
        const char **opt_names = include_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "lank", 5) == 0) {
            ret = option_list_action_bool(&opts.blank_bool, opt, &opts.blank_isset);

        }
        else if ('c' == *((opt->name) + 0) && '\0' == *((opt->name) + 1)) {
            ret = option_list_action_bool(&opts.c_bool, opt, &opts.c_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('q' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uiet", 5) == 0) {
            ret = option_list_action_bool(&opts.quiet_bool, opt, &opts.quiet_isset);

        }
        else if ('s' == *((opt->name) + 0) && 'y' == *((opt->name) + 1) && 's' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.sys_bool, opt, &opts.sys_isset);

        }
        else {
            goto cmod_strin_else_6;
 cmod_strin_else_6:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (include_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      include_opt_names[icuropt], include_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        include_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (pp->subparse)
            KEYWORD_ERROR0("not supported in sub-parse");

        /* infer [c] from file extension */
        if (strncmp(&(*x3)[strlen((*x3)) - 2], ".h", 3) == 0)
            opts.c_bool = true;

        if (opts.c_bool) {
            if (opts.sys_bool) {
                BUFPUTS("#include <", (*x3), ">\n");
                PRINTTEST("#include <%s>\n", (*x3));
            }
            else {
                BUFPUTS("#include \"", (*x3), "\"\n");
                PRINTTEST("#include \"%s\"\n", (*x3));
            }
            goto epilogue;
        }

        setix found = { 0 };
        FILE *fp = fopen_incpath((*x3), pp->sv_ipath, opts.sys_bool, &found);
        if (NULL == fp) {       // not found
            if (opts.opt_bool) {        // optional
                WARN("optional include file \"%s\" not found, skipping", (*x3));
                goto epilogue;
            }
            else {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("cannot find \"%s\" in %ssystem include search path", (*x3),
                              opts.sys_bool ? "" : "current working directory or ");
            }
        }

        {       // push include file into scanner buffer stack
            char *fullpath = fullpath_incpath((*x3), pp->sv_ipath, found);
            ret_t ret =
                pushfile(yyscanner, pp, fp, fullpath, fullpath, false, !opts.blank_bool,
                         opts.quiet_bool);
            if (ret) {
                {
                    check_ret_t(ret, (const struct ret_f *)&pushfile_ret_f);

                }
                KEYWORD_ERROR0("failed pushing buffer into stack");
            }
            free(fullpath);
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

static void ss_cat_xint(srt_string **ss, struct xint x, const char *mode)
{
    if (x.type == ENUM_XINT(UNSIGNED)) {
        int buf_off;
        char buf[STRUMAX_LEN];
        (buf_off) = strumax(XINT_UNSIGNED_get(x), buf, mode);

        /* do not append terminating NUL */
        ss_cat_cn(&(*ss), buf + buf_off, STRUMAX_LEN - buf_off - 1);
    }
    else {
        int buf_off;
        char buf[STRIMAX_LEN];
        (buf_off) = strimax(XINT_SIGNED_get(x), buf, mode);

        /* do not append terminating NUL */
        ss_cat_cn(&(*ss), buf + buf_off, STRIMAX_LEN - buf_off - 1);
    }
}

static void ss_cat_umax(srt_string **ss, uintmax_t x, const char *sign)
{
    ss_cat_c(ss, sign);
    int buf_off;
    char buf[STRUMAX_LEN];
    (buf_off) = strumax(x, buf, "");

    /* do not append terminating NUL */
    ss_cat_cn(&(*ss), buf + buf_off, STRUMAX_LEN - buf_off - 1);
}

__attribute__((unused))
static int intop_rule0_str(const struct param *const pp,
                           size_t debug_level,
                           const struct intop_cmod_opts *const opts,
                           vec_xint **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    intop_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const struct xint *x = sv_at(*x3, i);
        switch (x->type) {
        case ENUM_XINT(SIGNED):
            ss_cat_int(s, XINT_SIGNED_get(*x));
            break;
        case ENUM_XINT(UNSIGNED):
            ss_cat_int(s, XINT_UNSIGNED_get(*x));
            break;
        case ENUM_XINT(INVALID):
            break;
        }
    }
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_intop_action_0(void *yyscanner,
                        const struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        vec_xint **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct intop_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(intop_opt_names) / sizeof(*(intop_opt_names)));
        const char **opt_names = intop_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && 'd' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
            && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.add_bool, opt, &opts.add_isset);

        }
        else if ('d' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && strncmp((opt->name) + 2, "bug", 4) == 0) {
                ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

            }
            else if ('i' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.div_bool, opt, &opts.div_isset);

            }
            else {
                goto cmod_strin_else_7;
            }
        }
        else if ('m' == *((opt->name) + 0)) {
            if ('a' == *((opt->name) + 1) && 'x' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.max_bool, opt, &opts.max_isset);

            }
            else if ('i' == *((opt->name) + 1) && 'n' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.min_bool, opt, &opts.min_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.mod_bool, opt, &opts.mod_isset);

            }
            else if ('u' == *((opt->name) + 1) && 'l' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.mul_bool, opt, &opts.mul_isset);

            }
            else {
                goto cmod_strin_else_7;
            }
        }
        else if ('p' == *((opt->name) + 0)) {
            if ('o' == *((opt->name) + 1) && 'w' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.pow_bool, opt, &opts.pow_isset);

            }
            else if ('r' == *((opt->name) + 1) && 'o' == *((opt->name) + 2)
                     && 'd' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.prod_bool, opt, &opts.prod_isset);

            }
            else {
                goto cmod_strin_else_7;
            }
        }
        else if ('s' == *((opt->name) + 0)) {
            if ('i' == *((opt->name) + 1) && 'g' == *((opt->name) + 2) && 'n' == *((opt->name) + 3)
                && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.sign_bool, opt, &opts.sign_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else if ('u' == *((opt->name) + 1)) {
                if ('b' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                    ret = option_list_action_bool(&opts.sub_bool, opt, &opts.sub_isset);

                }
                else if ('m' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                    ret = option_list_action_bool(&opts.sum_bool, opt, &opts.sum_isset);

                }
                else {
                    goto cmod_strin_else_7;
                }
            }
            else {
                goto cmod_strin_else_7;
            }
        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nsigned", 8) == 0) {
            ret = option_list_action_bool(&opts.unsigned_bool, opt, &opts.unsigned_isset);

        }
        else {
            goto cmod_strin_else_7;
 cmod_strin_else_7:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (!
        (opts.sum_bool || opts.prod_bool || opts.min_bool || opts.max_bool || opts.add_bool
         || opts.mul_bool || opts.sub_bool || opts.div_bool || opts.mod_bool || opts.pow_bool)) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("please specify operation to perform");
    }

    int icuropt = -1, ibadopt = -1;
    if (intop_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      intop_opt_names[icuropt], intop_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        intop_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        /* set operation name */
        const char *opname = NULL;
        if (opts.sum_bool)
            opname = "sum";
        else if (opts.prod_bool)
            opname = "prod";
        else if (opts.min_bool)
            opname = "min";
        else if (opts.max_bool)
            opname = "max";
        else if (opts.add_bool)
            opname = "add";
        else if (opts.mul_bool)
            opname = "mul";
        else if (opts.sub_bool)
            opname = "sub";
        else if (opts.div_bool)
            opname = "div";
        else if (opts.mod_bool)
            opname = "mod";
        else if (opts.pow_bool)
            opname = "pow";
        const char *signmode = opts.sign_bool ? "+" : "";       // explicit sign or not

        /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
        if (opts.sum_bool) {
            struct xint sum = opts.unsigned_bool ? XINT_UNSIGNED_set(0) : XINT_SIGNED_set(0);
            for (size_t i = 0; i < sv_len((*x3)); i++) {
                const struct xint *x = sv_at((*x3), i);
                switch (x->type) {
                case ENUM_XINT(SIGNED):
                    if (opts.unsigned_bool) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                    }
                    else {
                        intmax_t addend = x->value.Signed;
                        if (((addend > 0) && (XINT_SIGNED_get(sum) > (INTMAX_MAX - addend))) ||
                            ((addend < 0) && (XINT_SIGNED_get(sum) < (INTMAX_MIN - addend)))) {
                            KEYWORD_ERROR("[%s] signed addition overflow", opname);
                        }
                        else {
                            XINT_SIGNED_get(sum) = XINT_SIGNED_get(sum) + addend;
                        }
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                    {
                        uintmax_t addend = x->value.Unsigned;
                        if (opts.unsigned_bool) {
                            if ((UINTMAX_MAX - XINT_UNSIGNED_get(sum)) < addend) {
                                KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                            }
                            else {
                                XINT_UNSIGNED_get(sum) = XINT_UNSIGNED_get(sum) + addend;
                            }
                        }
                        else {  // unsigned to signed conversion
                            intmax_t s_addend;
                            if (addend <= INTMAX_MAX) {
                                s_addend = (intmax_t) addend;
                            }
                            else {
                                KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",
                                              opname);
                            }
                            if (((s_addend > 0) && (XINT_SIGNED_get(sum) > (INTMAX_MAX - s_addend)))
                                || ((s_addend < 0)
                                    && (XINT_SIGNED_get(sum) < (INTMAX_MIN - s_addend)))) {
                                KEYWORD_ERROR("[%s] signed addition overflow", opname);
                            }
                            else {
                                XINT_SIGNED_get(sum) = XINT_SIGNED_get(sum) + s_addend;
                            }
                        }
                    }
                    break;
                case ENUM_XINT(INVALID):
                    assert(0 && "C% internal error" "");
                    break;
                }
            }
            ss_cat_xint(kwbuf, sum, signmode);
        }

        /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
        else if (opts.prod_bool) {
            struct xint prod = opts.unsigned_bool ? XINT_UNSIGNED_set(1) : XINT_SIGNED_set(1);
            for (size_t i = 0; i < sv_len((*x3)); i++) {
                const struct xint *x = sv_at((*x3), i);
                switch (x->type) {
                case ENUM_XINT(SIGNED):
                    if (opts.unsigned_bool) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                    }
                    else {
                        intmax_t factor = x->value.Signed;
                        if (XINT_SIGNED_get(prod) > 0) {
                            if (factor > 0) {
                                if (XINT_SIGNED_get(prod) > (INTMAX_MAX / factor)) {
                                    KEYWORD_ERROR("[%s] signed multiplication overflow", opname);
                                }
                            }
                            else {
                                if (factor < (INTMAX_MIN / XINT_SIGNED_get(prod))) {
                                    KEYWORD_ERROR("[%s] signed multiplication overflow", opname);
                                }
                            }
                        }
                        else {
                            if (factor > 0) {
                                if (XINT_SIGNED_get(prod) < (INTMAX_MIN / factor)) {
                                    KEYWORD_ERROR("[%s] signed multiplication overflow", opname);
                                }
                            }
                            else {
                                if ((XINT_SIGNED_get(prod) != 0)
                                    && (factor < (INTMAX_MAX / XINT_SIGNED_get(prod)))) {
                                    KEYWORD_ERROR("[%s] signed multiplication overflow", opname);
                                }
                            }
                        }
                        XINT_SIGNED_get(prod) = XINT_SIGNED_get(prod) * factor;
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                    {
                        uintmax_t factor = x->value.Unsigned;
                        if (opts.unsigned_bool) {
                            if ((factor != 0) && XINT_UNSIGNED_get(prod) > (UINTMAX_MAX / factor)) {
                                KEYWORD_ERROR("[%s] unsigned multiplication overflow", opname);
                            }
                            else {
                                XINT_UNSIGNED_get(prod) = XINT_UNSIGNED_get(prod) * factor;
                            }
                        }
                        else {  // unsigned to signed conversion
                            intmax_t s_factor;
                            if (factor <= INTMAX_MAX) {
                                s_factor = (intmax_t) factor;
                            }
                            else {
                                KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",
                                              opname);
                            }
                            if (XINT_SIGNED_get(prod) > 0) {
                                if (s_factor > 0) {
                                    if (XINT_SIGNED_get(prod) > (INTMAX_MAX / s_factor)) {
                                        KEYWORD_ERROR("[%s] signed multiplication overflow",
                                                      opname);
                                    }
                                }
                                else {
                                    if (s_factor < (INTMAX_MIN / XINT_SIGNED_get(prod))) {
                                        KEYWORD_ERROR("[%s] signed multiplication overflow",
                                                      opname);
                                    }
                                }
                            }
                            else {
                                if (s_factor > 0) {
                                    if (XINT_SIGNED_get(prod) < (INTMAX_MIN / s_factor)) {
                                        KEYWORD_ERROR("[%s] signed multiplication overflow",
                                                      opname);
                                    }
                                }
                                else {
                                    if ((XINT_SIGNED_get(prod) != 0)
                                        && (s_factor < (INTMAX_MAX / XINT_SIGNED_get(prod)))) {
                                        KEYWORD_ERROR("[%s] signed multiplication overflow",
                                                      opname);
                                    }
                                }
                            }
                            XINT_SIGNED_get(prod) = XINT_SIGNED_get(prod) * s_factor;
                        }
                    }
                    break;
                case ENUM_XINT(INVALID):
                    assert(0 && "C% internal error" "");
                    break;
                }
            }
            ss_cat_xint(kwbuf, prod, signmode);
        }

        /* operands are handled as uintmax_t if [unsigned], else as intmax_t */
        else if (opts.min_bool || opts.max_bool) {
            struct xint res = opts.unsigned_bool ? XINT_UNSIGNED_set(0) : XINT_SIGNED_set(0);
            for (size_t i = 0; i < sv_len((*x3)); i++) {
                const struct xint *x = sv_at((*x3), i);
                switch (x->type) {
                case ENUM_XINT(SIGNED):
                    if (opts.unsigned_bool) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                    }
                    else {
                        intmax_t item = x->value.Signed;
                        if (i == 0)
                            XINT_SIGNED_get(res) = item;        // initialize result
                        else {  // update result
                            if (opts.max_bool) {
                                (XINT_SIGNED_get(res)) =
                                    ((item) >
                                     (XINT_SIGNED_get(res))) ? (item) : (XINT_SIGNED_get(res));
                            }
                            else {
                                (XINT_SIGNED_get(res)) =
                                    ((item) <
                                     (XINT_SIGNED_get(res))) ? (item) : (XINT_SIGNED_get(res));
                            }
                        }
                    }
                    break;
                case ENUM_XINT(UNSIGNED):
                    {
                        uintmax_t item = x->value.Unsigned;
                        if (opts.unsigned_bool) {
                            if (i == 0)
                                XINT_UNSIGNED_get(res) = item;  // initialize result
                            else {      // update result
                                if (opts.max_bool) {
                                    (XINT_UNSIGNED_get(res)) =
                                        ((item) >
                                         (XINT_UNSIGNED_get(res))) ? (item)
                                        : (XINT_UNSIGNED_get(res));
                                }
                                else {
                                    (XINT_UNSIGNED_get(res)) =
                                        ((item) <
                                         (XINT_UNSIGNED_get(res))) ? (item)
                                        : (XINT_UNSIGNED_get(res));
                                }
                            }
                        }
                        else {  // unsigned to signed conversion
                            intmax_t s_item;
                            if (item <= INTMAX_MAX) {
                                s_item = (intmax_t) item;
                            }
                            else {
                                KEYWORD_ERROR("[%s] unsigned to signed conversion overflow",
                                              opname);
                            }
                            if (opts.max_bool) {
                                (XINT_SIGNED_get(res)) =
                                    ((s_item) >
                                     (XINT_SIGNED_get(res))) ? (s_item) : (XINT_SIGNED_get(res));
                            }
                            else {
                                (XINT_SIGNED_get(res)) =
                                    ((s_item) <
                                     (XINT_SIGNED_get(res))) ? (s_item) : (XINT_SIGNED_get(res));
                            }
                        }
                    }
                    break;
                case ENUM_XINT(INVALID):
                    assert(0 && "C% internal error" "");
                    break;
                }
            }
            ss_cat_xint(kwbuf, res, signmode);
        }

        /* operands are handled as uintmax_t, sign of result is computed separately */
        else if (opts.add_bool) {
            if (sv_len((*x3)) != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] operation takes exactly two arguments", opname);
            }
            const struct xint *xaugend = sv_at((*x3), 0);
            const struct xint *xaddend = sv_at((*x3), 1);

            uintmax_t sum = 0;
            bool isneg = false;

            if (opts.unsigned_bool) {
                if (xaugend->type != ENUM_XINT(UNSIGNED) || xaddend->type != ENUM_XINT(UNSIGNED)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                }
                if ((UINTMAX_MAX - xaugend->value.Unsigned) < xaddend->value.Unsigned) {
                    KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                }
                else {
                    sum = xaugend->value.Unsigned + xaddend->value.Unsigned;
                }
            }
            else {
                bool isneg_ag = false;
                uintmax_t abs_ag = 0;
                if (xaugend->type == ENUM_XINT(UNSIGNED))
                    abs_ag = xaugend->value.Unsigned;
                else {
                    isneg_ag = (xaugend->value.Signed < 0);
                    if (xaugend->value.Signed == INTMAX_MIN) {
                        abs_ag = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_ag = (uintmax_t) imaxabs(xaugend->value.Signed);
                    }
                }

                bool isneg_ad = false;
                uintmax_t abs_ad = 0;
                if (xaddend->type == ENUM_XINT(UNSIGNED))
                    abs_ad = xaddend->value.Unsigned;
                else {
                    isneg_ad = (xaddend->value.Signed < 0);
                    if (xaddend->value.Signed == INTMAX_MIN) {
                        abs_ad = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_ad = (uintmax_t) imaxabs(xaddend->value.Signed);
                    }
                }

                bool ag_smaller = (abs_ag < abs_ad);

                 /**/ if (!isneg_ag && !isneg_ad) {     // +(abs(ag) + abs(ad))
                    isneg = false;
                    if ((UINTMAX_MAX - abs_ag) < abs_ad) {
                        KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                    }
                    else {
                        sum = abs_ag + abs_ad;
                    }
                }
                else if (isneg_ag && isneg_ad) {        // -(abs(ag) + abs(ad))
                    isneg = true;
                    if ((UINTMAX_MAX - abs_ag) < abs_ad) {
                        KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                    }
                    else {
                        sum = abs_ag + abs_ad;
                    }
                }
                else if (isneg_ag && !isneg_ad && ag_smaller) { // +(abs(ad) - abs(ag))
                    isneg = false;
                    if (abs_ad < abs_ag) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        sum = abs_ad - abs_ag;
                    }
                }
                else if (!isneg_ag && isneg_ad && ag_smaller) { // -(abs(ad) - abs(ag))
                    isneg = true;
                    if (abs_ad < abs_ag) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        sum = abs_ad - abs_ag;
                    }
                }
                else if (!isneg_ag && isneg_ad && !ag_smaller) {        // +(abs(ag) - abs(ad))
                    isneg = false;
                    if (abs_ag < abs_ad) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        sum = abs_ag - abs_ad;
                    }
                }
                else if (isneg_ag && !isneg_ad && !ag_smaller) {        // -(abs(ag) - abs(ad))
                    isneg = true;
                    if (abs_ag < abs_ad) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        sum = abs_ag - abs_ad;
                    }
                }
            }
            ss_cat_umax(kwbuf, sum, isneg ? "-" : (opts.sign_bool ? "+" : ""));
        }

        /* operands are handled as uintmax_t, sign of result is computed separately */
        else if (opts.sub_bool) {
            if (sv_len((*x3)) != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] operation takes exactly two arguments", opname);
            }
            const struct xint *xminuend = sv_at((*x3), 0);
            const struct xint *xsubtrahend = sv_at((*x3), 1);

            uintmax_t difference = 0;
            bool isneg = false;

            if (opts.unsigned_bool) {
                if (xminuend->type != ENUM_XINT(UNSIGNED)
                    || xsubtrahend->type != ENUM_XINT(UNSIGNED)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                }
                if (xminuend->value.Unsigned < xsubtrahend->value.Unsigned) {
                    KEYWORD_ERROR("[%s] expecting positive difference", opname);
                }
                else {
                    difference = xminuend->value.Unsigned - xsubtrahend->value.Unsigned;
                }
            }
            else {
                bool isneg_m = false;
                uintmax_t abs_m = 0;
                if (xminuend->type == ENUM_XINT(UNSIGNED))
                    abs_m = xminuend->value.Unsigned;
                else {
                    isneg_m = (xminuend->value.Signed < 0);
                    if (xminuend->value.Signed == INTMAX_MIN) {
                        abs_m = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_m = (uintmax_t) imaxabs(xminuend->value.Signed);
                    }
                }

                bool isneg_s = false;
                uintmax_t abs_s = 0;
                if (xsubtrahend->type == ENUM_XINT(UNSIGNED))
                    abs_s = xsubtrahend->value.Unsigned;
                else {
                    isneg_s = (xsubtrahend->value.Signed < 0);
                    if (xsubtrahend->value.Signed == INTMAX_MIN) {
                        abs_s = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_s = (uintmax_t) imaxabs(xsubtrahend->value.Signed);
                    }
                }

                bool m_smaller = (abs_m < abs_s);

                 /**/ if (!isneg_m && isneg_s) {        // +(abs(m) + abs(s))
                    isneg = false;
                    if ((UINTMAX_MAX - abs_m) < abs_s) {
                        KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                    }
                    else {
                        difference = abs_m + abs_s;
                    }
                }
                else if (isneg_m && !isneg_s) { // -(abs(m) + abs(s))
                    isneg = true;
                    if ((UINTMAX_MAX - abs_m) < abs_s) {
                        KEYWORD_ERROR("[%s] unsigned addition overflow", opname);
                    }
                    else {
                        difference = abs_m + abs_s;
                    }
                }
                else if (isneg_m && isneg_s && m_smaller) {     // +(abs(s) - abs(m))
                    isneg = false;
                    if (abs_s < abs_m) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        difference = abs_s - abs_m;
                    }
                }
                else if (!isneg_m && !isneg_s && m_smaller) {   // -(abs(s) - abs(m))
                    isneg = true;
                    if (abs_s < abs_m) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        difference = abs_s - abs_m;
                    }
                }
                else if (!isneg_m && !isneg_s && !m_smaller) {  // +(abs(m) - abs(s))
                    isneg = false;
                    if (abs_m < abs_s) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        difference = abs_m - abs_s;
                    }
                }
                else if (isneg_m && isneg_s && !m_smaller) {    // -(abs(m) - abs(s))
                    isneg = true;
                    if (abs_m < abs_s) {
                        KEYWORD_ERROR("[%s] unsigned subtraction overflow", opname);
                    }
                    else {
                        difference = abs_m - abs_s;
                    }
                }
            }
            ss_cat_umax(kwbuf, difference, isneg ? "-" : (opts.sign_bool ? "+" : ""));
        }

        /* operands are handled as uintmax_t, sign of result is computed separately */
        else if (opts.mul_bool) {
            if (sv_len((*x3)) != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] operation takes exactly two arguments", opname);
            }
            const struct xint *xmultiplier = sv_at((*x3), 0);
            const struct xint *xmultiplicand = sv_at((*x3), 1);

            uintmax_t product = 1;
            bool isneg = false;

            if (opts.unsigned_bool) {
                if (xmultiplier->type != ENUM_XINT(UNSIGNED)
                    || xmultiplicand->type != ENUM_XINT(UNSIGNED)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                }
                if ((xmultiplicand->value.Unsigned != 0)
                    && xmultiplier->value.Unsigned >
                    (UINTMAX_MAX / xmultiplicand->value.Unsigned)) {
                    KEYWORD_ERROR("[%s] unsigned multiplication overflow", opname);
                }
                else {
                    product = xmultiplier->value.Unsigned * xmultiplicand->value.Unsigned;
                }
            }
            else {
                bool isneg_mr = false;
                uintmax_t abs_mr = 0;
                if (xmultiplier->type == ENUM_XINT(UNSIGNED))
                    abs_mr = xmultiplier->value.Unsigned;
                else {
                    isneg_mr = (xmultiplier->value.Signed < 0);
                    if (xmultiplier->value.Signed == INTMAX_MIN) {
                        abs_mr = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_mr = (uintmax_t) imaxabs(xmultiplier->value.Signed);
                    }
                }

                bool isneg_md = false;
                uintmax_t abs_md = 0;
                if (xmultiplicand->type == ENUM_XINT(UNSIGNED))
                    abs_md = xmultiplicand->value.Unsigned;
                else {
                    isneg_md = (xmultiplicand->value.Signed < 0);
                    if (xmultiplicand->value.Signed == INTMAX_MIN) {
                        abs_md = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_md = (uintmax_t) imaxabs(xmultiplicand->value.Signed);
                    }
                }

                isneg = (isneg_mr != isneg_md); // opposite signs -> negative
                if ((abs_md != 0) && abs_mr > (UINTMAX_MAX / abs_md)) {
                    KEYWORD_ERROR("[%s] unsigned multiplication overflow", opname);
                }
                else {
                    product = abs_mr * abs_md;
                }
            }
            ss_cat_umax(kwbuf, product, isneg ? "-" : (opts.sign_bool ? "+" : ""));
        }

        /* operands are handled as uintmax_t, sign of result is computed separately */
        else if (opts.div_bool || opts.mod_bool) {
            if (sv_len((*x3)) != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] operation takes exactly two arguments", opname);
            }
            const struct xint *xdividend = sv_at((*x3), 0);
            const struct xint *xdivisor = sv_at((*x3), 1);

            if ((xdivisor->type == ENUM_XINT(UNSIGNED) && xdivisor->value.Unsigned == 0)
                || (xdivisor->type == ENUM_XINT(SIGNED) && xdivisor->value.Signed == 0)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] attempted division by zero", opname);
            }

            uintmax_t result = 0;
            bool isneg = false;

            if (opts.unsigned_bool) {
                if (xdividend->type != ENUM_XINT(UNSIGNED) || xdivisor->type != ENUM_XINT(UNSIGNED)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("[%s] expecting only unsigned operands", opname);
                }
                result = opts.div_bool ? xdividend->value.Unsigned / xdivisor->value.Unsigned
                    : xdividend->value.Unsigned % xdivisor->value.Unsigned;
            }
            else {
                bool isneg_dd = false;
                uintmax_t abs_dd = 0;
                if (xdividend->type == ENUM_XINT(UNSIGNED))
                    abs_dd = xdividend->value.Unsigned;
                else {
                    isneg_dd = (xdividend->value.Signed < 0);
                    if (xdividend->value.Signed == INTMAX_MIN) {
                        abs_dd = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_dd = (uintmax_t) imaxabs(xdividend->value.Signed);
                    }
                }

                bool isneg_ds = false;
                uintmax_t abs_ds = 0;
                if (xdivisor->type == ENUM_XINT(UNSIGNED))
                    abs_ds = xdivisor->value.Unsigned;
                else {
                    isneg_ds = (xdivisor->value.Signed < 0);
                    if (xdivisor->value.Signed == INTMAX_MIN) {
                        abs_ds = (uintmax_t) INTMAX_MAX + 1;
                    }
                    else {
                        abs_ds = (uintmax_t) imaxabs(xdivisor->value.Signed);
                    }
                }

                isneg = opts.div_bool ? (isneg_dd != isneg_ds)  // opposite signs -> negative
                    : isneg_dd; // remainder has same sign as divisor
                result = opts.div_bool ? abs_dd / abs_ds : abs_dd % abs_ds;
            }
            ss_cat_umax(kwbuf, result, isneg ? "-" : (opts.sign_bool ? "+" : ""));
        }

        /* operands are handled as uintmax_t, sign of result is computed separately */
        else if (opts.pow_bool) {
            if (sv_len((*x3)) != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] operation takes exactly two arguments", opname);
            }
            const struct xint *xbase = sv_at((*x3), 0);
            const struct xint *xexponent = sv_at((*x3), 1);

            if (xexponent->type == ENUM_XINT(SIGNED)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("[%s] exponent must be unsigned", opname);
            }
            uintmax_t exponent = xexponent->value.Unsigned;

            uintmax_t base = 0;
            if (xbase->type == ENUM_XINT(UNSIGNED))
                base = xbase->value.Unsigned;
            else {
                if (opts.unsigned_bool) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("[%s] expecting unsigned base", opname);
                }
                if (xbase->value.Signed == INTMAX_MIN) {
                    base = (uintmax_t) INTMAX_MAX + 1;
                }
                else {
                    base = (uintmax_t) imaxabs(xbase->value.Signed);
                }

            }

            uintmax_t power = 1;
            bool isneg = (xbase->type == ENUM_XINT(SIGNED) && xbase->value.Signed < 0
                          && exponent % 2 == 1);

            for (uintmax_t i = 0; i < exponent; i++) {  // unwrap integer power as multiplication
                if ((base != 0) && power > (UINTMAX_MAX / base)) {
                    KEYWORD_ERROR("[%s] unsigned multiplication overflow", opname);
                }
                else {
                    power = power * base;
                }
            }
            ss_cat_umax(kwbuf, power, isneg ? "-" : (opts.sign_bool ? "+" : ""));
        }

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int map_rule0_str(const struct param *const pp,
                         size_t debug_level,
                         const struct map_cmod_opts *const opts,
                         struct table_like *x3, const char **x4, struct recall_like *x5,
                         srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    map_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    if (NULL != x3->lst) {      // table name list
        for (size_t i = 0; i < sv_len(x3->lst); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            hstr_str_repr(sv_at_ptr(x3->lst, i), s);
        }
    }
    else if (NULL != x3->sel) {
        ss_cat_c(s, x3->tab.name, " ");
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < sv_len(x3->sel); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(x3->sel, i), x3->tab.sv_colnarg, s);
        }
        ss_cat_cn1(s, ')');
    }
    else {      // table
        if (x3->is_lambda) {
            if (debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(s, "<lambda table>");
            }
            else
                ss_cat_c(s, "<lambda table>");
        }
        else {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(*&(&x3->tab)->sv_colnarg, SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
            ss_cat_c(s, " %" "{ ");
            if (debug_level >= 2) {
                /* TODO: print table body */
                ss_cat_c(s, "<table body>");
            }
            else
                ss_cat_c(s, "<table body>");
            ss_cat_c(s, " %" "}");
        }
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, x5->name, " ");
    if (NULL != x5->snip) {     // arguments were processed
        const size_t nout = SNIPPET_NARGOUT(x5->snip);
        if (nout > 0) {
            for (size_t i = 0; i < sv_len(x5->arg_lists); ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x5->arg_lists, i), SETIX_none, SETIX_set(nout), s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = 0; i < sv_len(x5->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x5->arg_lists, i), SETIX_set(nout), SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }
    else {      // arguments were not processed
        if (pp->nout > 0) {
            for (size_t i = 0; i < pp->nout; ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x5->arg_lists, i), SETIX_none, SETIX_none, s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = pp->nout; i < sv_len(x5->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x5->arg_lists, i), SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }

    return 0;
}

int cmod_map_action_0(void *yyscanner,
                      const struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      struct table_like *x3, const YYLTYPE *loc3,
                      const char **x4, const YYLTYPE *loc4,
                      struct recall_like *x5, const YYLTYPE *loc5,
                      bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct map_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(map_opt_names) / sizeof(*(map_opt_names)));
        const char **opt_names = map_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "dd1", 4) == 0) {
            ret = option_list_action_count(&opts.add1_count, opt, &opts.add1_isset);

        }
        else if ('b' == *((opt->name) + 0) && 'r' == *((opt->name) + 1) && 'k' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.brk_str, opt, &opts.brk_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ead", 4) == 0) {
            ret = option_list_action_count(&opts.head_count, opt, &opts.head_isset);

        }
        else if ('i' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.inv_bool, opt, &opts.inv_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('n' == *((opt->name) + 0)) {
            if ('b' == *((opt->name) + 1) && 'r' == *((opt->name) + 2) && 'k' == *((opt->name) + 3)
                && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_index(&opts.nbrk_index, opt, &opts.nbrk_isset);

            }
            else if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "2sp", 4) == 0) {
                ret = option_list_action_bool(&opts.nl2sp_bool, opt, &opts.nl2sp_isset);

            }
            else if ('o' == *((opt->name) + 1)) {
                if ('h' == *((opt->name) + 2) && strncmp((opt->name) + 3, "ead", 4) == 0) {
                    ret = option_list_action_count(&opts.nohead_count, opt, &opts.nohead_isset);

                }
                else if ('t' == *((opt->name) + 2)) {
                    if ('a' == *((opt->name) + 3) && 'i' == *((opt->name) + 4)
                        && 'l' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                        ret = option_list_action_count(&opts.notail_count, opt, &opts.notail_isset);

                    }
                    else if ('n' == *((opt->name) + 3) && strncmp((opt->name) + 4, "ull", 4) == 0) {
                        ret =
                            option_list_action_column(&opts.notnull_column, opt,
                                                      &opts.notnull_isset);

                    }
                    else {
                        goto cmod_strin_else_8;
                    }
                }
                else {
                    goto cmod_strin_else_8;
                }
            }
            else if ('u' == *((opt->name) + 1) && 'm' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_index(&opts.num_index, opt, &opts.num_isset);

            }
            else {
                goto cmod_strin_else_8;
            }
        }
        else if ('o' == *((opt->name) + 0)) {
            if ('p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

            }
            else if ('r' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.ord_bool, opt, &opts.ord_isset);

            }
            else {
                goto cmod_strin_else_8;
            }
        }
        else if ('r' == *((opt->name) + 0) && 'e' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.rev_bool, opt, &opts.rev_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.sep_str, opt, &opts.sep_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && 't' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_column(&opts.sort_column, opt, &opts.sort_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_8;
            }
        }
        else if ('t' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ail", 4) == 0) {
            ret = option_list_action_count(&opts.tail_count, opt, &opts.tail_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "niq", 4) == 0) {
            ret = option_list_action_column(&opts.uniq_column, opt, &opts.uniq_isset);

        }
        else if ('x' == *((opt->name) + 0) && '\0' == *((opt->name) + 1)) {
            ret = option_list_action_bool(&opts.x_bool, opt, &opts.x_isset);

        }
        else {
            goto cmod_strin_else_8;
 cmod_strin_else_8:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (map_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      map_opt_names[icuropt], map_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        map_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action
        struct table **tablist = NULL;

        if (inactive)
            goto epilogue;

        (void)(*x4);
        bool has_sep = !ss_empty(opts.sep_str);
        if (has_sep)
            ss_dec_esc_c(&opts.sep_str);

        size_t brklen = 1;
        bool has_brk = !ss_empty(opts.brk_str);
        has_brk = (has_brk && opts.nbrk_index.set && (brklen = opts.nbrk_index.ix) > 0) ||
            (has_brk && !opts.nbrk_index.set);
        if (has_brk)
            ss_dec_esc_c(&opts.brk_str);

        if (opts.x_bool)
            KEYWORD_ERROR0("unimplemented, sorry");     // TODO

        /* handle table_like */
        bool has_selcols = (NULL != (*x3).sel);
        bool is_lambda_table = (!has_selcols && NULL == (*x3).lst);
        if (has_selcols && NULL == (*x3).tab.svv_rows) {
            if (opts.opt_bool) {
                VERBOSE("ignoring undefined table `%s`", (*x3).tab.name);
                free((*x3).tab.name);
                goto epilogue;
            }
            else if (opts.lazy_bool) {
                WARN("undefined table `%s`, delaying evaluation", (*x3).tab.name);
                if (opts.str_isset)
                    kwbuf = kwbuf_save;
                map_rule0_str(pp, 3, &opts, x3, x4, x5, &str_repr);
                BUFPUTS(pp->kwname);
                BUFCAT(str_repr);
                BUFPUTC('\n');
                free((*x3).tab.name);
                goto epilogue;
            }
            else {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x3).tab.name;
                    mutpp->errlloc = (*loc3);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x3).tab.name, pp->sv_tab, sv_len(pp->sv_tab),
                                        strget_array_tab, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", (*x3).tab.name,
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown table `%s`", (*x3).tab.name);
                    }
                }
            }
        }
        else if (is_lambda_table && opts.lazy_bool) {   // lambda or explicit table
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("lazy evaluation with lambda tables is not supported");
        }

        size_t ntab = (NULL != (*x3).lst) ? sv_len((*x3).lst) : 1;
        if (NULL == (*x3).lst) {
            const vec_string *firstrow = (sv_len((&(*x3).tab)->svv_rows) > 0)
                ? sv_at_ptr( (& (*x3).tab)->svv_rows, 0)
                : NULL;
            if (NULL == firstrow || sv_len(firstrow) == 0) {
                WARN("ignoring empty table `%s`",
                     (&(*x3).tab)->name ? (&(*x3).tab)->name : "<lambda>");
                goto epilogue;
            }
        }

        (tablist) = rcalloc((ntab), sizeof(*tablist));
        if (NULL == (tablist)) {
            YYNOMEM;
        }
        ;
        if (NULL == (*x3).lst) {        // lambda table or table selection
            tablist[0] = &(*x3).tab;
        }
        else
            for (size_t i = 0; i < sv_len((*x3).lst); ++i) {    // resolve table names
                const char *table_name = sv_at_ptr((*x3).lst, i);
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs(table_name);
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tablist[i] = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tablist[i])) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`", table_name);
                        tablist[i] = NULL;
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation", table_name);
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        map_rule0_str(pp, 3, &opts, x3, x4, x5, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        BUFPUTC('\n');
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = table_name;
                            mutpp->errlloc = (*loc3);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match(table_name, pp->sv_tab, sv_len(pp->sv_tab),
                                                strget_array_tab, 0.75, &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", table_name,
                                              fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`", table_name);
                            }
                        }
                    }
                }
                if (sv_len(tablist[i]->svv_rows) == 0) {
                    WARN("ignoring empty table `%s`", table_name);
                    tablist[i] = NULL;
                    continue;;
                }

            }

        /* handle recall_like */
        const struct snippet *snip = (*x5).snip;
        if (NULL == (snip)) {
            if (opts.opt_bool) {        // ignore if optional
                VERBOSE("ignoring undefined snippet `%s`", (*x5).name);
                goto epilogue;
            }
            else if (opts.lazy_bool) {
                WARN("undefined snippet `%s`, delaying evaluation", (*x5).name);
                if (opts.str_isset)
                    kwbuf = kwbuf_save;
                map_rule0_str(pp, 3, &opts, x3, x4, x5, &str_repr);
                BUFPUTS(pp->kwname);
                BUFCAT(str_repr);
                BUFPUTC('\n');
                goto epilogue;
            }
            else {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x5).name;
                    mutpp->errlloc = (*loc5);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x5).name, pp->sv_snip, sv_len(pp->sv_snip),
                                        strget_array_snip, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown snippet `%s`, did you mean `%s`?", (*x5).name,
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown snippet `%s`", (*x5).name);
                    }
                }
            }
        }

        if (sv_len((*x5).arg_lists) != 1) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR0("expected only one argument list");
        }

        const vec_namarg *argvals = sv_at_ptr((*x5).arg_lists, 0);
        size_t nempty = 0;
        bool has_rowno = false;
        {
            const size_t _len = sv_len(argvals);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _14 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                switch (((struct namarg *)sv_at(argvals, _14))->type) {
                case ENUM_NAMARG(NOVALUE):
                    ++nempty;
                    break;
                case ENUM_NAMARG(ROWNO):
                    has_rowno = true;
                    break;
                case ENUM_NAMARG(EXPR):
                    for (size_t i = 0;
                         i < sv_len(CMOD_ARG_EXPR_get(*((struct namarg *)sv_at(argvals, _14))));
                         ++i) {
                        const struct namarg *earg =
                            sv_at(CMOD_ARG_EXPR_get(*((struct namarg *)sv_at(argvals, _14))), i);
                        if (earg->type == ENUM_NAMARG(ROWNO))
                            has_rowno = true;
                    }
                    break;
                default:
                    break;
                }
            }
        }
        bool use_selcols = (nempty == sv_len(argvals));

        if (!use_selcols && has_selcols) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("unexpected table column selection");
        }

        size_t ntot = 1;
        for (size_t i = 0; i < ntab; ++i) {
            const struct table *tab = tablist[i];
            if (NULL == tab)
                continue;

            vec_namarg *selcols = NULL;
            if (use_selcols) {  // special case: when no arguments, use columns in selection (or all)
                const vec_namarg *cols = has_selcols ? (*x3).sel : tab->sv_colnarg;
                size_t ncol = sv_len(cols);
                if (ncol > snip->nargs) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR
                        ("too many columns selected from table `%s` as arguments to snippet `%s`",
                         tab->name ? tab->name : "<lambda>", snip->name);
                }
                (selcols) = sv_alloc(sizeof(struct namarg), ncol, NULL);
                if (sv_void == (selcols)) {
                    YYNOMEM;
                }

                if (has_selcols) {      // selected columns
                    {
                        const size_t _len = sv_len(cols);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _15 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            if (!sv_push(&(selcols), &((*(struct namarg *)sv_at(cols, _15))))) {
                                YYNOMEM;
                            }
                            // borrow
                        }
                    }
                }
                else {  // all columns
                    {
                        const size_t _len = sv_len(cols);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _16 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            struct namarg item = CMOD_ARG_ARGLINK_set(_16);
                            if (!sv_push(&(selcols), &(item))) {
                                YYNOMEM;
                            }
                            // borrow
                        }
                    }
                }
                argvals = selcols;
            }
            else {      // resolve column references
                argvals = sv_at_ptr((*x5).arg_lists, 0);
                const char *badcol = NULL;
                {
                    const size_t _len = sv_len(argvals);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _17 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        if (((struct namarg *)sv_at(argvals, _17))->type == CMOD_ARG_SPECIAL) { // column name
                            char *colnam = CMOD_ARG_SPECIAL_get(*((struct namarg *)sv_at(argvals, _17)));       // blame
                            setix found = { 0 };
                            {
                                const srt_string *_ss = ss_crefs(colnam);
                                if (NULL == tab->shm_colnam) {
                                }
                                found.set = shm_atp_su(tab->shm_colnam, _ss, &(found).ix);
                            }

                            if (!found.set) {
                                badcol = colnam;
                                break;
                            }
                            free(colnam);       // clear
                            *((struct namarg *)sv_at(argvals, _17)) = CMOD_ARG_ARGLINK_xset(found.ix,.name = ((struct namarg *)sv_at(argvals, _17))->name);     // change type
                        }
                        else if (((struct namarg *)sv_at(argvals, _17))->type == CMOD_ARG_EXPR) {       // argexpr
                            const vec_namarg *expr =
                                CMOD_ARG_EXPR_get(*((struct namarg *)sv_at(argvals, _17)));
                            for (size_t i = 0; i < sv_len(expr); ++i) { // evaluate expression
                                struct namarg *item = (struct namarg *)sv_at(expr, i);
                                if (item->type != CMOD_ARG_SPECIAL)
                                    continue;   // not a column name
                                char *colnam = CMOD_ARG_SPECIAL_get(*item);
                                setix found = { 0 };
                                {
                                    const srt_string *_ss = ss_crefs(colnam);
                                    if (NULL == tab->shm_colnam) {
                                    }
                                    found.set = shm_atp_su(tab->shm_colnam, _ss, &(found).ix);
                                }

                                if (!found.set) {
                                    badcol = colnam;
                                    break;
                                }
                                free(colnam);   // clear
                                *item = CMOD_ARG_ARGLINK_set(found.ix); // change type
                            }
                            if (NULL != badcol)
                                break;
                        }
                    }
                }
                if (NULL != badcol) {
                    {
                        struct param *mutpp = yyget_extra(yyscanner);
                        mutpp->errtxt = badcol;
                        mutpp->errlloc = (*loc3);
                        const char *errline = ss_to_c(pp->line);
                        if (NULL != pp->errtxt && NULL != errline) {
                            bool found =
                                errline_find_name(errline, pp->errtxt, mutpp->errlloc.first_line,
                                                  &(mutpp->errlloc));
                            if (found) {
                            }
                        }
                    }
                    KEYWORD_ERROR("column `%s` not found in table `%s`", badcol, tab->name);
                }
            }

            /* check for non-default arguments without value */
            {
                const size_t _len = sv_len(snip->sv_forl);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _18 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    const struct namarg *varg = (_18 < sv_len(argvals))
                        ? sv_at(argvals, _18) : NULL;
                    if (((struct namarg *)sv_at(snip->sv_forl, _18))->type == CMOD_ARG_NOVALUE &&
                        (NULL == varg || varg->type == CMOD_ARG_NOVALUE)) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                        KEYWORD_ERROR("no value for non-default argument `%s` of snippet `%s`",
                                      ((struct namarg *)sv_at(snip->sv_forl, _18))->name,
                                      snip->name);
                    }
                }
            }

            size_t nrows = 0;
            size_t *order = NULL;
            {   // setup order
                const char *err = NULL;
                int ret = map_setup_order(tab, &order, &nrows, &err, &opts);
                if (ret) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    switch (ret) {
                    case 1:
                        YYNOMEM;
                        break;
                    case 2:    // empty row range
                        VERBOSE("ignoring empty row range for table `%s`",
                                tab->name ? tab->name : "<anonymous>");
                        break;
                    case 3:    // out-of-bounds row range
                        KEYWORD_ERROR("row range out of bounds for table `%s`",
                                      tab->name ? tab->name : "<anonymous>");
                        break;
                    case 4:    // problem with option column
                        KEYWORD_ERROR("[%s] column name or index not found in table `%s`", err,
                                      tab->name ? tab->name : "<anonymous>");
                        break;
                    }
                }
            }

            if (has_sep && i > 0)
                BUFCAT(opts.sep_str);   // separator
            {   // collect and print rowbufs
                srt_vector *rowbuflens = NULL;
                vec_string *rowbufs = NULL;
                if (map_defined_snippet
                    (tab, snip, argvals, nrows, order, has_rowno, &opts, &rowbufs, &rowbuflens))
                    YYNOMEM;
                {
                    const size_t _len = sv_len(rowbufs);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _19 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        size_t len = sv_at_u64(rowbuflens, _19);
                        if (has_sep && _19 > 0)
                            BUFCAT(opts.sep_str);       // separator
                        BUFCATN((*(const char **)sv_at(rowbufs, _19)), len - 1);        // NOTE: discounting NUL
                        if (has_brk && ntot % brklen == 0)
                            BUFCAT(opts.brk_str);       // group separator
                        ++(ntot);
                    }
                }
                {
                    for (size_t i = 0; i < sv_len(rowbufs); ++i) {
                        const void *_item = sv_at(rowbufs, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(rowbufs));
                }
                sv_free(&rowbuflens);
            }

            free(order);
            sv_free(&selcols);  // unborrow
        }
        if (has_brk && (ntot - 1) % brklen != 0)
            BUFCAT(opts.brk_str);       // group separator

        if (is_lambda_table)
            table_clear(tablist[0]);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op
        free(tablist);

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&(opts.sep_str));
    ss_free(&(opts.brk_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int map_rule1_str(const struct param *const pp,
                         size_t debug_level,
                         const struct map_cmod_opts *const opts,
                         struct table_like *x3, const char **x4, srt_string **x5,
                         srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    map_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    if (NULL != x3->lst) {      // table name list
        for (size_t i = 0; i < sv_len(x3->lst); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            hstr_str_repr(sv_at_ptr(x3->lst, i), s);
        }
    }
    else if (NULL != x3->sel) {
        ss_cat_c(s, x3->tab.name, " ");
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < sv_len(x3->sel); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(x3->sel, i), x3->tab.sv_colnarg, s);
        }
        ss_cat_cn1(s, ')');
    }
    else {      // table
        if (x3->is_lambda) {
            if (debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(s, "<lambda table>");
            }
            else
                ss_cat_c(s, "<lambda table>");
        }
        else {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(*&(&x3->tab)->sv_colnarg, SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
            ss_cat_c(s, " %" "{ ");
            if (debug_level >= 2) {
                /* TODO: print table body */
                ss_cat_c(s, "<table body>");
            }
            else
                ss_cat_c(s, "<table body>");
            ss_cat_c(s, " %" "}");
        }
    }

    (void)x4;

    if (NULL != x4)
        ss_cat_c(s, " | ");

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{\n");
    if (debug_level >= 2) {
        const struct snippet tmp = {
            .body = *x5,        // const borrow
            .sv_argl = pp->sv_dllarg    // const borrow
        };
        snippet_body_str_repr(&tmp, tmp.sv_argl, s);
    }
    else
        ss_cat_c(s, "<snippet body>");
    ss_cat_c(s, "%" "}");

    return 0;
}

int cmod_map_action_1(void *yyscanner,
                      const struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      struct table_like *x3, const YYLTYPE *loc3,
                      const char **x4, const YYLTYPE *loc4,
                      srt_string **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct map_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(map_opt_names) / sizeof(*(map_opt_names)));
        const char **opt_names = map_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "dd1", 4) == 0) {
            ret = option_list_action_count(&opts.add1_count, opt, &opts.add1_isset);

        }
        else if ('b' == *((opt->name) + 0) && 'r' == *((opt->name) + 1) && 'k' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.brk_str, opt, &opts.brk_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ead", 4) == 0) {
            ret = option_list_action_count(&opts.head_count, opt, &opts.head_isset);

        }
        else if ('i' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.inv_bool, opt, &opts.inv_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('n' == *((opt->name) + 0)) {
            if ('b' == *((opt->name) + 1) && 'r' == *((opt->name) + 2) && 'k' == *((opt->name) + 3)
                && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_index(&opts.nbrk_index, opt, &opts.nbrk_isset);

            }
            else if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "2sp", 4) == 0) {
                ret = option_list_action_bool(&opts.nl2sp_bool, opt, &opts.nl2sp_isset);

            }
            else if ('o' == *((opt->name) + 1)) {
                if ('h' == *((opt->name) + 2) && strncmp((opt->name) + 3, "ead", 4) == 0) {
                    ret = option_list_action_count(&opts.nohead_count, opt, &opts.nohead_isset);

                }
                else if ('t' == *((opt->name) + 2)) {
                    if ('a' == *((opt->name) + 3) && 'i' == *((opt->name) + 4)
                        && 'l' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                        ret = option_list_action_count(&opts.notail_count, opt, &opts.notail_isset);

                    }
                    else if ('n' == *((opt->name) + 3) && strncmp((opt->name) + 4, "ull", 4) == 0) {
                        ret =
                            option_list_action_column(&opts.notnull_column, opt,
                                                      &opts.notnull_isset);

                    }
                    else {
                        goto cmod_strin_else_9;
                    }
                }
                else {
                    goto cmod_strin_else_9;
                }
            }
            else if ('u' == *((opt->name) + 1) && 'm' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_index(&opts.num_index, opt, &opts.num_isset);

            }
            else {
                goto cmod_strin_else_9;
            }
        }
        else if ('o' == *((opt->name) + 0)) {
            if ('p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

            }
            else if ('r' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.ord_bool, opt, &opts.ord_isset);

            }
            else {
                goto cmod_strin_else_9;
            }
        }
        else if ('r' == *((opt->name) + 0) && 'e' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.rev_bool, opt, &opts.rev_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.sep_str, opt, &opts.sep_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && 't' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_column(&opts.sort_column, opt, &opts.sort_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_9;
            }
        }
        else if ('t' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ail", 4) == 0) {
            ret = option_list_action_count(&opts.tail_count, opt, &opts.tail_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "niq", 4) == 0) {
            ret = option_list_action_column(&opts.uniq_column, opt, &opts.uniq_isset);

        }
        else if ('x' == *((opt->name) + 0) && '\0' == *((opt->name) + 1)) {
            ret = option_list_action_bool(&opts.x_bool, opt, &opts.x_isset);

        }
        else {
            goto cmod_strin_else_9;
 cmod_strin_else_9:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (map_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      map_opt_names[icuropt], map_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        map_rule1_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action
        struct table **tablist = NULL;

        if (inactive)
            goto epilogue;

        (void)(*x4);
        bool has_sep = !ss_empty(opts.sep_str);
        if (has_sep)
            ss_dec_esc_c(&opts.sep_str);

        size_t brklen = 1;
        bool has_brk = !ss_empty(opts.brk_str);
        has_brk = (has_brk && opts.nbrk_index.set && (brklen = opts.nbrk_index.ix) > 0) ||
            (has_brk && !opts.nbrk_index.set);
        if (has_brk)
            ss_dec_esc_c(&opts.brk_str);
        if ('\0' == ss_at((*x5), 0)) {
            WARN0("ignoring empty or blank lambda");
            goto epilogue;
        }

        if (opts.nl2sp_bool) {
            ss_replace(&(*x5), 0, ss_crefs("\n"), ss_crefs(" "));
        }

        /* handle table_like */
        bool has_selcols = (NULL != (*x3).sel);
        bool is_lambda_table = (!has_selcols && NULL == (*x3).lst);
        if (has_selcols && NULL == (*x3).tab.svv_rows) {
            if (opts.opt_bool) {
                VERBOSE("ignoring undefined table `%s`", (*x3).tab.name);
                free((*x3).tab.name);
                goto epilogue;
            }
            else if (opts.lazy_bool) {
                WARN("undefined table `%s`, delaying evaluation", (*x3).tab.name);
                if (opts.str_isset)
                    kwbuf = kwbuf_save;
                map_rule1_str(pp, 3, &opts, x3, x4, x5, &str_repr);
                BUFPUTS(pp->kwname);
                BUFCAT(str_repr);
                BUFPUTC('\n');
                free((*x3).tab.name);
                goto epilogue;
            }
            else {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x3).tab.name;
                    mutpp->errlloc = (*loc3);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x3).tab.name, pp->sv_tab, sv_len(pp->sv_tab),
                                        strget_array_tab, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", (*x3).tab.name,
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown table `%s`", (*x3).tab.name);
                    }
                }
            }
        }
        else if (is_lambda_table && opts.lazy_bool) {   // lambda or explicit table
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("lazy evaluation with lambda tables is not supported");
        }

        size_t ntab = (NULL != (*x3).lst) ? sv_len((*x3).lst) : 1;
        if (NULL == (*x3).lst) {
            const vec_string *firstrow = (sv_len((&(*x3).tab)->svv_rows) > 0)
                ? sv_at_ptr( (& (*x3).tab)->svv_rows, 0)
                : NULL;
            if (NULL == firstrow || sv_len(firstrow) == 0) {
                WARN("ignoring empty table `%s`",
                     (&(*x3).tab)->name ? (&(*x3).tab)->name : "<lambda>");
                goto epilogue;
            }
        }

        (tablist) = rcalloc((ntab), sizeof(*tablist));
        if (NULL == (tablist)) {
            YYNOMEM;
        }
        ;
        if (NULL == (*x3).lst) {        // lambda table or table selection
            tablist[0] = &(*x3).tab;
        }
        else
            for (size_t i = 0; i < sv_len((*x3).lst); ++i) {    // resolve table names
                const char *table_name = sv_at_ptr((*x3).lst, i);
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs(table_name);
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tablist[i] = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tablist[i])) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`", table_name);
                        tablist[i] = NULL;
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation", table_name);
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        map_rule1_str(pp, 3, &opts, x3, x4, x5, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        BUFPUTC('\n');
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = table_name;
                            mutpp->errlloc = (*loc3);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match(table_name, pp->sv_tab, sv_len(pp->sv_tab),
                                                strget_array_tab, 0.75, &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", table_name,
                                              fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`", table_name);
                            }
                        }
                    }
                }
                if (sv_len(tablist[i]->svv_rows) == 0) {
                    WARN("ignoring empty table `%s`", table_name);
                    tablist[i] = NULL;
                    continue;;
                }

            }

        bool has_rowno = false;
        for (size_t i = 0; i < sv_len(pp->sv_dllarg) && !(has_rowno); ++i) {
            const struct dllarg *arg = sv_at(pp->sv_dllarg, i);
            if (arg->type == DLLTYPE_NR)
                has_rowno = true;
        }

        size_t ntot = 1;
        for (size_t i = 0; i < ntab; ++i) {
            const struct table *tab = tablist[i];
            if (NULL == tab)
                continue;

            /* define snippet with table columns as formal arguments */
            const vec_namarg *cols = has_selcols ? (*x3).sel : tab->sv_colnarg;
            size_t ncol = sv_len(cols);
            vec_namarg *(sv_forl) = sv_alloc(sizeof(struct namarg), ncol, NULL);
            if (sv_void == (sv_forl)) {
                YYNOMEM;
            }

            vec_namarg *(selcols) = sv_alloc(sizeof(struct namarg), ncol, NULL);
            if (sv_void == (selcols)) {
                YYNOMEM;
            }

            if (has_selcols) {  // column selection
                {
                    const size_t _len = sv_len(cols);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _20 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        if (!sv_push(&(selcols), &((*(struct namarg *)sv_at(cols, _20))))) {
                            YYNOMEM;
                        }

                        const char *colnam =
                            (NULL !=
                             (*(struct namarg *)sv_at(cols, _20)).
                             name) ? (*(struct namarg *)sv_at(cols, _20)).name : TABLE_COLNAME(tab,
                                                                                               CMOD_ARG_ARGLINK_get
                                                                                               ((*
                                                                                                 (struct
                                                                                                  namarg
                                                                                                  *)
                                                                                                 sv_at
                                                                                                 (cols,
                                                                                                  _20))));
                        struct namarg item = CMOD_ARG_NOVALUE_xset(.name = (char *)colnam);
                        if (!sv_push(&(sv_forl), &(item))) {
                            YYNOMEM;
                        }
                        // borrow
                    }
                }
            }
            else {      // all columns
                {
                    const size_t _len = sv_len(cols);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _21 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        struct namarg selitem = CMOD_ARG_ARGLINK_set(_21);
                        if (!sv_push(&(selcols), &(selitem))) {
                            YYNOMEM;
                        }

                        struct namarg item = CMOD_ARG_NOVALUE_xset(.name =
                                                                   ((const struct namarg *)
                                                                    sv_at(cols, _21))->name);
                        if (!sv_push(&(sv_forl), &(item))) {
                            YYNOMEM;
                        }
                        // borrow
                    }
                }
            }

            struct snippet snip = { 0 };
            snip = (struct snippet) {
                .name = tab->name,.body = (*x5),
                .sv_forl = sv_forl,.sv_argl = pp->sv_dllarg,
                .redef = false,.nout = 0
            };
            {   // check snippet
                size_t ierr = 0;
                const char *err = NULL;
                int ret = snip_def_check(&snip, true, &ierr, &err);     // is_lambda = true
                if (ret >= 2)
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                switch (ret) {
                case 0:
                    if (ierr == 0)
                        break;
                    VERBOSE("unused arguments (%zu) in lambda snippet for table `%s`",
                            ierr, tab->name ? tab->name : "<lambda>");
                    break;
                case 1:
                    YYNOMEM;;
                    break;
                    /* case 2 not possible for lambda snippet */
                    /* case 3 not possible for lambda snippet */
                case 4:
                    KEYWORD_ERROR
                        ("reference to undefined column `%s` in lambda snippet for table `%s`", err,
                         tab->name ? tab->name : "<lambda>");
                    break;
                case 5:
                    KEYWORD_ERROR("%s in lambda snippet for table `%s`", err,
                                  tab->name ? tab->name : "<lambda>");
                    break;
                }
            }

            size_t nrows = 0;
            size_t niter = 0;
            size_t *order = NULL;
            {   // setup order
                const char *err = NULL;
                int ret = map_setup_order(tab, &order, &nrows, &err, &opts);
                if (ret) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    switch (ret) {
                    case 1:
                        YYNOMEM;
                        break;
                    case 2:    // empty row range
                        VERBOSE("ignoring empty row range for table `%s`",
                                tab->name ? tab->name : "<anonymous>");
                        break;
                    case 3:    // out-of-bounds row range
                        KEYWORD_ERROR("row range out of bounds for table `%s`",
                                      tab->name ? tab->name : "<anonymous>");
                        break;
                    case 4:    // problem with option column
                        KEYWORD_ERROR("[%s] column name or index not found in table `%s`", err,
                                      tab->name ? tab->name : "<anonymous>");
                        break;
                    }
                }
                if (opts.x_bool) {
                    niter = 1;
                    for (size_t i = 0; i < ncol; ++i)
                        niter *= nrows; // nrows ** ncol
                    if (niter > pp->iter_limit) {
                        KEYWORD_ERROR
                            ("[x] cartesian product with %zu elements exceeds -L/--iter-limit=%zu",
                             niter, pp->iter_limit);
                    }
                }
            }

            if (has_sep && i > 0)
                BUFCAT(opts.sep_str);   // separator

            if (opts.x_bool) {  // cartesian product
                // NOTE: prints directly instead of collecting rowbufs
                const int ncol = sv_len(selcols);
                assert(ncol > 0);
                size_t dims[ncol];      // VLA
                dims[0] = 1;
                for (int j = 1; j < ncol; ++j)
                    dims[j] = dims[j - 1] * nrows;

                for (size_t i = 0; i < niter; i++) {
                    size_t nr =
                        ((opts.ord_bool && opts.rev_bool) ? niter - i - 1 : i) + opts.add1_count;
                    const char *rowno = NULL;
                    if (has_rowno) {
                        char rowno_buf[STRUMAX_LEN];
                        rowno = uint_tostr(nr, rowno_buf);
                    }

                    size_t row_col[ncol];       // VLA
                    row_col[0] = order[i / dims[ncol - 1]];
                    size_t ix = i % dims[ncol - 1];
                    for (int j = 1, k = ncol - 2; j < ncol; ++j) {
                        if (j % 2 == 0) {
                            ix %= dims[k--];
                            row_col[j] = order[ix];
                        }
                        else
                            row_col[j] = order[ix / dims[k]];
                    }

                    const char *values[ncol];
                    for (int j = 0; j < ncol; ++j) {
                        const struct namarg *selcol = sv_at(selcols, j);
                        values[j] = table_getcell(tab, row_col[j], CMOD_ARG_ARGLINK_get(*selcol));
                    }

                    size_t buflen = 0;
                    char *buf = snip_get_buf_rowno(&snip, ncol, values, &buflen, rowno);
                    if (has_sep && i > 0)
                        BUFCAT(opts.sep_str);   // separator
                    BUFCATN(buf, buflen - 1);   // NOTE: discounting NUL
                    if (has_brk && ntot % brklen == 0)
                        BUFCAT(opts.brk_str);   // group separator
                    ++(ntot);
                    free(buf);
                }
            }
            else {      // collect and print rowbufs
                srt_vector *rowbuflens = NULL;
                vec_string *rowbufs = NULL;
                if (map_table_lambda
                    (tab, &snip, selcols, nrows, order, has_rowno, &opts, &rowbufs, &rowbuflens))
                    YYNOMEM;
                {
                    const size_t _len = sv_len(rowbufs);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _22 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        size_t len = sv_at_u64(rowbuflens, _22);
                        if (has_sep && _22 > 0)
                            BUFCAT(opts.sep_str);       // separator
                        BUFCATN((*(const char **)sv_at(rowbufs, _22)), len - 1);        // NOTE: discounting NUL
                        if (has_brk && ntot % brklen == 0)
                            BUFCAT(opts.brk_str);       // group separator
                        ++(ntot);
                    }
                }
                {
                    for (size_t i = 0; i < sv_len(rowbufs); ++i) {
                        const void *_item = sv_at(rowbufs, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(rowbufs));
                }
                sv_free(&rowbuflens);
            }

            free(order);
            sv_free(&selcols);

            snip.name = NULL;   // unborrow
            snip.body = NULL;   // unborrow
            snip.sv_argl = NULL;        // unborrow
            sv_free(&snip.sv_forl);     // unborrow contents
            snippet_clear(&snip);
        }
        if (has_brk && (ntot - 1) % brklen != 0)
            BUFCAT(opts.brk_str);       // group separator

        if (is_lambda_table)
            table_clear(tablist[0]);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op
        free(tablist);

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&(opts.sep_str));
    ss_free(&(opts.brk_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int once_rule0_str(const struct param *const pp,
                          size_t debug_level,
                          const struct once_cmod_opts *const opts,
                          char **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    once_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    return 0;
}

int cmod_once_action_0(void *yyscanner,
                       struct param *pp,
                       vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                       char **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct once_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(once_opt_names) / sizeof(*(once_opt_names)));
        const char **opt_names = once_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "oskip", 6) == 0) {
            ret = option_list_action_bool(&opts.noskip_bool, opt, &opts.noskip_isset);

        }
        else {
            goto cmod_strin_else_10;
 cmod_strin_else_10:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (once_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      once_opt_names[icuropt], once_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        once_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        bool found; {
            const srt_string *_ss = ss_crefs((*x3));
            found = sms_count_s(pp->sms_once, _ss);
        }

        if (!found) {   // not previously defined
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (!sms_insert_s(&pp->sms_once, _ss)) {
                    YYNOMEM;
                }
            }
        }
        else if (!opts.noskip_bool) {   // include guard, skip file
            INFO("skipping previously included file with guard `%s`", (*x3));
            ret_t ret = popfile(yyscanner, pp); // pop it
            if (ret) {
                check_ret_t(ret, (const struct ret_f *)&popfile_ret_f);

            }
            if (NULL == pp->curbs) {    // no more buffers, finish parsing
                free((*x3));
                (*x3) = NULL;
                ss_free(kwbuf);
                YYACCEPT;
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

#ifndef __CYGWIN__
pid_t cpid = -1;

static void sigalrm_sigaction(int sig, siginfo_t *info, void *ucontext)
{
    (void)sig;
    (void)info;
    (void)ucontext;
    _Exit(EXIT_FAILURE);
}

#if HAVE_LIBSECCOMP
static void sigsys_sigaction(int sig, siginfo_t *info, void *ucontext)
{
    (void)sig;
    (void)ucontext;
    if (info->si_code == SYS_SECCOMP) {
        char *syscall_name = seccomp_syscall_resolve_num_arch(0, info->si_syscall);
        error("%%pipe: child PID %d made forbidden syscall %s(%d)",
              cpid, syscall_name, info->si_syscall);
        free(syscall_name);
    }
    else
        error("%%pipe: child PID %d made bad system call %d", cpid, info->si_syscall);
    _exit(EXIT_FAILURE);
}
#endif

// parse env vars, store consecutive (name, value) pairs
static int parse_env_vars(const vec_string *env_list, vec_string *env_vars[static 1])
{
    size_t list_len = sv_len(env_list);
    ((*env_vars)) = sv_alloc_t(SV_PTR, (2 * list_len));
    if (sv_void == ((*env_vars))) {
        return 1;
    }
    for (size_t i = 0; i < list_len; i++) {
        char *name = sv_at_ptr(env_list, i);    // before equals sign

        char *eq = strchr(name, '=');   // first equals sign
        if (NULL == eq)
            return 2;
        *eq = '\0';     // set equals to NUL

        char *value = eq + 1;   // after equals sign

        if (!sv_push(&(*env_vars), &(name))) {
            return 1;
        }
        if (!sv_push(&(*env_vars), &(value))) {
            return 1;
        }
    }
    return 0;
}

enum cmdexec {
    CMDEXEC_OK = 0,     /* default */
    CMDEXEC_P2C = -1,   /* failed creating parent-to-child pipe */
    CMDEXEC_P2C_WRITE = -2,     /* failed opening parent-to-child pipe for write */
    CMDEXEC_C2P = -3,   /* failed creating child-to-parent pipe */
    CMDEXEC_C2P_READ = -4,      /* failed opening child-to-parent pipe for read */
    CMDEXEC_FORK = -5,  /* failed forking child process */
    CMDEXEC_READ = -6,  /* failed reading command output */
    CMDEXEC_WRITE = -7, /* failed writing command input */
    CMDEXEC_SELECT = -8,        /* failed polling child stdout */
    CMDEXEC_SELECT_TIMEOUT = -9,        /* failed polling child stdout, timed out */
    CMDEXEC_WAIT = -10, /* failed retrieving child status */
    CMDEXEC_NONZERO = -11,      /* failed executing command, exit status */
    CMDEXEC_KILLED = -12,       /* failed executing command, killed by signal */
};
#define enum_cmdexec_len 12
static int cmd_exec_inout(const vec_string *restrict cmd_parts, const char *command,
                          const srt_string *text, const vec_string *restrict env_vars,
                          const vec_string *restrict allow_list, const int timeout,
                          const struct param *pp, const bool keepenv, const bool do_trace,
                          const bool strict, const bool unsafe, srt_string *recmsg[static 1])
{
    int fd_p2c[2];      // parent-to-child fds
    int fd_c2p[2];      // child-to-parent fds
    errno = 0;
    if (pipe(fd_p2c) < 0)
        return CMDEXEC_P2C;
    errno = 0;
    if (pipe(fd_c2p) < 0)
        return CMDEXEC_C2P;

    errno = 0;
    cpid = fork();      // child PID
    if (cpid < 0)
        return CMDEXEC_FORK;

    if (cpid == 0) {    // child
        /* setup SIGALRM signal handler */
        struct sigaction sa = {
            .sa_sigaction = sigalrm_sigaction,
            .sa_flags = SA_SIGINFO
        };
        sigemptyset(&sa.sa_mask);
        sigaction(SIGALRM, &sa, NULL);

        alarm(timeout); // set execution timeout to avoid hangups

        cpid = getpid();        // get child PID
        CHLD_VERBOSE0("spawned");

        errno = 0;
        if (dup2(fd_p2c[0], STDIN_FILENO) < 0) {        // set stdin to parent-to-child pipe read end
            CHLD_FATAL_ERROR("failed setting up stdin: %s (%d)", strerror(errno), errno);
        }
        close(fd_p2c[0]);       // close just duplicated fd
        close(fd_p2c[1]);       // close parent-to-child pipe write end

        errno = 0;
        if (dup2(fd_c2p[1], STDOUT_FILENO) < 0) {       // set stdout to child-to-parent pipe write end
            CHLD_FATAL_ERROR("failed setting up stdout: %s (%d)", strerror(errno), errno);
        }
        close(fd_c2p[1]);       // close just duplicated fd
        close(fd_c2p[0]);       // close child-to-parent pipe read end

        if (!keepenv) { // clear environment by default (CERT ENV03-C)
            for (char **x = environ; NULL != x && NULL != *x; ++x) {
                const char *envvar = *x;
                const char *eq = strchr(envvar, '=');   // find equals in NAME=VALUE
                if (NULL == eq)
                    continue;   // no equals, env is fishy, keep going
                intptr_t len = eq - envvar;
                char buf[len + 1];      // VLA
                memcpy(buf, envvar, len);       // copy NAME to buffer
                buf[len] = '\0';        // NUL-terminate
                int ret = unsetenv(buf);
                assert(ret == 0);
                (void)ret;
            }
        }

        /* setup user environment variables */
        for (size_t i = 0; i < sv_len(env_vars); i += 2) {
            const char *name = sv_at_ptr(env_vars, i);
            const char *value = sv_at_ptr(env_vars, i + 1);
            errno = 0;
            if (setenv(name, value, true) < 0)
                CHLD_FATAL_ERROR("failed setting environment variable %s = %s: %s (%d)",
                                 name, value, strerror(errno), errno);
        }

#if HAVE_LIBSECCOMP
        if (do_trace)
            ptrace(PTRACE_TRACEME, 0, NULL, NULL);      // allow trace by parent

        if (!unsafe) {  // setup seccomp filter
            /* setup SIGSYS signal handler */
            struct sigaction sa = {
                .sa_sigaction = sigsys_sigaction,
                .sa_flags = SA_SIGINFO
            };
            sigemptyset(&sa.sa_mask);
            sigaction(SIGSYS, &sa, NULL);

            int rc;
            scmp_filter_ctx ctx;
            /* init seccomp */
            if (NULL == (ctx = seccomp_init(SCMP_ACT_TRAP)))
                CHLD_FATAL_ERROR0("failed setting up seccomp context");

            /* allow write to stdout and stderr */
            if ((rc =
                 seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 1,
                                  SCMP_A0(SCMP_CMP_EQ, STDOUT_FILENO))) < 0)
                CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d) (%s): %s (%d)",
                                 "write", SCMP_SYS(write), "stdout", strerror(rc), rc);
            if ((rc =
                 seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 1,
                                  SCMP_A0(SCMP_CMP_EQ, STDERR_FILENO))) < 0)
                CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d) (%s): %s (%d)",
                                 "write", SCMP_SYS(write), "stderr", strerror(rc), rc);

            /* setup strict rules */
            static const char *strict_rule_name[] =
                { "access", "arch_prctl", "brk", "close", "execve", "exit_group", "fstat", "mmap",
"mprotect", "munmap", "newfstatat", "openat", "pread64", "prlimit64", "read", "rseq", "set_robust_list",
"set_tid_address" };
            static const int strict_rule[] =
                { SCMP_SYS(access), SCMP_SYS(arch_prctl), SCMP_SYS(brk), SCMP_SYS(close),
SCMP_SYS(execve), SCMP_SYS(exit_group), SCMP_SYS(fstat), SCMP_SYS(mmap), SCMP_SYS(mprotect),
SCMP_SYS(munmap), SCMP_SYS(newfstatat), SCMP_SYS(openat), SCMP_SYS(pread64), SCMP_SYS(prlimit64),
SCMP_SYS(read), SCMP_SYS(rseq), SCMP_SYS(set_robust_list), SCMP_SYS(set_tid_address) };
            for (size_t i = 0; i < 18; ++i) {
                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, strict_rule[i], 0)) < 0)
                    CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                     strict_rule_name[i], strict_rule[i], strerror(rc), rc);
            }

            /* setup base rules */
            static const char *base_rule_name[] =
                { "dup", "dup2", "fadvise64", "fcntl", "futex", "getcwd", "getdents64", "getegid",
"geteuid", "getgid", "getpgrp", "getpid", "getppid", "getrandom", "getuid", "ioctl", "lseek", "readlink",
"rt_sigaction", "rt_sigprocmask", "rt_sigreturn", "sysinfo", "uname" };
            static const int base_rule[] =
                { SCMP_SYS(dup), SCMP_SYS(dup2), SCMP_SYS(fadvise64), SCMP_SYS(fcntl),
SCMP_SYS(futex), SCMP_SYS(getcwd), SCMP_SYS(getdents64), SCMP_SYS(getegid), SCMP_SYS(geteuid),
SCMP_SYS(getgid), SCMP_SYS(getpgrp), SCMP_SYS(getpid), SCMP_SYS(getppid), SCMP_SYS(getrandom),
SCMP_SYS(getuid), SCMP_SYS(ioctl), SCMP_SYS(lseek), SCMP_SYS(readlink), SCMP_SYS(rt_sigaction),
SCMP_SYS(rt_sigprocmask), SCMP_SYS(rt_sigreturn), SCMP_SYS(sysinfo), SCMP_SYS(uname) };
            if (!strict) {
                for (size_t i = 0; i < 23; ++i) {
                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, base_rule[i], 0)) < 0)
                        CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         base_rule_name[i], base_rule[i], strerror(rc), rc);
                }
            }

            /* syscall classes */
            static const char *class_names[] =
                { "desc", "file", "ipc", "network", "process", "signal", "memory", "stat", "statfs",
"pure", "creds", "clock" };
            static const char *class_syscalls_desc[] = {
                "read", "write", "open", "close", "fstat", "poll", "lseek", "mmap", "ioctl",
                    "pread64", "pwrite64", "readv", "writev", "pipe", "select", "dup", "dup2",
                    "sendfile", "fcntl", "flock", "fsync", "fdatasync", "ftruncate", "getdents",
                    "fchdir", "creat", "fchmod", "fchown", "fstatfs", "readahead", "fsetxattr",
                    "fgetxattr", "flistxattr", "fremovexattr", "epoll_create", "getdents64",
                    "fadvise64", "epoll_wait", "epoll_ctl", "mq_open", "mq_timedsend",
                    "mq_timedreceive", "mq_notify", "mq_getsetattr", "inotify_init",
                    "inotify_add_watch", "inotify_rm_watch", "openat", "mkdirat", "mknodat",
                    "fchownat", "futimesat", "newfstatat", "unlinkat", "renameat", "linkat",
                    "symlinkat", "readlinkat", "fchmodat", "faccessat", "pselect6", "ppoll",
                    "splice", "tee", "sync_file_range", "vmsplice", "utimensat", "epoll_pwait",
                    "signalfd", "timerfd_create", "eventfd", "fallocate", "timerfd_settime",
                    "timerfd_gettime", "signalfd4", "eventfd2", "epoll_create1", "dup3", "pipe2",
                    "inotify_init1", "preadv", "pwritev", "perf_event_open", "fanotify_init",
                    "fanotify_mark", "name_to_handle_at", "open_by_handle_at", "syncfs", "setns",
                    "finit_module", "renameat2", "memfd_create", "kexec_file_load", "bpf",
                    "execveat", "userfaultfd", "copy_file_range", "preadv2", "pwritev2", "statx",
                    "pidfd_send_signal", "io_uring_setup", "io_uring_enter", "io_uring_register",
                    "open_tree", "move_mount", "fsopen", "fsconfig", "fsmount", "fspick",
                    "pidfd_open", "openat2", "pidfd_getfd", "faccessat2", "process_madvise",
                    "epoll_pwait2", "mount_setattr", "quotactl_fd", "landlock_create_ruleset",
                    "landlock_add_rule", "landlock_restrict_self", "memfd_secret",
                    "process_mrelease"
            };
            static const char *class_syscalls_file[] = {
                "open", "stat", "lstat", "access", "execve", "truncate", "getcwd", "chdir",
                    "rename", "mkdir", "rmdir", "creat", "link", "unlink", "symlink", "readlink",
                    "chmod", "chown", "lchown", "utime", "mknod", "uselib", "statfs", "pivot_root",
                    "chroot", "acct", "mount", "umount2", "swapon", "swapoff", "quotactl",
                    "setxattr", "lsetxattr", "getxattr", "lgetxattr", "listxattr", "llistxattr",
                    "removexattr", "lremovexattr", "utimes", "inotify_add_watch", "openat",
                    "mkdirat", "mknodat", "fchownat", "futimesat", "newfstatat", "unlinkat",
                    "renameat", "linkat", "symlinkat", "readlinkat", "fchmodat", "faccessat",
                    "utimensat", "fanotify_mark", "name_to_handle_at", "renameat2", "execveat",
                    "statx", "open_tree", "move_mount", "fsconfig", "fspick", "openat2",
                    "faccessat2", "mount_setattr"
            };
            static const char *class_syscalls_ipc[] = {
                "shmget", "shmat", "shmctl", "semget", "semop", "semctl", "shmdt", "msgget",
                    "msgsnd", "msgrcv", "msgctl", "semtimedop"
            };
            static const char *class_syscalls_network[] = {
                "sendfile", "socket", "connect", "accept", "sendto", "recvfrom", "sendmsg",
                    "recvmsg", "shutdown", "bind", "listen", "getsockname", "getpeername",
                    "socketpair", "setsockopt", "getsockopt", "getpmsg", "putpmsg", "accept4",
                    "recvmmsg", "sendmmsg"
            };
            static const char *class_syscalls_process[] = {
                "clone", "fork", "vfork", "execve", "exit", "wait4", "kill", "rt_sigqueueinfo",
                    "tkill", "exit_group", "tgkill", "waitid", "rt_tgsigqueueinfo", "execveat",
                    "pidfd_send_signal", "clone3"
            };
            static const char *class_syscalls_signal[] = {
                "rt_sigaction", "rt_sigprocmask", "rt_sigreturn", "pause", "kill", "rt_sigpending",
                    "rt_sigtimedwait", "rt_sigqueueinfo", "rt_sigsuspend", "sigaltstack", "tkill",
                    "tgkill", "signalfd", "signalfd4", "rt_tgsigqueueinfo", "pidfd_send_signal",
                    "io_uring_enter"
            };
            static const char *class_syscalls_memory[] = {
                "mmap", "mprotect", "munmap", "brk", "mremap", "msync", "mincore", "madvise",
                    "shmat", "shmdt", "mlock", "munlock", "mlockall", "munlockall", "io_setup",
                    "io_destroy", "remap_file_pages", "mbind", "set_mempolicy", "get_mempolicy",
                    "migrate_pages", "move_pages", "mlock2", "pkey_mprotect", "io_uring_register",
                    "set_mempolicy_home_node"
            };
            static const char *class_syscalls_stat[] = {
                "stat", "fstat", "lstat", "newfstatat", "statx"
            };
            static const char *class_syscalls_statfs[] = {
                "ustat", "statfs", "fstatfs"
            };
            static const char *class_syscalls_pure[] = {
                "getpid", "getuid", "getgid", "geteuid", "getegid", "getppid", "getpgrp", "gettid"
            };
            static const char *class_syscalls_creds[] = {
                "getuid", "getgid", "setuid", "setgid", "geteuid", "getegid", "setreuid",
                    "setregid", "getgroups", "setgroups", "setresuid", "getresuid", "setresgid",
                    "getresgid", "setfsuid", "setfsgid", "capget", "capset", "prctl"
            };
            static const char *class_syscalls_clock[] = {
                "gettimeofday", "adjtimex", "settimeofday", "time", "clock_settime",
                    "clock_gettime", "clock_getres", "clock_adjtime"
            };

            /* setup [allow] rules */
            for (size_t i = 0; i < sv_len(allow_list); ++i) {
                int rc, sc;
                const char *sc_name = sv_at_ptr(allow_list, i);
                if (':' == sc_name[0]) {        // by class
                    const char *class_name = sc_name + 1;       // skip initial colon
                    if ('c' == *((class_name) + 0)) {
                        if ('l' == *((class_name) + 1) && strncmp((class_name) + 2, "ock", 4) == 0) {
                            {
                                const size_t _len =
                                    (sizeof(class_syscalls_clock) /
                                     sizeof(*(class_syscalls_clock)));
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _1 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    if ((sc =
                                         seccomp_syscall_resolve_name((((class_syscalls_clock))
                                                                       [_1]))) == __NR_SCMP_ERROR) {
                                        CHLD_VERBOSE
                                            ("name resolution failed for syscall `%s` in class `:%s`",
                                             (((class_syscalls_clock))[_1]), class_name);
                                        continue;
                                    }
                                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                        CHLD_FATAL_ERROR
                                            ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                             (((class_syscalls_clock))[_1]), sc, strerror(rc), rc);
                                }
                            }

                        }
                        else if ('r' == *((class_name) + 1)
                                 && strncmp((class_name) + 2, "eds", 4) == 0) {
                            {
                                const size_t _len =
                                    (sizeof(class_syscalls_creds) /
                                     sizeof(*(class_syscalls_creds)));
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _2 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    if ((sc =
                                         seccomp_syscall_resolve_name((((class_syscalls_creds))
                                                                       [_2]))) == __NR_SCMP_ERROR) {
                                        CHLD_VERBOSE
                                            ("name resolution failed for syscall `%s` in class `:%s`",
                                             (((class_syscalls_creds))[_2]), class_name);
                                        continue;
                                    }
                                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                        CHLD_FATAL_ERROR
                                            ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                             (((class_syscalls_creds))[_2]), sc, strerror(rc), rc);
                                }
                            }

                        }
                        else {
                            goto cmod_strin_else_0;
                        }
                    }
                    else if ('d' == *((class_name) + 0) && strncmp((class_name) + 1, "esc", 4) == 0) {
                        {
                            const size_t _len =
                                (sizeof(class_syscalls_desc) / sizeof(*(class_syscalls_desc)));
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _3 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if ((sc =
                                     seccomp_syscall_resolve_name((((class_syscalls_desc))[_3]))) ==
                                    __NR_SCMP_ERROR) {
                                    CHLD_VERBOSE
                                        ("name resolution failed for syscall `%s` in class `:%s`",
                                         (((class_syscalls_desc))[_3]), class_name);
                                    continue;
                                }
                                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                    CHLD_FATAL_ERROR
                                        ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         (((class_syscalls_desc))[_3]), sc, strerror(rc), rc);
                            }
                        }

                    }
                    else if ('f' == *((class_name) + 0) && strncmp((class_name) + 1, "ile", 4) == 0) {
                        {
                            const size_t _len =
                                (sizeof(class_syscalls_file) / sizeof(*(class_syscalls_file)));
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _4 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if ((sc =
                                     seccomp_syscall_resolve_name((((class_syscalls_file))[_4]))) ==
                                    __NR_SCMP_ERROR) {
                                    CHLD_VERBOSE
                                        ("name resolution failed for syscall `%s` in class `:%s`",
                                         (((class_syscalls_file))[_4]), class_name);
                                    continue;
                                }
                                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                    CHLD_FATAL_ERROR
                                        ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         (((class_syscalls_file))[_4]), sc, strerror(rc), rc);
                            }
                        }

                    }
                    else if ('i' == *((class_name) + 0) && 'p' == *((class_name) + 1)
                             && 'c' == *((class_name) + 2) && '\0' == *((class_name) + 3)) {
                        {
                            const size_t _len =
                                (sizeof(class_syscalls_ipc) / sizeof(*(class_syscalls_ipc)));
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _5 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if ((sc =
                                     seccomp_syscall_resolve_name((((class_syscalls_ipc))[_5]))) ==
                                    __NR_SCMP_ERROR) {
                                    CHLD_VERBOSE
                                        ("name resolution failed for syscall `%s` in class `:%s`",
                                         (((class_syscalls_ipc))[_5]), class_name);
                                    continue;
                                }
                                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                    CHLD_FATAL_ERROR
                                        ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         (((class_syscalls_ipc))[_5]), sc, strerror(rc), rc);
                            }
                        }

                    }
                    else if ('m' == *((class_name) + 0)
                             && strncmp((class_name) + 1, "emory", 6) == 0) {
                        {
                            const size_t _len =
                                (sizeof(class_syscalls_memory) / sizeof(*(class_syscalls_memory)));
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _6 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if ((sc =
                                     seccomp_syscall_resolve_name((((class_syscalls_memory))[_6])))
                                    == __NR_SCMP_ERROR) {
                                    CHLD_VERBOSE
                                        ("name resolution failed for syscall `%s` in class `:%s`",
                                         (((class_syscalls_memory))[_6]), class_name);
                                    continue;
                                }
                                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                    CHLD_FATAL_ERROR
                                        ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         (((class_syscalls_memory))[_6]), sc, strerror(rc), rc);
                            }
                        }

                    }
                    else if ('n' == *((class_name) + 0)
                             && strncmp((class_name) + 1, "etwork", 7) == 0) {
                        {
                            const size_t _len =
                                (sizeof(class_syscalls_network) /
                                 sizeof(*(class_syscalls_network)));
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _7 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if ((sc =
                                     seccomp_syscall_resolve_name((((class_syscalls_network))[_7])))
                                    == __NR_SCMP_ERROR) {
                                    CHLD_VERBOSE
                                        ("name resolution failed for syscall `%s` in class `:%s`",
                                         (((class_syscalls_network))[_7]), class_name);
                                    continue;
                                }
                                if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                    CHLD_FATAL_ERROR
                                        ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         (((class_syscalls_network))[_7]), sc, strerror(rc), rc);
                            }
                        }

                    }
                    else if ('p' == *((class_name) + 0)) {
                        if ('r' == *((class_name) + 1)
                            && strncmp((class_name) + 2, "ocess", 6) == 0) {
                            {
                                const size_t _len =
                                    (sizeof(class_syscalls_process) /
                                     sizeof(*(class_syscalls_process)));
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _8 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    if ((sc =
                                         seccomp_syscall_resolve_name((((class_syscalls_process))
                                                                       [_8]))) == __NR_SCMP_ERROR) {
                                        CHLD_VERBOSE
                                            ("name resolution failed for syscall `%s` in class `:%s`",
                                             (((class_syscalls_process))[_8]), class_name);
                                        continue;
                                    }
                                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                        CHLD_FATAL_ERROR
                                            ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                             (((class_syscalls_process))[_8]), sc, strerror(rc),
                                             rc);
                                }
                            }

                        }
                        else if ('u' == *((class_name) + 1) && 'r' == *((class_name) + 2)
                                 && 'e' == *((class_name) + 3) && '\0' == *((class_name) + 4)) {
                            {
                                const size_t _len =
                                    (sizeof(class_syscalls_pure) / sizeof(*(class_syscalls_pure)));
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _9 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    if ((sc =
                                         seccomp_syscall_resolve_name((((class_syscalls_pure))
                                                                       [_9]))) == __NR_SCMP_ERROR) {
                                        CHLD_VERBOSE
                                            ("name resolution failed for syscall `%s` in class `:%s`",
                                             (((class_syscalls_pure))[_9]), class_name);
                                        continue;
                                    }
                                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                        CHLD_FATAL_ERROR
                                            ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                             (((class_syscalls_pure))[_9]), sc, strerror(rc), rc);
                                }
                            }

                        }
                        else {
                            goto cmod_strin_else_0;
                        }
                    }
                    else if ('s' == *((class_name) + 0)) {
                        if ('i' == *((class_name) + 1) && strncmp((class_name) + 2, "gnal", 5) == 0) {
                            {
                                const size_t _len =
                                    (sizeof(class_syscalls_signal) /
                                     sizeof(*(class_syscalls_signal)));
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _10 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    if ((sc =
                                         seccomp_syscall_resolve_name((((class_syscalls_signal))
                                                                       [_10]))) ==
                                        __NR_SCMP_ERROR) {
                                        CHLD_VERBOSE
                                            ("name resolution failed for syscall `%s` in class `:%s`",
                                             (((class_syscalls_signal))[_10]), class_name);
                                        continue;
                                    }
                                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                        CHLD_FATAL_ERROR
                                            ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                             (((class_syscalls_signal))[_10]), sc, strerror(rc),
                                             rc);
                                }
                            }

                        }
                        else if ('t' == *((class_name) + 1) && 'a' == *((class_name) + 2)
                                 && 't' == *((class_name) + 3)) {
                            if ('\0' == *((class_name) + 4)) {
                                {
                                    const size_t _len =
                                        (sizeof(class_syscalls_stat) /
                                         sizeof(*(class_syscalls_stat)));
                                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                                        const size_t _11 = _idx;
                                        const size_t _idx = 0;
                                        (void)_idx;
                                        if ((sc =
                                             seccomp_syscall_resolve_name((((class_syscalls_stat))
                                                                           [_11]))) ==
                                            __NR_SCMP_ERROR) {
                                            CHLD_VERBOSE
                                                ("name resolution failed for syscall `%s` in class `:%s`",
                                                 (((class_syscalls_stat))[_11]), class_name);
                                            continue;
                                        }
                                        if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                            CHLD_FATAL_ERROR
                                                ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                                 (((class_syscalls_stat))[_11]), sc, strerror(rc),
                                                 rc);
                                    }
                                }

                            }
                            else if ('f' == *((class_name) + 4) && 's' == *((class_name) + 5)
                                     && '\0' == *((class_name) + 6)) {
                                {
                                    const size_t _len =
                                        (sizeof(class_syscalls_statfs) /
                                         sizeof(*(class_syscalls_statfs)));
                                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                                        const size_t _12 = _idx;
                                        const size_t _idx = 0;
                                        (void)_idx;
                                        if ((sc =
                                             seccomp_syscall_resolve_name((((class_syscalls_statfs))
                                                                           [_12]))) ==
                                            __NR_SCMP_ERROR) {
                                            CHLD_VERBOSE
                                                ("name resolution failed for syscall `%s` in class `:%s`",
                                                 (((class_syscalls_statfs))[_12]), class_name);
                                            continue;
                                        }
                                        if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                                            CHLD_FATAL_ERROR
                                                ("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                                 (((class_syscalls_statfs))[_12]), sc, strerror(rc),
                                                 rc);
                                    }
                                }

                            }
                            else {
                                goto cmod_strin_else_0;
                            }
                        }
                        else {
                            goto cmod_strin_else_0;
                        }
                    }
                    else {
                        goto cmod_strin_else_0;
 cmod_strin_else_0:    ;
                        const char *fuzzy_match;
                        bool is_close;
                        is_close =
                            str_fuzzy_match(class_name, class_names,
                                            (sizeof(class_names) / sizeof(*(class_names))),
                                            strget_array, 0.75, &(fuzzy_match));

                        if (is_close) {
                            CHLD_FATAL_ERROR("unknown syscall class `:%s`, did you mean `:%s`?",
                                             class_name, fuzzy_match);
                        }
                        else {
                            CHLD_FATAL_ERROR("unknown syscall class `:%s`", class_name);
                        }

                    }
                }
                else if (strtoint_check(sc_name, &sc) == 0) {   // by number
                    char *sc_name_num = NULL;
                    if (NULL == (sc_name_num = seccomp_syscall_resolve_num_arch(0, sc)))
                        CHLD_FATAL_ERROR("syscall %d is not available", sc);
                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                        CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d): %s (%d)", sc_name_num, sc, strerror(rc), rc);  // LEAK: sc_name_num
                    free(sc_name_num);
                }
                else {  // by name
                    if ((sc = seccomp_syscall_resolve_name(sc_name)) == __NR_SCMP_ERROR)
                        CHLD_FATAL_ERROR("name resolution failed for syscall `%s`", sc_name);
                    if ((rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, sc, 0)) < 0)
                        CHLD_FATAL_ERROR("failed adding seccomp rule for syscall %s(%d): %s (%d)",
                                         sc_name, sc, strerror(rc), rc);
                }
            }

            /* load filter */
            if ((rc = seccomp_load(ctx)) < 0)
                CHLD_FATAL_ERROR("failed loading seccomp filter: %s (%d)", strerror(rc), rc);
        }
#else
        (void)allow_list;
        (void)do_trace;
        (void)strict;
        (void)unsafe;
#endif

        /* execute command */
        const char *cmd_name = sv_at_ptr(cmd_parts, 0);
        char *const *cmd_args = (char *const *)sv_get_buffer_r(cmd_parts);
        errno = 0;
        execvp(cmd_name, cmd_args);

        /* reachable only on exec error */
        CHLD_FATAL_ERROR("failed executing command `%s`: %s (%d)", command, strerror(errno), errno);
    }
    else {      // parent
        siginfo_t cinfo;

#if HAVE_LIBSECCOMP
        if (do_trace) {
            ptrace(PTRACE_SETOPTIONS, cpid, NULL, PTRACE_O_EXITKILL);
            {
                /* check child status */
                errno = 0;
                (cinfo).si_pid = 0;
                if (waitid(P_PID, cpid, &(cinfo), WEXITED | 0) == -1)
                    return CMDEXEC_WAIT;
                if ((cinfo).si_pid != 0) {
                }       // child exited
            }
            {
                /* check child status */
                errno = 0;
                (cinfo).si_pid = 0;
                if (waitid(P_PID, cpid, &(cinfo), WEXITED | 0) == -1)
                    return CMDEXEC_WAIT;
                if ((cinfo).si_pid != 0) {
                    goto end;
                }       // child exited
            }
        }
#endif

        close(fd_p2c[0]);       // close parent-to-child pipe read end
        close(fd_c2p[1]);       // close child-to-parent pipe write end

        {
            /* check child status */
            errno = 0;
            (cinfo).si_pid = 0;
            if (waitid(P_PID, cpid, &(cinfo), WEXITED | WNOHANG | WNOWAIT) == -1)
                return CMDEXEC_WAIT;
            if ((cinfo).si_pid != 0) {
                goto end;
            }   // child exited
        }
        {       // write to child stdin
            const size_t len = ss_len(text) - 1;        // exclude NUL byte
            if (len > 0) {
                errno = 0;
                ssize_t nwrite = write(fd_p2c[1], ss_get_buffer_r(text), len);
                if (nwrite < 0 || (size_t)nwrite != len)
                    return CMDEXEC_WRITE;
            }
            /* write newline */
            errno = 0;
            ssize_t nwrite = write(fd_p2c[1], &(char[1]) { '\n' }, 1);
            if (nwrite < 0 || (size_t)nwrite != 1)
                return CMDEXEC_WRITE;
            close(fd_p2c[1]);   // close parent-to-child write end
        }

        {
            /* check child status */
            errno = 0;
            (cinfo).si_pid = 0;
            if (waitid(P_PID, cpid, &(cinfo), WEXITED | WNOHANG | WNOWAIT) == -1)
                return CMDEXEC_WAIT;
            if ((cinfo).si_pid != 0) {
                goto end;
            }   // child exited
        }
        {       // read from child's stdout
            fd_set rfds;
            FD_ZERO(&rfds);
            FD_SET(fd_c2p[0], &rfds);   // child stdout
            struct timeval tv = {.tv_sec = timeout };
            errno = 0;
            int retval = select(1, &rfds, NULL, NULL, &tv);     // check child stdout for read
            // NOTE: may interfere with SIGCHLD
            if (retval == -1 && errno != EINTR) {
                return CMDEXEC_SELECT;
            }
            else if (retval == 0) {     // NOTE: this happens sporadically under make -j or valgrind
                return CMDEXEC_SELECT_TIMEOUT;
            }
            else {      // child stdout is ready
                char buf[128];
                ssize_t nread = 0;
                do {
                    errno = 0;
                    nread = read(fd_c2p[0], buf, sizeof(buf));
                    if (nread < 0)
                        return CMDEXEC_READ;
                    ss_cat_cn(recmsg, buf, nread);
                } while (nread > 0);
                close(fd_c2p[0]);       // close child-to-parent read end
            }
        }

        {
            /* check child status */
            errno = 0;
            (cinfo).si_pid = 0;
            if (waitid(P_PID, cpid, &(cinfo), WEXITED | 0) == -1)
                return CMDEXEC_WAIT;
            if ((cinfo).si_pid != 0) {
                goto end;
            }   // child exited
        }
 end:
        if (cinfo.si_code == CLD_EXITED) {
            if (cinfo.si_status == 0)
                return CMDEXEC_OK;
            else {
                errno = cinfo.si_status;        // hack to pass exit status
                return CMDEXEC_NONZERO;
            }
        }
        else {
            errno = cinfo.si_status;    // hack to pass exit status
            return CMDEXEC_KILLED;
        }
    }
}
#endif
__attribute__((unused))
static int pipe_rule0_str(const struct param *const pp,
                          size_t debug_level,
                          const struct pipe_cmod_opts *const opts,
                          char **x3, srt_string **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    pipe_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    hstr_str_repr(*x3, s);

    (void)x4;

    if (NULL != *x4) {
        ss_cat_cn1(s, ' ');
        ss_cat_c(s, "%" "{ ");
        if (debug_level >= 2)
            ss_cat_c(s, ss_to_c(*x4));
        else
            ss_cat_c(s, "<verbatim>");
        ss_cat_c(s, " %" "}");
    }

    return 0;
}

int cmod_pipe_action_0(void *yyscanner,
                       const struct param *pp,
                       vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                       char **x3, const YYLTYPE *loc3,
                       srt_string **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct pipe_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(pipe_opt_names) / sizeof(*(pipe_opt_names)));
        const char **opt_names = pipe_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "llow", 5) == 0) {
            ret = option_list_action_cidl(&opts.allow_cidl, opt, &opts.allow_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_strl(&opts.env_strl, opt, &opts.env_isset);

        }
        else if ('k' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eepenv", 7) == 0) {
            ret = option_list_action_bool(&opts.keepenv_bool, opt, &opts.keepenv_isset);

        }
        else if ('t' == *((opt->name) + 0) && strncmp((opt->name) + 1, "race", 5) == 0) {
            ret = option_list_action_bool(&opts.trace_bool, opt, &opts.trace_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nsafe", 6) == 0) {
            ret = option_list_action_bool(&opts.unsafe_bool, opt, &opts.unsafe_isset);

        }
        else if ('w' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ait", 4) == 0) {
            ret = option_list_action_index(&opts.wait_index, opt, &opts.wait_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1)
                 && 'r' == *((opt->name) + 2)) {
            if ('\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else if ('i' == *((opt->name) + 3) && 'c' == *((opt->name) + 4)
                     && 't' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                ret = option_list_action_bool(&opts.strict_bool, opt, &opts.strict_isset);

            }
            else {
                goto cmod_strin_else_11;
            }
        }
        else {
            goto cmod_strin_else_11;
 cmod_strin_else_11:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (pipe_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      pipe_opt_names[icuropt], pipe_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        pipe_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

#ifdef __CYGWIN__
        KEYWORD_ERROR0("not supported under Cygwin");
#else
        opts.unsafe_bool = opts.unsafe_bool || pp->disable_seccomp;
        opts.trace_bool = opts.trace_bool && !opts.unsafe_bool;

        if (geteuid() == 0) {
            ERROR0("privileged execution not supported, are you root?");
            exit(EXIT_FAILURE);
        }

        if (pp->disable_seccomp)
            WARN0("unsafe execution, libseccomp support disabled or not compiled-in");

        if (strisspace((*x3))) {
            WARN0("ignoring empty command");
            goto epilogue;
        }

        const int timeout = opts.wait_index.set ? (opts.wait_index.ix >= 1 ? opts.wait_index.ix : 5)
            : 5;        // seconds

        {       // trim whitespace off both ends
            srt_string *ss_cmd = ss_dup_c((*x3));
            if (NULL == ss_cmd)
                YYNOMEM;
            ss_trim(&ss_cmd);
            char *cmd = NULL;
            {
                const char *ctmp = ss_to_c(ss_cmd);
                cmd = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (cmd)) {
                }

            }
            ss_free(&(ss_cmd));
            if (NULL == cmd) {
                YYNOMEM;
            }

            free((*x3));
            (*x3) = cmd;
        }
        vec_string *cmd_parts = strsplit((*x3), isspace);       // split at spaces in-place
        if (NULL == cmd_parts)
            YYNOMEM;
        {
            char *null = NULL;
            if (!sv_push(&(cmd_parts), &(null))) {
                YYNOMEM;
            }
            ;
        }       // NULL-terminated array

        vec_string *env_vars = NULL;
        {       // parse env vars
            int ret = parse_env_vars(opts.env_strl, &env_vars);
            switch (ret) {
            case 0:
                for (size_t i = 0; i < sv_len(env_vars) / 2; i += 2) {
                    const char *name = sv_at_ptr(env_vars, i);
                    const char *value = sv_at_ptr(env_vars, i + 1);
                    VERBOSE("setting environment variable %s = %s", name, value);
                }
                break;
            case 1:
                YYNOMEM;
                break;
            case 2:
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                KEYWORD_ERROR0("invalid environment variable syntax, please use <key>=<value>");
                break;
            }
        }

        /* fix empty input */
        if (NULL == (*x4)) {    // none => alloc and add NUL
            ((*x4)) = ss_alloc(1);
            if (ss_void == ((*x4))) {
                YYNOMEM;
            }
            ss_cat_cn1(&(*x4), '\0');
        }
        else if (strisspace(ss_to_c((*x4)))) {  // empty => clear and add NUL
            ss_clear((*x4));
            ss_cat_cn1(&(*x4), '\0');
        }
#if DEBUG
        ss_write(stderr, (*x4), 0, ss_len((*x4)) - 1);
#endif

        srt_string *(recmsg) = ss_alloc(0);
        if (ss_void == (recmsg)) {
            YYNOMEM;
        }
        ;
#ifndef __CYGWIN__
        {       // execute command
            enum cmdexec ret = cmd_exec_inout(cmd_parts, (*x3), (*x4),
                                              env_vars, opts.allow_cidl,
                                              timeout, pp,
                                              opts.keepenv_bool, opts.trace_bool,
                                              opts.strict_bool, opts.unsafe_bool,
                                              &recmsg);
            if (ret != CMDEXEC_OK)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            switch (ret) {
            case CMDEXEC_OK:
                ss_cat(kwbuf, recmsg);
                break;
            case CMDEXEC_P2C:
                KEYWORD_ERROR("failed creating parent-to-child pipe" ": %s (%d)", strerror(errno),
                              errno);
                break;
            case CMDEXEC_P2C_WRITE:
                KEYWORD_ERROR("failed opening parent-to-child pipe for write" ": %s (%d)",
                              strerror(errno), errno);
                break;
            case CMDEXEC_C2P:
                KEYWORD_ERROR("failed creating child-to-parent pipe" ": %s (%d)", strerror(errno),
                              errno);
                break;
            case CMDEXEC_C2P_READ:
                KEYWORD_ERROR("failed opening child-to-parent pipe for read" ": %s (%d)",
                              strerror(errno), errno);
                break;
            case CMDEXEC_FORK:
                KEYWORD_ERROR("failed forking child process" ": %s (%d)", strerror(errno), errno);
                break;
            case CMDEXEC_WRITE:
                KEYWORD_ERROR("failed writing command input" ": %s (%d)", strerror(errno), errno);
                break;
            case CMDEXEC_SELECT:
                KEYWORD_ERROR("failed polling child stdout" ": %s (%d)", strerror(errno), errno);
                break;
            case CMDEXEC_WAIT:
                KEYWORD_ERROR("failed retrieving child status" ": %s (%d)", strerror(errno), errno);
                break;
            case CMDEXEC_READ:
                KEYWORD_ERROR0("failed reading command output");
                break;
            case CMDEXEC_SELECT_TIMEOUT:
                KEYWORD_ERROR("failed polling child stdout, timed out"
                              " after %d seconds", timeout);
                break;
            case CMDEXEC_NONZERO:
                KEYWORD_ERROR("failed executing command, exit status" " %d", errno);
                break;
            case CMDEXEC_KILLED:
#ifdef _GNU_SOURCE
                KEYWORD_ERROR("failed executing command, killed by signal" " SIG%s (%s)",
                              sigabbrev_np(errno), sigdescr_np(errno));
#else
                KEYWORD_ERROR("failed executing command, killed by signal" " %d (%s)",
                              errno, strsignal(errno));
#endif
                break;
            }
        }
#endif

        ss_free(&recmsg);

        sv_free(&cmd_parts);    // const unborrow
        sv_free(&env_vars);     // const unborrow
#endif

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    {
        for (size_t i = 0; i < sv_len(opts.env_strl); ++i) {
            const void *_item = sv_at(opts.env_strl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.env_strl));
    }

    {
        for (size_t i = 0; i < sv_len(opts.allow_cidl); ++i) {
            const void *_item = sv_at(opts.allow_cidl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.allow_cidl));
    }

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int pipe_rule1_str(const struct param *const pp,
                          size_t debug_level,
                          const struct pipe_cmod_opts *const opts,
                          char **x3, struct recall_like *x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    pipe_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    hstr_str_repr(*x3, s);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, x4->name, " ");
    if (NULL != x4->snip) {     // arguments were processed
        const size_t nout = SNIPPET_NARGOUT(x4->snip);
        if (nout > 0) {
            for (size_t i = 0; i < sv_len(x4->arg_lists); ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x4->arg_lists, i), SETIX_none, SETIX_set(nout), s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = 0; i < sv_len(x4->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x4->arg_lists, i), SETIX_set(nout), SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }
    else {      // arguments were not processed
        if (pp->nout > 0) {
            for (size_t i = 0; i < pp->nout; ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x4->arg_lists, i), SETIX_none, SETIX_none, s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = pp->nout; i < sv_len(x4->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x4->arg_lists, i), SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }

    return 0;
}

int cmod_pipe_action_1(void *yyscanner,
                       const struct param *pp,
                       vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                       char **x3, const YYLTYPE *loc3,
                       struct recall_like *x4, const YYLTYPE *loc4,
                       bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct pipe_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(pipe_opt_names) / sizeof(*(pipe_opt_names)));
        const char **opt_names = pipe_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "llow", 5) == 0) {
            ret = option_list_action_cidl(&opts.allow_cidl, opt, &opts.allow_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_strl(&opts.env_strl, opt, &opts.env_isset);

        }
        else if ('k' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eepenv", 7) == 0) {
            ret = option_list_action_bool(&opts.keepenv_bool, opt, &opts.keepenv_isset);

        }
        else if ('t' == *((opt->name) + 0) && strncmp((opt->name) + 1, "race", 5) == 0) {
            ret = option_list_action_bool(&opts.trace_bool, opt, &opts.trace_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nsafe", 6) == 0) {
            ret = option_list_action_bool(&opts.unsafe_bool, opt, &opts.unsafe_isset);

        }
        else if ('w' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ait", 4) == 0) {
            ret = option_list_action_index(&opts.wait_index, opt, &opts.wait_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1)
                 && 'r' == *((opt->name) + 2)) {
            if ('\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else if ('i' == *((opt->name) + 3) && 'c' == *((opt->name) + 4)
                     && 't' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                ret = option_list_action_bool(&opts.strict_bool, opt, &opts.strict_isset);

            }
            else {
                goto cmod_strin_else_12;
            }
        }
        else {
            goto cmod_strin_else_12;
 cmod_strin_else_12:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (pipe_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      pipe_opt_names[icuropt], pipe_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        pipe_rule1_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

#ifdef __CYGWIN__
        KEYWORD_ERROR0("not supported under Cygwin");
#else
        opts.unsafe_bool = opts.unsafe_bool || pp->disable_seccomp;
        opts.trace_bool = opts.trace_bool && !opts.unsafe_bool;

        if (geteuid() == 0) {
            ERROR0("privileged execution not supported, are you root?");
            exit(EXIT_FAILURE);
        }

        if (pp->disable_seccomp)
            WARN0("unsafe execution, libseccomp support disabled or not compiled-in");

        if (strisspace((*x3))) {
            WARN0("ignoring empty command");
            goto epilogue;
        }

        const int timeout = opts.wait_index.set ? (opts.wait_index.ix >= 1 ? opts.wait_index.ix : 5)
            : 5;        // seconds

        {       // trim whitespace off both ends
            srt_string *ss_cmd = ss_dup_c((*x3));
            if (NULL == ss_cmd)
                YYNOMEM;
            ss_trim(&ss_cmd);
            char *cmd = NULL;
            {
                const char *ctmp = ss_to_c(ss_cmd);
                cmd = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (cmd)) {
                }

            }
            ss_free(&(ss_cmd));
            if (NULL == cmd) {
                YYNOMEM;
            }

            free((*x3));
            (*x3) = cmd;
        }
        vec_string *cmd_parts = strsplit((*x3), isspace);       // split at spaces in-place
        if (NULL == cmd_parts)
            YYNOMEM;
        {
            char *null = NULL;
            if (!sv_push(&(cmd_parts), &(null))) {
                YYNOMEM;
            }
            ;
        }       // NULL-terminated array

        vec_string *env_vars = NULL;
        {       // parse env vars
            int ret = parse_env_vars(opts.env_strl, &env_vars);
            switch (ret) {
            case 0:
                for (size_t i = 0; i < sv_len(env_vars) / 2; i += 2) {
                    const char *name = sv_at_ptr(env_vars, i);
                    const char *value = sv_at_ptr(env_vars, i + 1);
                    VERBOSE("setting environment variable %s = %s", name, value);
                }
                break;
            case 1:
                YYNOMEM;
                break;
            case 2:
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                KEYWORD_ERROR0("invalid environment variable syntax, please use <key>=<value>");
                break;
            }
        }

        const struct snippet *snip = (*x4).snip;
        if (NULL == (snip)) {
            if (false) {        // ignore if optional
                VERBOSE("ignoring undefined snippet `%s`", (*x4).name);
                goto epilogue;
            }
            else if (false) {
                WARN("undefined snippet `%s`, delaying evaluation", (*x4).name);

                goto epilogue;
            }
            else {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x4).name;
                    mutpp->errlloc = (*loc4);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x4).name, pp->sv_snip, sv_len(pp->sv_snip),
                                        strget_array_snip, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown snippet `%s`, did you mean `%s`?", (*x4).name,
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown snippet `%s`", (*x4).name);
                    }
                }
            }
        }

        for (size_t i = 0; i < sv_len((*x4).arg_lists); ++i) {
            const vec_namarg *argvals = sv_at_ptr((*x4).arg_lists, i);

            /* check arguments */
            {
                /* recall-specific check */
                {
                    const size_t _len = sv_len(argvals);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _48 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        switch (((struct namarg *)sv_at(argvals, _48))->type) {
                        case ENUM_NAMARG(NOVALUE):     /* FALLTHROUGH */
                        case ENUM_NAMARG(VERBATIM):    /* FALLTHROUGH */
                            break;      // OK
                        case ENUM_NAMARG(EXPR):
                            {   // compute expression value
                                char *expr = NULL;
                                int ierr = 0;
                                int ret =
                                    compute_expression_value(CMOD_ARG_EXPR_get
                                                             (*
                                                              ((struct namarg *)
                                                               sv_at(argvals, _48))), &expr, &ierr);
                                if (ret)
                                    switch (ret) {
                                    case 1:
                                        YYNOMEM;
                                        break;
                                    case 2:
                                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                                        KEYWORD_ERROR("unexpected %s in argument expression",
                                                      namarg_type_strenum(ierr));
                                        break;
                                    }
                                namarg_free(((struct namarg *)sv_at(argvals, _48)));
                                *((struct namarg *)sv_at(argvals, _48)) = CMOD_ARG_VERBATIM_set(expr);  // set computed value
                            }
                            break;
                        default:
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                            KEYWORD_ERROR
                                ("unexpected %s argument at position %zu of argument list %zu",
                                 namarg_type_strenum(((struct namarg *)sv_at(argvals, _48))->type),
                                 _48, i);
                            break;
                        }
                    }
                }

                /* check for non-default arguments without value */
                {
                    const size_t _len = sv_len((snip)->sv_forl);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _49 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        const struct namarg *varg = (_49 < sv_len(argvals))
                            ? sv_at(argvals, _49) : NULL;
                        if (((struct namarg *)sv_at((snip)->sv_forl, _49))->type == CMOD_ARG_NOVALUE
                            && (NULL == varg || varg->type == CMOD_ARG_NOVALUE)) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                            KEYWORD_ERROR
                                ("no value provided for non-default argument `%s` of snippet `%s`",
                                 ((struct namarg *)sv_at((snip)->sv_forl, _49))->name,
                                 (snip)->name);
                        }
                    }
                }

                /* NOTE: We do not check for snippets taking arguments even when they do not use them at all.
                   This is to allow for such snippets to be called with a variable number of arguments. */
            }

            /* perform argument substitution */
            size_t buflen = 0;
            char *buf = NULL;
            if (snippet_subst(snip, argvals, &buf, &buflen) || NULL == buf)
                YYNOMEM;
            srt_string_ref sref;
            const srt_string *text = ss_ref_buf(&sref, buf, buflen);

            srt_string *(recmsg) = ss_alloc(0);
            if (ss_void == (recmsg)) {
                YYNOMEM;
            }
            ;
#ifndef __CYGWIN__
            {   // execute command
                enum cmdexec ret = cmd_exec_inout(cmd_parts, (*x3), text,
                                                  env_vars, opts.allow_cidl,
                                                  timeout, pp,
                                                  opts.keepenv_bool, opts.trace_bool,
                                                  opts.strict_bool, opts.unsafe_bool,
                                                  &recmsg);
                if (ret != CMDEXEC_OK)
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                switch (ret) {
                case CMDEXEC_OK:
                    ss_cat(kwbuf, recmsg);
                    break;
                case CMDEXEC_P2C:
                    KEYWORD_ERROR("failed creating parent-to-child pipe" ": %s (%d)",
                                  strerror(errno), errno);
                    break;
                case CMDEXEC_P2C_WRITE:
                    KEYWORD_ERROR("failed opening parent-to-child pipe for write" ": %s (%d)",
                                  strerror(errno), errno);
                    break;
                case CMDEXEC_C2P:
                    KEYWORD_ERROR("failed creating child-to-parent pipe" ": %s (%d)",
                                  strerror(errno), errno);
                    break;
                case CMDEXEC_C2P_READ:
                    KEYWORD_ERROR("failed opening child-to-parent pipe for read" ": %s (%d)",
                                  strerror(errno), errno);
                    break;
                case CMDEXEC_FORK:
                    KEYWORD_ERROR("failed forking child process" ": %s (%d)", strerror(errno),
                                  errno);
                    break;
                case CMDEXEC_WRITE:
                    KEYWORD_ERROR("failed writing command input" ": %s (%d)", strerror(errno),
                                  errno);
                    break;
                case CMDEXEC_SELECT:
                    KEYWORD_ERROR("failed polling child stdout" ": %s (%d)", strerror(errno),
                                  errno);
                    break;
                case CMDEXEC_WAIT:
                    KEYWORD_ERROR("failed retrieving child status" ": %s (%d)", strerror(errno),
                                  errno);
                    break;
                case CMDEXEC_READ:
                    KEYWORD_ERROR0("failed reading command output");
                    break;
                case CMDEXEC_SELECT_TIMEOUT:
                    KEYWORD_ERROR("failed polling child stdout, timed out"
                                  " after %d seconds", timeout);
                    break;
                case CMDEXEC_NONZERO:
                    KEYWORD_ERROR("failed executing command, exit status" " %d", errno);
                    break;
                case CMDEXEC_KILLED:
#ifdef _GNU_SOURCE
                    KEYWORD_ERROR("failed executing command, killed by signal" " SIG%s (%s)",
                                  sigabbrev_np(errno), sigdescr_np(errno));
#else
                    KEYWORD_ERROR("failed executing command, killed by signal" " %d (%s)",
                                  errno, strsignal(errno));
#endif
                    break;
                }
            }
#endif

            ss_free(&recmsg);

            free(buf);
        }

        sv_free(&cmd_parts);    // const unborrow
        sv_free(&env_vars);     // const unborrow
#endif

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    {
        for (size_t i = 0; i < sv_len(opts.env_strl); ++i) {
            const void *_item = sv_at(opts.env_strl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.env_strl));
    }

    {
        for (size_t i = 0; i < sv_len(opts.allow_cidl); ++i) {
            const void *_item = sv_at(opts.allow_cidl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.allow_cidl));
    }

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int recall_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct recall_cmod_opts *const opts,
                            struct recall_like *x3, char *x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    recall_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, x3->name, " ");
    if (NULL != x3->snip) {     // arguments were processed
        const size_t nout = SNIPPET_NARGOUT(x3->snip);
        if (nout > 0) {
            for (size_t i = 0; i < sv_len(x3->arg_lists); ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x3->arg_lists, i), SETIX_none, SETIX_set(nout), s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = 0; i < sv_len(x3->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x3->arg_lists, i), SETIX_set(nout), SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }
    else {      // arguments were not processed
        if (pp->nout > 0) {
            for (size_t i = 0; i < pp->nout; ++i) {
                ss_cat_cn1(s, '(');
                vec_namarg_str_repr(sv_at_ptr(x3->arg_lists, i), SETIX_none, SETIX_none, s);
                ss_cat_cn1(s, ')');
            }
            ss_cat_c(s, " <- ");
        }
        for (size_t i = pp->nout; i < sv_len(x3->arg_lists); ++i) {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(sv_at_ptr(x3->arg_lists, i), SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
        }
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, ('e' == *x4) ? "|" "%" : "\n");

    return 0;
}

int cmod_recall_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         struct recall_like *x3, const YYLTYPE *loc3,
                         char *x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct recall_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(recall_opt_names) / sizeof(*(recall_opt_names)));
        const char **opt_names = recall_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "mpty", 5) == 0) {
            ret = option_list_action_bool(&opts.empty_bool, opt, &opts.empty_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else {
            goto cmod_strin_else_13;
 cmod_strin_else_13:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (recall_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      recall_opt_names[icuropt], recall_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        recall_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        const bool shorthand = ('\n' != (*x4));
        const struct snippet *snip = (*x3).snip;
        if (NULL == (snip)) {
            if (opts.opt_bool) {        // ignore if optional
                VERBOSE("ignoring undefined snippet `%s`", (*x3).name);
                goto epilogue;
            }
            else if (opts.lazy_bool) {
                WARN("undefined snippet `%s`, delaying evaluation", (*x3).name);
                if (opts.str_isset)
                    kwbuf = kwbuf_save;
                recall_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                BUFPUTS(shorthand ? "%" "|" : pp->kwname);
                BUFCAT(str_repr);
                goto epilogue;
            }
            else {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x3).name;
                    mutpp->errlloc = (*loc3);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x3).name, pp->sv_snip, sv_len(pp->sv_snip),
                                        strget_array_snip, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown snippet `%s`, did you mean `%s`?", (*x3).name,
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown snippet `%s`", (*x3).name);
                    }
                }
            }
        }

        if (!opts.empty_bool && '\0' == ss_at(snip->body, 0)) {
            VERBOSE("ignoring empty or blank snippet `%s`", snip->name);
        }
        else {
            for (size_t i = 0; i < sv_len((*x3).arg_lists); ++i) {
                const vec_namarg *argvals = sv_at_ptr((*x3).arg_lists, i);

                /* check arguments */
                {
                    /* recall-specific check */
                    {
                        const size_t _len = sv_len(argvals);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _50 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            switch (((struct namarg *)sv_at(argvals, _50))->type) {
                            case ENUM_NAMARG(NOVALUE): /* FALLTHROUGH */
                            case ENUM_NAMARG(VERBATIM):        /* FALLTHROUGH */
                                break;  // OK
                            case ENUM_NAMARG(EXPR):
                                {       // compute expression value
                                    char *expr = NULL;
                                    int ierr = 0;
                                    int ret =
                                        compute_expression_value(CMOD_ARG_EXPR_get
                                                                 (*
                                                                  ((struct namarg *)
                                                                   sv_at(argvals, _50))), &expr,
                                                                 &ierr);
                                    if (ret)
                                        switch (ret) {
                                        case 1:
                                            YYNOMEM;
                                            break;
                                        case 2:
                                            ((struct param *)yyget_extra(yyscanner))->errlloc =
                                                (*loc3);
                                            KEYWORD_ERROR("unexpected %s in argument expression",
                                                          namarg_type_strenum(ierr));
                                            break;
                                        }
                                    namarg_free(((struct namarg *)sv_at(argvals, _50)));
                                    *((struct namarg *)sv_at(argvals, _50)) = CMOD_ARG_VERBATIM_set(expr);      // set computed value
                                }
                                break;
                            default:
                                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                                KEYWORD_ERROR
                                    ("unexpected %s argument at position %zu of argument list %zu",
                                     namarg_type_strenum(((struct namarg *)sv_at(argvals, _50))->
                                                         type), _50, i);
                                break;
                            }
                        }
                    }

                    /* check for non-default arguments without value */
                    {
                        const size_t _len = sv_len((snip)->sv_forl);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _51 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            const struct namarg *varg = (_51 < sv_len(argvals))
                                ? sv_at(argvals, _51) : NULL;
                            if (((struct namarg *)sv_at((snip)->sv_forl, _51))->type ==
                                CMOD_ARG_NOVALUE && (NULL == varg
                                                     || varg->type == CMOD_ARG_NOVALUE)) {
                                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                                KEYWORD_ERROR
                                    ("no value provided for non-default argument `%s` of snippet `%s`",
                                     ((struct namarg *)sv_at((snip)->sv_forl, _51))->name,
                                     (snip)->name);
                            }
                        }
                    }

                    /* NOTE: We do not check for snippets taking arguments even when they do not use them at all.
                       This is to allow for such snippets to be called with a variable number of arguments. */
                }

                /* perform argument substitution */
                size_t buflen = 0;
                char *buf = NULL;
                if (snippet_subst(snip, argvals, &buf, &buflen) || NULL == buf)
                    YYNOMEM;
                BUFCATN(buf, buflen - 1);       // print NOTE: excluding NUL
                free(buf);
            }
        }

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

enum loadsnip {
    LOADSNIP_OK = 0,    /* default */
    LOADSNIP_NOMEM = -1,        /* out of memory */
    LOADSNIP_FILEOPEN = -2,     /* failed opening file for read */
    LOADSNIP_FILESEEK = -3,     /* failed seeking file */
    LOADSNIP_FILEREAD = -4,     /* failed reading file */
    LOADSNIP_INIT = -5, /* failed initializing scanner */
    LOADSNIP_PARSE = -6,        /* failed parse */
};
#define enum_loadsnip_len 6
static int load_snippet_fromfile(const char *file, srt_string *snippet_body[static 1],
                                 vec_namarg *sv_argl[static 1])
{
    /* open file */
    errno = 0;
    FILE *fp = fopen(file, "r");
    if (NULL == fp)
        return LOADSNIP_FILEOPEN;

    size_t filesize = 0;
    {   // measure file size
        int ret;
        errno = 0;
        ret = fseeko(fp, 0, SEEK_END);
        if (ret == -1)
            return LOADSNIP_FILESEEK;
        filesize = ftello(fp);  // offset at end = size in bytes
        errno = 0;
        ret = fseeko(fp, 0, SEEK_SET);
        if (ret == -1)
            return LOADSNIP_FILESEEK;
    }

    srt_string *buf = NULL;
    {   // load entire file into buffer
        srt_string *(filebuf) = ss_alloc(32);
        if (ss_void == (filebuf)) {
            return LOADSNIP_NOMEM;
        }

        errno = 0;
        ssize_t nread = ss_read(&filebuf, fp, filesize);
        if (nread < 0 || (size_t)nread != filesize)
            return LOADSNIP_FILEREAD;

        buf = ss_dup_c("%" "snippet internal = " "%" "{ ");
        if (ss_void == buf)
            return LOADSNIP_NOMEM;
        ss_cat(&buf, filebuf);
        ss_cat_c(&buf, " %" "}");

        ss_free(&filebuf);
    }
    fclose(fp);

    /* initialize sub-parser */
    struct param pp = {
        .namein = "<sub-parse>",
        .pretty = glob_pp->pretty,
        .silent = glob_pp->silent,
        .subparse = true
    };
    /* NOTE: we only allocate what we need */
    (pp.sv_snip) = sv_alloc(sizeof(struct snippet), 1, NULL);
    if (sv_void == (pp.sv_snip)) {
        return LOADSNIP_NOMEM;
    }
    (pp.shm_snip) = shm_alloc(SHM_SU, 1);
    if (NULL == (pp.shm_snip)) {
        return LOADSNIP_NOMEM;
    }

    yyscan_t yyscanner;
    errno = 0;
    if (yylex_init_extra(&pp, &yyscanner))
        return LOADSNIP_INIT;

    /* parse snippet */
    YY_BUFFER_STATE _buf = yy_scan_bytes(ss_get_buffer_r(buf), ss_len(buf), yyscanner);
    pp.in_parse = true;
    int ret = yyparse(yyscanner, NULL);
    pp.in_parse = false;
    if (ret == 2)
        return LOADSNIP_NOMEM;
    else if (ret || pp.nerr > 0)
        return LOADSNIP_PARSE;

    /* retrieve result */
    struct snippet *snip = (struct snippet *)sv_at(pp.sv_snip, 0);
    *snippet_body = snip->body;
    snip->body = NULL;  // hand-over
    *sv_argl = snip->sv_argl;
    snip->sv_argl = NULL;       // hand-over

    /* cleanup */
    free(snip->name);
    ss_free(&pp.line);  // NOTE:
    ss_free(&pp.hstr_buf);      // we only
    sv_free(&pp.sv_snip);       // free
    shm_free(&pp.shm_snip);     // what we need
    ss_free(&buf);
    yy_delete_buffer(_buf, yyscanner);
    yylex_destroy(yyscanner);

    return 0;
}

enum parsesnip {
    PARSESNIP_OK = 0,   /* default */
    PARSESNIP_NOMEM = -1,       /* out of memory */
    PARSESNIP_STREAM = -2,      /* failed setting up string stream */
    PARSESNIP_INIT = -3,        /* failed initializing scanner */
    PARSESNIP_PARSE = -4,       /* failed parse */
};
#define enum_parsesnip_len 4
static int parse_snippet_body(const struct snippet *snip, srt_string *output[static 1])
{
    /* substitute snippet arguments */
    size_t buflen = 0;
    char *buf = NULL;
    if (snippet_subst(snip, NULL, &buf, &buflen) || NULL == buf)
        return PARSESNIP_NOMEM;

    /* initialize sub parser */
    struct bufstack *bs = calloc(1, sizeof(*bs));
    if (NULL == bs)
        return PARSESNIP_NOMEM;
    struct param pp = {
        .namein = "<sub-parse>",
        .pretty = glob_pp->pretty,
        .silent = glob_pp->silent,
        .subparse = true,
        .curbs = bs,
        /* const borrow resources */
        .const_rsrc = true,
        .sv_snip = glob_pp->sv_snip,    // const borrow
        .shm_snip = glob_pp->shm_snip,  // const borrow
        .sv_tab = glob_pp->sv_tab,      // const borrow
        .shm_tab = glob_pp->shm_tab,    // const borrow
        .sv_dtype = glob_pp->sv_dtype,  // const borrow
        .shm_dtype = glob_pp->shm_dtype,        // const borrow
        .sv_dproto = glob_pp->sv_dproto,        // const borrow
        .shm_dproto = glob_pp->shm_dproto,      // const borrow
        .sv_dsl = glob_pp->sv_dsl,      // const borrow
        .shm_dsl = glob_pp->shm_dsl,    // const borrow
        .sv_fdef = glob_pp->sv_fdef,    // const borrow
        .shm_fdef = glob_pp->shm_fdef,  // const borrow
        .sms_once = glob_pp->sms_once,  // const borrow
        .sms_enum = glob_pp->sms_enum,  // const borrow
    };

    yyscan_t yyscanner;
    errno = 0;
    if (yylex_init_extra(&pp, &yyscanner))
        return PARSESNIP_INIT;

    /* setup output stream */
    char *streambuf;
    size_t streamsiz;
    errno = 0;
    FILE *stream = open_memstream(&streambuf, &streamsiz);
    if (NULL == stream)
        return PARSESNIP_STREAM;

    // parse snippet body
    YY_BUFFER_STATE _buf = yy_scan_string(buf, yyscanner);
    yyset_out(stream, yyscanner);
    int ret = yyparse(yyscanner, NULL);
    if (ret == 2)
        return PARSESNIP_NOMEM;
    else if (ret || pp.nerr > 0)
        return PARSESNIP_PARSE;

    /* retrieve result */
    fclose(stream);
    if (ss_void == (*output = ss_dup_c(streambuf)))     // no embedded NUL bytes
        return PARSESNIP_NOMEM;
    ss_cat_cn1(output, '\0');   // NUL-terminate

    /* cleanup */
    free(streambuf);
    free(buf);
    pp.namein = NULL;   // unborrow
    pp.sv_snip = NULL;  // unborrow
    pp.shm_snip = NULL; // unborrow
    pp.sv_tab = NULL;   // unborrow
    pp.shm_tab = NULL;  // unborrow
    pp.sv_dtype = NULL; // unborrow
    pp.shm_dtype = NULL;        // unborrow
    pp.sv_dproto = NULL;        // unborrow
    pp.shm_dproto = NULL;       // unborrow
    pp.sv_dsl = NULL;   // unborrow
    pp.shm_dsl = NULL;  // unborrow
    pp.sv_fdef = NULL;  // unborrow
    pp.shm_fdef = NULL; // unborrow
    pp.sms_once = NULL; // unborrow
    pp.sms_enum = NULL; // unborrow
    pp_free(&pp, false);
    yy_delete_buffer(_buf, yyscanner);
    yylex_destroy(yyscanner);

    return PARSESNIP_OK;
}

__attribute__((unused))
static int snippet_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct snippet_cmod_opts *const opts,
                             char **x3, vec_namarg **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    snippet_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    if (pp->nout > 0) {
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < pp->nout; ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(*x4, i), *x4, s);
        }
        ss_cat_cn1(s, ')');
        ss_cat_c(s, " <- ");
    }
    ss_cat_cn1(s, '(');
    for (size_t i = pp->nout; i < sv_len(*x4); ++i) {
        if (i > pp->nout)
            ss_cat_cn1(s, ',');
        namarg_str_repr(sv_at(*x4, i), *x4, s);
    }
    ss_cat_c(s, ")");

    return 0;
}

int cmod_snippet_action_0(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3,
                          vec_namarg **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct snippet_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(snippet_opt_names) / sizeof(*(snippet_opt_names)));
        const char **opt_names = snippet_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_bool(&opts.append_bool, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ere", 4) == 0) {
            ret = option_list_action_bool(&opts.here_bool, opt, &opts.here_isset);

        }
        else if ('n' == *((opt->name) + 0)) {
            if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "2sp", 4) == 0) {
                ret = option_list_action_bool(&opts.nl2sp_bool, opt, &opts.nl2sp_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'w' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_14;
            }
        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else {
            goto cmod_strin_else_14;
 cmod_strin_else_14:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.append_isset) {
        opts.append_bool = false;
        opts.append_isset = false;
        WARN0("ignoring option [append]");
    }
    if (opts.here_isset) {
        opts.here_bool = false;
        opts.here_isset = false;
        WARN0("ignoring option [here]");
    }
    if (opts.now_isset) {
        opts.now_bool = false;
        opts.now_isset = false;
        WARN0("ignoring option [now]");
    }
    if (opts.nl2sp_isset) {
        opts.nl2sp_bool = false;
        opts.nl2sp_isset = false;
        WARN0("ignoring option [nl2sp]");
    }
    opts.redef_bool = true;
    opts.redef_isset = true;

    int icuropt = -1, ibadopt = -1;
    if (snippet_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      snippet_opt_names[icuropt], snippet_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        snippet_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct snippet *oldsnip = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_snip) {
                }
                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
            }

            if (_found.set)
                oldsnip = (void *)sv_at(pp->sv_snip, _found.ix);
        }
        if (NULL != (oldsnip)) {
            if (!(opts.append_bool) && opts.opt_bool) {
                VERBOSE("ignoring already defined snippet `%s`", (*x3));
                goto epilogue;
            }
            else if (!(oldsnip)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("snippet `%s` is not redefinable", (*x3));
            }
        }

        srt_string *(snippet_body) = ss_alloc(1);
        if (ss_void == (snippet_body)) {
            YYNOMEM;
        }

        ss_cat_cn1(&snippet_body, '\0');        // NUL-terminate

        struct snippet snip = { 0 };
        snip = (struct snippet) {
            .name = (*x3),.body = snippet_body,
            .sv_forl = (*x4),.sv_argl = NULL,
            .redef = opts.redef_bool,.nout = pp->nout
        };
        {       // check snippet
            size_t ierr = 0;
            const char *err = NULL;
            int ret = snip_def_check(&snip, false, &ierr, &err);        // is_lambda = false
            if (ret >= 2)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            switch (ret) {
            case 0:
                if (ierr == 0)
                    break;
                VERBOSE("unused arguments (%zu) in snippet `%s`", ierr, snip.name);
                break;
            case 1:
                YYNOMEM;;
                break;
            case 2:
                KEYWORD_ERROR("formal argument at position %zu has no name in snippet `%s`", ierr,
                              (*x3));
                break;
            case 3:
                KEYWORD_ERROR("invalid special argument %s in snippet `%s`", err, (*x3));
                break;
            case 4:
                KEYWORD_ERROR("reference to undefined formal argument `%s` in snippet `%s`", err,
                              (*x3));
                break;
            case 5:
                KEYWORD_ERROR("positional reference %zu out of bounds in snippet `%s`", ierr,
                              (*x3));
                break;
            case 6:
                KEYWORD_ERROR
                    ("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",
                     err, (*x3));
                break;
            case 7:
                KEYWORD_ERROR("invalid arglink to self in formal argument `%s` of snippet `%s`",
                              err, (*x3));
                break;
            case 8:
                KEYWORD_ERROR("arglink is too deep in formal argument `%s` of snippet `%s`", err,
                              (*x3));
                break;
            }
        }

        (*x3) = NULL;   // hand-over
        (*x4) = NULL;   // hand-over

        if (NULL == oldsnip) {
            {
                const srt_string *_ss = ss_crefs((snip).name);
                if (!sv_push(&(pp->sv_snip), &(snip))
                    || !shm_insert_su(&(pp->shm_snip), _ss, sv_len(pp->sv_snip) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining snippet `%s`", (snip).name);
            snippet_clear(oldsnip);
            *(oldsnip) = snip;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int snippet_rule1_str(const struct param *const pp,
                             size_t debug_level,
                             const struct snippet_cmod_opts *const opts,
                             char **x3, char *x4, struct snippet *x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    snippet_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    switch (*x4) {
    case '=':
        ss_cat_c(s, "  = ");
        break;
    case '?':
        ss_cat_c(s, " ?= ");
        break;
    case '+':
        ss_cat_c(s, " += ");
        break;
    case ':':
        ss_cat_c(s, " := ");
        break;
    case '|':
        ss_cat_c(s, " |= ");
        break;
    }

    (void)x5;
    ss_cat_cn1(s, ' ');
    if (SNIPPET_NARGOUT(x5) > 0) {
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < SNIPPET_NARGOUT(x5); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(*&x5->sv_forl, i), *&x5->sv_forl, s);
        }
        ss_cat_cn1(s, ')');
        ss_cat_c(s, " <- ");
    }
    ss_cat_cn1(s, '(');
    for (size_t i = SNIPPET_NARGOUT(x5); i < sv_len(*&x5->sv_forl); ++i) {
        if (i > SNIPPET_NARGOUT(x5))
            ss_cat_cn1(s, ',');
        namarg_str_repr(sv_at(*&x5->sv_forl, i), *&x5->sv_forl, s);
    }
    ss_cat_c(s, ")");
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{\n");
    if (debug_level >= 2) {
        const struct snippet tmp = {
            .body = *&x5->body, // const borrow
            .sv_argl = x5->sv_argl      // const borrow
        };
        snippet_body_str_repr(&tmp, tmp.sv_argl, s);
    }
    else
        ss_cat_c(s, "<snippet body>");
    ss_cat_c(s, "%" "}");

    return 0;
}

int cmod_snippet_action_1(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3,
                          char *x4, const YYLTYPE *loc4,
                          struct snippet *x5, const YYLTYPE *loc5,
                          bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct snippet_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(snippet_opt_names) / sizeof(*(snippet_opt_names)));
        const char **opt_names = snippet_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_bool(&opts.append_bool, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ere", 4) == 0) {
            ret = option_list_action_bool(&opts.here_bool, opt, &opts.here_isset);

        }
        else if ('n' == *((opt->name) + 0)) {
            if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "2sp", 4) == 0) {
                ret = option_list_action_bool(&opts.nl2sp_bool, opt, &opts.nl2sp_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'w' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_15;
            }
        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else {
            goto cmod_strin_else_15;
 cmod_strin_else_15:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    switch ((*x4)) {
    case '=':  /* normal */
        break;
    case '?':
        opts.opt_bool = true;
        opts.opt_isset = true;
        break;
    case ':':
        opts.redef_bool = true;
        opts.redef_isset = true;
        break;
    case '+':
        opts.append_bool = true;
        opts.append_isset = true;

        opts.redef_bool = true;
        opts.redef_isset = true;

        break;
    default:
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
        KEYWORD_ERROR0("unsupported assignment operator");
        break;
    }

    int icuropt = -1, ibadopt = -1;
    if (snippet_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      snippet_opt_names[icuropt], snippet_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        snippet_rule1_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct snippet *oldsnip = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_snip) {
                }
                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
            }

            if (_found.set)
                oldsnip = (void *)sv_at(pp->sv_snip, _found.ix);
        }
        if (NULL != (oldsnip)) {
            if (!(opts.append_bool) && opts.opt_bool) {
                VERBOSE("ignoring already defined snippet `%s`", (*x3));
                goto epilogue;
            }
            else if (!(oldsnip)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("snippet `%s` is not redefinable", (*x3));
            }
        }
        struct snippet snip = (*x5);
        (*x5) = (struct snippet) { 0 }; // hand-over
        snip.name = (*x3);
        (*x3) = NULL;   // hand-over
        snip.redef = opts.redef_bool;

        if (opts.now_bool) {
            if (SNIPPET_HAS_FORMAL_ARGS(&snip) && SNIPPET_NARG_NODEF(&snip) > 0) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR
                    ("immediate evaluation of snippet `%s` with non-default arguments is not possible",
                     snip.name);
            }
            srt_string *parsed_body = NULL;
            enum parsesnip ret = parse_snippet_body(&snip, &parsed_body);
            if (ret != PARSESNIP_OK)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            switch (ret) {
#define ERRORFMT ", while parsing body of snippet `%s`"
            case PARSESNIP_OK:
                /**/ break;
            case PARSESNIP_NOMEM:
                YYNOMEM;
                break;
            case PARSESNIP_STREAM:
                KEYWORD_ERROR("failed setting up string stream" ERRORFMT ": %s (%d)",
                              snip.name, strerror(errno), errno);
                break;
            case PARSESNIP_INIT:
                KEYWORD_ERROR("failed initializing scanner" ERRORFMT ": %s (%d)",
                              snip.name, strerror(errno), errno);
                break;
            case PARSESNIP_PARSE:
                KEYWORD_ERROR("failed parse" ERRORFMT, snip.name);
                break;
#undef ERRORFMT
            }
            {   // clear while keeping name
                char *name_save = snip.name;
                snip.name = NULL;       // hand-over
                snippet_clear(&snip);
                snip.name = name_save;  // reset
            }
            snip.body = parsed_body;
            snip.redef = opts.redef_bool;
        }

        if (opts.append_bool && NULL != oldsnip) {
            //checks
            if (SNIPPET_HAS_FORMAL_ARGS(&snip)) {
                if ((sv_len(snip.sv_forl) != sv_len(oldsnip->sv_forl))
                    || snip.nout != oldsnip->nout) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR("[append] mismatched formal argument lists in snippet `%s`",
                                  snip.name);
                }
                for (size_t i = 0; i < sv_len(snip.sv_forl); ++i) {
                    const struct namarg *oldfarg = sv_at(oldsnip->sv_forl, i);
                    const struct namarg *newfarg = sv_at(snip.sv_forl, i);
                    if (strcmp(oldfarg->name, newfarg->name) != 0) {    // formal argument names differ
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                        KEYWORD_ERROR
                            ("[append] mismatched formal argument `%s` at position %zu in snippet `%s`",
                             newfarg->name, i, snip.name);
                    }
                }
            }

            vec_dllarg *new_argl = snip.sv_argl;
            srt_string *new_body = snip.body;
            snip.sv_argl = NULL;        // hand-over
            snip.body = NULL;   // hand-over
            snippet_clear(&snip);

            size_t oldlen = ss_len(oldsnip->body) - 1;  // exclude NUL byte
            {   // fix new dllarg positions
                for (size_t i = 0; i < sv_len(new_argl); ++i) {
                    struct dllarg *arg = (struct dllarg *)sv_at(new_argl, i);
                    arg->pos += oldlen;
                }
            }
            {   // concatenate dllarg lists
                vec_dllarg *old_argl = oldsnip->sv_argl;
                oldsnip->sv_argl = NULL;        // hand-over
                sv_cat(&old_argl, new_argl);
                sv_free(&new_argl);
                new_argl = old_argl;
            }
            {   // concatenate bodies (includes last NUL byte)
                srt_string *old_body = oldsnip->body;
                assert(NULL != old_body);
                oldsnip->body = NULL;   // hand-over
                ss_erase(&old_body, oldlen, 1); // remove old NUL byte
                ss_cat(&old_body, new_body);
                ss_free(&new_body);
                new_body = old_body;
            }
            {   // define new snippet
                snip = (struct snippet) {
                    .name = oldsnip->name,.body = new_body,
                    .sv_forl = oldsnip->sv_forl,.sv_argl = new_argl,
                    .redef = oldsnip->redef,.nout = oldsnip->nout
                };
                {       // check snippet
                    size_t ierr = 0;
                    const char *err = NULL;
                    int ret = snip_def_check(&snip, false, &ierr, &err);        // is_lambda = false
                    if (ret >= 2)
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    switch (ret) {
                    case 0:
                        if (ierr == 0)
                            break;
                        VERBOSE("unused arguments (%zu) in snippet `%s`", ierr, snip.name);
                        break;
                    case 1:
                        YYNOMEM;;
                        break;
                    case 2:
                        KEYWORD_ERROR("formal argument at position %zu has no name in snippet `%s`",
                                      ierr, oldsnip->name);
                        break;
                    case 3:
                        KEYWORD_ERROR("invalid special argument %s in snippet `%s`", err,
                                      oldsnip->name);
                        break;
                    case 4:
                        KEYWORD_ERROR("reference to undefined formal argument `%s` in snippet `%s`",
                                      err, oldsnip->name);
                        break;
                    case 5:
                        KEYWORD_ERROR("positional reference %zu out of bounds in snippet `%s`",
                                      ierr, oldsnip->name);
                        break;
                    case 6:
                        KEYWORD_ERROR
                            ("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",
                             err, oldsnip->name);
                        break;
                    case 7:
                        KEYWORD_ERROR
                            ("invalid arglink to self in formal argument `%s` of snippet `%s`", err,
                             oldsnip->name);
                        break;
                    case 8:
                        KEYWORD_ERROR("arglink is too deep in formal argument `%s` of snippet `%s`",
                                      err, oldsnip->name);
                        break;
                    }
                }

                oldsnip->name = NULL;   // hand-over
                oldsnip->sv_forl = NULL;        // hand-over
            }
            pp->redef_nowarn = true;
        }

        if (opts.nl2sp_bool) {
            ss_replace(&snip.body, 0, ss_crefs("\n"), ss_crefs(" "));
        }

        if (opts.here_bool) {   // recall immediately
            if (SNIPPET_HAS_FORMAL_ARGS(&snip) && SNIPPET_NARG_NODEF(&snip) > 0) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR
                    ("immediate recall of snippet `%s` with non-default arguments is not possible",
                     snip.name);
            }
            size_t buflen = 0;
            char *buf = NULL;
            if (snippet_subst(&snip, NULL, &buf, &buflen) || NULL == buf)
                YYNOMEM;
            BUFCATN(buf, buflen - 1);   // NOTE: discounting NUL
            free(buf);
        }

        if (NULL == oldsnip) {
            {
                const srt_string *_ss = ss_crefs((snip).name);
                if (!sv_push(&(pp->sv_snip), &(snip))
                    || !shm_insert_su(&(pp->shm_snip), _ss, sv_len(pp->sv_snip) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining snippet `%s`", (snip).name);
            snippet_clear(oldsnip);
            *(oldsnip) = snip;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int snippet_rule2_str(const struct param *const pp,
                             size_t debug_level,
                             const struct snippet_cmod_opts *const opts,
                             char **x3, char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    snippet_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "\"", *x4, "\"");

    return 0;
}

int cmod_snippet_action_2(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3,
                          char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct snippet_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(snippet_opt_names) / sizeof(*(snippet_opt_names)));
        const char **opt_names = snippet_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_bool(&opts.append_bool, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ere", 4) == 0) {
            ret = option_list_action_bool(&opts.here_bool, opt, &opts.here_isset);

        }
        else if ('n' == *((opt->name) + 0)) {
            if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "2sp", 4) == 0) {
                ret = option_list_action_bool(&opts.nl2sp_bool, opt, &opts.nl2sp_isset);

            }
            else if ('o' == *((opt->name) + 1) && 'w' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_16;
            }
        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else {
            goto cmod_strin_else_16;
 cmod_strin_else_16:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.append_isset) {
        opts.append_bool = false;
        opts.append_isset = false;
        WARN0("ignoring option [append]");
    }
    if (opts.here_isset) {
        opts.here_bool = false;
        opts.here_isset = false;
        WARN0("ignoring option [here]");
    }
    if (opts.now_isset) {
        opts.now_bool = false;
        opts.now_isset = false;
        WARN0("ignoring option [now]");
    }
    if (opts.nl2sp_isset) {
        opts.nl2sp_bool = false;
        opts.nl2sp_isset = false;
        WARN0("ignoring option [nl2sp]");
    }

    int icuropt = -1, ibadopt = -1;
    if (snippet_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      snippet_opt_names[icuropt], snippet_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        snippet_rule2_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct snippet *oldsnip = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_snip) {
                }
                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
            }

            if (_found.set)
                oldsnip = (void *)sv_at(pp->sv_snip, _found.ix);
        }
        if (NULL != (oldsnip)) {
            if (!(opts.append_bool) && opts.opt_bool) {
                VERBOSE("ignoring already defined snippet `%s`", (*x3));
                goto epilogue;
            }
            else if (!(oldsnip)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("snippet `%s` is not redefinable", (*x3));
            }
        }

        srt_string *snippet_body = NULL;
        vec_namarg *sv_argl = NULL;
        {       // parse snippet from file
            enum loadsnip ret = load_snippet_fromfile((*x4), &snippet_body, &sv_argl);
            if (ret != LOADSNIP_OK)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            switch (ret) {
#define ERRORFMT ", while loading snippet from file \"%s\""
            case LOADSNIP_OK:
                /**/ break;
            case LOADSNIP_NOMEM:
                YYNOMEM;
                break;
            case LOADSNIP_FILEOPEN:
                KEYWORD_ERROR("failed opening file for read" ERRORFMT ": %s (%d)",
                              (*x4), strerror(errno), errno);
                break;
            case LOADSNIP_FILESEEK:
                KEYWORD_ERROR("failed seeking file" ERRORFMT ": %s (%d)",
                              (*x4), strerror(errno), errno);
                break;
            case LOADSNIP_FILEREAD:
                KEYWORD_ERROR("failed reading file" ERRORFMT ": %s (%d)",
                              (*x4), strerror(errno), errno);
                break;
            case LOADSNIP_INIT:
                KEYWORD_ERROR("failed initializing scanner" ERRORFMT ": %s (%d)",
                              (*x4), strerror(errno), errno);
                break;
            case LOADSNIP_PARSE:
                KEYWORD_ERROR("failed parse" ERRORFMT, (*x4));
                break;
#undef ERRORFMT
            }
        }

        vec_namarg *sv_forl = NULL;
        if (sv_len(sv_argl) > 0) {      // get argument references from snippet body
            srt_set *(set) = sms_alloc(SMS_S, sv_len(sv_argl));
            if (NULL == (set)) {
                YYNOMEM;
            }

            {
                const size_t _len = sv_len(sv_argl);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _23 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    if (!sms_insert_s
                        (&(set), ss_crefs(((const struct dllarg *)sv_at(sv_argl, _23))->name))) {
                        YYNOMEM;
                    };

                }
            }
            size_t setlen = sms_len(set);
            (sv_forl) = sv_alloc(sizeof(struct namarg), setlen, NULL);
            if (sv_void == (sv_forl)) {
                YYNOMEM;
            }

            for (size_t i = 0; i < setlen; ++i) {
                const srt_string *name = sms_it_s(set, i);
                struct namarg farg = { 0 };
                {
                    const char *ctmp = ss_to_c(name);
                    farg.name = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                    if (NULL != (ctmp) && NULL == (farg.name)) {
                        YYNOMEM;
                    }

                }
                if (!sv_push(&(sv_forl), &(farg))) {
                    YYNOMEM;
                }

            }
            sms_free(&set);
        }

        struct snippet snip = { 0 };
        snip = (struct snippet) {
            .name = (*x3),.body = snippet_body,
            .sv_forl = sv_forl,.sv_argl = sv_argl,
            .redef = opts.redef_bool,.nout = 0
        };
        {       // check snippet
            size_t ierr = 0;
            const char *err = NULL;
            int ret = snip_def_check(&snip, false, &ierr, &err);        // is_lambda = false
            if (ret >= 2)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            switch (ret) {
            case 0:
                if (ierr == 0)
                    break;
                VERBOSE("unused arguments (%zu) in snippet `%s`", ierr, snip.name);
                break;
            case 1:
                YYNOMEM;;
                break;
            case 2:
                KEYWORD_ERROR("formal argument at position %zu has no name in snippet `%s`", ierr,
                              (*x3));
                break;
            case 3:
                KEYWORD_ERROR("invalid special argument %s in snippet `%s`", err, (*x3));
                break;
            case 4:
                KEYWORD_ERROR("reference to undefined formal argument `%s` in snippet `%s`", err,
                              (*x3));
                break;
            case 5:
                KEYWORD_ERROR("positional reference %zu out of bounds in snippet `%s`", ierr,
                              (*x3));
                break;
            case 6:
                KEYWORD_ERROR
                    ("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",
                     err, (*x3));
                break;
            case 7:
                KEYWORD_ERROR("invalid arglink to self in formal argument `%s` of snippet `%s`",
                              err, (*x3));
                break;
            case 8:
                KEYWORD_ERROR("arglink is too deep in formal argument `%s` of snippet `%s`", err,
                              (*x3));
                break;
            }
        }

        (*x3) = NULL;   // hand-over

        if (NULL == oldsnip) {
            {
                const srt_string *_ss = ss_crefs((snip).name);
                if (!sv_push(&(pp->sv_snip), &(snip))
                    || !shm_insert_su(&(pp->shm_snip), _ss, sv_len(pp->sv_snip) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining snippet `%s`", (snip).name);
            snippet_clear(oldsnip);
            *(oldsnip) = snip;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int strcmp_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct strcmp_cmod_opts *const opts,
                            vec_namarg **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    strcmp_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x3, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_strcmp_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_namarg **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct strcmp_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(strcmp_opt_names) / sizeof(*(strcmp_opt_names)));
        const char **opt_names = strcmp_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0)) {
            if ('q' == *((opt->name) + 1) && '\0' == *((opt->name) + 2)) {
                ret = option_list_action_bool(&opts.eq_bool, opt, &opts.eq_isset);

            }
            else if ('r' == *((opt->name) + 1) && 'e' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.ere_bool, opt, &opts.ere_isset);

            }
            else {
                goto cmod_strin_else_17;
            }
        }
        else if ('g' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && '\0' == *((opt->name) + 2)) {
                ret = option_list_action_bool(&opts.ge_bool, opt, &opts.ge_isset);

            }
            else if ('t' == *((opt->name) + 1) && '\0' == *((opt->name) + 2)) {
                ret = option_list_action_bool(&opts.gt_bool, opt, &opts.gt_isset);

            }
            else {
                goto cmod_strin_else_17;
            }
        }
        else if ('i' == *((opt->name) + 0) && strncmp((opt->name) + 1, "case", 5) == 0) {
            ret = option_list_action_bool(&opts.icase_bool, opt, &opts.icase_isset);

        }
        else if ('l' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && '\0' == *((opt->name) + 2)) {
                ret = option_list_action_bool(&opts.le_bool, opt, &opts.le_isset);

            }
            else if ('t' == *((opt->name) + 1) && '\0' == *((opt->name) + 2)) {
                ret = option_list_action_bool(&opts.lt_bool, opt, &opts.lt_isset);

            }
            else {
                goto cmod_strin_else_17;
            }
        }
        else if ('n' == *((opt->name) + 0)) {
            if ('c' == *((opt->name) + 1) && strncmp((opt->name) + 2, "har", 4) == 0) {
                ret = option_list_action_count(&opts.nchar_count, opt, &opts.nchar_isset);

            }
            else if ('e' == *((opt->name) + 1)) {
                if ('\0' == *((opt->name) + 2)) {
                    ret = option_list_action_bool(&opts.ne_bool, opt, &opts.ne_isset);

                }
                else if ('w' == *((opt->name) + 2) && strncmp((opt->name) + 3, "line", 5) == 0) {
                    ret = option_list_action_bool(&opts.newline_bool, opt, &opts.newline_isset);

                }
                else {
                    goto cmod_strin_else_17;
                }
            }
            else if ('o' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.not_bool, opt, &opts.not_isset);

            }
            else {
                goto cmod_strin_else_17;
            }
        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egex", 5) == 0) {
            ret = option_list_action_bool(&opts.regex_bool, opt, &opts.regex_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nsigned", 8) == 0) {
            ret = option_list_action_bool(&opts.unsigned_bool, opt, &opts.unsigned_isset);

        }
        else if ('w' == *((opt->name) + 0) && strncmp((opt->name) + 1, "hole", 5) == 0) {
            ret = option_list_action_bool(&opts.whole_bool, opt, &opts.whole_isset);

        }
        else {
            goto cmod_strin_else_17;
 cmod_strin_else_17:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (strcmp_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      strcmp_opt_names[icuropt], strcmp_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        strcmp_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        /* check and parse cmod_function_arguments */
        {
#define INPUT_NAME "input"
#define MATCH_NAME "match"
#define THEN_NAME "then"
#define ELSE_NAME "else"
            size_t nargs = sv_len((*x3));
            if (nargs < 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected at least two arguments: <" INPUT_NAME "> <" MATCH_NAME
                               ">");
            }
            else if (nargs == 2) {      // insert defaults
                char *val_then = rstrdup("1");
                if (NULL == (val_then)) {
                    YYNOMEM;
                }
                ;
                struct namarg arg_then = CMOD_ARG_VERBATIM_set(val_then);
                if (!sv_push(&((*x3)), &(arg_then))) {
                    YYNOMEM;
                }

                char *val_else = rstrdup("0");
                if (NULL == (val_else)) {
                    YYNOMEM;
                }
                ;
                struct namarg arg_else = CMOD_ARG_VERBATIM_set(val_else);
                if (!sv_push(&((*x3)), &(arg_else))) {
                    YYNOMEM;
                }

                nargs = sv_len((*x3));
                assert(nargs == 4);
            }

            const bool evenargs = (nargs % 2 == 0);
            const size_t npaired = nargs - evenargs;    // odd number

            const struct namarg *first = sv_at((*x3), 0);
            if (first->type != CMOD_ARG_VERBATIM) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected herestring input argument");
            }
            if (NULL != first->name && strcmp(first->name, INPUT_NAME) != 0) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("input argument name must be `" INPUT_NAME "`");
            }

            srt_string *buf = NULL;
            (buf) = ss_alloc(8);
            if (ss_void == (buf)) {
                YYNOMEM;
            }

            for (size_t i = 1; i < npaired; ++i) {      // check match-then args
                const struct namarg *narg = sv_at((*x3), i);
                const bool is_match = (i % 2 != 0);
                if (!is_match && narg->type != CMOD_ARG_VERBATIM) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("expected herestring argument at position %zu", i);
                }
                else if (is_match && narg->type != CMOD_ARG_VERBATIM
                         && narg->type != CMOD_ARG_INTSTR) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("expected herestring or integer argument at position %zu", i);
                }
                if (NULL != narg->name) {
                    ss_clear(buf);
                    ss_cat_c(&buf, is_match ? MATCH_NAME : THEN_NAME);
                    if (i > 2) {
                        ss_cat_cn1(&buf, '/');
                        ss_cat_int(&buf, (i - 1) / 2);
                    }
                    if (strcmp(narg->name, ss_to_c(buf)) != 0) {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc3);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected named argument `%s` at position %zu",
                                      narg->name, i);
                    }
                }
            }
            ss_free(&buf);

            if (evenargs) {     // check else
                const struct namarg *last = sv_at((*x3), nargs - 1);
                if (last->type != CMOD_ARG_VERBATIM) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR0("expected herestring argument");
                }
                if (NULL != last->name && strcmp(last->name, ELSE_NAME) != 0) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR0("else argument name must be `" ELSE_NAME "`");
                }
            }
#undef INPUT_NAME
#undef MATCH_NAME
#undef THEN_NAME
#undef ELSE_NAME
        }

        const size_t nargs = sv_len((*x3));
        const bool evenargs = (nargs % 2 == 0);
        const size_t npaired = nargs - evenargs;        // odd number

        const struct namarg *arg_inp = sv_at((*x3), 0);
        const char *const inp = CMOD_ARG_VERBATIM_get(*arg_inp);
        bool cmp = false;
        const char *if_match = NULL;
        regmatch_t pmatch[10] = { {0} };        // up to 9 sub-matches

        for (size_t i = 1; i < npaired; i += 2) {
            const struct namarg *arg_match = sv_at((*x3), i);
            const char *pat = (arg_match->type == CMOD_ARG_INTSTR)
                ? CMOD_ARG_INTSTR_get(*arg_match)
                : CMOD_ARG_VERBATIM_get(*arg_match);

            /* if match is integer, perform numeric equals comparison, if not enabled */
            bool cmp_numbers = opts.eq_bool || opts.ne_bool || opts.le_bool || opts.lt_bool
                || opts.gt_bool || opts.ge_bool;
            bool force_numeric = (arg_match->type == CMOD_ARG_INTSTR && !cmp_numbers);
            if (force_numeric)
                opts.eq_bool = cmp_numbers = true;
            if (opts.regex_bool) {      // match regex
                int ret;
                regex_t preg;
                /* set flags */
                long cflags = 0L | (opts.icase_bool ? REG_ICASE : 0L)
                    | (opts.ere_bool ? REG_EXTENDED : 0L)
                    | (opts.newline_bool ? REG_NEWLINE : 0L);
                /* build pattern */
                srt_string *(spatt) = ss_alloc(0);
                if (ss_void == (spatt)) {
                    YYNOMEM;
                }
                ;
                if (opts.whole_bool)
                    ss_cat_cn1(&spatt, '^');
                ss_cat_c(&spatt, pat);
                if (opts.whole_bool)
                    ss_cat_cn1(&spatt, '$');
                /* compile regex */
                if ('\0' == *ss_to_c(spatt))
                    KEYWORD_ERROR0("empty regex not allowed for portability");
                if ((ret = regcomp(&preg, ss_to_c(spatt), cflags))) {
                    size_t errmsg_len = regerror(ret, &preg, NULL, 0);
                    char errmsg[errmsg_len];    // VLA
                    regerror(ret, &preg, errmsg, errmsg_len);
                    KEYWORD_ERROR("failed compiling regex `%s`: %s (%d)",
                                  ss_to_c(spatt), errmsg, ret);
                }
                ss_free(&spatt);

                /* match regex */
                cmp = (regexec(&preg, inp, (sizeof(pmatch) / sizeof(*(pmatch))), pmatch, 0) == 0);      // match or not

                regfree(&preg);
            }
            else if (!cmp_numbers) {    // compare as strings
                cmp = (opts.nchar_count > 0)
                    ? (opts.icase_bool ? (strncasecmp(inp, pat, opts.nchar_count) == 0)
                       : (strncmp(inp, pat, opts.nchar_count) == 0))
                    : (opts.icase_bool ? (strcasecmp(inp, pat) == 0)
                       : (strcmp(inp, pat) == 0));
            }
            else if (opts.unsigned_bool) {      // compare as unsigned integers
                uintmax_t a, b;
                if (strtoumax_check(inp, &a))
                    KEYWORD_ERROR("unsigned integer conversion failed for `%s`", inp);
                if (strtoumax_check(pat, &b))
                    KEYWORD_ERROR("unsigned integer conversion failed for `%s`", pat);
                if (opts.eq_bool)
                    cmp = (a == b);
                else if (opts.ne_bool)
                    cmp = (a != b);
                else if (opts.le_bool)
                    cmp = (a <= b);
                else if (opts.lt_bool)
                    cmp = (a < b);
                else if (opts.gt_bool)
                    cmp = (a > b);
                else if (opts.ge_bool)
                    cmp = (a >= b);
            }
            else {      // compare as signed integers
                intmax_t a, b;
                if (strtoimax_check(inp, &a))
                    KEYWORD_ERROR("integer conversion failed for `%s`", inp);
                if (strtoimax_check(pat, &b))
                    KEYWORD_ERROR("integer conversion failed for `%s`", pat);
                if (opts.eq_bool)
                    cmp = (a == b);
                else if (opts.ne_bool)
                    cmp = (a != b);
                else if (opts.le_bool)
                    cmp = (a <= b);
                else if (opts.lt_bool)
                    cmp = (a < b);
                else if (opts.gt_bool)
                    cmp = (a > b);
                else if (opts.ge_bool)
                    cmp = (a >= b);
            }
            if (opts.not_bool)
                cmp = !cmp;     // negate comparison

            if (force_numeric)
                opts.eq_bool = cmp_numbers = false;

            if (cmp) {
                const struct namarg *arg_then = sv_at((*x3), i + 1);
                if_match = CMOD_ARG_VERBATIM_get(*arg_then);
                break;
            }
        }

        if (cmp) {
            if (opts.regex_bool) {      // parse replacement string
                srt_string *ssub = strregsub_parse(inp, if_match, pmatch);
                if (ss_void == ssub)
                    YYNOMEM;
                BUFCAT(ssub);
                ss_free(&ssub);
            }
            else
                BUFPUTS(if_match);
        }
        else {
            const struct namarg *arg_else = sv_at((*x3), nargs - 1);
            const char *if_else = evenargs ? CMOD_ARG_VERBATIM_get(*arg_else) : "";
            BUFPUTS(if_else);
        }

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int strlen_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct strlen_cmod_opts *const opts,
                            vec_namarg **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    strlen_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x3, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_strlen_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_namarg **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct strlen_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(strlen_opt_names) / sizeof(*(strlen_opt_names)));
        const char **opt_names = strlen_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "dd1", 4) == 0) {
            ret = option_list_action_count(&opts.add1_count, opt, &opts.add1_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "scaped", 7) == 0) {
            ret = option_list_action_bool(&opts.escaped_bool, opt, &opts.escaped_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "tf8", 4) == 0) {
            ret = option_list_action_bool(&opts.utf8_bool, opt, &opts.utf8_isset);

        }
        else {
            goto cmod_strin_else_18;
 cmod_strin_else_18:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (strlen_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      strlen_opt_names[icuropt], strlen_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        strlen_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t len = opts.add1_count;
        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _24 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (((const struct namarg *)sv_at((*x3), _24))->type != CMOD_ARG_VERBATIM) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("expected verbatim argument at position %zu", _24);
                }
                else if (NULL != ((const struct namarg *)sv_at((*x3), _24))->name) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("unexpected named verbatim argument at position %zu", _24);
                }
                const char *str =
                    CMOD_ARG_VERBATIM_get(*((const struct namarg *)sv_at((*x3), _24)));
                if (opts.escaped_bool) {        // count escapes as length 1
                    srt_string *ss = NULL;
                    int ret = strcunesc(strlen(str), str, &(ss));
                    const char *errstr = NULL;
                    switch (ret) {
                    case 1:
                        YYNOMEM;;
                        break;
                    case 2:
                        errstr = "stray character";
                        break;
                    case 3:
                        errstr = "invalid escape sequence";
                        break;
                    case 4:
                        errstr = "incomplete escape sequence";
                        break;
                    case 5:
                        errstr = "numerical escape sequence out of range";
                        break;
                    case 6:
                        errstr = "unicode escape sequence out of range";
                        break;
                    default:
                        errstr = "unspecific error";
                        break;
                    }
                    if (ret)
                        KEYWORD_ERROR("%s", errstr);

                    len += (opts.utf8_bool ? ss_len_u(ss) : ss_len(ss));        // no final NUL
                    ss_free(&ss);
                }
                else {
                    const srt_string *ss = ss_crefs(str);
                    len += (opts.utf8_bool ? ss_len_u(ss) : ss_len(ss));        // final NUL is not counted
                }
            }
        }
        BUFPUTINT(len);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int strstr_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct strstr_cmod_opts *const opts,
                            vec_namarg **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    strstr_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x3, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_strstr_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_namarg **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct strstr_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(strstr_opt_names) / sizeof(*(strstr_opt_names)));
        const char **opt_names = strstr_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'r' == *((opt->name) + 1) && 'e' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.ere_bool, opt, &opts.ere_isset);

        }
        else if ('i' == *((opt->name) + 0) && strncmp((opt->name) + 1, "case", 5) == 0) {
            ret = option_list_action_bool(&opts.icase_bool, opt, &opts.icase_isset);

        }
        else if ('m' == *((opt->name) + 0) && strncmp((opt->name) + 1, "atch", 5) == 0) {
            ret = option_list_action_index(&opts.match_index, opt, &opts.match_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ewline", 7) == 0) {
            ret = option_list_action_bool(&opts.newline_bool, opt, &opts.newline_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egex", 5) == 0) {
            ret = option_list_action_bool(&opts.regex_bool, opt, &opts.regex_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else if ('u' == *((opt->name) + 1) && strncmp((opt->name) + 2, "bmatch", 7) == 0) {
                ret = option_list_action_index(&opts.submatch_index, opt, &opts.submatch_isset);

            }
            else {
                goto cmod_strin_else_19;
            }
        }
        else {
            goto cmod_strin_else_19;
 cmod_strin_else_19:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.icase_bool && !opts.regex_bool)
        KEYWORD_ERROR0("option [icase] requires option [regex]");
    if (opts.submatch_index.set && !opts.regex_bool)
        KEYWORD_ERROR0("option [submatch] requires option [regex]");

    int icuropt = -1, ibadopt = -1;
    if (strstr_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      strstr_opt_names[icuropt], strstr_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        strstr_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        const char *inp = NULL;
        const char *pat = NULL;
        /* check and parse cmod_function_arguments */
        {
            size_t nargs = sv_len((*x3));
            assert(nargs > 0);
            if (nargs != 2) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected exactly two arguments:" " <input>" " <match>");
            }
            {
                const char **ord[2] = { &inp, &pat };
                for (size_t i = 0; i < sv_len((*x3)); ++i) {
                    const struct namarg *narg = sv_at((*x3), i);
                    if (narg->type != CMOD_ARG_VERBATIM) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR("expected " "verbatim" " argument at position %zu", i);
                    }
                    const char *txt = CMOD_ARG_VERBATIM_get(*narg);
                    if (NULL == narg->name)
                        *ord[i] = txt;  // const borrow
                    else if (strcmp(narg->name, "input") == 0) {
                        if (*ord[0] != NULL) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                            KEYWORD_ERROR("argument `%s` already set", "input");
                        }
                        *ord[0] = txt;  // const borrow
                    }
                    else if (strcmp(narg->name, "match") == 0) {
                        if (*ord[1] != NULL) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                            KEYWORD_ERROR("argument `%s` already set", "match");
                        }
                        *ord[1] = txt;  // const borrow
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc3);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected " "verbatim" " named argument `%s`", narg->name);
                    }
                }
                if (NULL == inp) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR0("missing value for required argument `" "input" "`");
                }
                if (NULL == pat) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR0("missing value for required argument `" "match" "`");
                }
            }
        }

        size_t count = 0;
        regoff_t off = 0;
        regoff_t len = 0;
        if (opts.regex_bool) {
            int ret;
            regex_t preg;
            regmatch_t pmatch[17] = { {0} };    // up to 16 submatches
            /* set flags */
            long cflags = 0L | (opts.icase_bool ? REG_ICASE : 0L)
                | (opts.ere_bool ? REG_EXTENDED : 0L)
                | (opts.newline_bool ? REG_NEWLINE : 0L);
            /* compile regex */
            if ('\0' == *(pat))
                KEYWORD_ERROR0("empty regex not allowed for portability");
            if ((ret = regcomp(&preg, pat, cflags))) {
                size_t errmsg_len = regerror(ret, &preg, NULL, 0);
                char errmsg[errmsg_len];        // VLA
                regerror(ret, &preg, errmsg, errmsg_len);
                KEYWORD_ERROR("failed compiling regex `%s`: %s (%d)", pat, errmsg, ret);
            }
            /* find all regex matches */
            const char *rinp = (inp);
            while (regexec(&preg, rinp, (sizeof(pmatch) / sizeof(*(pmatch))), pmatch, 0) == 0) {
                if (opts.match_index.ix == (count)
                    && opts.submatch_index.ix < (sizeof(pmatch) / sizeof(*(pmatch)))
                    && pmatch[opts.submatch_index.ix].rm_so != -1) {
                    (off) = pmatch[opts.submatch_index.ix].rm_so + (rinp - inp);
                    (len) =
                        pmatch[opts.submatch_index.ix].rm_eo - pmatch[opts.submatch_index.ix].rm_so;
                }
                (count)++;
                rinp += pmatch[0].rm_eo;
                if ('\0' == *rinp || pmatch[0].rm_eo == 0)      // consumed string or zero-length match
                    break;
#if DEBUG
                fprintf(stderr, "%zu `%s` %jd %jd\n", (count), rinp,
                        (intmax_t) pmatch[0].rm_so, (intmax_t) pmatch[0].rm_eo);
#endif
            }
            regfree(&preg);
        }
        else if ('\0' == *(inp) || '\0' == *(pat)) ;    // empty input or pattern
        else {  // substring, find all occurrences
            size_t _off = 0;
            size_t _len = strlen(pat);
            while ((_off = ss_find(ss_crefs(inp), _off, ss_crefs(pat))) != S_NPOS) {
                if (opts.match_index.ix == (count)) {
                    (off) = _off;
                    (len) = _len;
                }
                (count)++;
                _off += _len;
            }
        }

        if (opts.match_index.set || opts.submatch_index.set) {
            if (len > 0)
                BUFCATN(inp + off, len);
        }
        else
            BUFPUTINT(count);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int strsub_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct strsub_cmod_opts *const opts,
                            vec_namarg **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    strsub_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x3, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_strsub_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_namarg **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct strsub_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(strsub_opt_names) / sizeof(*(strsub_opt_names)));
        const char **opt_names = strsub_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "yline", 6) == 0) {
            ret = option_list_action_bool(&opts.byline_bool, opt, &opts.byline_isset);

        }
        else if ('c' == *((opt->name) + 0) && 'a' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.cat_bool, opt, &opts.cat_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0)) {
            if ('r' == *((opt->name) + 1) && 'e' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.ere_bool, opt, &opts.ere_isset);

            }
            else if ('s' == *((opt->name) + 1) && strncmp((opt->name) + 2, "caped", 6) == 0) {
                ret = option_list_action_bool(&opts.escaped_bool, opt, &opts.escaped_isset);

            }
            else {
                goto cmod_strin_else_20;
            }
        }
        else if ('g' == *((opt->name) + 0)) {
            if ('\0' == *((opt->name) + 1)) {
                ret = option_list_action_bool(&opts.g_bool, opt, &opts.g_isset);

            }
            else if ('l' == *((opt->name) + 1) && strncmp((opt->name) + 2, "obal", 5) == 0) {
                ret = option_list_action_bool(&opts.global_bool, opt, &opts.global_isset);

            }
            else {
                goto cmod_strin_else_20;
            }
        }
        else if ('i' == *((opt->name) + 0) && strncmp((opt->name) + 1, "case", 5) == 0) {
            ret = option_list_action_bool(&opts.icase_bool, opt, &opts.icase_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ewline", 7) == 0) {
            ret = option_list_action_bool(&opts.newline_bool, opt, &opts.newline_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egex", 5) == 0) {
            ret = option_list_action_bool(&opts.regex_bool, opt, &opts.regex_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1) && 'p' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.sep_str, opt, &opts.sep_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else {
                goto cmod_strin_else_20;
            }
        }
        else if ('w' == *((opt->name) + 0) && strncmp((opt->name) + 1, "hole", 5) == 0) {
            ret = option_list_action_bool(&opts.whole_bool, opt, &opts.whole_isset);

        }
        else {
            goto cmod_strin_else_20;
 cmod_strin_else_20:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    opts.global_isset = opts.global_bool = opts.global_bool || opts.g_bool;
    if (opts.icase_bool && !opts.regex_bool)
        KEYWORD_ERROR0("option [icase] requires option [regex]");

    int icuropt = -1, ibadopt = -1;
    if (strsub_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      strsub_opt_names[icuropt], strsub_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        strsub_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

#define INPUT_NAME "input"
#define MATCH_NAME "match"
#define SUB_NAME "sub"
        /* check and parse cmod_function_arguments */
        {
            const size_t nargs = sv_len((*x3));
            if (nargs < 3) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected at least three arguments: <" INPUT_NAME "> <" MATCH_NAME
                               "> <" SUB_NAME ">");
            }
            else if (nargs % 2 == 0) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("unmatched pattern-replacement pair");
            }

            const struct namarg *first = sv_at((*x3), 0);
            if (first->type != CMOD_ARG_VERBATIM) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected herestring input argument");
            }
            else if (NULL != first->name && strcmp(first->name, INPUT_NAME) != 0) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("input argument name must be `" INPUT_NAME "`");
            }

            srt_string *buf = NULL;
            (buf) = ss_alloc(8);
            if (ss_void == (buf)) {
                YYNOMEM;
            }

            for (size_t i = 1; i < nargs; ++i) {        // check match-then arg pairs
                const struct namarg *narg = sv_at((*x3), i);
                const bool is_match = (i % 2 != 0);
                if (narg->type != CMOD_ARG_VERBATIM) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                    KEYWORD_ERROR("expected herestring argument at position %zu", i);
                }
                if (NULL != narg->name) {
                    ss_clear(buf);
                    ss_cat_c(&buf, is_match ? MATCH_NAME : SUB_NAME);
                    if (i > 2) {
                        ss_cat_cn1(&buf, '/');
                        ss_cat_int(&buf, (i - 1) / 2);
                    }
                    if (strcmp(narg->name, ss_to_c(buf)) != 0) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR("unexpected named argument `%s` at position %zu",
                                      narg->name, i);
                    }
                }
            }
            ss_free(&buf);
        }

        const char *sep = (ss_len(opts.sep_str) > 0) ? ss_to_c(opts.sep_str) : " ";

        const struct namarg *arg_inp = sv_at((*x3), 0);
        const char *inp = CMOD_ARG_VERBATIM_get(*arg_inp);      // in here...
        srt_string *ss_inp = NULL;
        if (opts.escaped_bool) {
            if (NULL == (ss_inp = ss_dup_c(inp)))
                YYNOMEM;
            if (ss_void == ss_dec_esc_c(&(ss_inp))) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                KEYWORD_ERROR0("failed unescaping " INPUT_NAME " string");
            }
            inp = ss_to_c(ss_inp);
        }

        srt_string *buf = NULL;
        if (ss_void == (buf = ss_dup_c(inp))) {
            YYNOMEM;
        }

        ss_cat_cn1(&buf, '\0'); // NUL-terminate, FIXME: do without
        // do not NUL-terminate
        const size_t nargs = sv_len((*x3));
        for (size_t i = 1; i < nargs; i += 2) {
            const struct namarg *arg_pat = sv_at((*x3), i);
            const struct namarg *arg_sub = sv_at((*x3), i + 1);
            const char *pat = CMOD_ARG_VERBATIM_get(*arg_pat);  // ...replace this...
            const char *sub = CMOD_ARG_VERBATIM_get(*arg_sub);  // ...with this

            if ('\0' == *pat) {
                WARN0("empty pattern provided");
                continue;
            }
            srt_string *ss_pat = NULL;
            if (opts.escaped_bool) {
                if (NULL == (ss_pat = ss_dup_c(pat)))
                    YYNOMEM;
                if (ss_void == ss_dec_esc_c(&(ss_pat))) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    KEYWORD_ERROR0("failed unescaping " MATCH_NAME " string");
                }
                pat = ss_to_c(ss_pat);
            }

            size_t pat_len = strlen(pat);       // pattern length

            srt_string *(newbuf) = ss_alloc(ss_len(buf));
            if (ss_void == (newbuf)) {
                YYNOMEM;
            }
            ;   // buffer with replacements
            bool notpresent = false;    // no match?

            srt_string *ss_sub = NULL;
            if (opts.regex_bool) {
                int ret;
                regex_t preg;
                /* set flags */
                int cflags = 0L | (opts.icase_bool ? REG_ICASE : 0L)
                    | (opts.ere_bool ? REG_EXTENDED : 0L)
                    | (opts.newline_bool ? REG_NEWLINE : 0L);
                /* build pattern */
                srt_string *(spatt) = ss_alloc(pat_len);
                if (ss_void == (spatt)) {
                    YYNOMEM;
                }
                ;
                const char *at_start = (opts.whole_bool) ? "^" : "";
                const char *at_end = (opts.whole_bool) ? "$" : "";
                ss_cat_c(&spatt, at_start, pat, at_end);
                /* compile regex */
                if ('\0' == ss_at(spatt, 0))
                    KEYWORD_ERROR0("empty regex not allowed for portability");
                if ((ret = regcomp(&preg, ss_to_c(spatt), cflags))) {
                    size_t errmsg_len = regerror(ret, &preg, NULL, 0);
                    char errmsg[errmsg_len];    // VLA
                    regerror(ret, &preg, errmsg, errmsg_len);
                    KEYWORD_ERROR("failed compiling regex `%s`: %s (%d)",
                                  ss_to_c(spatt), errmsg, ret);
                }
                ss_free(&spatt);

                if (opts.byline_bool) { // apply line-by-line FIXME: use ss_split
                    char *nlptr = NULL;
                    const char *rinp = ss_get_buffer_r(buf);
                    while (NULL != (nlptr = strchr(rinp, '\n'))) {
                        *nlptr = '\0';  // anul newline
                        if (strregsub
                            (rinp, preg, sub, opts.global_bool, &newbuf, &rinp, &notpresent)) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                            KEYWORD_ERROR0("unknown escape sequence in replacement string");
                        }

                        ss_cat_c(&newbuf, rinp);        // append after match
                        rinp = nlptr + 1;       // advance over newline
                        if ('\0' != *rinp)
                            ss_cat_cn1(&newbuf, '\n');  // insert newline
                    }
                    if ('\0' != *rinp) {        // last line
                        if (strregsub
                            (rinp, preg, sub, opts.global_bool, &newbuf, &rinp, &notpresent)) {
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                            KEYWORD_ERROR0("unknown escape sequence in replacement string");
                        }

                    }
                    ss_cat_c(&newbuf, rinp);    // append rest of input
                }
                else {  // apply to whole input
                    const char *endptr = NULL;
                    if (strregsub
                        (ss_to_c(buf), preg, sub, opts.global_bool, &newbuf, &endptr,
                         &notpresent)) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                        KEYWORD_ERROR0("unknown escape sequence in replacement string");
                    }

                    ss_cat_c(&newbuf, endptr);  // append rest of input
                }

                regfree(&preg);
            }
            else {      // no regex
                if (opts.escaped_bool) {
                    if (NULL == (ss_sub = ss_dup_c(sub)))
                        YYNOMEM;
                    if (ss_void == ss_dec_esc_c(&(ss_sub))) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                        KEYWORD_ERROR0("failed unescaping " SUB_NAME " string");
                    }
                    sub = ss_to_c(ss_sub);
                }

                size_t off = ss_len(newbuf);
                ss_cat_c(&newbuf, ss_to_c(buf));        // append rest of input
                if (ss_find(buf, 0, ss_crefs(pat)) == S_NPOS) {
                    notpresent = true;
                }
                else {
                    if (opts.global_bool)
                        ss_replace(&newbuf, off, ss_crefs(pat), ss_crefs(sub));
                    else
                        ss_replace_single(&newbuf, off, ss_crefs(pat), ss_crefs(sub));
                }
            }

            if (notpresent && opts.cat_bool) {
                ss_cat_c(&newbuf, sep, sub);    // append replacement string
            }
            else if (notpresent)
                VERBOSE0("no matches found");

            ss_cpy(&buf, newbuf);       // replace new buffer (no terminating NUL)
            ss_cat_cn1(&buf, '\0');     // NUL-terminate, FIXME: do without
            ss_free(&newbuf);   // free new buffer
            ss_free(&ss_sub);
            ss_free(&ss_pat);
#if DEBUG
            fprintf(stderr, "%s\n", ss_to_c(buf));
#endif
        }
        ss_popchar(&buf);       // remove terminating NUL

        BUFCAT(buf);
        ss_free(&buf);
        ss_free(&ss_inp);
#undef INPUT_NAME
#undef MATCH_NAME
#undef SUB_NAME

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&(opts.sep_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_rule0_str(const struct param *const pp,
                           size_t debug_level,
                           const struct table_cmod_opts *const opts,
                           char **x3, vec_namarg **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x4, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_table_action_0(void *yyscanner,
                        struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        char **x3, const YYLTYPE *loc3,
                        vec_namarg **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct table_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_opt_names) / sizeof(*(table_opt_names)));
        const char **opt_names = table_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_index(&opts.append_index, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ash", 4) == 0) {
            ret = option_list_action_column(&opts.hash_column, opt, &opts.hash_isset);

        }
        else if ('j' == *((opt->name) + 0) && strncmp((opt->name) + 1, "son", 4) == 0) {
            ret = option_list_action_bool(&opts.json_bool, opt, &opts.json_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else if ('t' == *((opt->name) + 0) && 's' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.tsv_bool, opt, &opts.tsv_isset);

        }
        else {
            goto cmod_strin_else_21;
 cmod_strin_else_21:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.json_isset) {
        opts.json_bool = false;
        opts.json_isset = false;
        WARN0("ignoring option [json]");
    }
    if (opts.tsv_isset) {
        opts.tsv_bool = false;
        opts.tsv_isset = false;
        WARN0("ignoring option [tsv]");
    }
    opts.redef_bool = true;
    opts.redef_isset = true;

    int icuropt = -1, ibadopt = -1;
    if (table_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_opt_names[icuropt], table_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct table *oldtab = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_tab) {
                }
                _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
            }

            if (_found.set)
                oldtab = (void *)sv_at(pp->sv_tab, _found.ix);
        }
        if (NULL != (oldtab)) {
            if (!(opts.append_index.set) && opts.opt_bool) {
                VERBOSE("ignoring already defined table `%s`", (*x3));

                goto epilogue;
            }
            else if (!(oldtab)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("table `%s` is not redefinable", (*x3));
            }
        }

        struct table tab = {
            .name = (*x3),.sv_colnarg = (*x4),.svv_rows = NULL,
            .redef = opts.redef_bool,.key = SETIX_none,.shm_colnam = NULL,
            .nnondef = sv_len((*x4))
        };

        (*x3) = NULL;   // hand-over
        (*x4) = NULL;   // hand-over

        {
            size_t ierr = 0;
            union table_body tbody = {.verbatim = NULL };
            int ret = table_build(tbody, tab.sv_colnarg, TABLE_FMT_TSV, &(tab), &ierr);
            switch (ret) {
            case TABLE_PARSE_OK:
                break;
            case TABLE_PARSE_OOM:
                YYNOMEM;
                break;
            case TABLE_PARSE_BADROW:
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR0("incorrect row size in table body");
                break;
            case TABLE_PARSE_NDEFAULT:
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("expected %zu (got %zu) non-default columns in table body",
                              tab.nnondef, ierr);
                break;
            default:
                switch (TABLE_FMT_TSV) {
                case TABLE_FMT_TSV:
                    switch (ret) {
                    case TSV_PARSE_INIT:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("failed initializing scanner" " in TSV table body"
                                      ": %s (%d)", strerror(errno), errno);
                        break;
                    default:
                        assert(0 && "C% internal error" "");
                        break;
                    }
                    break;
                case TABLE_FMT_JSON:
                    switch (ret) {
                    case JSON_PARSE_INVALID:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("invalid JSON token" " in JSON table body" " at position %zu",
                                      ierr);
                        break;
                    case JSON_PARSE_PARTIAL:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("partial JSON input" " in JSON table body" " at position %zu",
                                      ierr);
                        break;
                    case JSON_PARSE_DUPLICATE:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("duplicate column name" " in JSON table body"
                                      " at position %zu", ierr);
                        break;
                    case JSON_PARSE_BADCOLUMN:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR("unknown or mismatched column name" " in JSON table body"
                                      " at position %zu", ierr);
                        break;
                    case JSON_PARSE_FORMAT:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR0("bad format" " in JSON table body")
                            break;
                    case JSON_PARSE_UNKNOWN:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR0("unknown JSON error" " in JSON table body")
                            break;
                    case JSON_PARSE_MULTI:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR0("bad multi-table format" " in JSON table body")
                            break;
                    default:
                        assert(0 && "C% internal error" "");
                        break;
                    }
                    break;
                case TABLE_FMT_LAMBDA:
                    /* no lambda-specific error codes */
                    break;
                }
            }
        }
        if (NULL == tab.shm_colnam) {
            (tab.shm_colnam) = shm_alloc(SHM_SU, sv_len(tab.sv_colnarg));
            if (NULL == (tab.shm_colnam)) {
                YYNOMEM;
            }
            {
                const size_t _len = sv_len(tab.sv_colnarg);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _52 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    {
                        const srt_string *_ss =
                            ss_crefs(((const struct namarg *)sv_at(tab.sv_colnarg, _52))->name);
                        if (!shm_insert_su(&(tab.shm_colnam), _ss, _52)) {
                            YYNOMEM;
                        }
                    }
                }
            }
        }
        if (NULL == oldtab) {
            {
                const srt_string *_ss = ss_crefs((tab).name);
                if (!sv_push(&(pp->sv_tab), &(tab))
                    || !shm_insert_su(&(pp->shm_tab), _ss, sv_len(pp->sv_tab) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining table `%s`", (tab).name);
            table_clear(oldtab);
            *(oldtab) = tab;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_rule1_str(const struct param *const pp,
                           size_t debug_level,
                           const struct table_cmod_opts *const opts,
                           char **x3, char *x4, struct table *x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    switch (*x4) {
    case '=':
        ss_cat_c(s, "  = ");
        break;
    case '?':
        ss_cat_c(s, " ?= ");
        break;
    case '+':
        ss_cat_c(s, " += ");
        break;
    case ':':
        ss_cat_c(s, " := ");
        break;
    case '|':
        ss_cat_c(s, " |= ");
        break;
    }

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*&x5->sv_colnarg, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');
    ss_cat_c(s, " %" "{ ");
    if (debug_level >= 2) {
        /* TODO: print table body */
        ss_cat_c(s, "<table body>");
    }
    else
        ss_cat_c(s, "<table body>");
    ss_cat_c(s, " %" "}");

    return 0;
}

int cmod_table_action_1(void *yyscanner,
                        struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        char **x3, const YYLTYPE *loc3,
                        char *x4, const YYLTYPE *loc4,
                        struct table *x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct table_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_opt_names) / sizeof(*(table_opt_names)));
        const char **opt_names = table_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_index(&opts.append_index, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ash", 4) == 0) {
            ret = option_list_action_column(&opts.hash_column, opt, &opts.hash_isset);

        }
        else if ('j' == *((opt->name) + 0) && strncmp((opt->name) + 1, "son", 4) == 0) {
            ret = option_list_action_bool(&opts.json_bool, opt, &opts.json_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else if ('t' == *((opt->name) + 0) && 's' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.tsv_bool, opt, &opts.tsv_isset);

        }
        else {
            goto cmod_strin_else_22;
 cmod_strin_else_22:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.append_index.set && opts.append_index.ix > 1) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("[append] only takes values: 0 = rows, 1 = columns");
    }
    switch ((*x4)) {
    case '=':  /* normal */
        break;
    case '?':
        opts.opt_bool = true;
        opts.opt_isset = true;
        break;
    case ':':
        opts.redef_bool = true;
        opts.redef_isset = true;
        break;
    case '+':
        opts.append_index = SETIX_set(0);
        opts.append_isset = true;

        opts.redef_bool = true;
        opts.redef_isset = true;

        break;
    case '|':
        opts.append_index = SETIX_set(1);
        opts.append_isset = true;

        opts.redef_bool = true;
        opts.redef_isset = true;

        break;
    default:
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
        KEYWORD_ERROR0("unsupported assignment operator");
        break;
    }
    if (opts.json_isset) {
        opts.json_bool = false;
        opts.json_isset = false;
        WARN0("ignoring option [json]");
    }
    if (opts.tsv_isset) {
        opts.tsv_bool = false;
        opts.tsv_isset = false;
        WARN0("ignoring option [tsv]");
    }

    int icuropt = -1, ibadopt = -1;
    if (table_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_opt_names[icuropt], table_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_rule1_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        struct table *oldtab = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_tab) {
                }
                _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
            }

            if (_found.set)
                oldtab = (void *)sv_at(pp->sv_tab, _found.ix);
        }
        if (NULL != (oldtab)) {
            if (!(opts.append_index.set) && opts.opt_bool) {
                VERBOSE("ignoring already defined table `%s`", (*x3));

                goto epilogue;
            }
            else if (!(oldtab)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("table `%s` is not redefinable", (*x3));
            }
        }

        struct table tab = (*x5);
        (*x5) = (struct table) { 0 };   // hand-over
        tab.name = (*x3);
        (*x3) = NULL;   // hand-over
        tab.redef = opts.redef_bool;

        if (opts.append_index.set && NULL != oldtab) {
            if (opts.append_index.ix == 0) {    // row-wise
                // checks
                if (TABLE_NCOLS_NODEF(tab) != TABLE_NCOLS_NODEF(*oldtab)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR
                        ("[append] expected %zu (got %zu) non-default columns in table `%s`",
                         TABLE_NCOLS_NODEF(*oldtab), TABLE_NCOLS_NODEF(tab), tab.name);
                }
                if (TABLE_NCOLS(tab) > 0) {     // check column names, if present
                    if (TABLE_NCOLS_DEF(tab) > 0)
                        WARN("[append] ignoring columns (%zu) with default values",
                             TABLE_NCOLS_DEF(tab));
                    for (size_t i = 0; i < TABLE_NCOLS(tab); ++i) {
                        const struct namarg *oldcol = sv_at(oldtab->sv_colnarg, i);
                        const struct namarg *newcol = sv_at(tab.sv_colnarg, i);
                        if (strcmp(oldcol->name, newcol->name) != 0) {  // column names differ
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR
                                ("[append] mismatched column `%s` at position %zu in table `%s`",
                                 newcol->name, i, tab.name);
                        }
                    }
                }
                // get new stuff
                vec_vec_string *newrows = tab.svv_rows;
                tab.svv_rows = NULL;    // hand-over
                table_clear(&tab);
                // keep old stuff
                tab = *oldtab;
                tab.hmap = NULL;        // do NOT hand-over, to be updated
                oldtab->name = NULL;    // hand-over
                oldtab->sv_colnarg = NULL;      // hand-over
                oldtab->shm_colnam = NULL;      // hand-over
                oldtab->svv_rows = NULL;        // hand-over
                // add new rows
                sv_cat(&tab.svv_rows, newrows);
                sv_free(&newrows);
            }
            else {      // column-wise
                // checks
                if (TABLE_NROWS(tab) != TABLE_NROWS(*oldtab)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR("[append] expected %zu (got %zu) rows",
                                  TABLE_NROWS(*oldtab), TABLE_NROWS(tab));
                }
                if (TABLE_NCOLS(tab) > 0) {
                    for (size_t i = 0; i < TABLE_NCOLS(tab); ++i) {     // check column names
                        const struct namarg *newcol = sv_at(tab.sv_colnarg, i);
                        setix found = { 0 };
                        {
                            const srt_string *_ss = ss_crefs(newcol->name);
                            if (NULL == oldtab->shm_colnam) {
                            }
                            found.set = shm_atp_su(oldtab->shm_colnam, _ss, &(found).ix);
                        }

                        if (found.set) {        // duplicate column name
                            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                            KEYWORD_ERROR("[append] duplicate column `%s`", newcol->name);
                        }
                    }
                }
                else
                    KEYWORD_ERROR0("[append] expected at least one new column name");
                // get new stuff
                vec_vec_string *newrows = tab.svv_rows;
                tab.svv_rows = NULL;    // hand-over
                vec_namarg *newcols = tab.sv_colnarg;
                tab.sv_colnarg = NULL;  // hand-over
                size_t nnondef = tab.nnondef;
                table_clear(&tab);
                // keep old stuff
                tab = *oldtab;
                oldtab->name = NULL;    // hand-over
                oldtab->sv_colnarg = NULL;      // hand-over
                oldtab->svv_rows = NULL;        // hand-over
                {       // add new column names (keep order, defaults at end)
                    size_t ntotcol = TABLE_NCOLS(tab) + sv_len(newcols);
                    vec_namarg *(tmp) = sv_alloc(sizeof(struct namarg), ntotcol, NULL);
                    if (sv_void == (tmp)) {
                        YYNOMEM;
                    }

                    size_t n = 0;
                    // old non-default columns
                    for (size_t i = 0; i < TABLE_NCOLS_NODEF(tab); ++i, ++n)
                        sv_set(&tmp, n, sv_at(tab.sv_colnarg, i));
                    // new columns
                    for (size_t i = 0; i < sv_len(newcols); ++i, ++n)
                        sv_set(&tmp, n, sv_at(newcols, i));
                    // old default columns
                    for (size_t i = TABLE_NCOLS_NODEF(tab); i < TABLE_NCOLS(tab); ++i, ++n)
                        sv_set(&tmp, n, sv_at(tab.sv_colnarg, i));
                    assert(n == ntotcol);
                    sv_free(&tab.sv_colnarg);
                    sv_free(&newcols);
                    tab.sv_colnarg = tmp;
                }
                tab.nnondef += nnondef;
                // add new values to rows
                for (size_t i = 0; i < TABLE_NROWS(tab); ++i) {
                    vec_string **row = (vec_string **) sv_at(tab.svv_rows, i);
                    vec_string *newrow = sv_at_ptr(newrows, i);
                    sv_cat(row, newrow);
                    sv_free(&newrow);
                }
                sv_free(&newrows);
                // update column map
                tab.shm_colnam = NULL;  // do NOT hand-over, to be updated
                if (NULL == tab.shm_colnam) {
                    (tab.shm_colnam) = shm_alloc(SHM_SU, sv_len(tab.sv_colnarg));
                    if (NULL == (tab.shm_colnam)) {
                        YYNOMEM;
                    }
                    {
                        const size_t _len = sv_len(tab.sv_colnarg);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _53 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            {
                                const srt_string *_ss =
                                    ss_crefs(((const struct namarg *)sv_at(tab.sv_colnarg, _53))->
                                             name);
                                if (!shm_insert_su(&(tab.shm_colnam), _ss, _53)) {
                                    YYNOMEM;
                                }
                            }
                        }
                    }
                }
                // may update index column
                if (NULL != opts.hash_column) {
                    size_t hash_index = 0;
                    if (tab_icol_from_opt(&(tab), opts.hash_column, &hash_index)) {
                        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                        switch ((opts.hash_column)->type) {
                        case ENUM_CMOD_OPTION(XINT):
                            KEYWORD_ERROR("[hash] bad column index %" PRIuMAX " for table `%s`",
                                          XINT_UNSIGNED_get(CMOD_OPTION_XINT_get
                                                            (*(opts.hash_column))), (tab).name);
                            break;
                        case ENUM_CMOD_OPTION(STRING):
                            KEYWORD_ERROR("[hash] column `%s` not present in table `%s`",
                                          ss_to_c(CMOD_OPTION_STRING_get(*(opts.hash_column))),
                                          (tab).name);
                            break;
                        default:       // already checked in parser
                            assert(0 && "C% internal error" "");
                            break;
                        }
                    }
                    (tab).key = SETIX_set(hash_index);
                }
                if (oldtab->key.set && tab.key.set && oldtab->key.ix != tab.key.ix)
                    tab.hmap = NULL;    // do not hand-over, to be updated
            }
            pp->redef_nowarn = true;
        }
        else {
            /* if not present, get table column names from first row */
            if (sv_len(tab.svv_rows) > 0 && NULL == tab.sv_colnarg) {
                {
                    int ret =
                        table_firstrow_colnames(&(tab.svv_rows), &(tab.sv_colnarg), &(tab.nnondef));
                    const char *errstr = NULL;
                    if (ret)
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    switch (ret) {
                    case 1:
                        errstr = "out of memory" " in";
                        break;
                    case 4:
                        errstr = "expected an extended identifier as column name in";
                        break;
                    default:
                        errstr = "unspecific error in";
                        break;
                    }
                    if (ret)
                        KEYWORD_ERROR("%s table `%s`", errstr, tab.name);
                }
                if (NULL == tab.shm_colnam) {
                    (tab.shm_colnam) = shm_alloc(SHM_SU, sv_len(tab.sv_colnarg));
                    if (NULL == (tab.shm_colnam)) {
                        YYNOMEM;
                    }
                    {
                        const size_t _len = sv_len(tab.sv_colnarg);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _54 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            {
                                const srt_string *_ss =
                                    ss_crefs(((const struct namarg *)sv_at(tab.sv_colnarg, _54))->
                                             name);
                                if (!shm_insert_su(&(tab.shm_colnam), _ss, _54)) {
                                    YYNOMEM;
                                }
                            }
                        }
                    }
                }
            }
            if (NULL != opts.hash_column) {
                size_t hash_index = 0;
                if (tab_icol_from_opt(&(tab), opts.hash_column, &hash_index)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    switch ((opts.hash_column)->type) {
                    case ENUM_CMOD_OPTION(XINT):
                        KEYWORD_ERROR("[hash] bad column index %" PRIuMAX " for table `%s`",
                                      XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*(opts.hash_column))),
                                      (tab).name);
                        break;
                    case ENUM_CMOD_OPTION(STRING):
                        KEYWORD_ERROR("[hash] column `%s` not present in table `%s`",
                                      ss_to_c(CMOD_OPTION_STRING_get(*(opts.hash_column))),
                                      (tab).name);
                        break;
                    default:   // already checked in parser
                        assert(0 && "C% internal error" "");
                        break;
                    }
                }
                (tab).key = SETIX_set(hash_index);
            }
        }

        if ((tab).key.set && NULL == (tab).hmap) {
            size_t nrow = sv_len((tab).svv_rows);
            ((tab).hmap) = shm_alloc(SHM_SU, nrow);
            if (NULL == ((tab).hmap)) {
                YYNOMEM;
            }
            for (size_t i = 0; i < nrow; ++i) {
                const char *key = table_getcell(&(tab), i, (tab).key.ix);
                const uint64_t value = i;
                {
                    const srt_string *_ss = ss_crefs(key);
                    if (!shm_insert_su(&((tab).hmap), _ss, value)) {
                        YYNOMEM;
                    }
                }
            }
            if (shm_size((tab).hmap) != nrow) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("non-unique values in hash key column `%s` of table `%s`",
                              TABLE_COLNAME(&tab, (tab).key.ix), (tab).name);
            }
        }
        if (NULL == oldtab) {
            {
                const srt_string *_ss = ss_crefs((tab).name);
                if (!sv_push(&(pp->sv_tab), &(tab))
                    || !shm_insert_su(&(pp->shm_tab), _ss, sv_len(pp->sv_tab) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining table `%s`", (tab).name);
            table_clear(oldtab);
            *(oldtab) = tab;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_rule2_str(const struct param *const pp,
                           size_t debug_level,
                           const struct table_cmod_opts *const opts,
                           char **x3, char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_opt_string(opts, s);
    (void)x3;

    if (NULL != *x3) {
        ss_cat_cn1(s, ' ');
        ss_cat_c(s, "`", *x3, "`");
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "\"", *x4, "\"");

    return 0;
}

int cmod_table_action_2(void *yyscanner,
                        struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        char **x3, const YYLTYPE *loc3,
                        char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct table_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_opt_names) / sizeof(*(table_opt_names)));
        const char **opt_names = table_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ppend", 6) == 0) {
            ret = option_list_action_index(&opts.append_index, opt, &opts.append_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ash", 4) == 0) {
            ret = option_list_action_column(&opts.hash_column, opt, &opts.hash_isset);

        }
        else if ('j' == *((opt->name) + 0) && strncmp((opt->name) + 1, "son", 4) == 0) {
            ret = option_list_action_bool(&opts.json_bool, opt, &opts.json_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "edef", 5) == 0) {
            ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

        }
        else if ('t' == *((opt->name) + 0) && 's' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.tsv_bool, opt, &opts.tsv_isset);

        }
        else {
            goto cmod_strin_else_23;
 cmod_strin_else_23:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.append_isset) {
        opts.append_index = SETIX_none;
        opts.append_isset = false;
        WARN0("ignoring option [append]");
    }

    int icuropt = -1, ibadopt = -1;
    if (table_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_opt_names[icuropt], table_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_rule2_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        {       // get format (and table name) from filename
            char *path = (NULL != ((*x4))) ? rstrdup((*x4)) : NULL;
            if (NULL != ((*x4)) && NULL == (path)) {
                YYNOMEM;
            }

            char *base = basename(path);
            char *ext = strrchr(base, '.');     // last dot

            if (!opts.tsv_bool && !opts.json_bool) {    // get format from extension
                if ((NULL != (ext) && '.' == (ext)[0] && ('t' == (ext)[1] || 'T' == (ext)[1])
                     && ('s' == (ext)[2] || 'S' == (ext)[2]) && ('v' == (ext)[3] || 'V' == (ext)[3])
                     && '\0' == (ext)[4])
                    )
                    opts.tsv_bool = true;
                else if ((NULL != (ext) && '.' == (ext)[0] && ('j' == (ext)[1] || 'J' == (ext)[1])
                          && ('s' == (ext)[2] || 'S' == (ext)[2]) && ('o' == (ext)[3]
                                                                      || 'O' == (ext)[3])
                          && ('n' == (ext)[4] || 'N' == (ext)[4]) && '\0' == (ext)[5])
                    )
                    opts.json_bool = true;
                else {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR0("unknown file extension and no specified format");
                }
            }

            if (opts.tsv_bool && NULL == (*x3)) {       // get default table name from file
                if (NULL != ext)
                    *ext = '\0';
                (*x3) = (NULL != (base)) ? rstrdup(base) : NULL;
                if (NULL != (base) && NULL == ((*x3))) {
                    YYNOMEM;
                }

            }

            free(path);
        }

        srt_string *filestr = NULL;
        char *fullpath = NULL;
        setix found = { 0 };

        {       // load file into byte buffer (from CWD and include path)
            FILE *fp = fopen_incpath((*x4), pp->sv_ipath, false, &found);
            if (NULL == fp) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("cannot find \"%s\" in %ssystem include search path", (*x4),
                              "current working directory or ");
            }
            fullpath = fullpath_incpath((*x4), pp->sv_ipath, found);
            if (NULL == fullpath)
                YYNOMEM;

            size_t filebytes = 0;
            {   // count bytes in file
                errno = 0;
                if (fseeko(fp, 0, SEEK_END))    // goto end of file
                    KEYWORD_ERROR("failed counting bytes in file \"%s\": %s (%d)",
                                  fullpath, strerror(errno), errno);
                off_t len = 0;
                errno = 0;
                if ((len = ftello(fp)) == -1)   // get offset
                    KEYWORD_ERROR("failed counting bytes in file \"%s\": %s (%d)",
                                  fullpath, strerror(errno), errno);
                filebytes = len;
#if DEBUG
                fprintf(stderr, "%lld bytes\n", (long long)len);
#endif
            }

            if (filebytes == 0) {
                (void)fclose(fp);
                WARN("skipping empty file \"%s\"", fullpath);
                goto epilogue;
            }

            rewind(fp); // go to start of file
            ssize_t nread = ss_read(&filestr, fp, filebytes);
            if (nread <= 0 || (size_t)nread != filebytes)
                KEYWORD_ERROR("failed reading %zu bytes from file \"%s\": %s",
                              filebytes, fullpath, feof(fp) ? "end-of-file indicator set" :
                              (ferror(fp) ? "error indicator set" : "unknown failure"));
            (void)fclose(fp);
            ss_cat_cn1(&filestr, '\0'); // add NUL terminator
        }

        vec_table *(tables) = sv_alloc(sizeof(struct table), 16, NULL);
        if (sv_void == (tables)) {
            YYNOMEM;
        }
        ;
        if (opts.json_bool) {   // load multiple JSON tables
            {   // parse multiple JSON tables
                const char *jstr = ss_get_buffer_r(filestr);
                size_t ierr;
                enum json_parse ret =
                    multitable_json_parse(jstr, ss_len(filestr), &(tables), &ierr);
                switch (ret) {
                case JSON_PARSE_OK:
                    break;
                case JSON_PARSE_OOM:
                    YYNOMEM;
                    break;
                case JSON_PARSE_BADROW:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("incorrect row size in table in JSON file `%s`", fullpath);
                    break;
                case JSON_PARSE_NDEFAULT:      /* cannot happen here */
                    break;
                case JSON_PARSE_INVALID:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("invalid JSON token" " in JSON file `%s`" " at position %zu",
                                  fullpath, ierr);
                    break;
                case JSON_PARSE_PARTIAL:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("partial JSON input" " in JSON file `%s`" " at position %zu",
                                  fullpath, ierr);
                    break;
                case JSON_PARSE_DUPLICATE:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("duplicate column name" " in JSON file `%s`" " at position %zu",
                                  fullpath, ierr);
                    break;
                case JSON_PARSE_BADCOLUMN:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("unknown or mismatched column name" " in JSON file `%s`"
                                  " at position %zu", fullpath, ierr);
                    break;
                case JSON_PARSE_FORMAT:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("bad format" " in JSON file `%s`", fullpath)
                        break;
                case JSON_PARSE_UNKNOWN:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("unknown JSON error" " in JSON file `%s`", fullpath)
                        break;
                case JSON_PARSE_MULTI:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("bad multi-table format" " in JSON file `%s`", fullpath)
                        break;
                }
            }

            /* process loaded tables */
            for (size_t i = 0; i < sv_len(tables); ++i) {
                struct table *tab = (struct table *)sv_at(tables, i);
                if (NULL != (*x3)) {    // prepend prefix to table name
                    srt_string *(newname) = ss_alloc(0);
                    if (ss_void == (newname)) {
                        YYNOMEM;
                    }
                    ;
                    ss_cat_c(&newname, (*x3), tab->name);
                    free(tab->name);
                    {
                        const char *ctmp = ss_to_c(newname);
                        tab->name = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                        if (NULL != (ctmp) && NULL == (tab->name)) {
                        }

                    }
                    ss_free(&(newname));
                    if (NULL == tab->name) {
                        YYNOMEM;
                    }
                }
                tab->redef = opts.redef_bool;
                tab->nnondef = TABLE_NCOLS(*tab);
            }
        }
        else if (opts.tsv_bool) {       // load single TSV table
            size_t nnondef = 0;
            size_t tab_ncol = 0;
            vec_vec_string *svv_rows = NULL;
            vec_namarg *sv_colnarg = NULL;

            {   // parse TSV table
                const char *tsv = ss_get_buffer_r(filestr);
                enum tsv_parse ret =
                    table_tsv_parse(tsv, ss_len(filestr), &(tab_ncol), &(svv_rows));
                switch (ret) {
                case TSV_PARSE_OK:
                    break;
                case TSV_PARSE_OOM:
                    YYNOMEM;
                    break;
                case TSV_PARSE_BADROW:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("incorrect row size in TSV table `%s`", (*x3));
                    break;
                case TSV_PARSE_NDEFAULT:       /* not happening here */
                    break;
                case TSV_PARSE_INIT:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("failed initializing scanner" " in TSV table `%s`" ": %s (%d)",
                                  (*x3), strerror(errno), errno);
                    break;
                }
            }

            if (sv_len(svv_rows) > 0) { // get column names from first row
                {
                    int ret = table_firstrow_colnames(&(svv_rows), &(sv_colnarg), &(nnondef));
                    const char *errstr = NULL;
                    if (ret)
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    switch (ret) {
                    case 1:
                        errstr = "out of memory" " in";
                        break;
                    case 4:
                        errstr = "expected an extended identifier as column name in";
                        break;
                    default:
                        errstr = "unspecific error in";
                        break;
                    }
                    if (ret)
                        KEYWORD_ERROR("%s table `%s`", errstr, (*x3));
                }

            }

            struct table tab = {
                .name = (*x3),.sv_colnarg = sv_colnarg,.svv_rows = svv_rows,
                .redef = opts.redef_bool,.key = SETIX_none,.shm_colnam = NULL,
                .nnondef = nnondef
            };
            (*x3) = NULL;       // hand-over

            sv_push(&tables, &tab);
        }
        else {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR("unknown format for table file `%s`", (*x4));
        }
        ss_free(&filestr);

        /* process new tables */
        for (size_t i = 0; i < sv_len(tables); ++i) {
            struct table *newtab = (struct table *)sv_at(tables, i);

            /* check duplicate */
            struct table *oldtab = NULL;
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs(newtab->name);
                    if (NULL == pp->shm_tab) {
                    }
                    _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                }

                if (_found.set)
                    oldtab = (void *)sv_at(pp->sv_tab, _found.ix);
            }
            if (NULL != (oldtab)) {
                if (!(opts.append_index.set) && opts.opt_bool) {
                    VERBOSE("ignoring already defined table `%s`", newtab->name);
                    table_clear(newtab);
                    continue;
                }
                else if (!(oldtab)->redef) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("table `%s` is not redefinable", newtab->name);
                }
            }

            if (NULL == newtab->shm_colnam) {
                (newtab->shm_colnam) = shm_alloc(SHM_SU, sv_len(newtab->sv_colnarg));
                if (NULL == (newtab->shm_colnam)) {
                    YYNOMEM;
                }
                {
                    const size_t _len = sv_len(newtab->sv_colnarg);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _55 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        {
                            const srt_string *_ss =
                                ss_crefs(((const struct namarg *)sv_at(newtab->sv_colnarg, _55))->
                                         name);
                            if (!shm_insert_su(&(newtab->shm_colnam), _ss, _55)) {
                                YYNOMEM;
                            }
                        }
                    }
                }
            }
            if (NULL != opts.hash_column) {
                size_t hash_index = 0;
                if (tab_icol_from_opt(&(*newtab), opts.hash_column, &hash_index)) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    switch ((opts.hash_column)->type) {
                    case ENUM_CMOD_OPTION(XINT):
                        KEYWORD_ERROR("[hash] bad column index %" PRIuMAX " for table `%s`",
                                      XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*(opts.hash_column))),
                                      (*newtab).name);
                        break;
                    case ENUM_CMOD_OPTION(STRING):
                        KEYWORD_ERROR("[hash] column `%s` not present in table `%s`",
                                      ss_to_c(CMOD_OPTION_STRING_get(*(opts.hash_column))),
                                      (*newtab).name);
                        break;
                    default:   // already checked in parser
                        assert(0 && "C% internal error" "");
                        break;
                    }
                }
                (*newtab).key = SETIX_set(hash_index);
            }
            if ((*newtab).key.set && NULL == (*newtab).hmap) {
                size_t nrow = sv_len((*newtab).svv_rows);
                ((*newtab).hmap) = shm_alloc(SHM_SU, nrow);
                if (NULL == ((*newtab).hmap)) {
                    YYNOMEM;
                }
                for (size_t i = 0; i < nrow; ++i) {
                    const char *key = table_getcell(&(*newtab), i, (*newtab).key.ix);
                    const uint64_t value = i;
                    {
                        const srt_string *_ss = ss_crefs(key);
                        if (!shm_insert_su(&((*newtab).hmap), _ss, value)) {
                            YYNOMEM;
                        }
                    }
                }
                if (shm_size((*newtab).hmap) != nrow) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("non-unique values in hash key column `%s` of table `%s`",
                                  TABLE_COLNAME(&*newtab, (*newtab).key.ix), (*newtab).name);
                }
            }
            if (NULL == oldtab) {
                {
                    const srt_string *_ss = ss_crefs((*newtab).name);
                    if (!sv_push(&(pp->sv_tab), &(*newtab))
                        || !shm_insert_su(&(pp->shm_tab), _ss, sv_len(pp->sv_tab) - 1)) {
                        YYNOMEM;
                    }
                }
            }
            else {      // already exists
                if (!(pp->redef_nowarn))
                    WARN("redefining table `%s`", (*newtab).name);
                table_clear(oldtab);
                *(oldtab) = *newtab;
            }

            VERBOSE("loaded %stable `%s` from file \"%s\"",
                    sv_len(newtab->svv_rows) == 0 ? "empty " : "", newtab->name, fullpath);

            *newtab = (struct table) { 0 };     // hand-over
        }
        sv_free(&tables);

        if (fullpath != (*x4))
            free(fullpath);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_get_rule0_str(const struct param *const pp,
                               size_t debug_level,
                               const struct table_get_cmod_opts *const opts,
                               vec_string **x3, vec_namarg **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_get_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, "`", *(&x), "`");
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    vec_namarg_str_repr(*x4, SETIX_none, SETIX_none, s);
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_table_get_action_0(void *yyscanner,
                            const struct param *pp,
                            vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                            vec_string **x3, const YYLTYPE *loc3,
                            vec_namarg **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct table_get_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_get_opt_names) / sizeof(*(table_get_opt_names)));
        const char **opt_names = table_get_opt_names;   // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'r' == *((opt->name) + 1) && 'e' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.ere_bool, opt, &opts.ere_isset);

        }
        else if ('i' == *((opt->name) + 0) && strncmp((opt->name) + 1, "case", 5) == 0) {
            ret = option_list_action_bool(&opts.icase_bool, opt, &opts.icase_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egex", 5) == 0) {
            ret = option_list_action_bool(&opts.regex_bool, opt, &opts.regex_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1)
                 && 'r' == *((opt->name) + 2)) {
            if ('\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

            }
            else if ('i' == *((opt->name) + 3) && 'c' == *((opt->name) + 4)
                     && 't' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                ret = option_list_action_bool(&opts.strict_bool, opt, &opts.strict_isset);

            }
            else {
                goto cmod_strin_else_24;
            }
        }
        else {
            goto cmod_strin_else_24;
 cmod_strin_else_24:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (table_get_opt_compat(&opts, &icuropt, &ibadopt)) {      // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_get_opt_names[icuropt], table_get_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_get_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        const char *search_key = NULL;
        const char *search_column = NULL;
        const char *value_column = NULL;
        bool row_index_mode = false;
        /* check and parse cmod_function_arguments */
        {
#define SEARCH_NAME "search"
#define KEY_NAME "key"
#define VALUE_NAME "value"
            size_t nargs = sv_len((*x4));
            assert(nargs > 0);
            if (nargs > 3) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR0("expected up to three arguments: "
                               "[<" SEARCH_NAME ">] <" KEY_NAME "> [<" VALUE_NAME ">]");
            }
            if (nargs == 1) {
                const struct namarg *only = sv_at((*x4), 0);
                const char *txt = NULL;
                switch (only->type) {
                case CMOD_ARG_VERBATIM:
                    txt = CMOD_ARG_VERBATIM_get(*only);
                    if (NULL == only->name || strcmp(only->name, KEY_NAME) == 0) {
                        search_key = txt;       // const borrow
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = only->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected herestring named argument `%s`", only->name);
                    }
                    break;
                case CMOD_ARG_INTSTR:
                    row_index_mode = true;
                    txt = CMOD_ARG_INTSTR_get(*only);
                    if (NULL == only->name || strcmp(only->name, KEY_NAME) == 0) {
                        search_key = txt;       // const borrow
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = only->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected integer named argument `%s`", only->name);
                    }
                    break;
                default:
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR0("expected herestring or integer argument");
                    break;
                }
            }
            else {      // 2 or 3
                for (size_t i = 0; i < nargs; ++i) {
                    const struct namarg *narg = sv_at((*x4), i);
                    const char *txt = NULL;
                    switch (narg->type) {
                    case CMOD_ARG_SPECIAL:
                        txt = CMOD_ARG_SPECIAL_get(*narg);
                        if (NULL == narg->name) {
                            if (i == 0)
                                search_column = txt;    // const borrow
                            else if (i == nargs - 1)
                                value_column = txt;     // const borrow
                            else {
                                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                                KEYWORD_ERROR
                                    ("unexpected special identifier argument at position %zu", i);
                            }
                        }
                        else if (strcmp(narg->name, SEARCH_NAME) == 0) {
                            search_column = txt;        // const borrow
                        }
                        else if (strcmp(narg->name, VALUE_NAME) == 0) {
                            value_column = txt; // const borrow
                        }
                        else {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = narg->name;
                                mutpp->errlloc = (*loc4);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("unexpected special identifier named argument `%s`",
                                          narg->name);
                        }
                        break;
                    case CMOD_ARG_VERBATIM:
                        txt = CMOD_ARG_VERBATIM_get(*narg);
                        if (NULL == narg->name) {
                            if (nargs == 3 && i != 1) {
                                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                                KEYWORD_ERROR("unexpected herestring argument at position %zu", i);
                            }
                            search_key = txt;   // const borrow
                        }
                        else if (strcmp(narg->name, KEY_NAME) == 0) {
                            search_key = txt;   // const borrow
                        }
                        else {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = narg->name;
                                mutpp->errlloc = (*loc4);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("unexpected herestring named argument `%s`", narg->name);
                        }
                        break;
                    case CMOD_ARG_INTSTR:
                        row_index_mode = true;
                        txt = CMOD_ARG_INTSTR_get(*narg);
                        if (NULL == narg->name) {
                            if (nargs != 2 || i != 0) {
                                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                                KEYWORD_ERROR("unexpected integer argument at position %zu", i);
                            }
                            search_key = txt;   // const borrow
                        }
                        else if (strcmp(narg->name, KEY_NAME) == 0) {
                            search_key = txt;   // const borrow
                        }
                        else {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = narg->name;
                                mutpp->errlloc = (*loc4);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("unexpected integer named argument `%s`", narg->name);
                        }
                        break;
                    default:
                        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                        KEYWORD_ERROR
                            ("expected special identifier, integer or herestring argument at position %zu",
                             i);
                        break;
                    }
                }
                if (NULL == search_key) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR("missing value for argument `%s`", KEY_NAME);
                }
            }
#undef SEARCH_NAME
#undef KEY_NAME
#undef VALUE_NAME
        }

        if (row_index_mode && sv_len((*x3)) > 1) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("must provide a single table when getting by row index");
        }

        setix irow = { 0 };
        setix icol = { 0 };
        const struct table *tab = NULL;
        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _25 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                const struct table *tab_tmp = NULL;
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _25)));
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tab_tmp = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tab_tmp)) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`",
                                (*(const char **)sv_at((*x3), _25)));
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation",
                             (*(const char **)sv_at((*x3), _25)));
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        table_get_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = (*(const char **)sv_at((*x3), _25));
                            mutpp->errlloc = (*loc3);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match((*(const char **)sv_at((*x3), _25)), pp->sv_tab,
                                                sv_len(pp->sv_tab), strget_array_tab, 0.75,
                                                &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?",
                                              (*(const char **)sv_at((*x3), _25)), fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`",
                                              (*(const char **)sv_at((*x3), _25)));
                            }
                        }
                    }
                }
                if (sv_len(tab_tmp->svv_rows) == 0) {
                    WARN("ignoring empty table `%s`", (*(const char **)sv_at((*x3), _25)));
                    continue;;
                }

                setix irow_tmp = { 0 };
                if (!row_index_mode) {
                    setix search_ix = { 0 };
                    if (NULL == search_column) {
                        if (!tab_tmp->key.set && TABLE_NCOLS(*tab_tmp) > 1) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = tab_tmp->name;
                                mutpp->errlloc = (*loc3);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR
                                ("table `%s` has no key and more than one column, please provide a search column",
                                 tab_tmp->name);
                        }
                        search_ix = SETIX_set(tab_tmp->key.set ? tab_tmp->key.ix : 0);  // only column or key
                    }
                    else {
                        {
                            const srt_string *_ss = ss_crefs(search_column);
                            if (NULL == (tab_tmp)->shm_colnam) {
                            }
                            search_ix.set = shm_atp_su((tab_tmp)->shm_colnam, _ss, &(search_ix).ix);
                        }

                        if (search_ix.set) ;    // all good
                        else if (opts.opt_bool) {
                            VERBOSE("ignoring unknown column `%s` in table `%s`", search_column,
                                    tab_tmp->name);
                        }
                        else if (opts.lazy_bool) {
                            WARN("unknown column `%s` in table `%s`, delaying evaluation",
                                 search_column, tab_tmp->name);
                            if (opts.str_isset)
                                kwbuf = kwbuf_save;
                            table_get_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                            BUFPUTS(pp->kwname);
                            BUFCAT(str_repr);
                            goto epilogue;
                        }
                        else {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = tab_tmp->name;
                                mutpp->errlloc = (*loc3);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("unknown column `%s` in table `%s`", search_column,
                                          tab_tmp->name);
                        }
                    }
                    {
                        const char *search_column = TABLE_COLNAME(tab_tmp, search_ix.ix);
                        int ierr = 0;
                        char *err = NULL;
                        switch (tab_find_icol_irow
                                (tab_tmp, search_key, search_ix.ix, &(irow_tmp).ix, &opts, &ierr,
                                 &err)) {
                        case 0:
                            (irow_tmp).set = true;
                            break;
                        case 1:
                            if (opts.regex_bool) {
                                VERBOSE("regex `%s` did not match column `%s` of table `%s`",
                                        search_key, search_column, tab_tmp->name);
                            }
                            else {
                                VERBOSE("string `%s` not found in column `%s` of table `%s`",
                                        search_key, search_column, tab_tmp->name);
                            }
                            break;
                        case 2:
                            ERROR("failed compiling regex `%s`: %s (%d)", search_key, err, ierr);
                            {
                                free(err);
                                (err) = NULL;
                            }   // avoid leaking err
                            YYABORT;
                            break;
                        }
                    }

                    if (!irow_tmp.set)
                        continue;       // not in this table
                }
                else {
                    int ret = strtou64_check(search_key, &irow_tmp.ix);
                    (void)ret;
                    assert(ret == 0);   // should always work, as the integer was parsed from a string
                    if (irow_tmp.ix >= TABLE_NROWS(*tab_tmp)) {
                        KEYWORD_ERROR("row index %" PRIu64
                                      " out of bounds in table `%s` with %zu rows", irow_tmp.ix,
                                      tab_tmp->name, TABLE_NROWS(*tab_tmp));
                    }
                    irow_tmp.set = true;
                }

                if (NULL == value_column) {
                    irow = irow_tmp;
                    if (row_index_mode) {
                        if (!tab_tmp->key.set && TABLE_NCOLS(*tab_tmp) > 1) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = tab_tmp->name;
                                mutpp->errlloc = (*loc3);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR
                                ("table `%s` has no key and more than one column, please provide a value column",
                                 tab_tmp->name);
                        }
                        icol = SETIX_set(tab_tmp->key.set ? tab_tmp->key.ix : 0);       // only column or key
                        tab = tab_tmp;  // only table
                    }
                    break;      // first match => done
                }
                else {
                    setix icol_tmp = { 0 };
                    {
                        const srt_string *_ss = ss_crefs(value_column);
                        if (NULL == (tab_tmp)->shm_colnam) {
                        }
                        icol_tmp.set = shm_atp_su((tab_tmp)->shm_colnam, _ss, &(icol_tmp).ix);
                    }

                    if (icol_tmp.set) { // found!
                        tab = tab_tmp;
                        irow = irow_tmp;
                        icol = icol_tmp;
                        break;  // first match => done
                    }
                    else if (opts.opt_bool) {
                        VERBOSE("ignoring unknown column `%s` in table `%s`",
                                value_column, tab_tmp->name);
                        continue;
                    }
                    else if (opts.lazy_bool) {
                        WARN("unknown column `%s` in table `%s`, delaying evaluation", value_column,
                             tab_tmp->name);
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        table_get_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        goto epilogue;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = tab_tmp->name;
                            mutpp->errlloc = (*loc3);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unknown column `%s` in table `%s`", value_column,
                                      tab_tmp->name);
                    }
                }
            }
        }

        if (NULL != tab && irow.set) {
            BUFPUTS(table_getcell(tab, irow.ix, icol.ix));      // get value from table
        }
        else if (irow.set) {
            BUFPUTINT(irow.ix); // print row index only
        }
        else if (opts.strict_bool) {
            value_column = (NULL == value_column) ? "ROW_NUMBER()" : value_column;
            search_column = (NULL == search_column) ? "PRIMARY_INDEX" : search_column;
            KEYWORD_ERROR("no table matches query SELECT %s WHERE %s = '%s'",
                          value_column, search_column, search_key);
        }
        else
            BUFPUTS((NULL == value_column) ? "-1" : "");

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_length_rule0_str(const struct param *const pp,
                                  size_t debug_level,
                                  const struct table_length_cmod_opts *const opts,
                                  vec_string **x3, vec_string **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_length_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, "`", *(&x), "`");
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x4); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x4, i);
        ss_cat_c(s, "#", x);
    }
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_table_length_action_0(void *yyscanner,
                               const struct param *pp,
                               vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                               vec_string **x3, const YYLTYPE *loc3,
                               vec_string **x4, const YYLTYPE *loc4,
                               bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct table_length_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_length_opt_names) / sizeof(*(table_length_opt_names)));
        const char **opt_names = table_length_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "dd1", 4) == 0) {
            ret = option_list_action_count(&opts.add1_count, opt, &opts.add1_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('m' == *((opt->name) + 0)) {
            if ('a' == *((opt->name) + 1) && 'x' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.max_bool, opt, &opts.max_isset);

            }
            else if ('i' == *((opt->name) + 1) && 'n' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.min_bool, opt, &opts.min_isset);

            }
            else {
                goto cmod_strin_else_25;
            }
        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else {
            goto cmod_strin_else_25;
 cmod_strin_else_25:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (table_length_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_length_opt_names[icuropt], table_length_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_length_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t maxlen = 0;
        size_t minlen = SIZE_MAX;

        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _26 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct table *tab = NULL;
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _26)));
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tab = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tab)) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`",
                                (*(const char **)sv_at((*x3), _26)));
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation",
                             (*(const char **)sv_at((*x3), _26)));
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        table_length_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = (*(const char **)sv_at((*x3), _26));
                            mutpp->errlloc = (*loc3);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match((*(const char **)sv_at((*x3), _26)), pp->sv_tab,
                                                sv_len(pp->sv_tab), strget_array_tab, 0.75,
                                                &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?",
                                              (*(const char **)sv_at((*x3), _26)), fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`",
                                              (*(const char **)sv_at((*x3), _26)));
                            }
                        }
                    }
                }
                if (sv_len(tab->svv_rows) == 0) {
                    WARN("ignoring empty table `%s`", (*(const char **)sv_at((*x3), _26)));
                    continue;;
                }

                {
                    const size_t _len = sv_len((*x4));
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _56 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        setix found = { 0 };
                        {
                            const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x4), _56)));
                            if (NULL == (tab)->shm_colnam) {
                            }
                            found.set = shm_atp_su((tab)->shm_colnam, _ss, &(found).ix);
                        }

                        if (!found.set) {
                            if (opts.opt_bool) {
                                VERBOSE("ignoring unknown column `%s` in table `%s`",
                                        (*(const char **)sv_at((*x4), _56)),
                                        (*(const char **)sv_at((*x3), _26)));
                                continue;
                            }
                            else if (opts.lazy_bool) {
                                WARN("unknown column `%s` in table `%s`, delaying evaluation",
                                     (*(const char **)sv_at((*x4), _56)),
                                     (*(const char **)sv_at((*x3), _26)));
                                if (opts.str_isset)
                                    kwbuf = kwbuf_save;
                                table_length_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                                BUFPUTS(pp->kwname);
                                BUFCAT(str_repr);
                                goto epilogue;
                            }
                            else {
                                {
                                    struct param *mutpp = yyget_extra(yyscanner);
                                    mutpp->errtxt = (*(const char **)sv_at((*x3), _26));
                                    mutpp->errlloc = (*loc3);
                                    const char *errline = ss_to_c(pp->line);
                                    if (NULL != pp->errtxt && NULL != errline) {
                                        bool found =
                                            errline_find_name(errline, pp->errtxt,
                                                              mutpp->errlloc.first_line,
                                                              &(mutpp->errlloc));
                                        if (found) {
                                        }
                                    }
                                }
                                KEYWORD_ERROR("unknown column `%s` in table `%s`",
                                              (*(const char **)sv_at((*x4), _56)),
                                              (*(const char **)sv_at((*x3), _26)));
                            }
                        }

                        if (found.ix >= tab->nnondef) { // default column
                            size_t len = strlen(table_getcell(tab, 0, found.ix));
                            (minlen) = ((len) < (minlen)) ? (len) : (minlen);

                            (maxlen) = ((len) > (maxlen)) ? (len) : (maxlen);

                        }
                        else
                            for (size_t i = 0; i < TABLE_NROWS(*tab); ++i) {
                                size_t len = strlen(table_getcell(tab, i, found.ix));
                                (minlen) = ((len) < (minlen)) ? (len) : (minlen);

                                (maxlen) = ((len) > (maxlen)) ? (len) : (maxlen);

                            }
                    }
                }
            }
        }

        BUFPUTINT((opts.min_bool ? minlen : maxlen) + opts.add1_count);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_size_rule0_str(const struct param *const pp,
                                size_t debug_level,
                                const struct table_size_cmod_opts *const opts,
                                vec_string **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_size_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_table_size_action_0(void *yyscanner,
                             const struct param *pp,
                             vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                             vec_string **x3, const YYLTYPE *loc3,
                             bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct table_size_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_size_opt_names) / sizeof(*(table_size_opt_names)));
        const char **opt_names = table_size_opt_names;  // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "dd1", 4) == 0) {
            ret = option_list_action_count(&opts.add1_count, opt, &opts.add1_isset);

        }
        else if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ols", 4) == 0) {
            ret = option_list_action_bool(&opts.cols_bool, opt, &opts.cols_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ows", 4) == 0) {
            ret = option_list_action_bool(&opts.rows_bool, opt, &opts.rows_isset);

        }
        else if ('s' == *((opt->name) + 0) && 't' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_str(&opts.str_str, opt, &opts.str_isset);

        }
        else {
            goto cmod_strin_else_26;
 cmod_strin_else_26:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (table_size_opt_compat(&opts, &icuropt, &ibadopt)) {     // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_size_opt_names[icuropt], table_size_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_size_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    struct str_mod kwmod = { 0 };
    srt_string **kwbuf_save = NULL;
    srt_string *newbuf = NULL;
    if (opts.str_isset && '\0' != ss_at(opts.str_str, 0)) {
        char err;
        int ret = parse_str_mod(ss_to_c(opts.str_str), &kwmod, &err);
        if (ret)
            KEYWORD_ERROR("invalid string modifier '%c'", err);
        kwbuf_save = kwbuf;     // save buffer
        (newbuf) = ss_alloc(256);
        if (ss_void == (newbuf)) {
            YYNOMEM;
        }

        kwbuf = &newbuf;        // swap buffer
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t nrow = 0;
        size_t ncol = 0;

        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _27 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct table *tab = NULL;
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs((*(const char **)sv_at((*x3), _27)));
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tab = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tab)) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`",
                                (*(const char **)sv_at((*x3), _27)));
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation",
                             (*(const char **)sv_at((*x3), _27)));
                        if (opts.str_isset)
                            kwbuf = kwbuf_save;
                        table_size_rule0_str(pp, 3, &opts, x3, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = (*(const char **)sv_at((*x3), _27));
                            mutpp->errlloc = (*loc3);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match((*(const char **)sv_at((*x3), _27)), pp->sv_tab,
                                                sv_len(pp->sv_tab), strget_array_tab, 0.75,
                                                &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?",
                                              (*(const char **)sv_at((*x3), _27)), fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`",
                                              (*(const char **)sv_at((*x3), _27)));
                            }
                        }
                    }
                }
                if (sv_len(tab->svv_rows) == 0) {
                    WARN("ignoring empty table `%s`", (*(const char **)sv_at((*x3), _27)));
                    continue;;
                }

                nrow += TABLE_NROWS(*tab);
                ncol += TABLE_NCOLS(*tab);
            }
        }

        BUFPUTINT((opts.cols_bool ? ncol : nrow) + opts.add1_count);

        if (opts.str_isset) {
            kwbuf = kwbuf_save; // restore buffer
            ss_cat_mod(kwbuf, newbuf, NULL, kwmod);     // append to buffer
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

        ss_free(&newbuf);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.str_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int table_stack_rule0_str(const struct param *const pp,
                                 size_t debug_level,
                                 const struct table_stack_cmod_opts *const opts,
                                 char **x3, vec_string **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    table_stack_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x4); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x4, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    return 0;
}

int cmod_table_stack_action_0(void *yyscanner,
                              struct param *pp,
                              vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                              char **x3, const YYLTYPE *loc3,
                              vec_string **x4, const YYLTYPE *loc4,
                              bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct table_stack_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(table_stack_opt_names) / sizeof(*(table_stack_opt_names)));
        const char **opt_names = table_stack_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('h' == *((opt->name) + 0)) {
            if ('\0' == *((opt->name) + 1)) {
                ret = option_list_action_bool(&opts.h_bool, opt, &opts.h_isset);

            }
            else if ('a' == *((opt->name) + 1) && 's' == *((opt->name) + 2)
                     && 'h' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_column(&opts.hash_column, opt, &opts.hash_isset);

            }
            else {
                goto cmod_strin_else_27;
            }
        }
        else if ('l' == *((opt->name) + 0) && strncmp((opt->name) + 1, "azy", 4) == 0) {
            ret = option_list_action_bool(&opts.lazy_bool, opt, &opts.lazy_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('r' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1)) {
                if ('d' == *((opt->name) + 2) && 'e' == *((opt->name) + 3)
                    && 'f' == *((opt->name) + 4) && '\0' == *((opt->name) + 5)) {
                    ret = option_list_action_bool(&opts.redef_bool, opt, &opts.redef_isset);

                }
                else if ('n' == *((opt->name) + 2) && strncmp((opt->name) + 3, "ame", 4) == 0) {
                    ret = option_list_action_bool(&opts.rename_bool, opt, &opts.rename_isset);

                }
                else {
                    goto cmod_strin_else_27;
                }
            }
            else if ('o' == *((opt->name) + 1) && 'w' == *((opt->name) + 2)
                     && 's' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.rows_bool, opt, &opts.rows_isset);

            }
            else {
                goto cmod_strin_else_27;
            }
        }
        else if ('v' == *((opt->name) + 0) && '\0' == *((opt->name) + 1)) {
            ret = option_list_action_bool(&opts.v_bool, opt, &opts.v_isset);

        }
        else if ('c' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('l' == *((opt->name) + 2) && 's' == *((opt->name) + 3)
                && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_bool(&opts.cols_bool, opt, &opts.cols_isset);

            }
            else if ('m' == *((opt->name) + 2) && strncmp((opt->name) + 3, "mon", 4) == 0) {
                ret = option_list_action_bool(&opts.common_bool, opt, &opts.common_isset);

            }
            else {
                goto cmod_strin_else_27;
            }
        }
        else {
            goto cmod_strin_else_27;
 cmod_strin_else_27:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    opts.rows_isset = opts.rows_bool = opts.rows_bool || opts.v_bool;
    opts.cols_isset = opts.cols_bool = opts.cols_bool || opts.h_bool;
    if (!opts.rows_bool && !opts.cols_bool)
        opts.rows_isset = opts.rows_bool = true;        // default

    int icuropt = -1, ibadopt = -1;
    if (table_stack_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      table_stack_opt_names[icuropt], table_stack_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        table_stack_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action
        vec_table_ptr *tables = NULL;

        if (inactive)
            goto epilogue;

        /* check target table */
        struct table *oldtab = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs((*x3));
                if (NULL == pp->shm_tab) {
                }
                _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
            }

            if (_found.set)
                oldtab = (void *)sv_at(pp->sv_tab, _found.ix);
        }
        if (NULL != (oldtab)) {
            if (!(false) && opts.opt_bool) {
                VERBOSE("ignoring already defined table `%s`", (*x3));

                goto epilogue;
            }
            else if (!(oldtab)->redef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("table `%s` is not redefinable", (*x3));
            }
        }

        (tables) = sv_alloc_t(SV_PTR, sv_len((*x4)));
        if (sv_void == (tables)) {
            YYNOMEM;
        }

        /* check and collect input tables */
        {
            const size_t _len = sv_len((*x4));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _28 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct table *tab = NULL;
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs((*(char **)sv_at((*x4), _28)));
                        if (NULL == pp->shm_tab) {
                        }
                        _found.set = shm_atp_su(pp->shm_tab, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        tab = (void *)sv_at(pp->sv_tab, _found.ix);
                }
                if (NULL == (tab)) {
                    if (opts.opt_bool) {        // ignore if optional
                        VERBOSE("ignoring undefined table `%s`", (*(char **)sv_at((*x4), _28)));
                        continue;;
                    }
                    else if (opts.lazy_bool) {
                        WARN("undefined table `%s`, delaying evaluation",
                             (*(char **)sv_at((*x4), _28)));
                        table_stack_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                        BUFPUTS(pp->kwname);
                        BUFCAT(str_repr);
                        BUFPUTC('\n');
                        goto epilogue;;
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = (*(char **)sv_at((*x4), _28));
                            mutpp->errlloc = (*loc4);
                            const char *fuzzy_match;
                            bool is_close;
                            is_close =
                                str_fuzzy_match((*(char **)sv_at((*x4), _28)), pp->sv_tab,
                                                sv_len(pp->sv_tab), strget_array_tab, 0.75,
                                                &(fuzzy_match));

                            if (NULL != fuzzy_match && is_close) {
                                KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?",
                                              (*(char **)sv_at((*x4), _28)), fuzzy_match);
                            }
                            else {
                                KEYWORD_ERROR("unknown table `%s`", (*(char **)sv_at((*x4), _28)));
                            }
                        }
                    }
                }

                if (!sv_push(&(tables), &(tab))) {
                    YYNOMEM;
                }

            }
        }

        const size_t ntables = sv_len(tables);
#if DEBUG
        debug("have %zu tables", ntables);
#endif
        if (ntables == 0) {
            WARN0("all tables undefined, doing nothing");
            goto epilogue;
        }

        vec_namarg *sv_colnarg = NULL;  // columns of new table
        vec_vec_string *sv_allrows = NULL;      // rows of new table

        size_t imintab = 0;     // index in table list
        const struct table *mintab = sv_at_ptr(tables, 0);      // defaults to first table
        /*   [common]: find table with fewest columns [rows], or fewest rows [cols]
           otherwise : check all tables have same number of columns [rows] or rows [cols] */
        {
            size_t ncolmin = TABLE_NCOLS(*mintab);
            size_t nrowmin = TABLE_NROWS(*mintab);

            for (size_t i = 1; i < ntables; ++i) {
                const struct table *const tab = sv_at_ptr(tables, i);
                size_t ncol = TABLE_NCOLS(*tab);
                size_t nrow = TABLE_NROWS(*tab);

                if (opts.rows_bool) {   // stack rows
                    if (!opts.common_bool && ncol != ncolmin) { // check number of columns
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = tab->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("expected %zu (got %zu) columns in table `%s`",
                                      ncolmin, ncol, tab->name);
                    }
                    else if (ncol < ncolmin) {  // update table with least columns
                        imintab = i;
                        mintab = tab;
                        ncolmin = ncol;
                    }
                }
                else {  // stack columns
                    if (!opts.common_bool && nrow != nrowmin) { // check number of rows
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = tab->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("expected %zu (got %zu) rows in table `%s`",
                                      nrowmin, nrow, tab->name);
                    }
                    else if (nrow < nrowmin) {  // update table with least rows
                        imintab = i;
                        mintab = tab;
                        nrowmin = nrow;
                    }
                }
            }
#if DEBUG
            if (opts.rows_bool)
                debug("minimal table `%s` has %zu columns", mintab->name, ncolmin);
            else
                debug("minimal table `%s` has %zu rows", mintab->name, nrowmin);
#endif
        }

        /* get column names from minimal table */
        srt_set *mincolnames = NULL;
        (mincolnames) = sms_alloc(SMS_S, TABLE_NCOLS(*mintab));
        if (NULL == (mincolnames)) {
            YYNOMEM;
        }

        {
            const size_t _len = sv_len(mintab->sv_colnarg);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _29 = _idx;
                const size_t _idx = 0;
                (void)_idx;
#if DEBUG
                debug("minimal column `%s`",
                      ((struct namarg *)sv_at(mintab->sv_colnarg, _29))->name);
#endif
                if (!sms_insert_s
                    (&(mincolnames),
                     ss_crefs(((struct namarg *)sv_at(mintab->sv_colnarg, _29))->name))) {
                    YYNOMEM;
                };

            }
        }

        size_t nrowtot = TABLE_NROWS(*mintab);
        /* check tables */
        for (size_t i = 0, ncol; i < ntables; ++i) {
            if (i == imintab)
                continue;       // skip minimal table
            const struct table *const tab = sv_at_ptr(tables, i);

            if (opts.rows_bool) {       // update minimal column set
                nrowtot += TABLE_NROWS(*tab);
                if ((ncol = sms_len(mincolnames)) > 0) {
                    /* vector with columns that are not minimal */
                    vec_ss *(delcols) = sv_alloc_t(SV_PTR, ncol);
                    if (sv_void == (delcols)) {
                        YYNOMEM;
                    }

                    for (size_t c = 0; c < ncol; ++c) {
                        const srt_string *min_colnam = sms_it_s(mincolnames, c);
                        uint64_t ix;
                        /* minimal column not present in table */
                        if (!shm_atp_su(tab->shm_colnam, min_colnam, &ix)) {
                            if (opts.common_bool) {     // collect for removal
                                if (!sv_push(&(delcols), &(min_colnam))) {
                                    YYNOMEM;
                                }
                                // borrow
                            }
                            else {      // ERROR: not a common column
                                {
                                    struct param *mutpp = yyget_extra(yyscanner);
                                    mutpp->errtxt = tab->name;
                                    mutpp->errlloc = (*loc4);
                                    const char *errline = ss_to_c(pp->line);
                                    if (NULL != pp->errtxt && NULL != errline) {
                                        bool found =
                                            errline_find_name(errline, pp->errtxt,
                                                              mutpp->errlloc.first_line,
                                                              &(mutpp->errlloc));
                                        if (found) {
                                        }
                                    }
                                }
                                KEYWORD_ERROR("column [table `%s`]#%s is not common to all tables",
                                              tab->name, ss_to_c(min_colnam));
                            }
                        }
                    }
                    /* remove not found columns from minimal set */
                    {
                        const size_t _len = sv_len(delcols);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _30 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            sms_delete_s(mincolnames, (*(const srt_string **)sv_at(delcols, _30)));
                        }
                    }
                    sv_free(&delcols);  // unborrow
                }
            }
            else {      // get maximal unique column set
                {
                    const size_t _len = sv_len(tab->sv_colnarg);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _31 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        const srt_string *ss =
                            ss_crefs(((const struct namarg *)sv_at(tab->sv_colnarg, _31))->name);
                        if (sms_count_s(mincolnames, ss)) {     // already in minimal set
                            if (opts.rename_bool) {     // rename column
                                srt_string *(tmp) = ss_alloc(4);
                                if (ss_void == (tmp)) {
                                    YYNOMEM;
                                }
                                ;
                                ss_cat_cn1(&tmp, 'c');  // 'c' for column
                                ss_cat_int(&tmp, sms_len(mincolnames)); // <column index>
                                ss_cat_c(&tmp, "_", ((const struct namarg *)sv_at(tab->sv_colnarg, _31))->name);        // _<original name>
                                WARN("renamed column [table `%s`]#%s -> [table `%s`]#%s",
                                     tab->name,
                                     ((const struct namarg *)sv_at(tab->sv_colnarg, _31))->name,
                                     (*x3), ss_to_c(tmp));
                                sms_insert_s(&mincolnames, tmp);        // add to set
                                ss_free(&tmp);
                            }
                            else {      // ERROR: duplicate column
                                {
                                    struct param *mutpp = yyget_extra(yyscanner);
                                    mutpp->errtxt = tab->name;
                                    mutpp->errlloc = (*loc4);
                                    const char *errline = ss_to_c(pp->line);
                                    if (NULL != pp->errtxt && NULL != errline) {
                                        bool found =
                                            errline_find_name(errline, pp->errtxt,
                                                              mutpp->errlloc.first_line,
                                                              &(mutpp->errlloc));
                                        if (found) {
                                        }
                                    }
                                }
                                KEYWORD_ERROR("duplicate column [table `%s`]#%s",
                                              tab->name,
                                              ((const struct namarg *)sv_at(tab->sv_colnarg, _31))->
                                              name);
                            }
                        }
                        else
                            sms_insert_s(&mincolnames, ss);     // add to minimal set
                    }
                }
            }
        }
        size_t ncoltot = sms_len(mincolnames);  // number of minimal column names

        /* empty set of minimal columns / rows */
        if (ncoltot == 0 || nrowtot == 0)
            goto empty;

        /* init output columns (arbitrary order) */
        (sv_colnarg) = sv_alloc(sizeof(struct namarg), ncoltot, NULL);
        if (sv_void == (sv_colnarg)) {
            YYNOMEM;
        }

        for (size_t i = 0; i < ncoltot; ++i) {
            const srt_string *min_colnam = sms_it_s(mincolnames, i);
            const char *c_colnam = ss_to_c(min_colnam);
#if DEBUG
            debug("common column: `%s`", c_colnam);
#endif
            char *colnam = (NULL != (c_colnam)) ? rstrdup(c_colnam) : NULL;
            if (NULL != (c_colnam) && NULL == (colnam)) {
                YYNOMEM;
            }

            struct namarg item = CMOD_ARG_NOVALUE_xset(.name = colnam);
            if (!sv_push(&(sv_colnarg), &(item))) {
                YYNOMEM;
            }
            ;
        }

        /* init output rows */
#if DEBUG
        debug("have %zu rows in total", nrowtot);
#endif
        (sv_allrows) = sv_alloc_t(SV_PTR, nrowtot);
        if (sv_void == (sv_allrows)) {
            YYNOMEM;
        }

        for (size_t i = 0; i < nrowtot; ++i) {
            vec_string *(row) = sv_alloc_t(SV_PTR, ncoltot);
            if (sv_void == (row)) {
                YYNOMEM;
            }
            ;   // full-size empty row
            if (!sv_push(&(sv_allrows), &(row))) {
                YYNOMEM;
            }

        }

        /* stack tables */
        for (size_t i = 0, nrow = 0; i < ntables; ++i) {
            const struct table *tab = sv_at_ptr(tables, i);

            if (opts.rows_bool) {       // stack rows
                /* stack some (or all) columns for all rows */
                {
                    const size_t _len = sv_len(sv_colnarg);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _32 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
// iterate in order
                        setix found = { 0 }; {
                            const srt_string *_ss =
                                ss_crefs(((const struct namarg *)sv_at(sv_colnarg, _32))->name);
                            if (NULL == (tab)->shm_colnam) {
                            }
                            found.set = shm_atp_su((tab)->shm_colnam, _ss, &(found).ix);
                        }

                        if (!found.set)
                            assert(0 && "C% internal error" ": " "column must exist");

                        for (size_t r = 0; r < TABLE_NROWS(*tab); ++r) {        // add value to column (as if)
                            char *colval = (char *)table_getcell(tab, r, found.ix);
                            colval = (NULL != (colval)) ? rstrdup(colval) : NULL;
                            if (NULL != (colval) && NULL == (colval)) {
                                YYNOMEM;
                            }

                            /* NOTE: must use indirection, as pointer may be realloc'd after push */
                            vec_string **newrow = (vec_string **) sv_at(sv_allrows, nrow + r);
                            if (!sv_push(&(*newrow), &(colval))) {
                                YYNOMEM;
                            }
                        }
                    }
                }
                nrow += TABLE_NROWS(*tab);      // increase row count
            }
            else {      // stack columns
                /* stack all columns for some (or all) rows */
                {
                    const size_t _len = sv_len(tab->sv_colnarg);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _33 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        for (size_t r = 0; r < sv_len(sv_allrows); ++r) {       // extend rows with new column
                            char *colval = (char *)table_getcell(tab, r, _33);
                            colval = (NULL != (colval)) ? rstrdup(colval) : NULL;
                            if (NULL != (colval) && NULL == (colval)) {
                                YYNOMEM;
                            }

                            /* NOTE: must use indirection, as pointer may be realloc'd after push */
                            vec_string **oldrow = (vec_string **) sv_at(sv_allrows, r);
                            if (!sv_push(&(*oldrow), &(colval))) {
                                YYNOMEM;
                            }
                        }
                    }
                }
            }
        }

 empty:
        sms_free(&mincolnames);

        if (sv_len(sv_colnarg) == 0 || sv_len(sv_allrows) == 0) {
            WARN("empty result, not creating table `%s`", (*x3));
            goto epilogue;
        }

        hmap_su *shm_colnam = NULL;
        if (NULL == shm_colnam) {
            (shm_colnam) = shm_alloc(SHM_SU, sv_len(sv_colnarg));
            if (NULL == (shm_colnam)) {
                YYNOMEM;
            }
            {
                const size_t _len = sv_len(sv_colnarg);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _57 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    {
                        const srt_string *_ss =
                            ss_crefs(((const struct namarg *)sv_at(sv_colnarg, _57))->name);
                        if (!shm_insert_su(&(shm_colnam), _ss, _57)) {
                            YYNOMEM;
                        }
                    }
                }
            }
        }

        // NOTE: tab.nnondef defaults to sv_len(sv_colnarg), so all columns are non-default
        struct table tab = {
            .name = (*x3),.sv_colnarg = sv_colnarg,.svv_rows = sv_allrows,
            .redef = opts.redef_bool,.key = SETIX_none,.shm_colnam = shm_colnam,
            .nnondef = sv_len(sv_colnarg)
        };
        (*x3) = NULL;   // hand-over

        if (NULL != opts.hash_column) {
            size_t hash_index = 0;
            if (tab_icol_from_opt(&(tab), opts.hash_column, &hash_index)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                switch ((opts.hash_column)->type) {
                case ENUM_CMOD_OPTION(XINT):
                    KEYWORD_ERROR("[hash] bad column index %" PRIuMAX " for table `%s`",
                                  XINT_UNSIGNED_get(CMOD_OPTION_XINT_get(*(opts.hash_column))),
                                  (tab).name);
                    break;
                case ENUM_CMOD_OPTION(STRING):
                    KEYWORD_ERROR("[hash] column `%s` not present in table `%s`",
                                  ss_to_c(CMOD_OPTION_STRING_get(*(opts.hash_column))), (tab).name);
                    break;
                default:       // already checked in parser
                    assert(0 && "C% internal error" "");
                    break;
                }
            }
            (tab).key = SETIX_set(hash_index);
        }
        if ((tab).key.set && NULL == (tab).hmap) {
            size_t nrow = sv_len((tab).svv_rows);
            ((tab).hmap) = shm_alloc(SHM_SU, nrow);
            if (NULL == ((tab).hmap)) {
                YYNOMEM;
            }
            for (size_t i = 0; i < nrow; ++i) {
                const char *key = table_getcell(&(tab), i, (tab).key.ix);
                const uint64_t value = i;
                {
                    const srt_string *_ss = ss_crefs(key);
                    if (!shm_insert_su(&((tab).hmap), _ss, value)) {
                        YYNOMEM;
                    }
                }
            }
            if (shm_size((tab).hmap) != nrow) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("non-unique values in hash key column `%s` of table `%s`",
                              TABLE_COLNAME(&tab, (tab).key.ix), (tab).name);
            }
        }
        if (NULL == oldtab) {
            {
                const srt_string *_ss = ss_crefs((tab).name);
                if (!sv_push(&(pp->sv_tab), &(tab))
                    || !shm_insert_su(&(pp->shm_tab), _ss, sv_len(pp->sv_tab) - 1)) {
                    YYNOMEM;
                }
            }
        }
        else {  // already exists
            if (!(pp->redef_nowarn))
                WARN("redefining table `%s`", (tab).name);
            table_clear(oldtab);
            *(oldtab) = tab;
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op
        sv_free(&tables);

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int unittest_rule0_str(const struct param *const pp,
                              size_t debug_level,
                              const struct unittest_cmod_opts *const opts,
                              char **x3, char **x4, srt_string **x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    unittest_opt_string(opts, s);
    (void)x3;

    if (NULL != *x3) {
        ss_cat_cn1(s, ' ');
        ss_cat_c(s, *x3);
    }

    (void)x4;

    if (NULL != *x4) {
        ss_cat_cn1(s, ' ');
        hstr_str_repr(*x4, s);
    }

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{\n");
    if (debug_level >= 2) {
        const struct snippet tmp = {
            .body = *x5,        // const borrow
            .sv_argl = pp->sv_dllarg    // const borrow
        };
        snippet_body_str_repr(&tmp, tmp.sv_argl, s);
    }
    else
        ss_cat_c(s, "<snippet body>");
    ss_cat_c(s, "%" "}");

    return 0;
}

int cmod_unittest_action_0(void *yyscanner,
                           const struct param *pp,
                           vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                           char **x3, const YYLTYPE *loc3,
                           char **x4, const YYLTYPE *loc4,
                           srt_string **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct unittest_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(unittest_opt_names) / sizeof(*(unittest_opt_names)));
        const char **opt_names = unittest_opt_names;    // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0)) {
            if ('e' == *((opt->name) + 1)) {
                if ('b' == *((opt->name) + 2) && 'u' == *((opt->name) + 3)
                    && 'g' == *((opt->name) + 4) && '\0' == *((opt->name) + 5)) {
                    ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

                }
                else if ('i' == *((opt->name) + 2) && strncmp((opt->name) + 3, "nit", 4) == 0) {
                    ret = option_list_action_cid(&opts.deinit_cid, opt, &opts.deinit_isset);

                }
                else {
                    goto cmod_strin_else_28;
                }
            }
            else if ('i' == *((opt->name) + 1) && strncmp((opt->name) + 2, "sabled", 7) == 0) {
                ret = option_list_action_bool(&opts.disabled_bool, opt, &opts.disabled_isset);

            }
            else {
                goto cmod_strin_else_28;
            }
        }
        else if ('i' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nit", 4) == 0) {
            ret = option_list_action_cid(&opts.init_cid, opt, &opts.init_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('i' == *((opt->name) + 1) && strncmp((opt->name) + 2, "gnal", 5) == 0) {
                ret = option_list_action_cid(&opts.signal_cid, opt, &opts.signal_isset);

            }
            else if ('t' == *((opt->name) + 1) && strncmp((opt->name) + 2, "atus", 5) == 0) {
                ret = option_list_action_int(&opts.status_int, opt, &opts.status_isset);

            }
            else if ('u' == *((opt->name) + 1) && strncmp((opt->name) + 2, "ite", 4) == 0) {
                ret = option_list_action_cid(&opts.suite_cid, opt, &opts.suite_isset);

            }
            else {
                goto cmod_strin_else_28;
            }
        }
        else if ('t' == *((opt->name) + 0) && strncmp((opt->name) + 1, "imeout", 7) == 0) {
            ret = option_list_action_count(&opts.timeout_count, opt, &opts.timeout_isset);

        }
        else if ('v' == *((opt->name) + 0) && strncmp((opt->name) + 1, "erbatim", 8) == 0) {
            ret = option_list_action_bool(&opts.verbatim_bool, opt, &opts.verbatim_isset);

        }
        else {
            goto cmod_strin_else_28;
 cmod_strin_else_28:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (unittest_opt_compat(&opts, &icuropt, &ibadopt)) {       // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      unittest_opt_names[icuropt], unittest_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        unittest_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (NULL == pp->fpout_tests)
            WARN0("tests output file not provided");

        char buf[512];
        srt_string *testname = ss_alloc_into_ext_buf(buf, sizeof(buf));
        ss_printf(&testname, ss_max(testname), "t%zu__%s", pp->nunittest,
                  (NULL != (*x3)) ? (*x3) : "noname");

        char *test_name = (NULL != (*x3))
            ? (*x3)
            : (char *)ss_to_c(testname);        // const borrow

        /* define snippet (non-lambda) */
        struct snippet snip = { 0 };
        snip = (struct snippet) {
            .name = test_name,.body = (*x5),
            .sv_forl = NULL,.sv_argl = pp->sv_dllarg,
            .redef = false,.nout = 0
        };
        {       // check snippet
            size_t ierr = 0;
            const char *err = NULL;
            int ret = snip_def_check(&snip, false, &ierr, &err);        // is_lambda = false
            if (ret >= 2)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            switch (ret) {
            case 0:
                if (ierr == 0)
                    break;
                VERBOSE("unused arguments (%zu) in snippet `%s`", ierr, snip.name);
                break;
            case 1:
                YYNOMEM;;
                break;
            case 2:
                KEYWORD_ERROR("formal argument at position %zu has no name in snippet `%s`", ierr,
                              test_name);
                break;
            case 3:
                KEYWORD_ERROR("invalid special argument %s in snippet `%s`", err, test_name);
                break;
            case 4:
                KEYWORD_ERROR("reference to undefined formal argument `%s` in snippet `%s`", err,
                              test_name);
                break;
            case 5:
                KEYWORD_ERROR("positional reference %zu out of bounds in snippet `%s`", ierr,
                              test_name);
                break;
            case 6:
                KEYWORD_ERROR
                    ("invalid arglink to undefined argument in formal argument `%s` of snippet `%s`",
                     err, test_name);
                break;
            case 7:
                KEYWORD_ERROR("invalid arglink to self in formal argument `%s` of snippet `%s`",
                              err, test_name);
                break;
            case 8:
                KEYWORD_ERROR("arglink is too deep in formal argument `%s` of snippet `%s`", err,
                              test_name);
                break;
            }
        }

        if (!opts.verbatim_bool) {
            PRINTTEST("Test(%s, %s",
                      (ss_len(opts.suite_cid) > 0) ? ss_to_c(opts.suite_cid) : "base",
                      ss_to_c(testname));
            PRINTTEST0(opts.disabled_bool ? ",\n.disabled=true" : ",\n.disabled=false");
            if (ss_len(opts.init_cid) > 0)
                PRINTTEST(",\n.init=%s", ss_to_c(opts.init_cid));
            if (ss_len(opts.deinit_cid) > 0)
                PRINTTEST(",\n.fini=%s", ss_to_c(opts.deinit_cid));
            if (ss_len(opts.signal_cid) > 0)
                PRINTTEST(",\n.signal=%s", ss_to_c(opts.signal_cid));
            if (opts.status_isset)
                PRINTTEST(",\n.exit_code=%" PRIiMAX, opts.status_int);
            if (opts.timeout_isset)
                PRINTTEST(",\n.timeout=%zu", opts.timeout_count);
            if (NULL != (*x4))
                PRINTTEST(",\n.description=\"%s\"", (*x4));
            PRINTTEST0(") {\n");
        }
        {
            size_t buflen = 0;
            char *buf = snip_get_buf(&snip, 0, NULL, &buflen);
            PRINTTEST("%s\n", buf);
            {
                free(buf);
                (buf) = NULL;
            }
        }
        if (!opts.verbatim_bool)
            PRINTTEST0("}\n");

        snip.name = NULL;       // unborrow
        snip.body = NULL;       // unborrow
        snip.sv_argl = NULL;    // unborrow
        sv_free(&snip.sv_forl); // unborrow contents
        snippet_clear(&snip);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.suite_cid));
    ss_free(&(opts.init_cid));
    ss_free(&(opts.deinit_cid));
    ss_free(&(opts.signal_cid));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int arrlen_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct arrlen_cmod_opts *const opts,
                            char **x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    arrlen_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "(", *x3, ")");

    return 0;
}

int cmod_arrlen_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         char **x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct arrlen_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(arrlen_opt_names) / sizeof(*(arrlen_opt_names)));
        const char **opt_names = arrlen_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else {
            goto cmod_strin_else_29;
 cmod_strin_else_29:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (arrlen_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      arrlen_opt_names[icuropt], arrlen_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        arrlen_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        BUFPUTS("(sizeof(", (*x3), ") / sizeof(*(", (*x3), ")))");

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int def_rule0_str(const struct param *const pp,
                         size_t debug_level,
                         const struct def_cmod_opts *const opts,
                         char **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    def_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_def_action_0(void *yyscanner,
                      struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      char **x3, const YYLTYPE *loc3,
                      const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct def_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(def_opt_names) / sizeof(*(def_opt_names)));
        const char **opt_names = def_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eturn", 6) == 0) {
            ret = option_list_action_str(&opts.return_str, opt, &opts.return_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('c' == *((opt->name) + 2) && strncmp((opt->name) + 3, "heck", 5) == 0) {
                ret = option_list_action_bool(&opts.nocheck_bool, opt, &opts.nocheck_isset);

            }
            else if ('w' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_30;
            }
        }
        else {
            goto cmod_strin_else_30;
 cmod_strin_else_30:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (def_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      def_opt_names[icuropt], def_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        def_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (prepend_cmod_prefix(pp->sm_prefix_us, &((*x3)), '_')) {
            YYNOMEM;
        }

        {       // check existing
            const struct fdef *fun = NULL;
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs((*x3));
                    if (NULL == pp->shm_fdef) {
                    }
                    _found.set = shm_atp_su(pp->shm_fdef, _ss, &(_found).ix);
                }

                if (_found.set)
                    fun = (void *)sv_at(pp->sv_fdef, _found.ix);
            }

            if (NULL != fun) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("cannot redefine function `%s`", (*x3));
            }
        }

        setix found = { 0 }; {
            const srt_string *_ss = ss_crefs((*x3));
            found.set = shm_atp_su(pp->shm_dproto, _ss, &(found).ix);
        }

        if (!found.set) {       // proto not found
            if (opts.now_bool) {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x3);
                    mutpp->errlloc = (*loc3);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x3), pp->sv_dproto, sv_len(pp->sv_dproto),
                                        strget_array_decl, 0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown prototype `%s`, did you mean `%s`?", (*x3),
                                      fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown prototype `%s`", (*x3));
                    }
                }
            }
            else {      // delay eval
                WARN("delaying evaluation of undefined prototype `%s`", (*x3));
                def_rule0_str(pp, 3, &opts, x3, x4, &str_repr);
                BUFPUTS(pp->kwname);
                BUFCAT(str_repr);
                goto epilogue;
            }
        }

        const struct decl *dproto = sv_at(pp->sv_dproto, found.ix);
        /* write function signature */
        {
            srt_string *tmp = decl_to_str(dproto, (dproto)->name,
                                          ((dproto)->named || (dproto)->ndefault > 0)
                                          ? ss_to_c((dproto)->fsuffix) : "",
                                          false, opts.const_bool);
            ss_cat(&((*kwbuf)), tmp);
            ss_free(&tmp);
        }
        BUFPUTS((*x4));

        if (!opts.nocheck_bool) {
            {
                const size_t _len = sv_len(dproto->sv_fargs_type);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _58 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    const struct decl *argtyp = NULL;
                    {
                        setix _found = { 0 };
                        {
                            const srt_string *_ss =
                                ss_crefs((*(const char **)sv_at(dproto->sv_fargs_type, _58)));
                            if (NULL == pp->shm_dtype) {
                            }
                            _found.set = shm_atp_su(pp->shm_dtype, _ss, &(_found).ix);
                        }

                        if (_found.set)
                            argtyp = (void *)sv_at(pp->sv_dtype, _found.ix);
                    }

                    if (NULL == argtyp)
                        continue;       // type not %typedef'd
                    for (size_t i = 0; i < sv_len(argtyp->sv_begin); ++i) {
                        const char *name = sv_at_ptr(argtyp->sv_begin, i);
                        BUFPUTS("\n%" "recall [lazy] ", name, " ");

                        BUFPUTS("(");
                        if (NULL != dproto->sv_fargs) { // pass function parameters to snippet
                            BUFPUTS((dproto->named) ? ss_to_c(dproto->wrapper) : "",
                                    (dproto->named) ? "." : "", sv_at_ptr(dproto->sv_fargs, _58));

                        }

                        BUFPUTS(")\n");
                    }
                }
            }
            for (size_t i = 0; i < sv_len(dproto->sv_begin); ++i) {
                const char *name = sv_at_ptr(dproto->sv_begin, i);
                BUFPUTS("\n%" "recall [lazy] ", name, " ");

                BUFPUTS("(");
                if (NULL != dproto->sv_fargs) { // pass function parameters to snippet
                    {
                        const size_t _len = sv_len(dproto->sv_fargs);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _60 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS((_60 > 0) ? "," : "", "`",
                                    (dproto->named) ? ss_to_c(dproto->wrapper) : "",
                                    (dproto->named) ? "." : "",
                                    (*(const char **)sv_at(dproto->sv_fargs, _60)), "`");
                        }
                    }

                }

                BUFPUTS(")\n");
            }
        }

        /* append to function definition list */
        struct fdef fun = {
            .from_typedef = false,
            .name = (*x3),
            .index = found.ix,
            .retexpr = opts.return_str
        };
        (*x3) = NULL;   // hand-over
        opts.return_str = NULL; // hand-over

        /* index of current function definition */
        pp->infdef = SETIX_set(sv_len(pp->sv_fdef));
        {
            struct snippet *snip = NULL;
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs("!cmod:__func__");
                    if (NULL == pp->shm_snip) {
                    }
                    _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                }

                if (_found.set)
                    snip = (void *)sv_at(pp->sv_snip, _found.ix);
            }

            if (NULL == snip)
                assert(0 && "C% internal error" ": " "built-in snippet not found");

            {   // update snippet body
                ss_clear(snip->body);
                ss_cat_c(&snip->body, fun.name);
                ss_cat_cn1(&snip->body, '\0');  // NUL-terminate
            }
        }
        {
            const srt_string *_ss = ss_crefs((fun).name);
            if (!sv_push(&(pp->sv_fdef), &(fun))
                || !shm_insert_su(&(pp->shm_fdef), _ss, sv_len(pp->sv_fdef) - 1)) {
                YYNOMEM;
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.return_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int def_rule1_str(const struct param *const pp,
                         size_t debug_level,
                         const struct def_cmod_opts *const opts,
                         char **x3, char **x4, const char **x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    def_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x5);
    return 0;
}

int cmod_def_action_1(void *yyscanner,
                      struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      char **x3, const YYLTYPE *loc3,
                      char **x4, const YYLTYPE *loc4,
                      const char **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct def_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(def_opt_names) / sizeof(*(def_opt_names)));
        const char **opt_names = def_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eturn", 6) == 0) {
            ret = option_list_action_str(&opts.return_str, opt, &opts.return_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('c' == *((opt->name) + 2) && strncmp((opt->name) + 3, "heck", 5) == 0) {
                ret = option_list_action_bool(&opts.nocheck_bool, opt, &opts.nocheck_isset);

            }
            else if ('w' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_31;
            }
        }
        else {
            goto cmod_strin_else_31;
 cmod_strin_else_31:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (def_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      def_opt_names[icuropt], def_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        def_rule1_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (prepend_cmod_prefix(pp->sm_prefix_us, &((*x4)), '_')) {
            YYNOMEM;
        }

        {       // check existing
            const struct fdef *fun = NULL;
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs((*x4));
                    if (NULL == pp->shm_fdef) {
                    }
                    _found.set = shm_atp_su(pp->shm_fdef, _ss, &(_found).ix);
                }

                if (_found.set)
                    fun = (void *)sv_at(pp->sv_fdef, _found.ix);
            }

            if (NULL != fun) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("cannot redefine function `%s`", (*x4));
            }
            setix found = { 0 }; {
                const srt_string *_ss = ss_crefs((*x4));
                found.set = shm_atp_su(pp->shm_dproto, _ss, &(found).ix);
            }

            if (found.set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("function `%s` already has a prototype", (*x4));
            }
        }

        setix found = { 0 }; {
            const srt_string *_ss = ss_crefs((*x3));
            found.set = shm_atp_su(pp->shm_dtype, _ss, &(found).ix);
        }

        if (!found.set)
            assert(0 && "C% internal error" ": " "type must exist as C_TYPEDEF_NAME");

        const struct decl *dtype = sv_at(pp->sv_dtype, found.ix);
        if (!dtype->fargs_i0.set || !dtype->fargs_i1.set) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR("non-function type `%s`", dtype->name);
        }

        /* write function signature */
        {
            srt_string *tmp = decl_to_str(dtype, (*x4),
                                          ((dtype)->named || (dtype)->ndefault > 0)
                                          ? ss_to_c((dtype)->fsuffix) : "",
                                          false, opts.const_bool);
            ss_cat(&((*kwbuf)), tmp);
            ss_free(&tmp);
        }
        BUFPUTS((*x5));

        if (!opts.nocheck_bool) {
            {
                const size_t _len = sv_len(dtype->sv_fargs_type);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _59 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    const struct decl *argtyp = NULL;
                    {
                        setix _found = { 0 };
                        {
                            const srt_string *_ss =
                                ss_crefs((*(const char **)sv_at(dtype->sv_fargs_type, _59)));
                            if (NULL == pp->shm_dtype) {
                            }
                            _found.set = shm_atp_su(pp->shm_dtype, _ss, &(_found).ix);
                        }

                        if (_found.set)
                            argtyp = (void *)sv_at(pp->sv_dtype, _found.ix);
                    }

                    if (NULL == argtyp)
                        continue;       // type not %typedef'd
                    for (size_t i = 0; i < sv_len(argtyp->sv_begin); ++i) {
                        const char *name = sv_at_ptr(argtyp->sv_begin, i);
                        BUFPUTS("\n%" "recall [lazy] ", name, " ");

                        BUFPUTS("(");
                        if (NULL != dtype->sv_fargs) {  // pass function parameters to snippet
                            BUFPUTS((dtype->named) ? ss_to_c(dtype->wrapper) : "",
                                    (dtype->named) ? "." : "", sv_at_ptr(dtype->sv_fargs, _59));

                        }

                        BUFPUTS(")\n");
                    }
                }
            }
            for (size_t i = 0; i < sv_len(dtype->sv_begin); ++i) {
                const char *name = sv_at_ptr(dtype->sv_begin, i);
                BUFPUTS("\n%" "recall [lazy] ", name, " ");

                BUFPUTS("(");
                if (NULL != dtype->sv_fargs) {  // pass function parameters to snippet
                    {
                        const size_t _len = sv_len(dtype->sv_fargs);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _61 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS((_61 > 0) ? "," : "", "`",
                                    (dtype->named) ? ss_to_c(dtype->wrapper) : "",
                                    (dtype->named) ? "." : "",
                                    (*(const char **)sv_at(dtype->sv_fargs, _61)), "`");
                        }
                    }

                }

                BUFPUTS(")\n");
            }
        }

        /* append to function definition list */
        struct fdef fun = {
            .from_typedef = true,
            .name = (*x4),
            .index = found.ix,
            .retexpr = opts.return_str
        };
        (*x4) = NULL;   // hand-over
        opts.return_str = NULL; // hand-over

        /* index of current function definition */
        pp->infdef = SETIX_set(sv_len(pp->sv_fdef));
        {
            struct snippet *snip = NULL;
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs("!cmod:__func__");
                    if (NULL == pp->shm_snip) {
                    }
                    _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                }

                if (_found.set)
                    snip = (void *)sv_at(pp->sv_snip, _found.ix);
            }

            if (NULL == snip)
                assert(0 && "C% internal error" ": " "built-in snippet not found");

            {   // update snippet body
                ss_clear(snip->body);
                ss_cat_c(&snip->body, fun.name);
                ss_cat_cn1(&snip->body, '\0');  // NUL-terminate
            }
        }
        {
            const srt_string *_ss = ss_crefs((fun).name);
            if (!sv_push(&(pp->sv_fdef), &(fun))
                || !shm_insert_su(&(pp->shm_fdef), _ss, sv_len(pp->sv_fdef) - 1)) {
                YYNOMEM;
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.return_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int def_rule2_str(const struct param *const pp,
                         size_t debug_level,
                         const struct def_cmod_opts *const opts,
                         char **x3, char **x4, const char **x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    def_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x5);
    return 0;
}

int cmod_def_action_2(void *yyscanner,
                      struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      char **x3, const YYLTYPE *loc3,
                      char **x4, const YYLTYPE *loc4,
                      const char **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct def_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(def_opt_names) / sizeof(*(def_opt_names)));
        const char **opt_names = def_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eturn", 6) == 0) {
            ret = option_list_action_str(&opts.return_str, opt, &opts.return_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('c' == *((opt->name) + 2) && strncmp((opt->name) + 3, "heck", 5) == 0) {
                ret = option_list_action_bool(&opts.nocheck_bool, opt, &opts.nocheck_isset);

            }
            else if ('w' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_32;
            }
        }
        else {
            goto cmod_strin_else_32;
 cmod_strin_else_32:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (def_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      def_opt_names[icuropt], def_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        def_rule2_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (opts.now_bool) {
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = (*x3);
                mutpp->errlloc = (*loc3);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match((*x3), pp->sv_dtype, sv_len(pp->sv_dtype), strget_array_decl,
                                    0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown type `%s`, did you mean `%s`?", (*x3), fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown type `%s`", (*x3));
                }
            }
        }
        WARN("delaying evaluation of undefined type `%s`", (*x3));
        def_rule2_str(pp, 3, &opts, x3, x4, x5, &str_repr);
        BUFPUTS(pp->kwname);
        BUFCAT(str_repr);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.return_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int def_rule3_str(const struct param *const pp,
                         size_t debug_level,
                         const struct def_cmod_opts *const opts,
                         char **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    def_opt_string(opts, s);
    (void)x3;

    if (NULL != *x3) {
        ss_cat_cn1(s, ' ');
        ss_cat_c(s, *x3);
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_def_action_3(void *yyscanner,
                      struct param *pp,
                      vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                      char **x3, const YYLTYPE *loc3,
                      const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct def_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(def_opt_names) / sizeof(*(def_opt_names)));
        const char **opt_names = def_opt_names; // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('r' == *((opt->name) + 0) && strncmp((opt->name) + 1, "eturn", 6) == 0) {
            ret = option_list_action_str(&opts.return_str, opt, &opts.return_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('c' == *((opt->name) + 2) && strncmp((opt->name) + 3, "heck", 5) == 0) {
                ret = option_list_action_bool(&opts.nocheck_bool, opt, &opts.nocheck_isset);

            }
            else if ('w' == *((opt->name) + 2) && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.now_bool, opt, &opts.now_isset);

            }
            else {
                goto cmod_strin_else_33;
            }
        }
        else {
            goto cmod_strin_else_33;
 cmod_strin_else_33:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (def_opt_compat(&opts, &icuropt, &ibadopt)) {    // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      def_opt_names[icuropt], def_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        def_rule3_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (opts.return_isset)
            WARN0("ignoring option [return]");

        const struct fdef *fun = NULL;
        const struct fdef *fun_indef =  // current function definition
            (pp->infdef.set ? sv_at(pp->sv_fdef, pp->infdef.ix) : NULL);

        if (NULL == (*x3)) {
            if (NULL == fun_indef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR0("currently not in a function definition");
            }
            (*x3) = (NULL != (fun_indef->name)) ? rstrdup(fun_indef->name) : NULL;
            if (NULL != (fun_indef->name) && NULL == ((*x3))) {
                YYNOMEM;
            }

            fun = fun_indef;
        }
        else {
            if (prepend_cmod_prefix(pp->sm_prefix_us, &((*x3)), '_')) {
                YYNOMEM;
            }
            {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs((*x3));
                    if (NULL == pp->shm_fdef) {
                    }
                    _found.set = shm_atp_su(pp->shm_fdef, _ss, &(_found).ix);
                }

                if (_found.set)
                    fun = (void *)sv_at(pp->sv_fdef, _found.ix);
            }

            if (NULL != fun && NULL != fun_indef && fun != fun_indef) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("bad function `%s`, currently defining function `%s`",
                              (*x3), fun_indef->name);
            }
        }

        if (NULL == fun) {
            if (opts.now_bool) {
                {
                    struct param *mutpp = yyget_extra(yyscanner);
                    mutpp->errtxt = (*x3);
                    mutpp->errlloc = (*loc3);
                    const char *fuzzy_match;
                    bool is_close;
                    is_close =
                        str_fuzzy_match((*x3), pp->sv_fdef, sv_len(pp->sv_fdef), strget_array_fdef,
                                        0.75, &(fuzzy_match));

                    if (NULL != fuzzy_match && is_close) {
                        KEYWORD_ERROR("unknown type `%s`, did you mean `%s`?", (*x3), fuzzy_match);
                    }
                    else {
                        KEYWORD_ERROR("unknown type `%s`", (*x3));
                    }
                }
            }
            else {      // delay eval
                WARN("delaying evaluation of undefined function `%s`", (*x3));
                def_rule3_str(pp, 3, &opts, x3, x4, &str_repr);
                BUFPUTS(pp->kwname);
                BUFCAT(str_repr);
            }
        }
        else {
            const struct decl *dfun =
                sv_at(fun->from_typedef ? pp->sv_dtype : pp->sv_dproto, fun->index);

            if (NULL != fun->retexpr) {
                /* end label */
                BUFPUTS("goto end;\n"); // use label
                BUFPUTS("end: ;\n");    // valid label
                if (!opts.nocheck_bool) {
                    /* return type checks */
                    const struct decl *rettyp = NULL;
                    {
                        setix _found = { 0 };
                        {
                            const srt_string *_ss = ss_crefs(dfun->type);
                            if (NULL == pp->shm_dtype) {
                            }
                            _found.set = shm_atp_su(pp->shm_dtype, _ss, &(_found).ix);
                        }

                        if (_found.set)
                            rettyp = (void *)sv_at(pp->sv_dtype, _found.ix);
                    }

                    if (NULL != rettyp) {
                        for (size_t i = 0; i < sv_len(rettyp->sv_begin); ++i) {
                            const char *name = sv_at_ptr(rettyp->sv_begin, i);
                            BUFPUTS("\n%" "recall [lazy] ", name, " ");

                            BUFPUTS("(");
                            BUFCAT(fun->retexpr);

                            BUFPUTS(")\n");
                        }
                    }
                    /* function post-condition checks */
                    for (size_t i = 0; i < sv_len(dfun->sv_end); ++i) {
                        const char *name = sv_at_ptr(dfun->sv_end, i);
                        BUFPUTS("\n%" "recall [lazy] ", name, " ");
                        if (NULL != fun->retexpr) {
                            BUFPUTS("(`");
                            BUFCAT(fun->retexpr);
                            BUFPUTS("`) <- ");
                        }

                        BUFPUTS("(");
                        if (NULL != dfun->sv_fargs) {   // pass function parameters to snippet
                            {
                                const size_t _len = sv_len(dfun->sv_fargs);
                                for (size_t _idx = 0; _idx < _len; ++_idx) {
                                    const size_t _62 = _idx;
                                    const size_t _idx = 0;
                                    (void)_idx;
                                    BUFPUTS((_62 > 0) ? "," : "", "`",
                                            (dfun->named) ? ss_to_c(dfun->wrapper) : "",
                                            (dfun->named) ? "." : "",
                                            (*(const char **)sv_at(dfun->sv_fargs, _62)), "`");
                                }
                            }

                        }

                        BUFPUTS(")\n");
                    }
                }
                /* return statement */
                BUFPUTS("\nreturn ");
                BUFCAT(fun->retexpr);
                BUFPUTC(';');
            }

            BUFPUTS("\n", (*x4));
            pp->infdef = SETIX_none;    // no longer in function definition
            {
                struct snippet *snip = NULL;
                {
                    setix _found = { 0 };
                    {
                        const srt_string *_ss = ss_crefs("!cmod:__func__");
                        if (NULL == pp->shm_snip) {
                        }
                        _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
                    }

                    if (_found.set)
                        snip = (void *)sv_at(pp->sv_snip, _found.ix);
                }

                if (NULL == snip)
                    assert(0 && "C% internal error" ": " "built-in snippet not found");

                {       // update snippet body
                    ss_clear(snip->body);
                    ss_cat_c(&snip->body, NULL);
                    ss_cat_cn1(&snip->body, '\0');      // NUL-terminate
                }
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.return_str));
    ss_free(&str_repr);

    return _ret;
}

static int enum_action(const char *enum_name, const srt_string *default_name, const size_t nrows,
                       const char *names[static nrows], vec_vec_string *svv_rows,
                       const setix name_col, const setix desc_col, const setix value_col,
                       const struct enum_cmod_opts *const opts, srt_string **kwbuf)
{
    (void)name_col;
    bool has_default = (NULL != default_name);
    srt_string *enumval = NULL;

    if (NULL == ((enumval) = ss_alloc(32))) {
        return 1;
    }

    const char *equ = opts->bitflag_bool ? "" : "= ";
    const char *sep = opts->bitflag_bool ? "" : ",";

    if (!opts->bitflag_bool)
        BUFPUTS("enum ", enum_name, " {\n");    // begin enum

    if (has_default) {
        if (opts->bitflag_bool) {
            if (ss_void == ss_cat_c(&enumval, "#define ")) {
                return 1;
            }

        }

        if (ss_void == ss_cat(&enumval, default_name)) {
            return 1;
        }

        if (ss_void == ss_cat_c(&enumval, " ", equ)) {
            return 1;
        }

        if (opts->bitflag_bool) {

            if (ss_void == ss_cat_c(&enumval, "0UL")) {
                return 1;
            }

        }
        else {
            int defval = opts->shift_int;

            if (ss_void == ss_cat_int(&enumval, defval)) {
                return 1;
            }

        }

        if (ss_void == ss_cat_c(&enumval, sep, " /* default */", "\n")) {
            return 1;
        }

        BUFCAT(enumval);
        ss_clear(enumval);
    }

    for (size_t i = 0; i < nrows; ++i) {
        const vec_string *row = sv_at_ptr(svv_rows, i);
        int val = opts->shift_int + (opts->rev_bool ? -1 : 1) * (i + has_default);
        if (opts->bitflag_bool) {
            val -= has_default; // start at 0 even with default

            if (ss_void == ss_cat_c(&enumval, "#define ")) {
                return 1;
            }

        }

        if (ss_void == ss_cat_c(&enumval, names[i], " ")) {
            return 1;
        }

        if (value_col.set) {
            const char *value = sv_at_ptr(row, value_col.ix);
            if ('\0' == value[0])
                return 2;       // ERROR: empty name

            if (ss_void == ss_cat_c(&enumval, equ, value)) {
                return 1;
            }

        }
        else if (opts->bitflag_bool) {

            if (ss_void == ss_cat_c(&enumval, "(1UL << ")) {
                return 1;
            }

            if (ss_void == ss_cat_int(&enumval, val)) {
                return 1;
            }

            if (ss_void == ss_cat_cn1(&enumval, ')')) {
                return 1;
            }

        }
        else {

            if (ss_void == ss_cat_c(&enumval, equ)) {
                return 1;
            }

            if (ss_void == ss_cat_int(&enumval, val)) {
                return 1;
            }

        }

        if (ss_void == ss_cat_c(&enumval, sep)) {
            return 1;
        }

        if (desc_col.set) {
            const char *desc = sv_at_ptr(row, desc_col.ix);

            if (ss_void == ss_cat_c(&enumval, " /* ", desc, " */")) {
                return 1;
            }

        }

        if (ss_void == ss_cat_cn1(&enumval, '\n')) {
            return 1;
        }

        BUFCAT(enumval);
        ss_clear(enumval);
    }
    ss_free(&enumval);

    if (!opts->bitflag_bool)
        BUFPUTS("};\n");        // end enum

    if (opts->bitflag_bool)
        return 0;       // finished with bitflags

    /* number of non-default elements */
    BUFPUTS("#define enum_", enum_name, "_len ");
    BUFPUTINT(nrows);
    BUFPUTC('\n');

    if (opts->helper_index.set) {
        /* function mapping enum values to descriptions */
        if (((((opts->helper_index.ix) >> (0)) & 1UL) != 0)) {
            BUFPUTS("\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (const)\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (unused)\n");
            BUFPUTS("static inline const char* ", enum_name,
                    "_strenum(const enum ", enum_name, " x) {\n");
            BUFPUTS("switch(x) {\n");
            for (size_t i = 0; i < nrows; ++i) {
                const vec_string *row = sv_at_ptr(svv_rows, i);
                if (desc_col.set) {
                    const char *desc = sv_at_ptr(row, desc_col.ix);
                    BUFPUTS("case ", names[i], ": return \"", desc, "\"; break;\n");
                }
                else
                    BUFPUTS("case ", names[i], ": return \"", names[i], "\"; break;\n");
            }
            BUFPUTS("default: return \"");
            if (has_default)
                BUFCAT(default_name);
            BUFPUTS("\"; break;\n");
            BUFPUTS("}\n}\n");
        }

        /* function mapping enum values to string enum constants */
        if (((((opts->helper_index.ix) >> (1)) & 1UL) != 0)) {
            BUFPUTS("\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (const)\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (unused)\n");
            BUFPUTS("static inline const char* ", enum_name,
                    "_enumtostr(const enum ", enum_name, " x) {\n");
            BUFPUTS("switch(x) {\n");
            for (size_t i = 0; i < nrows; ++i)
                BUFPUTS("case ", names[i], ": return \"", names[i], "\"; break;\n");
            BUFPUTS("default: return \"");
            if (has_default)
                BUFCAT(default_name);
            BUFPUTS("\"; break;\n");
            BUFPUTS("}\n}\n");

            /* define convenience snippet for std:getopt */
            BUFPUTS("%" "defined [once] std:getopt (%<<\n",
                    "%" "snippet std:getopt_default_str:enum_", enum_name,
                    " = (variable,buf) " "%" "{\n",
                    "(void)snprintf(${buf},sizeof(${buf}),\"%%s\",", enum_name,
                    "_enumtostr(${variable}));\n", "%" "}\n", ">>%)\n");
        }

        /* function mapping string enum constants to values */
        if (((((opts->helper_index.ix) >> (2)) & 1UL) != 0)) {
            BUFPUTS("\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (const)\n");
            BUFPUTS("%" "recall [opt] std:emit_c_attribute (unused)\n");
            BUFPUTS("static inline bool ", enum_name,
                    "_strtoenum(const char *str, int val[static 1]) {\n");
            {   // print iftrie for enum constants
#define ACTION_LEN 1024
                char buf_else[ACTION_LEN];
                srt_string *action_else = ss_alloc_into_ext_buf(buf_else, sizeof(buf_else));
                if (has_default) {
                    ss_cat_c(&action_else, "*val = ", ss_to_c(default_name), "; return true;");
                }
                else {
                    ss_cat_c(&action_else, "return false;");
                }

                char **actions = rmalloc(nrows * sizeof(*actions));
                if (NULL == actions)
                    return 1;   // ERROR: out of memory
                for (size_t i = 0; i < nrows; ++i) {
                    char buf[ACTION_LEN];
                    srt_string *action = ss_alloc_into_ext_buf(buf, sizeof(buf));
                    ss_cat_c(&action, "*val = ", names[i], "; return true;");
                    const char *tmp = ss_to_c(action);
                    actions[i] = (NULL != (tmp)) ? rstrdup(tmp) : NULL;
                    if (NULL != (tmp) && NULL == (actions[i])) {
                        return 1;
                    }

                }
#undef ACTION_LEN

                struct iftrie_params ifp = {
                    .arrlen = nrows,
                    .words = (const char **)names,
                    .actions = (const char **)actions,
                    .action_else = ss_to_c(action_else),
                    .c2ix = loc_ascii_printable,
                    .var = "str",
                    .label = "iftrie_end",
                    .nexpand = 3,
                    .fuzzy = false
                };
                ss_cat_iftrie_ascii(kwbuf, &ifp);

                for (size_t i = 0; i < nrows; ++i)
                    free(actions[i]);
                free(actions);
            }
            BUFPUTS("}\n");
        }
    }

    return 0;
}

__attribute__((unused))
static int enum_rule0_str(const struct param *const pp,
                          size_t debug_level,
                          const struct enum_cmod_opts *const opts,
                          char **x3, const char **x4, struct table_like *x5,
                          srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    enum_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    (void)x5;
    ss_cat_cn1(s, ' ');
    if (NULL != x5->lst) {      // table name list
        for (size_t i = 0; i < sv_len(x5->lst); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            hstr_str_repr(sv_at_ptr(x5->lst, i), s);
        }
    }
    else if (NULL != x5->sel) {
        ss_cat_c(s, x5->tab.name, " ");
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < sv_len(x5->sel); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(x5->sel, i), x5->tab.sv_colnarg, s);
        }
        ss_cat_cn1(s, ')');
    }
    else {      // table
        if (x5->is_lambda) {
            if (debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(s, "<lambda table>");
            }
            else
                ss_cat_c(s, "<lambda table>");
        }
        else {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(*&(&x5->tab)->sv_colnarg, SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
            ss_cat_c(s, " %" "{ ");
            if (debug_level >= 2) {
                /* TODO: print table body */
                ss_cat_c(s, "<table body>");
            }
            else
                ss_cat_c(s, "<table body>");
            ss_cat_c(s, " %" "}");
        }
    }

    return 0;
}

int cmod_enum_action_0(void *yyscanner,
                       struct param *pp,
                       vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                       char **x3, const YYLTYPE *loc3,
                       const char **x4, const YYLTYPE *loc4,
                       struct table_like *x5, const YYLTYPE *loc5,
                       bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct enum_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(enum_opt_names) / sizeof(*(enum_opt_names)));
        const char **opt_names = enum_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "itflag", 7) == 0) {
            ret = option_list_action_bool(&opts.bitflag_bool, opt, &opts.bitflag_isset);

        }
        else if ('h' == *((opt->name) + 0) && strncmp((opt->name) + 1, "elper", 6) == 0) {
            ret = option_list_action_index(&opts.helper_index, opt, &opts.helper_isset);

        }
        else if ('p' == *((opt->name) + 0) && strncmp((opt->name) + 1, "refix", 6) == 0) {
            ret = option_list_action_str(&opts.prefix_str, opt, &opts.prefix_isset);

        }
        else if ('r' == *((opt->name) + 0) && 'e' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.rev_bool, opt, &opts.rev_isset);

        }
        else if ('s' == *((opt->name) + 0) && strncmp((opt->name) + 1, "hift", 5) == 0) {
            ret = option_list_action_int(&opts.shift_int, opt, &opts.shift_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "pper", 5) == 0) {
            ret = option_list_action_bool(&opts.upper_bool, opt, &opts.upper_isset);

        }
        else if ('d' == *((opt->name) + 0) && 'e' == *((opt->name) + 1)) {
            if ('b' == *((opt->name) + 2) && 'u' == *((opt->name) + 3) && 'g' == *((opt->name) + 4)
                && '\0' == *((opt->name) + 5)) {
                ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

            }
            else if ('f' == *((opt->name) + 2) && strncmp((opt->name) + 3, "ault", 5) == 0) {
                ret = option_list_action_cid(&opts.default_cid, opt, &opts.default_isset);

            }
            else {
                goto cmod_strin_else_34;
            }
        }
        else {
            goto cmod_strin_else_34;
 cmod_strin_else_34:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.bitflag_bool && opts.shift_int < 0) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("[bitflag] requires non-negative [shift]");
    }
    if (opts.helper_index.set) {
        if (opts.helper_index.ix > 7)
            KEYWORD_ERROR0("[helper] takes values <= 7");
        if (opts.helper_index.ix == 0)
            opts.helper_index.ix = 7;   // all set
    }

    int icuropt = -1, ibadopt = -1;
    if (enum_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      enum_opt_names[icuropt], enum_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        enum_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        if (sv_len((*x5).lst) > 0) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR0("expected column selection");
        }
        else if (NULL != (*x5).sel && NULL == (*x5).tab.svv_rows) {
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = (*x5).tab.name;
                mutpp->errlloc = (*loc5);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match((*x5).tab.name, pp->sv_tab, sv_len(pp->sv_tab),
                                    strget_array_tab, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", (*x5).tab.name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown table `%s`", (*x5).tab.name);
                }
            }
        }

        struct table *tab = &(*x5).tab;
        if (!opts.default_isset) {      // print default even if table is empty
            {
                const vec_string *firstrow = (sv_len((tab)->svv_rows) > 0)
                    ? sv_at_ptr((tab)->svv_rows, 0)
                    : NULL;
                if (NULL == firstrow || sv_len(firstrow) == 0) {
                    WARN("ignoring empty table `%s`", (tab)->name ? (tab)->name : "<lambda>");
                    goto epilogue;
                }
            }
        }
        size_t nargs = (NULL != (*x5).sel) ? sv_len((*x5).sel)
            : TABLE_NCOLS(*tab);
        if (nargs > 3) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            KEYWORD_ERROR("expected up to %d columns", 3);
        }

        setix cols_ix[3] = { {0} };
        if (NULL != (*x5).sel) {
            {
                for (size_t i = 0; i < sv_len((*x5).sel); ++i) {
                    const struct namarg *narg = sv_at((*x5).sel, i);
                    if (narg->type != CMOD_ARG_ARGLINK)
                        assert(0 && "C% internal error" "");

                    size_t ix = CMOD_ARG_ARGLINK_get(*narg);
                    if (NULL == narg->name)
                        cols_ix[i] = SETIX_set(ix);
                    else if (strcmp(narg->name, "name") == 0) {
                        if (cols_ix[0].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "name";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "name");
                        }
                        cols_ix[0] = SETIX_set(ix);
                    }
                    else if (strcmp(narg->name, "description") == 0) {
                        if (cols_ix[1].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "description";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "description");
                        }
                        cols_ix[1] = SETIX_set(ix);
                    }
                    else if (strcmp(narg->name, "value") == 0) {
                        if (cols_ix[2].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "value";
                                mutpp->errlloc = (*loc5);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "value");
                        }
                        cols_ix[2] = SETIX_set(ix);
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc5);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected special identifier named argument `%s`",
                                      narg->name);
                    }
                }
                if (!cols_ix[0].set) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                    KEYWORD_ERROR0("missing required column `" "name" "`");
                }
            }
        }
        else {
            {
                const srt_string *_ss = ss_crefs("name");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[0].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[0]).ix);
            }
            {
                const srt_string *_ss = ss_crefs("description");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[1].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[1]).ix);
            }
            {
                const srt_string *_ss = ss_crefs("value");
                if (NULL == (*x5).tab.shm_colnam) {
                }
                cols_ix[2].set = shm_atp_su((*x5).tab.shm_colnam, _ss, &(cols_ix[2]).ix);
            }
            if (!cols_ix[0].set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                KEYWORD_ERROR("missing required column `%s`", "name");
            }
        }

        srt_string *enum_prefix = NULL;
        if (prepend_cmod_prefix(pp->sm_prefix_us, &((*x3)), '_')) {
            YYNOMEM;
        }
        {
            srt_string *ss = NULL;
            (ss) = ss_alloc(32);
            if (ss_void == (ss)) {
                YYNOMEM;
            }

            if (opts.prefix_isset) {    // use provided prefix
                if (ss_len(opts.prefix_str) > 0 && !striscid(ss_to_c(opts.prefix_str))) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    KEYWORD_ERROR0("prefix must be a valid C identifier");
                }
                ss_cat(&ss, opts.prefix_str);
                if (opts.upper_bool)
                    ss_toupper(&ss);
            }
            else {      // use uppercased enum name and underscore
                ss_cat_c(&ss, (*x3), "_");
                ss_toupper(&ss);
            }
            enum_prefix = ss;
        }

        /* setup default enum constant */
        srt_string *default_name = NULL;
        if (ss_len(opts.default_cid) > 0) {
            default_name = ss_dup_c(ss_to_c(enum_prefix));      // NOTE: handle empty string gracefully
            if (ss_void == default_name)
                YYNOMEM;
            ss_cat(&default_name, opts.default_cid);

            if (opts.upper_bool)
                ss_toupper(&default_name);

            bool found;
            found = sms_count_s(pp->sms_enum, default_name);

            if (found) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                KEYWORD_ERROR("duplicate enum constant `%s` in enum `%s`",
                              ss_to_c(default_name), (*x3));
            }
            if (!sms_insert_s(&pp->sms_enum, default_name)) {
                YYNOMEM;
            }
        }

        /* setup enum constants */
        size_t nrows = sv_len(tab->svv_rows);
        char **names = rmalloc(nrows * sizeof(*names));
        if (NULL == names)
            return 1;
        for (size_t i = 0; i < nrows; ++i) {
            const vec_string *row = sv_at_ptr(tab->svv_rows, i);
            const char *name = sv_at_ptr(row, cols_ix[0].ix);
            if ('\0' == name[0]) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                KEYWORD_ERROR("empty name in row %zu of enum `%s`", i, (*x3));
            }

            /* add prefix */
            srt_string *sname = ss_dup_c(ss_to_c(enum_prefix)); // NOTE: handle empty string gracefully
            if (ss_void == sname)
                YYNOMEM;
            ss_cat_c(&sname, name);

            if (opts.upper_bool)
                ss_toupper(&sname);

            bool found;
            found = sms_count_s(pp->sms_enum, sname);

            if (found) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                KEYWORD_ERROR("duplicate enum constant `%s` in row %zu of enum `%s`",
                              ss_to_c(default_name), i, (*x3));
            }
            if (!sms_insert_s(&pp->sms_enum, sname)) {
                YYNOMEM;
            }
            {
                const char *ctmp = ss_to_c(sname);
                names[i] = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (names[i])) {
                }

            }
            ss_free(&(sname));
            if (NULL == names[i]) {
                YYNOMEM;
            }
        }

        {
            int ret = enum_action((*x3), default_name,
                                  nrows, (const char **)names, tab->svv_rows,
                                  cols_ix[0], cols_ix[1], cols_ix[2],
                                  &opts, kwbuf);
            const char *errstr = NULL;
            switch (ret) {
            case 1:
                YYNOMEM;
                break;
            case 2:
                errstr = "empty value";
                break;
            default:
                errstr = "unspecific error";
                break;
            }
            if (ret)
                KEYWORD_ERROR("%s in enum `%s`", errstr, (*x3));
        }

        ss_free(&enum_prefix);
        ss_free(&default_name);
        for (size_t i = 0; i < nrows; ++i)
            free(names[i]);
        {
            free(names);
            (names) = NULL;
        }

        (void)(*x4);
        {
            for (size_t i = 0; i < sv_len((*x5).sel); ++i) {
                const void *_item = sv_at((*x5).sel, i);
                namarg_free((struct namarg *)_item);
            }
            sv_free(&((*x5).sel));
        }
        if (NULL == tab->name)  // free lamda table
            table_clear(tab);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.default_cid));
    ss_free(&(opts.prefix_str));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int foreach_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct foreach_cmod_opts *const opts,
                             char **x3, vec_string **x4, srt_string **x5, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    foreach_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x4); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x4, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    (void)x5;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{\n");
    if (debug_level >= 2) {
        const struct snippet tmp = {
            .body = *x5,        // const borrow
            .sv_argl = pp->sv_dllarg    // const borrow
        };
        snippet_body_str_repr(&tmp, tmp.sv_argl, s);
    }
    else
        ss_cat_c(s, "<snippet body>");
    ss_cat_c(s, "%" "}");

    return 0;
}

int cmod_foreach_action_0(void *yyscanner,
                          const struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          char **x3, const YYLTYPE *loc3,
                          vec_string **x4, const YYLTYPE *loc4,
                          srt_string **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct foreach_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(foreach_opt_names) / sizeof(*(foreach_opt_names)));
        const char **opt_names = foreach_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "utoarr", 7) == 0) {
            ret = option_list_action_bool(&opts.autoarr_bool, opt, &opts.autoarr_isset);

        }
        else if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('p' == *((opt->name) + 0)) {
            if ('a' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_bool(&opts.par_bool, opt, &opts.par_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)
                     && '\0' == *((opt->name) + 3)) {
                ret = option_list_action_str(&opts.ptr_str, opt, &opts.ptr_isset);

            }
            else {
                goto cmod_strin_else_35;
            }
        }
        else if ('r' == *((opt->name) + 0) && 'e' == *((opt->name) + 1) && 'v' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.rev_bool, opt, &opts.rev_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('k' == *((opt->name) + 1) && 'i' == *((opt->name) + 2) && 'p' == *((opt->name) + 3)
                && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_count(&opts.skip_count, opt, &opts.skip_isset);

            }
            else if ('v' == *((opt->name) + 1) && 'e' == *((opt->name) + 2)
                     && 'c' == *((opt->name) + 3) && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_str(&opts.svec_str, opt, &opts.svec_isset);

            }
            else {
                goto cmod_strin_else_35;
            }
        }
        else if ('d' == *((opt->name) + 0) && 'e' == *((opt->name) + 1)) {
            if ('b' == *((opt->name) + 2) && 'u' == *((opt->name) + 3) && 'g' == *((opt->name) + 4)
                && '\0' == *((opt->name) + 5)) {
                ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

            }
            else if ('r' == *((opt->name) + 2) && 'e' == *((opt->name) + 3)
                     && 'f' == *((opt->name) + 4) && '\0' == *((opt->name) + 5)) {
                ret = option_list_action_count(&opts.deref_count, opt, &opts.deref_isset);

            }
            else {
                goto cmod_strin_else_35;
            }
        }
        else {
            goto cmod_strin_else_35;
 cmod_strin_else_35:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (opts.const_bool && ss_len(opts.ptr_str) == 0 && ss_len(opts.svec_str) == 0) {
        ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
        KEYWORD_ERROR0("[const] requires a non-blank [ptr] or [svec]");
    }

    int icuropt = -1, ibadopt = -1;
    if (foreach_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      foreach_opt_names[icuropt], foreach_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        foreach_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t nargs = sv_len((*x4));
        if (nargs > 2) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR0("expected up to two arguments");
        }
        if ('\0' == ss_at((*x5), 0)) {
            WARN0("ignoring empty or blank lambda");
            goto epilogue;
        }

        const char *str_x = sv_at_ptr((*x4), 0);
        const char *str_len = NULL;
        srt_string *lenstr = NULL;
        srt_string *loopstr = NULL;

        if (nargs > 1)
            str_len = sv_at_ptr((*x4), 1);
        else {  // build <length> string
            (lenstr) = ss_alloc(8);
            if (ss_void == (lenstr)) {
                YYNOMEM;
            }
            ;
            if (opts.autoarr_bool) {    // std:autoarr array
                ss_cat_c(&lenstr, "(", str_x, ").len");
            }
            else if (ss_len(opts.svec_str) > 0) {       // srt_vector
                ss_cat_c(&lenstr, "sv_len(", str_x, ")");
            }
            else {      // static array
                ss_cat_c(&lenstr, "(sizeof(", str_x, ") / sizeof(*(", str_x, ")))");
            }
            str_len = ss_to_c(lenstr);  // const borrow
        }

        if (strisspace(str_x) || strisspace(str_len)) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR0("empty or whitespace argument");
        }

        {       // compose unique loop index variable name
            (loopstr) = ss_alloc(24);
            if (ss_void == (loopstr)) {
                YYNOMEM;
            }

            ss_cat_cn1(&loopstr, '_');
            ss_cat_int(&loopstr, pp->nforeach - 1);
        }
        const char *loopvar = ss_to_c(loopstr);

        /* define temporary snippet */
        vec_namarg *(sv_forl) = sv_alloc(sizeof(struct namarg), 1, NULL);
        if (sv_void == (sv_forl)) {
            YYNOMEM;
        }
        ;
        {
            struct namarg item = CMOD_ARG_NOVALUE_xset(.name = (*x3));
            if (!sv_push(&(sv_forl), &(item))) {
                YYNOMEM;
            }

        }
        struct snippet snip = { 0 };
        snip = (struct snippet) {
            .name = NULL,.body = (*x5),
            .sv_forl = sv_forl,.sv_argl = pp->sv_dllarg,
            .redef = false,.nout = 0
        };
        {       // check snippet
            size_t ierr = 0;
            const char *err = NULL;
            int ret = snip_def_check(&snip, true, &ierr, &err); // is_lambda = true
            if (ret >= 2)
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
            switch (ret) {
            case 0:
                if (ierr == 0)
                    break;
                VERBOSE("unused arguments (%zu) in lambda snippet", ierr);
                break;
            case 1:
                YYNOMEM;;
                break;
                /* case 2 not possible for lambda snippet */
                /* case 3 not possible for lambda snippet */
            case 4:
                KEYWORD_ERROR("reference to undefined column `%s` in lambda snippet", err);
                break;
            case 5:
                KEYWORD_ERROR("%s in lambda snippet", err);
                break;
            }
        }

        BUFPUTS("{\n",  // begin block
                "const size_t _len = ", str_len, ";\n");        // preclude changing number of iterations
        if (opts.par_bool) {    // parallel
            BUFPUTS("#ifdef _OPENMP\n", "#pragma omp parallel for\n", "#endif\n");
        }
        BUFPUTS("for(size_t _idx = ");
        BUFPUTINT(opts.skip_count);
        BUFPUTS("; _idx < _len; ++_idx) {\n",   /* begin for */
                "const size_t ", loopvar, " = ", opts.rev_bool ? "_len - (_idx + 1)" : "_idx", ";\n", "const size_t _idx = 0; (void)_idx;\n");  // mask iteration index

        const char *str_const = opts.const_bool ? "const " : "";

        {       // prepare loop body
            srt_string *(val) = ss_alloc(8);
            if (ss_void == (val)) {
                YYNOMEM;
            }
            ;

            ss_cat_cn1(&val, '(');
            for (size_t i = 0; i < opts.deref_count; ++i)
                ss_cat_cn1(&val, '*');
            if (ss_len(opts.svec_str) > 0) {
                ss_cat_c(&val, "(", str_const);
                ss_cat(&val, opts.svec_str);
                ss_cat_c(&val, "*)sv_at(", str_x, ",", loopvar, ")");
            }
            else {
                ss_cat_cn1(&val, '(');
                if (ss_len(opts.ptr_str) > 0) {
                    ss_cat_c(&val, "(", str_const);
                    ss_cat(&val, opts.ptr_str);
                    ss_cat_cn1(&val, ')');
                }
                ss_cat_c(&val, "(", str_x, ")");
                if (opts.autoarr_bool)
                    ss_cat_c(&val, ".arr");
                ss_cat_c(&val, ")[", loopvar, "]");
            }
            ss_cat_cn1(&val, ')');

            const char *values = ss_to_c(val);
            size_t buflen = 0;
            char *buf = snip_get_buf_rowno(&snip, 1, &values, &buflen, loopvar);
            BUFPUTS(buf);
            {
                free(buf);
                (buf) = NULL;
            }

            ss_free(&val);
        }

        BUFPUTS("}\n",  /* end for */
                "}\n"); // end block

        snip.name = NULL;       // unborrow
        snip.body = NULL;       // unborrow
        snip.sv_argl = NULL;    // unborrow
        sv_free(&snip.sv_forl); // unborrow contents
        snippet_clear(&snip);

        ss_free(&lenstr);
        ss_free(&loopstr);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.svec_str));
    ss_free(&(opts.ptr_str));

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int free_rule0_str(const struct param *const pp,
                          size_t debug_level,
                          const struct free_cmod_opts *const opts,
                          vec_string **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    free_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_free_action_0(void *yyscanner,
                       const struct param *pp,
                       vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                       vec_string **x3, const YYLTYPE *loc3,
                       const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct free_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(free_opt_names) / sizeof(*(free_opt_names)));
        const char **opt_names = free_opt_names;        // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else {
            goto cmod_strin_else_36;
 cmod_strin_else_36:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (free_opt_compat(&opts, &icuropt, &ibadopt)) {   // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      free_opt_names[icuropt], free_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        free_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        (void)(*x4);
        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _34 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                const char *sep = (_34 + 1) < sv_len((*x3)) ? "\n" : "";
                if (opts.const_bool) {
                    BUFPUTS("{ (", (*(const char **)sv_at((*x3), _34)), ") = NULL; }", sep);
                }
                else {
                    BUFPUTS("{ free(", (*(const char **)sv_at((*x3), _34)), "); (",
                            (*(const char **)sv_at((*x3), _34)), ") = NULL; }", sep);
                }
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int prefix_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct prefix_cmod_opts *const opts,
                            char **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    prefix_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x3);

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_prefix_action_0(void *yyscanner,
                         struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         char **x3, const YYLTYPE *loc3,
                         const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct prefix_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(prefix_opt_names) / sizeof(*(prefix_opt_names)));
        const char **opt_names = prefix_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('u' == *((opt->name) + 0) && strncmp((opt->name) + 1, "nset", 5) == 0) {
            ret = option_list_action_bool(&opts.unset_bool, opt, &opts.unset_isset);

        }
        else {
            goto cmod_strin_else_37;
 cmod_strin_else_37:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (prefix_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      prefix_opt_names[icuropt], prefix_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        prefix_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        (void)(*x4);    // unused
        const srt_string *prefix = ss_crefs((*x3));

        if (opts.unset_bool) {  // unset prefix
            INFO("unsetting prefix `%s`", (*x3));
            if (!sm_count_s(pp->sm_prefix_su, prefix)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("cannot unset unknown prefix `%s`", (*x3));
            }
            uint64_t ix = sm_at_su(pp->sm_prefix_su, prefix);
            if (!sm_delete_u(pp->sm_prefix_us, ix) || !sm_delete_s(pp->sm_prefix_su, prefix))
                assert(0 && "C% internal error" ": " "issue with smap data structure");
        }
        else {  // set prefix
            INFO("setting prefix `%s`", (*x3));
            if (sm_count_s(pp->sm_prefix_su, prefix)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("prefix `%s` is already set", (*x3));
            }
            if (!sm_insert_su(&pp->sm_prefix_su, prefix, pp->iprefix) ||
                !sm_insert_us(&pp->sm_prefix_us, pp->iprefix, prefix))
                assert(0 && "C% internal error" ": " "issue with smap data structure");
            ++pp->iprefix;      // increase counter
        }

        /* update built-in snippet */
        struct snippet *snip = NULL;
        {
            setix _found = { 0 };
            {
                const srt_string *_ss = ss_crefs("!cmod:get_prefix");
                if (NULL == pp->shm_snip) {
                }
                _found.set = shm_atp_su(pp->shm_snip, _ss, &(_found).ix);
            }

            if (_found.set)
                snip = (void *)sv_at(pp->sv_snip, _found.ix);
        }

        if (NULL == snip)
            assert(0 && "C% internal error" ": " "built-in snippet not found");

        {       // update snippet body
            ss_clear(snip->body);
            {   // add prefix using common function (hack: with dollar separator)
                char *body = NULL;
                if (prepend_cmod_prefix(pp->sm_prefix_us, &(body), '$')) {
                    YYNOMEM;
                }
                if (ss_void == (snip->body = ss_cpy_c(&snip->body, body))) {
                    YYNOMEM;
                }
                free(body);
            }
            ss_cat_cn1(&snip->body, '\0');      // NUL-terminate
#if DEBUG
            ss_write(stderr, snip->body, 0, ss_len(snip->body));
#endif
        }
        {       // update argument placeholders
            size_t narg = sm_len(pp->sm_prefix_su);
            {
                for (size_t i = 0; i < sv_len(snip->sv_argl); ++i) {
                    const void *_item = sv_at(snip->sv_argl, i);
                    dllarg_clear((struct dllarg *)(_item));
                }
                sv_free(&(snip->sv_argl));
            }
            (snip->sv_argl) = sv_alloc(sizeof(struct dllarg), narg, NULL);
            if (sv_void == (snip->sv_argl)) {
                YYNOMEM;
            }
            const char *body = ss_to_c(snip->body);
            for (const char *rbuf = body, *c = strchr(rbuf, '$'); NULL != c; c = strchr(rbuf, '$')) {
                ptrdiff_t ix = c - body;        // index of match
                struct dllarg item = {.pos = ix,.num = 0 };     // reference to first formal argument
                if (!sv_push(&(snip->sv_argl), &(item))) {
                    YYNOMEM;
                }
                rbuf = ++c;     // jump after next match
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

static int ss_cat_shadow_macro(srt_string **buf, const struct decl *decl, const char *fname,
                               const char argv_suffix[static 1])
{

    if (decl->named) {

        if (ss_void == ss_cat_c(buf, "\n#define ")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, fname)) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, "(...) ")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, fname)) {
            return 1;
        }

        if (ss_void == ss_cat(buf, decl->fsuffix)) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, "((struct ")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, decl->name)) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, argv_suffix)) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, "){")) {
            return 1;
        }

        for (size_t i = 0; i < sv_len(decl->sv_fargs_init); ++i) {
            const char *name = sv_at_ptr(decl->sv_fargs, i);
            const char *init = sv_at_ptr(decl->sv_fargs_init, i);
            if (NULL == init)
                continue;

            if (ss_void == ss_cat_c(buf, " .")) {
                return 1;
            }

            if (ss_void == ss_cat_c(buf, name)) {
                return 1;
            }

            if (ss_void == ss_cat_c(buf, " = ")) {
                return 1;
            }

            if (ss_void == ss_cat_c(buf, init)) {
                return 1;
            }

            if (ss_void == ss_cat_cn1(buf, ',')) {
                return 1;
            }

        }
        /* add dummy initializer to allow positional arguments */

        if (ss_void == ss_cat_c(buf, " ._=0,")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, " __VA_ARGS__ })")) {
            return 1;
        }

    }
    else if (decl->ndefault > 0) {
        static const char sepvar[] = ",x";

        if (ss_void == ss_cat_c(buf, "\n#define ")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, fname)) {
            return 1;
        }

        if (ss_void == ss_cat_cn1(buf, '(')) {
            return 1;
        }

        for (size_t i = 0, z = 1; i < sv_len(decl->sv_fargs) - decl->ndefault; ++i, z = 0) {

            if (ss_void == ss_cat_c(buf, sepvar + z)) {
                return 1;
            }

            if (ss_void == ss_cat_int(buf, i)) {
                return 1;
            }

        }

        if (ss_void == ss_cat_c(buf, ") ")) {
            return 1;
        }

        if (ss_void == ss_cat_c(buf, fname)) {
            return 1;
        }

        if (ss_void == ss_cat(buf, decl->fsuffix)) {
            return 1;
        }

        if (ss_void == ss_cat_cn1(buf, '(')) {
            return 1;
        }

        /* all non-default arguments, then values for default arguments */
        for (size_t i = 0, z = 1; i < sv_len(decl->sv_fargs_init); ++i, z = 0) {
            const char *init = sv_at_ptr(decl->sv_fargs_init, i);
            if (NULL == init) {

                if (ss_void == ss_cat_c(buf, sepvar + z)) {
                    return 1;
                }

                if (ss_void == ss_cat_int(buf, i)) {
                    return 1;
                }

            }
            else {

                if (ss_void == ss_cat_cn1(buf, ',')) {
                    return 1;
                }

                if (ss_void == ss_cat_c(buf, init)) {
                    return 1;
                }

            }
        }

        if (ss_void == ss_cat_cn1(buf, ')')) {
            return 1;
        }

    }
    return 0;
}

__attribute__((unused))
static int proto_rule0_str(const struct param *const pp,
                           size_t debug_level,
                           const struct proto_cmod_opts *const opts,
                           struct decl *x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    proto_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    srt_string *tmp = decl_to_str((x3), x3->name, "", true, false);
    ss_cat(s, tmp);
    ss_free(&tmp);

    return 0;
}

int cmod_proto_action_0(void *yyscanner,
                        struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        struct decl *x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct proto_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(proto_opt_names) / sizeof(*(proto_opt_names)));
        const char **opt_names = proto_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "rgv", 4) == 0) {
            ret = option_list_action_cid(&opts.argv_cid, opt, &opts.argv_isset);

        }
        else if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egin", 5) == 0) {
            ret = option_list_action_idextl(&opts.begin_idextl, opt, &opts.begin_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_idextl(&opts.end_idextl, opt, &opts.end_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "amed", 5) == 0) {
            ret = option_list_action_bool(&opts.named_bool, opt, &opts.named_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('q' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uiet", 5) == 0) {
            ret = option_list_action_bool(&opts.quiet_bool, opt, &opts.quiet_isset);

        }
        else if ('s' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uffix", 6) == 0) {
            ret = option_list_action_cid(&opts.suffix_cid, opt, &opts.suffix_isset);

        }
        else {
            goto cmod_strin_else_38;
 cmod_strin_else_38:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (proto_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      proto_opt_names[icuropt], proto_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        proto_rule0_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        static const char argv_suffix[] = "__args";

        struct decl dproto = (*x3);
        (*x3) = (struct decl) { 0 };    // hand-over
        (dproto).named = opts.named_bool;
        (dproto).sv_begin = opts.begin_idextl;
        opts.begin_idextl = NULL;       /* hand-over */
        (dproto).sv_end = opts.end_idextl;
        opts.end_idextl = NULL; /* hand-over */

        if (ss_len(opts.argv_cid) > 0) {
            (dproto).wrapper = opts.argv_cid;
            opts.argv_cid = NULL;       // hand-over
        }
        else
            (dproto).wrapper = ss_dup_c("argv");

        if (ss_len(opts.suffix_cid) > 0) {
            (dproto).fsuffix = opts.suffix_cid;
            opts.suffix_cid = NULL;     // hand-over
        }
        else
            (dproto).fsuffix = ss_dup_c("__");

        assert(sv_len(dproto.sv_fargs) == sv_len(dproto.sv_fargs_init));
        for (size_t i = 0; i < sv_len((dproto).sv_fargs_init); ++i) {
            if (NULL != sv_at_ptr((dproto).sv_fargs_init, i))
                (dproto).ndefault++;
            else if (i > 0 && NULL != sv_at_ptr((dproto).sv_fargs_init, i - 1)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("non-default argument cannot go after default argument");
            }
        }

        if (!(dproto).named && (dproto).ndefault > 0
            && (dproto).ndefault == sv_len((dproto).sv_fargs)) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("must have at least one non-default argument, otherwise use [named]");
        }

        {       // prepend prefix
            char **name = (char **)sv_at(dproto.sv_decl, dproto.iname.ix);
            if (prepend_cmod_prefix(pp->sm_prefix_us, &(*name), '_')) {
                YYNOMEM;
            }
            dproto.name = *name;
        }

        bool found; {
            const srt_string *_ss = ss_crefs(dproto.name);
            found = shm_count_s(pp->shm_dproto, _ss);
        }

        if (found) {    // existing
            if (opts.opt_bool) {
                WARN("ignoring already-defined prototype `%s`", dproto.name);
                decl_clear(&dproto);
                goto epilogue;
            }
            else {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("cannot redefine prototype `%s`", dproto.name);
            }
        }

        if (sv_len(dproto.sv_decl) == 1 && sv_len(dproto.sv_spec) == 1 && dproto.iname.set && dproto.iname.ix == 0 && !dproto.fargs_i0.set && !dproto.fargs_i1.set) {   // simple version (typed)
            const struct c_specifier *spec = sv_at(dproto.sv_spec, 0);
            if (spec->type != ENUM_C_SPECIFIER(TYPE)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("expected type name");
            }
            const char *type_name = C_TYPESPEC_get(C_SPECIFIER_TYPE_get(*spec));
            const struct decl *dtype = NULL; {
                setix _found = { 0 };
                {
                    const srt_string *_ss = ss_crefs(type_name);
                    if (NULL == pp->shm_dtype) {
                    }
                    _found.set = shm_atp_su(pp->shm_dtype, _ss, &(_found).ix);
                }

                if (_found.set)
                    dtype = (void *)sv_at(pp->sv_dtype, _found.ix);
            }

            if (NULL == dtype) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("unknown type `%s` in prototype `%s`", type_name, dproto.name);
            }
            else if (dtype->named != dproto.named) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR("both type `%s` and prototype `%s` must be [named] or not",
                              dtype->name, dproto.name);
            }
            /* insert prototype */
            BUFPUTS(dtype->name, " ", dproto.name, ";");
            /* insert shadow macro (if needed) */
            if (ss_cat_shadow_macro(kwbuf, dtype, dproto.name, argv_suffix))
                YYNOMEM;
            /* clear prototype (not inserted into list) */
            decl_clear(&dproto);
            goto epilogue;
        }
        else if (!(dproto.fargs_i0.set && dproto.fargs_i1.set)) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("not a function declaration");
        }

#if DEBUG
        fprintf(stderr, "%s", dproto.type);
        for (size_t i = 0; i < sv_len(dproto.pointer); ++i)
            fprintf(stderr, " %s", (char *)sv_at_ptr(dproto.pointer, i));
        fprintf(stderr, " %s(", dproto.name);
        for (size_t i = 0; i < sv_len(dproto.sv_fargs); ++i) {
            const char *type = sv_at_ptr(dproto.sv_fargs_type, i);
            const char *name = sv_at_ptr(dproto.sv_fargs, i);
            const char *init = sv_at_ptr(dproto.sv_fargs_init, i);
            const vec_string *ptrs = sv_at_ptr(dproto.sv_fargs_ptrs, i);
            if (i > 0)
                fprintf(stderr, ", ");
            fprintf(stderr, "%s", type);
            for (size_t j = 0; j < sv_len(ptrs); ++j)
                fprintf(stderr, " %s", (char *)sv_at_ptr(ptrs, j));
            fprintf(stderr, " %s", name);
            if (NULL != init)
                fprintf(stderr, " = %s", init);
        }
        fprintf(stderr, ")\n");
#endif

        if (dproto.named) {     // struct-wrap arguments
            BUFPUTS("struct ", dproto.name, argv_suffix, " {\n");
            BUFPUTS("int _;\n");        // dummy member to allow positional arguments
            for (size_t i = dproto.fargs_i0.ix; i <= dproto.fargs_i1.ix; i++) {
                const char *item = sv_at_ptr(dproto.sv_decl, i);
                if (',' == item[0])
                    continue;   // skip comma
                BUFPUTS(item, ";\n");
            }
            BUFPUTS("};\n");
        }

        if (!opts.quiet_bool) {
            /* insert prototype */
            {
                srt_string *tmp = decl_to_str(&dproto, (&dproto)->name,
                                              ((&dproto)->named || (&dproto)->ndefault > 0)
                                              ? ss_to_c((&dproto)->fsuffix) : "",
                                              true, false);
                ss_cat(&((*kwbuf)), tmp);
                ss_free(&tmp);
            }
            /* insert shadow macro (if needed) */
            if (ss_cat_shadow_macro(kwbuf, &dproto, dproto.name, argv_suffix))
                YYNOMEM;
        }

        /* add to prototype list */
        {
            const srt_string *_ss = ss_crefs((dproto).name);
            if (!sv_push(&(pp->sv_dproto), &(dproto))
                || !shm_insert_su(&(pp->shm_dproto), _ss, sv_len(pp->sv_dproto) - 1)) {
                YYNOMEM;
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.argv_cid));
    ss_free(&(opts.suffix_cid));
    {
        for (size_t i = 0; i < sv_len(opts.begin_idextl); ++i) {
            const void *_item = sv_at(opts.begin_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.begin_idextl));
    }

    {
        for (size_t i = 0; i < sv_len(opts.end_idextl); ++i) {
            const void *_item = sv_at(opts.end_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.end_idextl));
    }

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int strin_rule0_str(const struct param *const pp,
                           size_t debug_level,
                           const struct strin_cmod_opts *const opts,
                           char **x3, struct table_like *x4, srt_string **x5,
                           srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    strin_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "`", *x3, "`");

    (void)x4;
    ss_cat_cn1(s, ' ');
    if (NULL != x4->lst) {      // table name list
        for (size_t i = 0; i < sv_len(x4->lst); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            hstr_str_repr(sv_at_ptr(x4->lst, i), s);
        }
    }
    else if (NULL != x4->sel) {
        ss_cat_c(s, x4->tab.name, " ");
        ss_cat_cn1(s, '(');
        for (size_t i = 0; i < sv_len(x4->sel); ++i) {
            if (i > 0)
                ss_cat_cn1(s, ',');
            namarg_str_repr(sv_at(x4->sel, i), x4->tab.sv_colnarg, s);
        }
        ss_cat_cn1(s, ')');
    }
    else {      // table
        if (x4->is_lambda) {
            if (debug_level >= 2) {
                /* TODO: print lambda table */
                ss_cat_c(s, "<lambda table>");
            }
            else
                ss_cat_c(s, "<lambda table>");
        }
        else {
            ss_cat_cn1(s, '(');
            vec_namarg_str_repr(*&(&x4->tab)->sv_colnarg, SETIX_none, SETIX_none, s);
            ss_cat_cn1(s, ')');
            ss_cat_c(s, " %" "{ ");
            if (debug_level >= 2) {
                /* TODO: print table body */
                ss_cat_c(s, "<table body>");
            }
            else
                ss_cat_c(s, "<table body>");
            ss_cat_c(s, " %" "}");
        }
    }

    (void)x5;

    if (NULL != *x5) {
        ss_cat_cn1(s, ' ');
        ss_cat_c(s, "%" "{\n");
        if (debug_level >= 2) {
            const struct snippet tmp = {
                .body = *x5,    // const borrow
                .sv_argl = pp->sv_dllarg        // const borrow
            };
            snippet_body_str_repr(&tmp, tmp.sv_argl, s);
        }
        else
            ss_cat_c(s, "<snippet body>");
        ss_cat_c(s, "%" "}");
    }

    return 0;
}

int cmod_strin_action_0(void *yyscanner,
                        const struct param *pp,
                        vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                        char **x3, const YYLTYPE *loc3,
                        struct table_like *x4, const YYLTYPE *loc4,
                        srt_string **x5, const YYLTYPE *loc5, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    (void)loc5;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct strin_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(strin_opt_names) / sizeof(*(strin_opt_names)));
        const char **opt_names = strin_opt_names;       // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ysize", 6) == 0) {
            ret = option_list_action_bool(&opts.bysize_bool, opt, &opts.bysize_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "xpand", 6) == 0) {
            ret = option_list_action_index(&opts.expand_index, opt, &opts.expand_isset);

        }
        else if ('f' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uzzy", 5) == 0) {
            ret = option_list_action_bool(&opts.fuzzy_bool, opt, &opts.fuzzy_isset);

        }
        else if ('n' == *((opt->name) + 0) && 'o' == *((opt->name) + 1)) {
            if ('m' == *((opt->name) + 2) && strncmp((opt->name) + 3, "atch", 5) == 0) {
                ret = option_list_action_idext(&opts.nomatch_idext, opt, &opts.nomatch_isset);

            }
            else if ('n' == *((opt->name) + 2) && 'e' == *((opt->name) + 3)
                     && '\0' == *((opt->name) + 4)) {
                ret = option_list_action_str(&opts.none_str, opt, &opts.none_isset);

            }
            else {
                goto cmod_strin_else_39;
            }
        }
        else {
            goto cmod_strin_else_39;
 cmod_strin_else_39:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (strin_opt_compat(&opts, &icuropt, &ibadopt)) {  // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      strin_opt_names[icuropt], strin_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        strin_rule0_str(pp, debug_level, &opts, x3, x4, x5, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        char buflabel[17 + STRUMAX_LEN];        // goto label
        srt_string *ss_label = ss_alloc_into_ext_buf(buflabel, sizeof(buflabel));
        ss_printf(&ss_label, ss_max(ss_label), "cmod_strin_else_%zu", pp->nstrin - 1);

        if (sv_len((*x4).lst) > 0) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR0("unexpected table name list");
        }
        else if (NULL != (*x4).sel && NULL == (*x4).tab.svv_rows) {
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = (*x4).tab.name;
                mutpp->errlloc = (*loc4);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match((*x4).tab.name, pp->sv_tab, sv_len(pp->sv_tab),
                                    strget_array_tab, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown table `%s`, did you mean `%s`?", (*x4).tab.name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown table `%s`", (*x4).tab.name);
                }
            }
        }

        struct table *tab = &(*x4).tab;
        {
            const vec_string *firstrow = (sv_len((tab)->svv_rows) > 0)
                ? sv_at_ptr((tab)->svv_rows, 0)
                : NULL;
            if (NULL == firstrow || sv_len(firstrow) == 0) {
                WARN("ignoring empty table `%s`", (tab)->name ? (tab)->name : "<lambda>");
                goto epilogue;
            }
        }
        size_t nargs = (NULL != (*x4).sel) ? sv_len((*x4).sel)
            : TABLE_NCOLS(*tab);
        if (nargs > 2) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
            KEYWORD_ERROR("expected up to %d columns", 2);
        }

        setix cols_ix[2] = { {0} };
        if (NULL != (*x4).sel) {
            {
                for (size_t i = 0; i < sv_len((*x4).sel); ++i) {
                    const struct namarg *narg = sv_at((*x4).sel, i);
                    if (narg->type != CMOD_ARG_ARGLINK)
                        assert(0 && "C% internal error" "");

                    size_t ix = CMOD_ARG_ARGLINK_get(*narg);
                    if (NULL == narg->name)
                        cols_ix[i] = SETIX_set(ix);
                    else if (strcmp(narg->name, "word") == 0) {
                        if (cols_ix[0].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "word";
                                mutpp->errlloc = (*loc4);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "word");
                        }
                        cols_ix[0] = SETIX_set(ix);
                    }
                    else if (strcmp(narg->name, "action") == 0) {
                        if (cols_ix[1].set) {
                            {
                                struct param *mutpp = yyget_extra(yyscanner);
                                mutpp->errtxt = "action";
                                mutpp->errlloc = (*loc4);
                                const char *errline = ss_to_c(pp->line);
                                if (NULL != pp->errtxt && NULL != errline) {
                                    bool found =
                                        errline_find_name(errline, pp->errtxt,
                                                          mutpp->errlloc.first_line,
                                                          &(mutpp->errlloc));
                                    if (found) {
                                    }
                                }
                            }
                            KEYWORD_ERROR("column `%s` already set", "action");
                        }
                        cols_ix[1] = SETIX_set(ix);
                    }
                    else {
                        {
                            struct param *mutpp = yyget_extra(yyscanner);
                            mutpp->errtxt = narg->name;
                            mutpp->errlloc = (*loc4);
                            const char *errline = ss_to_c(pp->line);
                            if (NULL != pp->errtxt && NULL != errline) {
                                bool found =
                                    errline_find_name(errline, pp->errtxt,
                                                      mutpp->errlloc.first_line, &(mutpp->errlloc));
                                if (found) {
                                }
                            }
                        }
                        KEYWORD_ERROR("unexpected special identifier named argument `%s`",
                                      narg->name);
                    }
                }
                if (!cols_ix[0].set) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR0("missing required column `" "word" "`");
                }
            }
        }
        else {
            {
                const srt_string *_ss = ss_crefs("word");
                if (NULL == (*x4).tab.shm_colnam) {
                }
                cols_ix[0].set = shm_atp_su((*x4).tab.shm_colnam, _ss, &(cols_ix[0]).ix);
            }
            {
                const srt_string *_ss = ss_crefs("action");
                if (NULL == (*x4).tab.shm_colnam) {
                }
                cols_ix[1].set = shm_atp_su((*x4).tab.shm_colnam, _ss, &(cols_ix[1]).ix);
            }
            if (!cols_ix[0].set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("missing required column `%s`", "word");
            }
        }

        bool has_rowno = false;
        const bool has_action = (NULL != (*x5));
        struct snippet snip = { 0 };
        if (!has_action) {      // get action from table
            if (!cols_ix[1].set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("missing required column `%s`", "action");
            }
        }
        else {  // define action snippet
            if (cols_ix[1].set) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                KEYWORD_ERROR("unexpected column `%s`", "action");
            }

            has_rowno = false;
            for (size_t i = 0; i < sv_len(pp->sv_dllarg) && !(has_rowno); ++i) {
                const struct dllarg *arg = sv_at(pp->sv_dllarg, i);
                if (arg->type == DLLTYPE_NR)
                    has_rowno = true;
            }

            vec_namarg *(sv_forl) = sv_alloc(sizeof(struct namarg), TABLE_NCOLS(*tab), NULL);
            if (sv_void == (sv_forl)) {
                YYNOMEM;
            }
            ;
            {
                const size_t _len = sv_len(tab->sv_colnarg);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _35 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    struct namarg item = CMOD_ARG_NOVALUE_xset(.name =
                                                               ((struct namarg *)
                                                                sv_at(tab->sv_colnarg, _35))->name);
                    if (!sv_push(&(sv_forl), &(item))) {
                        YYNOMEM;
                    }
                    // const borrow
                }
            }
            snip = (struct snippet) {
                .name = (*x3),.body = (*x5),
                .sv_forl = sv_forl,.sv_argl = pp->sv_dllarg,
                .redef = false,.nout = 0
            };
            {   // check snippet
                size_t ierr = 0;
                const char *err = NULL;
                int ret = snip_def_check(&snip, true, &ierr, &err);     // is_lambda = true
                if (ret >= 2)
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc5);
                switch (ret) {
                case 0:
                    if (ierr == 0)
                        break;
                    VERBOSE("unused arguments (%zu) in lambda snippet", ierr);
                    break;
                case 1:
                    YYNOMEM;;
                    break;
                    /* case 2 not possible for lambda snippet */
                    /* case 3 not possible for lambda snippet */
                case 4:
                    KEYWORD_ERROR("reference to undefined column `%s` in lambda snippet", err);
                    break;
                case 5:
                    KEYWORD_ERROR("%s in lambda snippet", err);
                    break;
                }
            }

        }

        size_t nrow = TABLE_NROWS(*tab);
        if (nrow > 0) { // collect words, build actions and print iftrie
            const char *words[nrow];
            char *actions[nrow];
            char *action_else = NULL;

            /* build actions */
            {
                const size_t _len = sv_len(tab->svv_rows);
                for (size_t _idx = 0; _idx < _len; ++_idx) {
                    const size_t _36 = _idx;
                    const size_t _idx = 0;
                    (void)_idx;
                    const char *word =
                        sv_at_ptr((*(const vec_string **)sv_at(tab->svv_rows, _36)), cols_ix[0].ix);

                    const char **values =
                        (const char **)
                        sv_get_buffer_r((*(const vec_string **)sv_at(tab->svv_rows, _36)));
                    assert(NULL != values);

                    size_t buflen = 0;
                    char *action = NULL;
                    if (has_action) {
                        const char *rowno = NULL;
                        if (has_rowno) {
                            char rowno_buf[STRUMAX_LEN];
                            rowno = uint_tostr(_36, rowno_buf);
                        }

                        action =
                            snip_get_buf_rowno(&snip,
                                               sv_len((*(const vec_string **)
                                                       sv_at(tab->svv_rows, _36))), values, &buflen,
                                               rowno);
                    }
                    else
                        action = sv_at_ptr((*(const vec_string **)sv_at(tab->svv_rows, _36)), cols_ix[1].ix);   // const borrow

                    words[_36] = word;  // const borrow
                    actions[_36] = action;      // store action
                }
            }

            /* build action_else */
            const char rowno[] = "-1";  // pass -1 as row number
            if (opts.nomatch_isset) {
                const struct snippet *snip = NULL;
                {
                    setix _found = { 0 };
                    if (NULL == pp->shm_snip) {
                    }
                    _found.set = shm_atp_su(pp->shm_snip, opts.nomatch_idext, &(_found).ix);

                    if (_found.set)
                        snip = (void *)sv_at(pp->sv_snip, _found.ix);
                }

                if (NULL == snip) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    KEYWORD_ERROR("[nomatch] undefined snippet `%s`", ss_to_c(opts.nomatch_idext));
                }
                if (SNIPPET_NARGOUT_NODEF(snip) > 0 || SNIPPET_NARGIN_NODEF(snip) != 1) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                    KEYWORD_ERROR
                        ("[nomatch] invalid snippet `%s` with other than one non-default input argument",
                         snip->name);
                }

                vec_namarg *sv_inpargs = NULL;
                (sv_inpargs) = sv_alloc(sizeof(struct namarg), 1, NULL);
                if (sv_void == (sv_inpargs)) {
                    YYNOMEM;
                }

                const char *txt = opts.none_isset ? ss_to_c(opts.none_str) : (*x3);
                struct namarg arg = CMOD_ARG_VERBATIM_set((char *)txt); // const borrow
                if (!sv_push(&(sv_inpargs), &(arg))) {
                    YYNOMEM;
                }

                size_t buflen = 0;
                if (snippet_subst(snip, sv_inpargs, &action_else, &buflen) || NULL == action_else)
                    YYNOMEM;

                sv_free(&sv_inpargs);   // unborrow contents
            }
            else if (has_action) {
                const char *values[] = { opts.none_isset ? ss_to_c(opts.none_str) : (*x3) };
                size_t buflen = 0;
                action_else = snip_get_buf_rowno(&snip, 1, values, &buflen, rowno);
            }

            struct iftrie_params ifp = {
                .arrlen = nrow,
                .words = words,
                .actions = (const char **)actions,
                .action_else = (const char *)action_else,
                .c2ix = loc_ascii_printable,
                .var = (*x3),   // const borrow
                .label = ss_to_c(ss_label),     // const borrow
                .nexpand = opts.expand_index.set ? opts.expand_index.ix : 3,
                .fuzzy = opts.fuzzy_bool,
                .sort_by_size = opts.bysize_bool
            };
            ss_cat_iftrie_ascii(kwbuf, &ifp);

            /* cleanup actions */
            if (has_action)
                for (size_t i = 0; i < nrow; ++i)
                    free(actions[i]);
            free(action_else);
        }

        /* cleanup */
        snip.name = NULL;       // unborrow
        snip.body = NULL;       // unborrow
        snip.sv_argl = NULL;    // unborrow
        sv_free(&snip.sv_forl); // unborrow contents
        snippet_clear(&snip);

        {
            for (size_t i = 0; i < sv_len((*x4).sel); ++i) {
                const void *_item = sv_at((*x4).sel, i);
                namarg_free((struct namarg *)_item);
            }
            sv_free(&((*x4).sel));
        }
        if (NULL == tab->name)  // free lambda table
            table_clear(tab);

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.none_str));
    ss_free(&(opts.nomatch_idext));
    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int switch_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct switch_cmod_opts *const opts,
                            vec_string **x3, vec_mcase **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    switch_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    ss_cat_cn1(s, '(');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }
    ss_cat_cn1(s, ')');

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, "%" "{ ");
    if (debug_level >= 2) {
        /* TODO: print switch body */
        ss_cat_c(s, "<switch body>");
    }
    else
        ss_cat_c(s, "<switch body>");
    ss_cat_c(s, " %" "}");

    return 0;
}

int cmod_switch_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_string **x3, const YYLTYPE *loc3,
                         vec_mcase **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct switch_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(switch_opt_names) / sizeof(*(switch_opt_names)));
        const char **opt_names = switch_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "rray", 5) == 0) {
            ret = option_list_action_bool(&opts.array_bool, opt, &opts.array_isset);

        }
        else if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "itflag", 7) == 0) {
            ret = option_list_action_bool(&opts.bitflag_bool, opt, &opts.bitflag_isset);

        }
        else if ('c' == *((opt->name) + 0) && strncmp((opt->name) + 1, "onst", 5) == 0) {
            ret = option_list_action_bool(&opts.const_bool, opt, &opts.const_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ach", 4) == 0) {
            ret = option_list_action_bool(&opts.each_bool, opt, &opts.each_isset);

        }
        else if ('i' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.int_bool, opt, &opts.int_isset);

        }
        else if ('s' == *((opt->name) + 0)) {
            if ('u' == *((opt->name) + 1) && strncmp((opt->name) + 2, "bstr", 5) == 0) {
                ret = option_list_action_bool(&opts.substr_bool, opt, &opts.substr_isset);

            }
            else if ('t' == *((opt->name) + 1) && 'r' == *((opt->name) + 2)) {
                if ('i' == *((opt->name) + 3) && 'n' == *((opt->name) + 4)
                    && 'g' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                    ret = option_list_action_bool(&opts.string_bool, opt, &opts.string_isset);

                }
                else if ('u' == *((opt->name) + 3) && 'c' == *((opt->name) + 4)
                         && 't' == *((opt->name) + 5) && '\0' == *((opt->name) + 6)) {
                    ret = option_list_action_bool(&opts.struct_bool, opt, &opts.struct_isset);

                }
                else {
                    goto cmod_strin_else_40;
                }
            }
            else {
                goto cmod_strin_else_40;
            }
        }
        else {
            goto cmod_strin_else_40;
 cmod_strin_else_40:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    if (!
        (opts.const_bool || opts.int_bool || opts.array_bool || opts.string_bool || opts.struct_bool
         || opts.bitflag_bool)) {
        opts.const_bool = true;
        opts.const_isset = true;

    }

    int icuropt = -1, ibadopt = -1;
    if (switch_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      switch_opt_names[icuropt], switch_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        switch_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        size_t narg = sv_len((*x3));
        if (narg > 2) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("expected up to two arguments");
        }
        const char *varname = sv_at_ptr((*x3), 0);
        const char *varlen = (narg > 1) ? sv_at_ptr((*x3), 1) : NULL;
        if (opts.array_bool && NULL == varlen) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("length required for array variable");
        }

        if (sv_len((*x4)) == 0) {
            WARN0("ignoring empty switch");
            goto epilogue;
        }

        const bool do_switch = opts.const_bool;
        if (do_switch)
            BUFPUTS("switch(", varname, ") {\n");

        const char *strcmpfn = (NULL != varlen) ? "strncmp" : "strcmp";
        bool has_default = false;       // has default case
        size_t idef = 0;        // default case location
        for (size_t i = 0; i < sv_len((*x4)); i++) {
            const struct mod_case *cs = sv_at((*x4), i);
            switch (cs->type) {
            case ENUM_MOD_CASE(DEFAULT):
                if (has_default) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR0("more than one default case");
                }
                has_default = true;
                idef = i;
                break;
            case ENUM_MOD_CASE(EXPRL):
                if (opts.struct_bool) {
                    ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc4);
                    KEYWORD_ERROR0("expression case not supported when switching struct");
                }
                if (NULL == cs->body) {
                    WARN0("empty case for expression label");
                    break;
                }
                const vec_string *exprl = MOD_CASE_EXPRL_get(*cs);
                if (opts.const_bool) {
                    const char *fthr = (sv_len(exprl) > 1) ? " /* FALLTHROUGH */" : "";
                    {
                        const size_t _len = sv_len(exprl);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _37 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS("case ", (*(const char **)sv_at(exprl, _37)), ":", fthr, "\n");
                        }
                    }
                }
                else if (opts.string_bool) {
                    if (opts.each_bool)
                        BUFPUTS("if(");
                    else
                        BUFPUTS((i > 0) ? "else if(" : "if(");
                    if (opts.substr_bool) {
                        {
                            const size_t _len = sv_len(exprl);
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _38 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                BUFPUTS((_38 > 0) ? "\n|| " : "",
                                        "(strstr(", varname, ",",
                                        (*(const char **)sv_at(exprl, _38)), ") != NULL)");
                            }
                        }
                    }
                    else {
                        {
                            const size_t _len = sv_len(exprl);
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _39 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if (NULL == varlen && '"' == *((*(const char **)sv_at(exprl, _39)))) {  // string literal
                                    BUFPUTS((_39 > 0) ? "\n|| " : "",
                                            "(strncmp(", varname, ",",
                                            (*(const char **)sv_at(exprl, _39)), ",sizeof(",
                                            (*(const char **)sv_at(exprl, _39)), ")) == 0)");
                                }
                                else {
                                    BUFPUTS((_39 > 0) ? "\n|| " : "",
                                            "(", strcmpfn, "(", (*(const char **)sv_at(exprl, _39)),
                                            ",", varname, (NULL != varlen) ? "," : "",
                                            (NULL != varlen) ? varlen : "", ") == 0)");
                                }
                            }
                        }
                    }
                    BUFPUTS(") {\n");
                }
                else if (opts.array_bool) {
                    BUFPUTS((i > 0) ? "else if(" : "if(");
                    {
                        const size_t _len = sv_len(exprl);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _40 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS((_40 > 0) ? "\n|| " : "",
                                    "(memcmp(", (*(const char **)sv_at(exprl, _40)), ",", varname,
                                    ",(", varlen, ") * sizeof(*(", varname, "))) == 0)");
                        }
                    }
                    BUFPUTS(") {\n");
                }
                else if (opts.int_bool) {
                    BUFPUTS((i > 0) ? "else if(" : "if(");
                    {
                        const size_t _len = sv_len(exprl);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _41 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS((_41 > 0) ? "\n|| " : "",
                                    "(", (*(const char **)sv_at(exprl, _41)), " == ", varname, ")");
                        }
                    }
                    BUFPUTS(") {\n");
                }
                else if (opts.bitflag_bool) {
                    if (opts.each_bool)
                        BUFPUTS("if(");
                    else
                        BUFPUTS((i > 0) ? "else if(" : "if(");
                    {
                        const size_t _len = sv_len(exprl);
                        for (size_t _idx = 0; _idx < _len; ++_idx) {
                            const size_t _42 = _idx;
                            const size_t _idx = 0;
                            (void)_idx;
                            BUFPUTS((_42 > 0) ? "\n|| " : "",
                                    "(((", varname, ") & (", (*(const char **)sv_at(exprl, _42)),
                                    ")) == (", (*(const char **)sv_at(exprl, _42)), "))");
                        }
                    }
                    BUFPUTS(") {\n");
                }
                /* print case body */
                if (do_switch)
                    BUFPUTS(cs->body, "\nbreak;\n");
                else
                    BUFPUTS(cs->body, "\n}\n");
                break;
            case ENUM_MOD_CASE(VACDI):
                BUFPUTS((i > 0) ? "else if(" : "if(");
                vec_string *(name) = sv_alloc_t(SV_PTR, 1);
                if (sv_void == (name)) {
                    YYNOMEM;
                }

                {
                    char *tmp = (NULL != (varname)) ? rstrdup(varname) : NULL;
                    if (NULL != (varname) && NULL == (tmp)) {
                        YYNOMEM;
                    }

                    if (!sv_push(&(name), &(tmp))) {
                        YYNOMEM;
                    }

                }
                srt_string *(buf) = ss_alloc(0);
                if (ss_void == (buf)) {
                    YYNOMEM;
                }
                ;
                const vec_arr_cdi *vacdi = MOD_CASE_VACDI_get(*cs);
                {
                    const size_t _len = sv_len(vacdi);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _43 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        ss_cat_c(&buf, (_43 > 0) ? " || " : "");
                        {
                            int ret =
                                arr_cdi_to_str(((const arr_cdi *)sv_at(vacdi, _43)), name, &buf,
                                               true);
                            const char *errstr = NULL;
                            switch (ret) {
                            case 1:
                                YYNOMEM;
                                break;
                            case 2:
                                errstr = "invalid non-designated initializer";
                                break;
                            case 3:
                                errstr = "invalid pointer designator in expression initializer";
                                break;
                            case 4:
                                errstr = "invalid type cast in non-pointer designator";
                                break;
                            default:
                                errstr = "unspecific error";
                                break;
                            }
                            if (ret)
                                KEYWORD_ERROR("%s", errstr);
                        }

                    }
                }
                BUFCAT(buf);
                BUFPUTS(") {\n", cs->body, "\n}\n");
                ss_free(&buf);
                {
                    for (size_t i = 0; i < sv_len(name); ++i) {
                        const void *_item = sv_at(name, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(name));
                }
                break;
            }
        }

        if (has_default) {      // print default case
            if (opts.bitflag_bool) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
                KEYWORD_ERROR0("[bitflag] does not allow a default case");
            }
            const struct mod_case *cs = sv_at((*x4), idef);
            if (do_switch)
                BUFPUTS("default:\n", cs->body, "\nbreak;\n");
            else
                BUFPUTS("else {\n", cs->body, "\n}\n"); // print default case body
        }
        else if (!opts.bitflag_bool)
            VERBOSE0("no default case");

        if (do_switch)
            BUFPUTS("}\n");

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int typedef_rule0_str(const struct param *const pp,
                             size_t debug_level,
                             const struct typedef_cmod_opts *const opts,
                             vec_string **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    typedef_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_typedef_action_0(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          vec_string **x3, const YYLTYPE *loc3,
                          const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct typedef_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(typedef_opt_names) / sizeof(*(typedef_opt_names)));
        const char **opt_names = typedef_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "rgv", 4) == 0) {
            ret = option_list_action_cid(&opts.argv_cid, opt, &opts.argv_isset);

        }
        else if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egin", 5) == 0) {
            ret = option_list_action_idextl(&opts.begin_idextl, opt, &opts.begin_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_idextl(&opts.end_idextl, opt, &opts.end_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "amed", 5) == 0) {
            ret = option_list_action_bool(&opts.named_bool, opt, &opts.named_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('q' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uiet", 5) == 0) {
            ret = option_list_action_bool(&opts.quiet_bool, opt, &opts.quiet_isset);

        }
        else if ('s' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uffix", 6) == 0) {
            ret = option_list_action_cid(&opts.suffix_cid, opt, &opts.suffix_isset);

        }
        else {
            goto cmod_strin_else_41;
 cmod_strin_else_41:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (typedef_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      typedef_opt_names[icuropt], typedef_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        typedef_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        (void)(*x4);    // unused

        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _44 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                char *type_name = *((char **)sv_at((*x3), _44));
                *((char **)sv_at((*x3), _44)) = NULL;   // hand-over

                bool found; {
                    const srt_string *_ss = ss_crefs(type_name);
                    found = shm_count_s(pp->shm_dtype, _ss);
                }

                if (found) {    // existing
                    WARN("ignoring already registered type `%s`", type_name);
                    {
                        free(type_name);
                        (type_name) = NULL;
                    }
                    continue;
                }

                vec_string *(sv_decl) = sv_alloc_t(SV_PTR, 1);
                if (sv_void == (sv_decl)) {
                    YYNOMEM;
                }
                ;
                if (!sv_push(&(sv_decl), &(type_name))) {
                    YYNOMEM;
                }

                struct decl dtype = {
                    .named = opts.named_bool,
                    .sv_decl = sv_decl,
                    .iname = SETIX_set(0),
                    .name = type_name
                };

                /* add to type list */
                {
                    const srt_string *_ss = ss_crefs((dtype).name);
                    if (!sv_push(&(pp->sv_dtype), &(dtype))
                        || !shm_insert_su(&(pp->shm_dtype), _ss, sv_len(pp->sv_dtype) - 1)) {
                        YYNOMEM;
                    }
                }
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.argv_cid));
    ss_free(&(opts.suffix_cid));
    {
        for (size_t i = 0; i < sv_len(opts.begin_idextl); ++i) {
            const void *_item = sv_at(opts.begin_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.begin_idextl));
    }

    {
        for (size_t i = 0; i < sv_len(opts.end_idextl); ++i) {
            const void *_item = sv_at(opts.end_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.end_idextl));
    }

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int typedef_rule1_str(const struct param *const pp,
                             size_t debug_level,
                             const struct typedef_cmod_opts *const opts,
                             struct decl *x3, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    typedef_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    srt_string *tmp = decl_to_str((x3), x3->name, "", true, false);
    ss_cat(s, tmp);
    ss_free(&tmp);

    return 0;
}

int cmod_typedef_action_1(void *yyscanner,
                          struct param *pp,
                          vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                          struct decl *x3, const YYLTYPE *loc3, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    if (pp->const_rsrc)
        KEYWORD_ERROR0("creating or modifying resources is not allowed in this sub-parse");

    struct typedef_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(typedef_opt_names) / sizeof(*(typedef_opt_names)));
        const char **opt_names = typedef_opt_names;     // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('a' == *((opt->name) + 0) && strncmp((opt->name) + 1, "rgv", 4) == 0) {
            ret = option_list_action_cid(&opts.argv_cid, opt, &opts.argv_isset);

        }
        else if ('b' == *((opt->name) + 0) && strncmp((opt->name) + 1, "egin", 5) == 0) {
            ret = option_list_action_idextl(&opts.begin_idextl, opt, &opts.begin_isset);

        }
        else if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else if ('e' == *((opt->name) + 0) && 'n' == *((opt->name) + 1) && 'd' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_idextl(&opts.end_idextl, opt, &opts.end_isset);

        }
        else if ('n' == *((opt->name) + 0) && strncmp((opt->name) + 1, "amed", 5) == 0) {
            ret = option_list_action_bool(&opts.named_bool, opt, &opts.named_isset);

        }
        else if ('o' == *((opt->name) + 0) && 'p' == *((opt->name) + 1) && 't' == *((opt->name) + 2)
                 && '\0' == *((opt->name) + 3)) {
            ret = option_list_action_bool(&opts.opt_bool, opt, &opts.opt_isset);

        }
        else if ('q' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uiet", 5) == 0) {
            ret = option_list_action_bool(&opts.quiet_bool, opt, &opts.quiet_isset);

        }
        else if ('s' == *((opt->name) + 0) && strncmp((opt->name) + 1, "uffix", 6) == 0) {
            ret = option_list_action_cid(&opts.suffix_cid, opt, &opts.suffix_isset);

        }
        else {
            goto cmod_strin_else_42;
 cmod_strin_else_42:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (typedef_opt_compat(&opts, &icuropt, &ibadopt)) {        // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      typedef_opt_names[icuropt], typedef_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        typedef_rule1_str(pp, debug_level, &opts, x3, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        static const char argv_suffix[] = "__args";

        struct decl dtype = (*x3);
        (*x3) = (struct decl) { 0 };    // hand-over

        const bool is_function_type = (dtype.fargs_i0.set && dtype.fargs_i1.set);
        if (!is_function_type && opts.end_isset) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = LOC_OPTLIST;
            KEYWORD_ERROR("[end] makes no sense for non-function type `%s`", dtype.name);
        }

        (dtype).named = opts.named_bool;
        (dtype).sv_begin = opts.begin_idextl;
        opts.begin_idextl = NULL;       /* hand-over */
        (dtype).sv_end = opts.end_idextl;
        opts.end_idextl = NULL; /* hand-over */

        if (ss_len(opts.argv_cid) > 0) {
            (dtype).wrapper = opts.argv_cid;
            opts.argv_cid = NULL;       // hand-over
        }
        else
            (dtype).wrapper = ss_dup_c("argv");

        if (ss_len(opts.suffix_cid) > 0) {
            (dtype).fsuffix = opts.suffix_cid;
            opts.suffix_cid = NULL;     // hand-over
        }
        else
            (dtype).fsuffix = ss_dup_c("__");

        assert(sv_len(dtype.sv_fargs) == sv_len(dtype.sv_fargs_init));
        for (size_t i = 0; i < sv_len((dtype).sv_fargs_init); ++i) {
            if (NULL != sv_at_ptr((dtype).sv_fargs_init, i))
                (dtype).ndefault++;
            else if (i > 0 && NULL != sv_at_ptr((dtype).sv_fargs_init, i - 1)) {
                ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
                KEYWORD_ERROR0("non-default argument cannot go after default argument");
            }
        }

        if (!(dtype).named && (dtype).ndefault > 0 && (dtype).ndefault == sv_len((dtype).sv_fargs)) {
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc3);
            KEYWORD_ERROR0("must have at least one non-default argument, otherwise use [named]");
        }

        /* NOTE: duplicate check not necessary since C_IDENTIFIER becomes C_TYPEDEF_NAME */

        if (is_function_type && opts.named_bool) {      // named function type
            BUFPUTS("struct ", dtype.name, argv_suffix, " {\n");
            BUFPUTS("int _;\n");        // dummy member to allow positional arguments
            for (size_t i = dtype.fargs_i0.ix; i <= dtype.fargs_i1.ix; i++) {
                const char *item = sv_at_ptr(dtype.sv_decl, i);
                if (item[0] != ',') {
                    BUFPUTS(item, ";\n");
                }
            }
            BUFPUTS("};\n");
        }
        if (!opts.quiet_bool) {
            BUFPUTS("typedef ");
            {
                srt_string *tmp = decl_to_str(&dtype, (&dtype)->name, "", true, false);
                ss_cat(&((*kwbuf)), tmp);
                ss_free(&tmp);
            }
        }

        /* add to type list */
        {
            const srt_string *_ss = ss_crefs((dtype).name);
            if (!sv_push(&(pp->sv_dtype), &(dtype))
                || !shm_insert_su(&(pp->shm_dtype), _ss, sv_len(pp->sv_dtype) - 1)) {
                YYNOMEM;
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&(opts.argv_cid));
    ss_free(&(opts.suffix_cid));
    {
        for (size_t i = 0; i < sv_len(opts.begin_idextl); ++i) {
            const void *_item = sv_at(opts.begin_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.begin_idextl));
    }

    {
        for (size_t i = 0; i < sv_len(opts.end_idextl); ++i) {
            const void *_item = sv_at(opts.end_idextl, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.end_idextl));
    }

    ss_free(&str_repr);

    return _ret;
}

__attribute__((unused))
static int unused_rule0_str(const struct param *const pp,
                            size_t debug_level,
                            const struct unused_cmod_opts *const opts,
                            vec_string **x3, const char **x4, srt_string *s[static 1])
{
    (void)pp;
    (void)debug_level;
    if (NULL == *s) {   // allocate
        srt_string *(tmp) = ss_alloc(32);
        if (ss_void == (tmp)) {
            return 1;
        }

        *s = tmp;
    }
    unused_opt_string(opts, s);
    (void)x3;
    ss_cat_cn1(s, ' ');
    for (size_t i = 0; i < sv_len(*x3); ++i) {
        if (i > 0)
            ss_cat_cn1(s, ',');
        const char *x = sv_at_ptr(*x3, i);
        ss_cat_c(s, x);
    }

    (void)x4;
    ss_cat_cn1(s, ' ');
    ss_cat_c(s, *x4);
    return 0;
}

int cmod_unused_action_0(void *yyscanner,
                         const struct param *pp,
                         vec_cmopt **oplist, const YYLTYPE *loc_optlist,
                         vec_string **x3, const YYLTYPE *loc3,
                         const char **x4, const YYLTYPE *loc4, bool inactive, srt_string **kwbuf)
{
    (void)kwbuf;        // NOTE: remove this to know which keywords do not produce output
    (void)loc3;
    (void)loc4;
    int _ret = 0;       // return code
    srt_string *str_repr = NULL;        // string representation

    struct unused_cmod_opts opts = { 0 };
    for (size_t i = 0; i < sv_len((*oplist)); ++i) {
        const size_t nopt = (sizeof(unused_opt_names) / sizeof(*(unused_opt_names)));
        const char **opt_names = unused_opt_names;      // static array
        struct cmod_option *opt = (struct cmod_option *)sv_at((*oplist), i);
        int ret = 0;
        if ('d' == *((opt->name) + 0) && strncmp((opt->name) + 1, "ebug", 5) == 0) {
            ret = option_list_action_count(&opts.debug_count, opt, &opts.debug_isset);

        }
        else {
            goto cmod_strin_else_43;
 cmod_strin_else_43:;
            {
                struct param *mutpp = yyget_extra(yyscanner);
                mutpp->errtxt = opt->name;
                mutpp->errlloc = (*loc_optlist);
                const char *fuzzy_match;
                bool is_close;
                is_close =
                    str_fuzzy_match(opt->name, opt_names, nopt, strget_array, 0.75, &(fuzzy_match));

                if (NULL != fuzzy_match && is_close) {
                    KEYWORD_ERROR("unknown option `%s`, did you mean `%s`?", opt->name,
                                  fuzzy_match);
                }
                else {
                    KEYWORD_ERROR("unknown option `%s`", opt->name);
                }
            }

        }
        if (ret > 1)
            ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        switch (ret) {
        case 1:
            YYNOMEM;
            break;
        case 2:
            KEYWORD_ERROR("wrong type or invalid value for option `%s`", opt->name);
            break;
        case 3:
            KEYWORD_ERROR("option `%s` is already set, use += or := instead", opt->name);
            break;
        case 4:
            KEYWORD_ERROR("out of bounds value in option `%s`", opt->name);
            break;
        case 5:
            KEYWORD_ERROR("option `%s` takes a single value, cannot use +=", opt->name);
            break;
        }
        if (':' == opt->assign)
            VERBOSE("overriding previously set option `%s`", opt->name);
    }

    int icuropt = -1, ibadopt = -1;
    if (unused_opt_compat(&opts, &icuropt, &ibadopt)) { // check option compatibility
        ((struct param *)yyget_extra(yyscanner))->errlloc = (*loc_optlist);
        KEYWORD_ERROR("incompatible options [%s] and [%s]",
                      unused_opt_names[icuropt], unused_opt_names[ibadopt]);
    }

    int debug_level =
        ((pp->debug) > ((int)opts.debug_count)) ? (pp->debug) : ((int)opts.debug_count);
    if (debug_level > 0) {      // print string representation
        srt_string *dbg_repr = ss_dup_c(inactive ? "[inactive] " : "");
        ss_cat_c(&dbg_repr, pp->kwname);
        unused_rule0_str(pp, debug_level, &opts, x3, x4, &dbg_repr);

        debug_byline(dbg_repr, __func__);
        ss_free(&dbg_repr);
    }

    {   // keyword action

        if (inactive)
            goto epilogue;

        {
            const size_t _len = sv_len((*x3));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _45 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if (_45 > 0)
                    BUFPUTC('\n');
                BUFPUTS("(void)", (*(char **)sv_at((*x3), _45)), (*x4));
            }
        }

        goto epilogue;  // silence unused label warning
 epilogue:
        ;       // no-op

    }

    goto cleanup;       // silence unused label warning;
 cleanup:

    ss_free(&str_repr);

    return _ret;
}
