#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#ifndef TABLE_PARSE_h_
#define TABLE_PARSE_h_
#include "cmod.ext.h"
#include "parts/variant_cmod_option.h"
enum table_parse {
    TABLE_PARSE_OK = 0, /* default */
    TABLE_PARSE_OOM = 1,        /* out of memory */
    TABLE_PARSE_BADROW = 2,     /* incorrect row size */
    TABLE_PARSE_NDEFAULT = 3,   /* bad number of non-default columns */
};
#define enum_table_parse_len 3

int (table_lambda_parse) (vec_vec_string * lambda_table[static 1], size_t ncol_o[static 1],
                          vec_vec_string * rows_o[static 1]);
int (table_firstrow_colnames) (vec_vec_string * svv_rows[static 1],
                               vec_string * colnarg_o[static 1], size_t nnondef_o[static 1]);
enum table_fmt {
    TABLE_FMT_TSV,
    TABLE_FMT_JSON,
    TABLE_FMT_LAMBDA
};
union table_body {
    const srt_string *verbatim;
    vec_vec_string **lambda;
};
int (table_build) (union table_body tbody, vec_namarg * sv_colnarg, enum table_fmt format,
                   struct table tab[static 1], size_t ierr[static 1]);
#endif

#include <assert.h>
#include <regex.h>
#include "table_tsv.h"
#include "table_json.h"
#include "util_string.h"

int table_lambda_parse(vec_vec_string *lambda_table[static 1], size_t ncol_o[static 1],
                       vec_vec_string *rows_o[static 1])
{
    int rc = TABLE_PARSE_OK;

    size_t ncol = sv_len(*lambda_table);
    const vec_string *firstcol = sv_at_ptr(*lambda_table, 0);
    size_t nrow = sv_len(firstcol);

    vec_vec_string *(svv_rows) = sv_alloc_t(SV_PTR, nrow);
    if (sv_void == (svv_rows)) {
        rc = TABLE_PARSE_OOM;
        goto cleanup;
    }

    for (size_t i = 0; i < nrow; ++i) { // init rows
        vec_string *(row) = sv_alloc_t(SV_PTR, ncol);
        if (sv_void == (row)) {
            rc = TABLE_PARSE_OOM;
            goto cleanup;
        }
        ;
        if (!sv_push(&(svv_rows), &(row))) {
            rc = TABLE_PARSE_OOM;
            goto cleanup;
        }

    }

    for (size_t j = 0; j < ncol; ++j) { // fill entries
        vec_string **row_lambda = (vec_string **) sv_at(*lambda_table, j);
        for (size_t i = 0; i < nrow; ++i) {
            vec_string **row = (vec_string **) sv_at(svv_rows, i);      // DO NOT remove a star
            sv_set(row, j, sv_at(*row_lambda, i));      // transposed
        }
        sv_free(row_lambda);
    }
    sv_free(lambda_table);

    *ncol_o = ncol;
    *rows_o = svv_rows;
    svv_rows = NULL;    // hand-over
 cleanup:
    sv_free(&svv_rows);
    return rc;
}

int table_firstrow_colnames(vec_vec_string *svv_rows[static 1], vec_string *colnarg_o[static 1],
                            size_t nnondef_o[static 1])
{
    int rc = 0; // return code

    vec_string *firstrow = sv_at_ptr(*svv_rows, 0);
    vec_namarg *sv_colnarg = NULL;
    (sv_colnarg) = sv_alloc(sizeof(struct namarg), sv_len(firstrow), NULL);
    if (sv_void == (sv_colnarg)) {
        rc = 1;
        goto cleanup;
    }

    {
        const size_t _len = sv_len(firstrow);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _0 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            if (!strisidext(*((char **)sv_at(firstrow, _0)))) {
                rc = 4;
                goto cleanup;
            }   // ERROR: not an extended identifier
            struct namarg colnarg = CMOD_ARG_NOVALUE_xset(.name = *((char **)sv_at(firstrow, _0)));
            if (!sv_push(&(sv_colnarg), &(colnarg))) {
                rc = 1;
                goto cleanup;
            }

            *(((char **)sv_at(firstrow, _0))) = NULL;   // hand-over
        }
    }
    sv_free(&firstrow); // clear
    /* remove first row from vector */
    vec_vec_string *svv_rows_new = NULL;
    (svv_rows_new) = sv_alloc_t(SV_PTR, sv_len(*svv_rows) - 1);
    if (sv_void == (svv_rows_new)) {
        rc = 1;
        goto cleanup;
    }

    for (size_t i = 1; i < sv_len(*svv_rows); ++i) {
        const vec_string *row = sv_at_ptr(*svv_rows, i);
        if (!sv_push(&(svv_rows_new), &(row))) {
            rc = 1;
            goto cleanup;
        }
        // hand-over
    }
    sv_free(svv_rows);

    *svv_rows = svv_rows_new;
    *colnarg_o = sv_colnarg;
    *nnondef_o = sv_len(sv_colnarg);
    sv_colnarg = NULL;  // hand-over
 cleanup:
    {
        for (size_t i = 0; i < sv_len(sv_colnarg); ++i) {
            const void *_item = sv_at(sv_colnarg, i);
            namarg_free((struct namarg *)_item);
        }
        sv_free(&(sv_colnarg));
    }

    return rc;
}

int table_build(union table_body tbody, vec_namarg *sv_colnarg, enum table_fmt format,
                struct table tab[static 1], size_t ierr[static 1])
{
    const size_t ncolnam = sv_len(sv_colnarg);
    assert(ncolnam > 0);        // we assume at least one column
    tab->sv_colnarg = sv_colnarg;

    size_t ndefault = 0;        // number of columns with default value
    /* count default columns */
    {
        const size_t _len = sv_len(sv_colnarg);
        for (size_t _idx = 0; _idx < _len; ++_idx) {
            const size_t _1 = _idx;
            const size_t _idx = 0;
            (void)_idx;
            char *defval = NULL;
            switch (((struct namarg *)sv_at(sv_colnarg, _1))->type) {
            case CMOD_ARG_NOVALUE:
                break;  // skip non-default column
            case ENUM_NAMARG(VERBATIM):        /* FALLTHROUGH */
                defval = CMOD_ARG_VERBATIM_get(*((struct namarg *)sv_at(sv_colnarg, _1)));
                *((struct namarg *)sv_at(sv_colnarg, _1)) = CMOD_ARG_VERBATIM_xset(defval,.name = ((struct namarg *)sv_at(sv_colnarg, _1))->name);      // change type
                ++ndefault;
                break;
            case ENUM_NAMARG(INTSTR):  /* FALLTHROUGH */
                defval = CMOD_ARG_INTSTR_get(*((struct namarg *)sv_at(sv_colnarg, _1)));
                *((struct namarg *)sv_at(sv_colnarg, _1)) = CMOD_ARG_VERBATIM_xset(defval,.name = ((struct namarg *)sv_at(sv_colnarg, _1))->name);      // change type
                ++ndefault;
                break;
            default:   // already checked in parser
                assert(0 && "C% internal error" "");
                break;
            }
        }
    }
    tab->nnondef = ncolnam - ndefault;  // number of non-default columns

    int ret = TABLE_PARSE_OK;
    size_t tab_ncol = 0;
    switch (format) {
    case TABLE_FMT_TSV:
        if (NULL != tbody.verbatim) {
            const char *tbody_buf = ss_get_buffer_r(tbody.verbatim);
            const size_t tbody_len = ss_len(tbody.verbatim);
            enum tsv_parse _ret = table_tsv_parse(tbody_buf, tbody_len, &tab_ncol, &tab->svv_rows);
            switch (_ret) {     // map return codes
            case TSV_PARSE_OOM:
                ret = TABLE_PARSE_OOM;
                break;
            case TSV_PARSE_BADROW:
                ret = TABLE_PARSE_BADROW;
                break;
            case TSV_PARSE_NDEFAULT:
                ret = TABLE_PARSE_NDEFAULT;
                break;
            default:
                ret = _ret;
                break;
            }
            if (TABLE_PARSE_OK != ret)
                return ret;
            if (sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
                *ierr = tab_ncol;
                return TABLE_PARSE_NDEFAULT;
            }
        }
        break;
    case TABLE_FMT_JSON:
        if (NULL != tbody.verbatim) {
            vec_namarg *json_colnam = NULL;
            const char *tbody_buf = ss_get_buffer_r(tbody.verbatim);
            const size_t tbody_len = ss_len(tbody.verbatim);
            enum json_parse _ret =
                table_json_parse(tbody_buf, tbody_len, &tab_ncol, &tab->svv_rows, &json_colnam,
                                 ierr);
            switch (_ret) {     // map return codes
            case JSON_PARSE_OOM:
                ret = TABLE_PARSE_OOM;
                break;
            case JSON_PARSE_BADROW:
                ret = TABLE_PARSE_BADROW;
                break;
            case JSON_PARSE_NDEFAULT:
                ret = TABLE_PARSE_NDEFAULT;
                break;
            default:
                ret = _ret;
                break;
            }
            if (TABLE_PARSE_OK != ret)
                return ret;
            if (sv_len(json_colnam) > 0) {      // check column names extracted from JSON
                enum json_parse _ret =
                    table_json_colnam_preset(tab->svv_rows, json_colnam, tab->nnondef, sv_colnarg,
                                             ierr);
                switch (_ret) { // map return codes
                case JSON_PARSE_OOM:
                    ret = TABLE_PARSE_OOM;
                    break;
                case JSON_PARSE_BADROW:
                    ret = TABLE_PARSE_BADROW;
                    break;
                case JSON_PARSE_NDEFAULT:
                    ret = TABLE_PARSE_NDEFAULT;
                    break;
                default:
                    ret = _ret;
                    break;
                }
                if (TABLE_PARSE_OK != ret)
                    return ret;
                {
                    for (size_t i = 0; i < sv_len(json_colnam); ++i) {
                        const void *_item = sv_at(json_colnam, i);
                        namarg_free((struct namarg *)_item);
                    }
                    sv_free(&(json_colnam));
                }
            }
            if (sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
                *ierr = tab_ncol;
                return TABLE_PARSE_NDEFAULT;
            }
        }
        break;
    case TABLE_FMT_LAMBDA:
        ret = table_lambda_parse(tbody.lambda, &tab_ncol, &tab->svv_rows);
        if (TABLE_PARSE_OK != ret)
            return ret;
        if (sv_len(tab->svv_rows) > 0 && tab->nnondef != tab_ncol) {
            *ierr = tab_ncol;
            return TABLE_PARSE_NDEFAULT;
        }
        break;
    }
    sv_shrink(&tab->svv_rows);  // free unused space

    return ret;
}
