include config.mk

# Command paths (user-configurable)
CC := gcc --std=c99
RM := rm -f
LEX := flex
YACC := bison
LN := ln
GIT := git
INSTALL := install

# Compilation flags
ENV_CFLAGS := $(CFLAGS)
ENV_CPPFLAGS := $(CPPFLAGS)

WFLAGS := -Wall -Wextra -Werror -pedantic -pedantic-errors
# these flags required by C%'s use of compound literals
WFLAGS += -Wno-error=override-init -Wno-error=override-init-side-effects
# do not fail due to compiler not supporting warning options
WFLAGS += -Wno-unknown-warning-option
CFLAGS = $(WFLAGS) $(OPTIM_FLAGS) $(ENV_CFLAGS)
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -D_BSD_SOURCE -D_DEFAULT_SOURCE -I. -Iinclude $(ENV_CPPFLAGS)

# Shell used by make (usually no need to change)
SHELL := /bin/bash

# DO NOT TOUCH anything beyond this point
wrap = $(addsuffix $3,$(addprefix $1,$2))

ifeq (Darwin,$(DETECTED_OS))
PREFIX ?= /usr/local
else
PREFIX ?= /usr
endif
BINDIR ?= $(PREFIX)/bin
INCLUDEDIR ?= $(PREFIX)/include
DATAROOTDIR ?= $(PREFIX)/share
VIMDIR ?= $(HOME)/.vim

STDLIB := $(wildcard stdlib/*.hm)
# NOTE: order matters!
SRC_C_FILES := validate_utf8.c ralloc.c util_scanner.c cmod.ext.c \
			   autotypes.c array_type.c util_string.c util_snippet.c \
			   table_parse.c table_json.c table_tsv.c cgrammar.c \
			   dsl_parser.c dsl_scanner.c dsl.ext.c \
			   parser.c scanner.c parser_api.c filestack.c \
			   docs.c cmod_pp.c cmod.c

UTF8_OBJS := utf8/naive.o utf8/range-sse.o utf8/range-avx2.o utf8/range-neon.o
OBJS := $(SRC_C_FILES:.c=.o) $(UTF8_OBJS)

ifeq (1,$(HAVE_GNU_SOURCE))
CPPFLAGS += -D_GNU_SOURCE
endif

ifeq (1,$(HAVE_LIBSECCOMP))
CPPFLAGS += -DHAVE_LIBSECCOMP=1
LDLIBS += $(LIBSECCOMP_LIBS)
endif

default: git-init all

# Git stuff
.PHONY: git-init
git-init: .git_init_run
.git_init_run:
	$(GIT) submodule update --init
	touch $@

# Build stuff
.PHONY: all
all: cmod check test

## libsrt
LIBSRT_DEPS ?= libsrt/libsrt.a
LIBSRT_CC_FLAGS = CC=gcc C99=1 PEDANTIC=1 ADD_CFLAGS="-DS_CRC32_SLC=0 $(LIBSRT_CC_ADDCFLAGS)"
LIBSRT_CC_ADDCFLAGS := -Wno-error=unused-function

.PHONY: libsrt
libsrt: libsrt.a

libsrt/libsrt.a:
	cd libsrt \
		&& $(MAKE) -f Makefile.posix $(LIBSRT_CC_FLAGS) libsrt.a

libsrt.a: $(LIBSRT_DEPS)
	$(LN) -sf $< $@

clean-libsrt:
	cd libsrt \
		&& $(MAKE) -f Makefile.posix clean
	$(RM) libsrt.a

## utf8 validator
UTF8_CFLAGS := -Wall -march=native
ifneq (,$(DEBUG)) # add debug info
UTF8_CFLAGS += -g -O0
else
UTF8_CFLAGS += -O3
endif
$(UTF8_OBJS): utf8/%:
	cd utf8 && $(MAKE) CPPFLAGS="$(UTF8_CFLAGS)" $*

## cmod
cmod: $(OBJS) libsrt.a

# C dependencies (automatic)
include deps.mk
deps.mk:
	$(CC) -MM $(CPPFLAGS) $(SRC_C_FILES) > $@

# File-specific flags
dsl_parser.o: CFLAGS += -Wno-error=format-security -Wno-error=unused-but-set-variable
parser.o: CFLAGS += -Wno-error=format-security -Wno-error=unused-but-set-variable

# Clean stuff
.PHONY: clean clean-sub clean-all
clean:
	$(RM) cmod $(OBJS)
	$(RM) unittest/test_*.exe
	$(RM) *~ vgcore.*

clean-sub: clean-libsrt
	cd src && $(MAKE) clean
	cd meta && $(MAKE) clean

clean-all: clean-sub clean

# Test stuff
.PHONY: test
TEST_CMOD ?= $(realpath ./cmod)
TEST_INC ?= -I$(realpath ./include)
TEST_SILENT ?= -s
test-parallel: TEST_PARALLEL ?= -j
test: test-parallel
	@cd tests && $(MAKE) $(TEST_SILENT) serial CMOD=$(TEST_CMOD) CMODINC=$(TEST_INC)
test-%:
	@cd tests && $(MAKE) $(TEST_SILENT) $(TEST_PARALLEL) $* CMOD=$(TEST_CMOD) CMODINC=$(TEST_INC)

ifneq (,$(TESTFILE))
test-add-pass test-add-fail: test-%:
	@cd tests && $(MAKE) $(TEST_SILENT) $* TESTFILE="$(realpath $(TESTFILE))" CMOD=$(TEST_CMOD)
endif # TESTFILE

# Unit test stuff
SRC_TESTABLE_FILES := autotypes.c cmod.ext.c table_json.c table_tsv.c util_string.c \
					  dsl.ext.c

unittest_exe = $(call wrap,unittest/test_,$1,.exe)
UNITTEST_EXECS := $(call unittest_exe,$(SRC_TESTABLE_FILES))

$(UNITTEST_EXECS): $(call unittest_exe,%.c): unittest/%.c

TESTLIB_OBJ := ralloc.o autotypes.o array_type.o util_string.o util_snippet.o \
			   cmod.ext.o cgrammar.o
unittest/testlib.a: unittest/testlib.a($(TESTLIB_OBJ))
	ranlib $@

$(call unittest_exe,autotypes.c):	
$(call unittest_exe,cmod.ext.c):	
$(call unittest_exe,table_json.c):	table_parse.o table_json.o table_tsv.o scanner.o filestack.o util_scanner.o
$(call unittest_exe,table_tsv.c):	table_tsv.o scanner.o filestack.o util_scanner.o
$(call unittest_exe,util_string.c):	
$(call unittest_exe,dsl.ext.c):		dsl.ext.o dsl_scanner.o dsl_parser.o

$(UNITTEST_EXECS): unittest/testlib.a libsrt.a
$(UNITTEST_EXECS): CFLAGS += -Iinclude
$(UNITTEST_EXECS): CRITERION_LIBS ?= -lcriterion
$(UNITTEST_EXECS):
	$(CC) -I. $(CFLAGS) $(CPPFLAGS) -o $@ $^ $(LDLIBS) $(CRITERION_LIBS)

.PHONY: check check-verbose check-memcheck
ifeq (1,$(HAVE_CRITERION))
check-verbose check-memcheck: check
check-verbose: CHECK_FLAGS := --verbose
check-memcheck: CHECK_MEMCHECK = $(MEMCHECK)

check-exec-%: $(call unittest_exe,%)
	$(CHECK_MEMCHECK) $< $(CHECK_FLAGS)

check: $(addprefix check-exec-,$(SRC_TESTABLE_FILES))
else  # HAVE_CRITERION
check:
	$(warning 'WARN: Criterion framework disabled, unit tests not available')
endif # HAVE_CRITERION

# Install stuff
.PHONY: install-bin install-vim install-inc install
ifeq (Darwin,$(DETECTED_OS))
install-bin: INSTALL_LINE = -m755 $< $(BINDIR)/cmod
install-vim: INSTALL_LINE = -m644 $< $(VIMDIR)/syntax
install-include: INSTALL_LINE = -m644 $^ $(INCLUDEDIR)/cmod
install-share: INSTALL_LINE = -m644 $^ $(DATAROOTDIR)/cmod
else
install-bin: INSTALL_LINE = -Dm755 $< $(BINDIR)/cmod
install-vim: INSTALL_LINE = -Dm644 $< $(VIMDIR)/syntax
install-include: INSTALL_LINE = -Dm644 -t$(INCLUDEDIR)/cmod $^
install-share: INSTALL_LINE = -Dm644 -t$(DATAROOTDIR)/cmod $^
endif

install-bin: cmod
	$(INSTALL) $(INSTALL_LINE)
	strip $(BINDIR)/cmod

install-vim: cmod.vim
	$(INSTALL) $(INSTALL_LINE)

install-include: $(STDLIB)
	$(INSTALL) -d $(INCLUDEDIR)/cmod
	$(INSTALL) $(INSTALL_LINE)

install-share: cmod.mk
	$(INSTALL) -d $(DATAROOTDIR)/cmod
	$(INSTALL) $(INSTALL_LINE)

install: install-bin install-include install-share
install-all: install install-vim


# Maintainer stuff
GAWK := gawk
COLUMN_CMD := column
COLUMN_SEP := 

SRC_H_FILES := $(wildcard src/*.h)
SRC_H_PARTS_FILES := $(wildcard src/parts/*.h)

ifneq (,$(DEBUG)) # no optim, add debug info
NO_OPTIM := 1
CFLAGS += -ggdb3
ifeq (all,$(DEBUG))
CPPFLAGS += -DDEBUG=1 -DYYDEBUG=1
endif # DEBUG == all
else  # !DEBUG
CPPFLAGS += -DNDEBUG
endif # DEBUG

ifeq (1,$(GPROF))
CFLAGS += -pg
LDFLAGS += -pg
endif # GPROF

ifeq (1,$(GCOV))
CFLAGS += -fprofile-arcs -ftest-coverage
LDFLAGS += -fprofile-arcs -ftest-coverage
endif # GCOV

ifeq (1,$(ASAN))
CFLAGS += -fsanitize=address
LDFLAGS += -fsanitize=address
endif # ASAN

ifeq (1,$(NO_OPTIM))
OPTIM_FLAGS := -O0 -mtune=generic
else
OPTIM_FLAGS := -O3 -march=native
endif

ifeq (1,$(LTO))
CFLAGS += -flto
LDFLAGS += -flto
endif

.PHONY: maintainer-self-host-test
maintainer-self-host-test: clean-sub cmod
	# recompile with optimizations
	cd src && $(MAKE)
	# recompile using src/cmod
	cd meta && $(MAKE)
	# check if binary changed (and if so, show differing files)
	diff src/cmod meta/cmod || (diff -q src/ meta/ | grep differ | grep '\.[cho]')

.PHONY:	maintainer-self-host maintainer-self-host-override \
		maintainer-update-sources maintainer-update-deps
maintainer-self-host: maintainer-self-host-test maintainer-update-sources maintainer-update-deps
maintainer-self-host-override: maintainer-update-sources maintainer-update-deps

maintainer-update-sources:
	cp -f $(SRC_H_FILES) .
	cp -f $(SRC_H_PARTS_FILES) parts/
	cp -f $(addprefix src/,$(SRC_C_FILES) cmod.vim) .
ifeq (1,$(HAVE_CRITERION))
	cp -f $(addprefix src/unittest/,$(SRC_TESTABLE_FILES)) unittest/
endif

maintainer-update-deps:
	$(MAKE) -B deps.mk

.PHONY: maintainer-update maintainer-update-readme m-u
m-u: maintainer-update
maintainer-update: maintainer-self-host
	$(MAKE) maintainer-update-readme
	$(MAKE) maintainer-benchmark

maintainer-update-readme: all
	$(MAKE) -B README.md

maintainer-benchmark:
	cd src && $(MAKE) -s benchmarks

.SILENT: README.md
README.md: src/README.pre.md
	cp $< $@.tmp
	echo -e '## cmod usage' >> $@.tmp
	./cmod --help | sed 's/^/    /' >> $@.tmp
	echo -e '\n\n' >> $@.tmp
	./cmod --docs | sed -e 's/^#/##/' >> $@.tmp
	echo -e '\n\n' >> $@.tmp
	./cmod --docs-full | sed -e 's/^#/##/' >> $@.tmp
	echo -e '\n\n' >> $@.tmp
	./cmod --docs-stdlib | sed 's/^#/##/' >> $@.tmp
	echo -e '\n----\n# Footnotes\n' >> $@.tmp
	$(GAWK) -f util/table_align.awk \
		-v column_cmd=$(COLUMN_CMD) \
		-v sep=$(COLUMN_SEP) \
		$@.tmp > $@
	$(RM) $@.tmp
	(file $@ | grep -q 'UTF-8') \
		|| (echo 'ERROR: Garbage in $@' && exit 1)

.PHONY: maintainer-getsize
maintainer-getsize:
	@objdump -t cmod \
		| cut -c26- \
		| $(GAWK) '{ x = strtonum("0x"$$2); all += x; print x,$$3 } END { print all,"TOTAL" }' \
		| sort -k1gr \
		| less
