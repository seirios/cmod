#ifndef CMOD_DSL_EXT_H
#define CMOD_DSL_EXT_H

#include "parts/struct_dsl_param.h"
#include "parts/variant_dsl_ast.h"

int (cmod_parse_dsl) (size_t code_len, const char code[static code_len], const struct dsl * lang,
                      bool pretty, int silent, bool verbose, srt_vector * astlist_o[static 1]);
int (dsl_ast_new_call1) (uint64_t kw_ix, const struct dsl_ast * first,
                         struct dsl_ast out[static 1]);
int (dsl_ast_new_call2) (uint64_t kw_ix, const struct dsl_ast * first,
                         const struct dsl_ast * second, struct dsl_ast out[static 1]);
int (dsl_ast_to_list) (const struct dsl_ast * node, srt_vector * out[static 1]);
#define dsl_ast_eval(a,b,c,d) dsl_ast_eval_aux(0,a,b,c,d)
int (dsl_ast_eval_aux) (size_t depth, struct dsl_ast * node, const struct dsl * lang,
                        size_t ierr[static 1], size_t errix[static 1]);

#endif
