#ifndef TABLE_JSON_H
#define TABLE_JSON_H

#include "libsrt/svector.h"
#include "cmod.ext.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

enum json_parse {
    JSON_PARSE_OK = 0,  /* default */
    JSON_PARSE_OOM = 1, /* out of memory */
    JSON_PARSE_BADROW = 2,      /* incorrect row size */
    JSON_PARSE_NDEFAULT = 3,    /* bad number of non-default columns */
    JSON_PARSE_FORMAT = 4,      /* bad format */
    JSON_PARSE_INVALID = 5,     /* invalid JSON token */
    JSON_PARSE_PARTIAL = 6,     /* partial JSON input */
    JSON_PARSE_UNKNOWN = 7,     /* unknown JSON error */
    JSON_PARSE_DUPLICATE = 8,   /* duplicate column name */
    JSON_PARSE_BADCOLUMN = 9,   /* unknown or mismatched column name */
    JSON_PARSE_MULTI = 10,      /* bad multi-table format */
};
#define enum_json_parse_len 10

/* parse a single JSON table */
int (table_json_parse) (const char *jstr, size_t jstrlen, size_t ncol_o[static 1],
                        vec_vec_string * rows_o[static 1], vec_namarg * colnarg_o[static 1],
                        size_t ierr[static 1]);
/* parse multiple JSON tables */
int (multitable_json_parse) (const char *jstr, size_t jstrlen, vec_table * tables_o[static 1],
                             size_t ierr[static 1]);
/* ensure implicit columns match preset column names */
int (table_json_colnam_preset) (vec_vec_string * rows, const vec_namarg * json_colnam,
                                size_t nnondef, const vec_namarg * preset_colnam,
                                size_t ierr[static 1]);
/* ensure implicit columns match variable column names */
int (table_json_colnam_variable) (const vec_namarg * json_colnam, size_t nnondef,
                                  const vec_namarg * preset_colnam, setix * reqcols[static 1],
                                  size_t ierr[static 1]);

#endif
