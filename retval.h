
#ifndef CMOD_STDLIB_RETVAL_H_
#define CMOD_STDLIB_RETVAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <string.h>
#include <stdint.h>

typedef uint64_t ret_t; // stack depth = 8 (bytes)

#define RET_ERR_ERRNO (int8_t)INT8_MIN
enum ret_t_err {
    RET_ERR_INVALID = 0,        /* Unknown error */
    RET_ERR_FAIL = -1,  /* Non-specific failure */
    RET_ERR_ARGIN = -2, /* Invalid input argument */
    RET_ERR_ARGOUT = -3,        /* Invalid output argument */
    RET_ERR_NOMEM = -4, /* Memory allocation error */
};
#define enum_ret_t_err_len 5

__attribute__((const))
    __attribute__((unused))
static inline const char *ret_t_err_strenum(const enum ret_t_err x)
{
    switch (x) {
    case RET_ERR_INVALID:
        return "Unknown error";
        break;
    case RET_ERR_FAIL:
        return "Non-specific failure";
        break;
    case RET_ERR_ARGIN:
        return "Invalid input argument";
        break;
    case RET_ERR_ARGOUT:
        return "Invalid output argument";
        break;
    case RET_ERR_NOMEM:
        return "Memory allocation error";
        break;
    default:
        return "";
        break;
    }
}

__attribute__((const))
    __attribute__((unused))
static inline const char *ret_t_err_enumtostr(const enum ret_t_err x)
{
    switch (x) {
    case RET_ERR_INVALID:
        return "RET_ERR_INVALID";
        break;
    case RET_ERR_FAIL:
        return "RET_ERR_FAIL";
        break;
    case RET_ERR_ARGIN:
        return "RET_ERR_ARGIN";
        break;
    case RET_ERR_ARGOUT:
        return "RET_ERR_ARGOUT";
        break;
    case RET_ERR_NOMEM:
        return "RET_ERR_NOMEM";
        break;
    default:
        return "";
        break;
    }
}

__attribute__((const))
    __attribute__((unused))
static inline bool ret_t_err_strtoenum(const char *str, int val[static 1])
{
    if ('R' == *((str) + 0) && strncmp((str) + 1, "ET_ERR_", 7) == 0) {
        if ('F' == *((str) + 8) && strncmp((str) + 9, "AIL", 4) == 0) {
            *val = RET_ERR_FAIL;
            return true;
        }
        else if ('I' == *((str) + 8) && strncmp((str) + 9, "NVALID", 7) == 0) {
            *val = RET_ERR_INVALID;
            return true;
        }
        else if ('N' == *((str) + 8) && strncmp((str) + 9, "OMEM", 5) == 0) {
            *val = RET_ERR_NOMEM;
            return true;
        }
        else if ('A' == *((str) + 8) && 'R' == *((str) + 9) && 'G' == *((str) + 10)) {
            if ('I' == *((str) + 11) && 'N' == *((str) + 12) && '\0' == *((str) + 13)) {
                *val = RET_ERR_ARGIN;
                return true;
            }
            else if ('O' == *((str) + 11) && 'U' == *((str) + 12) && 'T' == *((str) + 13)
                     && '\0' == *((str) + 14)) {
                *val = RET_ERR_ARGOUT;
                return true;
            }
            else {
                goto iftrie_end;
            }
        }
        else {
            goto iftrie_end;
        }
    }
    else {
        goto iftrie_end;
 iftrie_end:;
        return false;
    }
}

/* maximal ret_f type */
struct ret_f {
    char *name;
    const struct ret_f *p[1 + INT8_MAX];
};

/* minimal ret_f type */
struct ret_f0 {
    char *name;
    const struct ret_f *p;
};

void check_ret_t(ret_t, const struct ret_f *);

#endif
