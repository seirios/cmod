include os_detect.mk

PKG_CONFIG := pkg-config

# Auto-detect
HAVE_GNU_SOURCE := $(if $(filter GNU Linux,$(DETECTED_OS)),1,0)
LIBSECCOMP_LIBS := $(shell ($(PKG_CONFIG) --libs libseccomp || echo 0) 2>/dev/null)
HAVE_LIBSECCOMP := $(if $(findstring -lseccomp,$(LIBSECCOMP_LIBS)),1,0)
CRITERION_LIBS := $(shell ($(PKG_CONFIG) --libs criterion || echo 0) 2>/dev/null)
HAVE_CRITERION := $(if $(findstring -lcriterion,$(CRITERION_LIBS)),1,0)

ifeq (Darwin,$(DETECTED_OS))
HAVE_GNU_SOURCE := 0
HAVE_LIBSECCOMP := 0
HAVE_CRITERION := 0
endif

# Tunables (overrides)
#HAVE_GNU_SOURCE := 1
#HAVE_LIBSECCOMP := 1
#HAVE_CRITERION := 1

$(info HAVE_GNU_SOURCE = $(HAVE_GNU_SOURCE))
$(info HAVE_LIBSECCOMP = $(HAVE_LIBSECCOMP))
$(info HAVE_CRITERION = $(HAVE_CRITERION))
