#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <signal.h>
#include <stdbool.h>

#include <assert.h>
#include "ralloc.h"
#include "cmod.ext.h"
#include "util_string.h"

#ifndef CMOD_AUTOTYPES_H
#define CMOD_AUTOTYPES_H
/* tagged unions */
#include "parts/variant_xint.h"
#include "parts/variant_cmod_option.h"
/* C parsing */
#include "parts/variant_c_designator.h"
#include "parts/variant_c_initializer.h"
#include "parts/variant_c_declarator.h"
#include "parts/variant_c_specifier.h"
#include "parts/variant_c_typespec.h"
#include "parts/struct_c_designated_initializer.h"

#include "parts/autoarr_arr_cdi.h"

#include "parts/autoarr_arr_cdd.h"

srt_string *(cini_to_str) (const struct c_initializer * ini);
int (arr_cdi_to_str) (const arr_cdi * x, const vec_string * name, srt_string * buf[static 1],
                      bool is_comparison);
int (arr_cdd_to_decl) (arr_cdd * x, struct decl * decl);
srt_string *(decl_to_str) (const struct decl * decl, const char *fname, const char *fsuffix,
                           bool is_statement, bool const_named);
/* C% switch */
#include "parts/variant_mod_case.h"
#endif

void c_designator_free(struct c_designator *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_C_DESIGNATOR(ARRAY):
        {
            for (size_t i = 0; i < sv_len(x->value.array); ++i) {
                const void *_item = sv_at(x->value.array, i);
                {
                    char **item = (char **)(_item);
                    free(*item);
                    *item = NULL;
                }
            }
            sv_free(&(x->value.array));
        }

        break;
    case ENUM_C_DESIGNATOR(FIELD):
        free(x->value.field);
        x->value.field = NULL;
        break;
    case ENUM_C_DESIGNATOR(PTR):
        free(x->value.ptr);
        x->value.ptr = NULL;
        break;
    }

    *x = (struct c_designator) { 0 };
}

int c_designator_dup(const struct c_designator *x, struct c_designator *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_C_DESIGNATOR(ARRAY):
        {
            y->value.array = sv_dup(x->value.array);    // duplicate buffer
            if (sv_void == y->value.array && NULL != x->value.array) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.array); ++i) {
                char **_item = sv_elem_addr(y->value.array, i);
                const char *_item_old = *_item;
                *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                if (NULL != (_item_old) && NULL == (*_item)) {
                    return 2;
                }

            }
        }

        break;
    case ENUM_C_DESIGNATOR(FIELD):
        y->value.field = (NULL != (x->value.field)) ? rstrdup(x->value.field) : NULL;
        if (NULL != (x->value.field) && NULL == (y->value.field)) {
            return 2;
        }

        break;
    case ENUM_C_DESIGNATOR(PTR):
        y->value.ptr = (NULL != (x->value.ptr)) ? rstrdup(x->value.ptr) : NULL;
        if (NULL != (x->value.ptr) && NULL == (y->value.ptr)) {
            return 2;
        }

        break;
    }

    return 0;
}

void c_initializer_free(struct c_initializer *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_C_INITIALIZER(EXPR):
        {
            for (size_t i = 0; i < sv_len(x->value.expr); ++i) {
                const void *_item = sv_at(x->value.expr, i);
                {
                    char **item = (char **)(_item);
                    free(*item);
                    *item = NULL;
                }
            }
            sv_free(&(x->value.expr));
        }

        break;
    case ENUM_C_INITIALIZER(COMP):
        if (NULL != x->value.comp) {
            arr_cdi_free((arr_cdi *) (x->value.comp));
            free(x->value.comp);
            x->value.comp = NULL;
        }

        break;
    }

    *x = (struct c_initializer) { 0 };
}

int c_initializer_dup(const struct c_initializer *x, struct c_initializer *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_C_INITIALIZER(EXPR):
        {
            y->value.expr = sv_dup(x->value.expr);      // duplicate buffer
            if (sv_void == y->value.expr && NULL != x->value.expr) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.expr); ++i) {
                char **_item = sv_elem_addr(y->value.expr, i);
                const char *_item_old = *_item;
                *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                if (NULL != (_item_old) && NULL == (*_item)) {
                    return 2;
                }

            }
        }

        break;
    case ENUM_C_INITIALIZER(COMP):
        if (NULL == x->value.comp)
            y->value.comp = x->value.comp;
        else if (arr_cdi_dup(x->value.comp, y->value.comp)) {
            return 2;
        }

        break;
    }

    return 0;
}

void c_declarator_free(struct c_declarator *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_C_DECLARATOR(IDENT):
        free(x->value.ident);
        x->value.ident = NULL;
        break;
    case ENUM_C_DECLARATOR(ARRAY):
        {
            for (size_t i = 0; i < sv_len(x->value.array); ++i) {
                const void *_item = sv_at(x->value.array, i);
                {
                    char **item = (char **)(_item);
                    free(*item);
                    *item = NULL;
                }
            }
            sv_free(&(x->value.array));
        }

        break;
    case ENUM_C_DECLARATOR(FUNC):
        {
            for (size_t i = 0; i < sv_len(x->value.func); ++i) {
                const void *_item = sv_at(x->value.func, i);
                {
                    char **item = (char **)(_item);
                    free(*item);
                    *item = NULL;
                }
            }
            sv_free(&(x->value.func));
        }

        break;
    case ENUM_C_DECLARATOR(DECL):
        if (NULL != x->value.decl) {
            arr_cdd_free((arr_cdd *) (x->value.decl));
            free(x->value.decl);
            x->value.decl = NULL;
        }

        break;
    }

    {
        for (size_t i = 0; i < sv_len(x->typ); ++i) {
            const void *_item = sv_at(x->typ, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->typ));
    }
    {
        for (size_t i = 0; i < sv_len(x->ids); ++i) {
            const void *_item = sv_at(x->ids, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->ids));
    }
    {
        for (size_t i = 0; i < sv_len(x->ini); ++i) {
            const void *_item = sv_at(x->ini, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(x->ini));
    }
    {
        for (size_t i = 0; i < sv_len(x->ptr); ++i) {
            const void *_item = sv_at(x->ptr, i);
            {
                vec_string *vec = *(vec_string **) (_item);
                {
                    for (size_t i = 0; i < sv_len(vec); ++i) {
                        const void *_item = sv_at(vec, i);
                        {
                            char **item = (char **)(_item);
                            free(*item);
                            *item = NULL;
                        }
                    }
                    sv_free(&(vec));
                }
            }
        }
        sv_free(&(x->ptr));
    }

    *x = (struct c_declarator) { 0 };
}

int c_declarator_dup(const struct c_declarator *x, struct c_declarator *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_C_DECLARATOR(IDENT):
        y->value.ident = (NULL != (x->value.ident)) ? rstrdup(x->value.ident) : NULL;
        if (NULL != (x->value.ident) && NULL == (y->value.ident)) {
            return 2;
        }

        break;
    case ENUM_C_DECLARATOR(ARRAY):
        {
            y->value.array = sv_dup(x->value.array);    // duplicate buffer
            if (sv_void == y->value.array && NULL != x->value.array) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.array); ++i) {
                char **_item = sv_elem_addr(y->value.array, i);
                const char *_item_old = *_item;
                *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                if (NULL != (_item_old) && NULL == (*_item)) {
                    return 2;
                }

            }
        }

        break;
    case ENUM_C_DECLARATOR(FUNC):
        {
            y->value.func = sv_dup(x->value.func);      // duplicate buffer
            if (sv_void == y->value.func && NULL != x->value.func) {
                return 2;
            }
            for (size_t i = 0; i < sv_len(y->value.func); ++i) {
                char **_item = sv_elem_addr(y->value.func, i);
                const char *_item_old = *_item;
                *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                if (NULL != (_item_old) && NULL == (*_item)) {
                    return 2;
                }

            }
        }

        break;
    case ENUM_C_DECLARATOR(DECL):
        if (NULL == x->value.decl)
            y->value.decl = x->value.decl;
        else if (arr_cdd_dup(x->value.decl, y->value.decl)) {
            return 2;
        }

        break;
    }

    {
        y->typ = sv_dup(x->typ);        // duplicate buffer
        if (sv_void == y->typ && NULL != x->typ) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->typ); ++i) {
            char **_item = sv_elem_addr(y->typ, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->ids = sv_dup(x->ids);        // duplicate buffer
        if (sv_void == y->ids && NULL != x->ids) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->ids); ++i) {
            char **_item = sv_elem_addr(y->ids, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->ini = sv_dup(x->ini);        // duplicate buffer
        if (sv_void == y->ini && NULL != x->ini) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->ini); ++i) {
            char **_item = sv_elem_addr(y->ini, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }
    {
        y->ptr = sv_dup(x->ptr);        // duplicate buffer
        if (sv_void == y->ptr && NULL != x->ptr) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y->ptr); ++i) {
            vec_string **_item2 = sv_elem_addr(y->ptr, i);
            const vec_string *_item2_old = *_item2;
            {
                *_item2 = sv_dup(_item2_old);   // duplicate buffer
                if (sv_void == *_item2 && NULL != _item2_old) {
                    return 2;
                }
                for (size_t i = 0; i < sv_len(*_item2); ++i) {
                    char **_item = sv_elem_addr(*_item2, i);
                    const char *_item_old = *_item;
                    *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
                    if (NULL != (_item_old) && NULL == (*_item)) {
                        return 2;
                    }

                }
            }
        }
    }

    return 0;
}

void c_specifier_free(struct c_specifier *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_C_SPECIFIER(STORAGE):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_SPECIFIER(QUALIFIER):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_SPECIFIER(FUNCTION):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_SPECIFIER(ALIGNMENT):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_SPECIFIER(TYPE):
        c_typespec_free(&(x->value.CTyp));
        break;
    }

    *x = (struct c_specifier) { 0 };
}

int c_specifier_dup(const struct c_specifier *x, struct c_specifier *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_C_SPECIFIER(STORAGE):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_SPECIFIER(QUALIFIER):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_SPECIFIER(FUNCTION):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_SPECIFIER(ALIGNMENT):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_SPECIFIER(TYPE):
        if (c_typespec_dup(&(x->value.CTyp), &(y->value.CTyp))) {
            return 2;
        }
        break;
    }

    return 0;
}

void c_typespec_free(struct c_typespec *x)
{
    switch (x->type) {
    default:
        break;
    case ENUM_C_TYPESPEC(SIMPLE):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_TYPESPEC(ATOMIC):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_TYPESPEC(COMPOUND):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_TYPESPEC(ENUM):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    case ENUM_C_TYPESPEC(TYPEDEF):
        free(x->value.Text);
        x->value.Text = NULL;
        break;
    }

    *x = (struct c_typespec) { 0 };
}

int c_typespec_dup(const struct c_typespec *x, struct c_typespec *y)
{
    if (NULL == y)
        return 1;

    y->type = x->type;

    switch (x->type) {
    default:
        break;
    case ENUM_C_TYPESPEC(SIMPLE):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_TYPESPEC(ATOMIC):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_TYPESPEC(COMPOUND):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_TYPESPEC(ENUM):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    case ENUM_C_TYPESPEC(TYPEDEF):
        y->value.Text = (NULL != (x->value.Text)) ? rstrdup(x->value.Text) : NULL;
        if (NULL != (x->value.Text) && NULL == (y->value.Text)) {
            return 2;
        }

        break;
    }

    return 0;
}

arr_cdi *arr_cdi_box(arr_cdi x)
{
    arr_cdi *y = calloc(1, sizeof(*y));
    if (NULL != y) {
        y->arr = x.arr;
        y->siz = x.siz;
        y->len = x.len;
        y->cast = x.cast;
    }
    return y;
}

arr_cdi arr_cdi_unbox(arr_cdi *x)
{
    arr_cdi y = { 0 };  // empty
    if (NULL != x) {
        y.arr = x->arr;
        y.siz = x->siz;
        y.len = x->len;
        y.cast = x->cast;
    }
    return y;
}

int arr_cdi_dup(const arr_cdi *x, arr_cdi *out)
{
    if (NULL == out)
        return 1;

    arr_cdi y = { 0 };

    y.len = x->len;
    y = arr_cdi_fitsize(y);     // grow to fit
    for (size_t i = 0; i < y.len; ++i) {        // duplicate items
        if (c_designator_dup(&(x->arr[i].dsg), &(y.arr[i].dsg)) ||
            c_initializer_dup(&(x->arr[i].ini), &(y.arr[i].ini)))
            return 1;
    }

    y.cast = (NULL != (x->cast)) ? rstrdup(x->cast) : NULL;
    if (NULL != (x->cast) && NULL == (y.cast)) {
        return 2;
    }

    *out = y;
    return 0;
}

arr_cdd *arr_cdd_box(arr_cdd x)
{
    arr_cdd *y = calloc(1, sizeof(*y));
    if (NULL != y) {
        y->arr = x.arr;
        y->siz = x.siz;
        y->len = x.len;
        y->pointer = x.pointer;
    }
    return y;
}

arr_cdd arr_cdd_unbox(arr_cdd *x)
{
    arr_cdd y = { 0 };  // empty
    if (NULL != x) {
        y.arr = x->arr;
        y.siz = x->siz;
        y.len = x->len;
        y.pointer = x->pointer;
    }
    return y;
}

int arr_cdd_dup(const arr_cdd *x, arr_cdd *out)
{
    if (NULL == out)
        return 1;

    arr_cdd y = { 0 };

    y.len = x->len;
    y = arr_cdd_fitsize(y);     // grow to fit
    for (size_t i = 0; i < y.len; ++i) {        // duplicate items
        if (c_declarator_dup(&(x->arr[i]), &(y.arr[i])))
            return 1;
    }

    {
        y.pointer = sv_dup(x->pointer); // duplicate buffer
        if (sv_void == y.pointer && NULL != x->pointer) {
            return 2;
        }
        for (size_t i = 0; i < sv_len(y.pointer); ++i) {
            char **_item = sv_elem_addr(y.pointer, i);
            const char *_item_old = *_item;
            *_item = (NULL != (_item_old)) ? rstrdup(_item_old) : NULL;
            if (NULL != (_item_old) && NULL == (*_item)) {
                return 2;
            }

        }
    }

    *out = y;
    return 0;
}

/* reduce designated initializer to string */
int arr_cdi_to_str(const arr_cdi *x, const vec_string *name, srt_string *buf[static 1],
                   bool is_comparison)
{
    for (size_t i = 0; i < x->len; ++i) {
        srt_string *prefix = NULL;
        srt_string *suffix = NULL;

        const struct c_designator *dsg = &x->arr[i].dsg;
        const struct c_initializer *ini = &x->arr[i].ini;

        if (NULL == ((prefix) = ss_alloc(0))) {
            return 1;
        }

        if (NULL == ((suffix) = ss_alloc(0))) {
            return 1;
        }

        switch (ini->type) {
        case ENUM_C_INITIALIZER(EXPR):
            switch (dsg->type) {
            case ENUM_C_DESIGNATOR(ARRAY):
                {
                    srt_string *tmp = vstr_join(C_DESIGNATOR_ARRAY_get(*dsg), ' ');
                    if (ss_void == tmp)
                        return 1;

                    if (ss_void == ss_cat_cn1(&suffix, '[')) {
                        return 1;
                    }

                    if (ss_void == ss_cat(&suffix, tmp)) {
                        return 1;
                    }

                    if (ss_void == ss_cat_cn1(&suffix, ']')) {
                        return 1;
                    }

                    ss_free(&tmp);
                }
                break;
            case ENUM_C_DESIGNATOR(FIELD):

                if (ss_void == ss_cat_cn1(&suffix, '.')) {
                    return 1;
                }

                if (ss_void == ss_cat_c(&suffix, C_DESIGNATOR_FIELD_get(*dsg))) {
                    return 1;
                }

                break;
            case ENUM_C_DESIGNATOR(PTR):

                if (ss_void == ss_cat_c(&suffix, "->")) {
                    return 1;
                }

                if (ss_void == ss_cat_c(&suffix, C_DESIGNATOR_PTR_get(*dsg))) {
                    return 1;
                }

                break;
            case ENUM_C_DESIGNATOR(INVALID):
                if (is_comparison)
                    return 2;   // cannot be empty for comparison
                break;
            }
            break;
        case ENUM_C_INITIALIZER(COMP):
            {
                arr_cdi *comp = C_INITIALIZER_COMP_get(*ini);
                switch (dsg->type) {
                case ENUM_C_DESIGNATOR(ARRAY):
                    {
                        if (NULL != comp->cast)
                            return 4;
                        srt_string *tmp = vstr_join(C_DESIGNATOR_ARRAY_get(*dsg), ' ');
                        if (ss_void == tmp)
                            return 1;

                        if (ss_void == ss_cat_cn1(&suffix, '[')) {
                            return 1;
                        }

                        if (ss_void == ss_cat(&suffix, tmp)) {
                            return 1;
                        }

                        if (ss_void == ss_cat_cn1(&suffix, ']')) {
                            return 1;
                        }

                        ss_free(&tmp);
                    }
                    break;
                case ENUM_C_DESIGNATOR(FIELD):
                    if (NULL != comp->cast)
                        return 4;

                    if (ss_void == ss_cat_cn1(&suffix, '.')) {
                        return 1;
                    }

                    if (ss_void == ss_cat_c(&suffix, C_DESIGNATOR_FIELD_get(*dsg))) {
                        return 1;
                    }

                    break;
                case ENUM_C_DESIGNATOR(PTR):
                    {
                        const char *close = (NULL != comp->cast) ? ")))" : "))";

                        if (ss_void == ss_cat_c(&prefix, "(*(")) {
                            return 1;
                        }

                        if (NULL != comp->cast) {
                            if (ss_void == ss_cat_cn1(&prefix, '(')) {
                                return 1;
                            }

                            if (ss_void == ss_cat_c(&prefix, comp->cast)) {
                                return 1;
                            }

                            if (ss_void == ss_cat_c(&prefix, "*)(")) {
                                return 1;
                            }

                        }

                        if (ss_void == ss_cat_cn1(&suffix, '.')) {
                            return 1;
                        }

                        if (ss_void == ss_cat_c(&suffix, C_DESIGNATOR_PTR_get(*dsg))) {
                            return 1;
                        }

                        if (ss_void == ss_cat_c(&suffix, close)) {
                            return 1;
                        }

                    }
                    break;
                case ENUM_C_DESIGNATOR(INVALID):
                    return 2;
                    break;
                }
            }
            break;
        case ENUM_C_INITIALIZER(INVALID):
            break;
        }

        vec_string *new_name = NULL;
        char *csuffix = NULL;
        char *cprefix = NULL;
        {       // add prefix and suffix
            {
                const char *ctmp = ss_to_c(suffix);
                csuffix = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (csuffix)) {
                }

            }
            ss_free(&(suffix));
            if (NULL == csuffix) {
                return 1;
            }

            {
                const char *ctmp = ss_to_c(prefix);
                cprefix = (NULL != (ctmp)) ? rstrdup(ctmp) : NULL;
                if (NULL != (ctmp) && NULL == (cprefix)) {
                }

            }
            ss_free(&(prefix));
            if (NULL == cprefix) {
                return 1;
            }

            vec_string *(tmp) = sv_alloc_t(SV_PTR, sv_len(name) + 2);
            if (sv_void == (tmp)) {
                return 1;
            }

            sv_push(&tmp, &cprefix);
            sv_cat(&tmp, name); // borrow name
            sv_push(&tmp, &csuffix);
            new_name = tmp;
        }

        int ret;
        switch (ini->type) {
        case ENUM_C_INITIALIZER(EXPR):
            {
                const vec_string *expr = C_INITIALIZER_EXPR_get(*ini);
                const char *firstitem = sv_at_ptr(expr, 0);
                bool is_string_literal = ('"' == firstitem[0]);
                if (is_comparison)
                    ss_cat_c(buf, (i > 0) ? " && (" : "(", is_string_literal ? "strncmp(" : "");
                {
                    const size_t _len = sv_len(new_name);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _0 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        ss_cat_c(buf, (*(const char **)sv_at(new_name, _0)));
                    }
                }
                const char *cmp_eq = is_string_literal ? "," : " == ";
                const char *nocmp_eq = (dsg->type != ENUM_C_DESIGNATOR(INVALID)) ? " = " : "";
                ss_cat_c(buf, is_comparison ? cmp_eq : nocmp_eq);
                {
                    const size_t _len = sv_len(expr);
                    for (size_t _idx = 0; _idx < _len; ++_idx) {
                        const size_t _1 = _idx;
                        const size_t _idx = 0;
                        (void)_idx;
                        ss_cat_c(buf, (*(const char **)sv_at(expr, _1)));
                    }
                }
                if (is_comparison && is_string_literal) {
                    ss_cat_cn1(buf, ',');
                    ss_cat_int(buf, strlen(firstitem) - 1);
                    ss_cat_c(buf, ") == 0)");
                }
                else
                    ss_cat_c(buf, is_comparison ? ")" : ", ");
            }
            break;
        case ENUM_C_INITIALIZER(COMP):
            if (is_comparison)
                ss_cat_c(buf, (i > 0) ? "\n && (" : "\n (");
            if ((ret = arr_cdi_to_str(C_INITIALIZER_COMP_get(*ini), new_name, buf, is_comparison)))     // recurse
                return ret;     // return from the deep
            if (is_comparison)
                ss_cat_c(buf, ")\n");
            break;
        case ENUM_C_INITIALIZER(INVALID):
            break;
        }

        {       // remove prefix and suffix
            sv_free(&new_name); // unborrow name
            free(cprefix);
            free(csuffix);
        }
    }

    return 0;
}

int arr_cdd_to_decl(arr_cdd *x, struct decl *decl)
{

    if (NULL == decl->sv_decl && NULL == (decl->sv_decl = sv_alloc_t(SV_PTR, 32))) {
        return 1;
    }

    /* concatenate pointer */
    if (sv_void == sv_cat(&(decl->sv_decl), x->pointer)) {
        return 1;
    }
    sv_free(&(x->pointer));

    int ret;
    for (size_t i = 0; i < x->len; ++i) {
        struct c_declarator *dcl = &x->arr[i];
        switch (dcl->type) {
        case ENUM_C_DECLARATOR(IDENT):
            decl->name = C_DECLARATOR_IDENT_get(*dcl);  // const borrow
            decl->iname = SETIX_set(sv_len(decl->sv_decl));     // store name index
            if (!sv_push(&(decl->sv_decl), &(decl->name))) {
                return 1;
            }
            C_DECLARATOR_IDENT_get(*dcl) = NULL;        // hand-over
            break;
        case ENUM_C_DECLARATOR(ARRAY):
            if (!vstr_addstr(&(decl->sv_decl), "[")) {
                return 1;
            }

            if (sv_void == sv_cat(&(decl->sv_decl), C_DECLARATOR_ARRAY_get(*dcl))) {
                return 1;
            }
            sv_free(&(C_DECLARATOR_ARRAY_get(*dcl)));

            if (!vstr_addstr(&(decl->sv_decl), "]")) {
                return 1;
            }

            break;
        case ENUM_C_DECLARATOR(FUNC):
            /* save function arguments (identifiers, initializers, types, start/end indices) */
            if (NULL == decl->sv_fargs) {       // only for first declarator (outermost)
                decl->sv_fargs = dcl->ids;
                dcl->ids = NULL;        // hand-over
                decl->sv_fargs_init = dcl->ini;
                dcl->ini = NULL;        // hand-over
                decl->sv_fargs_type = dcl->typ;
                dcl->typ = NULL;        // hand-over
                decl->sv_fargs_ptrs = dcl->ptr;
                dcl->ptr = NULL;        // hand-over
                /* inclusive start index */
                decl->fargs_i0 = SETIX_set(sv_len(decl->sv_decl) + 1);
                /* inclusive end index */
                decl->fargs_i1 = SETIX_set((sv_len(decl->sv_decl) + 1) +
                                           sv_len(C_DECLARATOR_FUNC_get(*dcl)) - 1);
            }
            if (!vstr_addstr(&(decl->sv_decl), "(")) {
                return 1;
            }

            if (sv_void == sv_cat(&(decl->sv_decl), C_DECLARATOR_FUNC_get(*dcl))) {
                return 1;
            }
            sv_free(&(C_DECLARATOR_FUNC_get(*dcl)));

            if (!vstr_addstr(&(decl->sv_decl), ")")) {
                return 1;
            }

            break;
        case ENUM_C_DECLARATOR(DECL):
            if (!vstr_addstr(&(decl->sv_decl), "(")) {
                return 1;
            }

            if ((ret = arr_cdd_to_decl(C_DECLARATOR_DECL_get(*dcl), decl)))     // recurse
                return ret;     // return from the deep
            if (!vstr_addstr(&(decl->sv_decl), ")")) {
                return 1;
            }

            break;
        case ENUM_C_DECLARATOR(INVALID):
            assert(0 && "C% internal error" "");
            break;
        }
    }

    return 0;
}

srt_string *decl_to_str(const struct decl *decl, const char *fname, const char *fsuffix,
                        bool is_statement, bool const_named)
{
    srt_string *(ss) = ss_alloc(0);
    if (ss_void == (ss)) {
        return ss_void;
    }

    if (NULL != decl->sv_spec) {
        {
            const size_t _len = sv_len(decl->sv_spec);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _2 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                switch (((const struct c_specifier *)sv_at(decl->sv_spec, _2))->type) {
                case ENUM_C_SPECIFIER(STORAGE):
                    ss_cat_c(&ss,
                             C_SPECIFIER_STORAGE_get(*
                                                     ((const struct c_specifier *)
                                                      sv_at(decl->sv_spec, _2))), " ");
                    break;
                case ENUM_C_SPECIFIER(QUALIFIER):
                    ss_cat_c(&ss,
                             C_SPECIFIER_QUALIFIER_get(*
                                                       ((const struct c_specifier *)
                                                        sv_at(decl->sv_spec, _2))), " ");
                    break;
                case ENUM_C_SPECIFIER(FUNCTION):
                    ss_cat_c(&ss,
                             C_SPECIFIER_FUNCTION_get(*
                                                      ((const struct c_specifier *)
                                                       sv_at(decl->sv_spec, _2))), " ");
                    break;
                case ENUM_C_SPECIFIER(ALIGNMENT):
                    ss_cat_c(&ss,
                             C_SPECIFIER_ALIGNMENT_get(*
                                                       ((const struct c_specifier *)
                                                        sv_at(decl->sv_spec, _2))), " ");
                    break;
                case ENUM_C_SPECIFIER(TYPE):
                    ss_cat_c(&ss,
                             C_TYPESPEC_get(C_SPECIFIER_TYPE_get
                                            (*
                                             ((const struct c_specifier *)
                                              sv_at(decl->sv_spec, _2)))), " ");
                    break;
                case ENUM_C_SPECIFIER(INVALID):
                    assert(0 && "C% internal error" "");
                    break;
                }
            }
        }
    }

    for (size_t i = 0; i < sv_len(decl->pointer); ++i)
        ss_cat_c(&ss, sv_at_ptr(decl->pointer, i), " ");

    static const char argv_suffix[] = "__args";
    if (decl->fargs_i0.set && decl->fargs_i1.set && decl->iname.set) {  // function declarator
        for (size_t i = 0; i < decl->iname.ix; ++i)
            ss_cat_c(&ss, sv_at_ptr(decl->sv_decl, i), " ");

        if (is_statement)
            ss_cat_c(&ss, "( ", fname, fsuffix, " ) ");
        else
            ss_cat_c(&ss, fname, fsuffix, " ");

        for (size_t i = decl->iname.ix + 1; i < decl->fargs_i0.ix; ++i)
            ss_cat_c(&ss, sv_at_ptr(decl->sv_decl, i), " ");

        if (decl->named) {
            ss_cat_c(&ss, const_named ? "const " : "", "struct ", decl->name, argv_suffix, " ");
            ss_cat(&ss, decl->wrapper);
            ss_cat_cn1(&ss, ' ');
        }
        else
            for (size_t i = decl->fargs_i0.ix; i <= decl->fargs_i1.ix; ++i)
                ss_cat_c(&ss, sv_at_ptr(decl->sv_decl, i), " ");

        for (size_t i = decl->fargs_i1.ix + 1; i < sv_len(decl->sv_decl); ++i)
            ss_cat_c(&ss, sv_at_ptr(decl->sv_decl, i), " ");
    }
    else if (NULL != decl->sv_decl) {   // non-function declarator
        {
            const size_t _len = sv_len(decl->sv_decl);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _3 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                ss_cat_c(&ss, (*(const char **)sv_at(decl->sv_decl, _3)), " ");
            }
        }
    }

    if (is_statement)
        ss_cat_cn1(&ss, ';');

    return ss;
}

srt_string *cini_to_str(const struct c_initializer *ini)
{
    if (NULL == ini || ini->type == ENUM_C_INITIALIZER(INVALID))
        return ss_void;
    srt_string *(ss) = ss_alloc(0);
    if (ss_void == (ss)) {
        return ss_void;
    }

    srt_string *tmp = NULL;
    switch (ini->type) {
    case ENUM_C_INITIALIZER(EXPR):
        tmp = vstr_join(C_INITIALIZER_EXPR_get(*ini), ' ');
        if (ss_void == tmp)
            return ss_void;
        ss_cat(&ss, tmp);
        ss_free(&tmp);
        break;
    case ENUM_C_INITIALIZER(COMP):
        if (!arr_cdi_to_str(C_INITIALIZER_COMP_get(*ini), NULL, &tmp, false)) {
            char *cast = C_INITIALIZER_COMP_get(*ini)->cast;
            if (NULL != cast)
                ss_cat_c(&ss, "(", cast, ")");
            ss_cat_c(&ss, "{ ");
            ss_cat(&ss, tmp);
            ss_cat_c(&ss, " }");
            ss_free(&tmp);
        }
        break;
    case ENUM_C_INITIALIZER(INVALID):  /* handled above */
        break;
    }

    return ss;
}
