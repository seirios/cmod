#ifndef PARTS_VARIANT_C_DESIGNATOR
#define PARTS_VARIANT_C_DESIGNATOR

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

enum c_designator_type {
C_DESIGNATOR_TYPE_INVALID = 0, /* default */
C_DESIGNATOR_TYPE_ARRAY = 1,
C_DESIGNATOR_TYPE_FIELD = 2,
C_DESIGNATOR_TYPE_PTR = 3,
};
#define enum_c_designator_type_len 3
struct c_designator {
    enum c_designator_type type;
    union {
            vec_string* array;
           char* field;
           char* ptr;
       } value;
    };

void c_designator_free(struct c_designator *x);

__attribute__ ((warn_unused_result))
int c_designator_dup(const struct c_designator *x, struct c_designator *y);

#define ENUM_C_DESIGNATOR(x) C_DESIGNATOR_TYPE_##x
#define C_DESIGNATOR_INVALID_type .type=ENUM_C_DESIGNATOR(INVALID)
#define C_DESIGNATOR_INVALID_set() (struct c_designator){ C_DESIGNATOR_INVALID_type }
#define C_DESIGNATOR_INVALID_xset(...) (struct c_designator){ C_DESIGNATOR_INVALID_type, __VA_ARGS__ }

#define C_DESIGNATOR_ARRAY_type .type=ENUM_C_DESIGNATOR(ARRAY)
#define C_DESIGNATOR_ARRAY_value .value.array
#define C_DESIGNATOR_ARRAY_set(x) (struct c_designator){ C_DESIGNATOR_ARRAY_type, C_DESIGNATOR_ARRAY_value = (x) }
#define C_DESIGNATOR_ARRAY_xset(x, ...) (struct c_designator){ C_DESIGNATOR_ARRAY_type, C_DESIGNATOR_ARRAY_value = (x), __VA_ARGS__ }
#define C_DESIGNATOR_ARRAY_get(x) ((x) C_DESIGNATOR_ARRAY_value)
#define C_DESIGNATOR_FIELD_type .type=ENUM_C_DESIGNATOR(FIELD)
#define C_DESIGNATOR_FIELD_value .value.field
#define C_DESIGNATOR_FIELD_set(x) (struct c_designator){ C_DESIGNATOR_FIELD_type, C_DESIGNATOR_FIELD_value = (x) }
#define C_DESIGNATOR_FIELD_xset(x, ...) (struct c_designator){ C_DESIGNATOR_FIELD_type, C_DESIGNATOR_FIELD_value = (x), __VA_ARGS__ }
#define C_DESIGNATOR_FIELD_get(x) ((x) C_DESIGNATOR_FIELD_value)
#define C_DESIGNATOR_PTR_type .type=ENUM_C_DESIGNATOR(PTR)
#define C_DESIGNATOR_PTR_value .value.ptr
#define C_DESIGNATOR_PTR_set(x) (struct c_designator){ C_DESIGNATOR_PTR_type, C_DESIGNATOR_PTR_value = (x) }
#define C_DESIGNATOR_PTR_xset(x, ...) (struct c_designator){ C_DESIGNATOR_PTR_type, C_DESIGNATOR_PTR_value = (x), __VA_ARGS__ }
#define C_DESIGNATOR_PTR_get(x) ((x) C_DESIGNATOR_PTR_value)


#endif
