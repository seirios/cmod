#ifndef PARTS_STRUCT_STR_MOD_H
#define PARTS_STRUCT_STR_MOD_H


#include "parts/enum_str_enums.h"

struct str_mod {
    enum str_trim trim;
    enum str_tran tran;
    enum str_fold fold;
    enum str_pre pre;
    enum str_wrap wrap;
    enum str_lead lead;
    enum str_trail trail;
};

#endif
