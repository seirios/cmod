#ifndef PARTS_STRUCT_SETIX
#define PARTS_STRUCT_SETIX

#include <stdint.h>
#include <stdbool.h>

/* index with set flag */
struct setix {
    uint64_t ix; // index
    bool set;    // set flag
};

#define SETIX_none (struct setix){ .set= false }
#define SETIX_set(x) (struct setix){ .set= true, .ix= (x) }

#endif
