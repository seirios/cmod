#ifndef PARTS_STRUCT_ARRAY_TYPE
#define PARTS_STRUCT_ARRAY_TYPE

#include "parts/typedef_srt_alias.h"
#include "parts/enum_dlltype.h"
#include "parts/typedef_setix.h"
#include "parts/variant_namarg.h"
#include "parts/struct_str_mod.h"

struct dllarg {
    enum dlltype type; // argument type
    char* name; // argument name
    size_t pos; // argument position
    size_t num; // argument number
    size_t rep; // argument repeat
    struct str_mod mod; // argument string modifications
};
struct snippet {
    char* name; // snippet name
    srt_string* body; // snippet body
    vec_namarg* sv_forl; // vector of named formal arguments
    srt_hmap* shm_forl; // hash map of formal argument indices
    vec_dllarg* sv_argl; // vector of argument placeholders
    srt_set* sms_dllarg; // set of unique positional references
    size_t nargs; // total number of arguments
    size_t nout; // number of output arguments
    size_t ndefin; // number of default input arguments
    size_t ndefout; // number of default output arguments
    bool redef; // is redefinable?
};
struct table {
    char* name; // table name
    srt_hmap* hmap; // hash map for rows
    vec_namarg* sv_colnarg; // vector of column names and default values
    srt_hmap* shm_colnam; // hash map of column names
    vec_vec_string* svv_rows; // table rows
    struct setix key; // hash column key
    size_t nnondef; // number of non-default columns
    bool redef; // is redefinable?
};
struct decl {
    const char* type; // declarator type (points inside .sv_spec)
    const char* name; // declarator name (points inside .sv_decl)
    char* init; // declarator initializer
    vec_string* pointer; // declarator pointer part
    srt_string* wrapper; // name of argument-wrapping struct
    srt_string* fsuffix; // suffix of macro-shadowed function
    vec_string* sv_begin; // pre-condition snippets
    vec_string* sv_end; // post-condition snippets
    vec_cspec* sv_spec; // declaration specifiers
    vec_string* sv_decl; // declarator
    vec_string* sv_fargs; // function argument list
    vec_string* sv_fargs_type; // function argument type list
    vec_string* sv_fargs_init; // function argument initializer list
    vec_vec_string* sv_fargs_ptrs; // function argument pointer list
    struct setix iname; // index of declarator name
    struct setix fargs_i0; // index of start of function arguments
    struct setix fargs_i1; // index of end of function arguments
    size_t ndefault; // number of default arguments
    bool named; // has argument-wrapping struct?
};
struct fdef {
    bool from_typedef; // function definition from typedef?
    char* name; // function name
    uint64_t index; // function prototype index
    srt_string* retexpr; // function return expression
};

#define TABLE_NROWS(x) (sv_len((x).svv_rows))
#define TABLE_NCOLS(x) (sv_len((x).sv_colnarg))
#define TABLE_NCOLS_DEF(x) (sv_len((x).sv_colnarg) - (x).nnondef)
#define TABLE_NCOLS_NODEF(x) ((x).nnondef)
#define TABLE_COLNAME(tab,i) VNARG_GETNAME((tab)->sv_colnarg,i)

#define SNIPPET_HAS_FORMAL_ARGS(x) (sv_len((x)->sv_forl) > 0)
#define SNIPPET_NARG(x) ((x)->nargs)
#define SNIPPET_NARG_DEF(x) ((x)->ndefin + (x)->ndefout)
#define SNIPPET_NARG_NODEF(x) ((x)->nargs - SNIPPET_NARG_DEF(x))
#define SNIPPET_NARGIN(x) ((x)->nargs - (x)->nout)
#define SNIPPET_NARGIN_DEF(x) ((x)->ndefin)
#define SNIPPET_NARGIN_NODEF(x) (SNIPPET_NARGIN(x) - (x)->ndefin)
#define SNIPPET_NARGOUT(x) ((x)->nout)
#define SNIPPET_NARGOUT_DEF(x) ((x)->ndefout)
#define SNIPPET_NARGOUT_NODEF(x) (SNIPPET_NARGOUT(x) - (x)->ndefout)

#endif
