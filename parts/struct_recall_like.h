#ifndef PARTS_STRUCT_RECALL_LIKE
#define PARTS_STRUCT_RECALL_LIKE

#include "array_type.h"
#include "parts/typedef_srt_alias.h"

/* recall-like syntax element */
struct recall_like {
    char *name;                 // snippet name
    const struct snippet *snip; // snippet reference
    vec_vec_namarg *arg_lists;  // argument lists
};

#endif
