#ifndef PARTS_ENUM_DSL_TYPE
#define PARTS_ENUM_DSL_TYPE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>


#define DSL_TYPE_CLIKE(x) ((x) == DSL_TYPE_cexpr || (x) == DSL_TYPE_custom)
enum dsl_type {
DSL_TYPE_invalid = 0, /* default */
DSL_TYPE_asm = 1, /* assembler-like statements */
DSL_TYPE_sexpr = 2, /* S-expression tree */
DSL_TYPE_cexpr = 3, /* C-like expressions */
DSL_TYPE_custom = 4, /* custom expressions */
DSL_TYPE_tree = 5, /* hierarchical expression */
};
#define enum_dsl_type_len 5

__attribute__ ((const))
__attribute__ ((unused))
static inline const char* dsl_type_strenum(const enum dsl_type x) {
switch(x) {
case DSL_TYPE_asm: return "assembler-like statements"; break;
case DSL_TYPE_sexpr: return "S-expression tree"; break;
case DSL_TYPE_cexpr: return "C-like expressions"; break;
case DSL_TYPE_custom: return "custom expressions"; break;
case DSL_TYPE_tree: return "hierarchical expression"; break;
default: return "DSL_TYPE_invalid"; break;
}
}

__attribute__ ((const))
__attribute__ ((unused))
static inline const char* dsl_type_enumtostr(const enum dsl_type x) {
switch(x) {
case DSL_TYPE_asm: return "DSL_TYPE_asm"; break;
case DSL_TYPE_sexpr: return "DSL_TYPE_sexpr"; break;
case DSL_TYPE_cexpr: return "DSL_TYPE_cexpr"; break;
case DSL_TYPE_custom: return "DSL_TYPE_custom"; break;
case DSL_TYPE_tree: return "DSL_TYPE_tree"; break;
default: return "DSL_TYPE_invalid"; break;
}
}


__attribute__ ((const))
__attribute__ ((unused))
static inline bool dsl_type_strtoenum(const char *str, int val[static 1]) {
if('D' == *((str) + 0) && strncmp((str) + 1,"SL_TYPE_",8) == 0) {
if('a' == *((str) + 9) && 's' == *((str) + 10) && 'm' == *((str) + 11) && '\0' == *((str) + 12)) {
*val = DSL_TYPE_asm; return true;
}
else if('c' == *((str) + 9)) {
if('e' == *((str) + 10) && strncmp((str) + 11,"xpr",4) == 0) {
*val = DSL_TYPE_cexpr; return true;
}
else if('u' == *((str) + 10) && strncmp((str) + 11,"stom",5) == 0) {
*val = DSL_TYPE_custom; return true;
}
else {
goto iftrie_end;
}
}
else if('s' == *((str) + 9) && strncmp((str) + 10,"expr",5) == 0) {
*val = DSL_TYPE_sexpr; return true;
}
else if('t' == *((str) + 9) && strncmp((str) + 10,"ree",4) == 0) {
*val = DSL_TYPE_tree; return true;
}
else {
goto iftrie_end;
}
}
else {
goto iftrie_end;
iftrie_end: ;
*val = DSL_TYPE_invalid; return true;
}
}

#endif
