#ifndef PARTS_VARIANT_C_INITIALIZER
#define PARTS_VARIANT_C_INITIALIZER

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

enum c_initializer_type {
C_INITIALIZER_TYPE_INVALID = 0, /* default */
C_INITIALIZER_TYPE_EXPR = 1,
C_INITIALIZER_TYPE_COMP = 2,
};
#define enum_c_initializer_type_len 2
struct c_initializer {
    enum c_initializer_type type;
    union {
            vec_string* expr;
           struct arr_cdi* comp;
       } value;
    };

void c_initializer_free(struct c_initializer *x);

__attribute__ ((warn_unused_result))
int c_initializer_dup(const struct c_initializer *x, struct c_initializer *y);

#define ENUM_C_INITIALIZER(x) C_INITIALIZER_TYPE_##x
#define C_INITIALIZER_INVALID_type .type=ENUM_C_INITIALIZER(INVALID)
#define C_INITIALIZER_INVALID_set() (struct c_initializer){ C_INITIALIZER_INVALID_type }
#define C_INITIALIZER_INVALID_xset(...) (struct c_initializer){ C_INITIALIZER_INVALID_type, __VA_ARGS__ }

#define C_INITIALIZER_EXPR_type .type=ENUM_C_INITIALIZER(EXPR)
#define C_INITIALIZER_EXPR_value .value.expr
#define C_INITIALIZER_EXPR_set(x) (struct c_initializer){ C_INITIALIZER_EXPR_type, C_INITIALIZER_EXPR_value = (x) }
#define C_INITIALIZER_EXPR_xset(x, ...) (struct c_initializer){ C_INITIALIZER_EXPR_type, C_INITIALIZER_EXPR_value = (x), __VA_ARGS__ }
#define C_INITIALIZER_EXPR_get(x) ((x) C_INITIALIZER_EXPR_value)
#define C_INITIALIZER_COMP_type .type=ENUM_C_INITIALIZER(COMP)
#define C_INITIALIZER_COMP_value .value.comp
#define C_INITIALIZER_COMP_set(x) (struct c_initializer){ C_INITIALIZER_COMP_type, C_INITIALIZER_COMP_value = (x) }
#define C_INITIALIZER_COMP_xset(x, ...) (struct c_initializer){ C_INITIALIZER_COMP_type, C_INITIALIZER_COMP_value = (x), __VA_ARGS__ }
#define C_INITIALIZER_COMP_get(x) ((x) C_INITIALIZER_COMP_value)


#endif
