#ifndef PARTS_AUTOARR_ARR_CDI
#define PARTS_AUTOARR_ARR_CDI

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <signal.h>
#include <stdbool.h>
#include "parts/struct_c_designated_initializer.h"

#include <string.h>
#include <search.h>

#include <stdlib.h>

#define CMOD_AUTOARR_ARR_CDI_ITEM_TYPE struct c_designated_initializer
typedef struct arr_cdi { struct c_designated_initializer * arr ; size_t siz ; size_t len ; char * cast ; } arr_cdi ;


__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_fitsize(
        arr_cdi x)
{
    if(x.len > x.siz) { // grow
        size_t newsiz = (size_t)(x.len / 4 + ((x.len % 4) > 0 ? 1 : 0)) * 4; // new size
        void *tmp = rrealloc(x.arr,newsiz * sizeof(*x.arr));
        if(NULL != tmp) { x.arr = tmp; x.siz = newsiz; } // update
        else { fputs("out of memory""\n",stderr);
 exit(EXIT_FAILURE); } // fail
    }
    return x;
}


__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_add(
        arr_cdi x,
        struct c_designated_initializer item)
{
        switch(x.siz - x.len) {
        case 0:
        {
            void *tmp = rrealloc(x.arr,(x.siz += 4) * sizeof(*x.arr));
            if(NULL == tmp) { // fail
                error0("out of memory");
                exit(EXIT_FAILURE);
            }
            else { x.arr = tmp; } // update
        }
            /* FALLTHROUGH */
        default:
            x.arr[x.len++] = item;
            break;
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_preadd(
        arr_cdi x,
        struct c_designated_initializer item)
{
        switch(x.siz - x.len) {
        case 0:
        {
            void *tmp = rrealloc(x.arr,(x.siz += 4) * sizeof(*x.arr));
            if(NULL == tmp) { // fail
                error0("out of memory");
                exit(EXIT_FAILURE);
            }
            else { x.arr = tmp; } // update
        }
            /* FALLTHROUGH */
        default:
                        memmove(x.arr + 1,x.arr,x.len++ * sizeof(*x.arr));
            x.arr[0] = item;
   
            break;
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_cat(
        arr_cdi x,
        arr_cdi y)
{
    if(y.len > 0) {
        size_t xlen = x.len; // save old length
        x.len += y.len;      // sum lengths
        x = arr_cdi_fitsize(x); // grow to fit
        memcpy(x.arr + xlen,y.arr,y.len * sizeof(*y.arr));  // copy items
        memset(y.arr,0x0,y.len * sizeof(*y.arr)); // clear items
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_rem(
        arr_cdi x,
        size_t n)
{
    if((n = (n < x.len ? n : x.len)) > 0) {
        for(size_t i = 0; i < n; ++i) {
                c_designator_free(&(x.arr[x.len - 1 - i].dsg));
    c_initializer_free(&(x.arr[x.len - 1 - i].ini));
        }
        memset(x.arr + x.len - n,0x0,n * sizeof(*x.arr)); // clear items
        x.len -= n; // reduce length
    }

    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdi arr_cdi_prerem(
        arr_cdi x,
        size_t n)
{
    if((n = (n < x.len) ? n : x.len) > 0) {
        for(size_t i = 0; i < n; ++i) {
                c_designator_free(&(x.arr[i].dsg));
    c_initializer_free(&(x.arr[i].ini));
        }
        memmove(x.arr,x.arr + n,(x.len - n) * sizeof(*x.arr)); // move items
        x.len -= n; // reduce length
    }

    return x;
}

__attribute__ ((unused))
static inline void arr_cdi_free(
        arr_cdi *x)
{
    for(size_t i = 0; i < x->len; ++i) { // free items
            c_designator_free(&(x->arr[i].dsg));
    c_initializer_free(&(x->arr[i].ini));
    }
    { free(x -> arr); (x -> arr) = NULL; } // free array

            free(x->cast); x->cast = NULL;   

    *x = (arr_cdi){ 0 }; // empty
}

__attribute__ ((unused))
static inline void arr_cdi_clear(
        arr_cdi *x)
{
    for(size_t i = 0; i < x->len; ++i) { // free items
            c_designator_free(&(x->arr[i].dsg));
    c_initializer_free(&(x->arr[i].ini));
    }

            free(x->cast); x->cast = NULL;   

    if(NULL != x->arr) // zero memory for reuse
        memset(x->arr,0x0,x->siz * 4 * sizeof(*x->arr));
    x->len = x->siz = 0;
}

__attribute__ ((warn_unused_result))
arr_cdi * ( arr_cdi_box ) ( arr_cdi x  ) ;
__attribute__ ((warn_unused_result))
arr_cdi ( arr_cdi_unbox ) ( arr_cdi * x  ) ;
__attribute__ ((warn_unused_result))
int ( arr_cdi_dup ) ( const arr_cdi * x  , arr_cdi * out  ) ;
__attribute__ ((warn_unused_result))
arr_cdi ( arr_cdi_sort ) ( arr_cdi x  ) ;
__attribute__ ((warn_unused_result))
struct c_designated_initializer * ( arr_cdi_lsearch ) ( arr_cdi x  , const struct c_designated_initializer key  ) ;
__attribute__ ((warn_unused_result))
struct c_designated_initializer * ( arr_cdi_bsearch ) ( arr_cdi x  , const struct c_designated_initializer key  ) ;


#endif
