#ifndef PARTS_VARIANT_NAMARG
#define PARTS_VARIANT_NAMARG

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "typedef_srt_alias.h"

enum namarg_type {
CMOD_ARG_NOVALUE = 0, /* default */
CMOD_ARG_VERBATIM = 1, /* verbatim */
CMOD_ARG_ID = 2, /* C identifier */
CMOD_ARG_IDEXT = 3, /* extended identifier */
CMOD_ARG_INTSTR = 4, /* integer */
CMOD_ARG_SPECIAL = 5, /* special identifier */
CMOD_ARG_ROWNO = 6, /* row number */
CMOD_ARG_OPTIONAL = 7, /* optional argument */
CMOD_ARG_ARGLINK = 8, /* argument link */
CMOD_ARG_EXPR = 9, /* expression */
};
#define enum_namarg_type_len 9

__attribute__ ((const))
__attribute__ ((unused))
static inline const char* namarg_type_strenum(const enum namarg_type x) {
switch(x) {
case CMOD_ARG_VERBATIM: return "verbatim"; break;
case CMOD_ARG_ID: return "C identifier"; break;
case CMOD_ARG_IDEXT: return "extended identifier"; break;
case CMOD_ARG_INTSTR: return "integer"; break;
case CMOD_ARG_SPECIAL: return "special identifier"; break;
case CMOD_ARG_ROWNO: return "row number"; break;
case CMOD_ARG_OPTIONAL: return "optional argument"; break;
case CMOD_ARG_ARGLINK: return "argument link"; break;
case CMOD_ARG_EXPR: return "expression"; break;
default: return "CMOD_ARG_NOVALUE"; break;
}
}
struct namarg {
    enum namarg_type type;
    union {
            size_t Ref;
           char* Text;
           vec_namarg* Expr;
       } value;
            char* name;
   };

void namarg_free(struct namarg *x);

__attribute__ ((warn_unused_result))
int namarg_dup(const struct namarg *x, struct namarg *y);

#define ENUM_NAMARG(x) CMOD_ARG_##x
#define CMOD_ARG_NOVALUE_type .type=ENUM_NAMARG(NOVALUE)
#define CMOD_ARG_NOVALUE_set() (struct namarg){ CMOD_ARG_NOVALUE_type }
#define CMOD_ARG_NOVALUE_xset(...) (struct namarg){ CMOD_ARG_NOVALUE_type, __VA_ARGS__ }

#define CMOD_ARG_VERBATIM_type .type=ENUM_NAMARG(VERBATIM)
#define CMOD_ARG_VERBATIM_value .value.Text
#define CMOD_ARG_VERBATIM_set(x) (struct namarg){ CMOD_ARG_VERBATIM_type, CMOD_ARG_VERBATIM_value = (x) }
#define CMOD_ARG_VERBATIM_xset(x, ...) (struct namarg){ CMOD_ARG_VERBATIM_type, CMOD_ARG_VERBATIM_value = (x), __VA_ARGS__ }
#define CMOD_ARG_VERBATIM_get(x) ((x) CMOD_ARG_VERBATIM_value)
#define CMOD_ARG_ID_type .type=ENUM_NAMARG(ID)
#define CMOD_ARG_ID_value .value.Text
#define CMOD_ARG_ID_set(x) (struct namarg){ CMOD_ARG_ID_type, CMOD_ARG_ID_value = (x) }
#define CMOD_ARG_ID_xset(x, ...) (struct namarg){ CMOD_ARG_ID_type, CMOD_ARG_ID_value = (x), __VA_ARGS__ }
#define CMOD_ARG_ID_get(x) ((x) CMOD_ARG_ID_value)
#define CMOD_ARG_IDEXT_type .type=ENUM_NAMARG(IDEXT)
#define CMOD_ARG_IDEXT_value .value.Text
#define CMOD_ARG_IDEXT_set(x) (struct namarg){ CMOD_ARG_IDEXT_type, CMOD_ARG_IDEXT_value = (x) }
#define CMOD_ARG_IDEXT_xset(x, ...) (struct namarg){ CMOD_ARG_IDEXT_type, CMOD_ARG_IDEXT_value = (x), __VA_ARGS__ }
#define CMOD_ARG_IDEXT_get(x) ((x) CMOD_ARG_IDEXT_value)
#define CMOD_ARG_INTSTR_type .type=ENUM_NAMARG(INTSTR)
#define CMOD_ARG_INTSTR_value .value.Text
#define CMOD_ARG_INTSTR_set(x) (struct namarg){ CMOD_ARG_INTSTR_type, CMOD_ARG_INTSTR_value = (x) }
#define CMOD_ARG_INTSTR_xset(x, ...) (struct namarg){ CMOD_ARG_INTSTR_type, CMOD_ARG_INTSTR_value = (x), __VA_ARGS__ }
#define CMOD_ARG_INTSTR_get(x) ((x) CMOD_ARG_INTSTR_value)
#define CMOD_ARG_SPECIAL_type .type=ENUM_NAMARG(SPECIAL)
#define CMOD_ARG_SPECIAL_value .value.Text
#define CMOD_ARG_SPECIAL_set(x) (struct namarg){ CMOD_ARG_SPECIAL_type, CMOD_ARG_SPECIAL_value = (x) }
#define CMOD_ARG_SPECIAL_xset(x, ...) (struct namarg){ CMOD_ARG_SPECIAL_type, CMOD_ARG_SPECIAL_value = (x), __VA_ARGS__ }
#define CMOD_ARG_SPECIAL_get(x) ((x) CMOD_ARG_SPECIAL_value)
#define CMOD_ARG_ARGLINK_type .type=ENUM_NAMARG(ARGLINK)
#define CMOD_ARG_ARGLINK_value .value.Ref
#define CMOD_ARG_ARGLINK_set(x) (struct namarg){ CMOD_ARG_ARGLINK_type, CMOD_ARG_ARGLINK_value = (x) }
#define CMOD_ARG_ARGLINK_xset(x, ...) (struct namarg){ CMOD_ARG_ARGLINK_type, CMOD_ARG_ARGLINK_value = (x), __VA_ARGS__ }
#define CMOD_ARG_ARGLINK_get(x) ((x) CMOD_ARG_ARGLINK_value)
#define CMOD_ARG_EXPR_type .type=ENUM_NAMARG(EXPR)
#define CMOD_ARG_EXPR_value .value.Expr
#define CMOD_ARG_EXPR_set(x) (struct namarg){ CMOD_ARG_EXPR_type, CMOD_ARG_EXPR_value = (x) }
#define CMOD_ARG_EXPR_xset(x, ...) (struct namarg){ CMOD_ARG_EXPR_type, CMOD_ARG_EXPR_value = (x), __VA_ARGS__ }
#define CMOD_ARG_EXPR_get(x) ((x) CMOD_ARG_EXPR_value)
#define CMOD_ARG_ROWNO_type .type=ENUM_NAMARG(ROWNO)
#define CMOD_ARG_ROWNO_set() (struct namarg){ CMOD_ARG_ROWNO_type }
#define CMOD_ARG_OPTIONAL_type .type=ENUM_NAMARG(OPTIONAL)
#define CMOD_ARG_OPTIONAL_set() (struct namarg){ CMOD_ARG_OPTIONAL_type }


#define VNARG_GETNAME(x,i) (((struct namarg*)sv_at((x),(i)))->name)

#endif
