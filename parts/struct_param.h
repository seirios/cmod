#ifndef PARTS_STRUCT_PARAM
#define PARTS_STRUCT_PARAM

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>

#include "parts/typedef_srt_alias.h"

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Parser-lexer shared state */
struct param {
    srt_map* sm_prefix_su;
    srt_map* sm_prefix_us;
    vec_snippet* sv_snip;
    hmap_su* shm_snip;
    vec_table* sv_tab;
    hmap_su* shm_tab;
    vec_type* sv_dtype;
    hmap_su* shm_dtype;
    vec_proto* sv_dproto;
    hmap_su* shm_dproto;
    vec_dsl* sv_dsl;
    hmap_su* shm_dsl;
    vec_fdef* sv_fdef;
    hmap_su* shm_fdef;
    set_string* sms_once;
    set_string* sms_enum;
    vec_string* sv_ipath;
    char* nameout;
    char* nameout_tests;
    size_t eval_limit;
    size_t iter_limit;
    int silent;
    int debug;
    bool keep_tmp;
    bool verbose;
    int pretty;
    int disable_seccomp;
    int scanner_disable;
    int exit_on_first_err;
    bool inactive_comments;
    bool inactive_shorthands;
    bool inactive_idcap;
    bool const_rsrc;
    bool subparse;
    bool inactive_include;
    bool inactive_once;
    bool inactive_snippet;
    bool inactive_recall;
    bool inactive_table;
    bool inactive_table_stack;
    bool inactive_map;
    bool inactive_pipe;
    bool inactive_unittest;
    bool inactive_dsl_def;
    bool inactive_dsl;
    bool inactive_intop;
    bool inactive_delay;
    bool inactive_defined;
    bool inactive_strcmp;
    bool inactive_strstr;
    bool inactive_strlen;
    bool inactive_strsub;
    bool inactive_table_size;
    bool inactive_table_length;
    bool inactive_table_get;
    bool inactive_typedef;
    bool inactive_proto;
    bool inactive_def;
    bool inactive_unused;
    bool inactive_prefix;
    bool inactive_enum;
    bool inactive_strin;
    bool inactive_foreach;
    bool inactive_switch;
    bool inactive_free;
    bool inactive_arrlen;
    vec_string* sv_tmpfile;
    size_t nforeach;
    size_t nstrin;
    size_t nunittest;
    size_t nerr;
    uint64_t iprefix;
    bool success;
    int init;
    struct bufstack* curbs;
    srt_string* line;
    FILE* fpout_tests;
    char* namein;
    size_t kparse;
    bool stop_parse;
    bool in_parse;
    bool errabort;
    bool erraccept;
    bool errnomem;
    bool hstr_nl;
    const char* errtxt;
    YYLTYPE errlloc;
    setix infdef;
    vec_dllarg* sv_dllarg;
    vec_string* sv_idlist;
    srt_hmap* shm_dup;
    const char* kwname;
    vec_string* param_types;
    vec_vec_string* param_pntrs;
    vec_string* param_names;
    vec_string* param_inits;
    srt_string* kw_buf;
    srt_string* hstr_buf;
    char* hstr_id;
    YYLTYPE kwlloc;
    setix idup;
    size_t count;
    size_t nout;
    int brc_lvl;
    int par_lvl;
    int sqr_lvl;
    int exit_on;
    int mb_start;
    int opt_start;
    int hstr_start;
    int hstr_exit_on;
    int ccode_start;
    int ccode_exit_on;
    int ccode_force_exit_on;
    int cmp_base;
    bool blockcomm;
    bool nlcomm;
    bool dllspecial;
    bool ignore_enum_constants;
    bool redef_nowarn;
    bool is_shorthand;
};

#endif
