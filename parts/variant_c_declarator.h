#ifndef PARTS_VARIANT_C_DECLARATOR
#define PARTS_VARIANT_C_DECLARATOR

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

enum c_declarator_type {
C_DECLARATOR_TYPE_INVALID = 0, /* default */
C_DECLARATOR_TYPE_IDENT = 1,
C_DECLARATOR_TYPE_ARRAY = 2,
C_DECLARATOR_TYPE_FUNC = 3,
C_DECLARATOR_TYPE_DECL = 4,
};
#define enum_c_declarator_type_len 4
struct c_declarator {
    enum c_declarator_type type;
    union {
            char* ident;
           vec_string* array;
           vec_string* func;
           struct arr_cdd* decl;
       } value;
            vec_string* typ;
           vec_string* ids;
           vec_string* ini;
           vec_vec_string* ptr;
   };

void c_declarator_free(struct c_declarator *x);

__attribute__ ((warn_unused_result))
int c_declarator_dup(const struct c_declarator *x, struct c_declarator *y);

#define ENUM_C_DECLARATOR(x) C_DECLARATOR_TYPE_##x
#define C_DECLARATOR_INVALID_type .type=ENUM_C_DECLARATOR(INVALID)
#define C_DECLARATOR_INVALID_set() (struct c_declarator){ C_DECLARATOR_INVALID_type }
#define C_DECLARATOR_INVALID_xset(...) (struct c_declarator){ C_DECLARATOR_INVALID_type, __VA_ARGS__ }

#define C_DECLARATOR_IDENT_type .type=ENUM_C_DECLARATOR(IDENT)
#define C_DECLARATOR_IDENT_value .value.ident
#define C_DECLARATOR_IDENT_set(x) (struct c_declarator){ C_DECLARATOR_IDENT_type, C_DECLARATOR_IDENT_value = (x) }
#define C_DECLARATOR_IDENT_xset(x, ...) (struct c_declarator){ C_DECLARATOR_IDENT_type, C_DECLARATOR_IDENT_value = (x), __VA_ARGS__ }
#define C_DECLARATOR_IDENT_get(x) ((x) C_DECLARATOR_IDENT_value)
#define C_DECLARATOR_ARRAY_type .type=ENUM_C_DECLARATOR(ARRAY)
#define C_DECLARATOR_ARRAY_value .value.array
#define C_DECLARATOR_ARRAY_set(x) (struct c_declarator){ C_DECLARATOR_ARRAY_type, C_DECLARATOR_ARRAY_value = (x) }
#define C_DECLARATOR_ARRAY_xset(x, ...) (struct c_declarator){ C_DECLARATOR_ARRAY_type, C_DECLARATOR_ARRAY_value = (x), __VA_ARGS__ }
#define C_DECLARATOR_ARRAY_get(x) ((x) C_DECLARATOR_ARRAY_value)
#define C_DECLARATOR_FUNC_type .type=ENUM_C_DECLARATOR(FUNC)
#define C_DECLARATOR_FUNC_value .value.func
#define C_DECLARATOR_FUNC_set(x) (struct c_declarator){ C_DECLARATOR_FUNC_type, C_DECLARATOR_FUNC_value = (x) }
#define C_DECLARATOR_FUNC_xset(x, ...) (struct c_declarator){ C_DECLARATOR_FUNC_type, C_DECLARATOR_FUNC_value = (x), __VA_ARGS__ }
#define C_DECLARATOR_FUNC_get(x) ((x) C_DECLARATOR_FUNC_value)
#define C_DECLARATOR_DECL_type .type=ENUM_C_DECLARATOR(DECL)
#define C_DECLARATOR_DECL_value .value.decl
#define C_DECLARATOR_DECL_set(x) (struct c_declarator){ C_DECLARATOR_DECL_type, C_DECLARATOR_DECL_value = (x) }
#define C_DECLARATOR_DECL_xset(x, ...) (struct c_declarator){ C_DECLARATOR_DECL_type, C_DECLARATOR_DECL_value = (x), __VA_ARGS__ }
#define C_DECLARATOR_DECL_get(x) ((x) C_DECLARATOR_DECL_value)


#endif
