#ifndef PARTS_VARIANT_CMOD_OPTION
#define PARTS_VARIANT_CMOD_OPTION

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include "parts/variant_xint.h"

enum cmod_option_type {
CMOD_OPTION_TYPE_UNVALUED = 0, /* default */
CMOD_OPTION_TYPE_XINT = 1,
CMOD_OPTION_TYPE_STRING = 2,
};
#define enum_cmod_option_type_len 2
struct cmod_option {
    enum cmod_option_type type;
    union {
            struct xint xint;
           srt_string* string;
       } value;
            char* name;
           bool is_special;
           bool is_verbatim;
           int assign;
   };

void cmod_option_free(struct cmod_option *x);

__attribute__ ((warn_unused_result))
int cmod_option_dup(const struct cmod_option *x, struct cmod_option *y);

#define ENUM_CMOD_OPTION(x) CMOD_OPTION_TYPE_##x
#define CMOD_OPTION_UNVALUED_type .type=ENUM_CMOD_OPTION(UNVALUED)
#define CMOD_OPTION_UNVALUED_set() (struct cmod_option){ CMOD_OPTION_UNVALUED_type }
#define CMOD_OPTION_UNVALUED_xset(...) (struct cmod_option){ CMOD_OPTION_UNVALUED_type, __VA_ARGS__ }

#define CMOD_OPTION_XINT_type .type=ENUM_CMOD_OPTION(XINT)
#define CMOD_OPTION_XINT_value .value.xint
#define CMOD_OPTION_XINT_set(x) (struct cmod_option){ CMOD_OPTION_XINT_type, CMOD_OPTION_XINT_value = (x) }
#define CMOD_OPTION_XINT_xset(x, ...) (struct cmod_option){ CMOD_OPTION_XINT_type, CMOD_OPTION_XINT_value = (x), __VA_ARGS__ }
#define CMOD_OPTION_XINT_get(x) ((x) CMOD_OPTION_XINT_value)
#define CMOD_OPTION_STRING_type .type=ENUM_CMOD_OPTION(STRING)
#define CMOD_OPTION_STRING_value .value.string
#define CMOD_OPTION_STRING_set(x) (struct cmod_option){ CMOD_OPTION_STRING_type, CMOD_OPTION_STRING_value = (x) }
#define CMOD_OPTION_STRING_xset(x, ...) (struct cmod_option){ CMOD_OPTION_STRING_type, CMOD_OPTION_STRING_value = (x), __VA_ARGS__ }
#define CMOD_OPTION_STRING_get(x) ((x) CMOD_OPTION_STRING_value)


#endif
