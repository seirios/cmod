#ifndef PARTS_VARIANT_XINT
#define PARTS_VARIANT_XINT

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

enum xint_type {
XINT_TYPE_INVALID = 0, /* default */
XINT_TYPE_SIGNED = 1,
XINT_TYPE_UNSIGNED = 2,
};
#define enum_xint_type_len 2
struct xint {
    enum xint_type type;
    union {
            intmax_t Signed;
           uintmax_t Unsigned;
       } value;
    };

void xint_free(struct xint *x);

__attribute__ ((warn_unused_result))
int xint_dup(const struct xint *x, struct xint *y);

#define ENUM_XINT(x) XINT_TYPE_##x
#define XINT_INVALID_type .type=ENUM_XINT(INVALID)
#define XINT_INVALID_set() (struct xint){ XINT_INVALID_type }
#define XINT_INVALID_xset(...) (struct xint){ XINT_INVALID_type, __VA_ARGS__ }

#define XINT_SIGNED_type .type=ENUM_XINT(SIGNED)
#define XINT_SIGNED_value .value.Signed
#define XINT_SIGNED_set(x) (struct xint){ XINT_SIGNED_type, XINT_SIGNED_value = (x) }
#define XINT_SIGNED_xset(x, ...) (struct xint){ XINT_SIGNED_type, XINT_SIGNED_value = (x), __VA_ARGS__ }
#define XINT_SIGNED_get(x) ((x) XINT_SIGNED_value)
#define XINT_UNSIGNED_type .type=ENUM_XINT(UNSIGNED)
#define XINT_UNSIGNED_value .value.Unsigned
#define XINT_UNSIGNED_set(x) (struct xint){ XINT_UNSIGNED_type, XINT_UNSIGNED_value = (x) }
#define XINT_UNSIGNED_xset(x, ...) (struct xint){ XINT_UNSIGNED_type, XINT_UNSIGNED_value = (x), __VA_ARGS__ }
#define XINT_UNSIGNED_get(x) ((x) XINT_UNSIGNED_value)


#endif
