#ifndef PARTS_TYPEDEF_SRT_ALIAS
#define PARTS_TYPEDEF_SRT_ALIAS

#include "libsrt/libsrt.h"

#define sv_void (srt_vector*)sd_void

#define ss_crefs(c_str) ss_cref(&(srt_string_ref){ 0 }, c_str)
#define ss_refs_buf(buf, buf_size) ss_ref_buf(&(srt_string_ref){ 0 }, buf, buf_size)

typedef srt_vector vec_u8;
typedef srt_vector vec_ss;
typedef srt_vector vec_string;
typedef srt_vector vec_vec_string;
typedef srt_vector vec_namarg;
typedef srt_vector vec_vec_namarg;
typedef srt_vector vec_dllarg;
typedef srt_vector vec_u64;
typedef srt_vector vec_xint;
typedef srt_vector vec_decl;
typedef srt_vector vec_cspec;
typedef srt_vector vec_mcase;
typedef srt_vector vec_cmopt;
typedef srt_vector vec_dslast;
typedef srt_hmap hmap_su;
typedef srt_set set_string;

typedef srt_vector vec_snippet; // vector of struct snippet
typedef srt_vector vec_snippet_ptr; // vector of struct snippet*
typedef srt_vector vec_table; // vector of struct table
typedef srt_vector vec_table_ptr; // vector of struct table*
typedef srt_vector vec_type; // vector of struct decl
typedef srt_vector vec_type_ptr; // vector of struct decl*
typedef srt_vector vec_proto; // vector of struct decl
typedef srt_vector vec_proto_ptr; // vector of struct decl*
typedef srt_vector vec_dsl; // vector of struct dsl
typedef srt_vector vec_dsl_ptr; // vector of struct dsl*
typedef srt_vector vec_fdef; // vector of struct fdef
typedef srt_vector vec_fdef_ptr; // vector of struct fdef*

#endif
