#ifndef PARTS_ENUM_DLLTYPE
#define PARTS_ENUM_DLLTYPE

enum dlltype {
DLLTYPE_NORMAL = 0,
DLLTYPE_ARGNAME = 1,
DLLTYPE_M = 2,
DLLTYPE_B = 3,
DLLTYPE_D = 4,
DLLTYPE_N = 5,
DLLTYPE_S = 6,
DLLTYPE_T = 7,
DLLTYPE_U = 8,
DLLTYPE_X = 9,
DLLTYPE_COMMA = 10,
DLLTYPE_NAME = 11,
DLLTYPE_NR = 12,
DLLTYPE_ARGC = 13,
DLLTYPE_ARGC_OUT = 14,
DLLTYPE_ARGC_IN = 15,
DLLTYPE_ARGV = 16,
DLLTYPE_ARGV_OUT = 17,
DLLTYPE_ARGV_IN = 18,
DLLTYPE_RARGV = 19,
DLLTYPE_RARGV_OUT = 20,
DLLTYPE_RARGV_IN = 21,
};
#define enum_dlltype_len 22

#endif
