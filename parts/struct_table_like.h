#ifndef PARTS_STRUCT_TABLE_LIKE
#define PARTS_STRUCT_TABLE_LIKE

#include "array_type.h"
#include "parts/typedef_srt_alias.h"

/* table-like syntax element */
struct table_like {
     vec_namarg *sel;  // column selection
     vec_string *lst;  // table names list
     struct table tab; // lambda table (or table reference)
     bool is_lambda;   // is lambda table?
 };

#endif
