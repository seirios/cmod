#ifndef PARTS_ENUM_STR_ENUMS
#define PARTS_ENUM_STR_ENUMS


enum str_trim {
    TRIM_NONE = 0,
        TRIM_SPACE = (1UL << 0)
   ,    TRIM_LAST = (1UL << 1)
   ,    TRIM_NL = (1UL << 2)
   };
enum str_tran {
    TRAN_NONE = 0,
        TRAN_EMPTY = 'E'
   ,    TRAN_CID = 'C'
   ,    TRAN_SNIPPET = 'e'
   ,    TRAN_CSTR = 'g'
   ,    TRAN_JSON = 'j'
   ,    TRAN_HEX = 'x'
   ,    TRAN_B64 = 'X'
   ,    TRAN_LZH = 'l'
   };
enum str_fold {
    FOLD_NONE = 0,
        FOLD_LOWER = 'L'
   ,    FOLD_UPPER = 'U'
   };
enum str_pre {
    PRE_NONE = 0,
        PRE_HTAG = 'h'
   ,    PRE_BSLASH = 'q'
   ,    PRE_DOLLAR = 'd'
   ,    PRE_DOT = 't'
   ,    PRE_MOD = 'm'
   ,    PRE_ASTER = 'a'
   ,    PRE_AMPER = 'r'
   };
enum str_wrap {
    WRAP_NONE = 0,
        WRAP_SPACE = (1UL << 0)
   ,    WRAP_SQUOT = (1UL << 1)
   ,    WRAP_DQUOT = (1UL << 2)
   ,    WRAP_PAREN = (1UL << 3)
   ,    WRAP_BRACK = (1UL << 4)
   ,    WRAP_ANGLE = (1UL << 5)
   ,    WRAP_BRACE = (1UL << 6)
   ,    WRAP_BTICK = (1UL << 7)
   ,    WRAP_HSTR = (1UL << 8)
   };
enum str_lead {
    LEAD_NONE = 0,
        LEAD_PTR = (1UL << 0)
   ,    LEAD_REF = (1UL << 1)
   ,    LEAD_DOLLAR = (1UL << 2)
   ,    LEAD_NAME = (1UL << 3)
   ,    LEAD_COMMA = (1UL << 4)
   };
enum str_trail {
    TRAIL_NONE = 0,
        TRAIL_COMMA = (1UL << 0)
   ,    TRAIL_SPACE = (1UL << 1)
   ,    TRAIL_NL = (1UL << 2)
   };

#endif
