#ifndef PARTS_VARIANT_C_SPECIFIER
#define PARTS_VARIANT_C_SPECIFIER

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "parts/variant_c_typespec.h"

enum c_specifier_type {
C_SPECIFIER_TYPE_INVALID = 0, /* default */
C_SPECIFIER_TYPE_STORAGE = 1,
C_SPECIFIER_TYPE_QUALIFIER = 2,
C_SPECIFIER_TYPE_FUNCTION = 3,
C_SPECIFIER_TYPE_ALIGNMENT = 4,
C_SPECIFIER_TYPE_TYPE = 5,
};
#define enum_c_specifier_type_len 5
struct c_specifier {
    enum c_specifier_type type;
    union {
            char* Text;
           struct c_typespec CTyp;
       } value;
    };

void c_specifier_free(struct c_specifier *x);

__attribute__ ((warn_unused_result))
int c_specifier_dup(const struct c_specifier *x, struct c_specifier *y);

#define ENUM_C_SPECIFIER(x) C_SPECIFIER_TYPE_##x
#define C_SPECIFIER_INVALID_type .type=ENUM_C_SPECIFIER(INVALID)
#define C_SPECIFIER_INVALID_set() (struct c_specifier){ C_SPECIFIER_INVALID_type }
#define C_SPECIFIER_INVALID_xset(...) (struct c_specifier){ C_SPECIFIER_INVALID_type, __VA_ARGS__ }

#define C_SPECIFIER_STORAGE_type .type=ENUM_C_SPECIFIER(STORAGE)
#define C_SPECIFIER_STORAGE_value .value.Text
#define C_SPECIFIER_STORAGE_set(x) (struct c_specifier){ C_SPECIFIER_STORAGE_type, C_SPECIFIER_STORAGE_value = (x) }
#define C_SPECIFIER_STORAGE_xset(x, ...) (struct c_specifier){ C_SPECIFIER_STORAGE_type, C_SPECIFIER_STORAGE_value = (x), __VA_ARGS__ }
#define C_SPECIFIER_STORAGE_get(x) ((x) C_SPECIFIER_STORAGE_value)
#define C_SPECIFIER_QUALIFIER_type .type=ENUM_C_SPECIFIER(QUALIFIER)
#define C_SPECIFIER_QUALIFIER_value .value.Text
#define C_SPECIFIER_QUALIFIER_set(x) (struct c_specifier){ C_SPECIFIER_QUALIFIER_type, C_SPECIFIER_QUALIFIER_value = (x) }
#define C_SPECIFIER_QUALIFIER_xset(x, ...) (struct c_specifier){ C_SPECIFIER_QUALIFIER_type, C_SPECIFIER_QUALIFIER_value = (x), __VA_ARGS__ }
#define C_SPECIFIER_QUALIFIER_get(x) ((x) C_SPECIFIER_QUALIFIER_value)
#define C_SPECIFIER_FUNCTION_type .type=ENUM_C_SPECIFIER(FUNCTION)
#define C_SPECIFIER_FUNCTION_value .value.Text
#define C_SPECIFIER_FUNCTION_set(x) (struct c_specifier){ C_SPECIFIER_FUNCTION_type, C_SPECIFIER_FUNCTION_value = (x) }
#define C_SPECIFIER_FUNCTION_xset(x, ...) (struct c_specifier){ C_SPECIFIER_FUNCTION_type, C_SPECIFIER_FUNCTION_value = (x), __VA_ARGS__ }
#define C_SPECIFIER_FUNCTION_get(x) ((x) C_SPECIFIER_FUNCTION_value)
#define C_SPECIFIER_ALIGNMENT_type .type=ENUM_C_SPECIFIER(ALIGNMENT)
#define C_SPECIFIER_ALIGNMENT_value .value.Text
#define C_SPECIFIER_ALIGNMENT_set(x) (struct c_specifier){ C_SPECIFIER_ALIGNMENT_type, C_SPECIFIER_ALIGNMENT_value = (x) }
#define C_SPECIFIER_ALIGNMENT_xset(x, ...) (struct c_specifier){ C_SPECIFIER_ALIGNMENT_type, C_SPECIFIER_ALIGNMENT_value = (x), __VA_ARGS__ }
#define C_SPECIFIER_ALIGNMENT_get(x) ((x) C_SPECIFIER_ALIGNMENT_value)
#define C_SPECIFIER_TYPE_type .type=ENUM_C_SPECIFIER(TYPE)
#define C_SPECIFIER_TYPE_value .value.CTyp
#define C_SPECIFIER_TYPE_set(x) (struct c_specifier){ C_SPECIFIER_TYPE_type, C_SPECIFIER_TYPE_value = (x) }
#define C_SPECIFIER_TYPE_xset(x, ...) (struct c_specifier){ C_SPECIFIER_TYPE_type, C_SPECIFIER_TYPE_value = (x), __VA_ARGS__ }
#define C_SPECIFIER_TYPE_get(x) ((x) C_SPECIFIER_TYPE_value)


#endif
