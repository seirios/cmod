#ifndef PARTS_AUTOARR_ARR_CDD
#define PARTS_AUTOARR_ARR_CDD

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdbool.h>
#include <signal.h>
#include <stdbool.h>
#include "parts/variant_c_declarator.h"




#include <string.h>
#include <search.h>

#include <stdlib.h>

#define CMOD_AUTOARR_ARR_CDD_ITEM_TYPE struct c_declarator
typedef struct arr_cdd { struct c_declarator * arr ; size_t siz ; size_t len ; vec_string * pointer ; } arr_cdd ;


__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_fitsize(
        arr_cdd x)
{
    if(x.len > x.siz) { // grow
        size_t newsiz = (size_t)(x.len / 4 + ((x.len % 4) > 0 ? 1 : 0)) * 4; // new size
        void *tmp = rrealloc(x.arr,newsiz * sizeof(*x.arr));
        if(NULL != tmp) { x.arr = tmp; x.siz = newsiz; } // update
        else { fputs("out of memory""\n",stderr);
 exit(EXIT_FAILURE); } // fail
    }
    return x;
}


__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_add(
        arr_cdd x,
        struct c_declarator item)
{
        switch(x.siz - x.len) {
        case 0:
        {
            void *tmp = rrealloc(x.arr,(x.siz += 4) * sizeof(*x.arr));
            if(NULL == tmp) { // fail
                error0("out of memory");
                exit(EXIT_FAILURE);
            }
            else { x.arr = tmp; } // update
        }
            /* FALLTHROUGH */
        default:
            x.arr[x.len++] = item;
            break;
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_preadd(
        arr_cdd x,
        struct c_declarator item)
{
        switch(x.siz - x.len) {
        case 0:
        {
            void *tmp = rrealloc(x.arr,(x.siz += 4) * sizeof(*x.arr));
            if(NULL == tmp) { // fail
                error0("out of memory");
                exit(EXIT_FAILURE);
            }
            else { x.arr = tmp; } // update
        }
            /* FALLTHROUGH */
        default:
                        memmove(x.arr + 1,x.arr,x.len++ * sizeof(*x.arr));
            x.arr[0] = item;
   
            break;
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_cat(
        arr_cdd x,
        arr_cdd y)
{
    if(y.len > 0) {
        size_t xlen = x.len; // save old length
        x.len += y.len;      // sum lengths
        x = arr_cdd_fitsize(x); // grow to fit
        memcpy(x.arr + xlen,y.arr,y.len * sizeof(*y.arr));  // copy items
        memset(y.arr,0x0,y.len * sizeof(*y.arr)); // clear items
    }
    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_rem(
        arr_cdd x,
        size_t n)
{
    if((n = (n < x.len ? n : x.len)) > 0) {
        for(size_t i = 0; i < n; ++i) {
            c_declarator_free(&x.arr[x.len - 1 - i]);        }
        memset(x.arr + x.len - n,0x0,n * sizeof(*x.arr)); // clear items
        x.len -= n; // reduce length
    }

    return x;
}

__attribute__ ((unused))
__attribute__ ((warn_unused_result))
static inline arr_cdd arr_cdd_prerem(
        arr_cdd x,
        size_t n)
{
    if((n = (n < x.len) ? n : x.len) > 0) {
        for(size_t i = 0; i < n; ++i) {
            c_declarator_free(&x.arr[i]);        }
        memmove(x.arr,x.arr + n,(x.len - n) * sizeof(*x.arr)); // move items
        x.len -= n; // reduce length
    }

    return x;
}

__attribute__ ((unused))
static inline void arr_cdd_free(
        arr_cdd *x)
{
    for(size_t i = 0; i < x->len; ++i) { // free items
        c_declarator_free(&x->arr[i]);    }
    { free(x -> arr); (x -> arr) = NULL; } // free array

            {
    for(size_t i = 0; i < sv_len(x->pointer); ++i) {
        const void *_item = sv_at(x->pointer,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(x->pointer));
}
   

    *x = (arr_cdd){ 0 }; // empty
}

__attribute__ ((unused))
static inline void arr_cdd_clear(
        arr_cdd *x)
{
    for(size_t i = 0; i < x->len; ++i) { // free items
        c_declarator_free(&x->arr[i]);    }

            {
    for(size_t i = 0; i < sv_len(x->pointer); ++i) {
        const void *_item = sv_at(x->pointer,i);
        {
    char **item = (char**)(_item);
    free(*item); *item = NULL;
}
    }
    sv_free(&(x->pointer));
}
   

    if(NULL != x->arr) // zero memory for reuse
        memset(x->arr,0x0,x->siz * 4 * sizeof(*x->arr));
    x->len = x->siz = 0;
}

__attribute__ ((warn_unused_result))
arr_cdd * ( arr_cdd_box ) ( arr_cdd x  ) ;
__attribute__ ((warn_unused_result))
arr_cdd ( arr_cdd_unbox ) ( arr_cdd * x  ) ;
__attribute__ ((warn_unused_result))
int ( arr_cdd_dup ) ( const arr_cdd * x  , arr_cdd * out  ) ;
__attribute__ ((warn_unused_result))
arr_cdd ( arr_cdd_sort ) ( arr_cdd x  ) ;
__attribute__ ((warn_unused_result))
struct c_declarator * ( arr_cdd_lsearch ) ( arr_cdd x  , const struct c_declarator key  ) ;
__attribute__ ((warn_unused_result))
struct c_declarator * ( arr_cdd_bsearch ) ( arr_cdd x  , const struct c_declarator key  ) ;


#endif
