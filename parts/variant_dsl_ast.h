#ifndef PARTS_VARIANT_DSL_AST
#define PARTS_VARIANT_DSL_AST

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

enum dsl_ast_type {
DSL_AST_NODE_CONST = 0,
DSL_AST_NODE_CALL = 1,
};
#define enum_dsl_ast_type_len 2
struct dsl_ast {
    enum dsl_ast_type type;
    union {
            char* txt;
           uint64_t ix;
       } value;
            vec_dslast* child;
   };

void dsl_ast_free(struct dsl_ast *x);

__attribute__ ((warn_unused_result))
int dsl_ast_dup(const struct dsl_ast *x, struct dsl_ast *y);

#define ENUM_DSL_AST(x) DSL_AST_NODE_##x

#define DSL_AST_NODE_CONST_type .type=ENUM_DSL_AST(CONST)
#define DSL_AST_NODE_CONST_value .value.txt
#define DSL_AST_NODE_CONST_set(x) (struct dsl_ast){ DSL_AST_NODE_CONST_type, DSL_AST_NODE_CONST_value = (x) }
#define DSL_AST_NODE_CONST_xset(x, ...) (struct dsl_ast){ DSL_AST_NODE_CONST_type, DSL_AST_NODE_CONST_value = (x), __VA_ARGS__ }
#define DSL_AST_NODE_CONST_get(x) ((x) DSL_AST_NODE_CONST_value)
#define DSL_AST_NODE_CALL_type .type=ENUM_DSL_AST(CALL)
#define DSL_AST_NODE_CALL_value .value.ix
#define DSL_AST_NODE_CALL_set(x) (struct dsl_ast){ DSL_AST_NODE_CALL_type, DSL_AST_NODE_CALL_value = (x) }
#define DSL_AST_NODE_CALL_xset(x, ...) (struct dsl_ast){ DSL_AST_NODE_CALL_type, DSL_AST_NODE_CALL_value = (x), __VA_ARGS__ }
#define DSL_AST_NODE_CALL_get(x) ((x) DSL_AST_NODE_CALL_value)

#endif
