BEGIN {
    if(column_cmd == "") column_cmd = "column"
    if(sep == "") sep = "    "
    FS = " " # spaces
    n = 0
    ng = 0
}

# in-group lines
$0 ~ /\t/ {
    if(n == 0) {
        ng++
        grl[ng] = 0
    }
    gr[ng,n] = $0
    grl[ng]++
    grn[ng,n] = NR
    n++
}

# out-of-group lines
$0 !~ /\t/ {
    n = 0
    buf[NR] = $0
}

END {
    cmd = column_cmd " -t -s '	' -o '" sep "'"

    # send line groups out for processing
    for(i=1;i<=ng;i++) {
        for(j=0;j<grl[i];j++) {
            print gr[i,j] |& cmd
        }
        close(cmd, "to");
        j = 0
        while ((cmd |& getline line) > 0)
            gr[i,j++] = line
        close(cmd)
        for(j=0;j<grl[i];j++)
            buf[grn[i,j]] = gr[i,j]
    }

    # output all lines, with replacement
    for(i=1;i<=NR;i++)
        print buf[i]
}
