#include <assert.h>

#include "cmod.ext.h"
#include "parser.h"
#include "scanner.h"
#include "filestack.h"

#include <stdio.h>

#ifndef info0
#define info0(M) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((glob_pp->silent > 0) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((glob_pp->silent > 1) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((glob_pp->silent > 2) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!glob_pp->verbose) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((glob_pp->silent > 3) ? 0\
        : ((glob_pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <signal.h>
#include <stdbool.h>

#include "retval.h"

#include "docs.h"
#include "dsl.h"

#include "util_string.h"
#include "util_scanner.h"
#include "util_snippet.h"

/* logging macros */
#ifndef INFO0
#define INFO0(x) \
    { if(!(glob_pp->silent > 0)) info0(x); }
#endif
#ifndef INFO
#define INFO(x, ...) \
    { if(!(glob_pp->silent > 0)) info(x,__VA_ARGS__); }
#endif
#ifndef WARN0
#define WARN0(x) \
    { if(!(glob_pp->silent > 1)) warn0(x); }
#endif
#ifndef WARN
#define WARN(x, ...) \
    { if(!(glob_pp->silent > 1)) warn(x,__VA_ARGS__); }
#endif
#ifndef ERROR0
#define ERROR0(x) \
    { if(!(glob_pp->silent > 2)) error0(x); }
#endif
#ifndef ERROR
#define ERROR(x, ...) \
    { if(!(glob_pp->silent > 2)) error(x,__VA_ARGS__); }
#endif
#ifndef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!glob_pp->verbose)) verbose0(x); }
#endif
#ifndef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!glob_pp->verbose)) verbose(x,__VA_ARGS__); }
#endif
#ifndef DEBUG0
#define DEBUG0(x) \
    { if(!(glob_pp->silent > 3)) debug0(x); }
#endif
#ifndef DEBUG
#define DEBUG(x, ...) \
    { if(!(glob_pp->silent > 3)) debug(x,__VA_ARGS__); }
#endif

#ifndef FATAL_ERROR
#define FATAL_ERROR(x, ...) { ERROR(x, __VA_ARGS__); exit(EXIT_FAILURE); }
#endif
#ifndef FATAL_ERROR0
#define FATAL_ERROR0(x) { ERROR0(x); exit(EXIT_FAILURE); }
#endif

#if defined(__APPLE__) && defined(__MACH__)
#define DEFAULT_PREFIX "/usr/local"
#else
#define DEFAULT_PREFIX "/usr"
#endif
#define DEFAULT_INCDIR DEFAULT_PREFIX "/include"

typedef char *cmod_tmpdir;

/*
 * GLOBAL state
 */
struct param *glob_pp;  // parser-lexer parameters

/*
 * GLOBAL retry alloc settings
 */
struct cmod_ralloc_params ralloc_glob = {
    .verbose = false,
    .ntimes = 3,
    .msleep = 250,
};

static volatile sig_atomic_t terminate_in_progress = 0;
static void terminate_handler(int sig)
{
    if (terminate_in_progress)
        (void)raise(sig);
    terminate_in_progress = 1;
    char buf[1024] = { 0 };
    srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
    if (glob_pp->pretty)
        ss_cat_c(&(ss), "\x1B[38;5;11m");
    ss_cat_c(&(ss), "[" "C%" "] ", " WARN: ", "received signal ");

#ifdef _GNU_SOURCE
    ss_cat_c(&(ss), "SIG", sigabbrev_np(sig));
#else
    ss_cat_int(&(ss), sig);
#endif

    ss_cat_c(&ss, ", cleaning up before exit");
    if (glob_pp->pretty)
        ss_cat_c(&(ss), "\x1B[39m");
    ss_cat_cn1(&(ss), '\n');

    write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
    clear_global_state();
    /* reset default handler and re-raise signal */
    (void)signal(sig, SIG_DFL);
    (void)raise(sig);
}

static void ralloc_sigalrm_handler(int signum)
{
    (void)signum;
    ralloc_glob.giveup = 1;
}

static void sigalrm_handler(int sig)
{
    ralloc_sigalrm_handler(sig);
}

/* NOTE: glob_pp .verbose and .silent are not modified by signals */
static void sigchld_sigaction(int sig, siginfo_t *info, void *ucontext)
{
    (void)sig;
    (void)ucontext;
    char buf[1024] = { 0 };
    srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
    switch (info->si_code) {
    case CLD_EXITED:
        if (info->si_status == 0) {
            if (glob_pp->verbose) {
                if (glob_pp->pretty)
                    ss_cat_c(&(ss), "\x1B[38;5;12m");
                ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
                ss_cat_int(&(ss), info->si_pid);

                ss_cat_c(&ss, " exited normally");
                if (glob_pp->pretty)
                    ss_cat_c(&(ss), "\x1B[39m");
                ss_cat_cn1(&(ss), '\n');

                write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
            }
            return;
        }
        else {
            if (glob_pp->verbose) {
                if (glob_pp->pretty)
                    ss_cat_c(&(ss), "\x1B[38;5;12m");
                ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
                ss_cat_int(&(ss), info->si_pid);

                ss_cat_c(&ss, " exited with return code ");
                ss_cat_int(&ss, info->si_status);
                if (glob_pp->pretty)
                    ss_cat_c(&(ss), "\x1B[39m");
                ss_cat_cn1(&(ss), '\n');

                write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
            }
        }
        break;
    case CLD_KILLED:
        if (glob_pp->verbose) {
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[38;5;12m");
            ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
            ss_cat_int(&(ss), info->si_pid);

            switch (info->si_status) {
            case SIGALRM:
                ss_cat_c(&ss, " timed out");
                break;
            default:
                if (glob_pp->pretty)
                    ss_cat_c(&(ss), "\x1B[38;5;12m");
                ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
                ss_cat_int(&(ss), info->si_pid);

                ss_cat_c(&ss, " terminated by signal ");
#ifdef _GNU_SOURCE
                ss_cat_c(&(ss), "SIG", sigabbrev_np(info->si_status), " (",
                         sigdescr_np(info->si_status), ")");
#else
                ss_cat_int(&(ss), info->si_status);
                ss_cat_c(&(ss), " (", strsignal(info->si_status), ")");
#endif

            }
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[39m");
            ss_cat_cn1(&(ss), '\n');

            write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
        }
        break;
    case CLD_DUMPED:
        if (glob_pp->verbose) {
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[38;5;12m");
            ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
            ss_cat_int(&(ss), info->si_pid);

            switch (info->si_status) {
#if HAVE_LIBSECCOMP
            case SIGSYS:
                ss_cat_c(&ss, " made a forbidden syscall, use [trace] to find out which one");
                break;
#endif
            default:
                ss_cat_c(&ss, " terminated by signal ");
#ifdef _GNU_SOURCE
                ss_cat_c(&(ss), "SIG", sigabbrev_np(info->si_status), " (",
                         sigdescr_np(info->si_status), ")");
#else
                ss_cat_int(&(ss), info->si_status);
                ss_cat_c(&(ss), " (", strsignal(info->si_status), ")");
#endif

                ss_cat_c(&ss, " and dumped core");
            }
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[39m");
            ss_cat_cn1(&(ss), '\n');

            write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
        }
        break;
    case CLD_STOPPED:
        if (glob_pp->verbose) {
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[38;5;12m");
            ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
            ss_cat_int(&(ss), info->si_pid);

            ss_cat_c(&ss, " stopped by signal ");
#ifdef _GNU_SOURCE
            ss_cat_c(&(ss), "SIG", sigabbrev_np(info->si_status), " (",
                     sigdescr_np(info->si_status), ")");
#else
            ss_cat_int(&(ss), info->si_status);
            ss_cat_c(&(ss), " (", strsignal(info->si_status), ")");
#endif

            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[39m");
            ss_cat_cn1(&(ss), '\n');

            write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
        }
        return;
        break;
    case CLD_CONTINUED:
        if (glob_pp->verbose) {
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[38;5;12m");
            ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
            ss_cat_int(&(ss), info->si_pid);

            ss_cat_c(&ss, " (stopped) continued by signal ");
#ifdef _GNU_SOURCE
            ss_cat_c(&(ss), "SIG", sigabbrev_np(info->si_status), " (",
                     sigdescr_np(info->si_status), ")");
#else
            ss_cat_int(&(ss), info->si_status);
            ss_cat_c(&(ss), " (", strsignal(info->si_status), ")");
#endif

            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[39m");
            ss_cat_cn1(&(ss), '\n');

            write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
        }
        return;
        break;
    case CLD_TRAPPED:
#if HAVE_LIBSECCOMP
        {
            siginfo_t cinfo;
            ptrace(PTRACE_GETSIGINFO, info->si_pid, NULL, &cinfo);
            if (cinfo.si_code == SYS_SECCOMP && cinfo.si_signo == SIGSYS) {
                if (!(glob_pp->silent > 1)) {
                    char *sc_name =
                        seccomp_syscall_resolve_num_arch(cinfo.si_arch, cinfo.si_syscall);
                    if (glob_pp->pretty)
                        ss_cat_c(&(ss), "\x1B[38;5;11m");
                    ss_cat_c(&(ss), "[" "C%" "] ", " WARN: ", "child PID ");
                    ss_cat_int(&(ss), info->si_pid);

                    ss_cat_c(&ss, " made forbidden syscall ", sc_name, "(");
                    ss_cat_int(&ss, cinfo.si_syscall);
                    ss_cat_c(&ss, ")");
                    if (glob_pp->pretty)
                        ss_cat_c(&(ss), "\x1B[39m");
                    ss_cat_cn1(&(ss), '\n');

                    write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
                    free(sc_name);
                }
                return;
            }
            ptrace(PTRACE_CONT, info->si_pid, NULL, NULL);
            return;
        }
#else
        if (glob_pp->verbose) {
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[38;5;12m");
            ss_cat_c(&(ss), "[" "C%" "] ", " VERB: ", "child PID ");
            ss_cat_int(&(ss), info->si_pid);

            ss_cat_c(&ss, " (traced) has trapped");
            if (glob_pp->pretty)
                ss_cat_c(&(ss), "\x1B[39m");
            ss_cat_cn1(&(ss), '\n');

            write(STDERR_FILENO, ss_get_buffer_r(ss), ss_len(ss));
        }
#endif
        break;
    }
}

static const struct ret_f0 sigemptyset_ret_f = {
    .name = "sigemptyset"
};

static const struct ret_f0 sigaction_ret_f = {
    .name = "sigaction"
};

static const struct ret_f0 atexit_ret_f = {
    .name = "atexit"
};

struct ret_f3 {
    char *name;
    const struct ret_f3 *p[1 + 3];
};

static const struct ret_f3 setup_handlers_ret_f = {
    .name = "setup_handlers",
    .p[1] = (const struct ret_f3 *)&sigemptyset_ret_f,
    .p[2] = (const struct ret_f3 *)&sigaction_ret_f,
    .p[3] = (const struct ret_f3 *)&atexit_ret_f,
};

/* setup signals and exit handlers */
__attribute__((warn_unused_result))
static ret_t setup_handlers(void)
{
    ret_t ret = 0;
    {   // setup SIGINT handler
        struct sigaction sa = {
            .sa_handler = terminate_handler,
            .sa_flags = 0
        };
        errno = 0;
        if (sigemptyset(&sa.sa_mask) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

            goto end;
        }

        errno = 0;
        if (sigaction(SIGINT, &sa, NULL) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 1;

            goto end;
        }
    }
    {   // setup SIGTERM handler
        struct sigaction sa = {
            .sa_handler = terminate_handler,
            .sa_flags = 0
        };
        errno = 0;
        if (sigemptyset(&sa.sa_mask) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

            goto end;
        }

        errno = 0;
        if (sigaction(SIGTERM, &sa, NULL) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 1;

            goto end;
        }
    }
    {   // setup SIGALRM handler
        struct sigaction sa = {
            .sa_handler = sigalrm_handler,
            .sa_flags = 0
        };
        errno = 0;
        if (sigemptyset(&sa.sa_mask) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

            goto end;
        }

        errno = 0;
        if (sigaction(SIGALRM, &sa, NULL) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 1;

            goto end;
        }
    }
    {   // setup SIGCHLD handler
        struct sigaction sa = {
            .sa_sigaction = sigchld_sigaction,
            .sa_flags = SA_SIGINFO
        };
        errno = 0;
        if (sigemptyset(&sa.sa_mask) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 0;

            goto end;
        }

        errno = 0;
        if (sigaction(SIGCHLD, &sa, NULL) == -1) {
            ret = (unsigned char)RET_ERR_ERRNO;
            ret = ret << 8;
            ret += 1 + 1;

            goto end;
        }
    }
    if (atexit(clear_global_state) != 0) {
        ret = RET_ERR_FAIL;
        ret = ret << 8;
        ret += 1 + 2;

        goto end;
    }
 end:
    return ret;
}

static const char c_standard_types[][22] = {
    "ptrdiff_t", "size_t", "wchar_t", "jmp_buf", "sig_atomic_t", "va_list", "FILE", "fpos_t",
        "div_t", "ldiv_t", "clock_t", "time_t", "mbstate_t", "wint_t", "wctrans_t", "wctype_t",
        "fenv_t", "fexcept_t", "imaxdiv_t", "float_t", "double_t", "int8_t", "int16_t", "int32_t",
        "int64_t", "uint8_t", "uint16_t", "uint32_t", "uint64_t", "int_least8_t", "int_least16_t",
        "int_least32_t", "int_least64_t", "uint_least8_t", "uint_least16_t", "uint_least32_t",
        "uint_least64_t", "int_fast8_t", "int_fast16_t", "int_fast32_t", "int_fast64_t",
        "uint_fast8_t", "uint_fast16_t", "uint_fast32_t", "uint_fast64_t", "intptr_t", "uintptr_t",
        "intmax_t", "uintmax_t", "lldiv_t", "bool", "complex", "imaginary", "memory_order",
        "atomic_flag", "atomic_bool", "atomic_char", "atomic_schar", "atomic_uchar", "atomic_short",
        "atomic_ushort", "atomic_int", "atomic_uint", "atomic_long", "atomic_ulong", "atomic_llong",
        "atomic_ullong", "atomic_char16_t", "atomic_char32_t", "atomic_wchar_t",
        "atomic_int_least8_t", "atomic_uint_least8_t", "atomic_int_least16_t",
        "atomic_uint_least16_t", "atomic_int_least32_t", "atomic_uint_least32_t",
        "atomic_int_least64_t", "atomic_uint_least64_t", "atomic_int_fast8_t",
        "atomic_uint_fast8_t", "atomic_int_fast16_t", "atomic_uint_fast16_t", "atomic_int_fast32_t",
        "atomic_uint_fast32_t", "atomic_int_fast64_t", "atomic_uint_fast64_t", "atomic_intptr_t",
        "atomic_uintptr_t", "atomic_size_t", "atomic_ptrdiff_t", "atomic_intmax_t",
        "atomic_uintmax_t", "max_align_t", "cnd_t", "thrd_t", "tss_t", "mtx_t", "tss_dtor_t",
        "thrd_start_t", "once_flag", "char16_t", "char32_t", "errno_t", "rsize_t",
        "constraint_handler_t", "ssize_t"
};

static const char posix_types[][22] = {
    "blkcnt_t", "blksize_t", "cc_t", "clockid_t", "dev_t", "DIR", "fd_set", "fsblkcnt_t",
        "fsfilcnt_t", "gid_t", "glob_t", "iconv_t", "id_t", "in_addr_t", "in_port_t", "ino_t",
        "locale_t", "mode_t", "mqd_t", "msglen_t", "msgqnum_t", "nfds_t", "nlink_t", "off_t",
        "pid_t", "pthread_attr_t", "pthread_barrier_t", "pthread_barrierattr_t", "pthread_cond_t",
        "pthread_condattr_t", "pthread_key_t", "pthread_mutex_t", "pthread_mutexattr_t",
        "pthread_once_t", "pthread_rwlock_t", "pthread_rwlockattr_t", "pthread_spinlock_t",
        "pthread_t", "regex_t", "regmatch_t", "rlim_t", "sa_family_t", "sem_t", "siginfo_t",
        "sigset_t", "socklen_t", "speed_t", "stack_t", "suseconds_t", "tcflag_t", "timer_t",
        "uid_t", "wordexp_t"
};

static const char *grammar_parts[37] = {
    "arrlen", "comments", "def", "defined", "delay", "dsl", "dsl-def", "enum", "foreach", "free", "idcap", "include", "intop", "map", "once", "pipe", "prefix", "proto", "recall", "shorthands", "snippet", "strcmp", "strin", "strlen", "strstr", "strsub", "switch", "table", "table-get", "table-length", "table-size", "table-stack", "typedef", "unittest", "unused", "all", "none"        // last two unsorted, do not participate in binary search
};

static print_docs_f *const print_docs_parts[35] = {
    print_docs__arrlen, print_docs__comments, print_docs__def, print_docs__defined,
        print_docs__delay, print_docs__dsl, print_docs__dsl_def, print_docs__enum,
        print_docs__foreach, print_docs__free, print_docs__idcap, print_docs__include,
        print_docs__intop, print_docs__map, print_docs__once, print_docs__pipe, print_docs__prefix,
        print_docs__proto, print_docs__recall, print_docs__shorthands, print_docs__snippet,
        print_docs__strcmp, print_docs__strin, print_docs__strlen, print_docs__strstr,
        print_docs__strsub, print_docs__switch, print_docs__table, print_docs__table_get,
        print_docs__table_length, print_docs__table_size, print_docs__table_stack,
        print_docs__typedef, print_docs__unittest, print_docs__unused
};

static uint64_t find_grammar_part(const char *part, const char *optlabel)
{
    setix found = { 0 };
    {
        size_t l = (0);
        size_t r = (35);
        while (l < r) {
            size_t m = (l + r) / 2;
            if ((strcmp(part, (grammar_parts)[m]) > 0))
                l = m + 1;
            else
                r = m;
        }
        (found.set) = (l < (35)) &&
            !((strcmp(part, (grammar_parts)[l]) > 0)) && !((strcmp((grammar_parts)[l], part) > 0));
        (found.ix) = (found.set) ? l : l - (0);
    }
    if (found.set)
        return found.ix;
    /* not found, suggest and exit */
    const char *fuzzy_match;
    bool is_close;
    is_close =
        str_fuzzy_match(part, grammar_parts, (sizeof(grammar_parts) / sizeof(*(grammar_parts))),
                        strget_array, 0.75, &(fuzzy_match));

    if (is_close)
        error("invalid argument `%s` passed to %s, did you mean `%s`?",
              part, optlabel, fuzzy_match);
    else
        error("invalid argument `%s` passed to %s", part, optlabel);
    exit(EXIT_FAILURE);
}

inline static const char *hyphtostdin(const char fname[static 1])
{
    return ('-' == fname[0] && '\0' == fname[1]) ? "<stdin>" : fname;
}

// NOTE: code formatting could go here
static int write_output(FILE *fp_in, int fd_out)
{
    char buf[BUFSIZ];
    errno = 0;
    int fd_in = fileno(fp_in);
    if (fd_in < 0)
        return 1;
    ssize_t nread;
    ssize_t nwrite;
    while (!(errno = 0) && (nread = read(fd_in, buf, BUFSIZ)) > 0) {
        errno = 0;
        nwrite = write(fd_out, buf, nread);
        if (nwrite < 0 || (size_t)nwrite != (size_t)nread)
            return 2;
    }
    return (nread < 0);
}

int main(int argc, char *argv[])
{
    pid_t pid = getpid();

    /* GLOBAL retry alloc settings */
    ralloc_glob.verbose = false;
    ralloc_glob.ntimes = 3;
    ralloc_glob.msleep = 250;

    struct param *pp = NULL;
    {   // initialize
        ret_t ret1 = pp_alloc(&pp, true);
        if (ret1) {
            check_ret_t(ret1, (const struct ret_f *)&pp_alloc_ret_f);

        }

        ret_t ret2 = setup_handlers();
        if (ret2) {
            check_ret_t(ret2, (const struct ret_f *)&setup_handlers_ret_f);

        }

        if (ret1 || ret2) {
            (void)fputs("[" "C%" "] ERROR: failed init", stderr);
            exit(EXIT_FAILURE);
        }

        glob_pp = pp;   // set GLOBAL
    }
    assert(pp != NULL && glob_pp != NULL);

    {   // parse built-in stuff with vanilla scanner (into same GLOBAL state)
        yyscan_t yyscanner;
        errno = 0;
        if (yylex_init_extra(pp, &yyscanner))   // initialize scanner
            FATAL_ERROR("failed initializing scanner: %s (%d)", strerror(errno), errno);

        static const char *builtin_snippets[] = {
            ("%" "snippet !cmod:get_version:major = %{ " CPPSTR(VERSION_MAJOR) " %}\n"),
            ("%" "snippet !cmod:get_version:minor = %{ " CPPSTR(VERSION_MINOR) " %}\n"),
            ("%" "snippet !cmod:get_version:revision = %{ " CPPSTR(VERSION_REVISION) " %}\n"),
            ("%" "snippet !cmod:get_version:commit = %{ " CPPSTR(VERSION_COMMIT) " %}\n"),
            ("%" "snippet !cmod:get_prefix (sep=`_`)\n"),
            ("%" "snippet !cmod:__func__\n")
        };
        for (size_t i = 0; i < (sizeof(builtin_snippets) / sizeof(*(builtin_snippets))); ++i) { // setup built-in snippets
            const char *body = builtin_snippets[i];
            {
                YY_BUFFER_STATE _buf = yy_scan_string(body, yyscanner);
                pp->in_parse = true;
                int ret = yyparse(yyscanner, NULL);
                pp->in_parse = false;
                (void)ret;
                assert(ret == 0 && pp->nerr == 0);      // must work!
                ss_clear(pp->line);
                yy_delete_buffer(_buf, yyscanner);
            }
        }
        yylex_destroy(yyscanner);
    }

    /* booleans to deactivate all grammar parts */
    bool *const kw_inactive[35] = {
        &pp->inactive_arrlen, &pp->inactive_comments, &pp->inactive_def, &pp->inactive_defined,
            &pp->inactive_delay, &pp->inactive_dsl, &pp->inactive_dsl_def, &pp->inactive_enum,
            &pp->inactive_foreach, &pp->inactive_free, &pp->inactive_idcap, &pp->inactive_include,
            &pp->inactive_intop, &pp->inactive_map, &pp->inactive_once, &pp->inactive_pipe,
            &pp->inactive_prefix, &pp->inactive_proto, &pp->inactive_recall,
            &pp->inactive_shorthands, &pp->inactive_snippet, &pp->inactive_strcmp,
            &pp->inactive_strin, &pp->inactive_strlen, &pp->inactive_strstr, &pp->inactive_strsub,
            &pp->inactive_switch, &pp->inactive_table, &pp->inactive_table_get,
            &pp->inactive_table_length, &pp->inactive_table_size, &pp->inactive_table_stack,
            &pp->inactive_typedef, &pp->inactive_unittest, &pp->inactive_unused
    };

#if !HAVE_LIBSECCOMP
    pp->disable_seccomp = true;
#endif

    /* long option variables */
    print_docs_f *print_docs_part_f = NULL;
    enum {
        DOCS_VERSION,
        DOCS_SUMMARY,
        DOCS_KW,
        DOCS_KW_FULL,
        DOCS_FULL,
        DOCS_STDLIB
    } print_docs_type = 0;
    int print_and_exit = false;
    int report_snippet = false;
    int report_table = false;
    int report_type = false;
    int report_proto = false;
    int report_dsl = false;
    int report_fdef = false;
    int report_once = false;
    int report_enum = false;
    int no_blanks = false;
    int no_builtin_c = false;
    int no_builtin_posix = false;

    /* parse CLI options */

    struct {
        int debug;
        vec_string *disable;
        vec_string *enable;
        bool generic;
        vec_string *ipaths;
        bool keep_tmp;
        size_t eval_limit;
        size_t iter_limit;
        bool no_clobber;
        char *output;
        int silent;
        size_t steps;
        char *tests_output;
        cmod_tmpdir tmpdir;
        bool debug_isset;
        bool disable_isset;
        bool enable_isset;
        bool generic_isset;
        bool ipaths_isset;
        bool keep_tmp_isset;
        bool eval_limit_isset;
        bool iter_limit_isset;
        bool no_clobber_isset;
        bool output_isset;
        bool silent_isset;
        bool steps_isset;
        bool tests_output_isset;
        bool tmpdir_isset;
    } opts = { 0 };
    (void)opts;

    {   // set option default values
        opts.debug = 0;
        {
            (opts.disable) = sv_alloc_t(SV_PTR, 4);
            if (sv_void == (opts.disable)) {
                FATAL_ERROR0("out of memory");
            }
            const char *dflt_str = NULL;
            if (NULL != dflt_str) {
                char *str = (NULL != (dflt_str)) ? rstrdup(dflt_str) : NULL;
                if (NULL != (dflt_str) && NULL == (str)) {
                    error0("out of memory");
                    exit(EXIT_FAILURE);
                }

                ;
                if (!sv_push(&(opts.disable), &(str))) {
                    FATAL_ERROR0("out of memory");
                }
            }
        }
        {
            (opts.enable) = sv_alloc_t(SV_PTR, 4);
            if (sv_void == (opts.enable)) {
                FATAL_ERROR0("out of memory");
            }
            const char *dflt_str = NULL;
            if (NULL != dflt_str) {
                char *str = (NULL != (dflt_str)) ? rstrdup(dflt_str) : NULL;
                if (NULL != (dflt_str) && NULL == (str)) {
                    error0("out of memory");
                    exit(EXIT_FAILURE);
                }

                ;
                if (!sv_push(&(opts.enable), &(str))) {
                    FATAL_ERROR0("out of memory");
                }
            }
        }
        opts.generic = false;
        {
            (opts.ipaths) = sv_alloc_t(SV_PTR, 4);
            if (sv_void == (opts.ipaths)) {
                FATAL_ERROR0("out of memory");
            }
            const char *dflt_str = DEFAULT_INCDIR;
            if (NULL != dflt_str) {
                char *str = (NULL != (dflt_str)) ? rstrdup(dflt_str) : NULL;
                if (NULL != (dflt_str) && NULL == (str)) {
                    error0("out of memory");
                    exit(EXIT_FAILURE);
                }

                ;
                if (!sv_push(&(opts.ipaths), &(str))) {
                    FATAL_ERROR0("out of memory");
                }
            }
        }
        opts.keep_tmp = false;
        opts.eval_limit = 100;
        opts.iter_limit = 1000;
        opts.no_clobber = false;
        opts.output = NULL;

        opts.silent = 0;
        opts.steps = -1;
        opts.tests_output = NULL;

        {
            const char *tmpdir = getenv("TMPDIR");      // POSIX
            if (NULL == tmpdir)
                tmpdir = "/tmp";
            else {      // TODO: check contents of TMPDIR!
            }
            if (NULL != (tmpdir) && NULL == ((opts.tmpdir) = rstrdup(tmpdir))) {
                error0("out of memory");
                exit(EXIT_FAILURE);
            }
        }
    }
    opterr = 1;

    if (argc == 1) {    // no arguments at all
        /* noop */ }
    else {      // process option tables
        const char *optlabel = NULL;
        (void)optlabel;

        struct option optlist[] = {
            {"help", no_argument, NULL, 0},     // print usage information
            {"usage", no_argument, NULL, 0},    // alias for --help
            {"verbose", no_argument, NULL, 0},  // print diagnostic messages
            /* print version */
            {"version", no_argument, &print_and_exit, true},
            /* print C% keyword summary */
            {"docs", no_argument, &print_and_exit, true},
            /* print syntax summary for a single C% keyword */
            {"docs-kw", required_argument, &print_and_exit, true},
            /* print full documentation for a single C% keyword */
            {"docs-kw-full", required_argument, &print_and_exit, true},
            /* print full C% documentation */
            {"docs-full", no_argument, &print_and_exit, true},
            /* print standard library documentation */
            {"docs-stdlib", no_argument, &print_and_exit, true},
            /* pretty-print logging output */
            {"color", no_argument, &pp->pretty, true},
            /* do not pretty-print logging output */
            {"no-color", no_argument, &pp->pretty, false},
            /* suppress blank lines in output */
            {"no-blanks", no_argument, &no_blanks, true},
            /* no built-in standard C types */
            {"no-builtin-c", no_argument, &no_builtin_c, true},
            /* no built-in standard POSIX types */
            {"no-builtin-posix", no_argument, &no_builtin_posix, true},
            /* disable keywords in scanner */
            {"scanner-disable", no_argument, &pp->scanner_disable, true},
            /* disable seccomp syscall filtering */
            {"disable-seccomp", no_argument, &pp->disable_seccomp, true},
            /* exit on first error */
            {"exit-on-first-error", no_argument, &pp->exit_on_first_err, true},
            /* report parsed snippets */
            {"report-snippets", no_argument, &report_snippet, true},
            /* report parsed tables */
            {"report-tables", no_argument, &report_table, true},
            /* report parsed type definitions */
            {"report-types", no_argument, &report_type, true},
            /* report parsed function prototypes */
            {"report-protos", no_argument, &report_proto, true},
            /* report parsed domain-specific languages */
            {"report-dsls", no_argument, &report_dsl, true},
            /* report parsed function definitions */
            {"report-fdefs", no_argument, &report_fdef, true},
            /* report parsed include/repeat guards */
            {"report-onces", no_argument, &report_once, true},
            /* report parsed enum constants */
            {"report-enums", no_argument, &report_enum, true},
            /* alias for --quiet */
            {"silent", no_argument, NULL, 'q'},
            /* parser debugging level */
            {"debug", no_argument, NULL, 'D'},
            /* disable syntax element: <keyword>, all, none, comments, shorthands or idcap */
            {"disable", required_argument, NULL, 'd'},
            /* enable syntax element: <keyword>, all, none, comments, shorthands or idcap */
            {"enable", required_argument, NULL, 'e'},
            /* disable C-specific keywords */
            {"generic", no_argument, NULL, 'g'},
            /* add directory to include search path */
            {"include", required_argument, NULL, 'I'},
            /* keep temporary files */
            {"keep", no_argument, NULL, 'k'},
            /* parser re-evaluation limit */
            {"eval-limit", required_argument, NULL, 'l'},
            /* iteration limit */
            {"iter-limit", required_argument, NULL, 'L'},
            /* do not overwrite existing output file */
            {"no-clobber", no_argument, NULL, 'n'},
            /* output filename */
            {"output", required_argument, NULL, 'o'},
            /* parser logging suppression level */
            {"quiet", no_argument, NULL, 'q'},
            /* perform up to this number of parsing steps */
            {"step", required_argument, NULL, 's'},
            /* output filename for unit tests */
            {"tests-output", required_argument, NULL, 't'},
            /* directory for temporary files */
            {"tmpdir", required_argument, NULL, 'T'},
            {NULL, 0, NULL, 0}
        };

        opterr = 0;     // silent
        /* build optstring */
        const char optstring[] =
#if 1
            ":"
#endif
            "D" "d:" "e:" "g" "I:" "k" "l:" "L:" "n" "o:" "q" "s:" "t:" "T:";

        int opt, longidx;
        bool getopt_verbose = false;    // --verbose passed
        bool longopt_isset[26] = { 0 };
        char *longopt_arg[26] = { 0 };
        (void)longopt_isset;
        (void)longopt_arg;

        while ((opt = getopt_long(argc, argv, optstring, optlist, &longidx)) != -1) {
            if (opt == 0) {     /* long options with no short option */
                if (longidx < 26)
                    longopt_isset[longidx] = true;
                if (longidx == 0 || longidx == 1) {     // --help or --usage
                    print_usage();

                    {
                        printf("Available parameters: (* = required) (+ = multiple) [default]\n");

                        const int maxlen = 19;
                        (void)maxlen;

                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'D',
                               maxlen, "debug",
                               "     ", "+ ", "parser debugging level", " [" "0" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'd',
                               maxlen, "disable",
                               "<arg>",
                               "+ ",
                               "disable syntax element: <keyword>, all, none, comments, shorthands or idcap",
                               "");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'e',
                               maxlen, "enable",
                               "<arg>",
                               "+ ",
                               "enable syntax element: <keyword>, all, none, comments, shorthands or idcap",
                               "");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'l',
                               maxlen, "eval-limit",
                               "<arg>", "  ", "parser re-evaluation limit", " [" "100" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'g',
                               maxlen, "generic",
                               "     ", "  ", "disable C-specific keywords", " [" "false" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'I',
                               maxlen, "include",
                               "<arg>",
                               "+ ",
                               "add directory to include search path", " [" DEFAULT_INCDIR "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'L',
                               maxlen, "iter-limit",
                               "<arg>", "  ", "iteration limit", " [" "1000" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'k',
                               maxlen, "keep",
                               "     ", "  ", "keep temporary files", " [" "false" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'n',
                               maxlen, "no-clobber",
                               "     ",
                               "  ", "do not overwrite existing output file", " [" "false" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'o',
                               maxlen, "output", "<arg>", "  ", "output filename", "");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'q',
                               maxlen, "quiet",
                               "     ", "+ ", "parser logging suppression level", " [" "0" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 's',
                               maxlen, "step",
                               "<arg>",
                               "  ", "perform up to this number of parsing steps", " [" "-1" "]");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 't',
                               maxlen, "tests-output",
                               "<arg>", "  ", "output filename for unit tests", "");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
                               "      ", 'T',
                               maxlen, "tmpdir",
                               "<arg>", "  ", "directory for temporary files", " [" "/tmp" "]");

                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "color", "     ", "  ", "pretty-print logging output", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "disable-seccomp",
                               "     ", "  ", "disable seccomp syscall filtering", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "docs", "     ", "  ", "print C% keyword summary", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "docs-full",
                               "     ", "  ", "print full C% documentation", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "docs-kw",
                               "<arg>", "  ", "print syntax summary for a single C% keyword", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
                               "      ",
                               maxlen, "docs-kw-full",
                               "<arg>", "  ", "print full documentation for a single C% keyword",
                               "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "docs-stdlib", "     ", "  ", "print standard library documentation",
                               "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "exit-on-first-error", "     ", "  ", "exit on first error", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "help",
                               "     ", "  ", "print usage information", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "no-blanks",
                               "     ", "  ", "suppress blank lines in output", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "no-builtin-c", "     ", "  ", "no built-in standard C types", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "no-builtin-posix", "     ", "  ",
                               "no built-in standard POSIX types", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "no-color",
                               "     ", "  ", "do not pretty-print logging output", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-dsls", "     ", "  ",
                               "report parsed domain-specific languages", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-enums", "     ", "  ", "report parsed enum constants", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-fdefs", "     ", "  ", "report parsed function definitions",
                               "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-onces", "     ", "  ", "report parsed include/repeat guards",
                               "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-protos", "     ", "  ", "report parsed function prototypes",
                               "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-snippets", "     ", "  ", "report parsed snippets", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-tables", "     ", "  ", "report parsed tables", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "report-types", "     ", "  ", "report parsed type definitions", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen,
                               "scanner-disable", "     ", "  ", "disable keywords in scanner", "");
                        printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n", "      ", 'q', maxlen,
                               "silent", "     ", "  ", "alias for --quiet", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "usage",
                               "     ", "  ", "alias for --help", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "verbose",
                               "     ", "  ", "print diagnostic messages", "");
                        printf("%s    --%-*s %s%s " ":" " %s%s" "\n", "      ", maxlen, "version",
                               "     ", "  ", "print version", "");
                    }
                    goto cleanup;       // one-shot
                }
                else if (longidx == 2) {        // --verbose
                    getopt_verbose = true;
                }
                else if (longidx >= 3) {
                    if (longidx < 26) {
                        if (NULL != (optarg) && NULL == ((longopt_arg[longidx]) = rstrdup(optarg))) {
                            error0("out of memory");
                            exit(EXIT_FAILURE);
                        }
                    }

                    longidx -= 3;       // subtract default

                    /* set optlabel for each long option */
                    switch (longidx) {
                    case 0:
                        optlabel = "--version";
                        break;
                    case 1:
                        optlabel = "--docs";
                        break;
                    case 2:
                        optlabel = "--docs-kw";
                        break;
                    case 3:
                        optlabel = "--docs-kw-full";
                        break;
                    case 4:
                        optlabel = "--docs-full";
                        break;
                    case 5:
                        optlabel = "--docs-stdlib";
                        break;
                    case 6:
                        optlabel = "--color";
                        break;
                    case 7:
                        optlabel = "--no-color";
                        break;
                    case 8:
                        optlabel = "--no-blanks";
                        break;
                    case 9:
                        optlabel = "--no-builtin-c";
                        break;
                    case 10:
                        optlabel = "--no-builtin-posix";
                        break;
                    case 11:
                        optlabel = "--scanner-disable";
                        break;
                    case 12:
                        optlabel = "--disable-seccomp";
                        break;
                    case 13:
                        optlabel = "--exit-on-first-error";
                        break;
                    case 14:
                        optlabel = "--report-snippets";
                        break;
                    case 15:
                        optlabel = "--report-tables";
                        break;
                    case 16:
                        optlabel = "--report-types";
                        break;
                    case 17:
                        optlabel = "--report-protos";
                        break;
                    case 18:
                        optlabel = "--report-dsls";
                        break;
                    case 19:
                        optlabel = "--report-fdefs";
                        break;
                    case 20:
                        optlabel = "--report-onces";
                        break;
                    case 21:
                        optlabel = "--report-enums";
                        break;
                    case 22:
                        optlabel = "--silent";
                        break;
                    }

                    /* user-defined actions */
                    switch (longidx) {
                    case 0:    // --version
                        print_docs_type = DOCS_VERSION;
                        break;
                    case 1:    // --docs
                        print_docs_type = DOCS_SUMMARY;
                        break;
                    case 2:    // --docs-kw
                        print_docs_type = DOCS_KW;
                        print_docs_part_f = print_docs_parts[find_grammar_part(optarg, optlabel)];
                        break;
                    case 3:    // --docs-kw-full
                        print_docs_type = DOCS_KW_FULL;
                        print_docs_part_f = print_docs_parts[find_grammar_part(optarg, optlabel)];
                        break;
                    case 4:    // --docs-full
                        print_docs_type = DOCS_FULL;
                        break;
                    case 5:    // --docs-stdlib
                        print_docs_type = DOCS_STDLIB;
                        break;
                    }
                }
            }
            else if ('?' == opt || ':' == opt) {        // getopt error
                bool missing_arg = (':' == opt);
#define OPBUFSIZ 128
                if (optopt == 0 || optopt == true) {    // long option
                    size_t oplen = 0;   // length until '='
                    const char *optxt = argv[optind - 1] + 2;   // start after '--'
                    for (const char *c = optxt; '\0' != *c && *c != '='; ++c, ++oplen) ;
                    char opbuf[oplen + 1];      // VLA
                    for (size_t i = 0; i < oplen; ++i)
                        opbuf[i] = optxt[i];
                    opbuf[oplen] = '\0';
                    if (missing_arg) {
                        ERROR("option --%s requires an argument", opbuf);
                    }
                    else {
                        size_t hi = (sizeof(optlist) / sizeof(*(optlist))) - 1;
                        const char *fuzzy_match;
                        bool is_close;
                        is_close =
                            str_fuzzy_match(opbuf, optlist, hi, strget_array_opt, 0.75,
                                            &(fuzzy_match));

                        if (NULL != fuzzy_match && strstr(fuzzy_match, opbuf) != fuzzy_match
                            && is_close) {
                            ERROR("unknown option `--%s`, did you mean `--%s`?", opbuf,
                                  fuzzy_match);
                        }
                        else {
                            ERROR("unknown option `--%s`", opbuf);
                        }
                    }

                }
                else {  // short option
                    if (missing_arg) {
                        ERROR("option -%c requires an argument", optopt);
                    }
                    else {
                        ERROR("unknown option -%c", optopt);
                    }

                }
                exit(EXIT_FAILURE);
            }
            else {      /* long options with short option */

                switch (opt) {
                case 'D':
                    optlabel = "-D/--debug";

                    opts.debug_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    if (optarg && strtoint_check(optarg, &opts.debug)) {
                        error("not an integer, out of bounds or empty argument passed to %s: %s",
                              optlabel, optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'd':
                    optlabel = "-d/--disable";

                    opts.disable_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        char *str = (NULL != (optarg)) ? rstrdup(optarg) : NULL;
                        if (NULL != (optarg) && NULL == (str)) {
                            error0("out of memory");
                            exit(EXIT_FAILURE);
                        }

                        ;
                        if (!sv_push(&(opts.disable), &(str))) {
                            FATAL_ERROR0("out of memory");
                        }
                    }
                    break;
                case 'e':
                    optlabel = "-e/--enable";

                    opts.enable_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        char *str = (NULL != (optarg)) ? rstrdup(optarg) : NULL;
                        if (NULL != (optarg) && NULL == (str)) {
                            error0("out of memory");
                            exit(EXIT_FAILURE);
                        }

                        ;
                        if (!sv_push(&(opts.enable), &(str))) {
                            FATAL_ERROR0("out of memory");
                        }
                    }
                    break;
                case 'g':
                    optlabel = "-g/--generic";

                    if (opts.generic_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.generic_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    opts.generic = !false;
                    break;
                case 'I':
                    optlabel = "-I/--include";

                    opts.ipaths_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        char *str = (NULL != (optarg)) ? rstrdup(optarg) : NULL;
                        if (NULL != (optarg) && NULL == (str)) {
                            error0("out of memory");
                            exit(EXIT_FAILURE);
                        }

                        ;
                        if (!sv_push(&(opts.ipaths), &(str))) {
                            FATAL_ERROR0("out of memory");
                        }
                    }
                    break;
                case 'k':
                    optlabel = "-k/--keep";

                    if (opts.keep_tmp_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.keep_tmp_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    opts.keep_tmp = !false;
                    break;
                case 'l':
                    optlabel = "-l/--eval-limit";

                    if (opts.eval_limit_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.eval_limit_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    if (optarg && strtosize_check(optarg, &opts.eval_limit)) {
                        error
                            ("not an unsigned integer, out of bounds or empty argument passed to %s: %s",
                             optlabel, optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'L':
                    optlabel = "-L/--iter-limit";

                    if (opts.iter_limit_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.iter_limit_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    if (optarg && strtosize_check(optarg, &opts.iter_limit)) {
                        error
                            ("not an unsigned integer, out of bounds or empty argument passed to %s: %s",
                             optlabel, optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'n':
                    optlabel = "-n/--no-clobber";

                    if (opts.no_clobber_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.no_clobber_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    opts.no_clobber = !false;
                    break;
                case 'o':
                    optlabel = "-o/--output";

                    if (opts.output_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.output_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        free(opts.output);
                        (opts.output) = NULL;
                    }
                    if (NULL != (optarg) && NULL == ((opts.output) = rstrdup(optarg))) {
                        error0("out of memory");
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'q':
                    optlabel = "-q/--quiet";

                    opts.silent_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    if (optarg && strtoint_check(optarg, &opts.silent)) {
                        error("not an integer, out of bounds or empty argument passed to %s: %s",
                              optlabel, optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 's':
                    optlabel = "-s/--step";

                    if (opts.steps_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.steps_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    if (optarg && strtosize_check(optarg, &opts.steps)) {
                        error
                            ("not an unsigned integer, out of bounds or empty argument passed to %s: %s",
                             optlabel, optarg);
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 't':
                    optlabel = "-t/--tests-output";

                    if (opts.tests_output_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.tests_output_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        free(opts.tests_output);
                        (opts.tests_output) = NULL;
                    }
                    if (NULL != (optarg) && NULL == ((opts.tests_output) = rstrdup(optarg))) {
                        error0("out of memory");
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'T':
                    optlabel = "-T/--tmpdir";

                    if (opts.tmpdir_isset) {
                        error("duplicate option %s", optlabel);
                        exit(EXIT_FAILURE);
                    }
                    opts.tmpdir_isset = true;

                    /* option default actions, overriden by user-defined actions below */
                    {
                        free(opts.tmpdir);
                        (opts.tmpdir) = NULL;
                    }
                    if (NULL != (optarg) && NULL == ((opts.tmpdir) = rstrdup(optarg))) {
                        error0("out of memory");
                        exit(EXIT_FAILURE);
                    }
                    break;
                }

                /* user-defined actions */
                switch (opt) {
                case 'D':      // --debug
                    pp->debug = ++opts.debug;   // assumes default 0 for option
                    if (pp->debug >= 3) {
#if YYDEBUG
                        yydebug = 1;
#endif
                    }
                    break;
                case 'g':
                    no_builtin_c = true;        // imply --no-builtin-c
                    no_builtin_posix = true;    // imply --no-builtin-posix
                    pp->scanner_disable = true; // imply --scanner-disable
                    // deactivate all C keywords
                    // NOTE: cannot use array kw_inactive, since it's sorted for binary search
                    pp->inactive_typedef = true;
                    pp->inactive_proto = true;
                    pp->inactive_def = true;
                    pp->inactive_unused = true;
                    pp->inactive_prefix = true;
                    pp->inactive_enum = true;
                    pp->inactive_strin = true;
                    pp->inactive_foreach = true;
                    pp->inactive_switch = true;
                    pp->inactive_free = true;
                    pp->inactive_arrlen = true;
                    break;
                case 'k':
                    pp->keep_tmp = opts.keep_tmp;
                    break;      // assumes default false for option
                case 'l':
                    if (!(opts.eval_limit >= 1)) {
                        error0("Condition not fulfilled: opts.eval_limit >= 1");
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 'q':
                    pp->silent = ++opts.silent;
                    break;      // assumes default 0 for option
                case 's':
                    if (opts.steps == 0) {      // cat only
                        no_builtin_c = true;
                        no_builtin_posix = true;
                        pp->scanner_disable = true;
                        for (size_t i = 0; i < 35; ++i)
                            *kw_inactive[i] = true;
                    }
                    break;
                }
            }
        }

        /* check if required options are present */

        /* if verbose, print all option values */
        if (getopt_verbose) {
            pp->verbose = true;
            ralloc_glob.verbose = true;

            {
                verbose0("Parameter summary: --flag <bool> = value .");

                const int maxlen = 19;
                (void)maxlen;

                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%" "d", opts.debug);
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'D', maxlen, "debug",
                            opts.debug_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (sv_len(opts.disable) == 0) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else {
                        srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
                        for (size_t i = 0; i < sv_len(opts.disable); ++i) {
                            if (i > 0)
                                ss_cat_c(&ss, " : ");
                            ss_cat_c(&ss, sv_at_ptr(opts.disable, i));
                        }
                        size_t len = ss_len(ss);
                        memmove(buf, ss_get_buffer(ss), len);   // XXX: overwrites srt_string header
                        buf[len] = '\0';
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'd', maxlen, "disable",
                            opts.disable_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (sv_len(opts.enable) == 0) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else {
                        srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
                        for (size_t i = 0; i < sv_len(opts.enable); ++i) {
                            if (i > 0)
                                ss_cat_c(&ss, " : ");
                            ss_cat_c(&ss, sv_at_ptr(opts.enable, i));
                        }
                        size_t len = ss_len(ss);
                        memmove(buf, ss_get_buffer(ss), len);   // XXX: overwrites srt_string header
                        buf[len] = '\0';
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'e', maxlen, "enable",
                            opts.enable_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%" "zu", opts.eval_limit);
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'l', maxlen, "eval-limit",
                            opts.eval_limit_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%s", opts.generic ? "<true>" : "<false>");
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'g', maxlen, "generic",
                            opts.generic_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (sv_len(opts.ipaths) == 0) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else {
                        srt_string *ss = ss_alloc_into_ext_buf(buf, sizeof(buf));
                        for (size_t i = 0; i < sv_len(opts.ipaths); ++i) {
                            if (i > 0)
                                ss_cat_c(&ss, " : ");
                            ss_cat_c(&ss, sv_at_ptr(opts.ipaths, i));
                        }
                        size_t len = ss_len(ss);
                        memmove(buf, ss_get_buffer(ss), len);   // XXX: overwrites srt_string header
                        buf[len] = '\0';
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'I', maxlen, "include",
                            opts.ipaths_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%" "zu", opts.iter_limit);
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'L', maxlen, "iter-limit",
                            opts.iter_limit_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%s", opts.keep_tmp ? "<true>" : "<false>");
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'k', maxlen, "keep",
                            opts.keep_tmp_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%s", opts.no_clobber ? "<true>" : "<false>");
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'n', maxlen, "no-clobber",
                            opts.no_clobber_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (NULL == opts.output) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else if (snprintf(buf, sizeof(buf), "%s", opts.output) >= (int)sizeof(buf)) {
                        (void)snprintf(buf + sizeof(buf) - 4, 4, "...");
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'o', maxlen, "output",
                            opts.output_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%" "d", opts.silent);
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'q', maxlen, "quiet",
                            opts.silent_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    (void)snprintf(buf, sizeof(buf), "%" "zu", opts.steps);
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 's', maxlen, "step",
                            opts.steps_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (NULL == opts.tests_output) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else if (snprintf(buf, sizeof(buf), "%s", opts.tests_output) >=
                             (int)sizeof(buf)) {
                        (void)snprintf(buf + sizeof(buf) - 4, 4, "...");
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 't', maxlen, "tests-output",
                            opts.tests_output_isset ? "  <set>" : "<unset>", "", buf, " .");
                }
                {
                    char buf[1024] = "<non-representable>";
                    if (NULL == opts.tmpdir) {
                        (void)snprintf(buf, sizeof(buf), "<none>");
                    }
                    else if (snprintf(buf, sizeof(buf), "%s", opts.tmpdir) >= (int)sizeof(buf)) {
                        (void)snprintf(buf + sizeof(buf) - 4, 4, "...");
                    }
                    verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                            "      ", 'T', maxlen, "tmpdir",
                            opts.tmpdir_isset ? "  <set>" : "<unset>", "", buf, " .");
                }

                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "color", longopt_isset[9] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "disable-seccomp",
                        longopt_isset[15] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "docs", longopt_isset[4] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "docs-full", longopt_isset[7] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s %s %s%s", "      ",
                        maxlen, "docs-kw",
                        longopt_isset[5] ? "  <set>" : "<unset>", "",
                        longopt_isset[5] ? "=" : "", longopt_isset[5] ? longopt_arg[5] : "", "");
                verbose("%s    --%-*s %s%s %s %s%s", "      ",
                        maxlen, "docs-kw-full",
                        longopt_isset[6] ? "  <set>" : "<unset>", "",
                        longopt_isset[6] ? "=" : "", longopt_isset[6] ? longopt_arg[6] : "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "docs-stdlib",
                        longopt_isset[8] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "exit-on-first-error",
                        longopt_isset[16] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "help", longopt_isset[0] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "no-blanks", longopt_isset[11] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "no-builtin-c",
                        longopt_isset[12] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "no-builtin-posix",
                        longopt_isset[13] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "no-color", longopt_isset[10] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-dsls",
                        longopt_isset[21] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-enums",
                        longopt_isset[24] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-fdefs",
                        longopt_isset[22] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-onces",
                        longopt_isset[23] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-protos",
                        longopt_isset[20] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-snippets",
                        longopt_isset[17] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-tables",
                        longopt_isset[18] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "report-types",
                        longopt_isset[19] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "scanner-disable",
                        longopt_isset[14] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "silent", longopt_isset[25] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "usage", longopt_isset[1] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "verbose", longopt_isset[2] ? "  <set>" : "<unset>", "", "", "");
                verbose("%s    --%-*s %s%s " "" " %s%s", "      ",
                        maxlen, "version", longopt_isset[3] ? "  <set>" : "<unset>", "", "", "");
            }
        }

        const size_t len_optab_long = 26;
        {
            const size_t _len = len_optab_long;
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _4 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                {
                    free((((longopt_arg))[_4]));
                    ((((longopt_arg))[_4])) = NULL;
            }}
        }
    }

#if ! YYDEBUG
    if (pp->debug >= 3)
        DEBUG0("parser tracing not compiled-in");
#endif

    if (print_and_exit) {
        switch (print_docs_type) {
        case DOCS_VERSION:
            puts(VERSION_STRING);
            break;
        case DOCS_SUMMARY:
            print_docs(false, false);
            break;
        case DOCS_KW:
            print_docs_part_f(true, false);
            break;
        case DOCS_KW_FULL:
            print_docs_part_f(true, true);
            break;
        case DOCS_FULL:
            print_docs(true, true);
            break;
        case DOCS_STDLIB:
            print_docs_stdlib();
            break;
        }
        goto cleanup;
    }

    /* set parameters from options (honoring option defaults) */
    pp->eval_limit = opts.eval_limit;
    pp->iter_limit = opts.iter_limit;

    /* setup include paths */
    pp->sv_ipath = opts.ipaths;
    opts.ipaths = NULL; // hand-over

    /* setup active/inactive keywords */
    if (opts.disable_isset) {
        {
            const size_t _len = sv_len(opts.disable);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _0 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if ((NULL != ((*(const char **)sv_at(opts.disable, _0)))
                     && 'a' == ((*(const char **)sv_at(opts.disable, _0)))[0]
                     && 'l' == ((*(const char **)sv_at(opts.disable, _0)))[1]
                     && 'l' == ((*(const char **)sv_at(opts.disable, _0)))[2]
                     && '\0' == ((*(const char **)sv_at(opts.disable, _0)))[3])
                    || (NULL != ((*(const char **)sv_at(opts.disable, _0)))
                        && 'n' == ((*(const char **)sv_at(opts.disable, _0)))[0]
                        && 'o' == ((*(const char **)sv_at(opts.disable, _0)))[1]
                        && 'n' == ((*(const char **)sv_at(opts.disable, _0)))[2]
                        && 'e' == ((*(const char **)sv_at(opts.disable, _0)))[3]
                        && '\0' == ((*(const char **)sv_at(opts.disable, _0)))[4])
                    ) {
                    for (size_t i = 0; i < 35; ++i)
                        *kw_inactive[i] = true;
                }
                else {
                    size_t ix =
                        find_grammar_part((*(const char **)sv_at(opts.disable, _0)),
                                          "-d/--disable");
                    *kw_inactive[ix] = true;
                }
            }
        }
    }
    if (opts.enable_isset) {
        {
            const size_t _len = sv_len(opts.enable);
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _1 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                if ((NULL != ((*(const char **)sv_at(opts.enable, _1)))
                     && 'a' == ((*(const char **)sv_at(opts.enable, _1)))[0]
                     && 'l' == ((*(const char **)sv_at(opts.enable, _1)))[1]
                     && 'l' == ((*(const char **)sv_at(opts.enable, _1)))[2]
                     && '\0' == ((*(const char **)sv_at(opts.enable, _1)))[3])
                    || (NULL != ((*(const char **)sv_at(opts.enable, _1)))
                        && 'n' == ((*(const char **)sv_at(opts.enable, _1)))[0]
                        && 'o' == ((*(const char **)sv_at(opts.enable, _1)))[1]
                        && 'n' == ((*(const char **)sv_at(opts.enable, _1)))[2]
                        && 'e' == ((*(const char **)sv_at(opts.enable, _1)))[3]
                        && '\0' == ((*(const char **)sv_at(opts.enable, _1)))[4])
                    ) {
                    for (size_t i = 0; i < 35; ++i)
                        *kw_inactive[i] = false;
                }
                else {
                    size_t ix =
                        find_grammar_part((*(const char **)sv_at(opts.enable, _1)), "-e/--enable");
                    *kw_inactive[ix] = false;
                }
            }
        }
    }

    /* setup built-in type declarations */
    if (!no_builtin_c) {        // add built-in C types
        // NOTE: no need to reserve space, as init size of sv_dtype is large enough
        {
            const size_t _len = (sizeof(c_standard_types) / sizeof(*(c_standard_types)));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _2 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct decl item = {.name = (((c_standard_types))[_2]) };
                {
                    const srt_string *_ss = ss_crefs((item).name);
                    if (!sv_push(&(pp->sv_dtype), &(item))
                        || !shm_insert_su(&(pp->shm_dtype), _ss, sv_len(pp->sv_dtype) - 1)) {
                        FATAL_ERROR0("out of memory");
                    }
                }
            }
        }
    }
    if (!no_builtin_posix) {    // add built-in POSIX types
        // NOTE: no need to reserve space, as init size of sv_dtype is large enough
        {
            const size_t _len = (sizeof(posix_types) / sizeof(*(posix_types)));
            for (size_t _idx = 0; _idx < _len; ++_idx) {
                const size_t _3 = _idx;
                const size_t _idx = 0;
                (void)_idx;
                struct decl item = {.name = (((posix_types))[_3]) };
                {
                    const srt_string *_ss = ss_crefs((item).name);
                    if (!sv_push(&(pp->sv_dtype), &(item))
                        || !shm_insert_su(&(pp->shm_dtype), _ss, sv_len(pp->sv_dtype) - 1)) {
                        FATAL_ERROR0("out of memory");
                    }
                }
            }
        }
    }

    {   // parsing

/* redefine logging macros */
#undef INFO0
#define INFO0(x) \
    { if(!(glob_pp->silent > 0))\
        yyinfo(NULL,yyscanner,NULL,x); }
#undef INFO
#define INFO(x, ...) \
    { if(!(glob_pp->silent > 0))\
        yyinfo(NULL,yyscanner,NULL,x,__VA_ARGS__); }
#undef WARN0
#define WARN0(x) \
    { if(!(glob_pp->silent > 1))\
        yywarn(NULL,yyscanner,NULL,x); }
#undef WARN
#define WARN(x, ...) \
    { if(!(glob_pp->silent > 1))\
        yywarn(NULL,yyscanner,NULL,x,__VA_ARGS__); }
#undef ERROR0
#define ERROR0(x) \
    { if(!(glob_pp->silent > 2))\
        yyerror(NULL,yyscanner,NULL,x); }
#undef ERROR
#define ERROR(x, ...) \
    { if(!(glob_pp->silent > 2))\
        yyerror(NULL,yyscanner,NULL,x,__VA_ARGS__); }
#undef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!glob_pp->verbose))\
        yyverbose(NULL,yyscanner,NULL,x); }
#undef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!glob_pp->verbose))\
        yyverbose(NULL,yyscanner,NULL,x,__VA_ARGS__); }
#undef DEBUG0
#define DEBUG0(x) \
    { if(!(glob_pp->silent > 3))\
        yydebug(NULL,yyscanner,NULL,x); }
#undef DEBUG
#define DEBUG(x, ...) \
    { if(!(glob_pp->silent > 3))\
        yydebug(NULL,yyscanner,NULL,x,__VA_ARGS__); }

        yyscan_t yyscanner;
        errno = 0;
        if (yylex_init_extra(pp, &yyscanner))   // initialize scanner
            FATAL_ERROR("failed initializing scanner: %s (%d)", strerror(errno), errno);

        /* init input files */
        // XXX: opens all input files at start, system imposes limits on this
        if (optind >= argc) {   // no arguments after options -> use stdin
            yyset_in(stdin, yyscanner);
            ret_t ret = pushfile(yyscanner, pp, stdin, "<stdin>", NULL, true, false, false);
            if (ret) {
                {
                    check_ret_t(ret, (const struct ret_f *)&pushfile_ret_f);

                }
                FATAL_ERROR0("failed pushing buffer into stack");
            }
        }
        else
            for (int i = optind, seen_stdin = 0; i < argc; ++i) {       // push all filenames
                const char *fname = argv[argc + optind - i - 1];        // in reverse order
                errno = 0;
                FILE *fpin = NULL;
                if ('-' == fname[0] && '\0' == fname[1]) {      // stdin
                    if (seen_stdin++) {
                        WARN0("standard input already specified as input file, skipping");
                        continue;
                    }
                    fpin = stdin;
                    fname = "<stdin>";
                }
                else if (NULL == (fpin = fopen(fname, "r"))) {
                    FATAL_ERROR("failed opening input file for read \"%s\": %s (%d)",
                                fname, strerror(errno), errno);
                }
                yyset_in(fpin, yyscanner);      // set input file pointer, last called for first file
                ret_t ret = pushfile(yyscanner, pp, fpin, fname, fname, true, false, false);
                if (ret) {
                    {
                        check_ret_t(ret, (const struct ret_f *)&pushfile_ret_f);

                    }
                    FATAL_ERROR0("failed pushing buffer into stack");
                }
            }

        /* init output file */
        int fd_out = STDOUT_FILENO;     // default is stdout
        if (NULL != opts.output && strcmp(opts.output, "-") != 0) {
            int fd = -1;
            ret_t ret = file_open_write(opts.output, !opts.no_clobber, &fd);
            if (ret == 0 && fd != -1) { // success
                fd_out = fd;
                pp->nameout = (NULL != (opts.output)) ? rstrdup(opts.output) : NULL;
                if (NULL != (opts.output) && NULL == (pp->nameout)) {
                    FATAL_ERROR0("out of memory");
                }

            }
            else if (ret == 0 && fd == -1) {    // exists, non-fatal
                WARN("not overwriting existing output file \"%s\", using standard output",
                     opts.output);
            }
            else {      // failed, non-fatal
                WARN("failed opening output file \"%s\", using standard output", opts.output);
                {
                    check_ret_t(ret, (const struct ret_f *)&file_open_write_ret_f);

                }
            }
        }

        /* init tests output */
        pp->fpout_tests = NULL; // no output if not requested
        if (opts.tests_output_isset) {
            pp->fpout_tests = stderr;   // default is stderr
            FILE *fp = NULL;
            int fd = -1;
            ret_t ret = file_open_write(opts.tests_output, !opts.no_clobber, &fd);
            if (ret == 0 && fd != -1 && !(errno = 0) && NULL != (fp = fdopen(fd, "w"))) {       // success
                VERBOSE("tests output file is \"%s\"", opts.tests_output);
                pp->fpout_tests = fp;
            }
            else if (ret == 0 && fd == -1) {    // exists, non-fatal
                WARN("not overwriting existing tests output file \"%s\", using standard error",
                     opts.tests_output);
            }
            else {      // failed, non-fatal
                WARN("failed opening tests output file \"%s\", using standard error",
                     opts.tests_output);
                {
                    check_ret_t(ret, (const struct ret_f *)&file_open_write_ret_f);

                }
            }
            /* Criterion headers (TODO: support other frameworks?) */
            (void)fputs("#include <string.h>\n", pp->fpout_tests);
            (void)fputs("#include <criterion/criterion.h>\n", pp->fpout_tests);
            (void)fputs("#include <criterion/new/assert.h>\n\n", pp->fpout_tests);
        }

        size_t k = pp->kparse = 0;
        do {    // parsing loop
            /* setup temporary filename */
            char template[PATH_MAX];
            if ((size_t)snprintf(template, sizeof(template), "%s/cmod_%d_%zu.XXXXXX",
                                 opts.tmpdir, (int)pid, k)
                >= sizeof(template))
                FATAL_ERROR("failed creating temporary filename \"%s\": %s",
                            template, "truncated string");

            /* create temporary file */
            int fdtmp = -1;
            errno = 0;
            if ((fdtmp = mkstemp(template)) == -1)
                FATAL_ERROR("failed creating temporary file \"%s\": %s (%d)",
                            template, strerror(errno), errno);

            FILE *fptmp = NULL;
            /* open temporary file */
            errno = 0;
            if (NULL == (fptmp = fdopen(fdtmp, "w")))
                FATAL_ERROR("failed opening temporary file for write \"%s\": %s (%d)",
                            template, strerror(errno), errno);

            {   // append temporary filename to list
                char *str = rstrdup(template);
                if (NULL == (str)) {
                    FATAL_ERROR0("out of memory");
                }

                if (!sv_push(&(pp->sv_tmpfile), &(str))) {
                    FATAL_ERROR0("out of memory");
                }
            }
            /* set output file pointer */
            yyset_out(fptmp, yyscanner);

            const char *input_path = (sv_len(pp->sv_tmpfile) >= 2)
                ? sv_at_ptr(pp->sv_tmpfile, sv_len(pp->sv_tmpfile) - 2)
                : pp->curbs->fname;

            INFO0("BEGIN");
            VERBOSE("[input: %s] [output: %s]", input_path, template);

            /* do parse */
            pp->in_parse = true;
            int ret = yyparse(yyscanner, NULL);
            pp->in_parse = false;

            if (ret || pp->nerr > 0) {
                switch (ret) {
                case 0:        // from Bison YYACCEPT or success (with errors)
                    ERROR("failed parse: invalid input, %zu %s found, please check error log",
                          pp->nerr, (pp->nerr == 1) ? "error" : "errors");
                    break;
                case 1:        // from Bison YYABORT
                    ERROR0("failed parse: invalid input, cannot proceed");
                    break;
                case 2:        // from Bison YYNOMEM
                    ERROR0("failed parse: " "out of memory");
                    break;
                default:
                    ERROR0("failed parse: unknown error");
                    break;
                }
                VERBOSE0("input file(s):");
                VERBOSE("      " "%s", (optind == argc) ? "<stdin>" : hyphtostdin(argv[optind]));
                for (int i = optind + 1; i < argc; ++i)
                    VERBOSE("      " "%s", hyphtostdin(argv[i]));
                exit(EXIT_FAILURE);
            }

            pp->kparse = ++k;   // increase parse count
            {   // setup new buffer
                errno = 0;
                FILE *fpin = freopen(NULL, "r", yyget_out(yyscanner));  // reopen last output file
                if (NULL == fpin)
                    FATAL_ERROR0("failed reopening last output file for read");
                errno = 0;
                if (fseek(fpin, 0L, SEEK_SET) == -1)
                    FATAL_ERROR0("failed rewinding input stream");
                clearerr(fpin); // clear stream error indicator
                yyset_in(fpin, yyscanner);      // set input file pointer

                char buf[32] = { 0 };
                srt_string *parsename = ss_alloc_into_ext_buf(buf, sizeof(buf));
                ss_printf(&parsename, ss_max(parsename), "<parse:%zu>", k);

                ret_t ret = pushfile(yyscanner, pp, fpin, ss_to_c(parsename),
                                     sv_at_ptr(pp->sv_tmpfile, sv_len(pp->sv_tmpfile) - 1),
                                     true, no_blanks, false);
                if (ret) {
                    {
                        check_ret_t(ret, (const struct ret_f *)&pushfile_ret_f);

                    }
                    FATAL_ERROR0("failed pushing buffer into stack");
                }
            }
        } while (!pp->stop_parse && (!opts.steps_isset || k < opts.steps)
                 && k < pp->eval_limit);

        if (k == pp->eval_limit)
            FATAL_ERROR0("reached re-evaluation limit");

        pp->success = true;     // successful run
        {       // write output
            int ret;
            if ((ret = write_output(yyget_in(yyscanner), fd_out))) {
                const char *err = (ret == 1) ? "input" : "output";
                FATAL_ERROR("%s error while writing output file \"%s\": %s (%d)",
                            err, (NULL == pp->nameout) ? "<stdin>" : pp->nameout,
                            strerror(errno), errno);
            }
        }

        /* cleanup */
        if (fd_out != STDOUT_FILENO)
            close(fd_out);
        {       // pop last buffer
            ret_t ret = popfile(yyscanner, pp);
            if (ret) {
                WARN0("failed popping last buffer in stack");
                {
                    check_ret_t(ret, (const struct ret_f *)&popfile_ret_f);

                }
            }
        }
        yylex_destroy(yyscanner);       // free scanner
    }

/* redefine logging macros */
#undef INFO0
#define INFO0(x) \
    { if(!(glob_pp->silent > 0)) info0(x); }
#undef INFO
#define INFO(x, ...) \
    { if(!(glob_pp->silent > 0)) info(x,__VA_ARGS__); }
#undef WARN0
#define WARN0(x) \
    { if(!(glob_pp->silent > 1)) warn0(x); }
#undef WARN
#define WARN(x, ...) \
    { if(!(glob_pp->silent > 1)) warn(x,__VA_ARGS__); }
#undef ERROR0
#define ERROR0(x) \
    { if(!(glob_pp->silent > 2)) error0(x); }
#undef ERROR
#define ERROR(x, ...) \
    { if(!(glob_pp->silent > 2)) error(x,__VA_ARGS__); }
#undef VERBOSE0
#define VERBOSE0(x) \
    { if(!(!glob_pp->verbose)) verbose0(x); }
#undef VERBOSE
#define VERBOSE(x, ...) \
    { if(!(!glob_pp->verbose)) verbose(x,__VA_ARGS__); }
#undef DEBUG0
#define DEBUG0(x) \
    { if(!(glob_pp->silent > 3)) debug0(x); }
#undef DEBUG
#define DEBUG(x, ...) \
    { if(!(glob_pp->silent > 3)) debug(x,__VA_ARGS__); }

    {
        size_t nitem = sv_len(pp->sv_snip);
        if (report_snippet && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct snippet *item = sv_at(pp->sv_snip, i);
                if (strncmp("!cmod", item->name, 5) == 0)
                    continue;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_snip));
                puts("\n**" "Snippet" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct snippet *item = sv_at(pp->sv_snip, i);
                    if (strncmp("!cmod", item->name, 5) == 0)
                        continue;
                    srt_string *(ss) = ss_alloc(0);
                    if (ss_void == (ss)) {
                        FATAL_ERROR0("out of memory");
                    }

                    ss_cat_c(&ss, "+ `", item->name);
                    if (SNIPPET_HAS_FORMAL_ARGS(item)) {
                        if (item->nout > 0) {   // has output arguments
                            ss_cat_c(&ss, " (");
                            for (size_t i = 0; i < item->nout; ++i) {
                                if (i > 0)
                                    ss_cat_c(&ss, ", ");
                                const struct namarg *farg = sv_at(item->sv_forl, i);
                                if (farg->type != CMOD_ARG_NOVALUE)
                                    ss_cat_c(&ss, "[", farg->name, "]");
                                else
                                    ss_cat_c(&ss, farg->name);
                            }
                            ss_cat_c(&ss, ") <-");
                        }
                        /* input arguments */
                        ss_cat_c(&ss, " (");
                        for (size_t i = item->nout; i < item->nargs; ++i) {
                            if (i > item->nout)
                                ss_cat_c(&ss, ", ");
                            const struct namarg *farg = sv_at(item->sv_forl, i);
                            if (farg->type != CMOD_ARG_NOVALUE)
                                ss_cat_c(&ss, "[", farg->name, "]");
                            else
                                ss_cat_c(&ss, farg->name);
                        }
                        ss_cat_cn1(&ss, ')');
                    }
                    ss_cat_c(&ss, "`\n");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_free(&ss);
                }
                sv_free(&tmp);
            }
        }
    }
    {
        size_t nitem = sv_len(pp->sv_tab);
        if (report_table && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct table *item = sv_at(pp->sv_tab, i);
                (void)item;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_tab));
                puts("\n**" "Table" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct table *item = sv_at(pp->sv_tab, i);
                    srt_string *(ss) = ss_alloc(0);
                    if (ss_void == (ss)) {
                        FATAL_ERROR0("out of memory");
                    }

                    ss_cat_c(&ss, "+ `", item->name);
                    if (sv_len(item->sv_colnarg) > 0) {
                        ss_cat_c(&ss, " (");
                        {
                            const size_t _len = sv_len(item->sv_colnarg);
                            for (size_t _idx = 0; _idx < _len; ++_idx) {
                                const size_t _5 = _idx;
                                const size_t _idx = 0;
                                (void)_idx;
                                if (_5 > 0)
                                    ss_cat_c(&ss, ", ");
                                ss_cat_c(&ss,
                                         ((const struct namarg *)sv_at(item->sv_colnarg, _5))->
                                         name);
                            }
                        }
                        ss_cat_cn1(&ss, ')');
                    }
                    ss_cat_c(&ss, "`\n");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_free(&ss);
                }
                sv_free(&tmp);
            }
        }
    }
    {
        size_t nitem = sv_len(pp->sv_dtype);
        if (report_type && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct decl *item = sv_at(pp->sv_dtype, i);
                if (!item->iname.set)
                    continue;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_dtype));
                puts("\n**" "Type definition" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct decl *item = sv_at(pp->sv_dtype, i);
                    if (!item->iname.set)
                        continue;
                    srt_string *(ss) = ss_alloc(0);
                    if (ss_void == (ss)) {
                        FATAL_ERROR0("out of memory");
                    }

                    ss_cat_c(&ss, "+ `", item->name, "`\n", "      ", "\n", "      ");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_clear(ss);
                    {
                        srt_string *tmp = decl_to_str(item, (item)->name, "", true, false);
                        ss_cat(&(ss), tmp);
                        ss_free(&tmp);
                    }

                    ss_cat_c(&ss, "      ", "\n\n");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_free(&ss);
                }
                sv_free(&tmp);
            }
        }
    }
    {
        size_t nitem = sv_len(pp->sv_dproto);
        if (report_proto && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct decl *item = sv_at(pp->sv_dproto, i);
                (void)item;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_dproto));
                puts("\n**" "Function prototype" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct decl *item = sv_at(pp->sv_dproto, i);
                    srt_string *(ss) = ss_alloc(0);
                    if (ss_void == (ss)) {
                        FATAL_ERROR0("out of memory");
                    }

                    ss_cat_c(&ss, "+ `", item->name, "      ", "\n", "      ");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_clear(ss);
                    {
                        srt_string *tmp = decl_to_str(item, (item)->name,
                                                      ((item)->named || (item)->ndefault > 0)
                                                      ? ss_to_c((item)->fsuffix) : "",
                                                      true, false);
                        ss_cat(&(ss), tmp);
                        ss_free(&tmp);
                    }

                    ss_cat_c(&ss, "      ", "\n\n");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_free(&ss);
                }
                sv_free(&tmp);
            }
        }
    }
    {
        size_t nitem = sv_len(pp->sv_dsl);
        if (report_dsl && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct dsl *item = sv_at(pp->sv_dsl, i);
                (void)item;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_dsl));
                puts("\n**" "Domain-specific language" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct dsl *item = sv_at(pp->sv_dsl, i);
                    srt_string *(ss) = ss_alloc(0);
                    if (ss_void == (ss)) {
                        FATAL_ERROR0("out of memory");
                    }

                    ss_cat_c(&ss, "+ `", item->name, "`\n", "      ", "\n");
                    for (size_t i = 0; i < sv_len(item->kw_name); ++i)
                        ss_cat_c(&ss, "      ", sv_at_ptr(item->kw_name, i), "\t",
                                 ((const struct snippet *)sv_at_ptr(item->kw_snip, i))->name, "\n");
                    ss_cat_c(&ss, "      ", "\n\n");
                    ss_write(stdout, ss, 0, ss_len(ss));
                    ss_free(&ss);
                }
                sv_free(&tmp);
            }
        }
    }
    {
        size_t nitem = sv_len(pp->sv_fdef);
        if (report_fdef && nitem > 0) {
            size_t not_skipped = 0;
            for (size_t i = 0; i < nitem; ++i) {
                const struct fdef *item = sv_at(pp->sv_fdef, i);
                (void)item;
                not_skipped++;
            }
            if (not_skipped) {
                srt_vector *tmp = sv_sort(sv_dup(pp->sv_fdef));
                puts("\n**" "Function definition" "s" "**\n");
                for (size_t i = 0; i < nitem; ++i) {
                    const struct fdef *item = sv_at(pp->sv_fdef, i);
                    printf("+ `%s`\n", item->name);
                }
                sv_free(&tmp);
            }
        }
    }

    {
        size_t nitem = sms_len(pp->sms_once);
        if (report_once && nitem > 0) {
            puts("\n**" "Include/repeat guard" "s" "**\n");
            vec_ss *(v) = sv_alloc_t(SV_PTR, nitem);
            if (sv_void == (v)) {
                FATAL_ERROR0("out of memory");
            }
            ;
            for (size_t i = 0; i < nitem; ++i) {
                const srt_string *item = sms_it_s(pp->sms_once, i);
                sv_push(&v, &item);
            }
            sv_sort(v);
            for (size_t i = 0; i < nitem; ++i) {
                const srt_string *item = sv_at_ptr(v, i);
                if (NULL == item)
                    continue;
                const char *str = ss_to_c(item);
                printf("+ `%s`\n", str);
            }
            sv_free(&v);
        }
    }
    {
        size_t nitem = sms_len(pp->sms_enum);
        if (report_enum && nitem > 0) {
            puts("\n**" "Enum constant" "s" "**\n");
            vec_ss *(v) = sv_alloc_t(SV_PTR, nitem);
            if (sv_void == (v)) {
                FATAL_ERROR0("out of memory");
            }
            ;
            for (size_t i = 0; i < nitem; ++i) {
                const srt_string *item = sms_it_s(pp->sms_enum, i);
                sv_push(&v, &item);
            }
            sv_sort(v);
            for (size_t i = 0; i < nitem; ++i) {
                const srt_string *item = sv_at_ptr(v, i);
                if (NULL == item)
                    continue;
                const char *str = ss_to_c(item);
                printf("+ `%s`\n", str);
            }
            sv_free(&v);
        }
    }

 cleanup:
    {
        for (size_t i = 0; i < sv_len(opts.disable); ++i) {
            const void *_item = sv_at(opts.disable, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.disable));
    }
    {
        for (size_t i = 0; i < sv_len(opts.enable); ++i) {
            const void *_item = sv_at(opts.enable, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.enable));
    }
    {
        for (size_t i = 0; i < sv_len(opts.ipaths); ++i) {
            const void *_item = sv_at(opts.ipaths, i);
            {
                char **item = (char **)(_item);
                free(*item);
                *item = NULL;
            }
        }
        sv_free(&(opts.ipaths));
    }
    {
        free(opts.output);
        (opts.output) = NULL;
    }
    {
        free(opts.tests_output);
        (opts.tests_output) = NULL;
    }
    {
        free(opts.tmpdir);
        (opts.tmpdir) = NULL;
    }
}
