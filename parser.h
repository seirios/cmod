/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 14 "parser.y"

#ifndef CMOD_YYLVAL_H
#define CMOD_YYLVAL_H
#include "parts/struct_range.h"
#include "parts/struct_table_like.h"
#include "parts/struct_recall_like.h"
#include "parts/variant_c_typespec.h"
#include "parts/variant_c_specifier.h"
#endif

#line 60 "parser.h"

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    SCANNER_ERROR = 258,           /* SCANNER_ERROR  */
    TSV_VALUE = 259,               /* "TSV value"  */
    CHAR = 260,                    /* "character"  */
    DLLARG_TRANSFORMS = 261,       /* "argument modifiers"  */
    C_CONST = 262,                 /* "const"  */
    C_RESTRICT = 263,              /* "restrict"  */
    C_VOLATILE = 264,              /* "volatile"  */
    C_ATOMIC = 265,                /* "_Atomic"  */
    C_VOID = 266,                  /* "void"  */
    C_CHAR = 267,                  /* "char"  */
    C_SHORT = 268,                 /* "short"  */
    C_INT = 269,                   /* "int"  */
    C_LONG = 270,                  /* "long"  */
    C_SIGNED = 271,                /* "signed"  */
    C_UNSIGNED = 272,              /* "unsigned"  */
    C_BOOL = 273,                  /* "_Bool"  */
    C_FLOAT = 274,                 /* "float"  */
    C_DOUBLE = 275,                /* "double"  */
    C_COMPLEX = 276,               /* "_Complex"  */
    C_STRUCT = 277,                /* "struct"  */
    C_UNION = 278,                 /* "union"  */
    C_ENUM = 279,                  /* "enum"  */
    C_TYPEDEF = 280,               /* "typedef"  */
    C_EXTERN = 281,                /* "extern"  */
    C_STATIC = 282,                /* "static"  */
    C_THREAD_LOCAL = 283,          /* "_Thread_local"  */
    C_AUTO = 284,                  /* "auto"  */
    C_REGISTER = 285,              /* "register"  */
    C_INLINE = 286,                /* "inline"  */
    C_NORETURN = 287,              /* "_Noreturn"  */
    C_ALIGNAS = 288,               /* "_Alignas"  */
    C_IF = 289,                    /* "if"  */
    C_ELSE = 290,                  /* "else"  */
    C_SWITCH = 291,                /* "switch"  */
    C_DO = 292,                    /* "do"  */
    C_FOR = 293,                   /* "for"  */
    C_WHILE = 294,                 /* "while"  */
    C_GOTO = 295,                  /* "goto"  */
    C_CONTINUE = 296,              /* "continue"  */
    C_BREAK = 297,                 /* "break"  */
    C_RETURN = 298,                /* "return"  */
    C_CASE = 299,                  /* "case"  */
    C_DEFAULT = 300,               /* "default"  */
    C_GENERIC = 301,               /* "_Generic"  */
    C_STATIC_ASSERT = 302,         /* "_Static_assert"  */
    C_FUNC_NAME = 303,             /* "__func__"  */
    C_SIZEOF = 304,                /* "sizeof"  */
    C_ALIGNOF = 305,               /* "_Alignof"  */
    C_IMAGINARY = 306,             /* "_Imaginary"  */
    C_MUL_ASSIGN = 307,            /* "*="  */
    C_DIV_ASSIGN = 308,            /* "/="  */
    C_MOD_ASSIGN = 309,            /* "%="  */
    C_ADD_ASSIGN = 310,            /* "+="  */
    C_SUB_ASSIGN = 311,            /* "-="  */
    C_LEFT_ASSIGN = 312,           /* "<<="  */
    C_RIGHT_ASSIGN = 313,          /* ">>="  */
    C_AND_ASSIGN = 314,            /* "&="  */
    C_XOR_ASSIGN = 315,            /* "^="  */
    C_OR_ASSIGN = 316,             /* "|="  */
    C_LEFT_OP = 317,               /* "<<"  */
    C_RIGHT_OP = 318,              /* ">>"  */
    C_INCR_OP = 319,               /* "++"  */
    C_DECR_OP = 320,               /* "--"  */
    C_PTR_OP = 321,                /* "->"  */
    C_AND_OP = 322,                /* "&&"  */
    C_OR_OP = 323,                 /* "||"  */
    C_LE_OP = 324,                 /* "<="  */
    C_GE_OP = 325,                 /* ">="  */
    C_EQ_OP = 326,                 /* "=="  */
    C_NE_OP = 327,                 /* "!="  */
    C_ELLIPSIS = 328,              /* "..."  */
    C_IDENTIFIER = 329,            /* "C identifier"  */
    C_TYPEDEF_NAME = 330,          /* "C typedef name"  */
    C_ENUMERATION_CONSTANT = 331,  /* "C enum constant"  */
    C_STRING_LITERAL = 332,        /* "C string literal"  */
    C_INT_CONSTANT = 333,          /* "C integer constant"  */
    C_FP_CONSTANT = 334,           /* "C floating-point constant"  */
    MOD_INCLUDE = 335,             /* "% include"  */
    MOD_ONCE = 336,                /* "% once"  */
    MOD_SNIPPET = 337,             /* "% snippet"  */
    MOD_RECALL = 338,              /* "% recall"  */
    MOD_TABLE = 339,               /* "% table"  */
    MOD_TABLE_STACK = 340,         /* "% table-stack"  */
    MOD_MAP = 341,                 /* "% map"  */
    MOD_PIPE = 342,                /* "% pipe"  */
    MOD_UNITTEST = 343,            /* "% unittest"  */
    MOD_DSL_DEF = 344,             /* "% dsl-def"  */
    MOD_DSL = 345,                 /* "% dsl"  */
    MOD_INTOP = 346,               /* "% intop"  */
    MOD_DELAY = 347,               /* "% delay"  */
    MOD_DEFINED = 348,             /* "% defined"  */
    MOD_STRCMP = 349,              /* "% strcmp"  */
    MOD_STRSTR = 350,              /* "% strstr"  */
    MOD_STRLEN = 351,              /* "% strlen"  */
    MOD_STRSUB = 352,              /* "% strsub"  */
    MOD_TABLE_SIZE = 353,          /* "% table-size"  */
    MOD_TABLE_LENGTH = 354,        /* "% table-length"  */
    MOD_TABLE_GET = 355,           /* "% table-get"  */
    MOD_TYPEDEF = 356,             /* "% typedef"  */
    MOD_PROTO = 357,               /* "% proto"  */
    MOD_DEF = 358,                 /* "% def"  */
    MOD_UNUSED = 359,              /* "% unused"  */
    MOD_PREFIX = 360,              /* "% prefix"  */
    MOD_ENUM = 361,                /* "% enum"  */
    MOD_STRIN = 362,               /* "% strin"  */
    MOD_FOREACH = 363,             /* "% foreach"  */
    MOD_SWITCH = 364,              /* "% switch"  */
    MOD_FREE = 365,                /* "% free"  */
    MOD_ARRLEN = 366,              /* "% arrlen"  */
    MOD_RECALL_END = 367,          /* "|%"  */
    FROM = 368,                    /* "<-"  */
    PERIOD = 369,                  /* "."  */
    MOD_BRACE_OPEN = 370,          /* "%{"  */
    MOD_BRACE_CLOSE = 371,         /* "%}"  */
    MOD_HERESTR_OPEN = 372,        /* "%<<"  */
    MOD_HERESTR_CLOSE = 373,       /* ">>%"  */
    MOD_HERESTR_EMPTY = 374,       /* "%nul"  */
    MOD_ROWNO = 375,               /* "%NR"  */
    OPT_ASSIGN = 376,              /* "?="  */
    DEF_ASSIGN = 377,              /* ":="  */
    ASTERISK = 378,                /* "*"  */
    MOD_TABLE_OPEN = 379,          /* "%TSV|JSON{"  */
    DLLARG_SPECIAL = 380,          /* "special argument reference"  */
    DLLARG = 381,                  /* "argument reference"  */
    RANGE = 382,                   /* "integer range"  */
    IDENTIFIER = 383,              /* "identifier"  */
    IDENTIFIER_EXT = 384,          /* "extended identifier"  */
    SPECIAL_IDENTIFIER = 385,      /* "special identifier"  */
    FILE_PATH = 386,               /* "file path"  */
    SIGNED_INT = 387,              /* "signed integer"  */
    UNSIGNED_INT = 388             /* "unsigned integer"  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 256 "parser.y"

    char c;
    int i;
    struct range range;
    const char* ctxt;
    char* txt;
    srt_string* str;
    struct decl decl;
    arr_cdi acdi;
    arr_cdd acdd;
    struct dllarg dll;
    struct namarg narg;
    struct table tab;
    struct snippet snip;
    struct table_like tlike;
    struct recall_like rlike;
    struct c_designator cdsg;
    struct c_initializer cini;
    struct c_specifier cspc;
    struct c_typespec ctyp;
    struct xint xint;
    struct mod_case mcase;
    struct cmod_option opt;
    vec_xint* vxint;
    vec_string* vstr;
    vec_vec_string* vvstr;
    vec_arr_cdi* vacdi;
    vec_decl* vdecl;
    vec_mcase* vmcase;
    vec_namarg* vnarg;
    vec_vec_namarg* vvnarg;
    vec_cmopt* vopt;
    vec_cspec* vcspc;

#line 245 "parser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




int yyparse (void* yyscanner, struct param *pp);

/* "%code provides" blocks.  */
#line 25 "parser.y"


#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include <stdio.h>

#ifndef info0
#define info0(M) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((pp->silent > 3) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((pp->silent > 0) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","C%", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((pp->silent > 1) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","C%", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((pp->silent > 2) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","C%", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((!pp->verbose) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","C%", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","C%", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((pp->silent > 3) ? 0\
        : ((pp->pretty)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","C%", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","C%", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>


__attribute__ ((format(printf,4,5)))
void yyinfo(YYLTYPE*, void*, const struct param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
void yywarn(YYLTYPE*, void*, const struct param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
void yyerror(YYLTYPE*, void*, const struct param*, const char*, ...);
__attribute__ ((format(printf,4,5)))
void yyverbose(YYLTYPE*, void*, const struct param*, const char*, ...);
inline static void get_actual_argval(const struct namarg *narg, struct namarg *argval) {
    switch(narg->type) {
            case ENUM_NAMARG(INTSTR): // change type to verbatim
            *argval = CMOD_ARG_VERBATIM_xset(CMOD_ARG_INTSTR_get(*narg),.name= narg->name);
            break;
           default:
            *argval = *narg; // pass-through
            break;
    }
}
void print_error(YYLTYPE*, void*);
int errline_find_name(const char*, const char*, int, YYLTYPE*);
int collect_actual_args(vec_namarg*, vec_namarg*, const struct snippet *snip,
                        size_t lo, size_t nargs, const char* [static 1]);

#line 377 "parser.h"

#endif /* !YY_YY_PARSER_H_INCLUDED  */
